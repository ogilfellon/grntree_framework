package uk.ac.ncl.icos.eaframework.grn.simulator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author ogilfellon
 */
abstract class AbstractCopasiSimulator implements CopasiSimulator {

    protected CopasiModel m;
    protected int duration;
    protected int stepNumber;

    /**
     * Default constructor. Sets duration to 15,000 and runlength to 1000
     */
    public AbstractCopasiSimulator() {
        this(15000, 1000);
    }
    
    public AbstractCopasiSimulator(int duration, int stepNumber) {
        this.duration = duration;
        this.stepNumber = stepNumber;
        this.m = new CopasiModel(duration, stepNumber);
    }

    @Override
    public void setDuration(int duration) {
        this.duration = duration;
    }

    @Override
    public void setStepNumber(int runlength) {
        this.stepNumber = runlength;
    }

    @Override
    public void setModel(String sbml) {
        m.setSBMLDocument(sbml);
    }

    @Override
    public double getMetaboliteInitialValue(String metabolite) {
        return m.getMetaboliteInitialValue(metabolite);
    }

    @Override
    public void setMetaboliteValue(String metabolite, double metaboliteValue) {
        m.setMetaboliteInitialValue(metabolite, metaboliteValue);
    }

    @Override
    public List<Double> getTimeSeries(String metabolite) {
        return m.getTimeSeries(metabolite);
    }
    
    @Override
    public HashMap<String, ArrayList<Double>> getAllTimeSeries()        
    {
        return m.getAllTimeSeries();
    }

    @Override
    public abstract boolean run();
    
}
