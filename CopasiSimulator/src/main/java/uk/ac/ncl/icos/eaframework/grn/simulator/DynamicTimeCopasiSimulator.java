package uk.ac.ncl.icos.eaframework.grn.simulator;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ogilfellon
 */
public class DynamicTimeCopasiSimulator extends AbstractCopasiSimulator {
 
    protected double maxIncrementValue = 0.0;
    private int increments;
    
    private final double MIN_CHANGE = 0.0005; // i.e. change must be > 1 / MIN_CHANGE
    private int increment = 50;
    private final int MIN_NUMBER_OF_SIMULATIONS = 50;

    
    private String independentMetabolite;
    private String dependentMetabolite;
    
    private ArrayList<ArrayList<Double>> dependentMetaboliteValues;
    private ArrayList<Double> independentMetaboliteValues;

    public DynamicTimeCopasiSimulator(int duration,
                                        int runlength) {

        super(duration, runlength);
       
    }
    
    public void setNumberOfMetaboliteIncrements(int increments)
    {
        this.increments = increments;
    }
    
    public void setMaximumMetaboliteConcentration(int maxMetaboliteConcentration)
    {
        this.maxIncrementValue = maxMetaboliteConcentration;
    }
    
    public void setMetabolitePair(String independentMetabolite, String dependantMetabolite)
    {
        this.independentMetabolite = independentMetabolite;
        this.dependentMetabolite = dependantMetabolite;
    }
    
    public String getIndependentMetabolite()
    {
        return this.independentMetabolite; 
    }
    
    public String getDependentMetabolite()
    {
        
        return this.dependentMetabolite;
    }
    
    public ArrayList<ArrayList<Double>> getDependentMetaboliteValues()
    {
        return dependentMetaboliteValues;
    }
    
    public ArrayList<Double> getIndependentMetaboliteValues()
    {
        return independentMetaboliteValues;
    }

    @Override
    public boolean run() {
               
        dependentMetaboliteValues = new ArrayList<ArrayList<Double>>();
        independentMetaboliteValues = new ArrayList<Double>();

        double previousValue = 0;
        double currentValue = 0;
        double metaboliteValue = 0;

        // while the change in output still increases above a certain value

        do {           
            independentMetaboliteValues.add(metaboliteValue);
            m.setMetaboliteInitialValue(independentMetabolite, metaboliteValue);

            if(m.run()) {

                dependentMetaboliteValues.add((ArrayList)m.getTimeSeries(dependentMetabolite));
                List<Double> lastSubtilinRun = dependentMetaboliteValues.get(dependentMetaboliteValues.size()-1);
                previousValue = currentValue;
                currentValue = lastSubtilinRun.get(lastSubtilinRun.size() - 1);
                metaboliteValue += increment;
            }
            else {
                return false;
            }
               
        } while (((((currentValue - previousValue) / previousValue) / increment) * 100) > MIN_CHANGE ||
                    dependentMetaboliteValues.size() < MIN_NUMBER_OF_SIMULATIONS);

        //System.out.println(dependentMetaboliteValues.size() + " Subtilin runs");
        //System.out.println("Max Subtilin: " + independentMetaboliteValues.get(independentMetaboliteValues.size()-1));

        return true;
    }
}
