package uk.ac.ncl.icos.eaframework.grn.simulator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author ogilfellon
 */
public interface CopasiSimulator {
    
    public void setDuration(int duration);
    public void setStepNumber(int runlength);
    public void setModel(String sbml);
    
    public void setMetaboliteValue(String metabolite, double subtilinval);
    public double getMetaboliteInitialValue(String metabolite);
    public List<Double> getTimeSeries(String metabolite);
    public HashMap<String, ArrayList<Double>> getAllTimeSeries();

    public boolean run();
    
}
