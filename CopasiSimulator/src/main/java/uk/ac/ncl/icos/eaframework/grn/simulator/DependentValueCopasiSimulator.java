package uk.ac.ncl.icos.eaframework.grn.simulator;
import java.util.ArrayList;

/**
 *
 * @author ogilfellon
 */
public class DependentValueCopasiSimulator extends AbstractCopasiSimulator {
 
    protected double maxIncrementValue = 0.0;
    private int increments;
    
    private String independentMetabolite;
    private String dependentMetabolite;
    
    private ArrayList<ArrayList<Double>> dependentMetaboliteValues;
    private ArrayList<Double> independentMetaboliteValues;

    public DependentValueCopasiSimulator(int duration, int runlength) {

        super(duration, runlength);
    }
    
    public void setNumberOfMetaboliteIncrements(int increments)
    {
        this.increments = increments;
    }
    
    public void setMaximumMetaboliteConcentration(int maxMetaboliteConcentration)
    {
        this.maxIncrementValue = maxMetaboliteConcentration;
    }
    
    public void setMetabolitePair(String independentMetabolite, String dependantMetabolite)
    {
        this.independentMetabolite = independentMetabolite;
        this.dependentMetabolite = dependantMetabolite;
    }
    
    public String getIndependentMetabolite()
    {
        return this.independentMetabolite; 
    }
    
    public String getDependentMetabolite()
    {
        
        return this.dependentMetabolite;
    }
    
    public ArrayList<ArrayList<Double>> getDependentMetaboliteValues()
    {
        return dependentMetaboliteValues;
    }
    
    public ArrayList<Double> getIndependentMetaboliteValues()
    {
        return independentMetaboliteValues;
    }

    @Override
    public boolean run() {
        
        dependentMetaboliteValues = new ArrayList<ArrayList<Double>>();
        independentMetaboliteValues = new ArrayList<Double>();

        for (int increment = 0; increment < increments; increment++)
        {           
            double metaboliteValue = increment * (maxIncrementValue / increments);
            independentMetaboliteValues.add(metaboliteValue);
            
            m.setMetaboliteInitialValue(independentMetabolite, metaboliteValue);
            if(m.run())
            {
                dependentMetaboliteValues.add((ArrayList)m.getTimeSeries(dependentMetabolite));
            }
            else
            {
                return false;
            } 
        }
        
        return true;
    }
}
