package uk.ac.ncl.icos.eaframework.grn.simulator;

import org.COPASI.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author owengilfellon
 */
public class CopasiModel {
    
    protected HashMap<String, Long> nameKeys = new HashMap<String, Long>();

    protected CModel model;
    protected CTrajectoryTask trajectoryTask;
    
    // CCopasiDataModel contains the model, all tasks, reports and plots
    
    protected CCopasiDataModel datamodel;
    protected CTimeSeries timeSeries;
    
    protected HashMap<String, Double> metaboliteInitialValues = new HashMap<String, Double>();
    protected HashMap<String, ArrayList<Double>> timeseriesResults;
    
    private int stepNumber;
    private int duration;

    public CopasiModel(int duration, int stepNumber) {
        this.stepNumber = stepNumber;
        this.duration = duration;
    }

    public boolean run() {

        CTrajectoryTask trajectoryTask = (CTrajectoryTask) datamodel.getTask("Time-Course");
        assert trajectoryTask != null;
        trajectoryTask = new CTrajectoryTask();
        datamodel.getTaskList().addAndOwn(trajectoryTask);
        trajectoryTask.setMethodType(CCopasiMethod.deterministic);
        trajectoryTask.getProblem().setModel(datamodel.getModel());
        trajectoryTask.setScheduled(true);
        CTrajectoryProblem problem = (CTrajectoryProblem) trajectoryTask.getProblem();
        problem.setStepNumber(stepNumber);        
        datamodel.getModel().setInitialTime(0.0);
        problem.setDuration(duration);
        problem.setTimeSeriesRequested(true);

        boolean result = true;
        try {
            
            ObjectStdVector container = new ObjectStdVector();
            
            for(String key:metaboliteInitialValues.keySet())
            {
                if(model.findMetabByName(key) == -1){
                    return false;
                }
                CMetab metab = model.getMetabolite(model.findMetabByName(key));
                metab.setInitialConcentration(metaboliteInitialValues.get(key).doubleValue());
                container.add(metab.getObject(new CCopasiObjectName("Reference=InitialConcentration")));
            }
           
            model.updateInitialValues(container);
            result = trajectoryTask.process(true);
        } catch (java.lang.Exception ex) {
            System.err.println("Error. Running the time course simulation failed.");
            // check if there are additional error messages
            if (CCopasiMessage.size() > 0) {
                // print the messages in chronological order
                System.err.println(CCopasiMessage.getAllMessageText(true));
            }
            System.exit(1);
        }
        if (!result) {
            System.err.println("An error occured while running the time course simulation.");
            // check if there are additional error messages
            if (CCopasiMessage.size() > 0) {
                // print the messages in chronological order
                System.err.println(CCopasiMessage.getAllMessageText(true));
            }
            System.exit(1);
        }
        this.trajectoryTask = trajectoryTask;
        return true;    
    }
    
    public void setSBMLDocument(String SBMLDocument)
    {
        try {
            datamodel = CCopasiRootContainer.addDatamodel();
            datamodel.importSBMLFromString(SBMLDocument);
            this.model = datamodel.getModel();
            
            MetabVector mv = model.getMetabolites();
            CCopasiObject co;
            
            for(long i =0; i<mv.size(); i++)
            {
                co = mv.get(i);
                nameKeys.put(co.getObjectDisplayName(), Long.valueOf(i));
            }
            
        } catch (Exception ex) {
            Logger.getLogger(CopasiModel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void setDuration(int duration) {
        this.duration = duration;
    }

    public void setStepNumber(int runlength) {
        this.stepNumber = runlength;
    }
    
    public void setMetaboliteInitialValue(String metabolite, double metaboliteValue)
    {
        metaboliteInitialValues.put(metabolite, metaboliteValue);
    }
    
    public double getMetaboliteInitialValue(String metabolite)
    {
        return metaboliteInitialValues.get(metabolite);
    }
    
    public List<Double> getTimeSeries(String metabolite)
    {
            
        List<Double> timeseriesconcentrations = new ArrayList<Double>();
        
        this.timeSeries = this.trajectoryTask.getTimeSeries();      
     
        for (int i = 0; i < timeSeries.getRecordedSteps(); i++) {
         
            timeseriesconcentrations.add(timeSeries.getConcentrationData(i, (model.findMetabByName(metabolite) + 1)));
        }
        
        return timeseriesconcentrations;
    }
    
    public HashMap<String, ArrayList<Double>> getAllTimeSeries()
    {

        this.timeSeries = this.trajectoryTask.getTimeSeries();
        HashMap<String, ArrayList<Double>> timeseriesconcentrations = new HashMap<String, ArrayList<Double>>();
        
        for (int i = 0; i < timeSeries.getNumVariables(); i++)
        {
            ArrayList<Double> current = new ArrayList<Double>();
            String key = timeSeries.getTitle(i);

            for (int j = 0; j < timeSeries.getRecordedSteps(); j++) {

            current.add(timeSeries.getConcentrationData(j, (model.findMetabByName(key) + 1)));
        }
            
            timeseriesconcentrations.put(key, current);

    }
        
        return timeseriesconcentrations;
    }
    
    public List<String> getMetaboliteNames()
    {
        List keys = new ArrayList<String>();
        for (String s:nameKeys.keySet())
        {
            keys.add(s);
        }
        return keys;
    }
    
    public double getMetaboliteConcentration(String name)
    {
        long l = nameKeys.get(name);
        return model.getModelValue(l).getValue();
    }

    @Override
    public String toString()
    {
        
        StringBuilder sb = new StringBuilder();        
        MetabVector mv = model.getMetabolites();

        for(int i =0; i<mv.size(); i++)
        {
            CCopasiObject co = mv.get(i);
            
            sb.append("DName: ").append(co.getObjectDisplayName()).append(" | ");
            sb.append("Name: ").append(co.getObjectName()).append(" | ");

            CModelValue modelvalue = model.getModelValue(i);
            
            if(modelvalue != null)
            {
                sb.append("IValue: ").append(modelvalue.getInitialValue()).append(" | ");
                sb.append("Value: ").append(modelvalue.getValue());
            }
            sb.append("\n");
            
        }
        return sb.toString();
    }
}

