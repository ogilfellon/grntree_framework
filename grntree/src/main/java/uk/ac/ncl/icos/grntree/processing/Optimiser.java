package uk.ac.ncl.icos.grntree.processing;

import uk.ac.ncl.icos.grntree.api.GRNTree;
import uk.ac.ncl.icos.grntree.api.GRNTreeNode;
import uk.ac.ncl.icos.grntree.impl.BranchNode;
import uk.ac.ncl.icos.grntree.impl.LeafNode;
import uk.ac.ncl.icos.grntree.impl.NodeManager;
import uk.ac.ncl.icos.grntree.impl.SVPType;
import uk.ac.ncl.icos.grntree.traversal.FlowNavigator;
import uk.ac.ncl.intbio.virtualparts.entity.Property;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by owengilfellon on 27/02/2014.
 */
public class Optimiser {


   private GRNTree tree;
   private String ignore;
   private FlowNavigator flownav;

   public Optimiser(GRNTree tree, String ignore)
   {
       this.tree = tree;
       this.ignore = ignore;
   }

   public void optimise()
   {
       removeDownstreamElements();
       removeInducedTUs();
       while(loop());

   }

    private boolean loop()
    {
        boolean modified = false;
        if(removeRedundantParts()) modified = true;
        if(removeEmptyTUs()) modified = true;
        return modified;
    }

    private boolean removeDownstreamElements(){

        System.out.println("Remove Downstream Elements...");

        this.flownav = tree.getFlowNavigator(ignore);
        while(flownav.hasNext()) {
            flownav.next();
        }
        List<GRNTreeNode> downstream = flownav.getUnvisited();
        for(GRNTreeNode bn:downstream) {
            bn.getParent().removeNode(bn);
        }

        System.out.println("Downstream: " + downstream.size());

        return !downstream.isEmpty();
    }

    private boolean removeInducedTUs()
    {

        System.out.println("Remove Uninduced TUs...");

        List<LeafNode> redundant = new ArrayList<LeafNode>();
        Iterator<GRNTreeNode> it = tree.getPreOrderIterator();

        while(it.hasNext()){

            GRNTreeNode node = it.next();

            if(node instanceof LeafNode) {

                LeafNode ln = (LeafNode) node;

                if(ln.getType() == SVPType.Prom) {

                    List<Property> properties = ln.getSVP().getProperties();

                    if(properties!=null){

                        for(Property p:properties){

                            if(p.getValue().equals("BO_InduciblePromoter") ||
                                    p.getValue().equals("BO_RepressiblePromoter")) {

                                if(tree.getInteractingParts(ln).isEmpty()) {
                                    redundant.add(ln);
                                }
                            }
                        }
                    }
                }

            }
        }

        for(LeafNode ln:redundant) {
            if(ln.getType() == SVPType.Prom) {

                // Remove entire TU

                ln.getParent().getParent().removeNode(ln.getParent());
            }
        }

        System.out.println("Uninduced: " + redundant.size());

        return !redundant.isEmpty();
    }

    // TODO remove parts involved in phosphorylation, that do not regulate TUs

    private boolean removeRedundantParts()
    {
        System.out.println("Remove Unrepressed Ops and redundant CDSs...");

        List<LeafNode> redundant = new ArrayList<LeafNode>();
        Iterator<GRNTreeNode> it = tree.getPreOrderIterator();
        GRNTreeNode prevPart = null;

        while(it.hasNext()){

            GRNTreeNode node = it.next();

            if(node instanceof LeafNode) {

                LeafNode ln = (LeafNode) node;

                if(!ln.getSVP().getName().equals(ignore) &&
                        (ln.getType() == SVPType.CDS || ln.getType() == SVPType.Op)){

                    if(tree.getInteractingParts(ln).isEmpty()) {
                        redundant.add(ln);

                        if(ln.getType() ==SVPType.CDS) {
                            redundant.add((LeafNode)prevPart);
                        }
                    }
                }
            }

            prevPart = node;
        }

        for(LeafNode ln:redundant) {

                ln.getParent().removeNode(ln);
        }

        System.out.println("Redundant: " + redundant.size());


        return !redundant.isEmpty();
    }

    private boolean removeEmptyTUs() {

        System.out.println("Remove Empty TUs...");

        List<BranchNode> redundant = new ArrayList<BranchNode>();
        Iterator<GRNTreeNode>  it = tree.getPreOrderIterator();

        while(it.hasNext()){

            GRNTreeNode node = it.next();

            if(node.isTranscriptionUnit()) {

                BranchNode bn = (BranchNode) node;
                boolean containsCDS = false;

                for(GRNTreeNode  child:bn.getChildren()) {

                    LeafNode ln = (LeafNode) child;

                    if(ln.getType().equals(SVPType.CDS)) {
                        containsCDS = true;
                    }
                }

                if (!containsCDS) {
                    redundant.add(bn);
                }
            }
        }

        for(BranchNode bn:redundant) {
            bn.getParent().removeNode(bn);
        }

        System.out.println("Empty: " + redundant.size());

        return !redundant.isEmpty();
    }

}
