package uk.ac.ncl.icos.grntree.tests;

import junit.framework.TestCase;
import uk.ac.ncl.icos.grntree.api.GRNTree;
import uk.ac.ncl.icos.grntree.api.GRNTreeFactory;
import uk.ac.ncl.icos.grntree.api.GRNTreeNode;
import uk.ac.ncl.icos.grntree.impl.BranchNode;
import uk.ac.ncl.icos.grntree.impl.LeafNode;
import uk.ac.ncl.icos.grntree.impl.NodeManager;
import uk.ac.ncl.icos.grntree.impl.SVPType;
import uk.ac.ncl.intbio.virtualparts.entity.Property;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by owengilfellon on 27/02/2014.
 */
public class RedundantPartsTest extends TestCase {


   public void testRedundantParts()
   {
/*
         String model = "PspaS:Prom; RBS_SpaR:RBS; SpaR:CDS; BO_28140:RBS; SpaK:CDS; BO_27925:RBS; BO_31152:CDS; BO_4296:Ter; " +
               "BO_27654:Prom; BO_4062:Op; RBS_SpaS:RBS; GFP_rrnb:CDS; BO_4296:Ter; " +
               "BO_3017:Prom; BO_27814:RBS; SpaR:CDS; BO_28246:RBS; BO_32147:CDS; BO_27875:RBS; BO_32147:CDS; BO_28522:RBS; BO_28831:CDS; BO_28458:RBS; BO_28831:CDS; BO_28458:RBS; BO_28831:CDS; BO_5248:Ter; " +
               "BO_3475:Prom; BO_28458:RBS; BO_28831:CDS; BO_6486:Ter; " +
               "BO_3403:Prom; BO_27793:RBS; SpaK:CDS; BO_5388:Ter; " +
               "BO_3403:Prom; BO_28140:RBS; BO_32147:CDS; BO_5418:Ter";
 */

       String model = "BO_2917:Prom; BO_28510:RBS; SpaK:CDS; BO_27993:RBS; BO_32147:CDS; BO_27893:RBS; BO_32743:CDS; BO_4973:Ter; PspaS:Prom; BO_4227:Op; BO_3906:Op; BO_28326:RBS; BO_32307:CDS; BO_28326:RBS; BO_32307:CDS; BO_5427:Ter; BO_2939:Prom; BO_3583:Op; BO_28333:RBS; SpaR:CDS; BO_4775:Ter; PspaS:Prom; BO_3789:Op; BO_28475:RBS; SpaK:CDS; BO_28017:RBS; BO_32147:CDS; BO_5498:Ter; BO_3143:Prom; BO_28493:RBS; BO_28950:CDS; BO_28224:RBS; BO_32997:CDS; BO_6476:Ter; BO_27718:Prom; BO_28220:RBS; BO_31762:CDS; BO_28275:RBS; BO_31001:CDS; BO_4813:Ter; BO_3403:Prom; BO_28182:RBS; BO_32601:CDS; BO_28486:RBS; BO_32601:CDS; BO_28234:RBS; BO_32227:CDS; BO_28270:RBS; BO_30746:CDS; BO_5738:Ter; PspaS:Prom; BO_3600:Op; BO_3906:Op; BO_28092:RBS; BO_32307:CDS; BO_28142:RBS; BO_32307:CDS; BO_28096:RBS; BO_28950:CDS; BO_28317:RBS; BO_32731:CDS; BO_28152:RBS; BO_28950:CDS; BO_5427:Ter; BO_27782:Prom; BO_28152:RBS; BO_32633:CDS; BO_28298:RBS; SpaK:CDS; BO_28518:RBS; BO_30680:CDS; BO_5205:Ter; BO_27780:Prom; BO_3596:Op; BO_27809:RBS; BO_28831:CDS; BO_28322:RBS; SpaR:CDS; BO_5139:Ter; PspaS:Prom; BO_28326:RBS; BO_32307:CDS; BO_28319:RBS; BO_31337:CDS; BO_27809:RBS; BO_28831:CDS; BO_27910:RBS; BO_31762:CDS; RBS_SpaK:RBS; GFP_rrnb:CDS; BO_5427:Ter; BO_2937:Prom; BO_28152:RBS; BO_28950:CDS; BO_6420:Ter; PspaS:Prom; BO_3906:Op; BO_28260:RBS; BO_31762:CDS; BO_5483:Ter; PspaS:Prom; BO_3559:Op; BO_3906:Op; BO_28326:RBS; BO_32307:CDS; BO_28144:RBS; BO_32307:CDS; BO_28326:RBS; BO_32307:CDS; BO_28157:RBS; BO_28950:CDS; BO_28182:RBS; BO_32601:CDS; BO_5427:Ter; BO_3388:Prom; RBS_SpaK:RBS; GFP_rrnb:CDS; BO_28185:RBS; BO_32743:CDS; BO_4296:Ter; PspaS:Prom; BO_3906:Op; BO_28326:RBS; BO_32307:CDS; BO_27910:RBS; BO_31762:CDS; BO_5427:Ter; PspaS:Prom; BO_28082:RBS; BO_32307:CDS; BO_28096:RBS; BO_28950:CDS; BO_27910:RBS; BO_31762:CDS; BO_5427:Ter; PspaS:Prom; BO_3906:Op; BO_28326:RBS; BO_32307:CDS; BO_28319:RBS; BO_31337:CDS; BO_27900:RBS; BO_32307:CDS; BO_5427:Ter; PspaS:Prom; BO_28152:RBS; BO_28950:CDS; BO_27836:RBS; BO_28950:CDS; BO_6420:Ter; PspaS:Prom; BO_4190:Op; BO_3600:Op; BO_3906:Op; BO_27847:RBS; BO_32307:CDS; BO_28326:RBS; BO_32307:CDS; BO_27903:RBS; BO_32307:CDS; BO_28096:RBS; BO_28950:CDS; BO_28051:RBS; BO_32731:CDS; BO_28391:RBS; BO_30753:CDS; BO_5427:Ter; PspaS:Prom; BO_3906:Op; BO_28134:RBS; BO_32307:CDS; BO_28326:RBS; BO_28892:CDS; BO_5427:Ter; BO_27782:Prom; BO_3711:Op; BO_28326:RBS; BO_32307:CDS; BO_5427:Ter; PspaS:Prom; RBS_SpaK:RBS; GFP_rrnb:CDS; BO_4296:Ter; PspaS:Prom; BO_28326:RBS; GFP_rrnb:CDS; BO_4296:Ter; PspaS:Prom; RBS_SpaK:RBS; GFP_rrnb:CDS; BO_4296:Ter; PspaS:Prom; RBS_SpaK:RBS; GFP_rrnb:CDS; BO_4296:Ter; PspaS:Prom; BO_3606:Op; BO_3906:Op; BO_28326:RBS; BO_32307:CDS; BO_28319:RBS; BO_31337:CDS; BO_27900:RBS; BO_32307:CDS; BO_5427:Ter; PspaS:Prom; RBS_SpaK:RBS; GFP_rrnb:CDS; BO_28144:RBS; BO_32307:CDS; BO_4296:Ter; PspaS:Prom; RBS_SpaK:RBS; GFP_rrnb:CDS; BO_4296:Ter; BO_27782:Prom; BO_28152:RBS; BO_32633:CDS; BO_28298:RBS; SpaK:CDS; BO_28287:RBS; SpaR:CDS; BO_28249:RBS; BO_32653:CDS; BO_6472:Ter; BO_3387:Prom; BO_28096:RBS; BO_32997:CDS; BO_4583:Ter; PspaS:Prom; RBS_SpaK:RBS; GFP_rrnb:CDS; BO_28144:RBS; BO_32307:CDS; BO_4296:Ter; BO_27629:Prom; BO_28287:RBS; SpaR:CDS; BO_28249:RBS; BO_32653:CDS; BO_6472:Ter";

       GRNTree tree = GRNTreeFactory.getGRNTree(model);
       String ignore = "GFP_rrnb";

       List<LeafNode> redundant = new ArrayList<LeafNode>();

       Iterator<GRNTreeNode> it = tree.getPreOrderIterator();
       while(it.hasNext()){
           GRNTreeNode node = it.next();
           if(node instanceof LeafNode) {
               LeafNode ln = (LeafNode) node;
               if(!ln.getSVP().getName().equals(ignore)){
                   if(ln.getType() == SVPType.CDS){



                        if(tree.getInteractingParts(ln).isEmpty()) {

                            redundant.add(ln);
                        }
                       else
                        {
                            System.out.println(tree.getInteractingParts(ln).size());
                        }
                   }
                   else if (ln.getType() == SVPType.Op)
                       {
                           if(tree.getInteractingParts(ln).isEmpty()) {

                               //redundant.add(ln);
                           }
                       }
                       else if(ln.getType() == SVPType.Prom)
                       {
                           List<Property> properties = ln.getSVP().getProperties();
                           if(properties!=null){
                               for(Property p:properties){
                                   if(p.getValue().equals("BO_InduciblePromoter") ||
                                           p.getValue().equals("BO_RepressiblePromoter")){
                                       if(tree.getInteractingParts(ln).isEmpty()) {

                                           redundant.add(ln);
                                       }
                                   }
                               }
                           }
                       }
                   }
               }
           }


       System.out.println("Redundant Parts : " + redundant.size());
       List<BranchNode> redundantTUs = new ArrayList<BranchNode>();



       for(LeafNode ln:redundant) {

           System.out.println(ln);

           if(ln.getType() ==SVPType.Prom) {
               redundantTUs.add(( BranchNode)ln.getParent());
           }
           else {
               ln.getParent().removeNode(ln);
           }
       }



       it = tree.getPreOrderIterator();
       while(it.hasNext()){
           GRNTreeNode node = it.next();
           if(node.isTranscriptionUnit()) {
               BranchNode bn = (BranchNode) node;
               boolean containsCDS = false;
               for(GRNTreeNode  child:bn.getChildren()) {
                   LeafNode ln = (LeafNode) child;
                   if(ln.getType().equals(SVPType.CDS)) {
                        containsCDS = true;
                   }
               }

               if (!containsCDS) {
                   redundantTUs.add(bn);
               }
           }
       }

       System.out.println("Redundant TUs : " + redundantTUs.size());


   }

}
