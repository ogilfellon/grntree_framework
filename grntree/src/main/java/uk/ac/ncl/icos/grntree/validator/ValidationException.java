package uk.ac.ncl.icos.grntree.validator;

/**
 * Created by owengilfellon on 13/05/2014.
 */
public class ValidationException extends Exception {

    public ValidationException(String message)
    {
        super(message);
    }

}
