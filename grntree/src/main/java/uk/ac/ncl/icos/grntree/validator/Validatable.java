package uk.ac.ncl.icos.grntree.validator;

/**
 * Created by owengilfellon on 13/05/2014.
 */
public interface Validatable {

    public void validateWith(Validator validator) throws ValidationException;

}
