package uk.ac.ncl.icos.grntree.validator;

import uk.ac.ncl.icos.grntree.api.GRNTree;
import uk.ac.ncl.icos.grntree.api.GRNTreeNode;
import uk.ac.ncl.icos.grntree.impl.BranchNode;
import uk.ac.ncl.icos.grntree.impl.LeafNode;

import java.util.List;

/**
 * Created by owengilfellon on 13/05/2014.
 */
public class GRNTreeValidator implements Validator {


    @Override
    public void validate(Validatable validatable) throws ValidationException {

        if(validatable instanceof GRNTree) {

        }
        else if (validatable instanceof BranchNode) {

            BranchNode node = (BranchNode) validatable;
            if(node.hasSiblings()) {
                List<GRNTreeNode> siblings = node.getSiblings();
                for(GRNTreeNode sibling : siblings) {
                    if (sibling instanceof LeafNode) {
                        throw new ValidationException("Cannot mix BranchNodes and LeafNodes");
                    }
                }
            }
        }
        else if (validatable instanceof LeafNode) {
            LeafNode node = (LeafNode) validatable;
            if(node.hasSiblings()) {
                List<GRNTreeNode> siblings = node.getSiblings();
                for(GRNTreeNode sibling : siblings) {
                    if (sibling instanceof  BranchNode) {
                        throw new ValidationException("Cannot mix BranchNodes and LeafNodes");
                    }
                }
            }
        }
        else { throw new ValidationException("Not a GRNTree element");}
    }
}
