package uk.ac.ncl.icos.grntree.tests;

import junit.framework.TestCase;
import uk.ac.ncl.icos.grntree.api.GRNTree;
import uk.ac.ncl.icos.grntree.api.GRNTreeFactory;
import uk.ac.ncl.icos.grntree.impl.LeafNode;

/**
 * Created by owengilfellon on 28/02/2014.
 */
public class GRNTreeNodeTest extends TestCase {

    public void testEquality() {
        GRNTree tree = GRNTreeFactory.getGRNTree(
                "PspaS:Prom; RBS_SpaR:RBS; SpaR:CDS; RBS_SpaK:RBS; SpaK:CDS; BO_4296:Ter;" +
                        "PspaS:Prom; RBS_SpaR:RBS; SpaR:CDS; RBS_SpaK:RBS; SpaK:CDS; BO_4296:Ter;" +
                        "PspaS:Prom; RBS_SpaS:RBS; GFP_rrnb:CDS; RBS_SpaR:RBS; SpaR:CDS; RBS_SpaK:RBS; SpaK:CDS; BO_4296:Ter");


        System.out.println(((LeafNode)tree.getRootNode().getChildren().get(0).getChildren().get(0)).getSVP().getName());

        System.out.println("LeafNode Equality: ");
        if(tree.getRootNode().getChildren().get(0).getChildren().get(0).equals(tree.getRootNode().getChildren().get(1).getChildren().get(0))) {
            System.out.println("=Correct");
        }
        else {
            System.out.println("=Incorrect");
        }

        System.out.println("LeafNode Inequality: ");
        if(tree.getRootNode().getChildren().get(0).getChildren().get(0).equals(tree.getRootNode().getChildren().get(1).getChildren().get(1))) {
            System.out.println("=Incorrect");
        }
        else {
            System.out.println("=Correct");
        }

        System.out.println("BranchNode Equality: ");
        if(tree.getRootNode().getChildren().get(0).equals(tree.getRootNode().getChildren().get(1))) {
            System.out.println("=Correct");
        }
        else {
            System.out.println("=Incorrect");
        }

        System.out.println("BranchNode Inequality: ");
        if(tree.getRootNode().getChildren().get(0).equals(tree.getRootNode().getChildren().get(2))) {
            System.out.println("=Incorrect");
        }
        else {
            System.out.println("=Correct");
        }

        GRNTree tree2 = (GRNTree) tree.duplicate();

        System.out.println("Duplicate Equality: ");
        if(tree.getRootNode().equals(tree2.getRootNode())) {
            System.out.println("=Correct");
        }
        else {
            System.out.println("=Incorrect");
        }

        tree.getRootNode().removeNode(tree.getRootNode().getChildren().get(1));

        System.out.println("Duplicate Inequality: ");
        if(tree.getRootNode().equals(tree2.getRootNode())) {
            System.out.println("=Incorrect");
        }
        else {
            System.out.println("=Correct");
        }




    }
}
