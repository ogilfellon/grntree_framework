package uk.ac.ncl.icos.grntree.validator;

/**
 * Created by owengilfellon on 13/05/2014.
 */
public interface Validator {

    public void validate(Validatable validatable) throws ValidationException;

}
