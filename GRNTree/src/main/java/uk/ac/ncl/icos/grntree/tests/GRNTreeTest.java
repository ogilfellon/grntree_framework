package uk.ac.ncl.icos.grntree.tests;

import junit.framework.TestCase;
import uk.ac.ncl.icos.svpmanager.SVPManager;
import uk.ac.ncl.icos.grntree.api.GRNTree;
import uk.ac.ncl.icos.grntree.api.GRNTreeFactory;
import uk.ac.ncl.icos.grntree.api.GRNTreeNode;
import uk.ac.ncl.icos.grntree.api.NetworkSerialiser;
import uk.ac.ncl.icos.grntree.impl.LeafNode;
import uk.ac.ncl.icos.grntree.io.XMLNetworkSerialiser;
import uk.ac.ncl.intbio.virtualparts.entity.Interaction;

import java.io.*;
import java.util.Iterator;
import java.util.Set;

/**
 * Created by owengilfellon on 17/11/2014.
 */
public class GRNTreeTest extends TestCase {

    GRNTree tree1;
    GRNTree tree2;

    public static void main(String[] args) {
        GRNTreeTest test = new GRNTreeTest();
        try {
            //test.testSerialization();
            //test.testDeserialization();
            test.textXStream();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void textXStream() throws Exception {
        GRNTree tree = GRNTreeFactory.getGRNTree("PspaRK:Prom; RBS_SpaK:RBS; SpaK:CDS; RBS_SpaR:RBS; SpaR:CDS; BO_4296:Ter; PspaS:Prom; RBS_SpaS:RBS; GFP_rrnb:CDS; BO_4296:Ter");

        //tree.getRootNode().increaseDepth();
        
        
        //GRNTree tree = GRNTreeFactory.getGRNTree("");
        System.out.println("Original: " + tree);
        NetworkSerialiser<String> s = new XMLNetworkSerialiser();
        String serialised = s.exportNetwork(tree);

        FileWriter fileWriter = new FileWriter("model.xml");
        BufferedWriter out = new BufferedWriter(fileWriter);


        out.write(serialised);
        out.close();
        fileWriter.close();

        System.out.println("Serialised: \n" + serialised);
        GRNTree tree2 = s.importNetwork(serialised);
        System.out.println("Imported: " + tree);
    }


    public void testSerialization() throws Exception {
        tree1 = GRNTreeFactory.getGRNTree("");
        debugTree(tree1);
        FileOutputStream fileOut =  new FileOutputStream("grntree.ser");
        ObjectOutputStream out = new ObjectOutputStream(fileOut);
        out.writeObject(tree1);
        fileOut.flush();
        out.close();
        fileOut.close();
    }

    public void testDeserialization() throws Exception {
        GRNTree tree2 = null;
        //FileInputStream fileIn = new FileInputStream("grntree.ser");
        FileInputStream fileIn = new FileInputStream("new.ser");
        ObjectInputStream in = new ObjectInputStream(fileIn);
        tree2 = (GRNTree) in.readObject();
        in.close();
        fileIn.close();

        System.out.println(tree2);
        if(tree2 != null) {
            debugTree(tree2);
        }

        System.out.println("Tree1 == Tree2 : " + tree1.equals(tree2));
    }

    public void testPartSerialization() throws Exception {
        SVPManager m = SVPManager.getSVPManager();
        m.getPart("PspaS");


    }

    public void debugTree(GRNTree tree) {

        Iterator<GRNTreeNode> it = tree.getPreOrderIterator();
        while(it.hasNext()) {
            GRNTreeNode node = it.next();
            if(node instanceof LeafNode) {
                Set<Interaction> interactions = tree.getInteractions(node);
                if(interactions != null) {
                    for(Interaction interaction : interactions) {
                        System.out.println(node + " | " + interaction.getName());
                    }
                }
            }
        }
    }

}
