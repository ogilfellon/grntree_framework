package uk.ac.ncl.icos.grntree.api;


import uk.ac.ncl.icos.grntree.impl.LeafNode;
import uk.ac.ncl.icos.grntree.impl.InterfaceType;
import uk.ac.ncl.icos.grntree.impl.NodeManager;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import uk.ac.ncl.icos.svpmanager.SVPManager;
import uk.ac.ncl.icos.grntree.traversal.BreadthFirstIterator;
import uk.ac.ncl.icos.grntree.traversal.FlowNavigator;
import uk.ac.ncl.icos.grntree.traversal.PreOrderIterator;
import uk.ac.ncl.intbio.virtualparts.entity.Interaction;
import uk.ac.ncl.intbio.virtualparts.entity.Part;

import java.io.Serializable;
import java.util.*;


/**
 * A tree-based structure for the hierarchical representation of Genetic Regulatory Networks (GRNs).
 * "Interface Types" can be specified in order to limit the interactionDocuments permitted between nodes (e.g. INPUT, OUTPUT).
 * A node (e.g. Promoter) with interface type INPUT can interact with sibling OUTPUT nodes. If a parent has an Interface
 * Type, the bounds of a node's potential interactionDocuments are extended to the parent's siblings. The set of nodes specified
 * by a given node's interaction bounds and it's compliment must be orthogonal (i.e. interactionDocuments are not allowed outside
 * of a given node's interaction bounds).
 *
 * @author owengilfellon
 */

public class GRNTree implements Serializable, ModelSubject {

    final private String xmlns = "http://www.ico2s.org/synbad";
    final private transient SVPManager m = SVPManager.getSVPManager();
    final private NodeManager nodeManager;
    final private  GRNTreeNode rootNode;

    protected GRNTree()
    {
        nodeManager = new NodeManager();
        this.rootNode = GRNTreeNodeFactory.getBranchNode(InterfaceType.NONE);
        this.rootNode.setNodeManager(nodeManager);
    }

    protected GRNTree(GRNTreeNode rootNode)
    {
        nodeManager = new NodeManager();
        this.rootNode = rootNode;
        this.rootNode.setNodeManager(nodeManager);
    }

    private NodeManager getNodeManager()
    {
        return nodeManager;
    }

    /**
     * Returns the root node of the tree. The root is a branch node, and a container
     * for other nodes.
     * @return the root Node of the tree.
     */
    public GRNTreeNode getRootNode()
    {
        return rootNode;
    }

    public boolean isDetectingInteractions() {
        return nodeManager.isDetectingInteractions();
    }

    public void setDetectInteractions(boolean detectInteractions) {
       nodeManager.setDetectInteractions(detectInteractions);
    }

    /**
     * Returns the number of nodes in the tree, including all branch and leaf nodes.
     * @return the number of nodes in the tree
     */
    public int getNodesSize()
    {
        int nodesSize = 0;
        Iterator<GRNTreeNode> it = this.getPreOrderIterator();
        while(it.hasNext())
        {
           nodesSize++;
           it.next();
        }

        return nodesSize;
    }

    /**
     * Returns the number of leaf nodes (i.e. SVPs) in the tree.
     * @return
     */
    public int getPartsSize()
    {
        return nodeManager.getSize();
    }

    /**
     * Returns the number of transcriptional units in the tree (i.e. those nodes that are non-empty and contain only
     * LeafNodes)
     * @return
     */
    public int getTUSize()
    {
        int tuSize = 0;
        Iterator<GRNTreeNode> it = new PreOrderIterator(this);

        while(it.hasNext()) {
            GRNTreeNode node = it.next();
            if(node.isTranscriptionUnit()) {
                tuSize++;
            }
        }

        return tuSize;
    }

    /**
     * Returns the number of unique interactions in the tree
     * @return
     */
    public int getInteractionsSize()
    {
        return nodeManager.getInteractions().size();
    }

    /**
     * Returns all parts (i.e. SVPs) that are in the tree
     * @return
     */
    public List<Part> getAllParts()
    {
        return rootNode.getDescendents();
    }

    /**
     * Returns all input nodes (i.e. those that have been selected to receive inputs from outside their parent node)
     * @return
     */
    public List<GRNTreeNode> getInputNodes()
    {
        return rootNode.getInputNodes();
    }

    /**
     * Returns all Output Nodes (i.e. those that have been selected to send output outside their parent node)
     * @return
     */
    public List<GRNTreeNode> getOutputNodes()
    {
        return rootNode.getOutputNodes();
    }

    /**
     * Returns the parents of the given part.
     * @param node
     * @return 
     */
    public List<GRNTreeNode> getParents(GRNTreeNode node) {
        if(!(node instanceof LeafNode)) {
            List<GRNTreeNode> nodes = new ArrayList<>();
            nodes.add(node.getParent());
            return nodes;
        }
        return nodeManager.getParents((LeafNode)node);
    }

    /**
     * Returns all interactions within the tree
     * @return 
     */
    
    public Set<Interaction> getInteractions() {
        return nodeManager.getInteractions();
    }

    /**
     * Returns the interactions within the tree involving the supplied leaf node.
     * @param node
     * @return 
     */
    public Set<Interaction> getInteractions(GRNTreeNode node){
        if(!(node instanceof LeafNode)) {
            return new HashSet<>();
        }

        return nodeManager.getInteractions((LeafNode)node);

    }

     /**
     * Returns the leaf nodes that interact with the given leaf node
     * @param node
     * @return 
     */
    public List<GRNTreeNode> getInteractingParts(GRNTreeNode node) {
        if(!(node instanceof LeafNode)) {
            return new ArrayList<>();
        }
        return nodeManager.getInteractingNodes((LeafNode) node);
    }
    
    /**
     * Returns true if the part represented by the leaf node is in the tree
     * @param leafNode
     * @return 
     */
    public boolean containsLeafNode(GRNTreeNode leafNode)
    {
        if(!(leafNode instanceof LeafNode)) {
            return false;
        }
        return nodeManager.containsNode((LeafNode) leafNode);
    }
    
    /**
     * Returns the parts in the tree associated with a given interaction
     * @param interaction
     * @return 
     */
    public List<GRNTreeNode> getParts(Interaction interaction) {
        return nodeManager.getNodes(interaction);
    }
    
    /**
     * Returns a list of GRNTreeNodes that contain SVPs with the provided name
     * @param name
     * @return 
     */
    public List<GRNTreeNode> getParts(String name) {
        return nodeManager.getNodes(name);
    }
    /**
     * Returns a verbose string with details of the tree's organisation, including location of parts, and interactionDocuments
     * @return
     */
    public String debugInfo()
    {

        nodeManager.debug();

        return rootNode.debugInfo();
    }

    /**
     * Returns a description of the model in SVPWrite format.
     * @return
     */
    public String toString()
    {
        return rootNode.toString();
    }


    public GRNTree duplicate() {
        return GRNTreeFactory.getGRNTree(this);
    }

    public boolean addInteraction(Interaction interaction) {
        return nodeManager.addInteraction(interaction);
    }

    // TODO This needs to be validated and optimised - is currently slow?

    // TODO Separate out validation from data structure? Create new classes for validation

    /**
     * Given a part p, returns allowed locations for p in the model, determined by ensuring required orthogonality as
     * specified by the INPUT and OUTPUTs of each node (i.e. p must be orthogonal to all other parts outside a given
     * node's interaction bounds).
     * @param p
     * @return
     * @throws Exception
     */
    public List<GRNTreeNode> getValidLocations(Part p) throws Exception
    {
       
        List<GRNTreeNode> validLocations = new ArrayList<>();
        List<GRNTreeNode> allTUs = rootNode.getAllDescendantTUs();
        LeafNode ln;
        
        switch (p.getType()) {
            case "Promoter":
                ln = LeafNode.getLeafNode(p, InterfaceType.INPUT);
                break;
            case "FunctionalPart":
                ln = LeafNode.getLeafNode(p, InterfaceType.OUTPUT);
                break;
            default:
                return allTUs;
        }

        for(GRNTreeNode t:allTUs){
            if(t.isValidLocation(ln)){
                validLocations.add(t);
            }
        }
        
        return validLocations;
    }

    // TODO This needs to be validated and optimised - is currently slow?

    /**
     * Given a node n, returns allowed locations for n in the model, determined by ensuring required orthogonality as
     * specified by the INPUT and OUTPUTs of each node (i.e. n must be orthogonal to all other nodes outside a given
     * node's interaction bounds).
     * @param n
     * @return
     * @throws Exception
     */
    public List<GRNTreeNode> getValidLocations(GRNTreeNode n) throws Exception
    {
       
        List<GRNTreeNode> validTranscriptionalUnits = new ArrayList<>();
        List<GRNTreeNode> allTranscriptionalUnits = new ArrayList<>();
        Iterator<GRNTreeNode> it = this.getBreadthFirstIterator();
        
        while(it.hasNext()){
            GRNTreeNode node = it.next();
            if(!(node instanceof LeafNode) && node.isTranscriptionUnit()){
                allTranscriptionalUnits.add(node);
            }
        }

        for(GRNTreeNode t:allTranscriptionalUnits){
            if(t.isValidLocation(n)){
                validTranscriptionalUnits.add(t);
            }
        }
        
        return validTranscriptionalUnits;
    }

    /**
     * Returns a PreOrder Iterator for traversing the tree. Descendants are prioritised over siblings (i.e. Depth First)
     * @return
     */
    public Iterator<GRNTreeNode> getPreOrderIterator()
    {
        return new PreOrderIterator(this);
    }

    /**
     * Returns a BreadthFirst Iterator for traversing the tree. Siblings are prioritised over descendants (i.e. Breadth
     * First)
     * @return
     */
    public Iterator<GRNTreeNode> getBreadthFirstIterator()
    {
        return new BreadthFirstIterator(this);
    }

    public FlowNavigator getFlowNavigator(GRNTreeNode startNode)
    {
        if(!(startNode instanceof LeafNode))
        {
            return null;
        }
        return new FlowNavigator(this, (LeafNode)startNode);
    }

    public FlowNavigator getFlowNavigator(String startNode)
    {
        return new FlowNavigator(this, startNode);
    }

    @Override
    public void attach(ModelObserver o) {
        nodeManager.attach(o);
    }

    @Override
    public void dettach(ModelObserver d) {
        nodeManager.dettach(d);
    }

    @Override
    public void alert() {
    }

    @Override
    public int hashCode() {
        return this.toString().hashCode();
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null)
            return false;
        if(obj == this)
            return true;
        if(!(obj instanceof GRNTree))
            return false;

        GRNTree other = (GRNTree) obj;
        if(!other.toString().equals(this.toString())) {
            return false;
        }

        return true;
    }
}
