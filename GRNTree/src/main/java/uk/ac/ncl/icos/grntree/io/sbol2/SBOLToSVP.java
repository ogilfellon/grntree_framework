/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.grntree.io.sbol2;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.xml.namespace.QName;
import org.sbolstandard.core2.Annotation;
import org.sbolstandard.core2.ComponentDefinition;
import org.sbolstandard.core2.FunctionalComponent;
import org.sbolstandard.core2.GenericTopLevel;
import org.sbolstandard.core2.ModuleDefinition;
import org.sbolstandard.core2.Participation;
import org.sbolstandard.core2.SBOLDocument;
import org.sbolstandard.core2.Sequence;
import uk.ac.ncl.intbio.virtualparts.entity.Interaction;
import uk.ac.ncl.intbio.virtualparts.entity.InteractionPartDetail;
import uk.ac.ncl.intbio.virtualparts.entity.Part;
import uk.ac.ncl.intbio.virtualparts.entity.Property;

/**
 *
 * @author goksel
 */
public class SBOLToSVP
{
     public static Part getSVP(ComponentDefinition componentDef, SBOLDocument document) throws SBOL2SerialisationException
    {
        Part part = new Part();
        try
        {
            part.setName(componentDef.getName());
            part.setDescription(componentDef.getDescription());
            part.setDisplayName(componentDef.getName());

            if (componentDef.getTypes().contains(Terms.componentType.dna))
            {
                part.setMetaType(Terms.vprMetaTypes.part);
                Sequence sequence = document.getSequence(componentDef.getSequenceURI());
                if (sequence != null)
                {
                    part.setSequence(getSVPNucleotides(sequence.getElements()));
                }
                part.setType(Mappings.fromSO(componentDef.getRoles()));
            }
            else if (componentDef.getTypes().contains(Terms.componentType.smallMolecule))
            {
                part.setMetaType(Terms.vprMetaTypes.smallMolecule);
            }
            else
            {
                throw new SBOL2SerialisationException("Only DNA and small molecule components can be processed. Component:" + componentDef.getIdentity());
            }
            
            
            document.getGenericTopLevels();

            addSVPProperties(part, componentDef.getAnnotations(), document);
        }
        catch (Exception exception)
        {
            throw new SBOL2SerialisationException("Could not create the Part object for the component " + componentDef.getIdentity(), exception);
        }
        return part;
    }

    private static String getSVPNucleotides(String sequence)
    {
        return "![CDATA[" + sequence + "]]";
    }

    public static List<Interaction> getInteractions(SBOLDocument document) throws SBOL2SerialisationException
    {
        List<Interaction> interactions = new ArrayList<Interaction>();
        for (ModuleDefinition moduleDef : document.getModuleDefinitions())
        {
            if (moduleDef.getInteractions() != null)
            {
                for (org.sbolstandard.core2.Interaction sbolInteraction : moduleDef.getInteractions())
                {
                    Interaction interaction = getInteraction(sbolInteraction, moduleDef, document);
                    interactions.add(interaction);
                }
            }
        }
        return interactions;
    }

    private static Interaction getInteraction(org.sbolstandard.core2.Interaction sbolInteraction, ModuleDefinition moduleDef, SBOLDocument document) throws SBOL2SerialisationException
    {
        Interaction interaction = new Interaction();
        try
        {
            interaction.setName(sbolInteraction.getDisplayId());
            interaction.setInteractionType(SBOLHelper.getLocalName(sbolInteraction.getTypes(), Terms.vpr));
            setInteractionProperties(interaction, sbolInteraction.getAnnotations());
            populateInteraction(interaction, document, moduleDef, sbolInteraction);
            return interaction;
        }
        catch (Exception exception)
        {
            throw new SBOL2SerialisationException(exception.getMessage() + " Interaction:" + sbolInteraction.getIdentity().toString(), exception);
        }
    }

    private static void populateInteraction(Interaction interaction, SBOLDocument document, ModuleDefinition moduleDef, org.sbolstandard.core2.Interaction sbolInteraction) throws SBOL2SerialisationException
    {
        Set<String> partNames = new HashSet<String>();
        for (Participation participation : sbolInteraction.getParticipations())
        {
            try
            {
                URI functionalComponentIdentity = participation.getParticipantURI();
                FunctionalComponent functionalComponent = moduleDef.getFunctionalComponent(functionalComponentIdentity);
                ComponentDefinition componentDef = document.getComponentDefinition(functionalComponent.getDefinitionURI());
                partNames.add(componentDef.getName());
                if (participation.getRoles() != null && participation.getRoles().size() > 0)
                {
                    InteractionPartDetail partDetail = new InteractionPartDetail();
                    partDetail.setPartName(componentDef.getName());
                    setInteractionDetailProperties(partDetail, participation.getAnnotations());
                    partDetail.setInteractionRole(SBOLHelper.getLocalName(participation.getRoles(), Terms.vpr));
                    interaction.AddPartDetail(partDetail);
                }
            }
            catch (Exception exception)
            {
                throw new SBOL2SerialisationException(exception.getMessage() + " Participation:" + participation.getIdentity().toString(), exception);
            }
        }

        for (String part : partNames)
        {
            interaction.AddPart(part);
        }
    }

    private static void addSVPProperties(Part part, List<Annotation> annotations, SBOLDocument document) throws SBOL2SerialisationException
    {
        for (Annotation annotation : annotations)
        {
           // NamedProperty<QName> property = annotation.getValue();

            QName property = annotation.getQName();
            
            try
            {
                if (property.equals(Terms.vprPartTerms.designMethod))
                {
                    part.setDesignMethod(annotation.getStringValue());
                }
                else if (property.equals(Terms.vprPartTerms.organism))
                {
                    part.setOrganism(annotation.getStringValue());
                }
                else if (property.equals(Terms.vprPartTerms.status))
                {
                    part.setStatus(annotation.getStringValue());
                }
                else if (property.equals(Terms.vprPartTerms.hasProperty))
                {
                    addSVPProperty(part, annotation, document);
                }
            }
            catch (Exception exception)
            {
                throw new SBOL2SerialisationException("Could not add the property to the part " + part.getName() + ". Property:" + property.toString(), exception);
            }

        }
    }

    private static void addSVPProperty(Part part, Annotation property, SBOLDocument document) throws SBOL2SerialisationException
    {
        Property partProperty = new Property();
        if (property.getQName().getNamespaceURI().equals(Terms.vpr.getNamespaceURI()))
        {
            String name = property.getQName().getLocalPart();
            String value = property.getStringValue();
            GenericTopLevel topLevel = document.getGenericTopLevel(Terms.toURI(property.getQName()));
            if (topLevel != null)
            {
                String annotationValue = SBOLHelper.getAnnotation(topLevel, Terms.rdfsTerms.comment);
                if (annotationValue != null)
                {
                    partProperty.setDescription(annotationValue);
                }
            }
        }
    }

    private static void setInteractionProperties(Interaction interaction, List<Annotation> annotations) throws SBOL2SerialisationException
    {
        for (Annotation annotation : annotations)
        {
            
            // OG: Fixed changes to libSBOLj
            
            //NamedProperty<QName> property = annotation.getValue();
            Annotation property=annotation;
            QName propertyName = annotation.getQName();

            if (propertyName.equals(Terms.documentedTerms.description))
            {
                interaction.setDescription(SBOLHelper.getLiteralValue(property).toString());
            }
            else if (propertyName.equals(Terms.vprInteractionTerms.freeTextMath))
            {
                interaction.setFreeTextMath(SBOLHelper.getLiteralValue(property).toString());
            }            
            else if (propertyName.equals(Terms.vprInteractionTerms.isInternal))
            {
                interaction.setIsInternal(Boolean.parseBoolean(SBOLHelper.getLiteralValue(property).toString()));
            }
            else if (property.getQName().equals(Terms.vprInteractionTerms.isReaction))
            {
                interaction.setIsReaction(Boolean.parseBoolean(SBOLHelper.getLiteralValue(property).toString()));
            }
            else if (propertyName.equals(Terms.vprInteractionTerms.isReversible))
            {
                interaction.setIsReversible(Boolean.parseBoolean(SBOLHelper.getLiteralValue(property).toString()));
            }
            else if (propertyName.equals(Terms.vprInteractionTerms.mathName))
            {
                interaction.setMathName(SBOLHelper.getLiteralValue(property).toString());
            }
            else if (propertyName.equals(Terms.vprInteractionTerms.hasParameter))
            {
                // How to retrieve a nested document from a String?
               
               // addParameter(interaction, (NestedDocument) property.getValue());
            }
        }
    }
    
    /*

    private static void addParameter(Interaction interaction, NestedDocument<QName> parameterDocument) throws SBOL2SerialisationException
    {
        
        List<NamedProperty<QName>> parameterProperties = parameterDocument.getProperties();
        Parameter parameter = new Parameter();

        for (NamedProperty<QName> property : parameterProperties)
        {
            if (property.getName().equals(Terms.vprInteractionTerms.parameterTerms.parameterType))
            {
                parameter.setParameterType(SBOLHelper.getLiteralValue(property).toString());
            }
            else if (property.getName().equals(Terms.vprInteractionTerms.parameterTerms.scope))
            {
                parameter.setScope(SBOLHelper.getLiteralValue(property).toString());
            }
            else if (property.getName().equals(Terms.vprInteractionTerms.parameterTerms.value))
            {
                parameter.setValue(Double.parseDouble(SBOLHelper.getLiteralValue(property).toString()));
            }
            else if (property.getName().equals(Terms.documentedTerms.name))
            {
                parameter.setName(SBOLHelper.getLiteralValue(property).toString());
            }
        }
        interaction.AddParameter(parameter);
    }*/

    private static void setInteractionDetailProperties(InteractionPartDetail partDetail, List<Annotation> annotations) throws SBOL2SerialisationException
    {
        for (Annotation annotation : annotations)
        {
            QName property = annotation.getQName();
            
           // NamedProperty<QName> property = annotation.getValue();
            try
            {
                if (property.equals(Terms.vprInteractionTerms.mathName))
                {
                    partDetail.setMathName(SBOLHelper.getLiteralValue(annotation).toString());
                }
                else if (property.equals(Terms.vprInteractionTerms.partForm))
                {
                    partDetail.setPartForm(SBOLHelper.getLiteralValue(annotation).toString());
                }
                else if (property.equals(Terms.vprInteractionTerms.numberOfMolecules))
                {
                    //partDetail.setNumberOfMolecules((int) getLiteralValue(property));
                    partDetail.setNumberOfMolecules(Integer.parseInt(SBOLHelper.getLiteralValue(annotation).toString()));//TODO Can't read it as an integer
                }
            }
            catch (Exception exception)
            {
                throw new SBOL2SerialisationException("Could not read the property " + property + " for the interaction details.", exception);
            }
        }
    }
}
