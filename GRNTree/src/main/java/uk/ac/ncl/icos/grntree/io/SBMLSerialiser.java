/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.grntree.io;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.stream.XMLStreamException;
import org.sbml.jsbml.SBMLDocument;
import uk.ac.ncl.icos.grntree.api.GRNTree;
import uk.ac.ncl.icos.grntree.api.NetworkSerialiser;
import uk.ac.ncl.icos.svpcompiler.Compiler.SBMLCompiler;

/**
 *
 * @author owengilfellon
 */
public class SBMLSerialiser implements NetworkSerialiser<SBMLDocument> {

    @Override
    public GRNTree importNetwork(SBMLDocument toImport) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public SBMLDocument exportNetwork(GRNTree toExport) {
        try {
            
            // TODO: Update with name
            
            SBMLCompiler compiler = new  SBMLCompiler("untitled");
            compiler.setParts(GRNCompilableFactory.getCompilables(toExport));
            compiler.compileAll();
            return compiler.getDocument();
            
        } catch (IOException | XMLStreamException ex) {
            Logger.getLogger(SBMLSerialiser.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(SBMLSerialiser.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
}
