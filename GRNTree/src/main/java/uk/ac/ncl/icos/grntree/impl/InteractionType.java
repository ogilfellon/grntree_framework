/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.grntree.impl;

/**
 *
 * @author owengilfellon
 */
public enum InteractionType {ACTIVATION, REPRESSION, PHOSPHORYLATION}
