package uk.ac.ncl.icos.grntree.tests;

import org.sbml.jsbml.SBMLDocument;
import uk.ac.ncl.icos.svpmanager.MolecularForm;
import uk.ac.ncl.icos.svpmanager.RegulationRole;
import uk.ac.ncl.icos.svpmanager.SVPManager;
import uk.ac.ncl.icos.grntree.api.SVPHelper;
import uk.ac.ncl.intbio.virtualparts.ModelBuilder;
import uk.ac.ncl.intbio.virtualparts.PartsHandler;
import uk.ac.ncl.intbio.virtualparts.SBML.SBMLHandler;
import uk.ac.ncl.intbio.virtualparts.entity.Interaction;
import uk.ac.ncl.intbio.virtualparts.entity.Part;

import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.ncl.icos.grntree.api.GRNTreeNode;
import uk.ac.ncl.icos.grntree.impl.PrototypeNode;

/**
 * Created by owengilfellon on 20/01/2015.
 */
public class ManualComposition {


    private PartsHandler p;
    private SBMLDocument container;
    private SBMLHandler sbmlhandler;
    private String              server = "http://localhost:8080";
    private SVPManager m = SVPManager.getSVPManager();
    private ModelBuilder mb;
    private SVPHelper helper = SVPHelper.getSVPHelper();

    public void run() throws Exception
    {
        p = new PartsHandler(server);
        container = new SBMLHandler().GetSBMLTemplateModel("SVP_Model");
        mb = new ModelBuilder(container);

        PrototypeNode a = (PrototypeNode)helper.getPromoter("A", 0.0269);
        PrototypeNode b = (PrototypeNode)helper.getOperator("B");
        PrototypeNode c = (PrototypeNode)helper.getRBS("C", 0.34255);
        PrototypeNode d = (PrototypeNode)helper.getCDS("D", 0.01412);

        PrototypeNode e = (PrototypeNode)helper.getPromoter("E", 0.0269);
        PrototypeNode f = (PrototypeNode)helper.getOperator("F");
        PrototypeNode g = (PrototypeNode)helper.getRBS("G", 0.34255);
        PrototypeNode h = (PrototypeNode)helper.getCDS("H", 0.01412);

        PrototypeNode i = (PrototypeNode)helper.getPromoter("I", 0.0269);
        PrototypeNode j = (PrototypeNode)helper.getOperator("J");
        PrototypeNode k = (PrototypeNode)helper.getRBS("K", 0.34255);
        PrototypeNode l = (PrototypeNode)helper.getCDS("L", 0.01412);

        Interaction dRepressesf = helper.getRegulationInteraction(   f, d,
                MolecularForm.DEFAULT,
                RegulationRole.REPRESSOR,
                0.3456);

        Interaction hRepressesj = helper.getRegulationInteraction(   j, h,
                MolecularForm.DEFAULT,
                RegulationRole.REPRESSOR,
                0.4567);

        Interaction lRepressesb = helper.getRegulationInteraction(   b, l,
                MolecularForm.DEFAULT,
                RegulationRole.REPRESSOR,
                0.3754);

        // =======================================================
        // Create SBML models from parts and internal interactions
        // =======================================================

        SBMLDocument a_doc = m.createPartDocument(a.getSVP(), a.getInternalEvents());
        SBMLDocument b_doc = m.createPartDocument(b.getSVP(), b.getInternalEvents());
        SBMLDocument c_doc = m.createPartDocument(c.getSVP(), c.getInternalEvents());
        SBMLDocument d_doc = m.createPartDocument(d.getSVP(), d.getInternalEvents());
        SBMLDocument e_doc = m.createPartDocument(e.getSVP(), e.getInternalEvents());
        SBMLDocument f_doc = m.createPartDocument(f.getSVP(), f.getInternalEvents());
        SBMLDocument g_doc = m.createPartDocument(g.getSVP(), g.getInternalEvents());
        SBMLDocument h_doc = m.createPartDocument(h.getSVP(), h.getInternalEvents());
        SBMLDocument i_doc = m.createPartDocument(i.getSVP(), i.getInternalEvents());
        SBMLDocument j_doc = m.createPartDocument(j.getSVP(), j.getInternalEvents());
        SBMLDocument k_doc = m.createPartDocument(k.getSVP(), k.getInternalEvents());
        SBMLDocument l_doc = m.createPartDocument(l.getSVP(), l.getInternalEvents());

        // =======================================================
        // Create Lists of Parts for each Interaction
        // =======================================================

        List<Part> dTofParts = new ArrayList<Part>();
        dTofParts.add(d.getSVP());
        dTofParts.add(f.getSVP());

        List<Part> hTojParts = new ArrayList<Part>();
        hTojParts.add(h.getSVP());
        hTojParts.add(j.getSVP());

        List<Part> lTobParts = new ArrayList<Part>();
        lTobParts.add(l.getSVP());
        lTobParts.add(b.getSVP());

        // =======================================================
        // Create SBML Models for Interactions
        // (JParts attempts to retrieve non-existent parts)
        // TODO Update JParts to use above lists of parts
        // =======================================================



        SBMLDocument dTof = m.createInteractionDocument(dRepressesf, dTofParts);
        SBMLDocument hToj = m.createInteractionDocument(hRepressesj, hTojParts);
        SBMLDocument lTob = m.createInteractionDocument(lRepressesb, lTobParts);

/*
        SBMLDocument dTof = p.CreateInteractionModel(dRepressesf);
        SBMLDocument hToj = p.CreateInteractionModel(hRepressesj);
        SBMLDocument lTob = p.CreateInteractionModel(lRepressesb);
*/

        SBMLDocument mrna1 = m.getPartDocument(m.getPart("mRNA"));
        SBMLDocument mrna2 = m.getPartDocument(m.getPart("mRNA"));
        SBMLDocument mrna3 = m.getPartDocument(m.getPart("mRNA"));
   /**/
        // =======================================================
        // Compose Part and Interaction Models into a
        // simulateable model
        // TODO update compiler to automate this process
        // TODO can abstract parts and concrete parts be mixed?
        // =======================================================

        mb.Add(a_doc);
        mb.Link(a_doc, b_doc);
        mb.Add(b_doc);
        mb.Link(b_doc, lTob);
        mb.Add(lTob);
        mb.Add(mrna1);
        mb.Link(lTob, mrna1);
        mb.Link(mrna1, c_doc);
        mb.Add(c_doc);
        mb.Link(c_doc, d_doc);
        mb.Add(d_doc);

        mb.Add(e_doc);
        mb.Link(e_doc, f_doc);
        mb.Add(f_doc);
        mb.Link(f_doc, dTof);
        mb.Add(dTof);
        mb.Add(mrna2);
        mb.Link(dTof, mrna2);
        mb.Link(mrna2, g_doc);
        mb.Add(g_doc);
        mb.Link(g_doc, h_doc);
        mb.Add(h_doc);

        mb.Add(i_doc);
        mb.Link(i_doc, j_doc);
        mb.Add(j_doc);
        mb.Link(j_doc, hToj);
        mb.Add(hToj);
        mb.Add(mrna3);
        mb.Link(hToj, mrna3);
        mb.Link(mrna3, k_doc);
        mb.Add(k_doc);
        mb.Link(k_doc, l_doc);
        mb.Add(l_doc);/**/

        // =======================================================
        // Write compiled model to file
        // =======================================================

        try
        {



            FileWriter writer = new FileWriter("manual_composition" + ".xml");

            writer.write(mb.GetModelString());
            writer.flush();
            System.out.println("\nModel written to " + "newoutput" + ".xml");
        }
        catch(Exception ex)
        {
            System.out.println(ex.getMessage());
        }

    }

    public static void main(String[] args)
    {
        try {
            new ManualComposition().run();
        } catch (Exception ex) {
            Logger.getLogger(ManualComposition.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
