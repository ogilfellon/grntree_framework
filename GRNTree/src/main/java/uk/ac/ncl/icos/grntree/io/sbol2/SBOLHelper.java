/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.grntree.io.sbol2;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.namespace.QName;
import org.sbolstandard.core2.Annotation;
import org.sbolstandard.core2.Component;
import org.sbolstandard.core2.ComponentDefinition;
import org.sbolstandard.core2.ComponentInstance;
import org.sbolstandard.core2.FunctionalComponent;
import org.sbolstandard.core2.Identified;
import org.sbolstandard.core2.ModuleDefinition;
import org.sbolstandard.core2.Range;
import org.sbolstandard.core2.SBOLDocument;
import org.sbolstandard.core2.Sequence;
import org.sbolstandard.core2.SequenceAnnotation;
import org.sbolstandard.core2.SequenceConstraint;
import uk.ac.ncl.icos.grntree.api.GRNTreeNode;
import static uk.ac.ncl.intbio.core.datatree.Datatree.NamedProperty;
import uk.ac.ncl.intbio.core.datatree.Literal;
import uk.ac.ncl.intbio.core.datatree.NamedProperty;
import uk.ac.ncl.intbio.core.datatree.NamespaceBinding;

/**
 *
 * @author goksel
 */
public class SBOLHelper
{

    public static List<Component> getOrderedComponents(ComponentDefinition componentDef)
    {
        Set<Component> componentList = componentDef.getComponents();
        Component[] components = componentList.toArray(new Component[componentList.size()]);
        sort(components, 0, components.length - 1, componentDef);
        return new ArrayList<>(Arrays.asList(components));
    }

    //See more at: http://www.java2novice.com/java-sorting-algorithms/quick-sort/#sthash.6zVoEsaf.dpuf
    private static void sort(Component[] components, int lowerIndex, int higherIndex, ComponentDefinition componentDef)
    {

        int i = lowerIndex;
        int j = higherIndex;
        // calculate pivot number, I am taking pivot as middle index number
        Component pivot = components[lowerIndex + (higherIndex - lowerIndex) / 2];
        // Divide into two arrays
        while (i <= j)
        {
            /**
             * In each iteration, we will identify a number from left side which
             * is greater then the pivot value, and also we will identify a
             * number from right side which is less then the pivot value. Once
             * the search is done, then we exchange both numbers.
             */
            while (!components[i].getIdentity().toString().equals(pivot.getIdentity().toString()) && precedes(components[i], pivot, componentDef))
            {
                i++;
            }
            while (!components[j].getIdentity().toString().equals(pivot.getIdentity().toString()) && !precedes(components[j], pivot, componentDef))
            {
                j--;
            }
            if (i <= j)
            {
                exchangeComponents(components, i, j);
                //move index to next position on both sides
                i++;
                j--;
            }
        }
        // call sort() method recursively
        if (lowerIndex < j)
        {
            sort(components, lowerIndex, j, componentDef);
        }
        if (i < higherIndex)
        {
            sort(components, i, higherIndex, componentDef);
        }
    }

    private static void exchangeComponents(Component[] components, int i, int j)
    {
        Component temp = components[i];
        components[i] = components[j];
        components[j] = temp;
    }

    public static boolean precedes(Component component1, Component component2, ComponentDefinition componentDef)
    {
        Set<SequenceAnnotation> annotations = componentDef.getSequenceAnnotations();
        SequenceAnnotation annotation1 = getSequenceAnnotation(component1, new ArrayList<>(annotations));
        SequenceAnnotation annotation2 = getSequenceAnnotation(component2, new ArrayList<>(annotations));
        boolean precedes = true;
        boolean found = false;
        
        // If there are sequence annotations, compare their ranges
        /*
        if (annotation1 != null && annotation2 != null)
        {
            if (annotation1.getLocation() != null && annotation2.getLocation() != null)
            {
                if ((annotation1.getLocation() instanceof Range) && (annotation2.getLocation() instanceof Range))
                {
                    Range range1 = (Range) annotation1.getLocation();
                    Range range2 = (Range) annotation2.getLocation();
                    precedes = (range1.getStart() < range2.getStart());
                    found = true;
                }
            }
        }*/
        if (!found)
        {
            precedes = precedesByConstraint(component1, component2, componentDef);
           // System.out.println(component1.getDisplayId() + " precedes " + component2.getDisplayId() + " = " + precedes);
            
        }
       
        return precedes;
    }

    public static SequenceAnnotation getSequenceAnnotation(Component component, List<SequenceAnnotation> annotations)
    {
        for (SequenceAnnotation sequenceAnnotation : annotations)
        {
            if (sequenceAnnotation.getComponent().getIdentity().equals(component.getIdentity()))
            {
                return sequenceAnnotation;
            }
        }
        return null;
    }
    
    // OG: 14/04/15: Previously, only correylu identified precedes if component1
    // immediately preceded component2. Now, identifies correctly.

    private static boolean precedesByConstraint(Component component1, Component component2, ComponentDefinition componentDef)
    {
        Set<SequenceConstraint> constraints = componentDef.getSequenceConstraints();
        Map<String, String> constraintMap = new HashMap<>();

        for (SequenceConstraint constraint : constraints) {
            if (precedes(component1, component2, constraint)) {
                return true;
            }
            
            constraintMap.put(constraint.getSubjectURI().toString(), constraint.getObjectURI().toString());
        }
        
        // If component1 has no constraint where it is the object, it is first
        
        if(!constraintMap.containsValue(component1.getIdentity().toString())
                && constraintMap.containsKey(component1.getIdentity().toString())) {
            
            return true;
        } else {
            
            // Starting from component1, follow chain of subject -> object derived from all constraints
            
            String next = component1.getIdentity().toString();
            
            while(next != null) {
                if(constraintMap.containsKey(next)) {
                    next = constraintMap.get(next);

                    if(next.equals(component2.getIdentity().toString())) {
                        return true;
                    }
                } else {
                    next = null;
                }
            }
        }
        
        System.out.println(false);
        
        return false;
    }

    private static boolean precedes(Component component1, Component component2, SequenceConstraint constraint)
    {
        
        if (constraint.getSubject().equals(component1.getIdentity()) && constraint.getObject().equals(component2.getIdentity())
                && constraint.getRestriction().equals(SequenceConstraint.RestrictionType.PRECEDES))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static ModuleDefinition getModuleDefinition(SBOLDocument document, ComponentDefinition componentDef)
    {
        for (ModuleDefinition moduleDef : document.getModuleDefinitions())
        {
            for (FunctionalComponent functionalComp : moduleDef.getFunctionalComponents())
            {
                if (functionalComp.getDefinition().equals(componentDef.getIdentity()))
                {
                    return moduleDef;
                }
            }
        }
        return null;
    }
/*
    public static NamedProperty<QName> getAnnotation(Identified identified, QName qname)
    {
        if (identified != null && identified.getAnnotations() != null)
        {
            for (Annotation annotation : identified.getAnnotations())
            {
                /*
                
                if (annotation.getValue().getName().equals(qname))
                {
                    return annotation.getValue();
                }
                
                */
                
                // OG: Updating libSBOLj
                /*
                if (annotation.getQName().equals(qname)) {
                    return annotation.getStringValue();
                }
            }
        }
        return null;
    }*/
    
    public static String getAnnotation(Identified identified, QName qname)
    {
        if (identified != null && identified.getAnnotations() != null)
        {
            for (Annotation annotation : identified.getAnnotations())
            {
                /*
                
                if (annotation.getValue().getName().equals(qname))
                {
                    return annotation.getValue();
                }
                
                */
                
                // OG: Updating libSBOLj
                
                if (annotation.getQName().equals(qname)) {
                    return annotation.getStringValue();
                }
            }
        }
        return null;
    }

    // OG: Updated due to libSBOLj changes

    public static Object getLiteralValue(Annotation property) throws SBOL2SerialisationException
    {
        
        Object value = null;
        
        if(isInteger(property.getStringValue()))
        {
            value =  Integer.parseInt(property.getStringValue());
            //value = ((Literal.StringLiteral<QName>) property.getValue()).getValue();
        } else {
            value = property.getStringValue();
        }
        /*
        if (property.getValue() instanceof Literal.StringLiteral)
        {
            value = ((Literal.StringLiteral<QName>) property.getValue()).getValue();
        }
        else if (property.getValue() instanceof Literal.IntegerLiteral)
        {
            value = ((Literal.IntegerLiteral<QName>) property.getValue()).getValue();
        }
        else
        {
            throw new SBOL2SerialisationException("Only String and Integer literals can be processed");
        }*/
        return value;
    }
    
    // OG: Added method for testing type of annotations
    
    public static boolean isInteger(String s) {
        
        // remove whitespace
        
        Scanner sc = new Scanner(s.trim());
        if(!sc.hasNextInt()) return false;
        sc.nextInt();
        
        // if begins with int, confirm nothing follows it
        
        return !sc.hasNext();
    }
        
        

    public static String getNameFromURI(URI uri, char nameDelimiter) throws SBOL2SerialisationException
    {
        String uriString = uri.toString();
        int index = uriString.lastIndexOf(nameDelimiter);
        if (index > 0 && index < uriString.length() - 1)
        {
            String name = uriString.substring(index + 1);
            return name;
        }
        else
        {
            throw new SBOL2SerialisationException("Can't parse the name in URI " + uri.toString());
        }
    }

    /**
     * Returns the local name of the type
     *
     * @param types
     * @param namespace
     * @return
     */
    public static Set<String> getLocalNames(Set<URI> types, NamespaceBinding namespace)
    {
        Set<String> names = new HashSet<String>();
        if (types != null && types.size() > 0)
        {
            for (URI uri : types)
            {
                if (uri.toString().startsWith(namespace.getNamespaceURI()))
                {
                    String name = uri.toString().replace(namespace.getNamespaceURI(), "");
                    names.add(name);
                }
            }
        }
        return names;
    }

    public static String getLocalName(Set<URI> uris, NamespaceBinding preferredNamespace)
    {
        if (uris != null && uris.size() > 0)
        {
            Set<String> localnames = SBOLHelper.getLocalNames(uris, preferredNamespace);
            if (localnames != null && localnames.size() > 0)
            {
                return localnames.iterator().next();
            }
            else
            {
                return uris.iterator().next().toString();
            }
        }
        return null;
    }
   
    
    
    public static boolean isRoot(ComponentDefinition componentDef)
    {
        boolean isRoot = false;
        
        List<Annotation> as = componentDef.getAnnotations();
        for(Annotation a:as )  
        {
  
            /*
                
            try {
                if(getLiteralValue(a.getValue()).equals("true") &&
                        a.getValue().getName().getLocalPart().equals("isRoot")) {
                        isRoot = true;
                    }
                
                }   catch (SBOL2SerialisationException ex) {
                    Logger.getLogger(SBOLHelper.class.getName()).log(Level.SEVERE, null, ex);
                    return isRoot;
                }
             }*/ 
            
            // OG: Fixed changes to libSBOLj
                
            if(a.getStringValue().equals("true") &&
                    a.getQName().getLocalPart().equals("isRoot")) {
                    isRoot = true;
                }
            }
        
        
        return isRoot;
    }

    public static boolean isLeaf(ComponentDefinition componentDef)
    {

        boolean isLeaf = true;
                
        if ((componentDef.getComponents() != null && componentDef.getComponents().size() > 0)
                || (componentDef.getSequenceConstraints() != null) && (componentDef.getSequenceConstraints().size()>0))
        {
            isLeaf = false;
        }

        return isLeaf;
    }

    public static SequenceAnnotation createSequenceAnnotation(ComponentDefinition componentDef, Component childComponent, int rangeIndex, int start, int end)
    {
        
       
        //SequenceAnnotation annotation = componentDef.createSequenceAnnotation(URIFactory.getSequenceAnnotationURI(componentDef, String.valueOf(rangeIndex)).toString());
        SequenceAnnotation annotation = componentDef.createSequenceAnnotation(
                componentDef.getDisplayId() + "annotation" +  String.valueOf(rangeIndex), start, end); 
        
        //annotation.addRange(start, end);
        //annotation.setComponent(null);
      /*
        Component childComponent = componentDef.createComponent(
                componentDef.getDisplayId() + "child" + String.valueOf(rangeIndex),
                //URIFactory.getComponentURI(componentDef, .toString(),
                ComponentInstance.AccessType.PUBLIC,
                childComponentDef.getIdentity());*/

        annotation.setComponent(childComponent.getIdentity());
        //annotation.setComponent(childComponent.getDisplayId());
                //componentDef.getDisplayId() + "child" + String.valueOf(rangeIndex));
        
        return annotation;
    }
    
    public static SequenceConstraint createSequenceConstraint(ComponentDefinition componentDef, Component subject, Component object, int rangeIndex)
    {
        SequenceConstraint constraint = componentDef.createSequenceConstraint(
                "constraint_" + (rangeIndex - 1),
                SequenceConstraint.RestrictionType.PRECEDES,
                subject.getIdentity(),
                object.getIdentity()
        );

        return constraint;
    }


    public static Sequence createNucleicAcidSequence(SBOLDocument document, ComponentDefinition componentDef, String version, String nucleotides)
    {
        Sequence sequence = null;

        if (nucleotides != null)
        {
            if (nucleotides.startsWith("!"))
            {
                int start = "![CDATA[".length();
                int end = nucleotides.length() - "]]".length();
                nucleotides = nucleotides.substring(start, end);
            }
            URI sequenceURI = URIFactory.getSequenceURI(componentDef);
            
            sequence = document.createSequence(
                    SBOLHelper.getValidDisplayId(componentDef.getDisplayId()),
                    version,
                    nucleotides.replace(System.getProperty("line.separator"), ""),
                    Terms.sequenceEncoding.nucleicAcid);
            if (sequence == null)
            {
                sequence = document.getSequence(sequenceURI);
            }
            
            componentDef.setSequence(sequence.getIdentity());

        }
        return sequence;
    }

    public static void addAnnotation(Identified identified, QName property, String value)
    {
        if (value != null && value.length() > 0)
        {
            //identified.createAnnotation(NamedProperty(property, value));
            // OG: Updating libSBOLj
            identified.createAnnotation(property, value);
        }
    }

    public static void addAnnotation(Identified identified, QName property, int value)
    {
        //identified.createAnnotation(NamedProperty(property, value));
        // OG: Updating libSBOLj
        identified.createAnnotation(property, String.valueOf(value));
    }

    public static String getValidDisplayId(String displayId)
    {
        return displayId;
        //return displayId.replaceAll("[^A-Za-z0-9]", "");
    }
    
    public static ComponentDefinition getComponetDefinition(SBOLDocument document, GRNTreeNode node) {
        
        Set<ComponentDefinition> definitions = document.getComponentDefinitions();
        for(ComponentDefinition definition : definitions) {
            if(definition.getDisplayId().equals(SBOLHelper.getValidDisplayId(node.getId()))) {
                return definition;
            }
        }
        return null;
    }
}
