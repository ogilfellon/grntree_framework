package uk.ac.ncl.icos.grntree.api;

import uk.ac.ncl.icos.grntree.impl.*;
import uk.ac.ncl.intbio.virtualparts.entity.Interaction;
import uk.ac.ncl.intbio.virtualparts.entity.Part;

import java.util.List;

/**
 * Created by owengilfellon on 10/03/2015.
 */
public class GRNTreeNodeFactory {

    public static GRNTreeNode getBranchNode(InterfaceType type)
    {
        return BranchNode.getBranchNode(type);
    }

    public static GRNTreeNode getBranchNode(InterfaceType interfaceType, List<GRNTreeNode> nodes)
    {
        return BranchNode.getBranchNode(interfaceType, nodes);       
    }

    public static GRNTreeNode getLeafNode(Part svp, InterfaceType interfaceType)
    {
        return LeafNode.getLeafNode(svp, interfaceType);
    }

    public static GRNTreeNode getLeafNode(Part svp, List<Interaction> internalEvents, InterfaceType interfaceType)
    {
        return LeafNode.getLeafNode(svp, internalEvents, interfaceType);
    }
}
