/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.grntree.api;


import uk.ac.ncl.icos.svpcompiler.parsing.SVPParser;
import uk.ac.ncl.icos.svpmanager.SVPManager;
import uk.ac.ncl.icos.grntree.impl.BranchNode;
import uk.ac.ncl.icos.grntree.impl.ConcreteNode;
import uk.ac.ncl.icos.grntree.impl.InterfaceType;
import uk.ac.ncl.icos.grntree.io.SVPWriteNetworkSerialiser;
import uk.ac.ncl.intbio.virtualparts.entity.Part;
import uk.ac.ncl.intbio.virtualparts.entity.Property;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.ncl.icos.grntree.impl.LeafNode;


/**
 * A Factory Class for constructing instances of GRNTree.
 *
 * @author owengilfellon
 * @see GRNTree
 */
public class GRNTreeFactory {
    
    private static SVPManager m = SVPManager.getSVPManager();
    
    /**
     * Parses an SVPWrite string and returns a GRNTree. The model is broken up at Terminators into Transcription Units.
     * If there is more than one Transcription Unit, each Transcription Unit is placed within a root node, each with
     * the InterfaceType: NONE. This allows all Transcription Units to pass signals without restriction.
     *
     * @param svpWrite An SVPWrite description of the model
     * @return A GRNTree
     */
    public static GRNTree getGRNTree(String svpWrite)
    {   
        BranchNode root = BranchNode.getBranchNode(InterfaceType.NONE);
        root.setName("RootNode");
        GRNTree tree = new GRNTree(root);

        if(svpWrite.equals("")) {
            return tree;
        }

        List<GRNTreeNode> currentTU = new ArrayList<>();
        Part prevPart = null;
        String name = null;

        for(String s:SVPParser.getParts(svpWrite))
        {
            Part p = m.getPart(s);
            InterfaceType i = InterfaceType.NONE;
                 
            switch (p.getType()) {
                
                case "Promoter":
                    name = "";
                    for(Property prop:p.getProperties()) {
                        i = InterfaceType.INPUT;
                        /*
                        if(prop.getName().equals("type") &&
                                
                                // TODO Change these to be more inclusive
                                
                                (prop.getValue().equals("BO_InduciblePromoter") ||
                                prop.getValue().equals("BO_RepressiblePromoter"))) {
                            
                        }*/
                    }   currentTU.add(LeafNode.getLeafNode(p, i));
                    break;
                    
                case "Operator":
                    i = InterfaceType.INPUT;
                    currentTU.add(LeafNode.getLeafNode(p, i));
                    break;
                    
                case "RBS":
                    currentTU.add(LeafNode.getLeafNode(p, i));
                break;
                    
                case "FunctionalPart":
                    i= InterfaceType.OUTPUT;
                    name += p.getName();
                    currentTU.add(LeafNode.getLeafNode(p, i));
                    break;
                    
                case "Terminator":
                    currentTU.add(LeafNode.getLeafNode(p, i));
                    GRNTreeNode n = BranchNode.getBranchNode(InterfaceType.NONE, currentTU);
                    n.setName(name);
                    try {
                        root.addNode(n);
                    } catch (Exception ex) {
                        Logger.getLogger(GRNTreeFactory.class.getName()).log(Level.SEVERE, null, ex);
                    }   // Clear all placeholders to begin next transcription unit
                    
                    currentTU = new ArrayList<>();
                    prevPart = null;
                    p = null;
                    break;
            }

            prevPart = p;

        }

        return tree;
    }

    /**
     * Returns a copy of a GRNTree such that provided tree != returned tree.
     * @param tree A GRNTree instance to duplicate
     * @return An identical GRNTree
     */
    public static GRNTree getGRNTree(GRNTree tree)
    {
        NetworkSerialiser<String> exporter = new SVPWriteNetworkSerialiser();
        return getGRNTree(exporter.exportNetwork(tree));
    }

    public static GRNTree getGRNTree()
    {
        BranchNode rootNode = BranchNode.getBranchNode(InterfaceType.NONE);
        return new GRNTree(rootNode);
    }
}
