package uk.ac.ncl.icos.grntree.impl;

import uk.ac.ncl.icos.grntree.api.GRNTreeNode;
import uk.ac.ncl.intbio.virtualparts.SBML.SBMLHandler;
import uk.ac.ncl.intbio.virtualparts.entity.Part;

import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import uk.ac.ncl.intbio.virtualparts.entity.Interaction;

/**
 * Created by owengilfellon on 19/01/2015.
 */
final public class ConcreteNode extends LeafNode {

    protected ConcreteNode(Part svp, InterfaceType interfaceType)
    {
        super(svp, interfaceType);
        setSVP(svp);
        setName(svp.getName());
        document =  m.getPartDocument(svp);
    }



    public void setSVP(Part svp) {
        super.setSVP(svp, m.getInternalEvents(svp)); //To change body of generated methods, choose Tools | Templates.
    }
  
    public List<Interaction> getInternalEvents()
    {
        List<Interaction> interactions = m.getInternalEvents(svp);

        if(interactions != null) {
            return interactions;
        } else {
            return new ArrayList<>();
        }
    }
    

    @Override
    public GRNTreeNode duplicate()
    {
        return new ConcreteNode(svp, interfaceType);
    }

    private void writeObject(ObjectOutputStream out) throws Exception
    {
        out.defaultWriteObject();
        SBMLHandler handler = new SBMLHandler();
        if(hasDocument) {
            String sbml = handler.GetSBML(this.document);
            out.writeObject(sbml);
        }
    }

    private void readObject(ObjectInputStream in) throws IOException, XMLStreamException, ClassNotFoundException
    {
        in.defaultReadObject();
        SBMLHandler handler = new SBMLHandler();
        if(hasDocument) {
            String sbml = (String) in.readObject();
            this.document = handler.GetSBML(sbml);
        }
    }
}
