/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.grntree.io.sbol2;
import java.net.URI;
import uk.ac.ncl.intbio.core.datatree.NamespaceBinding;

import javax.xml.namespace.QName;

import static uk.ac.ncl.intbio.core.datatree.Datatree.NamespaceBinding;

/**
 *
 * @author goksel
 */
public class Terms {
    public static final NamespaceBinding vpr = NamespaceBinding("http://virtualparts.org#", "vpr");
    public static final NamespaceBinding dcterms = NamespaceBinding("http://purl.org/dc/terms/", "dcterms");
    public static final NamespaceBinding biopax = NamespaceBinding("http://www.biopax.org/release/biopax-level3.owl#", "biopax");
    public static final NamespaceBinding so = NamespaceBinding("http://identifiers.org/so/", "so");
    public static final NamespaceBinding synbad = NamespaceBinding("http://www.synbad.org#", "synbad");
    public static final NamespaceBinding rdf = NamespaceBinding("http://www.w3.org/1999/02/22-rdf-syntax-ns#", "rdf");
    public static final NamespaceBinding rdfs = NamespaceBinding("http://www.w3.org/2000/01/rdf-schema#", "rdfs");
    
    public static final class componentType
    {
        public static final URI dna = toURI(biopax.withLocalPart("DnaRegion"));
        public static final URI smallMolecule = toURI(biopax.withLocalPart("SmallMolecule"));   
    }
    
    public static final class componentRole
    {
         public static final URI generic = toURI(so.withLocalPart("SO:0000110"));
         public static final URI promoter = toURI(so.withLocalPart("SO:0000167"));
         public static final URI rbs = toURI(so.withLocalPart("SO:0000139"));
         public static final URI cds = toURI(so.withLocalPart("SO:0000316"));
         public static final URI terminator = toURI(so.withLocalPart("SO:0000141"));
         public static final URI operator = toURI(so.withLocalPart("SO:0000057"));
         public static final URI shim = toURI(so.withLocalPart("SO:0000997"));
         public static final URI mrna = toURI(so.withLocalPart("SO:0000234"));
         public static final URI rna = toURI(so.withLocalPart("SO:0000356"));         
    }
    
    public static final class rdfTerms {
        public static final QName Property = rdf.withLocalPart("Property");
    }
    
     public static final class rdfsTerms {
        public static final QName label = rdfs.withLocalPart("label");
        public static final QName comment = rdfs.withLocalPart("comment");
        
    }
    
    public static final class sequenceEncoding
    {
       public static final URI nucleicAcid = URI.create("http://www.chem.qmul.ac.uk/iubmb/misc/naseq.html");
    }
    
    public static final class documentedTerms {
        public static final QName name = dcterms.withLocalPart("title");
        public static final QName description = dcterms.withLocalPart("description");
    }
    
    public static final class vprInteractionTerms {
        public static final QName freeTextMath = vpr.withLocalPart("freeTextMath");
        public static final QName isReaction = vpr.withLocalPart("isReaction");
        public static final QName isReversible = vpr.withLocalPart("isReversible");
        public static final QName mathName = vpr.withLocalPart("mathName");
        public static final QName numberOfMolecules = vpr.withLocalPart("stoichiometry");
        public static final QName partForm = vpr.withLocalPart("partForm");
        public static final QName isInternal = vpr.withLocalPart("isInternal");
        public static final QName hasParameter = vpr.withLocalPart("parameter");
        public static final class parameterTerms
        {
            public static final QName Parameter = vpr.withLocalPart("Parameter");
            public static final QName scope = vpr.withLocalPart("scope");
            public static final QName value = vpr.withLocalPart("value");
            public static final QName parameterType = vpr.withLocalPart("parameterType");            
        }
    }
    
    public static final class vprPartTerms {    
        public static final QName designMethod = vpr.withLocalPart("designMethod");
        public static final QName status = vpr.withLocalPart("status");
        public static final QName organism = vpr.withLocalPart("organism");
        public static final QName value = vpr.withLocalPart("value");
        public static final QName hasProperty = vpr.withLocalPart("property");
        public static final QName property = vpr.withLocalPart("Property");        
    }

    public static final class vprTypes {
        public static final String promoter="Promoter";
        public static final String functionalPart="FunctionalPart";
        public static final String rbs="RBS";
        public static final String terminator="Terminator";
        public static final String operator="Operator";
        public static final String shim="Shim";
        public static final String mrna="mRNA";
        public static final String rna="RNA";        
    }
    
    public static final class vprMetaTypes {
        public static final String part="Part";
        public static final String smallMolecule="SmallMolecule";           
    }
    
    public static final class synbadTerms {
        public static final QName isRoot = synbad.withLocalPart("isRoot");   
        public static final QName isTranscriptionUnit = synbad.withLocalPart("isTranscriptionUnit");   
        public static final QName isPrototype = synbad.withLocalPart("isPrototype");   
        public static final QName isRegulatedTranscriptionUnit = synbad.withLocalPart("isRegulatedTranscriptionUnit");   
    }
     
    public static URI toURI(QName qname)
    {
        return URI.create(qname.getNamespaceURI() + qname.getLocalPart());
    }
    
}