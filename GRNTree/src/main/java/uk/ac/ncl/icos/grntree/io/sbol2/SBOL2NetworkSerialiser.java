/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.grntree.io.sbol2;

import java.net.URI;
import org.sbml.jsbml.SBMLDocument;
import org.sbolstandard.core2.SBOLDocument;
import org.sbolstandard.core2.SBOLReader;
import org.sbolstandard.core2.SBOLWriter;
import uk.ac.ncl.icos.grntree.api.GRNTree;
import uk.ac.ncl.icos.grntree.api.GRNTreeFactory;
import uk.ac.ncl.icos.grntree.api.NetworkSerialiser;
import uk.ac.ncl.icos.grntree.io.SBMLSerialiser;

/**
 *
 * @author goksel
 */
public class SBOL2NetworkSerialiser implements NetworkSerialiser<SBOLDocument>  {

    
     
    @Override
    public GRNTree importNetwork(SBOLDocument toImport){
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        Importer importer=new Importer();
         GRNTree tree=null;
        try
        {
            tree=importer.convert(toImport);
        }
        catch (Exception e)
        {
            e.printStackTrace(System.out);
            String str="";
            str="";
        }
        //GRNTree tree=GRNTreeFactory.getGRNTree();
        //tree.getRootNode().addNode(null)
        //GRNTreeNodeFactory.        
        return tree;
    }

    @Override
    public SBOLDocument exportNetwork(GRNTree tree)  {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
       
        SBOLDocument document=new SBOLDocument();
         try
        {
            
          
        document.addNamespace(URI.create(Terms.vpr.getNamespaceURI()), Terms.vpr.getPrefix());
        document.addNamespace(URI.create(Terms.dcterms.getNamespaceURI()), Terms.dcterms.getPrefix());
        document.addNamespace(URI.create(Terms.synbad.getNamespaceURI()), Terms.synbad.getPrefix());
        document.addNamespace(URI.create(Terms.rdfs.getNamespaceURI()), Terms.rdfs.getPrefix());
        
        
        String baseURL="http://www.synbad.org";//TODO: What is the base URL
        String version="1.0";
        Exporter exporter=new Exporter();
        document=exporter.export(tree, document, baseURL, version);      
        }
         catch (Exception ex)
         {
         ex.printStackTrace();
         }
        return document;
    }
    
    public static void main(String[] args) throws Exception,Throwable
    {
        SBOLDocument document=null;
        SBOL2NetworkSerialiser serialiser=new SBOL2NetworkSerialiser();
        System.out.println("Starting---");
        GRNTree tree = GRNTreeFactory.getGRNTree("PspaRK:Prom; RBS_SpaK:RBS; SpaK:CDS; RBS_SpaR:RBS; SpaR:CDS; BO_4296:Ter; PspaS:Prom; RBS_SpaS:RBS; GFP_rrnb:CDS; BO_4296:Ter");
        tree.setDetectInteractions(true);
        /* 
        GRNTree tree = GRNTreeFactory.getGRNTree();
        GRNTreeNode node = GRNTreeNodeFactory.getBranchNode(InterfaceType.NONE);
        GRNTreeNode promoter = SVPHelper.getSVPHelper().getPromoter("Promoter", 0.5);
        node.addNode(promoter);    
        node.addNode(SVPHelper.getSVPHelper().getRBS("RBS", 0.5));
        GRNTreeNode cds = SVPHelper.getSVPHelper().getCDS("CDS", 0.25);
        node.addNode(cds);
        Interaction interaction = SVPHelper.getSVPHelper().getRegulationInteraction(promoter, cds, MolecularForm.DEFAULT, RegulationRole.ACTIVATOR, 0.2);
        tree.getRootNode().addNode(node);
        tree.addInteraction(interaction);
       */
        
        /**/
   
        /*
            GRNTreeNode node = GRNTreeNodeFactory.getBranchNode(InterfaceType.NONE);
            node.addNode(SVPHelper.getSVPHelper().getPromoter("Promoter", 0.5));
            node.addNode(SVPHelper.getSVPHelper().getOperator("Operator"));
            GRNTreeNode node2 = GRNTreeNodeFactory.getBranchNode(InterfaceType.NONE);
            node2.addNode(SVPHelper.getSVPHelper().getRBS("RBS", 0.5));
            node2.addNode(GRNTreeNodeFactory.getLeafNode(SVPManager.getSVPManager().getPart("SpaK"), InterfaceType.NONE));
            node2.addNode(SVPHelper.getSVPHelper().getCDS("CDS", 0.02));
            node.addNode(node2);
            tree.getRootNode().addNode(node);
        */
        
        document=serialiser.exportNetwork(tree);

        SBOLWriter.writeRDF(document, System.out);
        SBOLWriter.writeRDF(document, "prototype.xml");
        
        document= SBOLReader.read("prototype.xml");
        GRNTree tree2=serialiser.importNetwork(document);
        //tree2.setDetectInteractions(true);
        
        SBMLDocument document2 = new SBMLSerialiser().exportNetwork(tree2);

        //document=serialiser.testDocument();
        
        System.out.println("EXPORTED TREE:");      
        System.out.println(tree.debugInfo());
        System.out.println("IMPORTED TREE:");
        System.out.println(tree2.debugInfo());
      
      
        System.out.println();
        System.out.println("Converted!");
        
       
        
        
        
    }
    
}

        //        Iterator<GRNTreeNode> i = tree.getPreOrderIterator();
//        while (i.hasNext()) {
//            GRNTreeNode node = i.next();
//            /*if(node.isLeafNode()) {
//             System.out.println(node.getSVP().getName());
//             for(Interaction in : tree.getInteractions(node)) {
//             System.out.println(" - " + in.getName());
//             }
//             }*/
//            if (node.isTranscriptionUnit()) {
//
//            }
//
//        }
        //System.out.println(tree.debugInfo());
      