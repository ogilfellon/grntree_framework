/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.grntree.api;

import uk.ac.ncl.icos.grntree.impl.InterfaceType;
import uk.ac.ncl.icos.grntree.impl.NodeManager;
import uk.ac.ncl.intbio.virtualparts.entity.Part;

import java.io.Serializable;
import java.util.List;


/**
 *
 * @author owengilfellon
 */
public interface GRNTreeNode extends Serializable {

    /**
     * Returns a node's parent
     * @return
     */
    public GRNTreeNode getParent();

    /**
     * Returns the interface type of a node
     * @return
     */
    public InterfaceType getInterfaceType();

    /**
     * Return a node's name, or a short description of the node, if not set.
     * @return
     */
    public String getName();
    
    /**
     * Returns the Parts contained within those descendants of this node that are InterfaceType: INPUT and contain this
     * node within their interaction bounds.
     * @return A list of parts. An empty list if no INPUT nodes are found.
     */
    public List<Part> getInputParts();

    /**
     * Returns the Nodes that are descendants of this node, are InterfaceType: INPUT and contain this
     * node within their interaction bounds.
     * @return A list of parts. An empty list if no INPUT nodes are found.
     */
    public List<GRNTreeNode> getInputNodes();

    /**
     * Returns the Parts contained within those descendants of this node that are InterfaceType: OUTPUT and contain this
     * node within their interaction bounds.
     * @return A list of parts. An empty list if no OUTPUT nodes are found.
     */
    public List<Part> getOutputParts();

    /**
     * Returns the Nodes that are descendants of this node, are InterfaceType: OUTPUT and contain this
     * node within their interaction bounds.
     * @return A list of parts. An empty list if no OUTPUT nodes are found.
     */
    public List<GRNTreeNode> getOutputNodes();

    /**
     * Returns only those nodes that have this node as a direct parent (i.e. a list of siblings)
     * @return
     */
    public List<GRNTreeNode> getChildren();

    /**
     * Retrieves all parts contained within the current node, or it's descendants, regardless of interaction bounds.
     * @return 
     */
    public List<Part> getDescendents();

    /**
     * Returns a list of all transcription units that are descendents of this node, as determined by isTranscriptionUnit()
     * @return
     */
    public List<GRNTreeNode> getAllDescendantTUs();

    public List<GRNTreeNode> getInteractingNodes();
 
    /**
     * Retrieves a list of edges contained within the current node, and all descendants (i.e. interactionDocuments that occur
     * between nodes that are descendants of this node)
     * @return 
     */
    public List<GRNEdge> getEdges();

    /**
     * Determines whether the current node is a Transcription Unit. A node is considered to be a transcription unit if
     * it is non-empty, holds LeafNodes (those representing parts), and contains no BranchNodes.
     * @return true if node is a transcription unit
     */
    public boolean isTranscriptionUnit();


    /**
     * Determines whether the current node is a regulated Transcription Unit. Returns true if the node is a transcription
     * unit, as defined in isTranscriptionUnit(), and if the node contains an Operator or a Regulated Promoter.
     * @return
     */
    public boolean isRegulatedTranscriptionUnit();

    /**
     * Returns true if a node has siblings (i.e. a node's parent has more than one child node)
     * @return
     */
    public boolean hasSiblings();

    public List<GRNTreeNode> getSiblings();

    public List<GRNTreeNode> getBounds(GRNTreeNode part) throws Exception;

    public boolean isValidLocation(GRNTreeNode part) throws Exception;

    // TODO Add this method
    /**
     * Not currently supported.
     * @return
     */
    public int getHeight();

    /**
     * Returns the depth of the node (i.e. the inclusive number of nodes between this, and the root node)
     * @return
     */
    public int getDepth();

    /**
     *
     * @param interfaceType
     */
    public void setInterfaceType(InterfaceType interfaceType);
    public void setName(String name);
    public void setDisplayName(String displayName);

    /**
     * Sets, or replaces if already set, the child nodes of the current node with
     * those provided.
     * @param nodes
     */
    public void setNodes(List<GRNTreeNode> nodes);

    /**
     * Adds a single node to the list of child nodes.
     * Constraints:
     * <ol>
     * <li>Can not be called on LeafNode</li>
     * <li>Part can not break encapsulation</li>
     * <li>LeafNodes and BranchNodes cannot be mixed.</li>
     * <li>Argument node must be unique within the tree</li>
     * <li>There can be only one promoter, or terminator per TU.</li>
     * <li>Parts must have valid context
     * <ul>
     * <li>Promoters must not follow any part</li>
     * <li>Operators must follow Promoters or Operators</li>
     * <li>RBSs must follow Promoters, Operators, or CDSs (in the case of operons).</li>
     * <li>CDSs must follow RBSs</li>
     * <li>Terminators must follow CDSs</li>
     * </ul></li></ol>

     * @param node
     * @return
     */
    public boolean addNode(GRNTreeNode node) throws Exception, IllegalArgumentException;

    /**
     * Adds a node at a specific index within the List of Child Nodes.
     * @param index
     * @param node
     * @throws Exception
     */
    public void addNode(int index, GRNTreeNode node) throws Exception;

    /**
     * Replaces this node with a provided node.
     * @param node
     * @throws Exception
     */
    public void replaceNode(GRNTreeNode node) throws Exception;
    
    /**
     * Removes the provided node from the list of child nodes, keeping it's child nodes, and moving them up one level.
     * @param node
     * @return 
     */
    public boolean removeNode(GRNTreeNode node);

    /**
     * Removes the provided node from the list of child nodes, and all descendants.
     * @param node
     * @return
     */
    public boolean removeNodeAndDescendents(GRNTreeNode node);

    /**
     * Sets the parent of a node (i.e. moves it within the tree)
     * @param parent
     */
    public void setParent(GRNTreeNode parent);


    /**
     * Increases the depth of a node within the tree. Replaces a given node with a newly created node, and sets the new
     * node as the parent node of the old.
     * @return
     * @throws Exception
     */
    public boolean increaseDepth() throws Exception;

    public GRNTreeNode duplicate();

    public void setNodeManager(NodeManager nm);

    public String debugInfo();

    public String getId();
    
    public boolean isLeafNode();
    
    public boolean isBranchNode();
    
    public boolean isPrototype();
    
    public Part getSVP();

}
