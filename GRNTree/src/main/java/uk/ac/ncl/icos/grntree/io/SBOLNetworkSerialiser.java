package uk.ac.ncl.icos.grntree.io;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.sbolstandard.core.*;
import uk.ac.ncl.icos.grntree.api.GRNTree;
import uk.ac.ncl.icos.grntree.api.GRNTreeNode;
import uk.ac.ncl.icos.grntree.api.NetworkSerialiser;
import uk.ac.ncl.icos.grntree.impl.LeafNode;
import uk.ac.ncl.intbio.virtualparts.entity.Part;

import java.io.InputStream;
import java.net.URI;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

/**
 * Given a GRNTree, returns an SBOL Document describing the model that the tree represents.
 * @author owengilfellon
 */
public class SBOLNetworkSerialiser implements NetworkSerialiser<SBOLDocument> {

    Random r = new Random();


    /*
        The URIs follow the guide at http://www.sbolstandard.org/initiatives/best-practices ... They are intended to
        identify resources, rather than locate them.
      */

    private final String uriBase;


    public SBOLNetworkSerialiser(String projectName)
    {
        uriBase = "http://www.intbio.ncl.ac.uk/evolvedmodel/" + projectName + "/";
    }


    @Override
    public GRNTree importNetwork(SBOLDocument toImport) {
        return null;
    }

    /**
     * The model is added to an SBOL document as a Collection. Each transcription unit is added to the Collection as a
     * DnaComponent. Parts within transcription units are added to them as subcomponents. The SBOL descriptions of each
     * individual Part are retrieved from the SVP repository.
     * @param toExport A GRNTree containing SVPs that are taken from the SVP Repository.
     * @return An SBOL Document describing the model in the GRNTree.
     */
    @Override
    public SBOLDocument exportNetwork(GRNTree toExport)
    {
        Collection model = SBOLFactory.createCollection();
        String modelName = toExport.getRootNode().getName() != null ? toExport.getRootNode().getName() : "SVP_Model";
        model.setName(modelName);
        model.setDisplayId(modelName);
        model.setURI(URI.create(uriBase + "collection/" + modelName));

        Iterator<GRNTreeNode> it = toExport.getPreOrderIterator();
        int transcriptionalUnits = 0;

        while(it.hasNext()) {

            GRNTreeNode node = it.next();

            if(node.isTranscriptionUnit()) {

                /*
                    Each Transcription Unit is added as a DnaComponent.
                    Nucleotide counts are relative to the DnaComponent.
                 */

                int nucleotideCount = 0;
                transcriptionalUnits++;
                DnaComponent transcriptionalUnit = SBOLFactory.createDnaComponent();

                List<GRNTreeNode> nodes = node.getChildren();
                List<Part> cds = node.getOutputParts();

                StringBuilder uriString = new StringBuilder();
                StringBuilder displayIdString = new StringBuilder();
                uriString.append("TU").append(transcriptionalUnits);
                displayIdString.append("TU").append(transcriptionalUnits);

                for(Part part:cds) {
                    displayIdString.append("-").append(part.getName());
                }

                for(GRNTreeNode child:nodes) {

                    // Casting is safe as all children of Transcriptional Units are Leaf Nodes

                    LeafNode ln = (LeafNode) child;
                    Part svp = ln.getSVP();
                    uriString.append("-").append(svp.getName());

                    // Obtain SBOL from repository for each Part

                    try {

                        CloseableHttpClient httpclient = HttpClients.createDefault();
                        HttpGet httpGet = new HttpGet("http://sbol.ncl.ac.uk:8081/part/" + svp.getName() +"/sbol");
                        CloseableHttpResponse response = httpclient.execute(httpGet);
                        HttpEntity entity = response.getEntity();
                        InputStream stream = entity.getContent();
                        SBOLDocument document = SBOLFactory.read(stream);
                        List<SBOLRootObject> rootObjects = document.getContents();

                        for(SBOLRootObject obj:rootObjects) {

                            if(obj instanceof Collection) {

                                Collection c = (Collection) obj;
                                java.util.Collection<DnaComponent> comps = c.getComponents();

                                for(DnaComponent dna:comps) {

                                    // Create a sequence annotation to add Parts to the DnaComponent

                                    SequenceAnnotation ann = SBOLFactory.createSequenceAnnotation();
                                    ann.setStrand(StrandType.POSITIVE);
                                    ann.setBioStart(nucleotideCount + 1);
                                    nucleotideCount += dna.getDnaSequence().getNucleotides().length();
                                    //tuSequence.append(dna.getDnaSequence().getNucleotides());
                                    ann.setBioEnd(nucleotideCount);
                                    ann.setSubComponent(dna);
                                    transcriptionalUnit.addAnnotation(ann);
                                }
                            }
                        }

                        EntityUtils.consume(entity);
                    }
                    catch(Exception e) {
                        e.printStackTrace();
                    }
                    finally {
                        //TODO Should probably fix this
                        //response.close();
                    }
                }

                transcriptionalUnit.setName(displayIdString.toString());
                transcriptionalUnit.setDisplayId(displayIdString.toString());
                transcriptionalUnit.setURI(URI.create(uriBase + transcriptionalUnit.getName()));
                List<SequenceAnnotation> annotations = transcriptionalUnit.getAnnotations();

                for(SequenceAnnotation ann:annotations) {
                    ann.setURI(URI.create(uriBase + transcriptionalUnit.getName() +
                            "/Ann-" + ann.getBioStart() +
                            "-" + ann.getBioEnd()));
                }

                model.addComponent(transcriptionalUnit);
            }
        }

        // Add collection to the document

        SBOLDocument document = SBOLFactory.createDocument();
        document.addContent(model);

        return document;
    }
}
