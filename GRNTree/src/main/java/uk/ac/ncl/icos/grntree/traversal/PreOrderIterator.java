/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.grntree.traversal;


import uk.ac.ncl.icos.grntree.api.GRNTree;
import uk.ac.ncl.icos.grntree.api.GRNTreeNode;

import java.util.HashSet;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.Set;

/**
 *
 * @author owengilfellon
 */
public class PreOrderIterator implements Iterator<GRNTreeNode>, GRNTreeIterator {
    
    private GRNTree tree;
    private GRNTreeNode currentNode = null;
    private Set<GRNTreeNode> explored = new HashSet<GRNTreeNode>();
    private int depth = 0;
    
    public PreOrderIterator(GRNTree tree) {
        this.tree = tree;
    }
    
    public int getDepth()
    {
        return depth;
    }

    @Override
    public boolean hasNext() {
        
        if(currentNode == null && tree.getRootNode() != null){
            return true;
        }
        
        if(currentNode!=null)
        {
            if(!currentNode.getChildren().isEmpty() && !explored.containsAll(currentNode.getChildren())){
                return true;
            }
            
            if(currentNode.hasSiblings())
            {
                if(!explored.containsAll(currentNode.getParent().getChildren())){
                    return true;
                }                   
            }
            
            GRNTreeNode tempNode = currentNode;
            
            while(tempNode.getParent()!=null){
                
                tempNode = tempNode.getParent();
                
                if(!explored.containsAll(tempNode.getChildren())) {
                    return true;
                }
            }
        }
        
        return false;
        
    }

    @Override
    public GRNTreeNode next() {
        
        if(currentNode == null && tree.getRootNode() != null){
            currentNode = tree.getRootNode();
            explored.add(currentNode);
            depth++;
            return currentNode;
        }
        
        if(currentNode!=null)
        {
            if(!currentNode.getChildren().isEmpty() && !explored.containsAll(currentNode.getChildren())){
                ListIterator<GRNTreeNode> it = currentNode.getChildren().listIterator();
                while(it.hasNext())
                {
                    GRNTreeNode n = it.next();
                    
                    if(!explored.contains(n))
                    {
                        currentNode = n;
                        explored.add(currentNode);
                        depth++;
                        return currentNode;
                    }
                }
            }

            if(currentNode.hasSiblings())
            {
                if(!explored.containsAll(currentNode.getParent().getChildren())){
                    ListIterator<GRNTreeNode> it = currentNode.getParent().getChildren().listIterator();
                    while(it.hasNext())
                    {
                        GRNTreeNode n = it.next();

                        if(!explored.contains(n))
                        {
                            currentNode = n;
                            explored.add(currentNode);
                            return currentNode;
                        }
                    }
                }                   
            }
            
            GRNTreeNode tempNode = currentNode;
            
            while(tempNode.getParent()!=null){
                
                tempNode = tempNode.getParent();
                depth--;
                
                if(!explored.containsAll(tempNode.getChildren())) {
                    ListIterator<GRNTreeNode> it = tempNode.getChildren().listIterator();
                    while(it.hasNext())
                    {
                        GRNTreeNode n = it.next();

                        if(!explored.contains(n))
                        {
                            currentNode = n;
                            explored.add(currentNode);
                            return currentNode;
                        }
                    }
                }
            }
        }
        
        return null;
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
}
