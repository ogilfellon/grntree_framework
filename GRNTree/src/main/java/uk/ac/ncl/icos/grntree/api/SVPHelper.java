package uk.ac.ncl.icos.grntree.api;

/**
 * Created by owengilfellon on 14/01/2015.
 */

import uk.ac.ncl.icos.svpmanager.MolecularForm;
import uk.ac.ncl.icos.svpmanager.RegulationRole;
import uk.ac.ncl.icos.grntree.impl.InterfaceType;
import uk.ac.ncl.icos.grntree.impl.PrototypeNode;
import uk.ac.ncl.intbio.virtualparts.PartsHandler;
import uk.ac.ncl.intbio.virtualparts.entity.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import uk.ac.ncl.icos.grntree.impl.ConcreteNode;
import uk.ac.ncl.icos.grntree.impl.LeafNode;
import uk.ac.ncl.icos.svpmanager.SVPManager;


/**
 * @author b1050029
 */
public class SVPHelper {

    private final static String repositoryURL = "http://sbol.ncl.ac.uk:8081";
    private final static PartsHandler partshandler = new PartsHandler(repositoryURL);
    private final SVPManager manager = SVPManager.getSVPManager();
    private final static SVPHelper m = new SVPHelper();

    private final Random r = new Random();
    private final Property dataSource = new Property();
    private final Property dnaComponent = new Property();

    private SVPHelper()
    {
        dataSource.setName("Data Source");
        dataSource.setValue("None");
        dnaComponent.setName("type");
        dnaComponent.setValue("DnaComponent");
    }

    /**
     * Factory method for obtaining an SVPManager. The SVPManager is part singleton -
     * each method call returns part reference to the single SVPManager instance.
     * @return
     */
    public static SVPHelper getSVPHelper()
    {
        return m;
    }

    // ==============================================================
    //  Part Methods
    // ==============================================================

    public GRNTreeNode getPromoter(String id, double transcriptionRate)
    {
        Part part = new Part();
        part.setName(id);
        part.setDisplayName(id);
        part.setType("Promoter");
        part.setMetaType("Part");

        List<Property> properties = new ArrayList<Property>();
        properties.add(dataSource);
        properties.add(dnaComponent);
        Property property = new Property();
        property.setName("type");
        property.setValue("Promoter");
        part.setProperties(properties);
        part.setStatus("Prototype");

        return(GRNTreeNodeFactory.getLeafNode(part, getPoPSProduction(part, transcriptionRate), InterfaceType.NONE));
    }

    public GRNTreeNode getRBS(String id, double translationRate)
    {
        Part rbs = new Part();
        rbs.setName(id);
        rbs.setDisplayName(id);
        rbs.setType("RBS");
        rbs.setMetaType("Part");

        List<Property> properties = new ArrayList<Property>();
        properties.add(dataSource);
        properties.add(dnaComponent);
        Property typeRBS = new Property();
        typeRBS.setName("type");
        typeRBS.setValue("Ribosome Binding Site");
        properties.add(typeRBS);
        rbs.setProperties(properties);
        rbs.setStatus("Prototype");

        return GRNTreeNodeFactory.getLeafNode(rbs, getRiPSProduction(rbs, translationRate), InterfaceType.NONE);
    }

    public GRNTreeNode getCDS(String id, double degradationRate)
    {
        Part part = new Part();
        part.setDisplayName(id);
        part.setName(id);
        part.setType("FunctionalPart");
        part.setMetaType("Part");

        List<Property> properties = new ArrayList<Property>();
        properties.add(dataSource);
        properties.add(dnaComponent);
        Property propCDS = new Property();
        propCDS.setName("type");
        propCDS.setValue("Protein");
        properties.add(propCDS);
        part.setProperties(properties);
        part.setStatus("Prototype");

        return GRNTreeNodeFactory.getLeafNode(part, getProteinProductionAndDegradation(part, degradationRate), InterfaceType.NONE);
    }

    public GRNTreeNode getOperator(String id)
    {
        Part part = new Part();
        part.setDisplayName(id);
        part.setName(id);
        part.setType("Operator");
        part.setMetaType("Part");

        List<Property> properties = new ArrayList<Property>();
        properties.add(dataSource);
        properties.add(dnaComponent);
        Property propOperator = new Property();
        propOperator.setName("type");
        propOperator.setValue("Operator");
        properties.add(propOperator);
        part.setProperties(properties);
        part.setStatus("Prototype");

        return GRNTreeNodeFactory.getLeafNode(part, getOperatorPoPSModulation(part), InterfaceType.NONE);
    }

    public GRNTreeNode getTerminator(String id)
    {
        Part part = new Part();
        part.setName(id);
        part.setDisplayName(id);
        part.setType("Terminator");
        part.setMetaType("Terminator");
        part.setMetaType("Part");

        List<Property> properties = new ArrayList<Property>();
        properties.add(dataSource);
        properties.add(dnaComponent);
        Property property = new Property();
        property.setName("type");
        property.setValue("Terminator");
        part.setProperties(properties);
        part.setStatus("Prototype");

        return(GRNTreeNodeFactory.getLeafNode(part, InterfaceType.NONE));
    }

    public GRNTreeNode getShim(String id)
    {
        Part part = new Part();
        part.setName(id);
        part.setDisplayName(id);
        part.setType("Shim");
        part.setMetaType("Shim");
        part.setMetaType("Part");

        List<Property> properties = new ArrayList<Property>();
        properties.add(dataSource);
        properties.add(dnaComponent);
        Property property = new Property();
        property.setName("type");
        property.setValue("Shim");
        part.setProperties(properties);
        part.setStatus("Prototype");

        return(GRNTreeNodeFactory.getLeafNode(part, InterfaceType.NONE));
    }

    /**
     * Given a part, and a transcription rate(?), returns an internal Reaction
     * that provides the mathematical rules for SBML Models
     * @param part
     * @param ktr
     * @return
     */
    private List<Interaction> getPoPSProduction(Part part, double ktr)
    {
        Parameter pops = new Parameter();

        pops.setName("PoPSOutput");
        pops.setParameterType("PoPS");
        pops.setScope("Output");

        Parameter aktr = new Parameter();
        aktr.setName("ktr");
        aktr.setParameterType("ktr");
        aktr.setValue(ktr);
        aktr.setScope("Local");

        Interaction interaction = new Interaction();
        interaction.AddPart(part.getName());
        interaction.setName(part.getName() + "_PoPSProduction");
        interaction.AddParameter(pops);
        interaction.AddParameter(aktr);

        interaction.setIsInternal(true);
        interaction.setIsReaction(false);

        interaction.setFreeTextMath("-> PoPS");
        interaction.setMathName("PoPSProduction");
        interaction.setInteractionType("PoPSProduction");
        List<Interaction> interactions = new ArrayList();
        interactions.add(interaction);
        interaction.setPartDetails(new ArrayList<InteractionPartDetail>());
        

        return interactions;
    }

    /**
     * Given a part, and a translation rate(?), returns an internal reaction
     * that provides the mathematical rules for SBML Models
     * @param part
     * @param ktl
     * @return
     */
    private List<Interaction> getRiPSProduction(Part part, double ktl)
    {
        Parameter volume = new Parameter();
        volume.setName("volume");
        volume.setParameterType("volume");
        volume.setScope("Global");
        volume.setValue(1.0);

        Parameter mrna = new Parameter();
        mrna.setName("mRNAInput");
        mrna.setParameterType("mRNA");
        mrna.setScope("Input");

        Parameter rips = new Parameter();
        rips.setName("RiPSOutput");
        rips.setParameterType("RiPS");
        rips.setScope("Output");

        Parameter bktl = new Parameter();
        bktl.setName("ktl");
        bktl.setParameterType("ktl");
        bktl.setScope("Local");
        bktl.setValue(ktl);

        Interaction interaction = new Interaction();
        interaction.AddPart(part.getName());
        interaction.setName(part.getName() + "_RiPSProduction");

        interaction.AddParameter(volume);
        interaction.AddParameter(mrna);
        interaction.AddParameter(rips);
        interaction.AddParameter(bktl);

        interaction.setInteractionType("RiPS Production");
        interaction.setIsInternal(true);
        interaction.setIsReaction(false);
        interaction.setFreeTextMath("-> RiPS");
        interaction.setMathName("RiPSProduction");

        interaction.setPartDetails(new ArrayList<InteractionPartDetail>());

        List<Interaction> interactions = new ArrayList();
        interactions.add(interaction);

        return interactions;
    }

    private List<Interaction> getProteinProductionAndDegradation(Part cds, double degradationRate)
    {

        Parameter volume = new Parameter();
        volume.setName("volume");
        volume.setParameterType("volume");
        volume.setScope("Global");
        volume.setValue(1.0);

        // Production

        Parameter ripsInput = new Parameter();
        ripsInput.setName("RiPSInput");
        ripsInput.setParameterType("RiPS");
        ripsInput.setScope("Input");

        Interaction production = new Interaction();
        production.AddPart(cds.getName());
        production.setName(cds.getName() + "_Production");
        production.AddParameter(volume);
        production.AddParameter(ripsInput);

        InteractionPartDetail o_ipd = new InteractionPartDetail(cds.getName(), "Default", "Output", 1, "Species");
        production.AddPartDetail(o_ipd);
        production.setInteractionType("Protein Production");
        production.setIsInternal(true);
        production.setIsReaction(true);
        production.setFreeTextMath("-> " + cds.getName());
        production.setMathName("ProteinProduction");

        // Degradation

        Parameter kd = new Parameter();
        kd.setName("kd");
        kd.setParameterType("kd");
        kd.setScope("Local");
        kd.setValue(degradationRate);

        Interaction degradation = new Interaction();
        degradation.AddPart(cds.getName());
        degradation.setName(cds.getName() + "_Degradation");
        degradation.AddParameter(kd);

        InteractionPartDetail d_ipd = new InteractionPartDetail(cds.getName(), "Default", "Input", 1, "Species");
        degradation.AddPartDetail(d_ipd);

        degradation.setInteractionType("Degradation");
        degradation.setIsInternal(true);
        degradation.setIsReaction(true);
        degradation.setFreeTextMath(cds.getName() + " ->");
        degradation.setMathName("Degradation");

        List<Interaction> interactions = new ArrayList<Interaction>();
        interactions.add(production);
        interactions.add(degradation);

        return interactions;
    }

    private List<Interaction> getOperatorPoPSModulation(Part part)
    {
        Parameter input = new Parameter();
        input.setName("Input");
        input.setParameterType("PoPS");
        input.setScope("Input");

        Parameter output = new Parameter();
        output.setName("PoPSOutput");
        output.setParameterType("PoPS");
        output.setScope("Output");

        Interaction interaction = new Interaction();
        interaction.AddPart(part.getName());
        interaction.setName(part.getName() + "_PoPSModulation");
        interaction.AddParameter(input);
        interaction.AddParameter(output);
        interaction.setInteractionType("OperatorPoPSModulation");
        interaction.setIsInternal(true);
        interaction.setIsReaction(false);
        interaction.setFreeTextMath("");
        interaction.setMathName("OperatorPoPSModulation");
        interaction.setPartDetails(new ArrayList<InteractionPartDetail>());

        List<Interaction> interactions = new ArrayList();
        interactions.add(interaction);
        return interactions;
    }


    // ==============================================================
    //  Interaction Methods
    // ==============================================================

    /**
     * Returns an Interaction describing Promoter or Operator regulation by a protein
     *
     * @param regulatedPart the part that is regulated - must be a promoter or operator
     * @param transcriptionFactor the part that regulates the part - must be a cds
     * @param form default or phosphorylated
     * @param role repression or activation
     * @param disassociationConstant
     * @return
     */
    public Interaction getRegulationInteraction(GRNTreeNode regulatedPart,
                                                GRNTreeNode transcriptionFactor,
                                                MolecularForm form,
                                                RegulationRole role,
                                                double disassociationConstant)
    {
        if(     !(regulatedPart instanceof PrototypeNode) &&
                !(transcriptionFactor instanceof PrototypeNode))
                    return null;
        
        Parameter n = new Parameter("n", "n", 1.0);

        Parameter output = new Parameter();
            output.setName("PoPSOutput");
            output.setParameterType("PoPS");
            output.setScope("Output");

        Parameter input = new Parameter();
            input.setName("PoPSInput");
            input.setParameterType("PoPS");
            input.setScope("Input");

        Parameter okm = new Parameter();
            okm.setName("Km");
            okm.setParameterType("Km");
            okm.setScope("Local");
            okm.setValue(disassociationConstant);

        Interaction interaction = new Interaction();
        interaction.AddPart(((LeafNode)transcriptionFactor).getSVP().getName());
        interaction.AddPart(((LeafNode)regulatedPart).getSVP().getName());
        interaction.setName(((LeafNode)transcriptionFactor).getSVP().getName() + "_bi_to_" + ((LeafNode)regulatedPart).getSVP().getName());
        //interaction.setInteractionType("Transcriptional regulation");

        interaction.AddParameter(n);
        interaction.AddParameter(output);
        interaction.AddParameter(input);
        interaction.AddParameter(okm);

        String tfRole = role == RegulationRole.ACTIVATOR ? "Activator" : "Repressor";
        String tfForm = form == MolecularForm.PHOSPHORYLATED ? "Phosphorylated" : "Default";

        InteractionPartDetail o_ipd = new InteractionPartDetail(((LeafNode)transcriptionFactor).getSVP().getName(), tfForm, "Modifier", 1, tfRole);
        interaction.AddPartDetail(o_ipd);

        StringBuilder sb = new StringBuilder();
        sb.append("Transcriptional ");
        if(role == RegulationRole.ACTIVATOR){
            sb.append("activation");
        }
        else{
            sb.append("repression");
        }
        if(((LeafNode)regulatedPart).getType().equals("Operator")){
            sb.append(" using an operator");
        }

        String math = (role.equals(RegulationRole.ACTIVATOR))
                ? (interaction.getName() + "_PoPS * " + ((LeafNode)transcriptionFactor).getSVP().getName() +" / (" + ((LeafNode)transcriptionFactor).getSVP().getName() +" + Km)")
                : "Km / (" + ((LeafNode)transcriptionFactor).getSVP().getName() +" + Km)";

        interaction.setInteractionType(sb.toString());
        interaction.setIsReaction(false);
        interaction.setFreeTextMath(math);

        String mathType = role == RegulationRole.ACTIVATOR
                ? ((LeafNode)regulatedPart).getSVP().getType() + "Induction"
                : ((LeafNode)regulatedPart).getSVP().getType() + "Repression";

        interaction.setMathName(mathType);

        return interaction;
    }

    public Interaction getPhosphorylationInteraction(GRNTreeNode donorPart, double dephosphorylationRate1, double degradationRate1,
                                                     GRNTreeNode acceptorPart, double dephosphorylationRate2, double degradationRate2, double kf)
    {
        StringBuilder sb = new StringBuilder();
        sb.append(((LeafNode)((LeafNode)donorPart)).getSVP().getName()).append("~P").append(" + ").append(((LeafNode)acceptorPart).getSVP().getName())
                .append(" -> ")
                .append(((LeafNode)donorPart).getSVP().getName()).append(" + ").append(((LeafNode)acceptorPart).getSVP().getName()).append("~P");

        List<InteractionPartDetail> constraints = new ArrayList<InteractionPartDetail>();
        constraints.add(new InteractionPartDetail(((LeafNode)donorPart).getName(), "Phosphorylated", "Input", 1, "Protein1_P"));
        constraints.add(new InteractionPartDetail(((LeafNode)donorPart).getName(), "Default", "Output", 1, "Protein1"));
        constraints.add(new InteractionPartDetail(((LeafNode)acceptorPart).getName(), "Default", "Input", 1, "Protein2"));
        constraints.add(new InteractionPartDetail(((LeafNode)acceptorPart).getName(), "Phosphorylated", "Output", 1, "Protein2_P"));

        Interaction interaction = new Interaction();
        interaction.AddPart(((LeafNode)donorPart).getSVP().getName());
        interaction.AddPart(((LeafNode)acceptorPart).getSVP().getName());
        interaction.setName(((LeafNode)acceptorPart).getSVP().getName() + "_ph_by_" + ((LeafNode)donorPart).getSVP().getName());

        interaction.setInteractionType("Phosphorylation");
        interaction.setIsReaction(true);
        interaction.setFreeTextMath(sb.toString());
        interaction.setMathName("TwoComponentPhosphorelay");
        interaction.setPartDetails(constraints);

        Parameter kfPar = new Parameter();
        kfPar.setName("kf");
        kfPar.setParameterType("kf");
        kfPar.setScope("Local");
        kfPar.setValue(kf);
        interaction.AddParameter(kfPar);

        return interaction;
    }

    public List<Interaction> getDephosphorylationandDegradation(GRNTreeNode cds,
                                                                 double dephosphorylationRate,
                                                                double degradationRate)
    {
        List<InteractionPartDetail> constraints = new ArrayList<InteractionPartDetail>();
        constraints.add(new InteractionPartDetail(((LeafNode)cds).getName(), "Phosphorylated", "Input", 1, "SpeciesPhosphorylated"));
        constraints.add(new InteractionPartDetail(((LeafNode)cds).getName(), "Default", "Output", 1, "Species"));

        // Dephosphorylation

        Interaction dephosphorylation = new Interaction();
        dephosphorylation.setIsInternal(true);
        dephosphorylation.setIsReaction(true);
        dephosphorylation.setName("Dephosphorylation");
        dephosphorylation.setFreeTextMath(((LeafNode)cds).getSVP().getName() + "~P->" + ((LeafNode)cds).getSVP().getName());
        dephosphorylation.setMathName("Dephosphorylation");
        dephosphorylation.AddPart(((LeafNode)cds).getSVP().getName());
        dephosphorylation.setPartDetails(constraints);
        Parameter kdeP = new Parameter();
        kdeP.setName("kdeP");
        kdeP.setParameterType("kdeP");
        kdeP.setScope("Local");
        kdeP.setValue(dephosphorylationRate);
        dephosphorylation.AddParameter(kdeP );

        // Degradation

        Interaction phosphorylatedDegradation = new Interaction();
        phosphorylatedDegradation.setIsInternal(true);
        phosphorylatedDegradation.setIsReaction(true);
        phosphorylatedDegradation.setFreeTextMath(((LeafNode)cds).getName() + "~P->");
        phosphorylatedDegradation.setMathName("Degradation");
        phosphorylatedDegradation.setName("Degradation");
        phosphorylatedDegradation.AddPart(((LeafNode)cds).getSVP().getName());
        phosphorylatedDegradation.AddPartDetail(new InteractionPartDetail(((LeafNode)cds).getSVP().getName(), "Phosphorylated", "Input", 1, "Species"));
        Parameter kd = new Parameter();
        kd.setName("kd");
        kd.setParameterType("kd");
        kd.setScope("Local");
        kd.setValue(degradationRate);
        phosphorylatedDegradation.AddParameter(kd);
        List<Interaction> interactions = new ArrayList<Interaction>();
        interactions.add(dephosphorylation);
        interactions.add(phosphorylatedDegradation);
        return interactions;
    }
    
        
    public PrototypeNode convertPrototypeNode(GRNTreeNode node) {
        
        if(node.isPrototype()) {
            return (PrototypeNode)node;
        }
        
        PrototypeNode prototype = (PrototypeNode)GRNTreeNodeFactory.getLeafNode(node.getSVP(),
                manager.getInternalEvents(node.getSVP()),
                node.getInterfaceType());
        
        prototype.setName(node.getName());
        prototype.getSVP().setStatus("Prototype");
        
        return prototype;
    }
}

