package uk.ac.ncl.icos.grntree.impl;

import uk.ac.ncl.icos.grntree.api.GRNEdge;
import uk.ac.ncl.icos.grntree.api.GRNTreeNode;
import uk.ac.ncl.intbio.virtualparts.entity.Interaction;
import uk.ac.ncl.intbio.virtualparts.entity.InteractionPartDetail;
import uk.ac.ncl.intbio.virtualparts.entity.Part;

import java.io.Serializable;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author owengilfellon
 */
public class BranchNode extends AbstractNode implements Serializable {
    
    private List<GRNTreeNode> nodes;
    private List<GRNEdge> edges;

    public static BranchNode getBranchNode(InterfaceType interfaceType)
    {
        return new BranchNode(interfaceType);
    }
    
    public static BranchNode getBranchNode(InterfaceType interfaceType, List<GRNTreeNode> nodes)
    {
        return new BranchNode(interfaceType, nodes);
    }
    
    private BranchNode() {
        
    }
    
    /**
     * Constructs an empty Node for containing Nodes and Edges.
     * @param interfaceType The interfaceType of the node being created. 
     */
    protected BranchNode(InterfaceType interfaceType) {
        super(interfaceType);
        nodes = new ArrayList<>();
        edges = new ArrayList<>();
    }
    
    /**
     * Constructs a Node that contains Nodes and Edges. Nodes are supplied as a parameter, edges are calculated automatically.
     * @param interfaceType The interfaceType of the node being created. 
     * @param nodes The nodes 
     */
    protected BranchNode(InterfaceType interfaceType, List<GRNTreeNode> nodes) {
        super(interfaceType);
        this.setNodes(nodes);
        edges = new ArrayList<GRNEdge>();
    }

    /**
     * Makes the current node a child within a parent node, with the parent node
     * replacing the current node in the tree.
     * @return true if successful
     */
    @Override
    public boolean increaseDepth() throws Exception {
        List<GRNTreeNode> nodesToNest = new ArrayList<GRNTreeNode>();
        nodesToNest.addAll(this.nodes);      
        BranchNode n = getBranchNode(this.interfaceType);
        n.setNodes(nodesToNest);
        n.setName(getName());        
        this.setName(null);
        this.nodes.clear();
        this.edges.clear();
        addNode(n);    
        return true;
    }

    /**
     *
     * @return A deep copy of the BranchNode and all children
     */
    @Override
    public GRNTreeNode duplicate() {
        
        List<GRNTreeNode> duplicates = new ArrayList<GRNTreeNode>();
        
        for(GRNTreeNode n:nodes) {
            duplicates.add(n.duplicate());
        }
        
        return BranchNode.getBranchNode(interfaceType, duplicates);
    }

    @Override
    public List<GRNTreeNode> getInteractingNodes() {
        List<GRNTreeNode> interactingNodes = new ArrayList<GRNTreeNode>();
        Set<GRNTreeNode> uniqueNodes = new HashSet<GRNTreeNode>();

        List<GRNTreeNode> nodesFromBelow = getInteractingNodesAbove(); // ?? This should be "below"?
        uniqueNodes.addAll(nodesFromBelow);
        List<GRNTreeNode> nodesFromAbove = getInteractingNodesAbove();
        uniqueNodes.addAll(nodesFromAbove);
        
        interactingNodes.addAll(uniqueNodes);
  
        return interactingNodes;
    }
    
    private List<GRNTreeNode> getInteractingNodesBelow() {
        List<GRNTreeNode> interactingNodes = new ArrayList<GRNTreeNode>();
        ListIterator<GRNTreeNode> it = nodes.listIterator();
        
        while(it.hasNext())
        {
            GRNTreeNode n = it.next();
            
            for(GRNTreeNode i:n.getInputNodes())
            {
                if(!interactingNodes.contains(i)){
                    interactingNodes.add(i);
                }
            }
            for(GRNTreeNode o:n.getOutputNodes())
            {
                if(!interactingNodes.contains(o)){
                    interactingNodes.add(o);
                }
            }       
        }
        return interactingNodes;
    }
    
    private List<GRNTreeNode> getInteractingNodesAbove() {
        List<GRNTreeNode> interactingNodes = new ArrayList<GRNTreeNode>();
        
        if(this.getParent() != null)
        {
            GRNTreeNode parent = this.getParent();
            
            while(  !parent.getInterfaceType().equals(InterfaceType.NONE) &&
                    parent.getInterfaceType().equals(this.interfaceType))
            {
                parent = parent.getParent() == null ? parent : parent.getParent();
            }
            
            if(  !parent.getInterfaceType().equals(InterfaceType.NONE) &&
                    parent.getInterfaceType().equals(this.interfaceType))
            {
                parent = parent.getParent() == null ? parent : parent.getParent();
            }
          
            ListIterator<GRNTreeNode> it2 = parent.getChildren().listIterator();
        
            while(it2.hasNext())
            {
                GRNTreeNode n = it2.next();

                for(GRNTreeNode i:n.getInputNodes())
                {
                    if(!interactingNodes.contains(i)){
                        interactingNodes.add(i);
                    }
                }
                for(GRNTreeNode o:n.getOutputNodes())
                {
                    if(!interactingNodes.contains(o)){
                        interactingNodes.add(o);
                    }
                }       
            }
        }
  
        return interactingNodes;
    }

    /**
     * Returns the parts contained within any nodes that are InterfaceType: INPUT
     * @return A list of parts. An empty list if no Input nodes are found.
     */
    @Override
    public List<Part> getInputParts() {

        List<Part> inputParts = new ArrayList<Part>();
        ListIterator<GRNTreeNode> it = nodes.listIterator();

        while(it.hasNext()) {

            GRNTreeNode node = it.next();

            if(node.getInterfaceType().equals(InterfaceType.INPUT) ||
                node.getInterfaceType().equals(InterfaceType.BOTH)) {

                inputParts.addAll(node.getInputParts());
           }
        }
        
        return inputParts;
    }
    
    /**
     * Returns the parts contained within any nodes that are InterfaceType: OUTPUT
     * @return A list of parts. An empty list if no Output nodes are found.
     */
    @Override
    public List<Part> getOutputParts() {

        List<Part> outputParts = new ArrayList<Part>();
        ListIterator<GRNTreeNode> it = nodes.listIterator();

        while(it.hasNext()) {

            GRNTreeNode node = it.next();
            
            if(node.getInterfaceType().equals(InterfaceType.OUTPUT) ||
                    node.getInterfaceType().equals(InterfaceType.BOTH)) {
                    outputParts.addAll(node.getOutputParts());                
                }
        }
        
        return outputParts;
    }

    /**
     * Returns the parts contained within any nodes that are InterfaceType: INPUT
     * @return A list of parts. An empty list if no Input nodes are found.
     */
    @Override
    public List<GRNTreeNode> getInputNodes() {
        List<GRNTreeNode> inputNodes = new ArrayList<GRNTreeNode>();
        ListIterator<GRNTreeNode> it = nodes.listIterator();
        
        while(it.hasNext())
        {
            GRNTreeNode node = it.next();
            
             if(node.getInterfaceType().equals(InterfaceType.INPUT) ||
                    node.getInterfaceType().equals(InterfaceType.BOTH))
                {
                        inputNodes.addAll(node.getInputNodes());         
                }
        }
        
        return inputNodes;
    }
    
    /**
     * Returns the parts contained within any nodes that are InterfaceType: OUTPUT
     * @return A list of parts. An empty list if no Output nodes are found.
     */
    @Override
    public List<GRNTreeNode> getOutputNodes() {
        List<GRNTreeNode> outputNodes = new ArrayList<GRNTreeNode>();
        ListIterator<GRNTreeNode> it = nodes.listIterator();
        
        while(it.hasNext())
        {
            GRNTreeNode node = it.next();
            
            if(node.getInterfaceType().equals(InterfaceType.OUTPUT) ||
                    node.getInterfaceType().equals(InterfaceType.BOTH))
            {
                    outputNodes.addAll(node.getOutputNodes());    
            }
        }
        
        return outputNodes;
    }

    /**
     * Retrieves all parts contained within the current node, or children of the
     * current node.
     * @return 
     */
    @Override
    public List<Part> getDescendents() {

        // Recursive call, to retrieve all descendents. LeafNode also has a getDescendents method which returns
        // the Part that they contain

        List<Part> parts = new ArrayList<Part>();
        ListIterator<GRNTreeNode> it = nodes.listIterator();
        while(it.hasNext())
        {
            GRNTreeNode node = it.next();
            parts.addAll(node.getDescendents());        
        }
        
        return parts;
    }

    /**
     * Returns the nodes contained only within the node, ignoring those contained
     * by children.
     * @return 
     */
    @Override
    public List<GRNTreeNode> getChildren() {
        return nodes;
    }




    // =============================================================
    // TODO Delete this method - if used, use node manager instead
    // Filter by interfaces? Or should this be performed by another
    // Class?
    // =============================================================

     /**
     * Retrieves a list of edges contained within the current node, and all 
     * child nodes.
     * @return 
     */
    @Override
    public List<GRNEdge> getEdges() {
              
        
        List<GRNEdge> edges2 = new ArrayList<GRNEdge>();
        List<GRNTreeNode> outputNodes = new ArrayList<GRNTreeNode>();
        List<GRNTreeNode> inputNodes = new ArrayList<GRNTreeNode>();
        ListIterator<GRNTreeNode> n_it = nodes.listIterator();
        
        /*
         * Iterate through all child nodes of the current node.
         */
 
        while(n_it.hasNext()) { 
            
            GRNTreeNode node = n_it.next();
            
            
            /*
             * Retrieve the INPUT and OUTPUT nodes available to the current node.
             * Retrieving Nodes rather than Parts directly allows for edges to
             * be added between the Nodes.
             */
            
            inputNodes.addAll(node.getInputNodes());
            outputNodes.addAll(node.getOutputNodes());
        }
       
        if(outputNodes.size() > 0) {

            ListIterator<GRNTreeNode> outputIterator = outputNodes.listIterator();

            while(outputIterator.hasNext()){
                
                /*
                 * If OUTPUT nodes are found, iterate through them, retrieving
                 * their Parts. All OUTPUT nodes returned from getOutputNodes()
                 * are LeafNodes, so the cast to LeafNode is safe.
                 */

                GRNTreeNode outputnode = outputIterator.next();
                LeafNode leaf1 = (LeafNode) outputnode;
                Part part1 = leaf1.getSVP(); 
                
                /*
                 * Check every OUTPUT part against every OUTPUT part. This could
                 * be optimised.
                 */
                
                ListIterator<GRNTreeNode> outputIterator2 = outputNodes.listIterator();

                while(outputIterator2.hasNext()){
                    
                    GRNTreeNode outputnode2 = outputIterator2.next();
                    LeafNode leaf2 = (LeafNode) outputnode2;
                    Part part2 = leaf2.getSVP();
                    
                    /*
                     * A pair of parts have been retrieved. Any interactionDocuments between
                     * these two parts are retrieved using a method in SVPManager
                     */
                    
                    List<Interaction> interactions = m.getInteractions(part1, part2);
                    
                     if(interactions.size() > 0) {
                        
                         ListIterator<Interaction> interactionsIterator = interactions.listIterator();
 
                        /*
                         * If there are interactionDocuments, iterate through them.
                         */

                         while(interactionsIterator.hasNext()) {

                             Interaction i = interactionsIterator.next();
                             
                             /*
                              * For now, the only OUTPUT-OUTPUT interactionDocuments we are interested
                              * in are Phosphorylation interactionDocuments
                              */

                             if(i.getInteractionType().equals("Phosphorylation")){

                                 List<InteractionPartDetail> ipd = i.getPartDetails();
                                 ListIterator<InteractionPartDetail> ipd_it = ipd.listIterator();
                                 
                                 while(ipd_it.hasNext()){

                                     InteractionPartDetail d = ipd_it.next();
                                     
                                     /*
                                      * If Part 1 acts on Part 2 in the interaction - i.e.
                                      * if Part 1 phosphorylates Part 2, add an edge to this Node.
                                      */

                                     if(d.getInteractionRole().equals("Input") &&
                                             d.getPartForm().equals("Phosphorylated") &&
                                             d.getPartName().equals(part1.getName())) {

                                         GRNInteractionEdge e = new GRNInteractionEdge(outputnode, outputnode2, InteractionType.PHOSPHORYLATION);

                                         /*
                                          * Duplicate check, due to the inefficient method of comparing
                                          * all OUTPUT nodes with all OUTPUT nodes.
                                          */
                                         
                                         if(!edges2.contains(e)){                                         
                                            edges2.add(e); 
                                         }
                                     }
                                 }
                             }
                         }
                     }
                }

                /*
                 * Check every OUTPUT part against every INPUT part.
                 */

                if(inputNodes.size() > 0){

                    ListIterator<GRNTreeNode> inputIterator = inputNodes.listIterator();

                    while(inputIterator.hasNext()){

                         GRNTreeNode inputnode = inputIterator.next();
                         LeafNode leaf2 = (LeafNode) inputnode;
                         Part part2 = leaf2.getSVP();
                         List<Interaction> interactions = m.getInteractions(part1, part2);
                        
                        /*
                        * A pair of parts have been retrieved. Any interactionDocuments between
                        * these two parts are retrieved using a method in SVPManager
                        */

                         if(interactions.size() > 0) {
                             
                            
                             ListIterator<Interaction> interactionsIterator = interactions.listIterator();

                             while(interactionsIterator.hasNext()) {

                                 Interaction i = interactionsIterator.next();
                                 
                                 /*
                                * For now, the only OUTPUT-INPUT interactionDocuments we are interested
                                * in are Transcriptional Regulation
                                */
             
                                 if(i.getInteractionType().equals("Transcriptional activation") ||
                                         i.getInteractionType().equals("Transcriptional repression") ||
                                         i.getInteractionType().equals("Transcriptional repression using an operator") ||
                                         i.getInteractionType().equals("Transcriptional activation using an operator")){

                                     List<InteractionPartDetail> ipd = i.getPartDetails();
                                     ListIterator<InteractionPartDetail> ipd_it = ipd.listIterator();

                                     while(ipd_it.hasNext()){

                                         InteractionPartDetail d = ipd_it.next();
                                         
                                         /*
                                        * If Part 1 regulates Part 2 add an edge to this Node.
                                        */

                                         if(d.getInteractionRole().equals("Modifier") &&
                                                 d.getPartName().equals(part1.getName())){

                                             InteractionType type = i.getInteractionType().equals("Transcriptional activation") ? InteractionType.ACTIVATION : InteractionType.REPRESSION;
                                             GRNInteractionEdge e = new GRNInteractionEdge(outputnode, inputnode, type);

                                             if(!edges2.contains(e)){                        
                                                edges2.add(e); 
                                             }
                                        }
                                    }
                                }
                            }
                        }
                    }                  
                }
            }
        }
        
           return edges2;
    }

    /**
     * Sets, or replaces if already set, the child nodes of the current node with
     * those provided.
     * @param nodes 
     */
    @Override
    final public void setNodes(List<GRNTreeNode> nodes) {

        this.nodes = nodes;

        for(GRNTreeNode node:nodes) {
            node.setParent(this);

            if(this.nodeManager!=null) {
                node.setNodeManager(nodeManager);
                super.alert(node);
            }

        }

    }
 
    @Override
    public boolean addNode(GRNTreeNode node) throws Exception, IllegalArgumentException{
        
        /*
         *  ADD CODE for ensuring uniqueness within tree
         */
        
        node.setParent(this);
        boolean added = nodes.add(node);
        if(added && nodeManager != null) {
            node.setNodeManager(nodeManager);
            super.alert(node);
        }
        return added;
    }

    @Override
    public void addNode(int index, GRNTreeNode node) throws Exception {
        
        /*
         *  ADD CODE for ensuring uniqueness within tree
         */
        
        node.setParent(this);
        if(index >= nodes.size()) {
            nodes.add(node);
        } else
        {
            nodes.add(index, node);
        }

        if(nodeManager != null) {
            node.setNodeManager(nodeManager);
            super.alert(node);
        }
       
    }

    /**
     * Removes the provided node from the list of child nodes
     * @param node
     * @return 
     */
    @Override
    public boolean removeNode(GRNTreeNode node) {

        // If node is Transcription Unit remove all parts it contains

        if(node.isBranchNode())
        {
            /*
            List<GRNTreeNode> toRemove = new ArrayList<GRNTreeNode>();
            toRemove.addAll(node.getChildren());
            for(GRNTreeNode n:toRemove)
            {
                node.removeNode(n);
            }
            */
            boolean b = nodes.remove(node);
            if(nodeManager!=null){
                super.alert(node);
            }
            return b;
        }

        // Otherwise move all child nodes up to removed node's parent

        else
        {
           if(nodes.contains(node))
           {
               for(GRNTreeNode n:node.getChildren())
               {
                   try {
                       addNode(n);
                   } catch (Exception ex) {
                       Logger.getLogger(BranchNode.class.getName()).log(Level.SEVERE, null, ex);
                   }
               }
           }
           
           boolean b = nodes.remove(node);
           if(nodeManager!=null){
               super.alert(node);
           }

           return b;
        }     
    }


    @Override
    public boolean removeNodeAndDescendents(GRNTreeNode node) {
        boolean b =  nodes.remove(node);
        if(b && nodeManager != null) {
            super.alert(node);
        }
        return b;
    }

    /**
     * 
     * @return 
     */
    @Override
    public String debugInfo() {
        StringBuilder sb = new StringBuilder();       
        ListIterator<GRNTreeNode> it = nodes.listIterator();
        
        sb.append("[Nodes(").append(nodes.size()).append("):");
        
        while(it.hasNext())
        {            
            GRNTreeNode n = it.next();
            if(n instanceof BranchNode){
                sb.append("\n\n[I/f:").append(n.getInterfaceType()).append(" TU: ").append(n.isTranscriptionUnit()).append(" | Parent: ").append(n.getParent().getName()).append(" | Content:\n").append(n.debugInfo()).append("]");
            }
            else {
                sb.append("\n").append(n.debugInfo());
            }  
        }

        /*
        if(edges.size()>0)
        {
            ListIterator<GRNEdge> eit = this.getEdges().listIterator();
            sb.append("\nEdges in ").append(this.getName()).append(":");

            while(eit.hasNext())
            {
                GRNEdge e = eit.next();
                sb.append("\n    -").append(e);
            }
        }*/
        
        sb.append("]");

        return sb.toString();
    }
    
    /**
     * Returns a string description of 
     * @return 
     */
    @Override
    public String toString() {     
        StringBuilder sb = new StringBuilder();       
        ListIterator<GRNTreeNode> it = nodes.listIterator();

        while(it.hasNext())
        {            
            GRNTreeNode n = it.next();
            
            sb.append(n);
            
            if(n instanceof LeafNode)
            {
                sb.append("; ");
            }
        }
        
        return sb.toString();
    }
    
    @Override
    public boolean isTranscriptionUnit() {
       
        if(nodes.isEmpty())
        {
            return false;
        }

       ListIterator<GRNTreeNode> it = nodes.listIterator();
       while(it.hasNext())
       {
           GRNTreeNode n = it.next();
           if(n instanceof BranchNode)
           {
               return false;
           }
       }
       return true;
    }

    @Override
    public boolean isRegulatedTranscriptionUnit() {
        
        if(nodes.isEmpty())
        {
            return false;
        }
        
       boolean isRegulated = false;
       ListIterator<GRNTreeNode> it = nodes.listIterator();
       while(it.hasNext())
       {
           GRNTreeNode n = it.next();
           if(n instanceof BranchNode)
           {
               return false;
           }
           else if(n instanceof LeafNode)
           {
               LeafNode ln = (LeafNode) n;
               if(ln.getType() == SVPType.Op)
               {
                   isRegulated = true;
               }
               else if(ln.getType() == SVPType.Prom)
               {
                   isRegulated = m.isRegulatedPromoter(ln.getSVP());
               }
           }
       }
       return isRegulated;
    }
   
    @Override
    public List<GRNTreeNode> getAllDescendantTUs() {

        List<GRNTreeNode> tus = new ArrayList<GRNTreeNode>();

        if(this.isTranscriptionUnit())
        {
            tus.add(this);
            return tus;
        }
        else
        {
            for(GRNTreeNode n:nodes)
            {
                tus.addAll(n.getAllDescendantTUs());
            }
            return tus;
        }
    }
    
    @Override
    public List<GRNTreeNode> getBounds(GRNTreeNode part) throws Exception{
        
        GRNTreeNode transcriptionUnit = this;
        
        List<GRNTreeNode> bounds = new ArrayList<GRNTreeNode>();
        
        // Supplied node must be a Transcription Unit
  
        if(!(part instanceof LeafNode))
        {
            throw new Exception("part must be a LeafNode");
        }
        
        if(!transcriptionUnit.isTranscriptionUnit())
        {
            throw new Exception("Method must be called on a Transcriptional Unit");
        }

        // If TU is root node, then the bounds contains only the root node.
        
        if(transcriptionUnit.getParent() == null){
            bounds.add(transcriptionUnit);
            return bounds;
        }
        
        // Set the IF type that interacts with the supplied leaf node
  
        List<InterfaceType> targetIF = new ArrayList<InterfaceType>();
        
        if(part.getInterfaceType().equals(InterfaceType.INPUT)){
            targetIF.add(InterfaceType.OUTPUT);
        }
        else if (part.getInterfaceType().equals(InterfaceType.OUTPUT)){
            targetIF.add(InterfaceType.INPUT);
            targetIF.add(InterfaceType.OUTPUT);
        }
        
        // Only nodes with Input and Output types can have interactionDocuments, and therefore bounds
        
        else
        {
            return bounds;
        }
        
        List<GRNTreeNode> explored = new ArrayList<GRNTreeNode>();
        GRNTreeNode previous = null;
        
        // Trace a path up identical InterfaceTypes (i.e. output nodes propagate lower output nodes)
        
        while((explored.isEmpty() || previous.getInterfaceType().equals(part.getInterfaceType())) && transcriptionUnit.getParent()!=null)
        {
            previous = transcriptionUnit;
            transcriptionUnit = transcriptionUnit.getParent();
            
            for(GRNTreeNode n:transcriptionUnit.getChildren()){
                if(!explored.contains(n)){
                    
                    // If we are in the parent of the initial TU, and the TU has siblings, add all to bounds
                    
                    if(explored.isEmpty() && n.isTranscriptionUnit()){
                        bounds.add(n);
                    }
                    
                    // Otherwise, trace a path down descendants with the target IF type and add TUs to bounds, if found
                    
                    else{
                        for(InterfaceType i:targetIF){
                            if(i.equals(InterfaceType.INPUT)){
                                for(GRNTreeNode x: n.getInputNodes())
                                {
                                    if(!bounds.contains(x.getParent()))
                                        bounds.add(x.getParent()); 
                                }
                                 
                            }
                            if(i.equals(InterfaceType.OUTPUT)){
                                for(GRNTreeNode x: n.getOutputNodes())
                                {
                                    if(!bounds.contains(x.getParent()))
                                        bounds.add(x.getParent()); 
                                }
                            }
                        }
                    }
                }
            }
            
            // add the current TU to the explored list, so it won't be revisted
            
            explored.add(transcriptionUnit);
        }
        
        return bounds;
    }

    // TODO separate out validation from data structure
    // TODO does this even work?

    @Override
    public boolean isValidLocation(GRNTreeNode part) throws Exception{    
        GRNTreeNode node = this;
        List<Part> svps = new ArrayList<Part>();
        
        if(part instanceof LeafNode)
        {
            if(!node.isTranscriptionUnit())
            {
                throw new Exception("LeafNode can only be added to TranscriptionUnits");
            }
            
            LeafNode ln = (LeafNode) part;
            svps.add(ln.getSVP());
            
        }
        else if ((part instanceof BranchNode))
        {
            
             if(!part.isTranscriptionUnit())
             {
                 throw new Exception("Supplied node must be LeafNode or TU");
             }
             
             for(GRNTreeNode n:part.getChildren())
             {
                 if(n instanceof LeafNode)
                 {
                     LeafNode ln = (LeafNode) n;
                     if(ln.getType() == SVPType.Prom ||
                             ln.getType() == SVPType.Op ||
                             ln.getType() == SVPType.CDS)
                     {
                         svps.add(ln.getSVP());
                     }
                 }
             }
        }

        // TODO Does this work?

        List<GRNTreeNode> boundsCompliment = getBounds(part);
        GRNTreeNode rootNode = this;
        while(rootNode.getParent()!=null)
        {
            rootNode = rootNode.getParent();
        }
  
        boundsCompliment.removeAll(rootNode.getAllDescendantTUs());
        
        for(GRNTreeNode n:boundsCompliment)
        {
            for(Part p:n.getDescendents())
            {
                
                for(Part svp:svps)
                {
                    if(m.hasInteraction(p, svp))
                    {
                        return false;
                    }
                }
                
            }
        }
        
        return true;
    }

    @Override
    public boolean isBranchNode() {
        return true;
    }

    @Override
    public int getHeight() {
        int bestHeight = 0;
        for(GRNTreeNode node : nodes) {
            if(node.getHeight() > bestHeight) bestHeight = (node.getHeight() +1);
        }
        return bestHeight;
    }
    
    
}