/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.grntree.api;

import uk.ac.ncl.icos.grntree.impl.InteractionType;

/**
 *
 * @author owengilfellon
 */
public interface GRNEdge {
    
    public GRNTreeNode getFrom();
    public GRNTreeNode getTo();
    
    public InteractionType getInteractionType();
    
}
