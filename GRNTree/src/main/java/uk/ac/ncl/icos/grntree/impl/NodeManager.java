package uk.ac.ncl.icos.grntree.impl;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import uk.ac.ncl.icos.svpmanager.SVPManager;
import uk.ac.ncl.icos.grntree.api.GRNTreeNode;
import uk.ac.ncl.icos.grntree.api.ModelObserver;
import uk.ac.ncl.icos.grntree.api.ModelSubject;
import uk.ac.ncl.intbio.virtualparts.entity.Interaction;
import uk.ac.ncl.intbio.virtualparts.entity.Part;

import java.io.Serializable;
import java.util.*;
import uk.ac.ncl.icos.svpcompiler.Compilable.SignalType;

/**
 * Created by owengilfellon on 27/02/2014.
 */
public class NodeManager implements ModelSubject, Serializable {

    private final transient SVPManager m = SVPManager.getSVPManager();
    private final Map<NodeWrapper, NodeRecord> nodeRecords;
    private final transient List<ModelObserver> observers = new ArrayList<>();
    private boolean detectInteractions = false;

    public NodeManager()
    {
        nodeRecords = new HashMap<>();
    }

    /**
     * Returns true if the part represented by the leaf node is in the tree
     * @param node
     * @return 
     */
    public boolean containsNode(LeafNode node)
    {
        return nodeRecords.containsKey(new NodeWrapper(node));
    }

    /**
     * Returns the branch nodes that contain the given leaf node
     * @param node
     * @return 
     */
    public List<GRNTreeNode> getParents(LeafNode node)
    {
        NodeWrapper nw = new NodeWrapper(node);
        if(!nodeRecords.containsKey(nw))
            return new ArrayList<>();        
        List<GRNTreeNode> nodes = new ArrayList<>();
        nodes.addAll(nodeRecords.get(nw).parents);
        return nodes;
    }

    public void setDetectInteractions(boolean detectInteractions) {
        this.detectInteractions = detectInteractions;
        for(NodeWrapper node : nodeRecords.keySet()) {
            detectInteractions(node);
        }
    }

    public boolean isDetectingInteractions() {
        return detectInteractions;
    }


    /**
     * Returns the total number of leaf nodes (parts) in the tree
     * @return 
     */
    public int getSize() {
        int size = 0;
        for(NodeWrapper nw : nodeRecords.keySet()) {
            size += nodeRecords.get(nw).parents.size();
        }
        return size;
    }

    public int getInstancesSize(LeafNode node)
    {
        NodeWrapper nw = new NodeWrapper(node);
        if(!nodeRecords.containsKey(nw))
            return 0;
        
        return nodeRecords.get(nw).parents.size();
    }

    public int getInteractionsSize(LeafNode node)
    {
        NodeWrapper nw = new NodeWrapper(node);
        if(!nodeRecords.containsKey(nw))
            return 0;
        
        return nodeRecords.get(nw).interactions.size();
    }

    public boolean addInteraction(Interaction interaction)
    {
        List<LeafNode> parts = new ArrayList<>();
        List<String> ids = interaction.getParts();

        if(ids != null) {
            for(NodeWrapper nw : nodeRecords.keySet()) {
                if(ids.contains(nw.node.getSVP().getName()))
                    parts.add(nw.node);
            }

            boolean allAdded = true;

            for (LeafNode node : parts) {

                if (!containsNode(node))
                    return false;

                NodeWrapper nw = new NodeWrapper(node);
                InteractionRecord ir = new InteractionRecord(interaction);
                allAdded = nodeRecords.get(nw).interactions.add(ir) ? allAdded : false;
            }
        }

        return false;
    }

    /**
     * Returns all interactions within the tree
     * @return 
     */
    public Set<Interaction> getInteractions() {
        Set<Interaction> interactions = new HashSet<>();
        for(NodeWrapper nw : nodeRecords.keySet())  {
            Set<InteractionRecord> interactionRecords = nodeRecords.get(nw).interactions;
            for(InteractionRecord record : interactionRecords ) {
                interactions.add(record.interaction);
            }
        }
        return interactions;
    }

    /**
     * Returns the interactions within the tree involving the supplied leaf node.
     * @param node
     * @return 
     */
    public Set<Interaction> getInteractions(LeafNode node){
        Set<Interaction> interactions = new HashSet<>();
        Set<InteractionRecord> interactionRecords = nodeRecords.get(new NodeWrapper(node)).interactions;
        for(InteractionRecord ir:interactionRecords) {
            interactions.add(ir.interaction);
        }
        return interactions;
    }

    /**
     * Returns the parts in the tree associated with a given interaction
     * @param interaction
     * @return 
     */
    public List<GRNTreeNode> getNodes(Interaction interaction) {
        List<GRNTreeNode> parts = new ArrayList<>();
        InteractionRecord ir = new InteractionRecord(interaction);
        for(NodeWrapper nw : nodeRecords.keySet())  {
            if(nodeRecords.get(nw).interactions.contains(ir)) {
                parts.add(nw.node);
            }
        }
        return parts;
    }
    
    public List<GRNTreeNode> getNodes(String name) {
        List<GRNTreeNode> parts = new ArrayList<>();
        for(NodeWrapper nw : nodeRecords.keySet())  {
            if(nw.node.getSVP().getName().equals(name)) {
                for(GRNTreeNode n : nodeRecords.get(nw).parents) {
                    for(GRNTreeNode child : n.getChildren()) {
                        if(child.isLeafNode() && ((LeafNode)child).getSVP().getName().equalsIgnoreCase(name)) {
                            parts.add(child);
                        }
                    }
                }
            }
        }
        return parts;
    }
    
    /**
     * Returns the leaf nodes that interact with the given leaf node
     * @param node
     * @return 
     */
    public List<GRNTreeNode> getInteractingNodes(LeafNode node)
    {
        NodeWrapper nw = new NodeWrapper(node);

        List<GRNTreeNode> nodes = new ArrayList<>();
        if(!nodeRecords.containsKey(nw)) {
            return nodes;
        }

        Set<String> identifiers = new HashSet<>();
        Set<InteractionRecord> interactions = nodeRecords.get(nw).interactions;

        for(InteractionRecord interaction:interactions) {
            for(String partName:interaction.interaction.getParts()) {
                if (!partName.equals(node.getSVP().getName())) {
                    identifiers.add(partName);
                }
            }
        }

        for(NodeWrapper n:nodeRecords.keySet()) {
            if(identifiers.contains(n.node.getSVP().getName())) {
                nodes.add(n.node);
            }
        }

        return nodes;
    }
    
    /**
     * Notifies the node manager of an update to the tree
     * @param node 
     */
    public void alert(GRNTreeNode node)
    {
        if(node instanceof BranchNode) {
            this.alert((BranchNode) node);
        }
        else if (node instanceof LeafNode){
            this.alert((LeafNode) node);
        }
    }

    private void alert(BranchNode node)
    {
        List<LeafNode> leafNodes = new ArrayList<LeafNode>();

        for(GRNTreeNode child : node.getChildren()) {
            if(child.isLeafNode()) {
               // leafNodes.add((LeafNode) child);
                this.alert(child);
            } else if (child.isBranchNode()) {
                this.alert(child);
            }
        }
        
        /*
        for(GRNTreeNode tu:node.getAllDescendantTUs()) {
            List<GRNTreeNode> children = tu.getChildren();
            for(GRNTreeNode child:children) {
                if(child instanceof LeafNode) {
                    leafNodes.add((LeafNode) child);
                }
            }
        }

        if(!leafNodes.isEmpty()) {
            for(LeafNode leafNode:leafNodes) {
                this.alert(leafNode);
            }

        }*/


        alert();
    }

    private void alert(LeafNode node)
    {

        BranchNode parent = (BranchNode) node.getParent();
        NodeWrapper nw = new NodeWrapper(node);

        if(!parent.getChildren().contains(node)) {
            removeNode(nw);
        }
        else {
            addNode(nw);
        }

        alert();
    }

    private void removeNode(NodeWrapper node)
    {
        NodeRecord nr = nodeRecords.get(node);
        nr.parents.remove(node.node.getParent());

        if(nr.parents.isEmpty()) {

            // Remove interactionDocuments referring to this part from any interacting part

            if(!nr.interactions.isEmpty()) {

                for(InteractionRecord i:nr.interactions){

                    List<String> parts = i.interaction.getParts();

                    for(String p:parts) {

                        if(!p.equals(node.node.getSVP().getName())) {

                            // p == the id of the other part in interaction
                            // Find node that has this id


                            Iterator<NodeWrapper> node_it = nodeRecords.keySet().iterator();
                            NodeWrapper matchedNode = null;

                            while(node_it.hasNext() && matchedNode == null) {
                                NodeWrapper n = node_it.next();
                                if(n.node.getSVP().getName().equals(p)) {
                                    matchedNode = n;
                                }
                            }

                            // Retrieve associated record

                            NodeRecord nrToAmmend = nodeRecords.get(matchedNode);
                            Iterator<InteractionRecord> it = nrToAmmend.interactions.iterator();

                            // Find interaction of interest and remove;

                            InteractionRecord toRemove = null;
                            while(it.hasNext()) {
                                InteractionRecord ir = it.next();

                                if(ir.interaction.getParts().contains(node.node.getSVP().getName())) {
                                    toRemove = ir;
                                }
                            }

                            if(toRemove!=null) {
                                nrToAmmend.interactions.remove(toRemove);
                            }
                        }
                    }
                }


            }

            nodeRecords.remove(node);
        }
    }

    private void addNode(NodeWrapper node)
    {
        // If the node doesn't exist in records, add it

        if(!nodeRecords.containsKey(node)) {
           NodeRecord nr =  new NodeRecord();
           nodeRecords.put(node, nr);
        }

        nodeRecords.get(node).parents.add((BranchNode)node.node.getParent());
        
        if(detectInteractions) {
            detectInteractions(node);
        }
    }
    
    void detectInteractions(NodeWrapper node) {
        
        List<Interaction> allPartInteractions;
        
        // Retrieve any interactions in the repository (non-repository interactions must be added manually)

        
        
        if(node.node instanceof LeafNode &&
                !(node.node instanceof PrototypeNode)) {
            
            //List<SignalType> signals = ((LeafNode)node.node).getInputSignal();
            //signals.addAll(((LeafNode)node.node).getOutputSignal());
            
            //if(!signals.contains(SignalType.None)) {
                
                allPartInteractions = m.getInteractions(node.node.getSVP());

                // Loop through all interactionDocuments the part is involved in

                HashMap<String, Interaction> potentialInteractions = new HashMap<>();

                if(allPartInteractions!=null && allPartInteractions.size() > 0) {

                    // Add interactionDocuments to a temporary map, with other part's name as key

                    for(Interaction interaction:allPartInteractions) {
                        for(String partName:interaction.getParts()) {
                            if(!partName.equals(node.node.getSVP().getName())) {
                                potentialInteractions.put(partName, interaction);
                            }
                        }
                    }

                    // If a node exists in records that has interactionDocuments with the added part, add interaction under that node

                    for(NodeWrapper n:nodeRecords.keySet()) {
                        if(potentialInteractions.containsKey(n.node.getSVP().getName())) {
                            InteractionRecord ir = new InteractionRecord(potentialInteractions.get(n.node.getSVP().getName()));
                            if(!nodeRecords.get(n).interactions.contains(ir)) {
                                nodeRecords.get(n).interactions.add(ir);
                            }

                            if(!nodeRecords.get(node).interactions.contains(ir)) {
                                nodeRecords.get(node).interactions.add(ir);
                            }
                        }
                    }
                }
           // }
        } 
    }

    public void debug() {

        String format = "%-20s %-13s %-10s %n";
        String divider = "--------------------------------------------------";
        String subformat = "%-30s %30s %n";
        System.out.println(divider);
        System.out.printf(format, "Part", "Interactions", "Instances");
        System.out.println(divider);

        for(NodeWrapper n:nodeRecords.keySet())  {
            System.out.printf(format, n, nodeRecords.get(n).interactions.size(), nodeRecords.get(n).parents.size());
        }
    }

    @Override
    public void attach(ModelObserver o) {
        this.observers.add(o);
    }

    @Override
    public void dettach(ModelObserver d) {
        this.observers.remove(d);
    }

    @Override
    public void alert() {

        for(ModelObserver o:observers)
        {
            o.update(this);
        }
    }

    /*
        Groups together information about a node
     */

    @XStreamAlias("NodeRecord")
    private class NodeRecord implements Serializable {
        Set<InteractionRecord> interactions = new HashSet<>();
        List<BranchNode> parents = new ArrayList<>();
    }

    /*
        Wrapper class for Interaction to provide logical equivalency
     */

    @XStreamAlias("InteractionRecord")
    private class InteractionRecord implements Serializable {

        Interaction interaction = null;

        public InteractionRecord(Interaction interaction)
        {
            this.interaction = interaction;
        }

        @Override
        public boolean equals(Object obj) {
            if(obj == null)
                return false;
            if(obj == this)
                return true;
            if(!(obj instanceof InteractionRecord))
                return false;

            InteractionRecord ir = (InteractionRecord) obj;

            return ir.interaction.getName().equals(this.interaction.getName());
        }


        @Override
        public int hashCode() {
            return interaction.getName().hashCode() * interaction.getInteractionType().hashCode();
        }
    }

    @XStreamAlias("NodeWrapper")
    private class NodeWrapper implements Serializable {

        LeafNode node = null;

        public NodeWrapper(LeafNode node)
        {
            this.node = node;
        }

        @Override
        public boolean equals(Object obj)
        {
            if(obj == null)
                return false;
            if(obj == this)
                return true;
            if(!(obj instanceof NodeWrapper))
                return false;
            Part p = ((NodeWrapper) obj).node.getSVP();

            return p.getName().equals(node.getSVP().getName()) &&
                    p.getType().equals(node.getSVP().getType());
        }

        @Override
        public int hashCode() {
            return node.getSVP().getName().hashCode()
                    * node.getSVP().getType().hashCode();
        }

        @Override
        public String toString() {
            return node.toString();
        }
    }

}
