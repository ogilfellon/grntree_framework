/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.grntree.io.sbol2;

import java.net.URI;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.xml.namespace.QName;
import org.sbolstandard.core2.Annotation;
import org.sbolstandard.core2.ComponentDefinition;
import org.sbolstandard.core2.ComponentInstance;
import org.sbolstandard.core2.FunctionalComponent;
import org.sbolstandard.core2.GenericTopLevel;
import org.sbolstandard.core2.Identified;
import org.sbolstandard.core2.ModuleDefinition;
import org.sbolstandard.core2.Participation;
import org.sbolstandard.core2.SBOLDocument;
import uk.ac.ncl.icos.grntree.api.GRNTreeNode;
import uk.ac.ncl.icos.grntree.impl.SVPType;
import static uk.ac.ncl.intbio.core.datatree.Datatree.NamedProperty;
import uk.ac.ncl.intbio.core.datatree.Literal;
import uk.ac.ncl.intbio.core.datatree.NamedProperty;
import uk.ac.ncl.intbio.virtualparts.entity.Interaction;
import uk.ac.ncl.intbio.virtualparts.entity.InteractionPartDetail;
import uk.ac.ncl.intbio.virtualparts.entity.Parameter;
import uk.ac.ncl.intbio.virtualparts.entity.Part;
import uk.ac.ncl.intbio.virtualparts.entity.Property;

/**
 *
 * @author goksel
 */
public class SVPToSBOL
{

    public static void addSVPProperties(ComponentDefinition componentDef, Part part, SBOLDocument document, String version) throws SBOL2SerialisationException
    {
        if (part.getDescription() != null && part.getDescription().length() > 0) {
            componentDef.setDescription(part.getDescription());
        }
        
        // Add properties stored as fields in Part

        //Terms.componentRole.cds....

        componentDef.addRole(Mappings.toSO(part.getType()));
        
        SBOLHelper.addAnnotation(componentDef, Terms.vprPartTerms.designMethod, part.getDesignMethod());
        SBOLHelper.addAnnotation(componentDef, Terms.vprPartTerms.status, part.getStatus());
        SBOLHelper.addAnnotation(componentDef, Terms.vprPartTerms.organism, part.getOrganism());

        // Add properties stored in .getProperties() collection
        
        if (part.getProperties() != null) {
            addPartProperties(part, componentDef, document, version);
        }
    }

    private static void addPartProperties(Part part, ComponentDefinition componentDef, SBOLDocument document, String version) throws SBOL2SerialisationException
    {
        for (Property svpProperty : part.getProperties())
        {
            try
            {
                String name = svpProperty.getName();
                if (name.contains(" "))
                {
                    name = name.replace(" ", "_");
                }
                
                // Create annotation for the property

                componentDef.createAnnotation(Terms.vpr.withLocalPart(name), svpProperty.getValue());
                
                // If property has a description, create a top level object
                
                if (svpProperty.getDescription() != null && svpProperty.getDescription().length() > 0)
                {                
                    URI topLevelURI = null; 

                    // TODO: Shouldn't be hardcoding URIs....
                    
                    topLevelURI = URI.create("http://www.synbad.org/gen/" +
                            SBOLHelper.getValidDisplayId( part.getName() + svpProperty.getName())+ "/" + version);

                    // If property has not been added

                    if (document.getGenericTopLevel(topLevelURI) == null)
                    {                        
                        GenericTopLevel topLevel = document.createGenericTopLevel(
                                SBOLHelper.getValidDisplayId(part.getName() + svpProperty.getName()),
                                version, 
                                Terms.rdfTerms.Property);
                        
                        // Add the description as an annotation to the top level object
                                              
                        topLevel.createAnnotation(Terms.rdfsTerms.label, svpProperty.getName());
                        topLevel.createAnnotation(Terms.rdfsTerms.comment, svpProperty.getDescription());
                    }
                }
            }
            catch (Exception ex)
            {
                throw new SBOL2SerialisationException("Could not add the part properties for part " + part.getName() + ". Parameter:" + svpProperty.getName(), ex);
            }
        }
    }

    public static void addInteractionModuleDefinition(SBOLDocument document, String baseURL, String version, Interaction interaction, List<GRNTreeNode> parts)
    {
        // Create module definition to represent the interaction

        ModuleDefinition moduleDef = document.createModuleDefinition(
                SBOLHelper.getValidDisplayId(interaction.getName()),
                version);

        URI interactionURI = URIFactory.getInteractionURI(moduleDef, interaction.getName());

        Set<URI> interactionRoles = null;
        try
        {
            interactionRoles = new HashSet<>( Arrays.asList( Terms.vpr.namespacedUri( URLEncoder.encode(interaction.getInteractionType(), "UTF-8"))));
        }
        catch (Exception e)
        {
        }

        // sbolInteraction correctly contains IPD annotations, but not added to file?
        
        org.sbolstandard.core2.Interaction sbolInteraction = moduleDef.createInteraction(
                SBOLHelper.getValidDisplayId(interaction.getName()),
                interactionRoles);

        // For each of the parts listed in the interaction

        for (String part : interaction.getParts()) {
            
            InteractionPartDetail partDetail = getPartDetail(interaction, part);
            ComponentDefinition definition = null;
            
            for(GRNTreeNode node : parts) {
                if (node.getSVP().getName().equals(part)) {
                    definition = SBOLHelper.getComponetDefinition(document, node);
                }
            }
            
            assert(definition != null);
            
            
            if (partDetail != null) { 
                addParticipation(sbolInteraction, partDetail, moduleDef, definition);
            } else {
                FunctionalComponent functionalComponent = getFunctionalComponent(
                    moduleDef,
                    definition.getDisplayId(),
                    definition.getDisplayId(),
                    definition.getVersion());
                
                
                sbolInteraction.createParticipation(
                        SBOLHelper.getValidDisplayId(part + "_in_" + interaction.getName()),
                            functionalComponent.getIdentity());
            }
        }
        
        addInteractionProperties(interaction, sbolInteraction);
        
        // Add parameters to interaction as annotations
        
        for (Parameter parameter : interaction.getParameters()) {
            addAnnotation(sbolInteraction, Terms.vprInteractionTerms.hasParameter, parameter);
        }
    }

    private static void addInteractionProperties(Interaction interaction, org.sbolstandard.core2.Interaction sbolInteraction)
    {
        SBOLHelper.addAnnotation(sbolInteraction, Terms.documentedTerms.description, interaction.getDescription());
        SBOLHelper.addAnnotation(sbolInteraction, Terms.vprInteractionTerms.freeTextMath, interaction.getFreeTextMath());
        SBOLHelper.addAnnotation(sbolInteraction, Terms.vprInteractionTerms.isReaction, interaction.getIsReaction().toString());
        SBOLHelper.addAnnotation(sbolInteraction, Terms.vprInteractionTerms.isReversible, interaction.getIsReversible().toString());
        SBOLHelper.addAnnotation(sbolInteraction, Terms.vprInteractionTerms.mathName, interaction.getMathName());
        SBOLHelper.addAnnotation(sbolInteraction, Terms.vprInteractionTerms.isInternal, interaction.getIsInternal().toString());
    }

    private static void addAnnotation(Identified identified, QName property, Parameter parameter)
    {

        List<NamedProperty<QName>> namedPropertyList = new ArrayList<>();
        if (parameter.getParameterType() != null && parameter.getParameterType().length() > 0)
        {
            namedPropertyList.add(NamedProperty(Terms.vprInteractionTerms.parameterTerms.parameterType, parameter.getParameterType()));
        }
        if (parameter.getScope() != null && parameter.getScope().length() > 0)
        {
            namedPropertyList.add(NamedProperty(Terms.vprInteractionTerms.parameterTerms.scope, parameter.getScope()));
        }
        if (parameter.getValue() != null)
        {
            namedPropertyList.add(NamedProperty(Terms.vprInteractionTerms.parameterTerms.value, parameter.getValue().toString()));
        }
        if (parameter.getName() != null)
        {
            namedPropertyList.add(NamedProperty(Terms.documentedTerms.name, parameter.getName()));
        }
        
        List<Annotation> annotations=new ArrayList<Annotation>();
        for (NamedProperty<QName> namedProperty:namedPropertyList)
        {
            if (namedProperty.getValue() instanceof Literal.StringLiteral)
            {
                String value = ((Literal.StringLiteral<QName>) namedProperty.getValue()).getValue();   
                Annotation annotation=new Annotation(namedProperty.getName(), value);
                annotations.add(annotation);
            }
            else if (namedProperty.getValue() instanceof Literal.IntegerLiteral)
            {
                int value = ((Literal.IntegerLiteral<QName>) namedProperty.getValue()).getValue();   
                Annotation annotation=new Annotation(namedProperty.getName(), String.valueOf(value));
                annotations.add(annotation);
            } 
        }

        identified.createAnnotation(
            property,
                Terms.vprInteractionTerms.parameterTerms.Parameter,
                    URI.create(identified.getPersistentIdentity() + "_" + parameter.getName()),
                    annotations
                );
    }

    private static Participation addParticipation(org.sbolstandard.core2.Interaction interaction, InteractionPartDetail partDetail, ModuleDefinition moduleDef, ComponentDefinition componentDef)
    {

        // Create a functional component based on an InteractionPartDetail
        
        FunctionalComponent comp = getFunctionalComponent(
                moduleDef,
                partDetail.getPartName() + "_" + partDetail.getPartForm(),
                componentDef.getDisplayId(),
                componentDef.getVersion()
                );

        Set<URI> participationRoles = new HashSet<>(Arrays.asList(Terms.vpr.namespacedUri(partDetail.getInteractionRole())));
   
        // Create a participation object for the given interaction

        Participation participation = interaction.createParticipation(
                SBOLHelper.getValidDisplayId(partDetail.getPartName() + "_" + partDetail.getPartForm()),
                comp.getIdentity());
        
        // Set the role of the entity described in InteractionPartDetail
        
        participation.setRoles(participationRoles);

        // Add custom data to the participation using annotations
        
        if (partDetail != null) {
            SBOLHelper.addAnnotation(participation, Terms.vprInteractionTerms.mathName, partDetail.getMathName());
            SBOLHelper.addAnnotation(participation, Terms.vprInteractionTerms.numberOfMolecules, partDetail.getNumberOfMolecules());
            SBOLHelper.addAnnotation(participation, Terms.vprInteractionTerms.partForm, partDetail.getPartForm());
        }
        
        return participation;
    }

    /**
     * Adds a functional component to the provided module based on the provided part name
     * @param moduleDef The module to which the functional component will be added
     * @param baseURL The base URL for the component definition
     * @param name The name of the functional component to be created
     * @param partName The part name for the component definition
     * @return The created functional component
     */
    private static FunctionalComponent getFunctionalComponent(ModuleDefinition moduleDef, String name, String definition, String version)
    {
        
        FunctionalComponent comp = moduleDef.createFunctionalComponent(
                SBOLHelper.getValidDisplayId(name),
                ComponentInstance.AccessType.PUBLIC,
                definition, 
                version,
                FunctionalComponent.DirectionType.INOUT);
        return comp;
    }

    private static InteractionPartDetail getPartDetail(Interaction interaction, String part)
    {
        for (InteractionPartDetail partDetail : interaction.getPartDetails()) {
            if (partDetail.getPartName().equals(part)) {
                return partDetail;
            }
        }
        return null;
    }
}
