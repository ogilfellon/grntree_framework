package uk.ac.ncl.icos.grntree.impl;

import uk.ac.ncl.icos.grntree.api.GRNTreeNode;
import uk.ac.ncl.intbio.virtualparts.SBML.SBMLHandler;
import uk.ac.ncl.intbio.virtualparts.entity.Interaction;
import uk.ac.ncl.intbio.virtualparts.entity.Part;

import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import org.sbml.jsbml.SBMLDocument;

/**
 * Created by owengilfellon on 19/01/2015.
 */
final public class PrototypeNode extends LeafNode {

    private List<Interaction> internalEvents = new ArrayList<Interaction>();

    protected PrototypeNode(Part svp, InterfaceType interfaceType)
    {
        super(svp, interfaceType);
        this.setSVP(svp);
        setName(svp.getName());
    }

    protected PrototypeNode(Part svp, List<Interaction> internalEvents, InterfaceType interfaceType)
    {
        super(svp, interfaceType);
        this.internalEvents = internalEvents;
        m.createPartDocument(svp, this.internalEvents);
    }

    @Override
    public SBMLDocument getDocument() {
        return document;
    }

    public void setSVP(Part svp) {
        setSVP(svp, internalEvents);
    }
    
    
    
    

    public List<Interaction> getInternalEvents()
    {
        if(internalEvents == null) {
            return internalEvents;
        } else {
            return new ArrayList<>();
        }
        
    }

    public boolean setInternalEvents(List<Interaction> interactions)
    {
        this.clearInternalEvents();
        boolean b = this.addInternalEvents(interactions);
        this.document = m.createPartDocument(svp, this.internalEvents);
        return b;
    }

    public boolean addInternalEvents(List<Interaction> interactions)
    {
        return this.internalEvents.addAll(interactions);
    }

    public boolean removeInternalEvents(List<Interaction> interactions)
    {
        return internalEvents.removeAll(interactions);
    }

    public void clearInternalEvents()
    {
        this.internalEvents.clear();
    }

    @Override
    public GRNTreeNode duplicate()
    {
        return new PrototypeNode(this.getSVP(), this.internalEvents, interfaceType);
    }

    @Override
    public boolean isPrototype() {
        return true;
    }

    private void writeObject(ObjectOutputStream out) throws Exception
    {
        out.defaultWriteObject();
        SBMLHandler handler = new SBMLHandler();
        if(hasDocument) {
            String sbml = handler.GetSBML(this.document);
            out.writeObject(sbml);
        }
    }

    private void readObject(ObjectInputStream in) throws IOException, XMLStreamException, ClassNotFoundException
    {
        in.defaultReadObject();
        SBMLHandler handler = new SBMLHandler();
        if(hasDocument) {
            String sbml = (String) in.readObject();
            this.document = handler.GetSBML(sbml);
        }
    }
}
