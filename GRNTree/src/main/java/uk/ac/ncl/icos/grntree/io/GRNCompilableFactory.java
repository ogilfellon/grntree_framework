package uk.ac.ncl.icos.grntree.io;

import org.sbml.jsbml.SBMLDocument;
import uk.ac.ncl.icos.svpcompiler.Compilable.Compilable;
import uk.ac.ncl.icos.svpcompiler.Compilable.ICompilable;
import uk.ac.ncl.icos.svpcompiler.Compiler.CompilableFactory;
import uk.ac.ncl.icos.svpmanager.SVPManager;
import uk.ac.ncl.icos.grntree.api.GRNTree;
import uk.ac.ncl.icos.grntree.api.GRNTreeNode;
import uk.ac.ncl.icos.grntree.impl.LeafNode;
import uk.ac.ncl.intbio.virtualparts.entity.Interaction;
import uk.ac.ncl.intbio.virtualparts.entity.Part;

import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.util.*;
import uk.ac.ncl.icos.grntree.impl.SVPType;
import uk.ac.ncl.icos.svpcompiler.Compilable.SignalType;

/**
 * Created by owengilfellon on 29/09/2014.
 */
public class GRNCompilableFactory extends CompilableFactory {


    public static List<ICompilable> getCompilables(GRNTree tree) throws IOException, XMLStreamException {


        SVPManager m = SVPManager.getSVPManager();

        List<ICompilable> compilables = new  ArrayList<>();

        // Retrieve list of all compilable parts

        List<LeafNode> parts = new ArrayList<>();
        Iterator<GRNTreeNode> i = tree.getPreOrderIterator();
        while(i.hasNext()) {
            GRNTreeNode treeNode = i.next();
            if(treeNode.isLeafNode()) {
                parts.add((LeafNode)treeNode);
            }
        }

        for(LeafNode n : parts)
        {

            // Retrieve document from node

            System.out.println(n.getType());
            
            if(n.hasDocument() &&  (!n.getOutputSignal().contains(SignalType.None) ||  n.getType().equals(SVPType.CDS) ))
            {
                SBMLDocument svpDocument = n.getDocument();
                Map<Interaction, SBMLDocument> interactionDocuments = new HashMap<>();
                Set<Interaction> interactions = tree.getInteractions(n);

                // Create interaction models

                for(Interaction interaction:interactions) {

                    List<Part> interactionParts = new ArrayList<>();
                    for(GRNTreeNode ln :tree.getParts(interaction)) {
                        interactionParts.add(((LeafNode)ln).getSVP());
                    }

                    if(interactionParts.size() > 0) {
                        try {
                            // create interactions?
                            interactionDocuments.put(interaction, m.getInteractionDocument(interaction));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
      
                Compilable c = (!interactionDocuments.isEmpty()) ?
                        new Compilable(n.getSVP(), n.getInternalEvents(), svpDocument, interactionDocuments) :
                        new Compilable(n.getSVP(), n.getInternalEvents(), svpDocument);

                if(c.getPart().getType().equals("RBS")) {
                    Part mrnaPart = m.getPart("mRNA");
                    SBMLDocument mrnaDocument = m.getPartDocument(mrnaPart);
                    
                    compilables.add(new Compilable(mrnaPart,
                            m.getInternalEvents(mrnaPart),
                            mrnaDocument));
                }

                compilables.add(c);
            } else if (n.getType().equals(SVPType.Ter)){
                compilables.add(new Compilable(n.getSVP(),
                            n.getInternalEvents(),
                            null));
            }
        }

        return compilables;
    }
}
