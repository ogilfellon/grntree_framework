package uk.ac.ncl.icos.grntree.io;

import com.thoughtworks.xstream.XStream;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import uk.ac.ncl.icos.grntree.api.GRNEdge;
import uk.ac.ncl.icos.grntree.api.GRNTree;
import uk.ac.ncl.icos.grntree.api.GRNTreeNode;
import uk.ac.ncl.icos.grntree.api.NetworkSerialiser;
import uk.ac.ncl.icos.grntree.impl.AbstractNode;
import uk.ac.ncl.icos.grntree.impl.BranchNode;
import uk.ac.ncl.icos.grntree.impl.InterfaceType;
import uk.ac.ncl.icos.grntree.impl.LeafNode;
import uk.ac.ncl.intbio.virtualparts.entity.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringWriter;

/**
 * Created by owengilfellon on 19/11/2014.
 */
public class XMLNetworkSerialiser implements NetworkSerialiser<String> {

    XStream xstream;

    public XMLNetworkSerialiser() {
        xstream = new XStream();

        xstream.alias("Part", Part.class);
        xstream.aliasField("Name", Part.class, "name");
        xstream.aliasField("Type", Part.class, "type");
        xstream.aliasField("Description", Part.class, "description");
        xstream.aliasField("MetaType", Part.class, "metaType");
        xstream.aliasField("Sequence", Part.class, "sequence");
        xstream.aliasField("SequenceURI", Part.class, "sequenceURI");
        xstream.aliasField("DisplayName", Part.class, "displayName");
        xstream.aliasField("Organism", Part.class, "organism");
        xstream.aliasField("Status", Part.class, "status");
        xstream.aliasField("DesignMethod", Part.class, "designMethod");

        xstream.addImplicitCollection(Part.class, "properties",Property.class);

        xstream.alias("Property", Property.class);
        xstream.aliasField("Name", Property.class, "name");
        xstream.aliasField("Value", Property.class, "value");
        xstream.aliasField("Description", Property.class, "description");


        //SerializableParts definitions
        xstream.addImplicitCollection(Parts.class, "parts", Part.class);
        xstream.alias("Parts", Parts.class);

        xstream.alias("GRNTree", GRNTree.class);

        xstream.alias("AbstractNode", AbstractNode.class);
        xstream.alias("BranchNode", BranchNode.class);
        xstream.alias("LeafNode", LeafNode.class);
        xstream.alias("InterfaceType", InterfaceType.class);
        xstream.aliasField("Interface", AbstractNode.class, "interfaceType");
        xstream.aliasField("Parent", AbstractNode.class, "parent");
        xstream.aliasField("Name", AbstractNode.class, "name");
        xstream.aliasField("ID", AbstractNode.class, "id");
        xstream.aliasField("Part", LeafNode.class, "svp");
        xstream.aliasField("PartID", LeafNode.class, "partId");

        xstream.addImplicitCollection(BranchNode.class, "nodes", GRNTreeNode.class);
        xstream.addImplicitCollection(BranchNode.class, "edges", GRNEdge.class);

        xstream.alias("Interaction", Interaction.class);
        xstream.aliasField("InteractionName", Interaction.class, "name");
        xstream.aliasField("InteractionDescription", Interaction.class, "description");
        xstream.aliasField("InteractionType", Interaction.class, "interactionType");
        xstream.aliasField("MathName", Interaction.class, "mathName");
        xstream.aliasField("FreeTextMath", Interaction.class, "freeTextMath");
        xstream.aliasField("IsReaction", Interaction.class, "isReaction");
        xstream.aliasField("IsInternal", Interaction.class, "isInternal");
        xstream.aliasField("IsReversible", Interaction.class, "isReversible");

        //String[] Part definitions
        xstream.addImplicitCollection(Interaction.class, "parts", "Part", String.class);

        //Interactions definitions
        //Interactions colelction is named as "Interactions" in the xml
        xstream.alias("Interactions", Interactions.class);
        //Interactions contains a filed named interactionDocuments which is a list of Interaction class
//        xstream.addImplicitCollection(Interactions.class, "interactionDocuments", Interaction.class);

        xstream.alias("PartDetail", InteractionPartDetail.class);
        xstream.aliasField("PartDetails", Interaction.class, "partDetails");
        xstream.aliasField("NameInMath", InteractionPartDetail.class, "mathName");
        xstream.aliasField("IPDPartName", InteractionPartDetail.class, "partName");
        xstream.aliasField("MoleculerFormType", InteractionPartDetail.class, "partForm");
        xstream.aliasField("InteractionRole", InteractionPartDetail.class, "interactionRole");
        xstream.aliasField("Stoichiometry", InteractionPartDetail.class, "numberOfMolecules");

        xstream.alias("Parameter", Parameter.class);
        xstream.aliasField("Parameters", Interaction.class, "parameters");
        xstream.aliasField("Scope", Parameter.class, "scope");
        xstream.aliasField("Name", Parameter.class, "name");
        xstream.aliasField("ParameterType", Parameter.class, "parameterType");
        xstream.aliasField("Value", Parameter.class, "value");
        xstream.aliasField("EvidenceType", Parameter.class, "evidenceType");
        xstream.aliasField("EvidenceType", Parameter.class, "evidenceType");
    }

    @Override
    public GRNTree importNetwork(String toImport) {
        xstream.autodetectAnnotations(true);
        GRNTree tree = (GRNTree)xstream.fromXML(toImport);
        return tree;
    }

    @Override
    public String exportNetwork(GRNTree toExport) {
        xstream.autodetectAnnotations(true);
        return xstream.toXML(toExport);
    }

    public Document StringToXML(String string) {

        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = null;
            dBuilder = dbFactory.newDocumentBuilder();
            Document document = dBuilder.parse(new InputSource(new ByteArrayInputStream(string.getBytes())));
            //document.getDocumentElement().normalize();
            return document;
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public String XMLtoString(Document document) {
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = null;
        try {
            transformer = tf.newTransformer();
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        }
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        StringWriter writer = new StringWriter();
        try {
            transformer.transform(new DOMSource(document), new StreamResult(writer));
        } catch (TransformerException e) {
            e.printStackTrace();
        }
        return (writer.getBuffer().toString().replaceAll("\n|\r", ""));
    }

}
