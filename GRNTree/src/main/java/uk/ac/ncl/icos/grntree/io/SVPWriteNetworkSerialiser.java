package uk.ac.ncl.icos.grntree.io;


import uk.ac.ncl.icos.grntree.api.GRNTree;
import uk.ac.ncl.icos.grntree.api.NetworkSerialiser;

/**
 * Given a GRNTree, returns an SVPWrite string describing the model that the tree represents.
 * @author owengilfellon
 */
public class SVPWriteNetworkSerialiser implements NetworkSerialiser<String> {

    @Override
    public GRNTree importNetwork(String toImport) {
        return null;
    }

    /**
     * The toString() method of GRNTree is used to retrieve an SVPWrite String. The toString() method on GRNTree calls
     * the toString() method on it's root node, which recursively calls toString() on it's children. The IDs of any
     * parts within the tree are concatenated with part types, and separators (";").
     * @param toExport A GRNTree
     * @return A compilable SVPWrite string
     */
    public String exportNetwork(GRNTree toExport)
    {
        String s  = toExport.toString();
        return s.substring(0, s.length()-2);
    }
}
