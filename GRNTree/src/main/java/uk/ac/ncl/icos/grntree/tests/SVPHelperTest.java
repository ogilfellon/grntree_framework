package uk.ac.ncl.icos.grntree.tests;

import uk.ac.ncl.icos.svpcompiler.Compilable.ICompilable;
import uk.ac.ncl.icos.svpcompiler.Compiler.CompilationDirector;
import uk.ac.ncl.icos.svpmanager.MolecularForm;
import uk.ac.ncl.icos.svpmanager.RegulationRole;
import uk.ac.ncl.icos.grntree.api.GRNTree;
import uk.ac.ncl.icos.grntree.api.GRNTreeFactory;
import uk.ac.ncl.icos.grntree.api.GRNTreeNode;
import uk.ac.ncl.icos.grntree.api.SVPHelper;
import uk.ac.ncl.icos.grntree.impl.InterfaceType;
import uk.ac.ncl.icos.grntree.impl.PrototypeNode;
import uk.ac.ncl.icos.grntree.io.GRNCompilableFactory;

import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;
import uk.ac.ncl.icos.grntree.api.GRNTreeNodeFactory;

/**
 * Created by owengilfellon on 19/01/2015.
 */
public class SVPHelperTest {

    public static void main(String[] args)
    {
        createModel();
    }

    public static void createModel()
    {
        SVPHelper helper = SVPHelper.getSVPHelper();

        // Create a tree
        
        GRNTree tree = GRNTreeFactory.getGRNTree();
        
        // Add Transcription Units
        
        GRNTreeNode tu1 = GRNTreeNodeFactory.getBranchNode(InterfaceType.NONE);
        GRNTreeNode tu2 = GRNTreeNodeFactory.getBranchNode(InterfaceType.NONE);
        
        // Add TUs to Tree TODO: Should be able to add directly to the tree (assume root node)

        try {
            tree.getRootNode().addNode(tu1);
            tree.getRootNode().addNode(tu2);
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        // Create Parts

        List<GRNTreeNode> nodes = new ArrayList<>();
        nodes.add(helper.getPromoter("A", 0.138));
        nodes.add(helper.getRBS("B", 0.314));
        nodes.add(helper.getCDS("C", 0.52));
        nodes.add(helper.getRBS("D", 0.123));
        nodes.add(helper.getCDS("E", 0.345));
        nodes.add(helper.getTerminator("F"));

        List<GRNTreeNode> nodes2 = new ArrayList<>();
        nodes2.add(helper.getPromoter("G", 0.032));
        nodes2.add(helper.getRBS("H", 0.295));
        nodes2.add(helper.getCDS("I", 0.341));
        nodes2.add(helper.getTerminator("J"));
        
        // Add Parts to TUs

        tu1.setNodes(nodes);
        tu2.setNodes(nodes2);
        
        // Add Phosphorylation

        PrototypeNode donor = (PrototypeNode) tree.getRootNode().getChildren().get(0).getChildren().get(4);
        PrototypeNode acceptor = (PrototypeNode) tree.getRootNode().getChildren().get(0).getChildren().get(2);
        donor.addInternalEvents(helper.getDephosphorylationandDegradation(donor, 0.132, 0.114));
        acceptor.addInternalEvents(helper.getDephosphorylationandDegradation(acceptor, 0.132, 0.114));
        tree.addInteraction(helper.getPhosphorylationInteraction(donor, 0.132, 0.164, acceptor, 0.123, 0.156, 0.231));
        
        // Add promoter regulation
        
        PrototypeNode promoter = (PrototypeNode) tree.getRootNode().getChildren().get(1).getChildren().get(0);
        tree.addInteraction(helper.getRegulationInteraction(promoter, acceptor, MolecularForm.PHOSPHORYLATED, RegulationRole.ACTIVATOR, 0.042));

        // Compile tree to SBML

        GRNCompilableFactory factory = new GRNCompilableFactory();
        String sbmlOutput = "";
        try {
            List<ICompilable> compilables = GRNCompilableFactory.getCompilables(tree);
            factory.setCompilables(compilables);
            CompilationDirector director = new CompilationDirector(factory);
            sbmlOutput = director.getSBMLString("GRNTreeTest");
        } catch (Exception e) {
            e.printStackTrace();
        }

        try
        {
            FileWriter f = new FileWriter( "grntree_composition" + ".xml" );
            f.write( sbmlOutput );
            f.flush();
        } catch( Exception e ) {
            System.out.println( e.getMessage() );

        }

        System.out.println(tree.debugInfo());
    }
}
