/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.grntree.impl;

import uk.ac.ncl.icos.grntree.api.GRNEdge;
import uk.ac.ncl.icos.grntree.api.GRNTreeNode;

/**
 *
 * @author owengilfellon
 */
public class GRNInteractionEdge implements GRNEdge {
    
    protected GRNTreeNode from;
    protected GRNTreeNode to;     
    InteractionType interactionType;

    public GRNInteractionEdge(GRNTreeNode from, GRNTreeNode to, InteractionType type) {
        this.from = from;
        this.to = to;
        this.interactionType = type;
    } 

    @Override
    public GRNTreeNode getFrom() {
        return from;
    }

    @Override
    public GRNTreeNode getTo() {
        return to;
    }
    

    /**
     * Returns the type of interaction that the edge represents. Currently, ACTIVATION, REPRESSION and PHOSPHORYLATION are supported.
     * @return The Interaction Type.
     */
    public InteractionType getInteractionType() {
        return interactionType;
    }

    /**
     * 
     * @return 
     */
    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("[").append(from).append("]").append("->").append("[").append(to).append("] [Interaction type: ").append(interactionType).append("]");
        return sb.toString();
    }
    
    /*
     * Provides a logical equivalence test. Edges are considered identical if they have matching "To", "From" and "InteractionType" fields.
     * @param obj The object to be compared to this edge
     * @return true if logically equivalent, false if not.
     */
    /*
    @Override
    public boolean equals(Object obj)
    {
        if(obj == null)
            return false;
        if(obj == this)
            return true;
        if(!(obj instanceof GRNInteractionEdge))
            return false;
        
        GRNInteractionEdge edge = (GRNInteractionEdge) obj;
        if(edge.getFrom().equals(this.getFrom()) &&
                edge.getTo().equals(this.getTo()) &&
                edge.getInteractionType().equals(this.getInteractionType()))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    */
}
