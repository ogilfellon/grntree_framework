/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package misc;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.sbolstandard.core2.SBOLDocument;
import org.sbolstandard.core2.SBOLWriter;
import uk.ac.ncl.icos.grntree.api.GRNTree;
import uk.ac.ncl.icos.grntree.api.GRNTreeFactory;
import uk.ac.ncl.icos.grntree.io.sbol2.SBOL2NetworkSerialiser;

/**
 *
 * @author owengilfellon
 */
public class SVPWriteToSBOL {
   
    public static void main(String[] args) throws FileNotFoundException, IOException {
        
        File file = new File("/Users/owengilfellon/Desktop/SVPWrite.txt");
        FileReader reader = new FileReader(file);
        BufferedReader br = new BufferedReader(reader);
        String line;
        int generation = 0;
        while((line = br.readLine()) != null) {
            Pattern p = Pattern.compile("Model_\\d+\\s");
            Matcher m = p.matcher(line);
            GRNTree tree;
            
            if(m.find()) {
                SBOL2NetworkSerialiser serialiser = new SBOL2NetworkSerialiser();
                System.out.println(line.substring(m.start(), line.length()-1));
                tree = GRNTreeFactory.getGRNTree(line.substring(m.end(), line.length()));
                tree.setDetectInteractions(true);
                File f = new File("/Users/owengilfellon/Desktop/SBOL2/subtilinReceiver_linearity_generation" + generation + ".xml");
                
                SBOLDocument document = serialiser.exportNetwork(tree);
                SBOLWriter.write(document, f);
                System.out.println("\nModel written to " + f.getName());
                generation++;
            }
        }
    }
    
    
}
