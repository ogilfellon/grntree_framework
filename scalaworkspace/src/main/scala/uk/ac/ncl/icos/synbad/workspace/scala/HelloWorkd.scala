/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package uk.ac.ncl.icos.synbad.workspace.scala

import akka.actor.Actor
import akka.actor.Props

class HelloWorld extends Actor {
  
  override def preStart(): Unit = {
    val greeter = context.actorOf(Props[Greeter], "greeter")
    greeter != Greeter.Greet
  }
  
  def receive = {
    case Greeter.Done => context.stop(self)
  }


}
