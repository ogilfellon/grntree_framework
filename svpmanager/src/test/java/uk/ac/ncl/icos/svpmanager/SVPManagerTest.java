/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.svpmanager;

import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import uk.ac.ncl.intbio.virtualparts.entity.Interaction;
import uk.ac.ncl.intbio.virtualparts.entity.Part;

/**
 *
 * @author owengilfellon
 */
public class SVPManagerTest {
    
    public SVPManagerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getSVPManager method, of class SVPManager.
     */
    @Test
    public void testGetSVPManager() {
        SVPManager m = SVPManager.getSVPManager();
        List<Part>  parts = m.getInteractingParts(m.getPart("SpaR"), 1);
        for(Part p : parts) {
             List<Interaction> i = m.getInternalEvents(p);
        m.createPartDocument(p, i);
        }
       
        
        
        
    }

}
