package uk.ac.ncl.icos.svpmanager;

import uk.ac.ncl.intbio.virtualparts.PartsHandler;
import uk.ac.ncl.intbio.virtualparts.entity.Part;
import uk.ac.ncl.intbio.virtualparts.entity.Parts;
import uk.ac.ncl.intbio.virtualparts.entity.SearchEntity;
import uk.ac.ncl.intbio.virtualparts.entity.Summary;

import java.io.IOException;
import java.util.*;

/**
 * Created by owengilfellon on 07/07/2014.
 */
public class RepositoryManager {

    private final static Set<Part> currentParts = new HashSet<Part>();
    private SearchEntity currentSearchEntity = null;
    private List<PartsHandler> currentHandlers = new ArrayList<PartsHandler>();

    public RepositoryManager()
    {

    }

    public void setCurrentHandlers(List<PartsHandler> partHandlers)
    {
        currentHandlers.clear();
        currentParts.clear();

        for(PartsHandler handler : partHandlers) {
            currentHandlers.add(handler);
        }
    }

    public List<PartsHandler> getCurrentHandlers() {
        return currentHandlers;
    }

    public void setSearchEntity(SearchEntity entity)
    {
        currentParts.clear();
        this.currentSearchEntity = entity;
    }

    public SearchEntity getSearchEntity()
    {
        return this.currentSearchEntity;
    }


    public void removeFilter()
    {
        currentSearchEntity = null;
        currentParts.clear();
    }

    private Map<PartsHandler, Summary> getPartSummaries() throws IOException {

        Map<PartsHandler, Summary> summaries = new HashMap<PartsHandler, Summary>();

        for(PartsHandler partsHandler : currentHandlers) {
            if(currentSearchEntity !=null) {
                summaries.put(partsHandler, partsHandler.GetPartsSummary(currentSearchEntity));
            } else {
                summaries.put(partsHandler, partsHandler.GetPartsSummary());
            }
        }

        return summaries;
    }


    public boolean updatePartsList() throws IOException {
        currentParts.clear();
        Map<PartsHandler, Summary> summaries = getPartSummaries();
        for(PartsHandler handler : summaries.keySet()) {
            Summary summary = summaries.get(handler);
            for (int i=1; i<=summary.getPageCount(); i++) {
                Parts parts;
                if(currentSearchEntity != null) {
                    parts = handler.GetParts(i, currentSearchEntity);
                } else {
                    parts = handler.GetParts(i);
                }
                currentParts.addAll(parts.getParts());
            }
        }
        return true;
    }

    public Set<Part> getCurrentParts()
    {
        return currentParts;
    }

}
