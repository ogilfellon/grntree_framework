package uk.ac.ncl.icos.svpmanager;

import uk.ac.ncl.icos.tree.Tree;
import uk.ac.ncl.intbio.virtualparts.PartsHandler;
import uk.ac.ncl.intbio.virtualparts.entity.Interaction;
import uk.ac.ncl.intbio.virtualparts.entity.Part;

import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by owengilfellon on 16/04/2014.
 */
public class Test2 {

    private final static String     REPOSITORY_URL = "http://sbol.ncl.ac.uk:8081";
    private final static PartsHandler PARTS_HANDLER = new PartsHandler(REPOSITORY_URL);

    public static void main(String[] args) {


        SVPManager m = SVPManager.getSVPManager();
        Random r = new Random();

        try {

            /*

            // ===============================================
            // Test Phosphorylation Parts / Interactions
            // ===============================================

            for(int i=0; i<30; i++)
            {
               Part p = m.getNegOperator();
               List<Part> modifiers = m.getModifierParts(p);
               if(!modifiers.isEmpty()) {
                   for(Part part:modifiers) {
                       List<Interaction> phosInt = m.getPhosphorylationInteractions(part);
                       if(!phosInt.isEmpty()) {
                           System.out.println("========Original Part=======");
                           m.debugPart(p);
                           System.out.println("========Modifier Part=======");
                           m.debugPart(part);
                           for (Interaction interaction:phosInt) {
                               System.out.println("========Interaction=======");
                               m.debugInteraction(interaction);
                           }
                       }
                   }
               }
            }

            */

            Tree<Part> tree = new Tree<Part>();

            for(int i=0; i<30; i++)
            {
                Part p = m.getInducPromoter();

                List<Part> modifiers = m.getModifierParts(p);
                if(!modifiers.isEmpty()) {
                    for(Part part:modifiers) {
                        List<Interaction> phosInt = m.getPhosphorylationInteractions(part);
                        if(!phosInt.isEmpty()) {
                            //m.getPhosphateDonars(part);
                            //System.out.println("========Original Part=======");
                            //m.debugPart(p);
                            //System.out.println("========Modifier Part=======");
                            //m.debugPart(part);

                            //if(phosInt.size() > 1) {
                                //System.out.println("!");
                                List<List<Part>> paths = m.getPhosphorylationPaths(part);
                                for(List<Part> path : paths) {
                                    for(Part part1:path) {
                                        System.out.println(part1.getName());
                                    }
                                    System.out.println("-----");
                                }
                                System.out.println();


                           // }/**/
                            /*
                            for (Interaction interaction:phosInt) {
                                System.out.println("========Interaction=======");
                                m.debugInteraction(interaction);
                            }*/
                        }
                    }
                }
            }



        } catch (Exception ex) {
            Logger.getLogger(SVPManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
