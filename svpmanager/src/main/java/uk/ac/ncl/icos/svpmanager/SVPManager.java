package uk.ac.ncl.icos.svpmanager;

import org.sbml.jsbml.SBMLDocument;
import uk.ac.ncl.icos.tree.Node;
import uk.ac.ncl.icos.tree.Tree;
import uk.ac.ncl.intbio.virtualparts.PartsHandler;
import uk.ac.ncl.intbio.virtualparts.entity.*;

import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.io.Serializable;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * TODO Update with Filters to restrict species (pass filter to Manager?)
 * TODO Update to remove use as singleton - pass in preconfigured PartsManager. Or add multiple PartsManager, and manage from single object? i.e. a random part across all repositories
 * @author b1050029
 */
public class SVPManager implements Serializable{

    transient private final static SVPManager MANAGER = new SVPManager();

    // ================================================================
    // TODO: MAKE THIS CONFIGURABLE FROM OUTSIDE THE CLASS
    // TODO: MAKE CONFIGURABLE FROM SYNBAD
    // ================================================================
    
    //private static final String     REPOSITORY_URL = "http://sbol-dev.ncl.ac.uk:8081";
    private static final String     REPOSITORY_URL = "http://sbol.ncl.ac.uk:8081";
    transient private final PartsHandler PARTS_HANDLER;
    private final Random            r;
    private final String            SPECIES = "Bacillus subtilis";
    private final int               PARTS_PER_PAGE = 50;

    private SVPManager()
    {
       PARTS_HANDLER = new PartsHandler(REPOSITORY_URL);
       r = new Random();
    }
    
    /**
     * Factory method for obtaining an SVPManager. The SVPManager is a singleton - 
     * each method call returns a reference to the single SVPManager instance.
     * @return 
     */
    public static SVPManager getSVPManager()
    {
        return MANAGER;
    }
    
    
  
    /**
     * Returns the SVP specified by identity
     * @param part
     * @return 
     */
    public Part getPart(String part)
    {
        try {
            return PARTS_HANDLER.GetPart(part);
        } catch (IOException ex) {
            Logger.getLogger(SVPManager.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public SBMLDocument getPartDocument(Part part)
    {

            try {
                return PARTS_HANDLER.GetModel(part);
            }
            catch (IOException | XMLStreamException e) {
               e.printStackTrace();
            }
        

        return null;
    }

    public SBMLDocument createPartDocument(Part part, List<Interaction> internalEvents)
    {
        try {
            return PARTS_HANDLER.CreatePartModel(part, internalEvents);
        } catch (Exception e) {
            e.printStackTrace();            
        }
        
        return null;
    }

    public SBMLDocument getInteractionDocument(Interaction interaction)
    {
        try {
            return PARTS_HANDLER.GetInteractionModel(interaction);
        } catch (IOException | XMLStreamException e) {
            e.printStackTrace();
            return null;
        }
    }

    public SBMLDocument createInteractionDocument(Interaction interaction, List<Part> parts)
    {
        try {
            return PARTS_HANDLER.CreateInteractionModel(parts, interaction);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    // =====================================================================================
    // Methods for returning random parts, by type
    // =====================================================================================

    /**
     * Retrieves a random <i>Bacillus subtilis</i> promoter from the SVP Repository
     * @return
     */
    public Part getPromoter()
    {
        SearchEntity searchEntity = new SearchEntity(null, "Promoter", SPECIES, null, null, false, false);
        return getRandomPart(searchEntity);
    }

    /**
     * Retrieves a random constitutive <i>Bacillus subtilis</i> promoter from the SVP Repository
     * @return
     */
    public Part getConstPromoter()
    {
        SearchEntity searchEntity = new SearchEntity(null, "Promoter", SPECIES, "type", "ConstitutiveSigAPromoter", false, false);
        return getRandomPart(searchEntity);
    }

    /**
     * Retrieves a random inducible <i>Bacillus subtilis</i> promoter from the SVP Repository
     * @return
     */
    public Part getInducPromoter()
    {
        SearchEntity searchEntity = new SearchEntity(null, "Promoter", SPECIES, "type", "InduciblePromoter", false, false);
        return getRandomPart(searchEntity);
    }

    /**
     * Retrieves a random <i>Bacillus subtilis</i> RBS from the SVP Repository
     * @return
     */
    public Part getRBS()
    {
        SearchEntity searchEntity = new SearchEntity(null, "RBS", SPECIES, null, null, false, false);
        return getRandomPart(searchEntity);
    }

    /**
     * Retrieves a random <i>Bacillus subtilis</i> Terminator from the SVP Repository
     * @return
     */
    public Part getTerminator()
    {
        SearchEntity searchEntity = new SearchEntity(null, "Terminator", SPECIES, null, null, false, true);
        return getRandomPart(searchEntity);
    }

    /**
     * Retrieves a random <i>Bacillus subtilis</i> Repressible Operator from the SVP Repository
     * @return
     */
    public Part getNegOperator()
    {
        SearchEntity searchEntity = new SearchEntity(null, "Operator", SPECIES, "type", "NegativelyRegulatedOperator", false, false);
        return getRandomPart(searchEntity);
    }

    public Part getRandomPart(SearchEntity searchEntity)
    {
        try {
            Summary summary = PARTS_HANDLER.GetPartsSummary(searchEntity);

            // Final page may not have maximum number of parts. Number of parts is calculated,
            // so that part selection probability is equally weighted

            int numberOfPages = summary.getPageCount();
            int numberOfPartsOnLastPage = PARTS_HANDLER.GetParts(numberOfPages, searchEntity).getParts().size();
            int numberOfParts = (((numberOfPages-1) * PARTS_PER_PAGE) + numberOfPartsOnLastPage);
            int randomPart = r.nextInt(numberOfParts);
            int selectedPage = (int) Math.floor((double)randomPart / (double)PARTS_PER_PAGE);
            int selectedPart = randomPart % PARTS_PER_PAGE;

            // Page numbers start at 1

            Parts partsList = PARTS_HANDLER.GetParts(selectedPage+1, searchEntity);
            Part part = partsList.getParts().get(selectedPart);

            // Retrieve part by ID, as properties aren't included in Part objects stored in Parts

            return(PARTS_HANDLER.GetPart(part.getName()));

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    // =====================================================================================
    // Methods for returning all parts of a given type
    // =====================================================================================

    public List<Part> getAllParts(SearchEntity searchEntity)
    {
        List<Part> allParts = new ArrayList<Part>();
        try {
            Summary s = PARTS_HANDLER.GetPartsSummary(searchEntity);

            for(int i = 1; i<=s.getPageCount(); i++) {
                Parts p = PARTS_HANDLER.GetParts(i, searchEntity);
                allParts.addAll(p.getParts());
            }

            return allParts;

        } catch (IOException ex) {
            Logger.getLogger(SVPManager.class.getName()).log(Level.SEVERE, null, ex);
            return allParts;
        }
    }

    /**
     * Retrives all <i>Bacillus subtilis</i> promoters from the SVP Repository
     * @return
     */
    public List<Part> getAllPromoters()
    {
        SearchEntity searchEntity = new SearchEntity(null, "Promoter", SPECIES, null, null, false, false);
        return getAllParts(searchEntity);
    }

    /**
     * Retrives all <i>Bacillus subtilis</i> constitutive Promoters from the SVP repository
     * @return
     */
    public List<Part> getAllConstPromoter() {
        SearchEntity searchEntity = new SearchEntity(null, "Promoter", SPECIES, "type", "ConstitutiveSigAPromoter", false, false);
        return getAllParts(searchEntity);
    }

    /**
     * Retrives all <i>Bacillus subtilis</i> promoters from the SVP Repository
     * @return
     */
    public List<Part> getAllInducPromoters()
    {
        SearchEntity searchEntity = new SearchEntity(null, "Promoter", SPECIES, "type", "InduciblePromoter", false, false);
        return getAllParts(searchEntity);
    }

    /**
     * Retrives all <i>Bacillus subtilis</i> Repressible Operators from the SVP Repository
     * @return
     */
    public List<Part> getAllNegOperators()
    {
        SearchEntity searchEntity = new SearchEntity(null, "Operator", SPECIES, "type", "NegativelyRegulatedOperator", false, false);
        return getAllParts(searchEntity);
    }

    /**
     * Retrives all <i>Bacillus subtilis</i> RBSs from the SVP repository
     * @return
     */
    public List<Part> getAllRBS() {
        SearchEntity searchEntity = new SearchEntity(null, "RBS", SPECIES, null, null, false, false);
        return getAllParts(searchEntity);
    }

    /**
     * Retrives all <i>Bacillus subtilis</i> Terminators from the SVP repository
     * @return
     */
    public List<Part> getAllTerminators() {
        SearchEntity searchEntity = new SearchEntity(null, "Terminator", SPECIES, null, null, false, true);
        return getAllParts(searchEntity);
    }

    // =====================================================================================
    // Methods for returning interactions
    // =====================================================================================


    /**
     * Provides a check that the two provided parts are both involved in at least
     * one interaction.
     * @param part1
     * @param part2
     * @return 
     */
    public boolean hasInteraction(Part part1, Part part2)
    {
        
        if( (part1.getStatus() != null &&  part1.getStatus().equals("Prototype"))
                || (part1.getStatus() != null &&  part2.getStatus().equals("Prototype"))) {
            return false;
        }
        
        if( part1.getName().equals( part2.getName() ) &&
            part1.getType().equals( part2.getType() )) {
                return false;
        }
        
        List<Interaction> interactions = getInteractions(part1);
        
        if(interactions == null || interactions.isEmpty()) {
            return false;
        }

        ListIterator<Interaction> i = interactions.listIterator();

        while(i.hasNext()) {

            ListIterator<String> parts = i.next().getParts().listIterator();

            while( parts.hasNext() ) {

                if(part2.getName().equals( parts.next() )) {
                    return true;
                }
            }
        }

        
        return false;
    }
    
    public List<Interaction> getInternalEvents(Part part)
    {
        List<Interaction> internalEvents = new ArrayList<>();
        
        if(part.getStatus() != null && part.getStatus().equals("Prototype")) {
            return internalEvents;
        }
        
        try {
            Interactions interactions = PARTS_HANDLER.GetInternalInteractions(part);
            if(interactions.getInteractions() != null && interactions.getInteractions().size() > 0) {
                 internalEvents.addAll(interactions.getInteractions());
            }
           
        } catch (IOException | XMLStreamException ex) {
            Logger.getLogger(SVPManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return internalEvents;
    }
    
    /**
     * Returns a list of interactions that include the provided Part, or an empty list if no interactions are found
     * @param part
     * @return 
     */
    public List<Interaction> getInteractions (Part part)
    {
        List<Interaction> interactions = new ArrayList<>();
       
        if(part.getStatus() != null && part.getStatus().equals("Prototype")) {
            return interactions;
        }
        
        try {
            interactions =  PARTS_HANDLER.GetInteractions(part).getInteractions();
            return(interactions);
        } catch (IOException ex) {
            Logger.getLogger(SVPManager.class.getName()).log(Level.SEVERE, null, ex);
            return interactions;
        } catch (XMLStreamException ex) {
            Logger.getLogger(SVPManager.class.getName()).log(Level.SEVERE, null, ex);
            return interactions;
        }
    }

    /**
     * Returns all interactions between the two provided parts
     * @param part1
     * @param part2
     * @return
     */
    public List<Interaction> getInteractions(Part part1, Part part2)
    {
        List<Interaction> matchedInteractions = new ArrayList<>();

        if( (part1.getStatus() != null && part1.getStatus().equals("Prototype")) ||
                (part2.getStatus() != null && part2.getStatus().equals("Prototype"))) {
            return matchedInteractions;
        }
        
        if( part1.getName().equals( part2.getName() ) &&
                part1.getType().equals( part2.getType() )) {
            return matchedInteractions;
        }

        List<Interaction> interactions = getInteractions(part1);

        if(interactions != null) {
            ListIterator<Interaction> i = interactions.listIterator();

            while(i.hasNext()) {
                Interaction interaction = i.next();
                ListIterator<String> j = interaction.getParts().listIterator();

                while( j.hasNext() ) {

                    if(part2.getName().equals( j.next() )) {
                        matchedInteractions.add(interaction);
                    }
                }
            }
        }

        return matchedInteractions;
    }


    public List<Interaction> getInteractions(String partId) {
        try {
            Part part = PARTS_HANDLER.GetPart(partId);
            return getInteractions(part);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return new ArrayList<Interaction>();
    }
    
    
    public List<Part> getInteractingParts(Part part, int range) {
        
        /*
        if(part.getStatus().equals("Prototype")) {
            return new ArrayList<Part>();
        }*/

        Map<String, Part> parts = new HashMap<>();
        parts.put(part.getName(), part);

        
        for(int i = 0; i < range; i++) {

            List<Part> toAdd = new ArrayList<>();
            
            for(Part storedPart : parts.values()) {
                
                List<Interaction> interactions = getInteractions(part);
                
                for(Interaction interaction : interactions) {
                    
                    for(String s : interaction.getParts()) {
                        
                        if(!parts.containsKey(s)) {
                            toAdd.add(getPart(s));
                        }
                    }
                }
            }
            
            for(Part add : toAdd) {
                parts.put(add.getName(), add);
                System.out.println(add.getName());
            }
        }
        
        parts.remove(part.getName());
        
        return new ArrayList<>(parts.values());
    }
    
    /**
     * Returns the complimentary Part from a random Interaction involving the
     * argument Part.
     * i.e. hasInteraction(argumentPart, returnedPart) = true
     * @param part
     * @return 
     */
    public Part getInteraction(Part part){

        if(part.getStatus() != null && part.getStatus().equals("Prototype")) {
            return null;
        }
        
        List<Interaction> interactions = getInteractions(part);

        int randomInteractionIndex = r.nextInt(interactions.size());
        List<InteractionPartDetail> partDetails = interactions.get(randomInteractionIndex).getPartDetails();

        if(partDetails == null) {
            return null;
        }

        ArrayList<String> interactingParts = new ArrayList<String>();

        for( int i = 0; i < partDetails.size(); i++ ) {

            if(!partDetails.get(i).getPartName().equals( part.getName() )){
               interactingParts.add(partDetails.get(i).getPartName());
            }
        }

        if(interactingParts.size() != 1) {
            return null;
        }
        return getPart(interactingParts.get(0));
    }

    // =====================================================================================
    //
    // =====================================================================================
       
    /**
     * Returns a list of all Parts that modify the provided Part interaction
     * i.e. transcription factors
     * 
     * @param part
     * @return 
     */
    public ArrayList<Part> getModifierParts(Part part)
    {
        List<Interaction> interactions = getInteractions(part);
        ArrayList<Part> modifierParts = new ArrayList<Part>();
        
        if(part.getStatus() != null && part.getStatus().equals("Prototype")) {
            return modifierParts;
        }

        if(interactions == null) {
            return modifierParts;
        }

        for(Interaction interaction : interactions) {

            List<InteractionPartDetail> partDetail;
            if( ( partDetail = interaction.getPartDetails() ) == null ) {
                return modifierParts;
            }

            ArrayList<String> modifierPartIds = new ArrayList<String>();

            for(int i=0; i< partDetail.size(); i++){

                if( !partDetail.get( i ).getPartName().equals( part.getName() ) &&
                    partDetail.get( i ).getInteractionRole().equalsIgnoreCase( "Modifier" ) ) {
                        modifierPartIds.add(partDetail.get(i).getPartName());
                }
            }

            if( modifierPartIds.size() != 1 ) {
                return new ArrayList<Part>();
            }

            for( String id : modifierPartIds ) {
                modifierParts.add( getPart( id ) );
            }
        }

        return modifierParts;
    }

    public List<Part> getSinglePhosphorylationPath(Part part)
    {
        List<List<Part>> path = getPhosphorylationPaths(part);
        

        if(path.isEmpty() ) {
            return new ArrayList<Part>();
        }

        return path.get(r.nextInt(path.size()));
    }

    public List<List<Part>> getPhosphorylationPaths(Part part) {

        // Retrieve a tree of phosphorylation interactions. Each child node phosphorylates its parent

        Tree<Part> tree = new Tree<Part>();
        List<List<Part>> paths = new ArrayList<List<Part>>();
        
        if(part.getStatus() != null && part.getStatus().equals("Prototype")) {
            return paths;
        }
        
        tree.setRootNode(pathLoop(part));

        // =========================================================================
        // Iterate through tree and create a List of Parts for each valid branch.
        // A valid branch requires a leaf node with an internal interaction which
        // phosphorylates the part in response to some stimulus
        // =========================================================================

        Iterator<Node<Part>> i = tree.getPreOrderIterator();

        while(i.hasNext()) {

            Node<Part> node = i.next();
            Part data = node.getData();

            try {

                boolean isStartPoint = false;
                Interactions internalInteractions = PARTS_HANDLER.GetInternalInteractions(data);

                if(internalInteractions != null && !internalInteractions.getInteractions().isEmpty()) {
                    for(Interaction interaction : internalInteractions.getInteractions()) {
                        if(interaction.getInteractionType().equals("Phosphorylation")) {
                            isStartPoint = true;
                        }
                    }
                }

                if( node.getChildren().isEmpty() && isStartPoint ) {

                    List<Part> path = new ArrayList<Part>();
                    path.add(data);

                    while(node.getParent()!=null) {
                        node = node.getParent();
                        path.add(0, node.getData());
                    }

                    paths.add(path);
                }
            } catch (IOException | XMLStreamException e) {
                e.printStackTrace();
            }
        }

        return paths;
    }

    private Node<Part> pathLoop(Part part) {
        
        if(part.getStatus() != null && part.getStatus().equals("Prototype")) {
            return null;
        }

        Node<Part> node = new Node<Part>(part);

        try {
            Interactions internalInteractions = PARTS_HANDLER.GetInternalInteractions(part);
            boolean startPoint = false;

            if( internalInteractions != null && internalInteractions.getInteractions().size() > 0 ) {
                for( Interaction interaction : internalInteractions.getInteractions() ) {
                    if( interaction.getInteractionType().equals("Phosphorylation") ) {
                        startPoint = true;
                    }
                }
            }

            if(!startPoint) {

                // ===========================================================
                // Get parts that phosphorylate the current part
                // Also, check for pre-inclusion to elimate loops?
                // ===========================================================

                List<Part> phosphorylatingParts = getPhosphateDonars(part);

                for(Part pPart : phosphorylatingParts) {
                    node.addChild(pathLoop(pPart));
                }

                return node;
            }
            else
            {
                return node;
            }


        } catch (IOException | XMLStreamException e) {
            e.printStackTrace();
            return node;
        }
    }

    public List<Part> getPhosphateDonars(Part part)
    {
        List<Part> phosphateDonars = new ArrayList<>();
        
        if(part.getStatus() != null && part.getStatus().equals("Prototype")) {
            return phosphateDonars;
        }
        
        List<Interaction> interactions = getInteractions(part);

        if(interactions == null) {
            return phosphateDonars;
        }

        for(Interaction interaction : interactions) {

            if(interaction.getInteractionType().equals("Phosphorylation")) {

                List<InteractionPartDetail> partDetails = interaction.getPartDetails();

                // Assuming 2x inputs and 2x outputs

                boolean isDefaultAtInput = false;
                boolean isPhosphoylatedAtOutput = false;
                Part phosphorlatedAtInput = null;

                if( partDetails != null && !partDetails.isEmpty()) {

                    for(int i=0; i< partDetails.size(); i++) {

                        InteractionPartDetail partDetail = partDetails.get(i);

                        if(     partDetail.getPartName().equals(part.getName()) &&
                                partDetail.getInteractionRole().equals("Input") &&
                                partDetail.getPartForm().equals("Default")) {
                                    isDefaultAtInput = true;
                        }

                        if(     partDetail.getPartName().equals(part.getName()) &&
                                partDetail.getInteractionRole().equals("Output") &&
                                partDetail.getPartForm().equals("Phosphorylated")) {
                                    isPhosphoylatedAtOutput = true;
                        }

                        if(     partDetail.getInteractionRole().equals("Input") &&
                                partDetail.getPartForm().equals("Phosphorylated")) {
                                    phosphorlatedAtInput = this.getPart(partDetail.getPartName());
                        }
                    }
                }

                if(isDefaultAtInput && isPhosphoylatedAtOutput && phosphorlatedAtInput != null) {
                    phosphateDonars.add(phosphorlatedAtInput);
                }
            }
        }

        return phosphateDonars;
    }


    
    /**
     *
     * 
     * @param part
     * @return 
     */
    public List<Part> getPhosphorylationParts(Part part)
    {
        
        List<Part> phosphorylationParts = new ArrayList<>();
        
        if( part.getStatus() != null && part.getStatus().equals("Prototype")) {
            return phosphorylationParts;
        }
        List<Interaction> interactions = getInteractions(part);

        if( interactions == null) {
            return phosphorylationParts;
        }

        for( Interaction interaction : interactions ) {
            if( interaction.getInteractionType().equalsIgnoreCase( "Phosphorylation" )) {
                for( String partId : interaction.getParts() ) {
                    if(!partId.equalsIgnoreCase( part.getName() )) {
                        phosphorylationParts.add( getPart( partId ) );
                    }
                }
            }
        }

        return phosphorylationParts;
    }

    public List<Interaction> getPhosphorylationInteractions(Part part)
    {
        List<Interaction> phosphorylationInteractions = new ArrayList<>();
        
        if(part.getStatus() != null && part.getStatus().equals("Prototype")) {
            return phosphorylationInteractions;
        }
        
        List<Interaction> interactions = getInteractions(part);
        

        if( interactions == null ) {
            return phosphorylationInteractions;
        }

        for( Interaction interaction : interactions ) {
            if( interaction.getInteractionType().equalsIgnoreCase( "Phosphorylation" )) {
                phosphorylationInteractions.add(interaction);
            }
        }

        return phosphorylationInteractions;
    }




    public HashMap<String, Double> getAllPartParameters(List<Part> parts)
      {
          // Iterates through a list of Parts, if parameter is found, adds the name and parameter to a Hashmap and returns
          
          HashMap<String, Double> toReturn = new HashMap<String, Double>();
          
          for(Part p:parts){
              Double d = getPartParameter(p);
              if(d!=null){
                   toReturn.put(p.getName(), getPartParameter(p));
              }
          }
          
          return toReturn;
      }

    // =====================================================================================
    // Methods for returning part parameters
    // =====================================================================================

    public Double getPartParameter(Part part)
    {
        if(part.getStatus() != null && part.getStatus().equals("Prototype")) {
            return null;
        }
          
        try {
            // First check for internal interactions - return "ktl" or "ktr" parameters if found
             
            if (PARTS_HANDLER.GetInternalInteractions(part).getInteractions() != null) {
                for (Interaction in : PARTS_HANDLER.GetInternalInteractions(part).getInteractions()) {
                    for (Parameter param : in.getParameters()) {
                        if (param.getName().equals("ktl")
                                || param.getName().equals("ktr")) {
                            return param.getValue();}
                    }
                }
            }
            
            // If "ktl" or "ktr" were not found, is an Operator - find "Km"
            
            if (PARTS_HANDLER.GetInteractions(part).getInteractions() != null){
                for(Interaction in: PARTS_HANDLER.GetInteractions(part).getInteractions()){
                     for (Parameter param : in.getParameters()) {
                        if (param.getName().equals("Km")) {
                            return param.getValue();}
                     }
                }
            }
            
            // If nothing is found, return null
            
            else {
                return null;
            }
            

         return null;
        } catch (IOException ex) {
            Logger.getLogger(SVPManager.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } catch (XMLStreamException ex) {
            Logger.getLogger(SVPManager.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
      }

    // =====================================================================================
    // Other helpful methods...
    // =====================================================================================

    public boolean isRegulatedPromoter(Part part)
    {
        List<Property> properties = part.getProperties();
        if(properties!=null){
            for(Property p:properties){
                if(p.getValue().equals("InduciblePromoter") ||
                        p.getValue().equals("RepressiblePromoter")){
                    return true;
                }
            }
        }

        return false;
    }

    public boolean isConstitutivePromoter(Part part)
    {
        List<Property> properties = part.getProperties();
        if(properties!=null){
            for(Property p:properties){
                if( p.getValue().equals("ConstitutivePromoter") ||
                    p.getValue().equals("SigHPromoter") || // TODO: this needs to be changed to ConstitutiveSigHPromoter
                    p.getValue().equals("ConstitutiveSigAPromoter"))
                {
                    return true;
                }
            }
        }

        return false;
    }

    // =====================================================================================
    // Debug Methods. For debugging.
    // =====================================================================================

    /**
     *
     * @param part
     */
    public void debugPart(Part part)
    {
        String format = "%-20s %-20s %n";
        String subformat =  "%-5s %-20s %-30s %-30s %n";
        String divider = "--------------------------------------------------";
        String subdivider = "     -----------------------------------------------------------------------";

        System.out.println(divider);
        System.out.printf(format, "Field", "Value");
        System.out.println(divider);
        System.out.printf(format, "Name", part.getName());
        System.out.printf(format, "Display Name", part.getDisplayName());
        System.out.printf(format, "Description", part.getDescription());
        System.out.printf(format, "Design Method", part.getDesignMethod());
        System.out.printf(format, "Organism", part.getOrganism());
        System.out.printf(format, "MetaType", part.getMetaType());
        System.out.printf(format, "Status", part.getStatus());
        System.out.printf(format, "Type", part.getType());

        System.out.println(subdivider);
        System.out.printf(subformat, "", "Name", "Value", "Description");
        System.out.println(subdivider);

        List<Property> properties = part.getProperties();

        if(properties != null) {
            for ( Property property : properties ) {
                System.out.printf(subformat, "", property.getName(), property.getValue(), property.getDescription());
            }
        }
    }

    /**
     * Returns a String representation of the properties of the provided interaction
     * @param interaction
     * @return
     */
    public void debugInteraction(Interaction interaction)
    {
        List<Constraint> constraints = interaction.getConstraints();
        List<InteractionPartDetail> partDetails = interaction.getPartDetails();
        ListIterator<InteractionPartDetail> i = partDetails.listIterator();
        List<Parameter> parameters = interaction.getParameters();
        ListIterator<Parameter> j = parameters.listIterator();

        String format = "%-20s %-20s %n";
        String subformat =  "%-5s %-20s %-30s %-30s %-30s %-30s %n";
        String parameterSubformat =  "%-5s %-20s %-30s %-30s %-30s %-30s %n";
        String divider = "--------------------------------------------------";
        String subdivider = "     -----------------------------------------------------------------------";

        System.out.println(divider);
        System.out.printf(format, "Field", "Value");
        System.out.println(divider);
        System.out.printf(format, "Name", interaction.getName());
        System.out.printf(format, "Type", interaction.getInteractionType());
        System.out.printf(format, "Math Name", interaction.getMathName());
        System.out.printf(format, "isReversible", interaction.getIsReversible());
        System.out.printf(format, "isReaction", interaction.getIsReaction());
        System.out.printf(format, "FreetextMath", interaction.getFreeTextMath());
        System.out.printf(format, "Description", interaction.getDescription());
        System.out.println();

        System.out.printf(subformat, "", "========", "========", "===============", "==============", "========");
        System.out.printf(subformat, "", "PartName", "PartForm", "InteractionRole", "# of Molecules", "MathName");
        System.out.printf(subformat, "", "========", "========", "===============", "==============", "========");

        while( i.hasNext() ) {
            InteractionPartDetail partDetail = i.next();
            System.out.printf(subformat, "",    partDetail.getPartName(),
                    partDetail.getPartForm(),
                    partDetail.getInteractionRole(),
                    partDetail.getNumberOfMolecules(),
                    partDetail.getMathName());

        }

        System.out.println();

        System.out.printf(parameterSubformat, "", "====", "=====", "=============", "=====", "============");
        System.out.printf(parameterSubformat, "", "Name", "Value", "ParameterType", "Scope", "EvidenceType");
        System.out.printf(parameterSubformat, "", "====", "=====", "=============", "=====", "============");

        while( j.hasNext() ) {
            Parameter parameter = j.next();
            System.out.printf(parameterSubformat, "",
                    parameter.getName(),
                    parameter.getValue(),
                    parameter.getParameterType(),
                    parameter.getScope(),
                    parameter.getEvidenceType());

        }

        System.out.println();

        System.out.println(subdivider);
/*
        if(constraints != null){
            ListIterator<Constraint> k = constraints.listIterator();
            stringBuilder.append("Constraints ===").append("\n");

            while(k.hasNext()){
                Constraint constraint = k.next();
                stringBuilder.append(" Qualifier: ").append(constraint.getQualifier()).append("\n");
                stringBuilder.append(" SourceID: ").append(constraint.getSourceID()).append("\n");
                stringBuilder.append(" SourceType: ").append(constraint.getSourceType()).append("\n");
                stringBuilder.append(" TargetID: ").append(constraint.getTargetID()).append("\n");
                stringBuilder.append(" TargetType: ").append(constraint.getTargetType()).append("\n");
            }
        }

        stringBuilder.append("InteractionType: ").append(interaction.getInteractionType()).append("\n");
        stringBuilder.append("Is Internal: ").append(interaction.getIsInternal()).append("\n");
        stringBuilder.append("Is Reaction: ").append(interaction.getIsReaction()).append("\n");
        stringBuilder.append("Interaction Part Details ===").append("\n");

        while(i.hasNext()){
            InteractionPartDetail partDetail = i.next();
            stringBuilder.append(" Interaction Role: ").append(partDetail.getInteractionRole()).append("\n");
            stringBuilder.append(" Math Name: ").append(partDetail.getMathName()).append("\n");
            stringBuilder.append(" Number Of Molecules: ").append(partDetail.getNumberOfMolecules()).append("\n");
            stringBuilder.append(" Part Form: ").append(partDetail.getPartForm()).append("\n");
            stringBuilder.append(" Part Name: ").append(partDetail.getPartName()).append("\n");
        }

        stringBuilder.append("Parameters ===").append("\n");

        while(j.hasNext()){
            Parameter parameter = j.next();
            stringBuilder.append(" Name: ").append(parameter.getName()).append("\n");
            stringBuilder.append(" ParameterType: ").append(parameter.getParameterType()).append("\n");
            stringBuilder.append(" Value: ").append(parameter.getValue()).append("\n");
            stringBuilder.append(" Scope: ").append(parameter.getScope()).append("\n");
            stringBuilder.append(" Evidence Type: ").append(parameter.getEvidenceType()).append("\n");
        }

        return stringBuilder.toString();

        */
    }
}
