package uk.ac.ncl.icos.tree;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by owengilfellon on 22/05/2014.
 */
public class Node<T> {

    private T data;
    private List<Node<T>> children;
    private Node<T> parent;

    public Node()
    {
        super();
        this.children = new ArrayList<Node<T>>();
    }

    public Node(T data)
    {
        super();
        this.children = new ArrayList<Node<T>>();
        this.data = data;
    }

    public List<Node<T>> getChildren() {
        return this.children;
    }

    public void setChildren(List<Node<T>> children) {
        for( Node<T> child:children) {
            child.setParent(this);
        }
        this.children = children;
    }

    public void addChild(Node<T> child) {
        child.setParent(this);
        this.children.add(child);
    }

    public boolean hasSiblings()
    {
        if( this.parent != null ) {
            if(this.parent.getChildren().size() > 1) {
                return true;
            }
        }
        return false;
    }

    public T getData()
    {
        return data;
    }

    public void setData(T data)
    {
        this.data = data;
    }

    public void setParent(Node<T> parent) { this.parent = parent; }

    public Node<T> getParent() { return parent; }
}
