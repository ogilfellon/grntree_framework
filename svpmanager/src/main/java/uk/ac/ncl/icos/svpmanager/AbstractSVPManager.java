package uk.ac.ncl.icos.svpmanager;

import uk.ac.ncl.intbio.virtualparts.PartsHandler;
import uk.ac.ncl.intbio.virtualparts.entity.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @author b1050029
 */
public class AbstractSVPManager implements Serializable {

    //TODO: Incorporate this class within SVP Manager? Or hide behind interface / factory?

    private final static String repositoryURL = "http://sbol.ncl.ac.uk:8081";
    //private final static String repositoryURL = "http://localhost:8080";


    private final static PartsHandler partshandler = new PartsHandler(repositoryURL);
    private final static AbstractSVPManager m = new AbstractSVPManager();
    
    private final Random r = new Random();
    private final Property dataSource = new Property();
    private final Property dnaComponent = new Property();
    
    private AbstractSVPManager()
    {    
        dataSource.setName("Data Source");
        dataSource.setValue("None");
        dnaComponent.setName("type");
        dnaComponent.setValue("DnaComponent");
    }
    
    /**
     * Factory method for obtaining an SVPManager. The SVPManager is part singleton - 
     * each method call returns part reference to the single SVPManager instance.
     * @return 
     */
    public static AbstractSVPManager getSVPManager()
    {
        return m;
    }
    
    /**
     * Returns an abstract Constituitive Promoter with the name specified by the
     * argument, and no internal reactions
     * @param id
     * @return 
     */
    public Part getConstPromoterPart(String id)
    {
        Part part = new Part();
        
        part.setDescription("Abstract SVP representing a Constituitive Promoter");
        part.setDesignMethod("Abstract");
        part.setDisplayName(id);
        part.setMetaType("Promoter");
        part.setName(id);
        part.setOrganism("None");
        part.setStatus("??");
        part.setType("Promoter");

        List<Property> properties = new ArrayList<Property>();
        properties.add(dataSource);
        properties.add(dnaComponent);
       
        Property propConstPromoter = new Property();
        propConstPromoter.setName("type");
        propConstPromoter.setValue("Constitutive Promoter");
        
        part.setProperties(properties);
        
        return part;
    }
    
    public Part getInducPromoterPart(String id)
    {
        Part part = new Part();
        
        part.setDescription("Abstract SVP representing an inducible Promoter");
        part.setDesignMethod("Abstract");
        part.setDisplayName(id);
        part.setMetaType("Promoter");
        part.setName(id);
        part.setOrganism("None");
        part.setStatus("??");
        part.setType("Promoter");
        
        List<Property> properties = new ArrayList<Property>();
        properties.add(dataSource);
        properties.add(dnaComponent);
       
        Property propConstPromoter = new Property();
        propConstPromoter.setName("type");
        propConstPromoter.setValue("Inducible Promoter");
        
        part.setProperties(properties);
        
        return part;
    }
   
    /**
     * Returns an abstract RBS with the name specified by the argument, and no
     * internal reactions
     * @param id
     * @return 
     */
    public Part getRBS(String id)
    {
        Part RBS = new Part();
        RBS.setDescription("Abstract SVP representing an RBS");
        RBS.setDesignMethod("Abstract");
        RBS.setDisplayName(id);
        RBS.setMetaType("??");
        RBS.setName(id);
        RBS.setOrganism("None");
        RBS.setStatus("??");
        RBS.setType("RBS");

        List<Property> properties = new ArrayList<Property>();
        properties.add(dataSource);
        properties.add(dnaComponent);
        
        Property typeRBS = new Property();
        typeRBS.setName("type");
        typeRBS.setValue("Ribosome Binding Site");
        properties.add(typeRBS);

        RBS.setProperties(properties);

        return RBS;
    }

    public Part getNegativeOperator(String id)
    {
        Part part = new Part();
        part.setDescription("Abstract SVP representing an Operator");
        part.setDesignMethod("Abstract");
        part.setDisplayName(id);
        part.setMetaType("??");
        part.setName(id);
        part.setOrganism("None");
        part.setStatus("??");
        part.setType("Operator");
             
        List<Property> properties = new ArrayList<Property>();
        properties.add(dataSource);
        properties.add(dnaComponent);
       
        Property propOperator = new Property();
        propOperator.setName("type");
        propOperator.setValue("Negatively regulated operator");
        properties.add(propOperator);
        
        part.setProperties(properties);
        
        return part;
    }
  
    public Part getCDS(String id)
    {
        Part part = new Part();
        part.setDescription("Abstract SVP representing a CDS");
        part.setDesignMethod("Abstract");
        part.setDisplayName(id);
        part.setMetaType("??");
        part.setName(id);
        part.setOrganism("None");
        part.setStatus("??");
        part.setType("FunctionalPart");
        
        List<Property> properties = new ArrayList<Property>();
        properties.add(dataSource);
        properties.add(dnaComponent);
       
        Property propCDS = new Property();
        propCDS.setName("type");
        propCDS.setValue("Protein");
        properties.add(propCDS);
        
        part.setProperties(properties);
        
        return part;
    }

    /**
     * Returns an Interaction describing Promoter or Operator regulation by a protein
     * 
     * @param regulatedPart the part that is regulated - must be a promoter or operator
     * @param transcriptionFactor the part that regulates the part - must be a cds
     * @param form default or phosphorylated
     * @param role repression or activation
     * @param km
     * @return 
     */
    public Interaction getRegulationInteraction(Part regulatedPart,
                                                Part transcriptionFactor,
                                                MolecularForm form,
                                                RegulationRole role,
                                                double km)
    {
       
        
        Parameter output = new Parameter();
                output.setName("Output");
                output.setParameterType("PoPS");
                output.setScope("Output");
        //"PoPS", "Outut", null);
        Parameter input = new Parameter();
                input.setName("Input");
                input.setParameterType("PoPS");
                input.setScope("Input");
        //"PoPS", "Input", null);
             Parameter n = new Parameter("n", "n", 1.0); 
        Parameter okm = new Parameter();
                okm.setName("Km");
                okm.setParameterType("Km");
                okm.setScope("Local");
                okm.setValue(km);

        Interaction interaction = new Interaction();
        interaction.AddPart(transcriptionFactor.getName());
        interaction.AddPart(regulatedPart.getName());
        interaction.setName(transcriptionFactor.getName() + "_bi_to_" + regulatedPart.getName());
        

        interaction.AddParameter(n);
        interaction.AddParameter(output);
        interaction.AddParameter(input);
        interaction.AddParameter(okm);
        
        String tfRole = role == RegulationRole.ACTIVATOR ? "Activator" : "Repressor";
        String tfForm = form == MolecularForm.PHOSPHORYLATED ? "Phosphorylated" : "Default";
        
        InteractionPartDetail o_ipd = new InteractionPartDetail(transcriptionFactor.getName(), tfForm, "Modifier", 1, tfRole);
        interaction.AddPartDetail(o_ipd);
        
        StringBuilder sb = new StringBuilder();
        sb.append("Transcriptional ");
        if(role == RegulationRole.ACTIVATOR){
            sb.append("activation");
        }
        else{
            sb.append("repression");
        }
        if(regulatedPart.getType().equals("Operator")){
            sb.append(" using an operator");
        }
        
        String math = role == RegulationRole.ACTIVATOR
                                ? (regulatedPart.getName() + "_PoPS * " + transcriptionFactor.getName() +" / (" + transcriptionFactor.getName() +" + Km)")
                                : "Km / (" + transcriptionFactor.getName() +" + Km)";
        
        interaction.setInteractionType(sb.toString());
        interaction.setIsReaction(false);
        interaction.setFreeTextMath(math);
                
        String mathType = role == RegulationRole.ACTIVATOR
                        ? regulatedPart.getType() + "Induction"
                        : regulatedPart.getType() + "Repression";
        
        System.out.println(mathType);
        
        interaction.setMathName(mathType);

        return interaction;
    }
    
    public Interaction getPhosphorylationInteraction(Part donorPart, Part acceptorPart, double kf)
    {
        StringBuilder sb = new StringBuilder();
        sb.append(donorPart.getName()).append("~P").append(" + ").append(acceptorPart.getName())
                .append(" -> ")
                .append(donorPart.getName()).append(" + ").append(acceptorPart.getName()).append("~P");

        List<InteractionPartDetail> constraints = new ArrayList<InteractionPartDetail>();      
        constraints.add(new InteractionPartDetail(donorPart.getName(), "Phosphorylated", "Input", 1, "Protein1_P"));
        constraints.add(new InteractionPartDetail(donorPart.getName(), "Default", "Output", 1, "Protein1"));
        constraints.add(new InteractionPartDetail(acceptorPart.getName(), "Default", "Input", 1, "Protein2"));
        constraints.add(new InteractionPartDetail(acceptorPart.getName(), "Phosphorylated", "Output", 1, "Protein2_P"));
        
        Interaction interaction = new Interaction();
        interaction.AddPart(donorPart.getName());
        interaction.AddPart(acceptorPart.getName());
        interaction.setName(acceptorPart.getName() + "_ph_by_" + donorPart.getName());

        interaction.setIsReaction(true);
        interaction.setFreeTextMath(sb.toString());
        interaction.setMathName("TwoComponentPhosphorelay");
        interaction.setPartDetails(constraints);
        Parameter kfPar = new Parameter();
        //"kf", "kf", kf);
        kfPar.setName("kf");
        kfPar.setParameterType("kf");
        kfPar.setScope("Local");
        kfPar.setValue(kf);
        interaction.AddParameter(kfPar);
        
        return interaction;
    }
    
    /**
     * Given a part, and a transcription rate(?), returns an internal Reaction
     * that provides the mathematical rules for SBML Models
     * @param part
     * @param ktr
     * @return 
     */
    public List<Interaction> getPoPSProduction(Part part, double ktr)
    {
        Parameter pops = new Parameter();
        //"PoPS", "PoPSOutput", null);
        pops.setName("PoPSOutput");
        pops.setParameterType("PoPS");
        pops.setScope("Output");
        
        Parameter aktr = new Parameter ();
        //"ktr", "ktr", ktr);
        aktr.setName("ktr");
        aktr.setParameterType("ktr");
        aktr.setValue(ktr);
        aktr.setScope("Local");
        
        Interaction interaction = new Interaction();
        interaction.AddPart(part.getName());
        interaction.setName(part.getName() + "_PoPSProduction");
        interaction.AddParameter(pops);
        interaction.AddParameter(aktr);
        
    
        interaction.setIsInternal(true);
        interaction.setIsReaction(false);
        
        interaction.setFreeTextMath("-> PoPS");
        interaction.setMathName("PoPSProduction");
        interaction.setInteractionType("PoPSProduction");
        
        List<Interaction> interactions = new ArrayList();
        interactions.add(interaction);
        
        interaction.setPartDetails(new ArrayList<InteractionPartDetail>());
        
        return interactions;
    }
    
    /**
     * Given a part, and a translation rate(?), returns an internal reaction
     * that provides the mathematical rules for SBML Models
     * @param part
     * @param ktl
     * @return 
     */
    public List<Interaction> getRiPSProduction(Part part, double ktl)
    {  
        //Parameter volume = new Parameter("volume", "volume", 1.0);
        
        Parameter volume = new Parameter();
        volume.setName("volume");
        volume.setParameterType("volume");
        volume.setScope("Global");
        volume.setValue(1.0);
        
        Parameter mrna = new Parameter();
        //"mRNA", "mRNAInput", null);
        mrna.setName("mRNAInput");
        mrna.setParameterType("mRNA");
        mrna.setScope("Input");
        
        Parameter rips = new Parameter();
        //"RiPSOutput", "RiPS", null);
        rips.setName("RiPSOutput");
        rips.setParameterType("RiPS");
        rips.setScope("Output");
        
        Parameter bktl = new Parameter();
        //"ktl", "ktl", ktl);
        bktl.setName("ktl");
        bktl.setParameterType("ktl");
        bktl.setScope("Local");
        bktl.setValue(ktl);
        

        Interaction interaction = new Interaction();
        interaction.AddPart(part.getName());
        interaction.setName(part.getName() + "_RiPSProduction");

        interaction.AddParameter(volume);
        interaction.AddParameter(mrna);
        interaction.AddParameter(rips);
        interaction.AddParameter(bktl);

        interaction.setInteractionType("RiPS Production");
        interaction.setIsInternal(true);
        interaction.setIsReaction(false);
        interaction.setFreeTextMath("-> RiPS");
        interaction.setMathName("RiPSProduction");
        
        interaction.setPartDetails(new ArrayList<InteractionPartDetail>());
        
        List<Interaction> interactions = new ArrayList();
        interactions.add(interaction);
        return interactions;
    }
    
    public List<Interaction> getProteinProductionAndDegradation(Part cds, double degradationRate)
    {
       
        Parameter volume = new Parameter();
        //"volume", "volume", 1.0);
        volume.setName("volume");
        volume.setParameterType("volume");
        volume.setScope("Global");
        volume.setValue(1.0);
        
        Parameter ripsInput = new Parameter();
        //"RiPS", "RiPSInput", null);
        ripsInput.setName("RiPSInput");
        ripsInput.setParameterType("RiPS");
        ripsInput.setScope("Input");

        Interaction production = new Interaction();
        production.AddPart(cds.getName());
        production.setName(cds.getName() + "_Production");

        production.AddParameter(volume);
        production.AddParameter(ripsInput);
        
        InteractionPartDetail o_ipd = new InteractionPartDetail(cds.getName(), "Default", "Output", 1, "Species");
        production.AddPartDetail(o_ipd);
        
        production.setInteractionType("Protein Production");
        production.setIsInternal(Boolean.TRUE);
        production.setIsReaction(Boolean.TRUE);
        production.setFreeTextMath("-> " + cds.getName());
        production.setMathName("ProteinProduction");
        
        Parameter kd = new Parameter();
        //Parameter kd = new Parameter("kd", "kd", degradationRate);
        kd.setName("kd");
        kd.setParameterType("kd");
        //kd.setScope("Global");
        kd.setScope("Local");
        kd.setValue(degradationRate);
        
        Interaction degradation = new Interaction();
        degradation.AddPart(cds.getName());
        degradation.setName(cds.getName() + "_Degradation");
        
        degradation.AddParameter(kd);
        
        InteractionPartDetail d_ipd = new InteractionPartDetail(cds.getName(), "Default", "Input", 1, "Species");
        degradation.AddPartDetail(d_ipd);
        
        degradation.setInteractionType("Degradation");
        degradation.setIsInternal(true);
        degradation.setIsReaction(true);
        degradation.setFreeTextMath(cds.getName() + " ->");
        degradation.setMathName("Degradation");
        
        List<Interaction> interactions = new ArrayList<Interaction>();
        interactions.add(production);
        interactions.add(degradation);

        return interactions;
    }
    
    public List<Interaction> getDephosphorylationandDegradation(Part cds,
            double dephosphorylationRate,
            double degradationRate)
    {
        List<InteractionPartDetail> constraints = new ArrayList<InteractionPartDetail>();
        constraints.add(new InteractionPartDetail(cds.getName(), "Phosphorylated", "Input", 1, "SpeciesPhosphorylated"));
        constraints.add(new InteractionPartDetail(cds.getName(), "Default", "Output", 1, "Species"));
        
        Interaction dephosphorylation = new Interaction();
        dephosphorylation.setIsInternal(true);
        dephosphorylation.setIsReaction(true);
        dephosphorylation.setName("Dephosphorylation");
        dephosphorylation.setFreeTextMath(cds.getName() + "~P->" + cds.getName());
        dephosphorylation.setMathName("Dephosphorylation");
        dephosphorylation.AddPart(cds.getName());
        dephosphorylation.setPartDetails(constraints);
        
        Parameter kdeP = new Parameter();
        //"kdeP", "kdeP", dephosphorylationRate)
        kdeP.setName("kdeP");
        kdeP.setParameterType("kdeP");
        kdeP.setScope("Local");
        kdeP.setValue(dephosphorylationRate);
        
        dephosphorylation.AddParameter(kdeP );
        

        Interaction phosphorylatedDegradation = new Interaction();
        phosphorylatedDegradation.setIsInternal(true);
        phosphorylatedDegradation.setIsReaction(true);
        phosphorylatedDegradation.setFreeTextMath(cds.getName() + "~P->");
        phosphorylatedDegradation.setMathName("Degradation");
        phosphorylatedDegradation.setName("Degradation");
        phosphorylatedDegradation.AddPart(cds.getName());
        phosphorylatedDegradation.AddPartDetail(new InteractionPartDetail(cds.getName(), "Phosphorylated", "Input", 1, "Species"));
        
        
        Parameter kd = new Parameter();
        kd.setName("kd");
        kd.setParameterType("kd");
        kd.setScope("Local");
        kd.setValue(degradationRate);
        
        phosphorylatedDegradation.AddParameter(kd);
        
        List<Interaction> interactions = new ArrayList<Interaction>();
        interactions.add(dephosphorylation);
        interactions.add(phosphorylatedDegradation);
        return interactions;
    }
    
     public List<Interaction> getOperatorPoPSModulation(Part part)
    {
        Parameter input = new Parameter();
        //"PoPS", "Input", null);
        input.setName("Input");
        input.setParameterType("PoPS");
        input.setScope("Input");
        
        Parameter output = new Parameter();
        //"PoPS", "Output", null);
        output.setName("Output");
        output.setParameterType("PoPS");
        output.setScope("Output");
      
        Interaction interaction = new Interaction();
        interaction.AddPart(part.getName());
        interaction.setName(part.getName() + "_PoPSModulation");

        interaction.AddParameter(input);
        interaction.AddParameter(output);

        interaction.setInteractionType("OperatorPoPSModulation");
        interaction.setIsInternal(Boolean.TRUE);
        interaction.setIsReaction(Boolean.FALSE);
        interaction.setFreeTextMath("");
        interaction.setMathName("OperatorPoPSModulation");
        
        interaction.setPartDetails(new ArrayList<InteractionPartDetail>());
        
        List<Interaction> interactions = new ArrayList();
        interactions.add(interaction);
        return interactions;
    }
}

 