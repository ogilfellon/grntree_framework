package uk.ac.ncl.icos.svpcompiler.Compiler;

import uk.ac.ncl.icos.svpcompiler.Compilable.Compilable;

import java.util.Collection;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by owengilfellon on 26/09/2014.
 */
public interface ICompiler<T> {

    public boolean addAll(Collection<Compilable> parts);
    public boolean add(Compilable part);
    public T getDocument();
    public boolean partsParsed(List<String> parts);
    public boolean partsAdded(List<String> part);

}
