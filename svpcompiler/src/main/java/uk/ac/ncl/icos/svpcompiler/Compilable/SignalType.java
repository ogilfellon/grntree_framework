package uk.ac.ncl.icos.svpcompiler.Compilable;

/**
 * Created by owengilfellon on 26/09/2014.
 */
public enum SignalType {

    PoPS,
    mRNA,
    RiPS,
    None,
    Species
}
