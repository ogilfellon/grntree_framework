package uk.ac.ncl.icos.svpcompiler.parsing;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import uk.ac.ncl.icos.svpcompiler.Compiler.CompilableFactory;
import uk.ac.ncl.icos.svpcompiler.Compiler.CompilationDirector;
import uk.ac.ncl.icos.svpcompiler.Compiler.SBMLCompiler;

import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Provides a public interface for using the OLDCompiler.
 * @author Owen Gilfellon
 */
public class SVPParser {

    
    /**
     * Takes an SVP Statement and compiles it into a simulatable model.
     * <ul>
     * <li>Operons must begin with a Promoter</li>
     * <li>Promoters can be followed by Operators or RBS Sites</li>
     * <li>Operators can be followed by Operators or RBS Sites</li>
     * <li>RBS can be followed by CDS</li>
     * <li>CDS can be followed by RBS or Terminators</li>
     * </ul>
     * All necessary Transcription Factors for Promoters or Operators must be provided.
     * @param svpstatement The design in SVP Statement format
     * @return An SBML document in String format
     */
    
    public static String getSBML(String svpstatement, String modelId)
    {
        CompilableFactory factory = new CompilableFactory();

        try {
            factory.setCompilables(CompilableFactory.getCompilables(svpstatement, "http://sbol.ncl.ac.uk:8081"));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (XMLStreamException e) {
            e.printStackTrace();
        }
        CompilationDirector director = new CompilationDirector(factory);
        String output = null;
        try {
            output = director.getSBMLString(modelId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return output;
    }

    public static String getSBML(String svpstatement)
    {
        CompilableFactory factory = new CompilableFactory();

        try {
            factory.setCompilables(CompilableFactory.getCompilables(svpstatement, "http://sbol.ncl.ac.uk:8081"));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (XMLStreamException e) {
            e.printStackTrace();
        }
        CompilationDirector director = new CompilationDirector(factory);
        String output = null;
        try {
            output = director.getSBMLString("Untitled");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return output;
    }
    
    public static List<String> getParts(String svpstatement)
    {
        
        try
        {   List<String> sParts = new ArrayList<String>();       
            List<ParsedPart> parts = Evaluation.eval(Parsing.parse(svpstatement));
            for(ParsedPart part:parts)
            {
                sParts.add(part.getVariable());
            }
            return sParts;
        }
        catch(Exception e)
        {
            System.out.println(e.getMessage());
            return null;
        }
    }
}
