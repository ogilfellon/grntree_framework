package uk.ac.ncl.icos.svpcompiler.Compilable;

import org.sbml.jsbml.SBMLDocument;
import org.sbml.jsbml.text.parser.ParseException;
import uk.ac.ncl.intbio.virtualparts.PartsHandler;
import uk.ac.ncl.intbio.virtualparts.entity.Interaction;
import uk.ac.ncl.intbio.virtualparts.entity.Part;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import uk.ac.ncl.intbio.virtualparts.entity.Parameter;

/**
 * Created by owengilfellon on 28/09/2014.
 */
public class Compilable implements ICompilable {

    private List<SignalType> inputSignal = new ArrayList<>();
    private List<SignalType> outputSignal = new ArrayList<>();
    protected List<SBMLModifier> modifiers = new ArrayList<SBMLModifier>();
    private List<Interaction> internalInteractions = new ArrayList<>();
    private Part svp;
    protected SBMLDocument document = null;
    protected Map<Interaction, SBMLDocument> interactions = new HashMap<Interaction, SBMLDocument>();
    //private static final String SVP_REPOSITORY_URL = "http://sbol-dev.ncl.ac.uk:8081";
    private static final String SVP_REPOSITORY_URL = "http://sbol.ncl.ac.uk:8081";
    protected final PartsHandler PART_HANDLER = new PartsHandler(SVP_REPOSITORY_URL);

    /**
     * Constructor for a Compilable encapsulating an SVP with no interactions with other SVPs in model
     * @param svp
     * @param document SBMLDocument for the SVP
     */
    public Compilable(Part svp, List<Interaction> internalInteractions, SBMLDocument document)
    {
        this.document = document;
        this.internalInteractions = internalInteractions;
        setPart(svp);
    }

    /**
     * Constructor for a Compilable encapsulating an SVP with interactions with other SVPs in model
     * @param svp
     * @param document SBMLDocument for the SVP
     * @param interactions Interactions with other SVPs, mapping Interactions to SBMLDocuments
     */
    public Compilable(Part svp, List<Interaction> internalInteractions, SBMLDocument document, Map<Interaction, SBMLDocument> interactions)
    {
        this.document = document;
        this.interactions = interactions;
        this.internalInteractions = internalInteractions;
        setPart(svp);
    }

    public List<SignalType> getInputSignal()
    {
        return inputSignal;
    }

    public List<SignalType> getOutputSignal()
    {
        return outputSignal;
    }

    public void addModifier(SBMLModifier modifier) { modifiers.add(modifier); }

    public Part getPart() { return svp; }

    protected void setPart(Part part)
    {
        
        System.out.println("Setting: " + part.getName());
        
        boolean inputFound = false;
        boolean outputFound = false;
        
        if(part.getName().contains("mRNA")) {
            
            inputFound = true;
            inputSignal.add(SignalType.PoPS);
            outputFound = true;
            outputSignal.add(SignalType.mRNA);
            
        } else if (!part.getType().equalsIgnoreCase("terminator")){
        
            if(internalInteractions!=null) {

                for(Interaction event : internalInteractions) {

                    for(Parameter parameter : event.getParameters()) {


                        System.out.println(part.getName() + " : " + parameter.getName() + " : " + parameter.getParameterType());

                        if(parameter.getParameterType().equalsIgnoreCase("pops")) {
                            if(parameter.getName().contains("Input")) {
                                inputSignal.add(SignalType.PoPS);
                                inputFound = true;
                            } else if (parameter.getName().contains("Output")){
                                outputSignal.add(SignalType.PoPS);
                                outputFound = true;
                            }
                        } else if (parameter.getParameterType().equalsIgnoreCase("rips")) {
                            if(parameter.getName().contains("Input")) {
                                inputSignal.add(SignalType.RiPS);
                                inputFound = true;
                            } else if (parameter.getName().contains("Output")){
                                outputSignal.add(SignalType.RiPS);
                                outputFound = true;
                            }
                        } else if (parameter.getName().contains("mRNA")) {
                            if(parameter.getName().contains("Input")) {
                                inputSignal.add(SignalType.mRNA);
                                inputFound = true;
                            } else if (parameter.getName().contains("Output")){
                                outputSignal.add(SignalType.mRNA);
                                outputFound = true;
                            }
                        }
                    }
                }
            } 
            
        }
        
        if(!inputFound) {
            inputSignal.add(SignalType.None);
        }
        
        if(!outputFound) {
            outputSignal.add(SignalType.None);
        }

        
        this.svp = part;
       
    }

    public SBMLDocument getPartDocument() {

        SBMLDocument filtered = document;

        for(SBMLModifier modifier : modifiers) {
            try {
                filtered = modifier.modify(filtered);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return filtered;
    }

    public Map<Interaction, SBMLDocument> getInteractions() {

        for(Interaction i : interactions.keySet()) {
            for(SBMLModifier modifier : modifiers) {
                try {
                    modifier.modify(interactions.get(i));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }

        return interactions;
    }



}
