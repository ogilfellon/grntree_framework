package uk.ac.ncl.icos.svpcompiler.Compiler;

import org.sbml.jsbml.SBMLDocument;
import uk.ac.ncl.icos.svpcompiler.Compilable.Compilable;
import uk.ac.ncl.icos.svpcompiler.Compilable.ICompilable;
import uk.ac.ncl.icos.svpcompiler.parsing.Evaluation;
import uk.ac.ncl.icos.svpcompiler.parsing.ParsedPart;
import uk.ac.ncl.icos.svpcompiler.parsing.Parsing;
import uk.ac.ncl.intbio.virtualparts.PartsHandler;
import uk.ac.ncl.intbio.virtualparts.entity.Interaction;
import uk.ac.ncl.intbio.virtualparts.entity.Interactions;
import uk.ac.ncl.intbio.virtualparts.entity.Part;

import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by owengilfellon on 29/09/2014.
 */
public class CompilableFactory {


    List<ICompilable> compilables = new  ArrayList<ICompilable>();

    public static List<ICompilable> getCompilable(Part part, List<Interaction> internalInteractions, Interaction externalInteraction, List<Part> interactingParts, String repositoryURL) {


        PartsHandler PART_HANDLER = new PartsHandler(repositoryURL);

        try {
            SBMLDocument partDocument = PART_HANDLER.CreatePartModel(part, internalInteractions);
            List<ICompilable> compilables = new ArrayList<ICompilable>();
            Map<Interaction, SBMLDocument> interactionDocuments = new HashMap<Interaction, SBMLDocument>();
            interactionDocuments.put(externalInteraction, PART_HANDLER.CreateInteractionModel(interactingParts, externalInteraction));

            ICompilable compilable = new Compilable(part, internalInteractions, partDocument, interactionDocuments);
            if(compilable.getPart().getType().equals("RBS")) {
                Part mrnaPart = PART_HANDLER.GetPart("mRNA");
                SBMLDocument mrnaDocument = PART_HANDLER.GetModel(mrnaPart);
                compilables.add(new Compilable(mrnaPart,
                            PART_HANDLER.GetInternalInteractions(mrnaPart).getInteractions(),
                            mrnaDocument));
            }

            compilables.add(compilable);
            return compilables;

        } catch (Exception e) {
            e.printStackTrace();

        }
        return null;
    }

    public static List<ICompilable>  getCompilable(Part part, List<Interaction> internalInteractions, Map<Interaction, List<Part>> externalInteractions, String repositoryURL) {

        PartsHandler PART_HANDLER = new PartsHandler(repositoryURL);

        try {
            SBMLDocument partDocument = PART_HANDLER.CreatePartModel(part, internalInteractions);
            List<ICompilable> compilables = new ArrayList<ICompilable>();
            Map<Interaction, SBMLDocument> interactionDocuments = new HashMap<Interaction, SBMLDocument>();
            for (Interaction interaction : externalInteractions.keySet() ) {
                interactionDocuments.put(interaction, PART_HANDLER.CreateInteractionModel(externalInteractions.get(interaction), interaction));
            }

            ICompilable compilable = new Compilable(part, internalInteractions, partDocument, interactionDocuments);

            if(compilable.getPart().getType().equals("RBS")) {
                Part mrnaPart = PART_HANDLER.GetPart("mRNA");
                SBMLDocument mrnaDocument = PART_HANDLER.GetModel(mrnaPart);
                compilables.add(new Compilable(mrnaPart,
                            PART_HANDLER.GetInternalInteractions(mrnaPart).getInteractions(),
                            mrnaDocument));
            }

            compilables.add(compilable);
            return compilables;

        } catch (Exception e) {
            e.printStackTrace();

        }
        return null;
    }

    public static List<ICompilable> getCompilable(Part part, List<Interaction> internalInteractions, String repositoryURL) {

        PartsHandler PART_HANDLER = new PartsHandler(repositoryURL);

        try {
            SBMLDocument partDocument = PART_HANDLER.CreatePartModel(part, internalInteractions);
            List<ICompilable> compilables = new ArrayList<ICompilable>();


            ICompilable compilable = new Compilable(part, internalInteractions,partDocument);

            if(compilable.getPart().getType().equals("RBS")) {
                Part mrnaPart = PART_HANDLER.GetPart("mRNA");
                SBMLDocument mrnaDocument = PART_HANDLER.GetModel(mrnaPart);
                compilables.add(new Compilable(mrnaPart,
                            PART_HANDLER.GetInternalInteractions(mrnaPart).getInteractions(),
                            mrnaDocument));
            }

            compilables.add(compilable);
            return compilables;

        } catch (Exception e) {
            e.printStackTrace();

        }
        return null;
    }

    public static List<ICompilable> getCompilables(String svpwrite, String repositoryURL) throws IOException, XMLStreamException {


        PartsHandler PART_HANDLER = new PartsHandler(repositoryURL);

        List<ICompilable> compilables = new ArrayList<ICompilable>();
        List<ParsedPart> parts = Evaluation.eval(Parsing.parse(svpwrite));
        for(ParsedPart part: parts) {

            Part svp = PART_HANDLER.GetPart(part.getVariable());

            if(svp!=null) {

                SBMLDocument svpDocument = PART_HANDLER.GetModel(svp);
                Interactions interactions = PART_HANDLER.GetInteractions(svp);
                List<Interaction> internalInteractions = PART_HANDLER.GetInternalInteractions(svp).getInteractions();
                
                Map<Interaction, SBMLDocument> interactionDocuments = null;
                
                

                if(interactions != null && interactions.getInteractions() != null)
                {
                    interactionDocuments = new HashMap<Interaction, SBMLDocument>();

                    for (Interaction interaction : interactions.getInteractions() ) {
                        interactionDocuments.put(interaction, PART_HANDLER.GetInteractionModel(interaction));

                    }

                }


                Compilable c = (interactionDocuments != null ) ?
                        new Compilable(svp, internalInteractions, svpDocument, interactionDocuments) :
                        new Compilable(svp, internalInteractions, svpDocument);


                if(c.getPart().getType().equals("RBS")) {
                    Part mrnaPart = PART_HANDLER.GetPart("mRNA");
                    SBMLDocument mrnaDocument = PART_HANDLER.GetModel(mrnaPart);
                    compilables.add(new Compilable(mrnaPart,
                            PART_HANDLER.GetInternalInteractions(mrnaPart).getInteractions(),
                            mrnaDocument));
                }

                /*
                if(c.getPart().getType().equals("Promoter")) {
                    c.addModifier(new SBMLModifier());
                }
                */

                compilables.add(c);
            }
        }

        return compilables;
    }


    public List<ICompilable> getCompilables() {
        return compilables;
    }

    public void addCompilables(List<ICompilable> parts)
    {
        compilables.addAll(parts);
    }

    public void setCompilables(List<ICompilable> parts)
    {
        compilables.clear();
        compilables.addAll(parts);
    }
}
