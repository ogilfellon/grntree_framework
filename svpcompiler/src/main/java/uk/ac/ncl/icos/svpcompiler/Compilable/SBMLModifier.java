package uk.ac.ncl.icos.svpcompiler.Compilable;

import org.sbml.jsbml.*;
import org.sbml.jsbml.text.parser.ParseException;

/**
 * Created by owengilfellon on 06/10/2014.
 */
public class SBMLModifier {

    public SBMLDocument modify(SBMLDocument document) throws ParseException {

        Model m = document.getModel();
        ListOf<Reaction> reactions = m.getListOfReactions();

        ListOf<Rule> rules = m.getListOfRules();

        for( Rule rule : rules )
        {
            ASTNode mathNode = rule.getMath();
            String mathFormula = JSBML.formulaToString(mathNode);
            System.out.println(mathFormula);
            double increase = 0.0025;
            ASTNode updatedMathNode = JSBML.parseFormula("(" + mathFormula + ")+" + increase);
            rule.setMath(updatedMathNode);
        }
/*
            for( Reaction reaction : reactions ) {



            if( reaction.getName().contains("Production") ) {
                KineticLaw kineticLaw = reaction.getKineticLaw();
                ASTNode mathNode = kineticLaw.getMath();
                String mathFormula = JSBML.formulaToString(mathNode);
                double increase = 5.0;
                ASTNode updatedMathNode = JSBML.parseFormula("(" + mathFormula + ")+" + increase);
                kineticLaw.setMath(updatedMathNode);
            }
        }*/

        return document;
    }



}
