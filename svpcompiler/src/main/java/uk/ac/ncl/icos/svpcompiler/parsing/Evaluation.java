package uk.ac.ncl.icos.svpcompiler.parsing;

import uk.ac.ncl.icos.svpcompiler.parsing.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Evaluation
{
    /**
     * @param e Program expression to be evaluated
     * @return Sequence of parts resulting from evaluation
     */
    public static List<ParsedPart> eval(Expression e)
    {
        return e.accept(new EvalVisitor());
    }

    // implements evaluation rules given in the dissertation
    static private class EvalVisitor implements ExpVisitor<List<ParsedPart>>
    {
        public List<ParsedPart> visit(ParsedPart e)
        {
            return Collections.singletonList(e);
        }

        public List<ParsedPart> visit(Def e)
        {
            // TODO refactor to not use instanceof
            if (e.getRest() instanceof App)
            {
                return e.getBody().accept(new SubstVisitor(e.getParameterNames(), ((App) e.getRest()).getArguments())).accept(this);
            }

            return e.getRest().accept(new AppSubstVisitor(e.getFunctionName(), e.getParameterNames(), e.getBody(),
                    e.getParameterTypes(), e.getReturnType())).accept(this);
        }

        public List<ParsedPart> visit(Var e)
        {
            throw new RuntimeException("This should never occur. " + e + "is not well typed (unbound variable).");
        }

        public List<ParsedPart> visit(App e)
        {
            throw new RuntimeException("This should never occur. " + e + "is not well typed (application of unbound function).");
        }

        public List<ParsedPart> visit(ExpressionList expList)
        {
            List<ParsedPart> parts = new ArrayList<ParsedPart>();

            for (Expression e : expList.getList()) parts.addAll(e.accept(this));

            return parts;
        }
    }

    // implements variable substitution function given in the dissertation
    static private class SubstVisitor implements ExpVisitor<Expression>
    {
        private List<String> vars;
        private List<Expression> expressions;

        public SubstVisitor(List<String> vars, List<Expression> expressions)
        {
            this.vars = vars;
            this.expressions = expressions;
        }

        public Expression visit(ParsedPart e)
        {
            return e;
        }

        public Expression visit(Def e)
        {
            if (e.getParameterNames().equals(vars))
            {
                return new Def(e.getFunctionName(), e.getParameterNames(), e.getParameterTypes(), e.getReturnType(),
                               e.getBody(), e.getRest().accept(this));
            }

            return new Def(e.getFunctionName(), e.getParameterNames(), e.getParameterTypes(), e.getReturnType(),
                           e.getBody().accept(this), e.getRest().accept(this));
        }

        public Expression visit(Var e)
        {
            for (int i = 0; i < vars.size(); i++)
            {
                if (vars.get(i).equals(e.getVar())) return expressions.get(i); 
            }
            
            return e;
        }

        public Expression visit(App e)
        {
            List<Expression> newArguments = new ArrayList<Expression>();
            
            for (Expression arg : e.getArguments())
            {
                newArguments.add(arg.accept(this));
            }
            
            return new App(e.getFunctionName(), newArguments);
        }

        public Expression visit(ExpressionList expList)
        {
            List<Expression> expressions = new ArrayList<Expression>();

            for (Expression e : expList.getList()) expressions.add(e.accept(this));

            return new ExpressionList(expressions);
        }
    }

    // implements application substitution function given in the dissertation
    static private class AppSubstVisitor implements ExpVisitor<Expression>
    {
        private String functionName;
        private List<String> parameterNames;
        private Expression body;
        private List<Type> parameterTypes;
        private Type returnType;

        public AppSubstVisitor(String functionName, List<String> parameterNames, Expression body, List<Type> parameterTypes, Type returnType)
        {
            this.functionName = functionName;
            this.parameterNames = parameterNames;
            this.body = body;
            this.parameterTypes = parameterTypes;
            this.returnType = returnType;
        }

        public Expression visit(ParsedPart e)
        {
            return e;
        }

        public Expression visit(Def e)
        {
            if (e.getFunctionName().equals(functionName)) return e;
            
            return new Def(e.getFunctionName(), e.getParameterNames(), e.getParameterTypes(), e.getReturnType(),
                    e.getBody().accept(this), e.getRest().accept(this));
        }

        public Expression visit(Var e)
        {
            return e;
        }

        public Expression visit(App e)
        {
            if (e.getFunctionName().equals(functionName))
            {
                return new Def(functionName, parameterNames, parameterTypes, returnType, body, e);
            }

            return e;
        }

        public Expression visit(ExpressionList expList)
        {        
            List<Expression> expressions = new ArrayList<Expression>();

            for (Expression e : expList.getList()) expressions.add(e.accept(this));

            return new ExpressionList(expressions);
        }
    }
}
