package uk.ac.ncl.icos.svpcompiler.Compiler;

import org.apache.log4j.PropertyConfigurator;
import org.sbml.jsbml.SBMLDocument;
import uk.ac.ncl.icos.svpcompiler.Compilable.ICompilable;
import uk.ac.ncl.icos.svpcompiler.Compilable.SignalType;
import uk.ac.ncl.intbio.virtualparts.ModelBuilder;
import uk.ac.ncl.intbio.virtualparts.SBML.SBMLHandler;
import uk.ac.ncl.intbio.virtualparts.entity.Interaction;
import uk.ac.ncl.intbio.virtualparts.entity.Parameter;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by owengilfellon on 26/09/2014.
 */
public class SBMLCompiler extends AbstractCompiler<SBMLDocument> {

    private SBMLHandler         SBML_HANDLER;
    private ModelBuilder        MODEL_BUILDER;
    private SBMLDocument        SBML_CONTAINER;

    private ICompilable currPopsEnd = null;
    private List<ICompilable>   popsEnds = new ArrayList<>();
    private ICompilable currRipsEnd = null;

    private SBMLDocument        currPopsDoc = null;
    private SBMLDocument        currRipsDoc = null;
    private SBMLDocument        currMRNADoc = null;
    private List<SBMLDocument>  popsDocs = new ArrayList<>();

    private SBMLDocument        currentDocument = null;
    private SBMLDocument        previousDocument = null;

    private Set<String>         allIds;
    private String filename;

    public SBMLCompiler(String filename)
    {
        PropertyConfigurator.configure("log4j.properties");
        this.filename = filename;
        SBML_HANDLER = new SBMLHandler();
        SBML_CONTAINER = SBML_HANDLER.GetSBMLTemplateModel(this.filename);
        MODEL_BUILDER = new ModelBuilder(SBML_CONTAINER);
        allIds = new HashSet<String>();

    }

    public void addPart(ICompilable part)
    {
        parts.add(part);
    }

    public void setParts(List<ICompilable> parts)
    {
        SBML_CONTAINER = SBML_HANDLER.GetSBMLTemplateModel(this.filename);
        MODEL_BUILDER = new ModelBuilder(SBML_CONTAINER);
        this.parts.addAll(parts);
        currPopsEnd = null;
        currRipsEnd = null;
        // Need to add all other resets here
    }

    public boolean compileNext() throws Exception {
        boolean b = true;

            boolean partAdded = false;

            // =====================================================
            // Reset previous and current parts and documents
            // =====================================================

            currentPart = currentIndex < parts.size() ? parts.get(currentIndex++) : null;

            List<SignalType> inputs = currentPart.getInputSignal();
            List<SignalType> outputs = currentPart.getOutputSignal();
            
            System.out.println("-----" + currentPart.getPart().getName());
            for(SignalType type : inputs) { System.out.println("i: " + type); }
            for(SignalType type : outputs) { System.out.println("o: " + type); }
            
           // if(!inputs.contains(SignalType.None) || !outputs.contains(SignalType.None)) {
                previousDocument = currentDocument;
                System.out.println("prevDoc = currDoc");
            //}
            
            currentDocument =   (addedParts.contains(currentPart.getPart().getName()) && !currentPart.getOutputSignal().contains(SignalType.mRNA))
                                ? MODEL_BUILDER.Clone( currentPart.getPartDocument(), currentPart.getPart())
                                : currentPart.getPartDocument();

            if(currPopsDoc != null && !(currentPart.getInputSignal().contains(SignalType.PoPS) && currentPart.getOutputSignal().contains(SignalType.PoPS))) {
                popsDocs.add(currPopsDoc);
                System.out.println("added PoPS doc");
            }

            System.out.println(currentPart.getPart().getType());

            if(currentPart != null && !currentPart.getPart().getType().equalsIgnoreCase("terminator")) {

                // ===========================================================
                // Determine LINKING / ADDING depending on Signal Type of ParsedPart
                // ===========================================================

                if( !inputs.contains(SignalType.None) || !outputs.contains(SignalType.None)) {
                 //   if( !(inputs.contains(SignalType.PoPS) && popsDocs.isEmpty()) &&
                   //     !(inputs.contains(SignalType.RiPS) && currRipsDoc == null) &&
                     //   !(inputs.contains(SignalType.mRNA) && currMRNADoc == null)) {

                         System.out.println("adding " + currentPart.getPart().getName());

                        if(inputs.contains(SignalType.PoPS) && outputs.contains(SignalType.mRNA)) {
                            for(SBMLDocument popsEnd : popsDocs) {
                                MODEL_BUILDER.Link(popsEnd, currentDocument);
                                 System.out.println("linking pops doc to : " + currentPart.getPart().getName());
                            }
                            currMRNADoc = currentDocument;
                        } 
                        
                        if (inputs.contains(SignalType.RiPS)) {
                            MODEL_BUILDER.Link(currRipsDoc, currentDocument);
                            System.out.println("linking rips doc to : " + currentPart.getPart().getName());
                        } 
                        
                        if (inputs.contains(SignalType.mRNA)) {
                            System.out.println("linking mrna doc to : " + currentPart.getPart().getName());
                            MODEL_BUILDER.Link(currMRNADoc, currentDocument);
                        } 
                        /*
                        
                        if (previousDocument != null && !inputs.contains(SignalType.None)) {
                            System.out.println("linking prev part to : " + currentPart.getPart().getName());
                            MODEL_BUILDER.Link(previousDocument, currentDocument);
                        }*/

                        MODEL_BUILDER.Add(currentDocument);
                        addedParts.add(currentPart.getPart().getName());

                        // =====================================================
                        // Store Sticky Signal Ends, as needed
                        // =====================================================
                        
                        // Need to extend these?

                        currPopsDoc = outputs.contains(SignalType.PoPS) ? currentDocument : null;
                        currRipsDoc = outputs.contains(SignalType.RiPS) ? currentDocument : null;

                        currPopsEnd = outputs.contains(SignalType.PoPS) ? currentPart : null;
                        currRipsEnd = outputs.contains(SignalType.RiPS) ? currentPart : null;
                        partAdded = true;
                    }
                } else {
                    currPopsEnd = null;
                    currPopsDoc = null;
                    currRipsDoc = null;
                    currRipsEnd = null;
                    currMRNADoc = null;
                    popsEnds.clear();
                    popsDocs.clear();
                }
          //  } else {
           //     b = false;
           // }

            // =================================
            // Add interactions here
            // =================================

            if (partAdded && currentPart.getInteractions() != null) {
                for(Interaction interaction:currentPart.getInteractions().keySet()) {
                    if(this.partsParsed(interaction.getParts())) {

                        SBMLDocument interactionDocument = addedParts.contains(interaction.getName())
                                            ? MODEL_BUILDER.CloneInteraction(currentPart.getInteractions().get(interaction), interaction)
                                            : currentPart.getInteractions().get(interaction);

                        List<Parameter> ps = interaction.getParameters();

                        boolean input = false;
                        boolean output = false;

                        for(Parameter p : ps) {
                            if(p.getScope().equals("Input")) { input = true;}
                            if(p.getScope().equals("Output")) { output = true;}
                        }

                        // If has input and output, is transcriptional regulation
                        // TODO Remove assumption of PoPS Regulation!

                        if ( input && output && outputs.contains(SignalType.PoPS)) {

                            addedParts.add(interaction.getName());

                            // Update contextual information, then link and add

                            currPopsDoc = interactionDocument;
                            previousDocument = currentDocument;
                            currentDocument = interactionDocument;

                            MODEL_BUILDER.Link(previousDocument, currentDocument);
                            MODEL_BUILDER.Add(currentDocument);
                        }

                        // Else is a protein-protein interaction, no contextual information, or linking required

                        else if (!input && !output && this.partsAdded(interaction.getParts())) {
                            addedParts.add(interaction.getName());
                            MODEL_BUILDER.Add(interactionDocument);
                        }
                    }
                }
            }

            setChanged();
            notifyObservers();


        return b;
    }

    public boolean compileAll() throws Exception {
        boolean b = true;

        while(currentIndex < parts.size()) {
            if (!this.compileNext()) {
                b = false;
            }
        }

        return b;
    }

    @Override
    public SBMLDocument getDocument()
    {
        try {
            return MODEL_BUILDER.GetModel();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public String getModelString()
    {
        try {
            return MODEL_BUILDER.GetModelString();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public ICompilable getCurrPopsEnd() {
        return currPopsEnd;
    }

    public ICompilable getCurrRipsEnd() {
        return currRipsEnd;
    }

    public List<ICompilable> getPopsEnds() {
        return popsEnds;
    }

    public SBMLDocument getCurrPopsDoc() {
        return currPopsDoc;
    }

    public SBMLDocument getCurrRipsDoc() {
        return currRipsDoc;
    }

    public List<SBMLDocument> getPopsDocs() {
        return popsDocs;
    }
}
