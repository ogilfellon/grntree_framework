package uk.ac.ncl.icos.svpcompiler.tests;

import org.apache.log4j.PropertyConfigurator;
import uk.ac.ncl.icos.svpcompiler.Compiler.CompilableFactory;

import javax.xml.stream.XMLStreamException;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by owengilfellon on 29/09/2014.
 */
public class Test {

    public static void main(String[] args) {
        PropertyConfigurator.configure("log4j.properties");
        CompilableFactory factory = new CompilableFactory();

        try {
            factory.addCompilables(CompilableFactory.getCompilables("PspaS:Prom; RBS_SpaR:RBS; SpaR:CDS; RBS_SpaK:RBS; SpaK:CDS; BO_4296:Ter; PspaS:Prom; RBS_SpaS:RBS; GFP_rrnb:CDS; BO_4296:Ter",
                    "http://sbol.ncl.ac.uk:8081"));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (XMLStreamException e) {
            e.printStackTrace();
        }
        uk.ac.ncl.icos.svpcompiler.Compiler.CompilationDirector director = new uk.ac.ncl.icos.svpcompiler.Compiler.CompilationDirector(factory);

        try
        {
            String sbmlOutput = director.getSBMLString("TestModel");
            FileWriter f = new FileWriter( "feedback" + ".xml" );
            f.write( sbmlOutput );
            f.flush();
        } catch( Exception e ) {
            System.out.println( e.getMessage() );

    }

}
}
