package uk.ac.ncl.icos.svpcompiler.parsing;



import uk.ac.ncl.icos.svpcompiler.parsing.*;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

/**
 * Implements the type system from the dissertation
 */
class Typing
{ 
    /**
     * @param e Program expression to typed
     * @return Type of e
     * @throws TypingException if e is not well-typed
     */
    public static Type typeOf(Expression e) throws TypingException
    {
        return e.accept(new TypingVisitor());
    }

    public static class TypingException extends RuntimeException
    {
        public TypingException(String message)
        {
            super("Type error:" + message);
        }
    }

    // represents the type of a function as the types of its parameters and its return type
    static private class FunctionType
    {
        private List<Type> parameterTypes;
        private Type returnType;

        public FunctionType(List<Type> parameterTypes, Type returnType)
        {
            this.parameterTypes = parameterTypes;
            this.returnType = returnType;
        }

        public List<Type> getParameterTypes()
        {
            return parameterTypes;
        }

        public Type getReturnType()
        {
            return returnType;
        }
    }

    // implements typing rules from the dissertation
    static private class TypingVisitor implements ExpVisitor<Type>
    {
        private Map<String, Type> variableTypings;
        private Map<String, FunctionType> functionTypings;

        public TypingVisitor()
        {
            variableTypings = new Hashtable<String, Type>();
            functionTypings = new Hashtable<String, FunctionType>();
        }

        public TypingVisitor(Map<String, Type> variableTypings, Map<String, FunctionType> functionTypings)
        {
            this.variableTypings = new Hashtable<String, Type>(variableTypings);
            this.functionTypings = new Hashtable<String, FunctionType>(functionTypings);
        }

        public Type visit(ParsedPart e)
        {
            return new Type(e.getType(), e.getType());
        }

        public Type visit(Var e)
        {
            if (!variableTypings.containsKey(e.getVar())) throw new TypingException("Unbound variable " + e);
            
            return variableTypings.get(e.getVar());
        }

        public Type visit(App e)
        {
            if (!functionTypings.containsKey(e.getFunctionName())) throw new TypingException("Application of undefined function: " + e);

            FunctionType ft = functionTypings.get(e.getFunctionName());
            
            if (ft.getParameterTypes().size() != e.getArguments().size()) throw new TypingException("Invalid number of arguments in " + e);
            
            for (int i = 0; i < ft.getParameterTypes().size(); i++)
            {
                if (!ft.getParameterTypes().get(i).equals(e.getArguments().get(i).accept(this)))
                {
                    throw new TypingException("Mismatched argument types in " + e);
                }
            }

            return ft.getReturnType();
        }

        public Type visit(Def e)
        {
            if (e.getParameterNames().size() != e.getParameterTypes().size())
            {
                throw new TypingException("Should never occur. Number of parameters does not match number of parameter types in " + e);            
            }
            
            Hashtable<String, Type> updatedVariableTypings = new Hashtable<String, Type>(variableTypings);
            for (int i = 0; i < e.getParameterNames().size(); i++)
            {
                updatedVariableTypings.put(e.getParameterNames().get(i), e.getParameterTypes().get(i));
            }
            
            Type bodyType = e.getBody().accept(new TypingVisitor(updatedVariableTypings, functionTypings));

            if (bodyType.equals(e.getReturnType()))
            {
                Hashtable<String, FunctionType> updatedFunctionTypings = new Hashtable<String, FunctionType>(functionTypings);
                updatedFunctionTypings.put(e.getFunctionName(), new FunctionType(e.getParameterTypes(), e.getReturnType()));

                return e.getRest().accept(new TypingVisitor(variableTypings, updatedFunctionTypings));
            }
            
            throw new TypingException("Type of function body does not match function return type in" + e + "."
                                    + "Body type: " + bodyType + ". Return type: " + e.getReturnType());
        }

        public Type visit(ExpressionList e)
        {
            List<Expression> expressions = e.getList();

            if (expressions.size() == 1) return expressions.get(0).accept(this);   

            BaseType lhs = expressions.get(0).accept(this).getLhs();
            
            BaseType rhs = null;

            for (int i = 1; i < expressions.size(); i++)
            {
                rhs = typeCheckComposition(expressions.get(i-1), expressions.get(i));
            }

            if (rhs == null) throw new TypingException("Should never occur. Empty expression list");

            return new Type(lhs, rhs);
        }

        // return rhs type of composition e1;e2 or throw TypingException if invalid 
        private BaseType typeCheckComposition(Expression e1, Expression e2)
        {
            Type e1Type = e1.accept(this);
            Type e2Type = e2.accept(this);
            BaseType e1RhsType = e1Type.getRhs();
            BaseType e2LhsType = e2Type.getLhs();
            BaseType e2RhsType = e2Type.getRhs();

            if (e1RhsType == BaseType.PROM && e2LhsType == BaseType.PROM ||
                e1RhsType == BaseType.PROM && e2LhsType == BaseType.RBS ||
              
                e1RhsType == BaseType.RBS && e2LhsType == BaseType.CDS ||
             
                e1RhsType == BaseType.CDS && e2LhsType == BaseType.RBS ||
                e1RhsType == BaseType.CDS && e2LhsType == BaseType.TER ||
                e1RhsType == BaseType.CDS && e2LhsType == BaseType.PROM ||

                e1RhsType == BaseType.TER && e2LhsType == BaseType.PROM)
            {
                return e2RhsType;
            }

            throw new TypingException("Invalid composition between " + e1 + " and " + e2);
        }
    }
}