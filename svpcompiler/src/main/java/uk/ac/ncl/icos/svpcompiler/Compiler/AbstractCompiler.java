package uk.ac.ncl.icos.svpcompiler.Compiler;

import uk.ac.ncl.icos.svpcompiler.Compilable.Compilable;
import uk.ac.ncl.icos.svpcompiler.Compilable.ICompilable;
import uk.ac.ncl.intbio.virtualparts.entity.Part;

import java.util.*;

/**
 * Created by owengilfellon on 26/09/2014.
 */
public abstract class AbstractCompiler<T> extends Observable implements ICompiler<T> {

    // Maintains the list of parts to be compiled
    protected List<ICompilable> parts = new LinkedList<ICompilable>();
    protected int currentIndex = 0;

    protected ICompilable previousPart = null;
    protected ICompilable currentPart = null;


    // Parts currently added to the model to allow contextual addition of interactions
    protected ArrayList<String>   addedParts = new ArrayList<String>();

    // IDs currently used within the model, to allow enforcing of unique IDs
    private HashSet<String>     allIds;

    public boolean add(Compilable part)
    {
        previousPart = currentPart;
        currentPart = part;
        return this.parts.add(part);
    }

    public boolean addAll(Collection<Compilable> parts)
    {
        boolean returnValue = true;

        for(Compilable part : parts) {
            boolean b = this.add(part);
            if(!b) {
                returnValue = false;
            }
        }

        return returnValue;
    }

    public boolean partsParsed(List<String> parts)
    {
        Iterator<String> i = parts.iterator();
        HashMap<String, Part> modelParts = new HashMap<String, Part>();
        Iterator<ICompilable> j = this.parts.listIterator();

        while(j.hasNext()) {
            Part parsedPart = j.next().getPart();
            modelParts.put(parsedPart.getName(), parsedPart);
        }

        while(i.hasNext()) {
            if( !modelParts.containsKey( i.next() ) ) {
                return false;
            }
        }

        return true;
    }

    @Override
    public boolean partsAdded(List<String> part) {

        Iterator<String> j = part.listIterator();

        while (j.hasNext()) {
            if( !addedParts.contains(j.next())) {
                return false;
            }
        }

        return true;
    }

    public ICompilable getCurrentPart() {
        return currentPart;
    }

    public ICompilable getPreviousPart() {
        return previousPart;
    }
}
