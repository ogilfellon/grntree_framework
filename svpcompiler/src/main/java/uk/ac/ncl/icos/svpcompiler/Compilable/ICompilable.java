package uk.ac.ncl.icos.svpcompiler.Compilable;

import java.util.List;
import org.sbml.jsbml.SBMLDocument;
import uk.ac.ncl.intbio.virtualparts.entity.Interaction;
import uk.ac.ncl.intbio.virtualparts.entity.Part;

import java.util.Map;

/**
 * Created by owengilfellon on 28/09/2014.
 */
public interface ICompilable {

    public Part getPart();

    public SBMLDocument getPartDocument();

    public Map<Interaction, SBMLDocument> getInteractions();

    public List<SignalType> getInputSignal();

    public List<SignalType> getOutputSignal();

}
