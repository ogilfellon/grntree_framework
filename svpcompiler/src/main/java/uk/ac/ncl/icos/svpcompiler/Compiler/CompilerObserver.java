package uk.ac.ncl.icos.svpcompiler.Compiler;

import java.util.Observable;
import java.util.Observer;

/**
 * Created by owengilfellon on 29/09/2014.
 */
public class CompilerObserver implements Observer {

    @Override
    public void update(Observable o, Object arg) {

        if(o instanceof SBMLCompiler) {
            SBMLCompiler ode = (SBMLCompiler) o;
            String format = "%-15s %-20s %-15s %-15s %-20s %n";
            String divider = "--------------------------------------------------";
            //String subformat = "%-30s %30s %n";
            System.out.println(divider);

            String popsEnd = ode.getCurrPopsEnd() != null ? ode.getCurrPopsEnd().getPart().getName() : "None";
            String ripsEnd = ode.getCurrRipsEnd() != null ? ode.getCurrRipsEnd().getPart().getName() : "None";
            String currPart = ode.getCurrentPart() != null ? ode.getCurrentPart().getPart().getName() : "None";
            String prevPart = ode.getPreviousPart() != null ? ode.getPreviousPart().getPart().getName() : "None";


            System.out.printf(format, "Current ParsedPart", "# of Sticky Ends", "PoPS End", "RiPS End", "# of Interactions");
            System.out.printf(format,   currPart,

                                        ode.getPopsDocs().size(),
                                        popsEnd,
                                        ripsEnd,
                                        ode.getCurrentPart().getInteractions().keySet().size());
        }
    }
}
