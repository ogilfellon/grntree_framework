package uk.ac.ncl.icos.svpcompiler.parsing;




enum BaseType { PROM, CDS, RBS, TER, OP }

/**
 * A Type is a pair of base types - base type of the left most part and base type of the right most part  
 */
class Type
{
    private BaseType lhs;
    private BaseType rhs;
    
    public Type(BaseType lhs, BaseType rhs)
    {
        this.lhs = lhs;
        this.rhs = rhs;
    }
    
    public BaseType getLhs()
    {
        return lhs;
    }
    
    public BaseType getRhs()
    {
        return rhs;
    }
    
    public String toString()
    {
        return "<" + lhs + ", " + rhs + ">";
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((lhs == null) ? 0 : lhs.hashCode());
        result = prime * result + ((rhs == null) ? 0 : rhs.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Type other = (Type) obj;
        if (lhs == null)
        {
            if (other.lhs != null)
                return false;
        }
        else if (!lhs.equals(other.lhs))
            return false;
        if (rhs == null)
        {
            if (other.rhs != null)
                return false;
        }
        else if (!rhs.equals(other.rhs))
            return false;
        return true;
    }
}