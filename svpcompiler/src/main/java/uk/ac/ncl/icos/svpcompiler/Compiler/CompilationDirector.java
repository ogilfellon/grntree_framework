package uk.ac.ncl.icos.svpcompiler.Compiler;

import org.sbml.jsbml.SBMLDocument;
import uk.ac.ncl.icos.svpcompiler.Compilable.ICompilable;

import java.util.List;

/**
 * Created by owengilfellon on 29/09/2014.
 */
public class CompilationDirector {

    private CompilableFactory factory;
    private SBMLCompiler compiler;
    private List<ICompilable> compilable;

    public CompilationDirector(CompilableFactory factory)
    {
        this.factory = factory;
    }

    private void compileSBML() throws Exception
    {
        this.compilable = factory.getCompilables();
        this.compiler.setParts(this.compilable);
        this.compiler.compileAll();
    }

    public SBMLDocument getSBML(String modelId) throws Exception
    {
        this.compiler = new SBMLCompiler(modelId);
        this.compileSBML();
        return compiler.getDocument();
    }

    public String getSBMLString(String modelId) throws Exception
    {
        this.compiler = new SBMLCompiler(modelId);
        this.compileSBML();
        return compiler.getModelString();
    }
}
