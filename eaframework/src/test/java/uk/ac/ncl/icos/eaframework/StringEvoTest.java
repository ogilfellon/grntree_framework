package uk.ac.ncl.icos.eaframework;

import org.apache.log4j.PropertyConfigurator;
import uk.ac.ncl.icos.eaframework.chromosome.EvaluatedChromosome;
import uk.ac.ncl.icos.eaframework.chromosome.StringChromosome;
import uk.ac.ncl.icos.eaframework.evoengines.EvoEngine;
import uk.ac.ncl.icos.eaframework.evoengines.StringEngine;
import uk.ac.ncl.icos.eaframework.factories.ChromosomeFactory;
import uk.ac.ncl.icos.eaframework.factories.StringChromosomeFactory;
import uk.ac.ncl.icos.eaframework.fitness.Evaluator;
import uk.ac.ncl.icos.eaframework.fitness.StringAllOnes;
import uk.ac.ncl.icos.eaframework.observer.EvolutionObserver;
import uk.ac.ncl.icos.eaframework.observer.SimplePopObserver;
import uk.ac.ncl.icos.eaframework.operator.Operator;
import uk.ac.ncl.icos.eaframework.operator.StringOperatorGroup;
import uk.ac.ncl.icos.eaframework.operator.StringPointMutation;
import uk.ac.ncl.icos.eaframework.selection.Selection;
import uk.ac.ncl.icos.eaframework.selection.SigmaScaledRouletteWheel;
import uk.ac.ncl.icos.eaframework.terminationconditions.FitnessTermination;
import uk.ac.ncl.icos.eaframework.terminationconditions.GenerationTermination;
import uk.ac.ncl.icos.eaframework.terminationconditions.TerminationCondition;
import uk.ac.ncl.icos.eaframework.terminationconditions.TerminationGroup;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author owengilfellon
 */
public class StringEvoTest {
    
    static final int POPULATION_SIZE = 5;
    static final int MUTATIONS_PER_GENERATION = 1;
    static final int NUMBER_OF_RUNS = 1;
       
    public static void main(String[] args)
    {  
   
        PropertyConfigurator.configure("log4j.properties");
        
        /*
         * Create initial model by passing SVPWrite string to GRNTreeFactory.
         */

        //GRNTree c = GRNTreeFactory.getGRNTree("PspaRK:Prom; RBS_SpaR:RBS; SpaR:CDS; RBS_SpaK:RBS; SpaK:CDS; BO_4296:Ter; PspaS:Prom; RBS_SpaS:RBS; GFP_rrnb:CDS; BO_4296:Ter");
        List<StringChromosome> seed = new ArrayList<>();
        seed.add(new StringChromosome("01010001100101100010"));
        //seed.add(GRNTreeFactory.getGRNTree("PspaS:Prom; RBS_SpaR:RBS; SpaR:CDS; RBS_SpaK:RBS; SpaK:CDS; BO_27925:RBS; BO_31152:CDS; BO_4296:Ter; BO_27654:Prom; RBS_SpaS:RBS; GFP_rrnb:CDS; BO_4296:Ter; BO_3017:Prom; BO_27814:RBS; SpaR:CDS; BO_27896:RBS; SpaK:CDS; BO_6486:Ter"));
        ChromosomeFactory<StringChromosome> cf = new StringChromosomeFactory(seed, POPULATION_SIZE);
        
        /*
         * Create all Mutation Operators, and add to an OperatorGroup by passing
         * them to the OperatorGroup constructor as a List
         */
        List<Operator<StringChromosome>> operators = new ArrayList<>();
        operators.add(new StringPointMutation());
        
        /*
        
        operators.add(new DuplicateParts());
        operators.add(new DuplicateTU());
        operators.add(new RandomiseConstPromoter());
        operators.add(new RandomiseRBS());
        operators.add(new RemoveParts());
        operators.add(new RemoveRegulation());
        operators.add(new SplitTU());
        operators.add(new SwapParts());
        * * */
        Operator<StringChromosome> o = new StringOperatorGroup(operators, MUTATIONS_PER_GENERATION);
        

        /*
         * Create multiple Termination Conditions by passing them to a
         * Termination Group in the same manner as the Operators were
         * created.
         */
  
        List<TerminationCondition> tc = new ArrayList<>();
        tc.add(new GenerationTermination(5000));  
        tc.add(new FitnessTermination(100.0));
        //tc.add(new NoIncreaseTermination(100));
        TerminationCondition t = new TerminationGroup(tc);
        
      
        Selection<StringChromosome> s = new SigmaScaledRouletteWheel<>(2);
        //Selection<EvaluatedChromosome<GRNTree>> s = new SigmaScaledRouletteWheel<EvaluatedChromosome<GRNTree>>();
        
        /*
         * An evaluator is created, passing the simulator as an argument for
         * the purposes of assessing the SBML models.
         */  
        
        Evaluator<StringChromosome> f = new StringAllOnes();
        
        /*
         * Create the EA, passing the Chromosome, Operators, Fitness Evaluators
         * and Termination Conditions.
         */
        
        
        for(int i=0; i<NUMBER_OF_RUNS; i++)
        {
            EvoEngine<StringChromosome> e = new StringEngine(cf, o, s, f, t);
        
            /*
             * Create an SVPTreeObserver implementing EvolutionObserver. SVPTreeObserver
             * takes a list of metabolite names, the timecourses of which are
             * retrieved from the COPASI simulations and recorded to disk.
             * 
             * The EvolutionObserver is attached to the EvoEngine.
             */

            EvolutionObserver observer = new SimplePopObserver();
           // EvolutionObserver observer = new StringResultsFileWriter();
            e.attach(observer);


            /*
             * Finally, the algorithm is run. The algorithm will continue until one
             * of the termination conditions is met, and output it's progress to
             * console, or to disk, depending on the EvolutionObserver used.
             */

            e.run(); 
        }
        
    }
    
     
}
