/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.terminationconditions;

import uk.ac.ncl.icos.annotation.EAModule;
import uk.ac.ncl.icos.eaframework.stats.PopulationStats;

import java.io.Serializable;

/**
 *
 * @author owengilfellon
 */
@EAModule( visualName = "Function Minimisation")
public class MinimisationFitnessTermination implements TerminationCondition, Serializable {

    @Override
    public boolean shouldTerminate(PopulationStats es) {
        if(es.getMeanFitness() > 0.0)
        {
            return false;
        }
        else
        {
            System.out.println("Minimisation Fitness Termination");
            return true;
        }
    }
    
    
    
}
