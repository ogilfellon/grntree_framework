/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.fitness;

import uk.ac.ncl.icos.datatypes.Fitness;
import uk.ac.ncl.icos.eaframework.chromosome.StringChromosome;

/**
 *
 * @author owengilfellon
 */
public class StringAllOnes implements Evaluator<StringChromosome> {

    @Override
    public Fitness evaluate(StringChromosome c) {
        
        char[] chromosome = c.getString().toCharArray();
        int count = 0;
        for(int i=0; i<chromosome.length; i++)
        {
            if(chromosome[i] == '1')
            {
                count++;
            }
        }
        double fitness = ((double)count / (double)chromosome.length) * 100.0;
        
        return new Fitness(fitness);

    }
    
    
    
}
