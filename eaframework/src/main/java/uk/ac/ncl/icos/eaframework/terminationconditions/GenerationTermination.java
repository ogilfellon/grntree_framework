/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.terminationconditions;

import uk.ac.ncl.icos.annotation.EAModule;
import uk.ac.ncl.icos.eaframework.stats.PopulationStats;

import java.io.Serializable;

/**
 *
 * @author owengilfellon
 */
@EAModule( visualName = "Generation Limit")
public class GenerationTermination implements TerminationCondition, Serializable {
    
    private int maxGenerations;

    public GenerationTermination(int maxGenerations) {
        this.maxGenerations = maxGenerations;
    }

    @Override
    public boolean shouldTerminate(PopulationStats es) {
        if(es.getCurrentGeneration() >= maxGenerations)
        {
            System.out.println("Generation Termination");
            return true;
        }
        else
        {
            return false;
        }
    }
}
