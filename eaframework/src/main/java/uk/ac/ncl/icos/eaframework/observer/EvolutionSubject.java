/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.observer;

/**
 *
 * @author owengilfellon
 */
public interface EvolutionSubject<T> {
    
    public void attach(EvolutionObserver<T> o);
    public void dettach(EvolutionObserver<T> d);
    public void alert();
}
