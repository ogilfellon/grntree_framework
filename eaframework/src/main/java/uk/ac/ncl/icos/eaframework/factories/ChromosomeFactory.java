/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.factories;

import uk.ac.ncl.icos.eaframework.chromosome.Chromosome;
import uk.ac.ncl.icos.eaframework.operator.Operator;

import java.util.List;

/**
 *
 * @author owengilfellon
 */
public interface ChromosomeFactory<T extends Chromosome> {
    
    
    /**
     * Generates a population of n random chromosomes
     * @return 
     */
    public List<T> generatePopulation();
    
    /**
     * Generates a population of n chromosomes, each with variations to the provided Chromosome
     * @param operators
     * @return 
     */
    public List<T> generatePopulation(List<Operator<T>> operators);
    
    public int getPopulationSize();
    
              
}
