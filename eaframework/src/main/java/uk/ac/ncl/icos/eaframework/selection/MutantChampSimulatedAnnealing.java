/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.selection;

import uk.ac.ncl.icos.annotation.EAModule;
import uk.ac.ncl.icos.eaframework.chromosome.Chromosome;
import uk.ac.ncl.icos.eaframework.chromosome.EvaluatedChromosome;
import uk.ac.ncl.icos.eaframework.scheduler.ParameterScheduler;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 *
 * @author owengilfellon
 */
@EAModule( visualName = "Mutant Champ Simulated Annealing")
public class MutantChampSimulatedAnnealing<T extends Chromosome> extends AbstractMutantChamp<T> {

    private ParameterScheduler scheduler = null;

    public MutantChampSimulatedAnnealing(ParameterScheduler scheduler) {
        super();
        this.scheduler = scheduler;
    }

    public MutantChampSimulatedAnnealing(EvaluatedChromosome bestChromosome, ParameterScheduler scheduler) {
        super(bestChromosome);
        this.scheduler = scheduler;
    }

    @Override
    public List<T> select(List<EvaluatedChromosome<T>> population) {
        
        List<T> selected = new ArrayList<>();
        
        if(population.size()!=1){
            throw new IllegalArgumentException("MutantChamp population size must be 1");
        }

        if(bestChromosome == null ||
          (population.get(0).getFitness().getFitness() > bestChromosome.getFitness().getFitness())) {
            bestChromosome = population.get(0);
        }
        else {

            double val = population.get(0).getFitness().getFitness() *
                    (new Random().nextDouble() * scheduler.getParameter());

            if( val > bestChromosome.getFitness().getFitness()) {
                bestChromosome = population.get(0);
                //System.out.println("Fit: " + bestChromosome.getFitness().getFitness() + " | Val: " + val);
                //System.out.println("======ACCEPTED LOWER=======");
            }
        }
        
        selected.add(bestChromosome.getChromosome());
        
        return selected;
    }
}
