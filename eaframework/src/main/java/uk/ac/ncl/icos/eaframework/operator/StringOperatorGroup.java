package uk.ac.ncl.icos.eaframework.operator;

import uk.ac.ncl.icos.eaframework.chromosome.StringChromosome;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 *
 * @author owengilfellon
 */
public class StringOperatorGroup extends AbstractOperator<StringChromosome> {
    
    List<Operator<StringChromosome>> operators = new ArrayList<Operator<StringChromosome>>();
    int mutationsPerGeneration = 1;
    Operator<StringChromosome> o;

    public StringOperatorGroup(List<Operator<StringChromosome>> operators) {
        this.operators = operators;
    }
    
    public StringOperatorGroup(List<Operator<StringChromosome>> operators, int mutationsPerGeneration) {
        this(operators);
        this.mutationsPerGeneration = mutationsPerGeneration;      
    }
    
    @Override
    public StringChromosome apply(StringChromosome c) {
        
        StringChromosome s = c;
        
        for(int i=0; i<mutationsPerGeneration; i++)
        {
            o = operators.get(new Random().nextInt(operators.size()));
            s = o.apply(s);
        }
        
        return s;
    }

    @Override
    public Operator<StringChromosome> getOperator() {
        return o;
    }
    
    
}
