/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.terminationconditions;

import uk.ac.ncl.icos.eaframework.stats.PopulationStats;

/**
 *
 * @author owengilfellon
 */
public interface TerminationCondition {
    
    public boolean shouldTerminate(PopulationStats es);
    
}
