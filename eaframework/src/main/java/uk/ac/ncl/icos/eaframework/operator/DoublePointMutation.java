package uk.ac.ncl.icos.eaframework.operator;

import uk.ac.ncl.icos.eaframework.chromosome.DoubleChromosome;

import java.util.Random;

/**
 * Selects a random Transcriptional Unit (A) from the GRNTree. A random RBS node within
 * the TU is identified as a split point. The parts following the split point are
 * replaced with an RBS, CDS and terminator, with the CDS coding for a TF for the 
 * inducible promoter of a newly created TU (B). The parts that are removed from A
 * are placed into B.
 * 
 * @author owengilfellon
 */
public class DoublePointMutation extends AbstractOperator<DoubleChromosome> {
    
 
    @Override
    public DoubleChromosome apply(DoubleChromosome c) {
            double[] chromosome = c.getChromosome();
            Random r = new Random();
            int point = r.nextInt(chromosome.length);
            chromosome[point] = r.nextDouble();
            return new DoubleChromosome(chromosome);
    }
}
