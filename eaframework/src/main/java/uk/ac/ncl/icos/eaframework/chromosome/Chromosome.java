/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.chromosome;

/**
 *
 * @author owengilfellon
 */
public interface Chromosome {
    
    public Chromosome duplicate();
    
}
