/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.filemanagement;

import org.w3c.dom.Document;
import uk.ac.ncl.icos.datatypes.ResponseCurve;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.util.ArrayList;
import java.util.regex.Pattern;

/**
 *
 * @author b1050029
 * Utility class for exporting results from EA runs
 */
public class Export {

    public static boolean exportString(String title,
                                       String resultsDirectory,
                                       String experimentGroup,
                                       String experimentDirectory,
                                       String filename,
                                       String columnHeaderText,
                                       String contents,
                                       boolean append) {

        String fileContents = columnHeaderText == null ? contents : columnHeaderText + "\n" + contents;

        try {

            final File homeDir = new File(System.getProperty("user.home"));
            File directory = new File(homeDir + "/" +  resultsDirectory + "/" + experimentGroup + "/" +  experimentDirectory);
            if (!directory.exists()) {
                if (!directory.mkdirs()) {
                      System.out.println("Couldn't create directory: " + resultsDirectory);
                };
            }
            
            /*
            File exDirectory = new File(directory,  experimentDirectory);
            if (!directory.exists()) {
                if (!directory.mkdir()) {
                      System.out.println("Couldn't create directory: " + resultsDirectory);
                };
            }
            */
       

            File file = new File(directory + "/" + filename);
            BufferedWriter writer = new BufferedWriter(new FileWriter(file, append));
            writer.write(fileContents);
            writer.close();
        } catch (Exception e) {
            return false;
        }

        return true;

    }

    public static boolean exportResults(String title,
                                        String resultsDirectory,
                                        String experimentGroup,
                                        String experimentDirectory,
                                        String filename,
                                        String columnLabels,
                                        ResponseCurve results,
                                        boolean append) {

        String s = "";

        // Add column titles

        if(title!=null)
        {
            s += title + "\n";
        }

        
        if(columnLabels!=null)
        {
            for (int i = 0; i < results.getResults().get(0).size(); i++) {

                if (i == (results.getResults().get(0).size() - 1)) {
                    s += columnLabels + "_" + (i + 1) + "\n";
                } else {
                    s += columnLabels + "_" + (i + 1) + " ";
                }
            }
        }

        for (int i = 0; i < results.getResults().size(); i++) {
            for (int j = 0; j < results.getResults().get(i).size(); j++) {
                if (j == (results.getResults().get(i).size() - 1)) {
                    s += results.getResults().get(i).get(j).doubleValue() + "\n";
                } else {
                    s += results.getResults().get(i).get(j).doubleValue() + " ";
                }
            }
        }

        try {

             final File homeDir = new File(System.getProperty("user.home"));
            File directory = new File(homeDir + "/" +  resultsDirectory + "/" + experimentGroup + "/" + experimentDirectory);
            if (!directory.exists()) {
                if (!directory.mkdirs()) {
                      System.out.println("Couldn't create directory: " + resultsDirectory);
                };
            }
            
            /*
            File exDirectory = new File(directory,  experimentDirectory);
            if (!directory.exists()) {
                if (!directory.mkdir()) {
                      System.out.println("Couldn't create directory: " + resultsDirectory);
                };
            }
            */
       

            File file = new File(directory + "/" + filename);

            BufferedWriter writer = new BufferedWriter(new FileWriter(file, append));
            writer.write(s);
            writer.close();
        } catch (Exception e) {
            return false;
        }

        return true;

    }
    
     public static boolean exportDoubles(String title,
                                         String resultsDirectory,
                                         String experimentGroup,
                                         String experimentDirectory,
                                         String filename,
                                         ArrayList<Double> results,
                                         boolean append) {

        String s = "";

        // Add column titles

        if(title!=null)
        {
            s += title + "\n";
        }

  

        for (int i = 0; i < results.size(); i++) 
        {
               if (i == (results.size() - 1)) {
                    s += results.get(i).doubleValue() + "\n";
                } else {
                    s += results.get(i).doubleValue() + " ";
                }
        }

        try {
             final File homeDir = new File(System.getProperty("user.home"));
            File directory = new File(homeDir + "/" +  resultsDirectory + "/" + experimentGroup + "/" + experimentDirectory);
            if (!directory.exists()) {
                if (!directory.mkdirs()) {
                      System.out.println("Couldn't create directory: " + resultsDirectory);
                };
            }
            
            /*
            File exDirectory = new File(directory,  experimentDirectory);
            if (!directory.exists()) {
                if (!directory.mkdir()) {
                      System.out.println("Couldn't create directory: " + resultsDirectory);
                };
            }
            */
       

            File file = new File(directory + "/" + filename);
            BufferedWriter writer = new BufferedWriter(new FileWriter(file, append));
            writer.write(s);
            writer.close();
        } catch (Exception e) {
            return false;
        }

        return true;

    }

    public static boolean exportXML(Document xmlDocument,
                                    String resultsDirectory,
                                    String experimentGroup,
                                    String experimentDirectory,
                                    String filename) {

        try {

            final File homeDir = new File(System.getProperty("user.home"));
            File directory = new File(homeDir + "/" +  resultsDirectory + "/" + experimentGroup + "/" + experimentDirectory);
            if (!directory.exists()) {
                if (!directory.mkdirs()) {
                    System.out.println("Couldn't create directory: " + resultsDirectory);
                }
            }

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(xmlDocument);
            StreamResult result = new StreamResult(new File(directory + "/" + filename));
            transformer.transform(source, result);
            return true;
        } catch (TransformerException e) {
            return false;
        }
    }
     
     
     public static ArrayList<ArrayList<Double>> loadDoubles(String directoryname, String filename) {

         Pattern p = Pattern.compile(" ");
         
         try {
            final File homeDir = new File(System.getProperty("user.home"));
            File directory = new File(homeDir, directoryname);
            if (!directory.exists()) {
                    System.out.println("Directory " + directoryname + " does not exist");
                    return null;}

            File file = new File(homeDir, directoryname + "/" + filename);
            
            if(!file.exists()){
                System.out.println("File " + filename + " does not exist");
                return null;}
         
            BufferedReader reader = new BufferedReader(new FileReader(file));
            String line;
            ArrayList<ArrayList<Double>> results = new ArrayList<ArrayList<Double>>();
            
            while((line = reader.readLine()) != null){
                ArrayList<Double> row = new ArrayList<Double>();           
                for(String s : p.split(line)){
                    try {
                        row.add(Double.parseDouble(s));}
                    catch (NumberFormatException ex){}}
                if(!row.isEmpty()){
                    results.add(row);}}
 
            reader.close();
            
            if(!results.isEmpty()){
                return results;}
            else {
                return null;}

        } catch (IOException ex) {
            return null;
        } 
 

    }
}
