package uk.ac.ncl.icos.datatypes;

import java.io.Serializable;

/**
 * Created by IntelliJ IDEA.
 * User: njsh2
 * Date: 05-Feb-2010
 * Time: 15:14:18
 * To change this template use File | Settings | File Templates.
 */
public class Fitness implements Serializable
{
    // internal variables
    
    private double fitness;
    
    //constructor
    public Fitness(double fitness)
    {
        this.fitness = fitness;
    }

   public double getFitness()
   {
       return fitness;
   }
   
   public void setFitness(double fitness)
   {
       this.fitness = fitness;
   }
}
