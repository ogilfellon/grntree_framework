/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.chromosome;

import uk.ac.ncl.icos.datatypes.Fitness;
import uk.ac.ncl.icos.eaframework.operator.Operator;

import java.io.Serializable;

/**
 *
 * @author owengilfellon
 */
public class EvaluatedChromosome<T extends Chromosome> implements Serializable {
    
    private T c;
    private Fitness f;
    private Operator o;

    public EvaluatedChromosome(T c, Fitness f, Operator o) {
        this.c = (T)c.duplicate();
        this.f = f;
        this.o = o;
    }
    
    public Fitness getFitness(){
        return f;
    }
    
    public T getChromosome(){
        return c;
    }
    
    public String getOperator(){
        if(o!=null)
        {
            return o.getOperator().getClass().getSimpleName();
        }
        else return null; 
    }
}
