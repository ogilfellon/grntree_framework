/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.selection;

import uk.ac.ncl.icos.eaframework.chromosome.Chromosome;
import uk.ac.ncl.icos.eaframework.chromosome.EvaluatedChromosome;

import java.util.List;

/**
 *
 * @author owengilfellon
 */
public interface Selection<T extends Chromosome> {
    
    List<T> select(List<EvaluatedChromosome<T>> population);
    
}
