/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.selection;

import uk.ac.ncl.icos.annotation.EAModule;
import uk.ac.ncl.icos.datatypes.Fitness;
import uk.ac.ncl.icos.eaframework.chromosome.Chromosome;
import uk.ac.ncl.icos.eaframework.chromosome.EvaluatedChromosome;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 *
 * @author owengilfellon
 */
@EAModule( visualName = "Sigma Scaled Roulette Wheel")
public class SigmaScaledRouletteWheel<T extends Chromosome> implements Selection<T>, Serializable {
    
    private double totalFitness;
    private double totalSigma;
    private double avgFitness;
    private double variance;
    private double sd;
    private List<EvaluatedChromosome<T>> population;
    private List<T> sigmaScaledFitnesses = new ArrayList<T>();
    
    private final int NUMBER_TO_RETURN;

    public SigmaScaledRouletteWheel(int numberToReturn) {
        this.NUMBER_TO_RETURN = numberToReturn;
    }
      
    
    @Override
    public List<T> select(List<EvaluatedChromosome<T>> population) {

        totalFitness = 0;
        totalSigma = 0;
        avgFitness = 0;
        variance = 0;
        sd = 0;
        this.population = population;
        sigmaScaledFitnesses.clear();
        
        calculateStats();
        
       // StringBuilder sb = new StringBuilder();
        for(EvaluatedChromosome ec:population)
        {
            double sigma = getSigmaScaledFitness(ec);
           // sb.append("Fit: ").append(ec.getFitness().getFitness()).append( " | Sigma: ").append(sigma).append("\n");
            EvaluatedChromosome ec2 = new EvaluatedChromosome(ec.getChromosome(), new Fitness(sigma), null);
            sigmaScaledFitnesses.add((T)ec2);
        }
        
        List<T> selected = new ArrayList<>();

        Random r = new Random();
        
        for(int i=0; i<NUMBER_TO_RETURN; i++)
        {
            
            double ptr = (r.nextDouble() * totalFitness);
            double ptr2 = 0.0;
            int modelIndex = 0;
            EvaluatedChromosome<T> c = null;
            
            while(ptr2 < ptr)
            {
                c = population.get(modelIndex);
                ptr2 += c.getFitness().getFitness();
                modelIndex++;
            }
            
            if(c != null)
            {
                //sb.append("Selected: ").append(c.getFitness().getFitness()).append(" | \n");
                selected.add(c.getChromosome());
            }
            
            
        }
        
        //System.out.println(sb.toString());
        return selected;
    }
    
    private void calculateStats()
    {
        for(EvaluatedChromosome ec:population)
        {
            totalFitness += ec.getFitness().getFitness();
        }
        
        avgFitness = ( totalFitness / population.size() );
        
        for(EvaluatedChromosome ec:population){
            variance += Math.pow((ec.getFitness().getFitness() - avgFitness), 2);
        }
        
        sd = Math.sqrt(variance);
        /*
        StringBuilder sb = new StringBuilder();
        sb.append("Total Fitness: ").append(totalFitness);
        sb.append(" | avgFitness: ").append(avgFitness);
        sb.append(" | variance: ").append(variance);
        sb.append(" | sd: ").append(sd).append("\n");
        System.out.println(sb.toString());
        * */
    }
    
    private double getSigmaScaledFitness(EvaluatedChromosome ec)
    {
        
        double sigma = 0;
        if(sd != 0)
        {
            sigma =  1.0 + ((ec.getFitness().getFitness() - avgFitness) / (2.0 * sd));
        }
        else
        {
            sigma =  1.0;
        }
        
        totalSigma += sigma;
        
        return sigma;
    }
}
