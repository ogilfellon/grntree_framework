package uk.ac.ncl.icos.eaframework.observer;

import uk.ac.ncl.icos.eaframework.chromosome.Chromosome;
import uk.ac.ncl.icos.eaframework.evoengines.EvoEngine;
import uk.ac.ncl.icos.eaframework.stats.PopulationStats;
import uk.ac.ncl.icos.filemanagement.Export;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author owengilfellon
 */
public class StringResultsFileWriter implements EvolutionObserver<Chromosome> {

    String timestamp;
    
    public StringResultsFileWriter() {
         DateFormat dateFormat = new SimpleDateFormat("yyMMdd_HHmmssSSS");
         timestamp = dateFormat.format(new Date()); 
    }

    @Override
    public void update(EvolutionSubject<Chromosome> s) {
        EvoEngine e = (EvoEngine) s;
        PopulationStats stats = e.getPopulationStats();
        ArrayList<Double> fitnesses = new ArrayList<Double>();
        fitnesses.add(stats.getMeanFitness());

            if (!Export.exportDoubles(null,
                    "Results",
                    "StringExperiments",
                    "Experiment_" + timestamp,
                    "AllFitnesses_" + timestamp + ".txt",
                    fitnesses,
                    true)) {  System.out.println("Cannot export all fitness results");}
         
    }
}
