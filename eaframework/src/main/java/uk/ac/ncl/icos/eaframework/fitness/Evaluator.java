/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.fitness;

import uk.ac.ncl.icos.datatypes.Fitness;
import uk.ac.ncl.icos.eaframework.chromosome.Chromosome;

/**
 *
 * @author owengilfellon
 */
public interface Evaluator<T extends Chromosome> {
    
    public Fitness evaluate(T c);
    
}
