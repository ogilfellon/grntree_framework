/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.evoengines;

/**
 *
 * @author owengilfellon
 */
public interface Optimiser  {

    /**
     * Moves forward one generation by applying evolutionary Operators, and running
     * selection. Termination conditions are not checked.
     */
    public void optimise();
    
    public boolean shouldTerminate();
  
}
