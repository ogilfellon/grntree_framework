/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.selection;

import uk.ac.ncl.icos.annotation.EAModule;
import uk.ac.ncl.icos.eaframework.chromosome.Chromosome;
import uk.ac.ncl.icos.eaframework.chromosome.EvaluatedChromosome;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author owengilfellon
 */
@EAModule( visualName = "Mutant Champ")
public class MutantChamp<T extends Chromosome> extends AbstractMutantChamp<T> {

    public List<T> select(List<EvaluatedChromosome<T>> population) {
        
        List<T> selected = new ArrayList<>();
        
        if(population.size()!=1){
            throw new IllegalArgumentException("MutantChamp population size must be 1");
        }

        /*
        
        if(bestChromosome!=null)
        {
            System.out.println("1: " + bestChromosome.getFitness().getFitness() +
                            " | 2: " + population.get(0).getFitness().getFitness());
        }*/
        
        
        if(bestChromosome == null ||
                (population.get(0).getFitness().getFitness() > bestChromosome.getFitness().getFitness())){
            bestChromosome = population.get(0);
        }
        
        selected.add(bestChromosome.getChromosome());
        
        return selected;
    }
}
