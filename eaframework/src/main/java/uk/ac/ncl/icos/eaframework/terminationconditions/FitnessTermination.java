/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.terminationconditions;

import uk.ac.ncl.icos.annotation.EAModule;
import uk.ac.ncl.icos.eaframework.stats.PopulationStats;

import java.io.Serializable;

/**
 *
 * @author owengilfellon
 */
@EAModule( visualName = "Function Maximisation")
public class FitnessTermination implements TerminationCondition, Serializable {

    double fitness;
    
    public FitnessTermination(double fitness) {
        this.fitness = fitness;
    }

    @Override
    public boolean shouldTerminate(PopulationStats es) {
        if(es.getBestFitness() >= fitness )
        {
            System.out.println("Max Fitness Termination");
            return true;
        }
        else
        {
            return false;
        }
    }
    
    
    
}
