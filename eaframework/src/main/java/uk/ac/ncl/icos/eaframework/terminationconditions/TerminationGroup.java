/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.terminationconditions;

import uk.ac.ncl.icos.eaframework.stats.PopulationStats;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author owengilfellon
 */
public class TerminationGroup implements TerminationCondition, Serializable {
    
    List<TerminationCondition> t = new ArrayList<TerminationCondition>();

    public TerminationGroup(List<TerminationCondition> t) {
        this.t.addAll(t);
    }

    @Override
    public boolean shouldTerminate(PopulationStats es) {
        
        for(TerminationCondition tc:t)
        {
            if(tc.shouldTerminate(es))
            {
                
                return true;
            }
        }
        return false;
    }
    
}
