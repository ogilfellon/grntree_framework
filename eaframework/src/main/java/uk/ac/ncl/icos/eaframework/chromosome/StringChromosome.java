/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.chromosome;

/**
 *
 * @author owengilfellon
 */
public class StringChromosome implements Chromosome {
    
    private final String chromosome;

    public StringChromosome(String chromosome) {
        this.chromosome = chromosome;
    }
    
    public String getString()
    {
        return chromosome;
    }
 
    @Override
    public Chromosome duplicate() {
        return new StringChromosome(chromosome);
    }

}
