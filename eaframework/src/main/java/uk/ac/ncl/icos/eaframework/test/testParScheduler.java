package uk.ac.ncl.icos.eaframework.test;

import uk.ac.ncl.icos.eaframework.chromosome.Chromosome;
import uk.ac.ncl.icos.eaframework.chromosome.EvaluatedChromosome;
import uk.ac.ncl.icos.eaframework.chromosome.StringChromosome;
import uk.ac.ncl.icos.eaframework.evoengines.EvoEngine;
import uk.ac.ncl.icos.eaframework.evoengines.StringEngine;
import uk.ac.ncl.icos.eaframework.factories.ChromosomeFactory;
import uk.ac.ncl.icos.eaframework.factories.StringChromosomeFactory;
import uk.ac.ncl.icos.eaframework.fitness.Evaluator;
import uk.ac.ncl.icos.eaframework.fitness.StringAllOnes;
import uk.ac.ncl.icos.eaframework.observer.EvolutionObserver;
import uk.ac.ncl.icos.eaframework.observer.EvolutionSubject;
import uk.ac.ncl.icos.eaframework.operator.Operator;
import uk.ac.ncl.icos.eaframework.operator.StringOperatorGroup;
import uk.ac.ncl.icos.eaframework.operator.StringPointMutation;
import uk.ac.ncl.icos.eaframework.scheduler.ParameterScheduler;
import uk.ac.ncl.icos.eaframework.selection.Selection;
import uk.ac.ncl.icos.eaframework.selection.SigmaScaledRouletteWheel;
import uk.ac.ncl.icos.eaframework.terminationconditions.FitnessTermination;
import uk.ac.ncl.icos.eaframework.terminationconditions.GenerationTermination;
import uk.ac.ncl.icos.eaframework.terminationconditions.TerminationCondition;
import uk.ac.ncl.icos.eaframework.terminationconditions.TerminationGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by owengilfellon on 06/03/2014.
 */
public class testParScheduler {

    public static void main(String[] args) {
        new testParScheduler().run();
    }

    public void run() {
        List<StringChromosome> seed = new ArrayList<StringChromosome>();
        seed.add(new StringChromosome("01010001100101100010"));
        ChromosomeFactory<StringChromosome> cf = new StringChromosomeFactory(seed, 1);
        List<Operator<StringChromosome>> operators = new ArrayList<Operator<StringChromosome>>();
        operators.add(new StringPointMutation());
        Operator<StringChromosome> o = new StringOperatorGroup(operators, 1);
        List<TerminationCondition> tc = new ArrayList<TerminationCondition>();
        tc.add(new GenerationTermination(100));
        tc.add(new FitnessTermination(100.0));
        TerminationCondition t = new TerminationGroup(tc);
        Selection<StringChromosome> s = new SigmaScaledRouletteWheel<>(2);
        Evaluator<StringChromosome> f = new StringAllOnes();

        for(int i=0; i<1; i++)
        {
            EvoEngine<StringChromosome> e = new StringEngine(cf, o, s, f, t);
            ParameterScheduler parameterScheduler = new ParameterScheduler(20, 80, 4.0, 2.0);
            EvolutionObserver observer = new ParTestObserver(parameterScheduler);
            e.attach((EvolutionObserver)parameterScheduler);
            e.attach(observer);
            e.run();
        }
    }


    private class ParTestObserver implements EvolutionObserver<Chromosome> {

        private ParameterScheduler ps;

        public ParTestObserver(ParameterScheduler scheduler) {
            this.ps = scheduler;
        }

        @Override
        public void update(EvolutionSubject<Chromosome> s) {
            EvoEngine e = (EvoEngine) s;
            System.out.println("Gen: " + e.getPopulationStats().getCurrentGeneration() +
                                " Par: " + ps.getParameter());
        }
    }
}
