/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.factories;

import uk.ac.ncl.icos.eaframework.chromosome.StringChromosome;
import uk.ac.ncl.icos.eaframework.operator.Operator;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 *
 * @author owengilfellon
 */
public class StringChromosomeFactory extends AbstractChromosomeFactory<StringChromosome> {
    
    private final int NUMBER_OF_INITIAL_MUTATIONS = 10;
    private final int CHROMOSOME_LENGTH = 20;

    public StringChromosomeFactory(List<StringChromosome> seeds, int POPULATON_SIZE) {
        super(seeds, POPULATON_SIZE);
    }
    
    @Override
    public List<StringChromosome> generatePopulation() {
        
        Random r = new Random();
        List<StringChromosome> population = new ArrayList<StringChromosome>();
        
        for(int i=0; i<POPULATON_SIZE; i++)
        {
            StringChromosome seed = seeds.get(r.nextInt(seeds.size()));
            char[] chromosome = seed.getString().toCharArray();
            
            if(chromosome.length != CHROMOSOME_LENGTH)
            {
                throw new IllegalStateException("Chromosome length is not " + CHROMOSOME_LENGTH);
            }
            
            for(int j=0; j<NUMBER_OF_INITIAL_MUTATIONS; j++)
            {
                int index = r.nextInt(chromosome.length);
                chromosome[index] = r.nextInt(2) == 0 ?  '0' :'1';
            }
            
            population.add(new StringChromosome(new String(chromosome)));
        }
        
        return population;
    }

    @Override
    public List<StringChromosome> generatePopulation(List<Operator<StringChromosome>> operators) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
}
