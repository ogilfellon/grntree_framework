/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.terminationconditions;

import uk.ac.ncl.icos.annotation.EAModule;
import uk.ac.ncl.icos.eaframework.stats.PopulationStats;

import java.io.Serializable;

/**
 *
 * @author owengilfellon
 */
@EAModule( visualName = "No Increase")
public class NoIncreaseTermination implements TerminationCondition, Serializable {
    
    private int generationOfLastIncrease = 0;
    private final int GENERATIONS_WITHOUT_INCREASE;
    private double bestFitness = 0.0;

    public NoIncreaseTermination(int generationsWithoutIncrease) {
        this.GENERATIONS_WITHOUT_INCREASE = generationsWithoutIncrease;
    }

    @Override
    public boolean shouldTerminate(PopulationStats es) {
        
        if(es.getBestFitness() > bestFitness)
        {
            bestFitness = es.getBestFitness();
            generationOfLastIncrease = es.getCurrentGeneration();
        }
        
        if((es.getCurrentGeneration() - generationOfLastIncrease) >= GENERATIONS_WITHOUT_INCREASE)
        {
            System.out.println("No Fitness Increase Termination");
            return true;
        }
        else
        {
            return false;
        }
    }
}
