/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.evoengines;

import uk.ac.ncl.icos.eaframework.chromosome.Chromosome;
import uk.ac.ncl.icos.eaframework.chromosome.EvaluatedChromosome;
import uk.ac.ncl.icos.eaframework.factories.ChromosomeFactory;
import uk.ac.ncl.icos.eaframework.fitness.Evaluator;
import uk.ac.ncl.icos.eaframework.observer.EvolutionObserver;
import uk.ac.ncl.icos.eaframework.operator.Operator;
import uk.ac.ncl.icos.eaframework.selection.Selection;
import uk.ac.ncl.icos.eaframework.stats.PopulationStats;
import uk.ac.ncl.icos.eaframework.terminationconditions.TerminationCondition;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author owengilfellon
 */
public abstract class AbstractEvoEngine<T extends Chromosome> implements EvoEngine<T>, Serializable{
    
    protected List<EvolutionObserver<T>> observers = new ArrayList<EvolutionObserver<T>>();
    protected Operator o;
    protected Evaluator e;
    protected ChromosomeFactory cf;
    protected Selection s;
    protected TerminationCondition t;
    
    protected List<T> population = new ArrayList<T>();
    protected List<EvaluatedChromosome<T>> evaluatedPopulation = new ArrayList<EvaluatedChromosome<T>>();
    protected int currentGeneration = 0; 

    public AbstractEvoEngine(ChromosomeFactory<T> cf, Operator<T> o, Selection<T> s, Evaluator<T> e, TerminationCondition t) {
        this.cf = cf;
        this.o = o;
        this.s = s;
        this.e = e;
        this.t = t;
    }

    @Override
    abstract public void stepForward();

    @Override
    public PopulationStats getPopulationStats() {
        
        double meanfitness = 0.0;
        EvaluatedChromosome bestChromosome = null;
        
        for(EvaluatedChromosome c: evaluatedPopulation){
            if(bestChromosome == null ||
                    c.getFitness().getFitness() > bestChromosome.getFitness().getFitness()){
                bestChromosome = c;}
            meanfitness += c.getFitness().getFitness();
        }
        
        meanfitness /= evaluatedPopulation.size();
        
        

        return new PopulationStats(bestChromosome.getChromosome(),
                                    currentGeneration,
                                    population.size(),
                                    meanfitness,
                                    bestChromosome.getFitness().getFitness());
    }
    
    @Override
    public List<T> getPopulation()
    {
        return population;
    }
    
    @Override
    public List<EvaluatedChromosome<T>> getEvaluatedPopulation()
    {
        return evaluatedPopulation;
    }

    @Override
    public TerminationCondition getTerminationCondition() {
        return t;
    }

    @Override
    public Selection getSelectionStrategy() { return s; }

    @Override
    public Evaluator<T> getFitnessEvaluator()
    {
        return e;
    }
        
    @Override
    abstract public List<T> run();



    @Override
    public void alert() {
        for(EvolutionObserver observer:observers)
        {
            observer.update(this);
        }
    }

    @Override
    public void attach(EvolutionObserver<T> o) {
        observers.add(o);
    }

    @Override
    public void dettach(EvolutionObserver<T> d) {
        observers.remove(d);
    }

    @Override
    public List<EvolutionObserver<T>> getObservers() {
        return observers;
    }
}
