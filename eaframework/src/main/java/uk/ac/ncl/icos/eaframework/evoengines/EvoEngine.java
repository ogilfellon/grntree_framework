package uk.ac.ncl.icos.eaframework.evoengines;

import uk.ac.ncl.icos.eaframework.chromosome.Chromosome;
import uk.ac.ncl.icos.eaframework.chromosome.EvaluatedChromosome;
import uk.ac.ncl.icos.eaframework.fitness.Evaluator;
import uk.ac.ncl.icos.eaframework.observer.EvolutionObserver;
import uk.ac.ncl.icos.eaframework.observer.EvolutionSubject;
import uk.ac.ncl.icos.eaframework.selection.Selection;
import uk.ac.ncl.icos.eaframework.stats.PopulationStats;
import uk.ac.ncl.icos.eaframework.terminationconditions.TerminationCondition;

import java.util.List;

/**
 * Defines operations on an Evolutionary Algorithm
 * @author owengilfellon
 * @param <T> The Chromosome type that the Evolutionary Algorithm is to operate on.     Operators, FitnessEvaluators should be identically typed.
 */
public interface EvoEngine<T extends Chromosome> extends EvolutionSubject<T> {

    /**
     * Starts the Evolutionary Algorithm, by repeatedly calling stepForward until
     * a termination condition is met.
     * @return The fittest Chromosome evolved during the evolution process.
     */
    public List<T> run();
    /**
     * Moves forward one generation by applying evolutionary Operators, and running
     * selection. Termination conditions are not checked.
     */
    public void stepForward();
    /**
     * Generates statistics for the current population, including the best
     * chromosome in the population, the current generation, the size of the
     * population, the mean fitness of the population, and the best fitness
     * among the population.
     * @return 
     */
    public PopulationStats getPopulationStats();
    /**
     * Retrieves the entire population.
     * @return 
     */
    public List<T> getPopulation();

    public Selection getSelectionStrategy();
    
    
    public List<EvaluatedChromosome<T>> getEvaluatedPopulation();

    public List<EvolutionObserver<T>> getObservers();
    /**
     * Returns the Evaluator passed to the Evolutionary Algorithm at
     * construction.
     * @return 
     */
    public Evaluator<T> getFitnessEvaluator();
    
    public TerminationCondition getTerminationCondition();
}
