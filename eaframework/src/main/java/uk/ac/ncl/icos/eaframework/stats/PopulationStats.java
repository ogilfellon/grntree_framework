/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.stats;

import uk.ac.ncl.icos.eaframework.chromosome.Chromosome;

import java.io.Serializable;

/**
 *
 * @author owengilfellon
 */
public class PopulationStats<T extends Chromosome> implements Serializable{
       
    private T bestChromosome;
    private int currentGeneration;
    private int populationSize;
    private double meanFitness;
    private double bestFitness;

    public PopulationStats(T bestChromosome, int currentGeneration, int populationSize, double meanFitness, double bestFitness) {
        this.bestChromosome = bestChromosome;
        this.currentGeneration = currentGeneration;
        this.populationSize = populationSize;
        this.meanFitness = meanFitness;
        this.bestFitness = bestFitness;
    }
   
    /**
     * @return the bestChromosome
     */
    public T getBestChromosome() {
        return bestChromosome;
    }

    /**
     * @return the currentGeneration
     */
    public int getCurrentGeneration() {
        return currentGeneration;
    }

    /**
     * @return the populationSize
     */
    public int getPopulationSize() {
        return populationSize;
    }

    /**
     * @return the meanFitness
     */
    public double getMeanFitness() {
        return meanFitness;
    }

    /**
     * @return the bestFitness
     */
    public double getBestFitness() {
        return bestFitness;
    }
   
}
