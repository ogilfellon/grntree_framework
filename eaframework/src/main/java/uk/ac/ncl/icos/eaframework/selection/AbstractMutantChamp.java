package uk.ac.ncl.icos.eaframework.selection;

import uk.ac.ncl.icos.eaframework.chromosome.EvaluatedChromosome;

import java.io.Serializable;
import uk.ac.ncl.icos.eaframework.chromosome.Chromosome;

/**
 * Created by owengilfellon on 15/04/2014.
 */
public abstract class AbstractMutantChamp <T extends Chromosome> implements Selection<T>, Serializable{


    EvaluatedChromosome<T> bestChromosome = null;

    AbstractMutantChamp() {
    }

    AbstractMutantChamp(EvaluatedChromosome bestChromosome) {
        this.bestChromosome = bestChromosome;
    }

    public EvaluatedChromosome<T> getBestChromosome()
    {
        return bestChromosome;
    }

    public void setBestChromosome(EvaluatedChromosome chromosome)
    {
        this.bestChromosome = chromosome;
    }

}
