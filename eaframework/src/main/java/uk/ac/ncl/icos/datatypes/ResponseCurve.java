package uk.ac.ncl.icos.datatypes;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.Serializable;
import java.util.ArrayList;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author ogilfellon
 */
public class ResponseCurve implements Serializable {

    private ArrayList<ArrayList<Double>> results;

    public ResponseCurve() {
        results = new ArrayList<ArrayList<Double>>();
    }
    
    public ResponseCurve(ArrayList<ArrayList<Double>> results) {
        this.results = results;
    }

    public ArrayList<ArrayList<Double>> getResults() {
        return this.results;
    }

    public void setResults(ArrayList<ArrayList<Double>> results) {
        this.results = results;
    }

    public void addResults(ArrayList<Double> results) {
        ArrayList<Double> temp = (ArrayList<Double>) results.clone();
        this.results.add(temp);
    }

    public double getAcentoticValue(int run) {
        /*
        double prev_value = -1000.0;
        double value = 0.0;
        double acentotic = 0.0;
        int count = 0;

        for (int i = 0; i < results.get(run).size(); i++) {
            value = results.get(run).get(i);
            if (Math.abs(value - prev_value) < 0.001) {
                count++;
                acentotic = value;
            } else {
                count = 0;
            }

            prev_value = value;
        }

        if (count > 10) {
            return acentotic;
        } else {
            return 0.0;
        }
        * */
        
        return results.get(run).get(results.get(run).size()-1).doubleValue();
    }

    public boolean exportResults(String title, String directoryname, String filename, String headerLabel, boolean append) {

        System.out.println("Exporting data.................................");
                
        String s = "";

        // Add column titles
        
        s+= title + "\n";
        
        
        for (int i = 0; i < this.results.get(0).size(); i++) {
            
            
            if (i == (this.results.get(0).size() - 1)) {
                s += headerLabel + "_" + (i + 1) + "\n";}
            else {
                s += headerLabel + "_" + (i + 1) + " ";}}

        for (int i = 0; i < this.results.size(); i++) {
            for (int j = 0; j < this.results.get(i).size(); j++) {
                if (j == (this.results.get(i).size() - 1)) {
                    s += this.results.get(i).get(j).doubleValue() + "\n";}
                else {
                    s += this.results.get(i).get(j).doubleValue() + " ";}}}

        try {
            final File homeDir = new File(System.getProperty("user.home"));
            File directory = new File(homeDir, directoryname);
            if(!directory.exists()) {
                if(!directory.mkdir())
                {
                    System.out.println("Couldn't create directory: "+directoryname);
                };
            }
            
            File file = new File(homeDir, directoryname + "/" + filename);
            BufferedWriter writer = new BufferedWriter(new FileWriter(file, append));
            writer.write(s);
            writer.close();}
        catch (Exception e) {
            return false;}

        return true;

    }
}
