package uk.ac.ncl.icos.eaframework.evoengines;

import uk.ac.ncl.icos.datatypes.Fitness;
import uk.ac.ncl.icos.eaframework.chromosome.Chromosome;
import uk.ac.ncl.icos.eaframework.chromosome.EvaluatedChromosome;
import uk.ac.ncl.icos.eaframework.chromosome.StringChromosome;
import uk.ac.ncl.icos.eaframework.factories.ChromosomeFactory;
import uk.ac.ncl.icos.eaframework.fitness.Evaluator;
import uk.ac.ncl.icos.eaframework.operator.Operator;
import uk.ac.ncl.icos.eaframework.selection.Selection;
import uk.ac.ncl.icos.eaframework.stats.PopulationStats;
import uk.ac.ncl.icos.eaframework.terminationconditions.TerminationCondition;

import java.util.List;
import java.util.Random;

/**
 *
 * @author owengilfellon
 */
public class StringEngine extends AbstractEvoEngine<StringChromosome> {
    
    /*
     * FUTURE WORK===============
     * Need way to specify this dynamically. Move this from this class. Should be
     * in the evaluation class, or something similar. SELECTION STRATEGY - there
     * is currently no selection strategy class. This should be added, and this
     * assessment / calculation included there.
     */
   
    
    /*
     * Provide a factory for generating a model for the algorithm
     */
    public StringEngine(ChromosomeFactory<StringChromosome> cf, Operator<StringChromosome> o, Selection<StringChromosome> s, Evaluator<StringChromosome> e, TerminationCondition t)
    {
        super(cf, o, s, e, t);
        this.population = cf.generatePopulation();
    }

    @Override
    public PopulationStats getPopulationStats() {

           
               return super.getPopulationStats();
           
    }


    @Override
    public void stepForward() {
                   
        currentGeneration++;
        
        if(evaluatedPopulation != null && evaluatedPopulation.size() > 0 )
        {
            population.clear();
            
            while(population.size() < cf.getPopulationSize())
            {
                List<Chromosome> selected = s.select(evaluatedPopulation);
                StringChromosome first = (StringChromosome) selected.get(0);
                StringChromosome second = (StringChromosome) selected.get(1);
                Random r = new Random();
                int crossoverpoint = r.nextInt(first.getString().toCharArray().length);
                
                char[] parent1 = first.getString().toCharArray();
                char[] parent2 = second.getString().toCharArray();
                char[] child = new char[parent1.length];
                
//                System.out.println("1: " + parent1.length + " 2: " + parent2.length);
                
                for(int i=0; i<parent1.length; i++){
                    if(i >= crossoverpoint) {
                        child[i] = parent2[i];
                    }
                    else {
                        child[i] = parent1[i];
                    }
                }
                
                StringChromosome t = (StringChromosome) o.apply(new StringChromosome(new String(child)));
                population.add(t);
                
            }
        }
        
        evaluatedPopulation.clear();
        int index = 0;
        
        for(StringChromosome t:population)
        {
            //System.out.println("Evaluating: " + index);
            Fitness fitness = e.evaluate(t);
            evaluatedPopulation.add(new EvaluatedChromosome(t, fitness, o.getOperator()));
            index++;
        }
        
        alert();
        
    }
    
    @Override
    public List<StringChromosome> run()
    {       

        do
        {
            stepForward();
        }
        while(!t.shouldTerminate(getPopulationStats()));
        
        return population;
    }
}