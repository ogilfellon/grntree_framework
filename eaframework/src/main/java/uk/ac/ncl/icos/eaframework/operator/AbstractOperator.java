package uk.ac.ncl.icos.eaframework.operator;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import uk.ac.ncl.icos.eaframework.chromosome.Chromosome;

import java.io.Serializable;

/**
 *
 * @author owengilfellon
 */
abstract public class AbstractOperator<T extends Chromosome> implements Operator<T>, Serializable {
    

    @Override
    abstract public T apply(T c);


    @Override
    public Operator<T> getOperator() {
        return this;
    }
    
    
    
}
