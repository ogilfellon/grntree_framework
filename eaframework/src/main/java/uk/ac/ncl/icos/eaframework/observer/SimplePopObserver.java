package uk.ac.ncl.icos.eaframework.observer;

import uk.ac.ncl.icos.eaframework.chromosome.Chromosome;
import uk.ac.ncl.icos.eaframework.chromosome.EvaluatedChromosome;
import uk.ac.ncl.icos.eaframework.evoengines.EvoEngine;
import uk.ac.ncl.icos.eaframework.stats.PopulationStats;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author owengilfellon
 */
public class SimplePopObserver implements EvolutionObserver<Chromosome>, Serializable {

    public SimplePopObserver() {
    }

    @Override
    public void update(EvolutionSubject<Chromosome> s) {
       
            EvoEngine e = (EvoEngine) s;
            List<EvaluatedChromosome> pop = e.getEvaluatedPopulation();
            PopulationStats ps = e.getPopulationStats();
           
            StringBuilder sb = new StringBuilder();
            
            sb.append("Gen: ").append(ps.getCurrentGeneration()).append(" | ");
            
            sb.append("AvgFit: ").append(ps.getMeanFitness()).append(" | ");
            sb.append("Best Fit: ").append(ps.getBestFitness()).append("\n");
            System.out.print(sb.toString());
    }
    
}
