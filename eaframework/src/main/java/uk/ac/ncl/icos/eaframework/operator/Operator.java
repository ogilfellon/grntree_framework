package uk.ac.ncl.icos.eaframework.operator;

import uk.ac.ncl.icos.eaframework.chromosome.Chromosome;

/**
 * The interface for Evolutionary Operators, which uses the Visitor design
 * pattern. The chromosome is passed as an argument. Concrete classes should
 * create a duplicate of the chromosome, apply mutations, and return the
 * modified, duplicate chromosome.
 * 
 * @author owengilfellon
 */
public interface Operator<T extends Chromosome> {
    
    /**
     * Applies the Evolutionary Operator to the specified Chromosome
     * @param c The chromosome to which the Operator will be applied
     * @return The modified chromosome.
     */
    public T apply(T c);
    public Operator<T> getOperator();
    
}
