/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.chromosome;

/**
 *
 * @author owengilfellon
 */
public class DoubleChromosome implements Chromosome {
    
    private final double[] chromosome;

    public DoubleChromosome(double[] chromosome) {
        this.chromosome = chromosome;
    }
    
    public double[] getChromosome()
    {
        return chromosome;
    }
 
    @Override
    public Chromosome duplicate() {
        return new DoubleChromosome(chromosome);
    }

    
}
