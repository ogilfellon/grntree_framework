package uk.ac.ncl.icos.eaframework.operator;

import uk.ac.ncl.icos.eaframework.chromosome.StringChromosome;

import java.util.Random;

/**
 * Selects a random Transcriptional Unit (A) from the GRNTree. A random RBS node within
 * the TU is identified as a split point. The parts following the split point are
 * replaced with an RBS, CDS and terminator, with the CDS coding for a TF for the 
 * inducible promoter of a newly created TU (B). The parts that are removed from A
 * are placed into B.
 * 
 * @author owengilfellon
 */
public class StringPointMutation extends AbstractOperator<StringChromosome> {
    
 
    @Override
    public StringChromosome apply(StringChromosome c) {
            char[] chromosome = c.getString().toCharArray();
            Random r = new Random();
            int point = r.nextInt(chromosome.length);
            chromosome[point] = r.nextInt(2) == 0 ? '0' : '1';
            return new StringChromosome(new String(chromosome));
    }
}
