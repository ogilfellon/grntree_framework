package uk.ac.ncl.icos.eaframework.operator;

import uk.ac.ncl.icos.eaframework.chromosome.DoubleChromosome;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 *
 * @author owengilfellon
 */
public class DoubleOperatorGroup extends AbstractOperator<DoubleChromosome> {
    
    List<Operator<DoubleChromosome>> operators = new ArrayList<Operator<DoubleChromosome>>();
    int mutationsPerGeneration = 1;
    Operator<DoubleChromosome> o;

    public DoubleOperatorGroup(List<Operator<DoubleChromosome>> operators) {
        this.operators = operators;
    }
    
    public DoubleOperatorGroup(List<Operator<DoubleChromosome>> operators, int mutationsPerGeneration) {
        this(operators);
        this.mutationsPerGeneration = mutationsPerGeneration;      
    }
    
    @Override
    public DoubleChromosome apply(DoubleChromosome c) {
        
        DoubleChromosome d = c;
        
        for(int i=0; i<mutationsPerGeneration; i++)
        {
            o = operators.get(new Random().nextInt(operators.size()));
            d = o.apply(d);
        }
        
        return d;
    }

    @Override
    public Operator<DoubleChromosome> getOperator() {
        return o;
    }
    
    
}
