/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.grntreeexamples;

import uk.ac.ncl.icos.grntree.api.GRNTree;
import uk.ac.ncl.icos.grntree.api.GRNTreeFactory;
import uk.ac.ncl.icos.grntree.api.GRNTreeNode;

/**
 *
 * @author owengilfellon
 */
public class GRNTreeExamples {
    
    public static void main(String[] args) {
        
       GRNTree tree = GRNTreeFactory.getGRNTree("PspaRK:Prom; RBS_SpaK:RBS; SpaK:CDS; RBS_SpaR:RBS; SpaR:CDS; BO_4296:Ter; PspaS:Prom; RBS_SpaS:RBS; GFP_rrnb:CDS; BO_4296:Ter");
        
       for(GRNTreeNode node : tree.getParts("BO_4296")) {
           System.out.println(node);
       }
       
       
       
       
       System.out.println(tree.debugInfo());
        
        /*
        
        
      
        Iterator<GRNTreeNode> i = tree.getPreOrderIterator();
        while(i.hasNext()) {
            GRNTreeNode node = i.next();
            if(node.isLeafNode()) {
                System.out.println(node.getSVP().getName());
                for(Interaction in : tree.getInteractions(node)) {
                    System.out.println(" - " + in.getName());
                }
            }
        }*/
        
        /*
        SBOLNetworkSerialiser s = new SBOLNetworkSerialiser("SubtilinReceiver");
        s.exportNetwork(tree);
        */   
    }
    
}
