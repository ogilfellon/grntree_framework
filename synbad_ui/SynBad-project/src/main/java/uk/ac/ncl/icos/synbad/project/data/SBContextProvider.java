/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.project.data;

import java.net.URI;
import org.netbeans.api.project.Project;
import org.openide.util.Lookup;
import org.openide.util.Lookup.Result;
import org.openide.util.Lookup.Template;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.Utilities;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;
import org.openide.util.lookup.ProxyLookup;
import org.openide.util.lookup.ServiceProvider;

/**
 * Manages SynBad Netbeans projects, ensures there is only one project open
 * at a time.
 * @author owengilfellon
 */
public interface SBContextProvider extends Lookup.Provider, LookupListener {

    @ServiceProvider(service = SBContextProvider.class)
    public static class DefaultContextProvider implements SBContextProvider {
 
        private final InstanceContent content;
        private final AbstractLookup lookup;
        private final Result<Project> res;

        public DefaultContextProvider() {
            content = new InstanceContent();
            lookup = new AbstractLookup(content);
            res = Utilities.actionsGlobalContext().lookupResult(Project.class);
            res.allItems();
            res.addLookupListener(this);
        }
        
        public String getSourceName(SbolDocDO project) {
            return URI.create("http://www.synbad.org/" + project.getName()).toASCIIString();
        }

        @Override
        public Lookup getLookup() {
            return lookup;
        }

        @Override
        public void resultChanged(LookupEvent le) {
            Project proj = Utilities.actionsGlobalContext().lookup(Project.class);
            if(proj != null && proj instanceof SBProj) {
                SBProj currentProj = lookup.lookup(SBProj.class);
                if(currentProj != null)
                    content.remove(currentProj);
                content.add(proj);
            }
        }
    

    }
    
}
