/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.project.data;

import java.beans.PropertyChangeListener;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ProjectInformation;
import org.netbeans.spi.project.ProjectState;
import org.openide.filesystems.FileObject;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;
import uk.ac.ncl.icos.datamodel.workspace.api.SBWorkspace;
import uk.ac.ncl.icos.synbad.event.SBEventSource;
import uk.ac.ncl.icos.synbad.event.SBSubscriber;

/**
 *
 * @author owengilfellon
 */
public class SBProj implements Project, SBEventSource {


        private final FileObject fo;
        private final ProjectState ps;
        private Lookup lkp;

        public SBProj(FileObject fo, ProjectState ps) {
            this.fo = fo;
            this.ps = ps;
        }

        @Override
        public FileObject getProjectDirectory() {
            return fo;
        }

        @Override
        public Lookup getLookup() {
            
            // The project's lookup contains a ProjectInformation object, and 
            // a LogicalView. The logical view object returns the root Node for
            // the project. 

            if (lkp == null) {
                    lkp = Lookups.fixed(new Object[]{
                        this,
                        new Info(),
                        new SBProjLogicalView(this),
                        new SBWorkspaceProvider(this)
                    });
            }
            
            return lkp;
        }
        
        public SBWorkspace getWorkspace() {
            return getLookup().lookup(SBWorkspaceProvider.class).getWorkspace();
        }
        
        // move this to code for managing workspaces...
        
        /*
        public URI openSBOL(SbolDocDO project) {
            
            try {               
                URI projId = UriHelper.synbad.namespacedUri(project.getName());
                SBWorkspace workspace = m.getWorkspace(projId);
                if(workspace == null)
                    return null;
                workspace.addRdf(project.getPrimaryFile().getInputStream());
                
               // for(SBSubscriber listener : listeners) {
                //    listener.onEvent(new DefaultEvent<>(SBEventType.ADDED, null, this));
               // }
                return workspace.getRepositoryId();
            } catch (FileNotFoundException ex) {
                Exceptions.printStackTrace(ex);
            }
            
            return null;

        }*/

        @Override
        public boolean addChangeListener(SBSubscriber listener) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public boolean removeChangeListener(SBSubscriber listener) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public void clearChangeListeners() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
        
        /**
         * This is registered in the Project's lookup and provides the project's
         * name, Icon etc.
         */
        private final class Info implements ProjectInformation {

            @Override
            public String getName() {
                return getProjectDirectory().getName();
            }

            @Override
            public String getDisplayName() {
                return getName();
            }

            @Override
            public Icon getIcon() {
                return new ImageIcon(ImageUtilities.loadImage("uk/ac/ncl/icos/synbad/images/icon.gif"));
            }

            @Override
            public Project getProject() {
                return SBProj.this;
            }

            @Override
            public void addPropertyChangeListener(PropertyChangeListener pl) {
               //
            }

            @Override
            public void removePropertyChangeListener(PropertyChangeListener pl) {
                //
            }
            
        }
    }
