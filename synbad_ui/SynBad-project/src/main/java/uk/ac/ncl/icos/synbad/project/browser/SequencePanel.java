/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.project.browser;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.List;
import javax.swing.Action;
import org.openide.explorer.view.BeanTreeView;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;
import uk.ac.ncl.icos.datamodel.workspace.api.SBEventFilter;
import uk.ac.ncl.icos.datamodel.workspace.api.SBWorkspace;
import uk.ac.ncl.icos.datamodel.workspace.sbol.ComponentDefinition;
import uk.ac.ncl.icos.datamodel.workspace.sbol.Definition;
import uk.ac.ncl.icos.datamodel.workspace.sbol.Sequence;
import uk.ac.ncl.icos.synbad.event.SBEvent;
import uk.ac.ncl.icos.synbad.event.SBSubscriber;
import uk.ac.ncl.icos.synbad.svpfragment.nodes.SBDefinitionNode;
import uk.ac.ncl.icos.synbad.svpfragment.nodes.SBTopLevelNode;

/**
 *
 * @author owengilfellon
 */
public class SequencePanel extends AbstractBrowserPanel {

    BeanTreeView view;
    
    public SequencePanel() {
        super();
        initComponents();
        
        GridBagLayout layout = new GridBagLayout();
        this.setLayout(layout);
        explorerManager.setRootContext(new AbstractNode(Children.LEAF));
        
        view = new BeanTreeView();
        view.setRootVisible(false);

        GridBagConstraints fill = new GridBagConstraints();
        fill.gridx= GridBagConstraints.REMAINDER;
        fill.gridy= GridBagConstraints.REMAINDER;
        fill.fill = GridBagConstraints.BOTH;     
        fill.weightx=1.0;
        fill.weighty=1.0;
        add(view, fill);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setLayout(new java.awt.GridBagLayout());
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables

    
    
    @Override
    public void setWorkspace(SBWorkspace workspace) {
        this.workspace = workspace;
        RootComponentNode rootNode = new RootComponentNode(Children.create(new SequencePanel.SequencePanelNodeFactory(workspace), true));
        this.explorerManager.setRootContext(rootNode);
    }

    @Override
    public void clearWorkspace() {
        this.explorerManager.setRootContext(Node.EMPTY);
        this.workspace = null;
    }

    private class SequencePanelNodeFactory extends ChildFactory<Sequence> implements SBSubscriber {

        private final SBWorkspace workspace;

        public SequencePanelNodeFactory(SBWorkspace workspace) {
            this.workspace = workspace;
            workspace.getDispatcher().subscribe(
                Sequence.SequenceCreatedEvent.class,
                new SBEventFilter.DefaultFilter(),
                this);
        }

        @Override
        protected boolean createKeys(List<Sequence> toPopulate) {
           
            for(Sequence definition : workspace.getObjects(Sequence.class)) {
                toPopulate.add(definition);
            }
            
            return true;
        }

        @Override
        protected Node createNodeForKey(Sequence key) {
            return new SBTopLevelNode(Children.LEAF, Lookups.fixed(key));
        }

        @Override
        public void onEvent(SBEvent e) {
            refresh(true);
        }

    }
    
    class RootComponentNode extends AbstractNode {

        public RootComponentNode(Children children) {
            super(children);
        }

        @Override
        public Action[] getActions(boolean context) {
            return new Action[] {
             
            };
        }
    }
}
