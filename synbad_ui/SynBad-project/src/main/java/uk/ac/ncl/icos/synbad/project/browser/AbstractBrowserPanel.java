/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.project.browser;

import org.openide.explorer.ExplorerManager;
import uk.ac.ncl.icos.datamodel.workspace.api.SBWorkspace;
/**
 *
 * @author owengilfellon
 */
public abstract class AbstractBrowserPanel extends javax.swing.JPanel implements ExplorerManager.Provider {
    
    protected final ExplorerManager explorerManager;
    protected SBWorkspace workspace;
    
    public AbstractBrowserPanel() {
        this.explorerManager = new ExplorerManager();
    }
    
    @Override
    public ExplorerManager getExplorerManager() {
        return explorerManager;
    }
    
    public abstract void setWorkspace(SBWorkspace entityManager);
    
    public abstract void clearWorkspace();
    
}
