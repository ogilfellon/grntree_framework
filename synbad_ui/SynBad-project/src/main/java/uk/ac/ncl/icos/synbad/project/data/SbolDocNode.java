/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.project.data;

import java.awt.Image;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import javax.xml.namespace.QName;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.PropertySupport;
import org.openide.nodes.Sheet;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;
import uk.ac.ncl.icos.datamodel.workspace.objects.SynBadObject;

/**
 *
 * @author owengilfellon
 */
public class SbolDocNode extends AbstractNode {

    public SbolDocNode(SbolDocDO doc) {
        super(Children.LEAF, Lookups.fixed(doc));
        super.setIconBaseWithExtension(getIconBase());
    }

    private String getIconBase()
    {
        return "uk/ac/ncl/icos/synbad/project/nodeicons/userdefined16.png";
    }

    @Override
    public Image getIcon(int type) {
        return super.getIcon(type);
    }

    @Override
    public String getName() {
        return getLookup().lookup(SbolDocDO.class).getName();
    }
    

    @Override
    public String getHtmlDisplayName() {
        return getName();
    }

    
    @Override
    public boolean canDestroy() {
        return true;
    }   


}