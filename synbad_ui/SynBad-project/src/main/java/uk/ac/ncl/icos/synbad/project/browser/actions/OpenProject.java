/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.project.browser.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionRegistration;
import org.openide.filesystems.FileChooserBuilder;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.NbBundle.Messages;
import uk.ac.ncl.icos.synbad.project.data.SBContextProvider;
import uk.ac.ncl.icos.synbad.project.data.SbolDocDO;

@ActionID(
        category = "File",
        id = "uk.ac.ncl.icos.synbad.project.browser.actions.openProject"
)
@ActionRegistration(
        displayName = "#CTL_openProject"
)
@ActionReference(path = "Menu/File", position = 100)
@Messages("CTL_openProject=Open Project")
public final class OpenProject implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
         
        File home = new File(System.getProperty("user.home"));

        File toAdd = new FileChooserBuilder("user-dir").setTitle("Open Project").
                setDefaultWorkingDirectory(home).setApproveText("Open").showOpenDialog();

        if (toAdd == null) { return; }
        
        DataObject obj = null;
         
        try {
            obj = DataObject.find(FileUtil.toFileObject(toAdd));
        } catch (DataObjectNotFoundException ex) {
            Exceptions.printStackTrace(ex);
        }

        if(obj == null || !(obj instanceof SbolDocDO)) { return; }
            
        SbolDocDO project = (SbolDocDO) obj;
        
        SBContextProvider manager = Lookup.getDefault().lookup(SBContextProvider.class);
        
       // manager.openProject(project);
    } 
    
      
    
}
