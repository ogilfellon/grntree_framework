/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.project.data;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.URI;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataObject;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import uk.ac.ncl.icos.datamodel.workspace.api.SBWorkspace;
import uk.ac.ncl.icos.datamodel.workspace.api.SBWorkspaceManager;

/**
 *
 * @author owengilfellon
 */
public class SBWorkspaceProvider {
    
    private SBWorkspace workspace;
    private final SBProj proj;

    public SBWorkspaceProvider(SBProj proj) {
        this.workspace = null;
        this.proj = proj;
    }

    public SBWorkspace getWorkspace() {
        
        if(workspace != null)
            return workspace;
        
        FileObject projDir = proj.getProjectDirectory();
        URI workspaceId = URI.create("file:/" + projDir.getPath());
        
        
        FileObject srcFo = projDir.getFileObject("src");
        DataFolder srcDf = DataFolder.findFolder(srcFo);
        SBWorkspaceManager m = Lookup.getDefault().lookup(SBWorkspaceManager.class);
        workspace = m.getWorkspace(workspaceId);
        for(DataObject obj : srcDf.getChildren()) {
            SbolDocDO sbol = obj.getLookup().lookup(SbolDocDO.class);
            if(sbol!=null) {
                try {
                    InputStream is = obj.getLookup().lookup(FileObject.class).getInputStream();
                    workspace.addRdf(is, URI.create("file:/" + sbol.getName()));
                } catch (FileNotFoundException ex) {
                    Exceptions.printStackTrace(ex);
                }
            }
        }
        
        return workspace;
    }
}
