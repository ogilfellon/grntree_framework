/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.project.cookies;

import org.openide.nodes.Node;
import uk.ac.ncl.icos.synbad.project.data.SbolDocDO;

/**
 *
 * @author owengilfellon
 */
public class CompilableCookie implements Node.Cookie {

    private final SbolDocDO obj;
    
    public CompilableCookie(SbolDocDO obj) {
        this.obj = obj;
    }

    public SbolDocDO getObj() {
        return obj;
    }
}
