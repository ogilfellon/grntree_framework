/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.project.data;

import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.MIMEResolver;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectExistsException;
import org.openide.loaders.MultiDataObject;
import org.openide.loaders.MultiFileLoader;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.NbBundle.Messages;

import java.io.IOException;
import javax.xml.stream.XMLStreamException;
import org.netbeans.spi.actions.AbstractSavable;
import org.openide.nodes.AbstractNode;
import org.openide.util.lookup.Lookups;
import org.openide.util.lookup.ProxyLookup;
import org.sbolstandard.core2.SBOLValidationException;
import uk.ac.ncl.intbio.core.io.CoreIoException;

@Messages({
    "LBL_SVPFragment_LOADER=Files of SVPFragment"
})
@MIMEResolver.NamespaceRegistration(
        displayName = "#LBL_SVPFragment_LOADER",
        mimeType = "text/sbol+xml",
        elementName = "RDF",
        elementNS = {"http://www.w3.org/1999/02/22-rdf-syntax-ns#"},
        position = 301
)
@DataObject.Registration(
        mimeType =  "text/sbol+xml",
        iconBase = "uk/ac/ncl/icos/synbad/images/icon.gif",
        displayName = "#LBL_SVPFragment_LOADER",
        position = 300
)
@ActionReferences({
    @ActionReference(
            path = "Loaders/text/sbol+xml/Actions",
            id = @ActionID(category = "System", id = "org.openide.actions.OpenAction"),
            position = 100,
            separatorAfter = 200
    ),
    @ActionReference(
            path = "Loaders/text/sbol+xml/Actions",
            id = @ActionID(category = "Edit", id = "org.openide.actions.CutAction"),
            position = 300
    ),
    @ActionReference(
            path = "Loaders/text/sbol+xml/Actions",
            id = @ActionID(category = "Edit", id = "org.openide.actions.CopyAction"),
            position = 400,
            separatorAfter = 500
    ),
    @ActionReference(
            path = "Loaders/text/sbol+xml/Actions",
            id = @ActionID(category = "Edit", id = "org.openide.actions.DeleteAction"),
            position = 600
    ),
    @ActionReference(
            path = "Loaders/text/sbol+xml/Actions",
            id = @ActionID(category = "System", id = "org.openide.actions.RenameAction"),
            position = 700,
            separatorAfter = 800
    ),
    @ActionReference(
            path = "Loaders/text/sbol+xml/Actions",
            id = @ActionID(category = "System", id = "org.openide.actions.SaveAsTemplateAction"),
            position = 900,
            separatorAfter = 1000
    ),
    @ActionReference(
            path = "Loaders/text/sbol+xml/Actions",
            id = @ActionID(category = "System", id = "org.openide.actions.FileSystemAction"),
            position = 1100,
            separatorAfter = 1200
    ),
    @ActionReference(
            path = "Loaders/text/sbol+xml/Actions",
            id = @ActionID(category = "System", id = "org.openide.actions.ToolsAction"),
            position = 1300
    ),
    @ActionReference(
            path = "Loaders/text/sbol+xml/Actions",
            id = @ActionID(category = "System", id = "org.openide.actions.PropertiesAction"),
            position = 1400
    )
})
public class SbolDocDO extends MultiDataObject implements Lookup.Provider {
    
    Lookup sourceLookup;

    public SbolDocDO(FileObject pf, MultiFileLoader loader) throws DataObjectExistsException, IOException, CoreIoException, XMLStreamException, SBOLValidationException {
        super(pf, loader);
        sourceLookup = Lookups.fixed(pf, this);
    }

    @Override
    protected Node createNodeDelegate() {
        return new AbstractNode(Children.LEAF);
    }
    
    public @Override Lookup getLookup() {     
        return new ProxyLookup(getCookieSet().getLookup(), sourceLookup);              
    }
   

    public class SynBadProjectSavable extends AbstractSavable {

        public void SynBadProjectSavable()
        {
            register();
        }
        
        @Override
        protected String findDisplayName() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        protected void handleSave() throws IOException {
            unregister();
        }

        @Override
        public boolean equals(Object obj) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public int hashCode() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
        
    }
    
}
