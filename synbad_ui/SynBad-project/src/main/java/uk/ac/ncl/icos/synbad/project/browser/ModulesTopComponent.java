/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.project.browser;

import java.util.ArrayList;
import java.util.List;
import javax.swing.Action;
import org.netbeans.api.project.Project;
import org.netbeans.api.settings.ConvertAsProperties;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.explorer.ExplorerUtils;
import org.openide.util.Lookup;
import org.openide.util.Lookup.Result;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.NbBundle.Messages;
import org.openide.windows.TopComponent;
import uk.ac.ncl.icos.synbad.project.data.SBContextProvider;
import uk.ac.ncl.icos.synbad.project.data.SBProj;


/**
 * Top component which displays something.
 */

@ConvertAsProperties(
        dtd = "-//uk.ac.ncl.icos.synbad.project.browser//ProjectBrowser//EN",
        autostore = false
)
@TopComponent.Description(
        preferredID = "ProjectBrowserTopComponent",
        //iconBase="SET/PATH/TO/ICON/HERE", 
        persistenceType = TopComponent.PERSISTENCE_ALWAYS
)
@TopComponent.Registration(mode = "explorer", openAtStartup = true)
@ActionID(category = "Window", id = "uk.ac.ncl.icos.synbad.project.browser.ModulesTopComponent")
@ActionReference(path = "Menu/Window", position = 222)
@TopComponent.OpenActionRegistration(
        displayName = "#CTL_ModulesAction",
        preferredID = "ModulesTopComponent"
)
@Messages({
    "CTL_ModulesAction=ModulesBrowser",
    "CTL_ModulesTopComponent=Modules",
    "HINT_ModulesTopComponent=Module browser"
})/**/
public final class ModulesTopComponent extends TopComponent implements LookupListener  {

    //private final ProjectManager manager;

    private List<Action> actions = new ArrayList<>();
    private final Result<Project> res;
    
    public ModulesTopComponent() {
        initComponents();
        setName(Bundle.CTL_ModulesTopComponent());
        setToolTipText(Bundle.HINT_ModulesTopComponent());
        SBContextProvider cp =  Lookup.getDefault().lookup(SBContextProvider.class);
        res = cp.getLookup().lookupResult(Project.class);
        res.allItems();
        res.addLookupListener(this);
        associateLookup(ExplorerUtils.createLookup(modulePanel1.getExplorerManager(), getActionMap())); 
    }
    
    @Override
    public void resultChanged(LookupEvent le) {
        if(res.allInstances().isEmpty())
            modulePanel1.clearWorkspace();
        else {
            Project sbproj = res.allInstances().iterator().next();
            if(sbproj != null && sbproj instanceof SBProj)
                modulePanel1.setWorkspace(((SBProj)sbproj).getWorkspace());
        }
    }


    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        modulePanel1 = new uk.ac.ncl.icos.synbad.project.browser.ModulePanel();

        setLayout(new java.awt.GridBagLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridwidth = java.awt.GridBagConstraints.REMAINDER;
        gridBagConstraints.gridheight = java.awt.GridBagConstraints.REMAINDER;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        add(modulePanel1, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private uk.ac.ncl.icos.synbad.project.browser.ModulePanel modulePanel1;
    // End of variables declaration//GEN-END:variables
    
    @Override
    public void componentOpened() {
        //project = manager.getLookup().lookup(SbolDocDO.class);
    }

    @Override
    public Action[] getActions() {
        return super.getActions(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void componentClosed() {
        // TODO add custom code on component closing
    }

    void writeProperties(java.util.Properties p) {
        // better to version settings since initial version as advocated at
        // http://wiki.apidesign.org/wiki/PropertyFiles
        p.setProperty("version", "1.0");
        // TODO store your settings
    }

    void readProperties(java.util.Properties p) {
        String version = p.getProperty("version");
        // TODO read your settings according to their version
    }

    
}
