/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.project.data;

import java.io.IOException;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ui.OpenProjects;
import org.netbeans.spi.project.ProjectFactory;
import org.netbeans.spi.project.ProjectState;
import org.openide.filesystems.FileObject;
import org.openide.util.lookup.ServiceProvider;

/**
 *
 * @author owengilfellon
 */
@ServiceProvider(service=ProjectFactory.class)
public class SBProjFactory implements ProjectFactory {
    
    public static final String PROJECT_FILE = "sbproject.txt";

    @Override
    public boolean isProject(FileObject fo) {
        
        // currently using the presence of a properties file to identify
        // synbad projects...
        
        return fo.getFileObject(PROJECT_FILE) != null;
    }

    @Override
    public Project loadProject(FileObject fo, ProjectState ps) throws IOException {

        if(!isProject(fo))
            return null;
        
        SBProj proj = new SBProj(fo, ps);
        return proj;
        /*
        assert proj != null;
        
        // Only allow one project open at a time
        
        OpenProjects op = OpenProjects.getDefault();
        op.close(op.getOpenProjects());
        
        */
    }

    @Override
    public void saveProject(Project prjct) throws IOException, ClassCastException {
        // write workspaces to sbol
    }
    
}
