/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.project.browser;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.Action;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.explorer.view.BeanTreeView;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import uk.ac.ncl.icos.datamodel.workspace.api.SBEventFilter;
import uk.ac.ncl.icos.datamodel.workspace.api.SBWorkspace;
import uk.ac.ncl.icos.datamodel.workspace.objects.Identity;
import uk.ac.ncl.icos.datamodel.workspace.sbol.Definition;
import uk.ac.ncl.icos.datamodel.workspace.sbol.ModuleDefinition;
import uk.ac.ncl.icos.synbad.datadefinition.UriHelper;
import uk.ac.ncl.icos.synbad.event.SBEvent;
import uk.ac.ncl.icos.synbad.event.SBSubscriber;
import uk.ac.ncl.icos.synbad.project.dialog.ModuleEditPanel;
import uk.ac.ncl.icos.synbad.svpfragment.nodes.SBDefinitionNode;

/**
 *
 * @author owengilfellon
 */
public class ModulePanel extends AbstractBrowserPanel {

    BeanTreeView view;
    
    public ModulePanel() {
        super();
        initComponents();
        
        GridBagLayout layout = new GridBagLayout();
        this.setLayout(layout);
        explorerManager.setRootContext(new AbstractNode(Children.LEAF));
        
        view = new BeanTreeView();
        view.setRootVisible(false);

        GridBagConstraints fill = new GridBagConstraints();
        fill.gridx= GridBagConstraints.REMAINDER;
        fill.gridy= GridBagConstraints.REMAINDER;
        fill.fill = GridBagConstraints.BOTH;     
        fill.weightx=1.0;
        fill.weighty=1.0;
        add(view, fill);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setLayout(new java.awt.GridBagLayout());
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables

    
    
    @Override
    public void setWorkspace(SBWorkspace workspace) {
        this.workspace = workspace;
        RootModuleNode rootNode = new RootModuleNode(Children.create(new ModulePanel.ModulePanelNodeFactory(workspace), true));
        this.explorerManager.setRootContext(rootNode);
    }

    @Override
    public void clearWorkspace() {
        this.explorerManager.setRootContext(Node.EMPTY);
        this.workspace = null;
    }

    private class ModulePanelNodeFactory extends ChildFactory<Definition> implements SBSubscriber {

        private final SBWorkspace workspace;

        public ModulePanelNodeFactory(SBWorkspace workspace) {
            this.workspace = workspace;
            workspace.getDispatcher().subscribe(
                ModuleDefinition.ModuleDefinitionCreatedEvent.class,
                new SBEventFilter.DefaultFilter(),
                this);
        }

        @Override
        protected boolean createKeys(List<Definition> toPopulate) {
           
            for(ModuleDefinition definition : workspace.getObjects(ModuleDefinition.class)) {
                toPopulate.add(definition);
            }
            
            return true;
        }

        @Override
        protected Node createNodeForKey(Definition key) {
            return new SBDefinitionNode(key);
        }

        @Override
        public void onEvent(SBEvent e) {
            refresh(true);
        }

    }
    
    class RootModuleNode extends AbstractNode {

        public RootModuleNode(Children children) {
            super(children);
        }

        @Override
        public Action[] getActions(boolean context) {
            return new Action[] {
             
                new AbstractAction("New Module") {
                
                    @Override
                    public void actionPerformed(ActionEvent e) {

                        ModuleEditPanel form = new ModuleEditPanel();
                        String msg = "New Module";
                        DialogDescriptor dd = new DialogDescriptor(form, msg);
                        Object result = DialogDisplayer.getDefault().notify(dd);
                        if (result != NotifyDescriptor.OK_OPTION) {
                            return;
                        }

                        Identity identity = new Identity(UriHelper.synbad.getNamespaceURI(), form.getID(), "1.0");
                        workspace.perform(new ModuleDefinition.CreateModuleDefinitionAction(new Identity(UriHelper.synbad.getNamespaceURI(), form.getID(), "1.0")));
                        ModuleDefinition module = workspace.getObject(identity.getIdentity(), ModuleDefinition.class);
                        module.setName(form.getDisplayName());
                        module.setDescription(form.getDescription());
                    }
                }   /**/
            };
        }
    }
}
