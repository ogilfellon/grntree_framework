/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.project.browser.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.concurrent.atomic.AtomicInteger;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.xml.stream.FactoryConfigurationError;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionRegistration;
import org.openide.filesystems.FileChooserBuilder;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.NbBundle.Messages;
import org.sbolstandard.core2.SBOLConversionException;
import org.sbolstandard.core2.SBOLDocument;
import org.sbolstandard.core2.SBOLWriter;
import uk.ac.ncl.icos.synbad.datadefinition.UriHelper;
import uk.ac.ncl.icos.synbad.project.data.SBContextProvider;
import uk.ac.ncl.icos.synbad.project.data.SbolDocDO;

@ActionID(
        category = "File",
        id = "uk.ac.ncl.icos.synbad.project.NewProject"
)
@ActionRegistration(
        displayName = "#CTL_NewProject"
)
@ActionReference(path = "Menu/File", position = 0)
@Messages("CTL_NewProject=New Project")
public final class NewProject implements ActionListener {
    
    private static final AtomicInteger fileCount = new AtomicInteger(0);

    @Override
    public void actionPerformed(ActionEvent e) {
        File home = new File(System.getProperty("user.home"));
        File projectDirectory = new FileChooserBuilder("user-dir").setTitle("Create Project").setDefaultWorkingDirectory(home).setDirectoriesOnly(true).setFileFilter(new FileNameExtensionFilter("SynBad", ".xml")).showSaveDialog();
        try {
 
            FileObject fileObject = FileUtil.createData(projectDirectory);
            SBOLDocument document = new SBOLDocument();
            document.addNamespace(UriHelper.synbad.withLocalPart("synbad"));
            try (OutputStream os = fileObject.getOutputStream()) {
                SBOLWriter.write(document, os);
                os.flush();
            } catch (SBOLConversionException ex) {
                Exceptions.printStackTrace(ex);
            }            
            if(fileObject.getMIMEType().equals("text/synbadsbol+xml")) {
                SbolDocDO dataObject = (SbolDocDO)DataObject.find(fileObject);
                SBContextProvider projectManager = Lookup.getDefault().lookup(SBContextProvider.class);
               // projectManager.openProject(dataObject);
            }
        } catch (FactoryConfigurationError | IOException ex) {
            Exceptions.printStackTrace(ex);
        }   
    }
}

