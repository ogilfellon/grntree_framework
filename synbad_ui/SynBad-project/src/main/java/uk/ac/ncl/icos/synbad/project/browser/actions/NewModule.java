/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.project.browser.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.awt.ActionID;
import org.openide.awt.ActionRegistration;
import org.openide.util.NbBundle.Messages;
import org.sbolstandard.core2.Annotation;
import uk.ac.ncl.icos.datamodel.workspace.api.SBWorkspace;
import uk.ac.ncl.icos.synbad.datadefinition.UriHelper;
import uk.ac.ncl.icos.synbad.datadefinition.types.EntityType;
import uk.ac.ncl.icos.synbad.project.dialog.ModuleEditPanel;
import uk.ac.ncl.icos.datamodel.workspace.objects.Identity;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;

@ActionID(
        category = "File",
        id = "uk.ac.ncl.icos.synbad.project.browser.actions.NewModule"
)
@ActionRegistration(
        displayName = "#CTL_NewModule"
)
@Messages("CTL_NewModule=New Module")
public final class NewModule implements ActionListener {

    private final SBWorkspace manager;
    
    public NewModule(SBWorkspace manager) {
        this.manager = manager;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        /*
        ModuleEditPanel form = new ModuleEditPanel();
        String msg = "Something bad happened...";
        DialogDescriptor dd = new DialogDescriptor(form, msg);
        Object result = DialogDisplayer.getDefault().notify(dd);
        if (result != NotifyDescriptor.OK_OPTION) {
            return;
        }
        System.out.println(form.getID());
        System.out.println(form.getDisplayName());
        System.out.println(form.getDescription());
        
        IEntity module = manager.createEntity(new Identity(UriHelper.synbad.getNamespaceURI(), form.getID(), "1.0"));
        module.addAnnotation(SynBadTerms.Annotatable.entitytype,
                                new Annotation(SynBadTerms.Annotatable.entitytype, EntityType.Module.getUri()));
        module.setName(form.getDisplayName());
        module.setDescription(form.getDescription());*/
    }
}
