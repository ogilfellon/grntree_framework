/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.project.data;

import java.awt.Image;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.URI;
import javax.swing.Action;
import org.netbeans.spi.project.ui.LogicalViewProvider;
import org.netbeans.spi.project.ui.support.CommonProjectActions;
import org.netbeans.spi.project.ui.support.NodeFactorySupport;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataObject;
import org.openide.nodes.FilterNode;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;
import org.openide.util.lookup.ProxyLookup;
import uk.ac.ncl.icos.datamodel.workspace.api.SBWorkspace;
import uk.ac.ncl.icos.datamodel.workspace.api.SBWorkspaceManager;
import uk.ac.ncl.icos.synbad.datadefinition.UriHelper;

/**
 *
 * @author owengilfellon
 */
public class SBProjLogicalView implements LogicalViewProvider {
    
    private final SBProj proj;
    
    public SBProjLogicalView(SBProj proj) {
        this.proj = proj;
    }

    @Override
    public Node createLogicalView() {
        
        // Retrieve the DataFolder for the project directory - contains the
        // DataObjects for the project
        
        FileObject projDir = proj.getProjectDirectory();
        DataFolder projFolder = DataFolder.findFolder(projDir);
        
        proj.getLookup().lookup(SBWorkspaceProvider.class).getWorkspace();

        // Decorate the folder's node, decorate and return
        
        Node projFolderNode = projFolder.getNodeDelegate();
        return new ProjectNode(projFolderNode, proj);        
    }

    @Override
    public Node findPath(Node node, Object o) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    private final class ProjectNode extends FilterNode {
        
        private final SBProj proj;
        
        public ProjectNode(Node node, SBProj proj) {
            super(  node,
                    NodeFactorySupport.createCompositeChildren(
                        proj,
                        "Projects/synbad-project/Nodes"),
                    new ProxyLookup(
                        Lookups.fixed(proj),
                        node.getLookup()
                    ));
            this.proj = proj;
        }

        @Override
        public Action[] getActions(boolean context) {
            
            // include common Project actions provided by NetBeans Platform 
            
            return new Action[] {
                CommonProjectActions.newFileAction(),
                CommonProjectActions.copyProjectAction(),
                CommonProjectActions.deleteProjectAction(),
                CommonProjectActions.renameProjectAction(),
                CommonProjectActions.closeProjectAction()
            };
        }

        @Override
        public Image getIcon(int type) {
            return ImageUtilities.loadImage("uk/ac/ncl/icos/synbad/images/icon.gif");
        }

        @Override
        public Image getOpenedIcon(int type) {
            return getIcon(type);
        }

        @Override
        public String getDisplayName() {
            return proj.getProjectDirectory().getName();
        }
    }
}
