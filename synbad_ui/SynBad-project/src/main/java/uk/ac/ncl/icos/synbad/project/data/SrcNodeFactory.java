/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.project.data;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.swing.event.ChangeListener;
import org.netbeans.api.project.Project;
import org.netbeans.spi.project.ui.support.NodeFactory;
import org.netbeans.spi.project.ui.support.NodeList;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataObject;
import org.openide.nodes.Children;
import org.openide.nodes.FilterNode;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;
import uk.ac.ncl.icos.datamodel.workspace.api.SBWorkspace;
import uk.ac.ncl.icos.datamodel.workspace.api.SBWorkspaceManager;
import uk.ac.ncl.icos.datamodel.workspace.sbol.Sequence;
import uk.ac.ncl.icos.synbad.datadefinition.UriHelper;
import uk.ac.ncl.icos.synbad.svpfragment.nodes.SBTopLevelNode;

/**
 *
 * @author owengilfellon
 */
@NodeFactory.Registration(projectType="synbad-project", position = 10)
public class SrcNodeFactory implements NodeFactory {

    @Override
    public NodeList<?> createNodes(Project p) {
        SBProj proj = p.getLookup().lookup(SBProj.class);
        assert proj != null;
        return new SrcNodeList(proj);
    }
    
    private class SrcNodeList implements NodeList<Node> {
        
        
        private final SBProj proj;

        public SrcNodeList(SBProj proj) {
            this.proj = proj;
        }

        @Override
        public List<Node> keys() {
           
            
            FileObject projDir = proj.getProjectDirectory();
            FileObject srcFo = projDir.getFileObject("src");
            DataFolder srcDf = DataFolder.findFolder(srcFo);
            
            List<Node> keys = new ArrayList<>();
            
            for(DataObject obj : srcDf.getChildren()) {
                SbolDocDO sbol = obj.getLookup().lookup(SbolDocDO.class);
                if(sbol!=null) {
                    keys.add(new SbolDocNode(sbol));
                }
            }
            
            return keys;  
        }

        @Override
        public void addChangeListener(ChangeListener l) {
        
        }

        @Override
        public void removeChangeListener(ChangeListener l) {
        
        }

        @Override
        public Node node(Node key) {
            return new FilterNode(key);
        }

        @Override
        public void addNotify() {
        
        }

        @Override
        public void removeNotify() {
        
        }
    }
}
