/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.project.browser.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.atomic.AtomicInteger;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionRegistration;
import org.openide.util.NbBundle.Messages;

@ActionID(
        category = "File",
        id = "uk.ac.ncl.icos.synbad.project.NewExampleProject"
)
@ActionRegistration(
        displayName = "#CTL_NewExampleProject"
)
@ActionReference(path = "Menu/File", position = 1)
@Messages("CTL_NewExampleProject=New Example Project")
public final class NewExampleProject implements ActionListener {
    
    private static final AtomicInteger fileCount = new AtomicInteger(0);

    @Override
    public void actionPerformed(ActionEvent e) {
      /*  File home = new File(System.getProperty("user.home"));
        File projectFile = new FileChooserBuilder("user-dir").setTitle("Create Project").setDefaultWorkingDirectory(home).setFileFilter(new FileNameExtensionFilter("SynBad", ".xml")).showSaveDialog();
       
        try {
            FileObject fileObject = FileUtil.createData(projectFile);
            
            // Create a document and write it to disk...
            
            SBOLDocument document = SbolParser.write(ExampleFactory.setupTemplateModel().getWorkspace());
           
            try (OutputStream os = fileObject.getOutputStream()) {
                SBOLWriter.write(document, os);
                os.flush();
            }            
            
            // Load example document
            
            if(fileObject.getMIMEType().equals("text/synbadsbol+xml")) {
                SbolDataObject dataObject = (SbolDataObject)DataObject.find(fileObject);
                ProjectManager projectManager = Lookup.getDefault().lookup(ProjectManager.class);
                projectManager.openProject(dataObject);
            }
        } catch (FactoryConfigurationError | IOException ex) {
            Exceptions.printStackTrace(ex);
        }    */
    }
}

