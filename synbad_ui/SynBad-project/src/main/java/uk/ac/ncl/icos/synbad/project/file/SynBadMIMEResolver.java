/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.project.file;

import java.io.IOException;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.MIMEResolver;
import org.openide.util.Exceptions;
import org.openide.util.lookup.ServiceProvider;

/**
 * SynBad Projects are recognised by searching for the presence of the SynBad
 * namespace - xmlns:synbad="http://www.synbad.org#" - in the root element.
 * @author owengilfellon
 */
@ServiceProvider(service=MIMEResolver.class)
public class SynBadMIMEResolver extends MIMEResolver {
    
    private static String mimeType="text/synbadsbol+xml";

    @Override
    public String findMIMEType(FileObject fo) {
        if(fo.hasExt("xml")) {
            try {
                String contents = fo.asText();
                if(contents != null
                        && contents.length() > 100
                        && contents.substring(0, 100).contains("xmlns:synbad=\"http://www.synbad.org#\"")) {
                   return mimeType; 
                }
                
            } catch (IOException ex) {
                return null;
            }
        }
        return null;
    }
}
