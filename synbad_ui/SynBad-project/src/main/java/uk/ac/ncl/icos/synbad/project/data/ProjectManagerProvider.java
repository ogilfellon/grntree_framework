/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.project.data;

import org.openide.util.Lookup;
import org.openide.util.lookup.ServiceProvider;

/**
 *
 * @author owengilfellon
 */
public interface ProjectManagerProvider {
    
    public SBContextProvider getProjectManager();
    
    @ServiceProvider(service = ProjectManagerProvider.class)
    public class DefaultProjectManagerProvider implements ProjectManagerProvider{

        public static SBContextProvider manager = null;
        
        @Override
        public SBContextProvider getProjectManager() {
            if (manager == null)
                manager = Lookup.getDefault().lookup(SBContextProvider.class);
            return manager;
        }

    }
    
}
