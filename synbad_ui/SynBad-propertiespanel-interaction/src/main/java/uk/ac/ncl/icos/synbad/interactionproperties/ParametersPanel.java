/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.interactionproperties;


import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.ArrayList;
import java.util.List;
import javax.swing.SwingWorker;
import org.openide.explorer.view.OutlineView;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.RequestProcessor;
import uk.ac.ncl.icos.svpmanager.SVPManager;
import uk.ac.ncl.icos.synbad.svpfragment.old.SVPInteractionParameterNode;
import uk.ac.ncl.icos.synbad.ui.CellRenderer;
import uk.ac.ncl.icos.synbad.ui.ExplorerPanel;

import uk.ac.ncl.intbio.virtualparts.entity.Interaction;
import uk.ac.ncl.intbio.virtualparts.entity.Parameter;

/**
 *
 * @author owengilfellon
 */
public class ParametersPanel extends ExplorerPanel {

    private final SVPManager svp_manager = SVPManager.getSVPManager();
    private final OutlineView  view;

    public ParametersPanel() {
       
        view = new OutlineView ("Name");
        view.getOutline().setRootVisible(false);
        view.addPropertyColumn("type", "Type");
        view.addPropertyColumn("value", "Value");
        view.addPropertyColumn("evidenceType", "Evidence Type");
        view.getOutline().setDefaultRenderer(Node.Property.class, new CellRenderer());
        GridBagLayout layout = new GridBagLayout();
        this.setLayout(layout);
        GridBagConstraints fill = new GridBagConstraints();
        fill.gridx=0;
        fill.gridy=0;
        fill.fill = GridBagConstraints.BOTH;     
        fill.weightx=1.0;
        fill.weighty=1.0;
        add(view, fill);
    }
   
    public void setInteraction(Interaction interaction) {

        if(interaction!=null) {
            InteractionsWorker worker = new InteractionsWorker(interaction);
            RequestProcessor.getDefault().post(worker);
        }
        
    }
    
    private class InteractionsWorker extends SwingWorker<Object, Object> {

        private final Interaction interaction;

        public InteractionsWorker(Interaction interaction) {
            this.interaction = interaction;    
        }

        @Override
        protected void done() {
            super.done();
        }

        @Override
        protected Object doInBackground() throws Exception {
           
            List<SVPInteractionParameterNode> nodes = new ArrayList<>();
            
            for(Parameter parameter : interaction.getParameters()) {
                nodes.add(new SVPInteractionParameterNode(parameter));
            }
            
            Children.Array children = new Children.Array();
            children.add(nodes.toArray(new AbstractNode[nodes.size()]));
            view.setVisible(true);
            getExplorerManager().setRootContext(new AbstractNode(children));   
           
            return null;
        }
    }
}
