/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.interactionproperties;

import java.awt.GridBagConstraints;
import java.util.Collection;
import org.openide.util.lookup.ServiceProvider;
import uk.ac.ncl.icos.synbad.properties.PropertyPanel;
import uk.ac.ncl.icos.synbad.svpfragment.old.SVPInteractionNode;
import uk.ac.ncl.intbio.virtualparts.entity.Interaction;

/**
 *
 * @author owengilfellon
 */
@ServiceProvider(service=PropertyPanel.Adapter.class)
public class InteractionProperties extends PropertyPanel.Adapter<SVPInteractionNode> {

    InteractionPropertiesPanel panel = new InteractionPropertiesPanel();
    
    public InteractionProperties() {
        super(SVPInteractionNode.class);
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 1.0;
        c.weighty = 1.0;
        c.fill = GridBagConstraints.BOTH;
        add(panel, c);
    }

    @Override
    public void hasResult(Collection<? extends SVPInteractionNode> instances) {
        SVPInteractionNode node = instances.iterator().next();
        Interaction interaction = node.getLookup().lookup(Interaction.class);
        panel.setInteraction(interaction);
    }

    @Override
    public String getDisplayName() {
        return "Interaction Properties";
    }
    
}
