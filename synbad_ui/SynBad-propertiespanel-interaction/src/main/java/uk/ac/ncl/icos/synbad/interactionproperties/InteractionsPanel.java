/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.interactionproperties;

import uk.ac.ncl.icos.synbad.ui.ExplorerPanel;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.swing.SwingWorker;
import org.openide.explorer.view.OutlineView;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.RequestProcessor;
import org.openide.util.Utilities;
import uk.ac.ncl.icos.svpmanager.SVPManager;
import uk.ac.ncl.icos.synbad.svpfragment.nodes.PartNode;
import uk.ac.ncl.icos.synbad.ui.CellRenderer;
import uk.ac.ncl.intbio.virtualparts.entity.Interaction;
import uk.ac.ncl.intbio.virtualparts.entity.Part;

/**
 *
 * @author owengilfellon
 */
public class InteractionsPanel extends ExplorerPanel implements LookupListener {
    
    private Lookup.Result<PartNode> result = null;
    private final SVPManager svp_manager = SVPManager.getSVPManager();
    private final OutlineView  view;

    public InteractionsPanel() {
        result = Utilities.actionsGlobalContext().lookupResult(PartNode.class);
        result.addLookupListener(this);
        view = new OutlineView ("Part");
        view.getOutline().setRootVisible(false);
        view.addPropertyColumn("interactionType", "Interaction Type");
        view.getOutline().setDefaultRenderer(Node.Property.class, new CellRenderer());
        GridBagLayout layout = new GridBagLayout();
        this.setLayout(layout);
        GridBagConstraints fill = new GridBagConstraints();
        fill.gridx=0;
        fill.gridy=0;
        fill.fill = GridBagConstraints.BOTH;     
        fill.weightx=1.0;
        fill.weighty=1.0;
        add(view, fill);
    }
    
    @Override
    public void resultChanged(LookupEvent ev) {
        Collection<? extends PartNode> nodes = result.allInstances();
        if(!nodes.isEmpty()) {
                PartNode node = nodes.iterator().next();
                InteractionsWorker worker = new InteractionsWorker(node);
                RequestProcessor.getDefault().post(worker);
        }
    }
    
    private class InteractionsWorker extends SwingWorker<Object, Object> {

        private final PartNode node;

        public InteractionsWorker(PartNode node) {
            this.node = node;    
        }

        @Override
        protected void done() {
            super.done();
        }

        @Override
        protected Object doInBackground() throws Exception {
           /*
            Part p = node.getLookup().lookup(GRNTreeNode.class).getSVP();
            List<Interaction> interactions = svp_manager.getInteractions(p);

            if(interactions==null || interactions.isEmpty()) {
                getExplorerManager().setRootContext(new AbstractNode(Children.LEAF));
                return null;
            }   */
            /* 
            //List<SVPFromToNode> leafNodes = new ArrayList<>();  
            boolean clear = true;
            
            for(Interaction interaction :  interactions) {
                if(!interaction.getInteractionType().equals("Metabolic Reaction")){
                    for(String s : interaction.getParts()) {
                            if(!s.equals(p.getName())) {      
                     leafNodes.add(new SVPFromToNode( node,
                                                    GRNTreeNodeFactory.getLeafNode(
                                                    svp_manager.getPart(s), InterfaceType.INPUT)));
                            clear = false;
                        }
                    }
                }
            }
            
            if(clear) {
                 getExplorerManager().setRootContext(new AbstractNode(Children.LEAF));
                 return null;
            }
            
            view.setVisible(true);
            Children.Array children = new Children.Array();
            
            */
          //  children.add(leafNodes.toArray(new SVPFromToNode[leafNodes.size()]));
          //  getExplorerManager().setRootContext(new AbstractNode(children));     
          
            return null;
        }
    } 
}
