/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui;

import javax.swing.JPanel;
import org.openide.explorer.ExplorerManager;

/**
 *
 * @author owengilfellon
 */
public class ExplorerPanel extends JPanel implements ExplorerManager.Provider {

    private final ExplorerManager explorerManager = new ExplorerManager();

    public ExplorerPanel() {
    }
    
    @Override
    public ExplorerManager getExplorerManager() {
        return explorerManager;
    }
    
}
