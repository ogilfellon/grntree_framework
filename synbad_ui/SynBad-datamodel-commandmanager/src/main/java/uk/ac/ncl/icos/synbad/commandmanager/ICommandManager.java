/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.commandmanager;


import uk.ac.ncl.icos.synbad.command.ICommand;
import org.openide.util.lookup.ServiceProvider;

/**
 *
 * @author owengilfellon
 */
public interface ICommandManager {
    
    public void execute(ICommand command);
    public void undo();
    
    @ServiceProvider(service = ICommandManager.class)
    public class DefaultCommandManager implements ICommandManager{
  

        private boolean modelSet = false;

        public DefaultCommandManager() {
        }

        @Override
        public void execute(ICommand command) {
            command.execute();
        }

        @Override
        public void undo() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

    }
}
