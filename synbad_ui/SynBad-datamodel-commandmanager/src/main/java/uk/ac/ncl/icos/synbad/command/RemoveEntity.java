/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.command;

import uk.ac.ncl.icos.datamodel.workspace.api.IEntity;
import uk.ac.ncl.icos.datamodel.workspace.impl.EntityInstance;

/**
 *
 * @author owengilfellon
 */
public class RemoveEntity implements ICommand {

    private final EntityInstance child;
    private final IEntity parent;
    private final Integer index;
    
    public RemoveEntity(EntityInstance child, IEntity parent) {
        this.child = child;
        this.parent = parent;
        this.index = null;
    }

    public RemoveEntity(IEntity parent, int index) {
        this.child = null;
        this.parent = parent;
        this.index = index;
    }

    @Override
    public void execute() {
        if (index != null) {
            parent.removeChild(index);
        } else {
            parent.removeChild(child);
        }
    }
}
