/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.command;

import uk.ac.ncl.icos.datamodel.workspace.api.IEntity;
import uk.ac.ncl.icos.datamodel.workspace.impl.EntityInstance;

/**
 *
 * @author owengilfellon
 */
public class InstantiateEntity implements ICommand {

    private final IEntity child;
    private final  IEntity parent;
    private Integer index = null;

    public InstantiateEntity(IEntity child, IEntity parent) {
        this.child = child;
        this.parent = parent;

        // TO-DO: check for loop
    }

    public InstantiateEntity(IEntity child, IEntity parent, int index) {
        this.child = child;
        this.parent = parent;
        this.index = index;
    }

    @Override
    public void execute() {
        if (index != null) {
            parent.addChild(index, child);
        } else {
            parent.addChild(child);
        }
    }
}
