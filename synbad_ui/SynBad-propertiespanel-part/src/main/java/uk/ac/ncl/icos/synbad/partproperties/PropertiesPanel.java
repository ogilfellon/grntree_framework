/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.partproperties;


import uk.ac.ncl.icos.synbad.ui.ExplorerPanel;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.Collection;
import javax.swing.SwingWorker;
import org.openide.explorer.view.OutlineView;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.Utilities;
import uk.ac.ncl.icos.svpmanager.SVPManager;
import uk.ac.ncl.icos.synbad.svpfragment.nodes.SynBadDefinitonNode;
import uk.ac.ncl.icos.synbad.svpfragment.nodes.VprPartNode;
import uk.ac.ncl.icos.synbad.ui.CellRenderer;

/**
 *
 * @author owengilfellon
 */
public class PropertiesPanel extends ExplorerPanel implements LookupListener {
    
    private Lookup.Result<SynBadDefinitonNode> result = null;
    private final SVPManager svp_manager = SVPManager.getSVPManager();
    private final OutlineView  view;

    public PropertiesPanel() {
        result = Utilities.actionsGlobalContext().lookupResult(SynBadDefinitonNode.class);
        result.addLookupListener(this);
        view = new OutlineView ("Property");
        view.getOutline().setRootVisible(false);
        view.addPropertyColumn("value", "Value");
        view.addPropertyColumn("description", "Description");
        view.getOutline().setDefaultRenderer(Node.Property.class, new CellRenderer());
        GridBagLayout layout = new GridBagLayout();
        
        this.setLayout(layout);
        GridBagConstraints fill = new GridBagConstraints();
        fill.gridx=0;
        fill.gridy=0;
        fill.fill = GridBagConstraints.BOTH;     
        fill.weightx=1.0;
        fill.weighty=1.0;
        add(view, fill);
    }
    
    @Override
    public void resultChanged(LookupEvent ev) {

        Collection<? extends SynBadDefinitonNode> nodes = result.allInstances();
        if(!nodes.isEmpty()) {

            SynBadDefinitonNode node = nodes.iterator().next();
               // InteractionsWorker worker = new InteractionsWorker(node);
               // RequestProcessor.getDefault().post(worker);
        }
    }
    
    private class InteractionsWorker extends SwingWorker<Object, Object> {

      
        public InteractionsWorker(VprPartNode node) {
        }

        @Override
        protected void done() {
            super.done();
        }

        @Override
        protected Object doInBackground() throws Exception {
           return null;
        }
     }
    
}
