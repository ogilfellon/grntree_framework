/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.partproperties;


import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.ArrayList;
import java.util.List;
import javax.swing.SwingWorker;
import org.openide.explorer.view.BeanTreeView;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.util.RequestProcessor;
import uk.ac.ncl.icos.datamodel.workspace.objects.SynBadObject;
import uk.ac.ncl.icos.synbad.ui.ExplorerPanel;
import uk.ac.ncl.icos.synbad.svpfragment.nodes.AnnotationNode;


/**
 *
 * @author owengilfellon
 */
public class PartPropertiesExplorer extends ExplorerPanel {

    private final BeanTreeView  view;

    public PartPropertiesExplorer() {
       
        view = new BeanTreeView();
        view.setRootVisible(false);
        /*view.getOutline().setRootVisible(false);
        view.addPropertyColumn("value", "Value");
        view.addPropertyColumn("description", "Description");
        view.getOutline().setDefaultRenderer(Node.Property.class, new CellRenderer());*/
        GridBagLayout layout = new GridBagLayout();
        this.setLayout(layout);
        GridBagConstraints fill = new GridBagConstraints();
        fill.gridx=0;
        fill.gridy=0;
        fill.fill = GridBagConstraints.BOTH;     
        fill.weightx=1.0;
        fill.weighty=1.0;
        add(view, fill);
    }
   
    public void setPart(SynBadObject part) {

        if(part!=null) {
                PartsWorker worker = new PartsWorker(part);
            RequestProcessor.getDefault().post(worker);
        }
        
    }
    
    private class PartsWorker extends SwingWorker<Object, Object> {

        private final SynBadObject part;

        public PartsWorker(SynBadObject part) {
            this.part = part;
        }

        @Override
        protected void done() {
            super.done();
        }
        
        

        @Override
        protected Object doInBackground() throws Exception {
           
            List<AnnotationNode> nodes = new ArrayList<>();
            /*
            for(Annotation a : part.getAnnotations()) {
                
                nodes.add(new AnnotationNode(a));
            }*/
     
            Children.Array children = new Children.Array();
            children.add(nodes.toArray(new AnnotationNode[nodes.size()]));
            view.setVisible(true);
            getExplorerManager().setRootContext(new AbstractNode(children));

            
           
            return null;
        }
    }
}
