/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.repositorysearch.nodes;

import java.util.List;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;

/**
 *
 * @author owengilfellon
 */
public class PropertiesNode extends AbstractNode {

    public PropertiesNode(List<uk.ac.ncl.intbio.virtualparts.entity.Property> properties)
    {
        super(Children.create(new PropertyTypeFactory(properties), true));
    }

}
