/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.repositorysearch.nodes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import uk.ac.ncl.intbio.virtualparts.entity.Property;

/**
 *
 * @author owengilfellon
 */
public class PropertyTypeFactory extends ChildFactory<String> {
    
    List<Property> properties;
    Map<String, List<Property>> map;

    public PropertyTypeFactory(List<Property> properties) {
        this.properties = properties;
    }

    
    @Override
    protected boolean createKeys(List<String> toPopulate) {
        
        map = new HashMap<>();
        for(Property property:properties) {
            if(map.get(property.getName()) == null)
               map.put(property.getName(), new ArrayList<Property>() ); 
             map.get(property.getName()).add(property);   
        }
        
        toPopulate.addAll(map.keySet());
        
        return true;
    }

    @Override
    protected Node createNodeForKey(String key) {
        PropertyTypeNode node =  new PropertyTypeNode(Children.create(new PropertiesValueFactory(map.get(key)), true));
        node.setName(key);
        return node;
    }

}
