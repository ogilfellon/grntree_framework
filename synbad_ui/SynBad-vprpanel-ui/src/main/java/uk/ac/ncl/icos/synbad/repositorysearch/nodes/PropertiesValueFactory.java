/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.repositorysearch.nodes;

import java.util.List;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Node;
import uk.ac.ncl.intbio.virtualparts.entity.Property;

/**
 *
 * @author owengilfellon
 */
public class PropertiesValueFactory extends ChildFactory<String> {
    
    List<Property> properties;

    public PropertiesValueFactory(List<Property> properties) {
        this.properties = properties;
    }

    
    @Override
    protected boolean createKeys(List<String> toPopulate) {
        
        for(Property property:properties)
        {
            toPopulate.add(property.getValue());
        }
        
        return true;
    }

    @Override
    protected Node createNodeForKey(String key) {
        return new PropertyValueNode(key);
    }
    
    
    
}
