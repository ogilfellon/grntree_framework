/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.repositorysearch.nodes;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Node;
import uk.ac.ncl.intbio.virtualparts.entity.Part;

import java.util.List;
import uk.ac.ncl.icos.synbad.repositorysearch.api.RepositorySearchResults;
import uk.ac.ncl.icos.synbad.svpfragment.nodes.VprPartNode;
/**
 *
 * @author owengilfellon
 */
public class SearchResultFactory extends ChildFactory<Part> implements PropertyChangeListener  {

    RepositorySearchResults results;
    //IWorkspaceManager manager = 

    public SearchResultFactory(RepositorySearchResults results) {
        this.results = results;
        results.addPropertyChangeListener(this);
    }

    @Override
    protected boolean createKeys(List<Part> toPopulate) {
        toPopulate.addAll(results.getParts());
        return true; 
    }

    @Override
    protected Node createNodeForKey(Part key) {     
        Node result = new VprPartNode(key);
        return result;
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
       this.refresh(true);
    }   
}
