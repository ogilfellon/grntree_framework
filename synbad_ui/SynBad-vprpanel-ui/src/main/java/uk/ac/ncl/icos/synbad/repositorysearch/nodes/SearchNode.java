/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.repositorysearch.nodes;

import javax.swing.Action;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;

import uk.ac.ncl.icos.synbad.repositorysearch.api.RepositorySearchResults;

/**
 *
 * @author owengilfellon
 */
public class SearchNode extends AbstractNode {
    /**
     * Creates a node representing the part that has been passed. 
     * @param results
     */
    public SearchNode(RepositorySearchResults results) {  
        super(Children.create(new SearchResultFactory(results), true));
    }

    @Override
    public Action[] getActions(boolean context) {
        return super.getActions(context); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean canCopy() {
        return true;
    }



}
