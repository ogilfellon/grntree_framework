/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.repositorysearch.nodes;

import org.openide.nodes.*;
import org.openide.util.Exceptions;
import org.openide.util.lookup.Lookups;
import uk.ac.ncl.intbio.virtualparts.entity.Constraint;
import uk.ac.ncl.intbio.virtualparts.entity.Interaction;
import uk.ac.ncl.intbio.virtualparts.entity.InteractionPartDetail;

import java.util.List;

/**
 *
 * @author owengilfellon
 */
public class InteractionNode extends AbstractNode {
    
    
    public InteractionNode(Interaction interaction) {  
        // can use an InteractionsNodeFactory to list interactions, etc, a phosphorylationNodeFactory etc.....
        super(Children.LEAF, Lookups.singleton(interaction));
        setDisplayName(interaction.getName() + " : " + interaction.getInteractionType());
    }
 

    @Override
    protected Sheet createSheet() {
        
        Sheet sheet = Sheet.createDefault();
        Sheet.Set set = Sheet.createPropertiesSet();
        Interaction interaction = getLookup().lookup(Interaction.class);
    
        if(interaction!= null) { try {

            /*
            
            interaction.getDescription();
            interaction.getFreeTextMath();
            interaction.getInteractionType();
            interaction.getName();
            interaction.getParameters();
           
            interaction.getParts();
            */        
            
            Node.Property nameProperty = new PropertySupport.Reflection(interaction, String.class, "getName", null);
            Node.Property typeProperty = new PropertySupport.Reflection(interaction, String.class, "getInteractionType", null);
            Node.Property mathProperty = new PropertySupport.Reflection(interaction, String.class, "getFreeTextMath", null);

            nameProperty.setName("name");
            typeProperty.setName("type");
            mathProperty.setName("math");

            set.put(nameProperty);
            set.put(typeProperty);
            set.put(mathProperty);
            
            
            List<InteractionPartDetail> ipds =  interaction.getPartDetails();
            
            if(ipds!=null) {
                for(InteractionPartDetail ipd : ipds) {
             }
            }
            
            
            
            
            List<Constraint> constraints = interaction.getConstraints();
            
            if(constraints !=  null) {
                for(Constraint c:constraints) {
                System.out.println("Qualifier: " +  c.getQualifier());
                System.out.println("SourceID: " +  c.getSourceID());
                System.out.println("SourceType: " +  c.getSourceType());
                System.out.println("TargetID: " +  c.getTargetID());
                System.out.println("TargetType: " +  c.getTargetType());
                
             }
            }
            
            

        } catch (NoSuchMethodException ex) {
            Exceptions.printStackTrace(ex);
        }
          }
        sheet.put(set);
        return sheet;
      
    }
    
}
