/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.repositorysearch.ui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.JPanel;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.view.OutlineView;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import uk.ac.ncl.icos.synbad.repositorysearch.api.RepositorySearchResults;
import uk.ac.ncl.icos.synbad.repositorysearch.nodes.SearchNode;
import uk.ac.ncl.icos.synbad.ui.CellRenderer;

/**
 *
 * @author owengilfellon
 */
public class SearchResultsPanel extends JPanel implements ExplorerManager.Provider {
    
    private final OutlineView outlineview;
    private final ExplorerManager manager = new ExplorerManager();

    public SearchResultsPanel() {
        outlineview = new OutlineView("Name");
        outlineview.addPropertyColumn("type", "Type");
        //outlineview.addPropertyColumn("metaType", "MetaType");
        outlineview.addPropertyColumn("designMethod", "Design Method");
        outlineview.addPropertyColumn("organism", "Organism");
        //outlineview.addPropertyColumn("status", "Status");
        outlineview.getOutline().setDefaultRenderer(Node.Property.class, new CellRenderer());
        outlineview.getOutline().setRootVisible(false);
        
        GridBagLayout layout = new GridBagLayout();
        this.setLayout(layout);
        GridBagConstraints fill = new GridBagConstraints();
        fill.gridx= GridBagConstraints.REMAINDER;
        fill.gridy= GridBagConstraints.REMAINDER;
        fill.fill = GridBagConstraints.BOTH;     
        fill.weightx=1.0;
        fill.weighty=1.0;
        add(outlineview, fill);
    }
    
    public void setResults(RepositorySearchResults results)
    {
        manager.setRootContext(new SearchNode(results));
    }
    
    public void clearResults()
    {
        manager.setRootContext(new AbstractNode(Children.LEAF));
    }

    @Override
    public ExplorerManager getExplorerManager() {
        return manager;
    }
}
