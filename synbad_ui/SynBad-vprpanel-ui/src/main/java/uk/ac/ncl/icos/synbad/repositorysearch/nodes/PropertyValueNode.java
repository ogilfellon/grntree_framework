/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.repositorysearch.nodes;

import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;

/**
 *
 * @author owengilfellon
 */
public class PropertyValueNode extends AbstractNode {

    public PropertyValueNode(String value) {
        super(Children.LEAF);
        this.setName(value);
    }
}
