/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.moduleeditor.dnd;

import java.awt.datatransfer.DataFlavor;

/**
 *
 * @author owengilfellon
 */
public class EntityFlavor extends DataFlavor {
    
    public static final DataFlavor ENTITY_FLAVOR = new EntityFlavor();

    public EntityFlavor() {
         super(EntityFlavor.class, "EntityFalvor");
    }
    
}
