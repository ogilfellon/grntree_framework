/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.editor.core;

import uk.ac.ncl.icos.synbad.datamodel.visualisation.widgetaction.InterModuleMoveProvider;
import uk.ac.ncl.icos.synbad.datamodel.visualisation.widgetaction.ClickToFront;
import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.geom.RoundRectangle2D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.netbeans.api.visual.action.ActionFactory;
import org.netbeans.api.visual.action.MoveStrategy;
import org.netbeans.api.visual.action.WidgetAction;
import org.netbeans.api.visual.anchor.Anchor;
import org.netbeans.api.visual.anchor.AnchorFactory;
import org.netbeans.api.visual.layout.LayoutFactory;
import org.netbeans.api.visual.layout.SceneLayout;
import org.netbeans.api.visual.model.ObjectState;
import org.netbeans.api.visual.model.StateModel;
import org.netbeans.api.visual.widget.LayerWidget;
import org.netbeans.api.visual.widget.Widget;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;
import org.openide.util.lookup.ProxyLookup;
import uk.ac.ncl.icos.view.api.ViewCursor;
import uk.ac.ncl.icos.synbad.datamodel.visualisation.style.EntityStyle;
import uk.ac.ncl.icos.synbad.datamodel.visualisation.style.HierarchyStyles.ComponentStyle;
import uk.ac.ncl.icos.synbad.datamodel.visualisation.style.HierarchyStyles.InteractionStyle;
import uk.ac.ncl.icos.synbad.datamodel.visualisation.style.HierarchyStyles.ModuleStyle;
import uk.ac.ncl.icos.datamodel.workspace.impl.PortDirection;
import uk.ac.ncl.icos.datamodel.workspace.objects.SynBadObject;
import uk.ac.ncl.icos.datamodel.workspace.sbol.Component;
import uk.ac.ncl.icos.datamodel.workspace.sbol.Interaction;
import uk.ac.ncl.icos.datamodel.workspace.sbol.Module;
import uk.ac.ncl.icos.synbad.datadefinition.IDataDefinitionManager;
import uk.ac.ncl.icos.synbad.svpfragment.nodes.VSynBadObjectNode;
import uk.ac.ncl.icos.synbad.svpfragment.nodes.VPortNode;
import uk.ac.ncl.icos.model.entities.VNode;
import uk.ac.ncl.icos.model.entities.VPort;

/**
 *
 * @author owengilfellon
 */
public abstract class EntityWidget<T> extends Widget implements  StateModel.Listener, Lookup.Provider {

    private final IDataDefinitionManager defManager = Lookup.getDefault().lookup(IDataDefinitionManager.class);
    
    private final SynBadObject instance;
    private final VSynBadObjectNode entityNode;
    private Map<VPort, Anchor> externalInPorts;
    private Map<VPort, Anchor> externalOutPorts;
    private Map<VPort, Anchor> internalInPorts;
    private Map<VPort, Anchor> internalOutPorts;
    private StateModel minimisedState;
    private List<EntityStyle> styles = new ArrayList<>();
    
    private final Lookup lookup;
    
    private final AHierarchicalGraphScene scene;

    private final LayerWidget hierarchywindow;
    private final HeaderWidget header;
    private final Widget contents;
    private final Widget entitiesContainer;
    private final Widget inPinWidget;
    private final Widget outPinWidget;
    private final LayerWidget entityViews;

    private SceneLayout hierarchyLayout;


    public EntityWidget(AHierarchicalGraphScene scene, VSynBadObjectNode entity) {
        
        super(scene);
        
        this.scene = scene; 
        this.entityNode = entity;
        this.instance = entity.getLookup().lookup(SynBadObject.class);
        this.externalInPorts = new HashMap<>();
        this.externalOutPorts = new HashMap<>();
        this.internalInPorts = new HashMap<>();
        this.internalOutPorts = new HashMap<>();
        
        this.lookup = new ProxyLookup(Lookups.fixed(this, entity, instance));
        

        // ======================================
        // Hierarchy window
        // ======================================
        
        hierarchywindow = new LayerWidget(scene);
        header = new HeaderWidget(scene, instance);
        contents = new Widget(scene);
        inPinWidget = new Widget(scene);
        entityViews = new LayerWidget(scene);
        entitiesContainer = new Widget(scene);
        outPinWidget = new Widget(scene);

        hierarchyLayout = LayoutFactory.createDevolveWidgetLayout(entityViews, LayoutFactory.createHorizontalFlowLayout(LayoutFactory.SerialAlignment.JUSTIFY, 40), false);
        hierarchywindow.setLayout(LayoutFactory.createVerticalFlowLayout(LayoutFactory.SerialAlignment.JUSTIFY, 0));
        contents.setLayout(LayoutFactory.createHorizontalFlowLayout(LayoutFactory.SerialAlignment.JUSTIFY, 20));
        inPinWidget.setLayout(LayoutFactory.createVerticalFlowLayout(LayoutFactory.SerialAlignment.JUSTIFY, 10));
        outPinWidget.setLayout(LayoutFactory.createVerticalFlowLayout(LayoutFactory.SerialAlignment.JUSTIFY, 10));
        
        hierarchywindow.addChild(header);
        hierarchywindow.addChild(contents);
        contents.addChild(inPinWidget);
        contents.addChild(entitiesContainer);
        contents.addChild(outPinWidget);
        
        entitiesContainer.addChild(entityViews);
        addChild(hierarchywindow);

        
        // ======================================
        // State Model
        // ======================================

        minimisedState = new StateModel();
        minimisedState.setBooleanState(true);
        minimisedState.addListener(this);
        contents.setVisible(!minimisedState.getBooleanState());

        // ======================================
        // Actions
        // ======================================
        
        if(instance instanceof Module)
            addStyle(new ModuleStyle());
        else if(instance instanceof Component)
            addStyle(new ComponentStyle());
        else  if(instance instanceof Interaction)
            addStyle(new InteractionStyle());

        
        createActions("default");
        getActions().addAction(new ClickToFront());
        getActions("default").addAction(scene.createObjectHoverAction());
        getActions("default").addAction(scene.createSelectAction());
        
        if(instance instanceof Module) {
            header.getActions().addAction(new WidgetAction.Adapter(){
                @Override
                public WidgetAction.State mouseClicked(Widget widget, WidgetAction.WidgetMouseEvent event) {
                    if(event.getClickCount()==2) {
                        minimisedState.toggleBooleanState();
                        return WidgetAction.State.CONSUMED; 
                    } else {
                        return WidgetAction.State.REJECTED;
                    }
                }
            });
        }
        
        
        
        getActions().addAction(ActionFactory.createActionMapAction());
        getActions("default").addAction(ActionFactory.createResizeAction());
        getActions("default").addAction(ActionFactory.createMoveAction(new MoveStrategy() {

            
            @Override
            public Point locationSuggested(Widget widget, Point point, Point point1) {
                // Force revalidation of scene to update connectionwidgets
                EntityWidget.this.scene.revalidate();
                int x = (point1.x - getLeftOffset()) <= 1 ? 1+getLeftOffset(): point1.x;
                int y = (point1.y - getTopOffset()) <= 1 ? 1+getTopOffset() : point1.y;                
                return new Point(x, y);
            }}, ActionFactory.createDefaultMoveProvider()));
               /* */
        createActions("move");
        getActions("move").addAction(ActionFactory.createMoveAction(
                ActionFactory.createFreeMoveStrategy(),
                new InterModuleMoveProvider(scene, this)));

        applyStyle();
        initSetup();
    }

    public VSynBadObjectNode getEntityNode() {
        return entityNode;
    }

    public StateModel getMinimisedState() {
        return minimisedState;
    }

    public void initSetup() {
        hierarchyLayout.invokeLayout();
    }

    public boolean addStyle(EntityStyle style)
    {
       return styles.add(style);
    }
    
    public boolean removeStyle(EntityStyle style)
    {
       return styles.remove(style);
    }
    
    public void applyStyle()
    {
        for(EntityStyle style : styles) {
            style.apply(this);
        }
    }

    public HeaderWidget getHeaderWidget()
    {
        return header;
    }
    
    public Widget getHierarchyView() {
        return hierarchywindow;
    }

    public Widget addPin(VPortNode port) {
        
        VPort pInstance = port.getLookup().lookup(VPort.class);
        PinWidget pin = new PinWidget(scene, port);

        if(pInstance.getData().getValue().getPortDirection() == PortDirection.IN) {
            inPinWidget.addChild(pin); 
            externalInPorts.put(pInstance, pin.getExternalAnchor());
            internalInPorts.put(pInstance, pin.getInternalAnchor());
        } else {
            outPinWidget.addChild(pin);
            externalOutPorts.put(pInstance, pin.getExternalAnchor());
            internalOutPorts.put(pInstance, pin.getInternalAnchor());
        }

        return pin;
    }

    public Anchor getAnchor(VPort port, boolean internal) {
        
        if(internal) {
            if(port.getData().getValue().getPortDirection() == PortDirection.IN)
                return AnchorFactory.createProxyAnchor(minimisedState, internalInPorts.get(port), getInAnchor());
            else
                return AnchorFactory.createProxyAnchor(minimisedState, internalOutPorts.get(port), getOutAnchor());
        } else {
            if(port.getData().getValue().getPortDirection() == PortDirection.IN)
                return AnchorFactory.createProxyAnchor(minimisedState, externalInPorts.get(port), getInAnchor());
            else
                return AnchorFactory.createProxyAnchor(minimisedState, externalOutPorts.get(port), getOutAnchor());
        }
    }   
    
    public abstract void addWidget(T widget);

    public LayerWidget getEntitiesLayer() {
        return entityViews;
    }

    @Override
    public void stateChanged() {
        ViewCursor c = scene.getDataModelView().createCursor();
        c.moveTo(entityNode.getLookup().lookup(VNode.class));
        if(minimisedState.getBooleanState()) {
            //c.collapse();
        }
            
        else {
            // c.expand();
        }
           
        hierarchyLayout.invokeLayout();
        contents.setVisible(!minimisedState.getBooleanState());/**/
    }
    
    public Anchor getInAnchor() {
        return AnchorFactory.createDirectionalAnchor(this, AnchorFactory.DirectionalAnchorKind.HORIZONTAL);

    }
    
    public Anchor getOutAnchor() {
        return AnchorFactory.createDirectionalAnchor(this, AnchorFactory.DirectionalAnchorKind.HORIZONTAL);
    }

   
    @Override
    protected void notifyStateChanged(ObjectState previousState, ObjectState state) {
        super.notifyStateChanged(previousState, state); //To change body of generated methods, choose Tools | Templates.
        applyStyle();
    }
    
    protected int getTopOffset() {
        return getBorder().getInsets().top;
    }
    protected int getLeftOffset() {
        return getBorder().getInsets().left;
    }

    @Override
    protected void paintWidget() {
        super.paintWidget();
        Color gradientColor1 =  getBackground() instanceof Color ? (Color)getBackground() : null;
        Color gradientColor2 =  gradientColor1 != null ? new Color(gradientColor1.getRed(),
                gradientColor1.getGreen(),
                gradientColor1.getBlue(),
                gradientColor1.getAlpha() - 100) : null;
        
        if(gradientColor1 != null && gradientColor2 != null) {
            GradientPaint gradient = new GradientPaint(0,0, gradientColor1,0, (getBounds().height), gradientColor2);
            Graphics2D g2 = getGraphics();
            g2.setPaint(gradient);
            g2.fill(new RoundRectangle2D.Float(0, 0, getBounds().width, getBounds().height, 20, 20));
        }
    }

    @Override
    public Lookup getLookup() {
        return lookup;
    }
}
