/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.datamodel.visualisation.style;

import java.awt.Color;
import org.netbeans.api.visual.border.BorderFactory;
import org.netbeans.api.visual.model.ObjectState;
import uk.ac.ncl.icos.synbad.editor.core.EntityWidget;

/**
 *
 * @author owengilfellon
 */
public class HierarchyStyles {
    
    private static final Color component = new Color(0, 255, 255);
    private static final Color component_edge = new Color(100, 255, 255);
    private static final Color interaction = new Color(255, 0, 255);
    private static final Color interaction_edge = new Color(255, 100, 255);
    private static final Color module = new Color(0, 255, 0);
    private static final Color module_edge = new Color(100, 255, 100);
    private static final Color i_module = new Color(255, 255, 0);
    private static final Color i_module_edge = new Color(255, 255, 100);
    private static final Color highlight = Color.white;
    
    public static Color asTransparent(Color color, int transparency)
    {
        return new Color(color.getRed(), color.getGreen(), color.getBlue(), transparency);
    }
    
    public static class ComponentStyle implements EntityStyle{
        @Override
        public void apply(EntityWidget widget) {
            ObjectState state = widget.getState();
            widget.getHeaderWidget().setBorder(BorderFactory.createRoundedBorder(20, 20, 5, 5, asTransparent(component, 30), component_edge));
            if(state.isSelected()) {
                widget.setBackground(asTransparent(component, 100));
                widget.getHeaderWidget().setBackground(asTransparent(component, 200));
                widget.setForeground(highlight);
                widget.getHeaderWidget().setForeground(highlight);
                widget.setBorder(BorderFactory.createRoundedBorder(20, 20, 0, 0, asTransparent(component, 30), highlight));
            } else {
                widget.setBackground(asTransparent(component, 100));
                widget.getHeaderWidget().setBackground(asTransparent(component, 100));
                widget.setForeground(component_edge);
                widget.getHeaderWidget().setForeground(component_edge);
                widget.setBorder(BorderFactory.createRoundedBorder(20, 20, 0, 0, asTransparent(component, 30), component_edge));
                
            }
            
        }
    }
    
    public static class InteractionStyle implements EntityStyle {
        @Override
        public void apply(EntityWidget widget) {
            ObjectState state = widget.getState();
            widget.getHeaderWidget().setBorder(BorderFactory.createRoundedBorder(20, 20, 5, 5, asTransparent(interaction, 30), interaction_edge));
            if(state.isSelected()) {
                widget.setBackground(asTransparent(interaction, 100));
                widget.getHeaderWidget().setBackground(asTransparent(interaction, 200));
                widget.setForeground(highlight);
                widget.getHeaderWidget().setForeground(highlight);
                widget.setBorder(BorderFactory.createRoundedBorder(20, 20, 0, 0, asTransparent(interaction, 30), highlight));
            } else {
                widget.setBackground(asTransparent(interaction, 100));
                widget.getHeaderWidget().setBackground(asTransparent(interaction, 100));
                widget.setForeground(interaction_edge);
                widget.getHeaderWidget().setForeground(interaction_edge);
                widget.setBorder(BorderFactory.createRoundedBorder(20, 20, 0, 0, asTransparent(interaction, 30), interaction_edge));
                
            }
        }
    }
    
    public static class ModuleStyle implements EntityStyle {

     @Override
        public void apply(EntityWidget widget) {
            
            Color background = ///isInteractionModule ? i_module :
                    module;
            Color foreground = //isInteractionModule ? i_module_edge : 
                    module_edge;
            
            ObjectState state = widget.getState();
            widget.getHeaderWidget().setBorder(BorderFactory.createRoundedBorder(20, 20, 5, 5, asTransparent(background, 30), foreground));
            if(state.isSelected()) {
                widget.setBackground(asTransparent(background, 100));
                widget.getHeaderWidget().setBackground(asTransparent(background, 200));
                widget.setForeground(highlight);
                widget.getHeaderWidget().setForeground(highlight);
                 widget.setBorder(BorderFactory.createRoundedBorder(20, 20, 0, 0, asTransparent(background, 30), highlight));
               /* widget.setBorder(BorderFactory.createCompositeBorder(
                        BorderFactory.createRoundedBorder(20, 20, 0, 0, asTransparent(module, 30), module_edge),
                        BorderFactory.createOpaqueBorder(20, 20, 20, 20)));*/
                //widget.setBorder();
  
            } else {
                widget.setBackground(asTransparent(background, 100));
                widget.getHeaderWidget().setBackground(asTransparent(background, 100));
                widget.setForeground(foreground);
                widget.getHeaderWidget().setForeground(foreground);
                widget.setBorder(BorderFactory.createRoundedBorder(20, 20, 0, 0, asTransparent(background, 30), foreground));
                
            }
        }
    }

}
