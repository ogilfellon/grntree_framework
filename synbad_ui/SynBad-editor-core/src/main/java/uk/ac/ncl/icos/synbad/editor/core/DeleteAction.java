/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.editor.core;

import org.netbeans.api.visual.action.WidgetAction;
import org.netbeans.api.visual.widget.Widget;

/**
 *
 * @author owengilfellon
 */
public class DeleteAction extends WidgetAction.Adapter {

    @Override
    public State keyPressed(Widget widget, WidgetKeyEvent event) {
        /*if(event.getKeyCode() == KeyEvent.VK_BACK_SPACE) {
            GRNTreeNode node = (GRNTreeNode)((ObjectScene)widget.getScene()).findObject(widget);
            if(node != null) {
                if(node.isLeafNode()) {
                    ((InteractionsImpl)widget.getScene()).removePart((LeafNode) node);
                } else if (node.isBranchNode()) {
                    ((InteractionsImpl)widget.getScene()).removeGroup((BranchNode) node);
                }
            } 
        }*/
        
        return State.REJECTED;
    }
 
}
