/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.editor.core;

import java.awt.BasicStroke;
import java.awt.Color;
import org.netbeans.api.visual.widget.LayerWidget;
import org.netbeans.api.visual.widget.Scene;

/**
 *
 * @author owengilfellon
 */
public class GridWidget extends LayerWidget {
    
    int gridDistance = 20;
    int zoom = 1;
    int gridUnits = 10;

    public GridWidget(Scene scene) {
        super(scene);
        setOpaque(true);
        setVisible(true);
    }

    @Override
    protected void paintWidget() {
        super.paintWidget();//To change body of generated methods, choose Tools | Templates.

        if((gridDistance * zoom) * getScene().getZoomFactor() <  (gridDistance / 2) )
            zoom = zoom * 2;
        
        if((gridDistance * zoom) * getScene().getZoomFactor() >  (gridDistance * 2) && zoom > 1)
            zoom = zoom / 2;
        
        int zoomedDistance = gridDistance * zoom;

        getGraphics().setColor(Color.DARK_GRAY.brighter());
   
        int minX = getScene().getBounds().x;
        int maxX = minX + getScene().getBounds().width;
        int minY = getScene().getBounds().y;
        int maxY = minY + getScene().getBounds().height;
        
        getGraphics().setColor(Color.darkGray);
        getGraphics().fillRect(minX, minY, maxX, maxY);

        for(int x = 0; x < (maxX - minX); x += zoomedDistance) {

            for(int y = 0; y < (maxY - minY); y += zoomedDistance) {

                getGraphics().setStroke(new BasicStroke((float)(1.0 / getScene().getZoomFactor())));
                getGraphics().drawRect(x + minX, y + minY, zoomedDistance, zoomedDistance);

                if(y % (zoomedDistance * 10) == 0) {
                    getGraphics().setColor(new Color(Color.orange.getRed(), Color.orange.getGreen(), Color.orange.getBlue(), 100));
                    getGraphics().drawLine(x + minX, y + minY, x + maxX, y + minY);
                    getGraphics().setColor(Color.darkGray.brighter());
                }
            }
            
            if(x % (zoomedDistance * 10) == 0) {
                    getGraphics().setColor(new Color(Color.orange.getRed(), Color.orange.getGreen(), Color.orange.getBlue(), 100));
                    getGraphics().drawLine(x + minX, minY, x + minX, maxY);
                    getGraphics().setColor(Color.darkGray.brighter());
                }
        } 
    }
    
}
