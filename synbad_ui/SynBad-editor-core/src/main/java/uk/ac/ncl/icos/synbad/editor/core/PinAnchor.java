/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.editor.core;

import uk.ac.ncl.icos.synbad.editor.core.PinWidget;
import java.awt.Point;
import java.awt.Rectangle;
import org.netbeans.api.visual.anchor.Anchor;
import org.netbeans.api.visual.widget.Widget;
import uk.ac.ncl.icos.datamodel.workspace.impl.PortDirection;

/**
 *
 * @author owengilfellon
 */
public class PinAnchor extends Anchor {

    private Widget thisWidget;
    private int gap;
    private PortDirection direction;
    private boolean internal;

    public PinAnchor (Widget widget, int gap, PortDirection direction, boolean internal) {
        super (widget);
        this.thisWidget = widget;
        this.gap = gap;
        this.direction = direction;
        this.internal = internal;
    }

    @Override
    public Result compute (Entry entry) {

        PinWidget widget = (PinWidget)getRelatedWidget();

        if(widget.getBounds() != null) {
             
            Rectangle bounds = widget.convertLocalToScene (widget.getBounds());
            
            Point center = new Point (bounds.x + bounds.width / 2, bounds.y + bounds.height / 2);

            switch(direction) {
                case IN:
                    if(!internal)
                        return new Anchor.Result (new Point (bounds.x - gap, center.y), Direction.LEFT);
                    else
                        return new Anchor.Result (new Point (bounds.x + bounds.width + gap, center.y), Direction.RIGHT);
                case OUT:
                    if(!internal)
                        return new Anchor.Result (new Point (bounds.x + bounds.width + gap, center.y), Direction.RIGHT);
                    else
                        return new Anchor.Result (new Point (bounds.x - gap, center.y), Direction.LEFT);
            }
        }

        return null;
    }

}