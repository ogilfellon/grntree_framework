/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.moduleeditor.dnd;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import uk.ac.ncl.icos.datamodel.workspace.objects.SynBadObject;

/**
 *
 * @author owengilfellon
 */
public class EntityTransferable implements Transferable {

    SynBadObject entity = null;
    
    public EntityTransferable(SynBadObject entity) {
        this.entity = entity;
    }

    @Override
    public DataFlavor[] getTransferDataFlavors() {
        DataFlavor[] flavors = { EntityFlavor.ENTITY_FLAVOR };
        return flavors;
    }

    @Override
    public boolean isDataFlavorSupported(DataFlavor flavor) {
        return flavor == EntityFlavor.ENTITY_FLAVOR;
    }

    @Override
    public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException {
        return entity;
    }
    
}
