/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.editor.core;

import org.netbeans.api.visual.action.ActionFactory;
import org.netbeans.api.visual.graph.GraphPinScene;
import org.netbeans.api.visual.layout.LayoutFactory;
import org.netbeans.api.visual.widget.LayerWidget;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;
import org.openide.util.lookup.ProxyLookup;
import uk.ac.ncl.icos.synbad.h.graph.api.GraphListener;
import uk.ac.ncl.icos.synbad.svpfragment.nodes.VSynBadObjectNode;
import uk.ac.ncl.icos.synbad.svpfragment.nodes.VPortNode;
import uk.ac.ncl.icos.synbad.svpfragment.nodes.VWireNode;
import uk.ac.ncl.icos.view.api.SBHView;

/**
 *
 * @author owengilfellon
 */
public abstract class AHierarchicalGraphScene extends GraphPinScene<VSynBadObjectNode, VWireNode, VPortNode> implements GraphListener<VNode, VEdge> {

    private final GridWidget background = new GridWidget(this);
    private final LayerWidget entities = new LayerWidget(this);
    private final LayerWidget movement = new LayerWidget(this);
    private final LayerWidget connections = new LayerWidget(this);
    private final SBHView view;

    public AHierarchicalGraphScene( SBHView view ) {
        super();
        setLayout(LayoutFactory.createOverlayLayout());
        addChild(background);
        addChild(entities);
        addChild(connections);
        addChild(movement);
        getActions().addAction(ActionFactory.createWheelPanAction());
        getActions().addAction(ActionFactory.createMouseCenteredZoomAction(1.1));
        this.view = view;
    }

    @Override
    public Lookup getLookup() {
        return new ProxyLookup(super.getLookup(), Lookups.fixed(view));
    }

    public LayerWidget getMovementLayer(){
        return movement;
    }
    
    public LayerWidget getEntitiesLayer(){
        return entities;
    }
    
    public LayerWidget getConnectionsLayer(){
        return entities;
    }

    public SBHView getDataModelView() {
        return view;
    }

}
