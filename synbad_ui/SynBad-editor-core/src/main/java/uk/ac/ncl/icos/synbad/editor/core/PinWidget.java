/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.editor.core;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.geom.Ellipse2D;
import java.util.ArrayList;
import java.util.List;
import org.netbeans.api.visual.anchor.Anchor;
import org.netbeans.api.visual.layout.LayoutFactory;
import org.netbeans.api.visual.widget.LabelWidget;
import org.netbeans.api.visual.widget.Scene;
import org.netbeans.api.visual.widget.Widget;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;
import org.openide.util.lookup.ProxyLookup;
import org.sbolstandard.core2.Annotation;
import uk.ac.ncl.icos.synbad.datamodel.visualisation.style.PinStyle;
import uk.ac.ncl.icos.synbad.datamodel.visualisation.style.PinStyles;
import uk.ac.ncl.icos.datamodel.workspace.impl.PortDirection;
import uk.ac.ncl.icos.datamodel.workspace.impl.PortRecord;
import uk.ac.ncl.icos.datamodel.workspace.impl.PortType;
import uk.ac.ncl.icos.synbad.datadefinition.IDataDefinitionManager;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.svpfragment.nodes.VPortNode;
import uk.ac.ncl.icos.model.entities.VPort;

/**
 *
 * @author owengilfellon
 */
public class PinWidget extends Widget implements Lookup.Provider {

    private final IDataDefinitionManager defManager = Lookup.getDefault().lookup(IDataDefinitionManager.class);
    
    private final PortDirection direction;
    private boolean connected = false;
    private final PortRecord port;
    private final VPortNode portNode;
    private final List<PinStyle> styles = new ArrayList<>();
    private final LabelWidget label;
    private final PinHoleWidget pinHole;


    public PinWidget(Scene scene, VPortNode portNode) {
        super(scene);
        this.portNode = portNode;
        this.port = portNode.getLookup().lookup(VPort.class).getData();
        this.label = new LabelWidget(scene);
        this.label.setMinimumSize(new  Dimension(10, 15));

        /*
        for(Annotation a : port.getValue().getAnnotations(SynBadTerms.Port.portType)) {
           label.setLabel(defManager.getDefinition(PortType.class, a.getURIValue().toASCIIString()).toString());
        }*/
        
        this.direction = port.getValue().getPortDirection();
        pinHole = new PinHoleWidget(scene, direction);

        if(direction == PortDirection.IN) {
            setLayout(LayoutFactory.createHorizontalFlowLayout(LayoutFactory.SerialAlignment.LEFT_TOP, 5));
            addChild(pinHole);
            addChild(label);
        } else {
            setLayout(LayoutFactory.createHorizontalFlowLayout(LayoutFactory.SerialAlignment.RIGHT_BOTTOM, 5));
            addChild(label);
            addChild(pinHole);
        }
        addStyle(new PinStyles.DefaultPinStyle());
        applyStyles();
    }
    
    
    public Anchor getInternalAnchor() {
        Anchor a = new PinAnchor(this, 2, direction, true);
        return a;
    }
    
    public Anchor getExternalAnchor() {
       Anchor a = new PinAnchor(this, 2, direction, false);
       return a;
    }
    
    public boolean isConnected() {
        return connected;
    }
    
    public boolean addStyle(PinStyle style)
    {
       return styles.add(style);
    }
    
    public boolean removeStyle(PinStyle style)
    {
       return styles.remove(style);
    }
    
    public void setIsConnected(boolean connected) {
        this.connected = connected;
        applyStyles();
    }
    
    private void applyStyles() {
        for(PinStyle style : styles)
            style.applyStyle(this);
    }
    
    private class PinHoleWidget extends Widget {
        
        private boolean connected = false;
        private PortDirection direction;

        public PinHoleWidget(Scene scene, PortDirection direction) {
            super(scene);
            this.direction = direction;
            setPreferredSize(new Dimension(15, 15));
        }
        
        public boolean isConnected() {
            return connected;
        }
        
        public void setIsConnected(boolean connected) {
            this.connected = connected;
        }

        @Override
        protected void paintWidget() {
            super.paintWidget();
            getGraphics().setColor(new Color(0, 255, 0, 100));
           // getGraphics().fill(new Ellipse2D.Float(2, 2, 16, 16));
            getGraphics().setColor(Color.white);
            getGraphics().draw(new Ellipse2D.Float(0, 0, 15, 15));
            getGraphics().draw(new Ellipse2D.Float(2, 2, 11, 11));
        }

    }

    @Override
    public Lookup getLookup() {
        return new ProxyLookup(super.getLookup(), Lookups.fixed(port, portNode));
    }
    
    
    
}
