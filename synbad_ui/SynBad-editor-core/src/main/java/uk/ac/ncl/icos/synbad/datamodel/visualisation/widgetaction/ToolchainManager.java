/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.datamodel.visualisation.widgetaction;

import java.awt.event.KeyEvent;
import org.netbeans.api.visual.action.WidgetAction;
import org.netbeans.api.visual.widget.Widget;

/**
 *
 * @author owengilfellon
 */
public class ToolchainManager extends WidgetAction.Adapter {

    @Override
    public WidgetAction.State keyPressed(Widget widget, WidgetAction.WidgetKeyEvent event) {
        if (event.getKeyCode () == KeyEvent.VK_CONTROL) {
            widget.getScene ().setActiveTool ("move");
        }

        return WidgetAction.State.REJECTED;
    }

    @Override
    public WidgetAction.State keyReleased(Widget widget, WidgetAction.WidgetKeyEvent event) {
        if (event.getKeyCode () == KeyEvent.VK_CONTROL)
            widget.getScene ().setActiveTool ("default");
        return WidgetAction.State.REJECTED;
    }
}