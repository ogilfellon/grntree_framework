/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.moduleeditor.dnd;

import java.awt.Point;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import org.netbeans.api.visual.action.AcceptProvider;
import org.netbeans.api.visual.action.ConnectorState;
import org.netbeans.api.visual.widget.Widget;
import org.openide.util.Exceptions;
import uk.ac.ncl.icos.sort.ReturnableAction;
import uk.ac.ncl.icos.sort.WorkspaceAction;
import uk.ac.ncl.icos.datamodel.workspace.api.IWorkspace;
import uk.ac.ncl.icos.datamodel.workspace.objects.SynBadObject;
import uk.ac.ncl.icos.synbad.parsers.SVPParser;
import uk.ac.ncl.icos.synbad.svpfragment.dnd.PartFlavor;
import uk.ac.ncl.icos.synbad.svpfragment.nodes.VSynBadObjectNode;
import uk.ac.ncl.icos.synbad.svpfragment.nodes.VprPartNode;
import uk.ac.ncl.intbio.virtualparts.entity.Part;
import uk.ac.ncl.synbad.svp.Svp;

/**
 *
 * @author owengilfellon
 */

public class PartAcceptProvider implements AcceptProvider {
    
    private final IWorkspace workspace;
    
    public PartAcceptProvider(IWorkspace workspace)
    {
        this.workspace = workspace;
    }

    @Override
    public ConnectorState isAcceptable(Widget widget, Point point, Transferable transferable) {
        if(isPart(transferable))
        { return ConnectorState.ACCEPT; } else { return ConnectorState.REJECT; }
    }

    @Override
    public void accept(Widget widget, final Point point, Transferable transferable) {

        SynBadObject parent = widget.getLookup().lookup(SynBadObject.class);
        Part part = getPartFromTransferable(transferable);
        /*Svp child = new SVPParser(workspace).read(part);
        workspace.getCommander().perform(new WorkspaceAction.AddEntity(workspace, child));
        workspace.getCommander().perform(new ReturnableAction.CreateChild(parent, child));*/
    }

    private boolean isPart(Transferable transferable) {

        Part p = getPartFromTransferable(transferable);
        return p != null; 
    }
    
      private Part getPartFromTransferable(Transferable transferable) {

        Object o = null;
        try {
            o = transferable.getTransferData(PartFlavor.PART_FLAVOR);
        } catch (IOException | UnsupportedFlavorException ex) {
            System.out.println("DataFlavor not valid");
        }
        
        if(o instanceof VprPartNode) {
            try {
                return ((VprPartNode)o).getLookup().lookup(Part.class);
            } catch (Exception ex) {
                Exceptions.printStackTrace(ex);
                return null;
            }
        }
            
        else return null;

    }
      
      
}     
