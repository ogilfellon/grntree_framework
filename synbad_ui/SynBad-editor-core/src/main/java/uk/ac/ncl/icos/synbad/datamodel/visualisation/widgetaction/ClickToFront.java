/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.datamodel.visualisation.widgetaction;

import java.util.Stack;
import org.netbeans.api.visual.action.WidgetAction;
import org.netbeans.api.visual.widget.Widget;
import uk.ac.ncl.icos.synbad.editor.core.EntityWidget;

/**
 *
 * @author owengilfellon
 */
public class ClickToFront extends WidgetAction.Adapter {

    @Override
    public WidgetAction.State mouseClicked(Widget widget, WidgetAction.WidgetMouseEvent event) {

        Stack<Widget> widgetStack = new Stack();
        Widget w = widget;
        widgetStack.push(w);

        while(w.getParentWidget() != null) {
            if(w.getParentWidget() instanceof EntityWidget)
                widgetStack.push(w.getParentWidget());
            w = w.getParentWidget();
        }

        while(!widgetStack.empty()) {
            Widget i = widgetStack.pop();
            i.bringToFront();
        }

        return WidgetAction.State.CHAIN_ONLY;
    }
}