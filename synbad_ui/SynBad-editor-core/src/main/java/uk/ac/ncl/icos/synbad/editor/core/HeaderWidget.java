/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.editor.core;

import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics2D;
import java.awt.geom.RoundRectangle2D;
import org.netbeans.api.visual.layout.LayoutFactory;
import org.netbeans.api.visual.widget.Scene;
import org.netbeans.api.visual.widget.Widget;
import org.netbeans.api.visual.widget.general.IconNodeWidget;
import org.openide.util.ImageUtilities;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentRole;
import uk.ac.ncl.icos.synbad.datadefinition.types.EntityType;
import uk.ac.ncl.icos.synbad.datadefinition.types.ModuleRole;
import uk.ac.ncl.icos.datamodel.workspace.impl.EntityInstance;
import uk.ac.ncl.icos.datamodel.workspace.objects.SynBadObject;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms2;
import uk.ac.ncl.icos.synbad.datadefinition.types.Role;

/**
 *
 * @author owengilfellon
 */
public class HeaderWidget extends Widget {

    private IconNodeWidget label=null;
    
    //private final Widget minimiseWidget;
    
    private final SynBadObject instance;
    private boolean useInstance = false;
    private String name;

    public HeaderWidget(Scene scene, SynBadObject instance) {
        super(scene);
        this.instance = instance;
        setLayout(LayoutFactory.createHorizontalFlowLayout(LayoutFactory.SerialAlignment.LEFT_TOP, 10));
        setName();
    }
      
    
    private boolean hasRole(SynBadObject entity, Role role) {
        return entity.getValues(SynBadTerms2.Sbol.role).stream()
                .filter(v -> v.isURI())
                .anyMatch(v -> v.asURI().equals(role.getUri()));
    }


    
    private String getIcon(SynBadObject entity) {
        
        
        if(hasRole(entity, ComponentRole.Promoter)) {
            return "uk/ac/ncl/icos/synbad/project/nodeicons/promoter16.png"; 
        } else if (hasRole(entity, ComponentRole.Promoter)) {
            return "uk/ac/ncl/icos/synbad/project/nodeicons/operator16.png";
        } else if (hasRole(entity, ComponentRole.RBS)) {
            return "uk/ac/ncl/icos/synbad/project/nodeicons/rbs16.png";
        } else if (hasRole(entity, ComponentRole.CDS)) {
            return "uk/ac/ncl/icos/synbad/project/nodeicons/cds16.png";
        } else if (hasRole(entity, ComponentRole.Terminator)) {
            return "uk/ac/ncl/icos/synbad/project/nodeicons/terminator16.png";
        } else if (hasRole(entity, ComponentRole.Shim)) {
            return "uk/ac/ncl/icos/synbad/project/nodeicons/shim16.png";
        } 
        
        return "uk/ac/ncl/icos/synbad/project/nodeicons/userdefined16.png";
        
       

    }
    
    private void setName() {
        //IIdentified identified = useInstance ? instance : instance.getValue();
        
        SynBadObject identified = instance;
       
        
        if(stringNotEmpty(identified.getDisplayId()))
            this.name = "  " + identified.getDisplayId();
        else if(stringNotEmpty(identified.getName())) 
            this.name = "  " + identified.getName();
        
        if(label!=null)
            removeChild(label);
        
        label = new IconNodeWidget(getScene(), IconNodeWidget.TextOrientation.RIGHT_CENTER);
        label.setLabel(name);
        label.setImage(ImageUtilities.loadImage(getIcon(instance)));
        
        addChild(label);
    }
    
    private boolean stringNotEmpty(String string) {
        return string != null && !string.equals("");
    }

    public boolean isUsingInstance() {
        return useInstance;
    }
    
    public void setUseInstance(boolean useInstance) {
        setName();
    }

    @Override
    protected void paintWidget() {
        super.paintWidget(); //To change body of generated methods, choose Tools | Templates.
        
        Color gradientColor1 =  getBackground() instanceof Color ? (Color)getBackground() : null;
        Color gradientColor2 =  gradientColor1 != null ? new Color(gradientColor1.getRed(),
                gradientColor1.getGreen(),
                gradientColor1.getBlue(), 0) : null;
        
        if(gradientColor1 != null && gradientColor2 != null)
        {
            
            
            GradientPaint gradient = new GradientPaint(0 - getBorder().getInsets().left , 0-getBorder().getInsets().top, gradientColor1, getBounds().width, getBounds().height, gradientColor2);
            Graphics2D g2 = getGraphics();
            g2.setPaint(gradient);
            g2.fill(new RoundRectangle2D.Float(0 - getBorder().getInsets().left , 0-getBorder().getInsets().top, getBounds().width, getBounds().height, 20, 20));
            //g2.fill (new Rectangle(0, 0, getBounds().width, getBounds().height));
        }
        
    }
    
    
}
