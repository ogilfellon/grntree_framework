/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.datamodel.visualisation.style;

import uk.ac.ncl.icos.synbad.editor.core.PinWidget;


/**
 *
 * @author owengilfellon
 */
public interface PinStyle {
    
    public void applyStyle(PinWidget widget);
    
}
