/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.view.impl;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import uk.ac.ncl.icos.synbad.graph.api.SBGraph;
import uk.ac.ncl.icos.datamodel.graph.impl.DefaultGraph;
import uk.ac.ncl.icos.datamodel.graph.impl.DefaultCrawler;
import uk.ac.ncl.icos.datamodel.workspace.api.SBEntity;
import uk.ac.ncl.icos.synbad.graph.api.SBCrawler;
import uk.ac.ncl.icos.synbad.graph.api.SBCrawlerListener;
import uk.ac.ncl.icos.synbad.graph.api.SBCursor;
import uk.ac.ncl.icos.datamodel.workspace.objects.SynBadEdge;
import uk.ac.ncl.icos.view.api.SBModel;
import uk.ac.ncl.icos.model.entities.SBViewEdge;
import uk.ac.ncl.icos.model.entities.SBViewObject;
import uk.ac.ncl.icos.synbad.event.SBEvent;

/**
 *
 * @author owengilfellon
 */
public class DefaultInternalView extends AView {
    
    private final SBGraph<SBViewObject, SBViewEdge> graph;
    private final SBModel model;
    private final int viewId;
    private final SBViewObject rootNode;

    public DefaultInternalView(int viewId, SBModel model) {
        this.graph = new DefaultGraph<>();
        this.model = model;
        this.viewId = viewId;
        rootNode = addObject(model.getModelRoot());
        processModel(model.getModelRoot());
    }
    
    private void processModel(SBEntity startingNode) {
        SBCursor<SBEntity, SynBadEdge> cursor = model.getWorkspace().getCursor(startingNode);
        SBCrawler<SBEntity, SynBadEdge> crawler = new DefaultCrawler();
        crawler.addListener(new SBCrawlerListener.SBCrawlerListenerAdapter<SBEntity, SynBadEdge>() {

            @Override
            public void onInstance(SBEntity instance, SBCursor<SBEntity, SynBadEdge> cursor) {
                super.onInstance(instance, cursor);
                if(getViewNodes(instance).isEmpty())
                    addObject(instance);
            }

            @Override
            public void onEdge(SynBadEdge edge) {
                super.onEdge(edge);
                
                System.out.println("...."+edge);
                
                
                SBEntity toObj = model.getWorkspace().getObjectClasses(model.getWorkspace().getEdgeTarget(edge).getIdentity())
                    .stream().map(c -> model.getWorkspace().getObject(model.getWorkspace().getEdgeTarget(edge).getIdentity(), c)).
                        findFirst().orElse(null);
                
                SBEntity fromObj = model.getWorkspace().getObjectClasses(model.getWorkspace().getEdgeSource(edge).getIdentity())
                    .stream()
                        .map(c -> model.getWorkspace().getObject(model.getWorkspace().getEdgeSource(edge).getIdentity(), c))
                        .findFirst().orElse(null);
                
                if(fromObj!=null && toObj != null) {
                    
                    SBViewObject fromNode;
                    
                    if(!getViewNodes(fromObj).isEmpty() ) {
                        fromNode = getViewNodes(fromObj).iterator().next();
                    } else {
                        fromNode = addObject(fromObj);
                    }
                    
                    SBViewObject toNode;
                    
                    if(!getViewNodes(toObj).isEmpty() ) {
                        toNode = getViewNodes(toObj).iterator().next();
                    } else {
                        toNode = addObject(toObj);
                    }
                    
                    addEdge(edge, fromNode, toNode);
                    
                }
            }
        });
        crawler.run(cursor);
        
    }
    
    protected SBViewObject addObject(SBEntity obj) {
        SBViewObject n = new SBViewObject(obj, this);
        graph.addNode(n);
        registerNode(n);
        return n;     
    }
 
    protected SBViewEdge addEdge(SynBadEdge edge, SBViewObject from, SBViewObject to) {
        SBViewEdge vEdge = new SBViewEdge(from, to, edge, this);
        graph.addEdge(from, to, vEdge);
        registerEdge(vEdge);
        return vEdge;
    }

    @Override
    public int getViewId() {
        return viewId;
    }

    @Override
    public SBModel getModel() {
        return model;
    }

    @Override
    public SBCursor<SBViewObject, SBViewEdge> createCursor() {
        return new SBCursor.ASBCursor<>(rootNode, this);
    }

    @Override
    public List<SBViewEdge> incomingEdges(SBViewObject node) {
        return new ArrayList<>(graph.incomingEdges(node));
    }

    @Override
    public List<SBViewEdge> outgoingEdges(SBViewObject node) {
        return new ArrayList<>(graph.outgoingEdges(node));
    }
    
    

    @Override
    public List<SBViewEdge> incomingEdges(SBViewObject node, String predicate) {
        URI p = URI.create(predicate);
        return graph.incomingEdges(node).stream()
                .filter(e -> e.getEdge().getEdge().equals(p))
                .collect(Collectors.toList());
    }

    @Override
    public List<SBViewEdge> outgoingEdges(SBViewObject node, String predicate) {
        URI p = URI.create(predicate);
        return graph.outgoingEdges(node).stream()
                .filter(e -> e.getEdge().getEdge().equals(p))
                .collect(Collectors.toList());
    }

    @Override
    public SBViewObject getEdgeSource(SBViewEdge edge) {
        return graph.getEdgeSource(edge);
    }

    @Override
    public SBViewObject getEdgeTarget(SBViewEdge edge) {
        return graph.getEdgeTarget(edge);
    }
    
    

    @Override
    public SBViewObject getRoot() {
        return rootNode;
    }


    public void onEvent(SBEvent e) {
    
    }

}
