/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.model;

import uk.ac.ncl.icos.view.api.SBModel;
import uk.ac.ncl.icos.view.api.SBView;

/**
 *
 * @author owengilfellon
 */
public interface SBModelListener {

    public void onCreateView(SBModel model, SBView view);
    
    public void onRemoveView(SBModel model, SBView view);
    
    public void onSetCurrentView(SBModel model, SBView currentView);
    
}
