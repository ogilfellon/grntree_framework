package uk.ac.ncl.icos.view.api;

import java.util.Set;
import uk.ac.ncl.icos.datamodel.workspace.api.SBWorkspace;
import uk.ac.ncl.icos.datamodel.workspace.sbol.ModuleDefinition;
import uk.ac.ncl.icos.model.SBModelListener;
import uk.ac.ncl.icos.model.entities.SBViewObject;
import uk.ac.ncl.intbio.core.datatree.NamespaceBinding;

/**
 *
 * SynBad data model is based on SBOL2.0, and captures entities such as  modules,
 * components and interactions. Entities are created and instantiated using the
 * data model. Properties can be added to entities to capture additional information.
 *
 * @author owengilfellon
 */
public interface SBModel {

    public NamespaceBinding getNamespaceBinding();

    // Workspace
    
    public SBWorkspace getWorkspace();
 
    // Models may not have roots...
    
    public ModuleDefinition getModelRoot();
    
    // Views
    
    public SBView getDefaultView();

    public Set<SBView> getViews();

    public SBView createView(SBView view);
    
    public Set<SBHView> getHViews();
    
    public SBHView createHView(SBView view, SBViewObject root);
    
    public SBView removeView(SBView view);

    // Listeners
    
    public void addListener(SBModelListener listener);
    
    public void removeListener(SBModelListener listener);

}
