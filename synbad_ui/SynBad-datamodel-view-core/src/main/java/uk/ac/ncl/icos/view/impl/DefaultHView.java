/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.view.impl;

import uk.ac.ncl.icos.datamodel.graph.impl.DefaultHCursor;
import uk.ac.ncl.icos.synbad.h.graph.api.SBHIterator;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import uk.ac.ncl.icos.datamodel.graph.impl.DefaultHGraph;
import uk.ac.ncl.icos.synbad.h.graph.api.HGraphListener;
import uk.ac.ncl.icos.datamodel.graph.impl.DefaultCrawler;
import uk.ac.ncl.icos.view.api.SBModel;
import uk.ac.ncl.icos.model.entities.SBViewEdge;
import uk.ac.ncl.icos.model.entities.SBViewObject;
import uk.ac.ncl.icos.synbad.event.SBEvent;
import uk.ac.ncl.icos.synbad.event.SBSubscriber;
import uk.ac.ncl.icos.synbad.graph.api.SBCrawler;
import uk.ac.ncl.icos.synbad.graph.api.SBCursor;
import uk.ac.ncl.icos.synbad.h.graph.api.SBHCrawlerListener;
import uk.ac.ncl.icos.synbad.h.graph.api.SBHCursor;
import uk.ac.ncl.icos.synbad.h.graph.api.SBHGraph;
import uk.ac.ncl.icos.view.api.SBHView;
import uk.ac.ncl.icos.view.api.SBView;
import uk.ac.ncl.icos.view.rewrite.ViewRewriteRule;

/**
 *
 * @author owengilfellon
 */
public final class DefaultHView extends AView implements SBHView, SBSubscriber {

    private final SBView view;
    private final int viewId;
    private final SBViewObject rootNode;
    private final SBHGraph<SBViewObject, SBViewEdge> graph;
    private final List<ViewRewriteRule> rules;
    
    public DefaultHView(int viewId, SBView view) {
        this.graph = new DefaultHGraph<>(view.getRoot());
        this.view = view;
        this.viewId = viewId;
        this.rootNode = view.getRoot();
        this.rules = new ArrayList<>();
        this.rules.add(new ViewRewriteRule.MergeInstanceAndDefinitionRule());
        
        SBCursor<SBViewObject, SBViewEdge> c = view.createCursor();
        SBCrawler<SBViewObject, SBViewEdge> crawler = new DefaultCrawler<>();
        crawler.addListener(new SBHCrawlerListener.SBHCrawlerListenerAdapter<SBViewObject, SBViewEdge>() {

            @Override
            public void onEnded() {
                super.onEnded();
                for(ViewRewriteRule rule : rules) {
                    rule.apply(graph);
                }
            }

            @Override
            public void onProxyEdge(SBViewEdge edge) {
                super.onProxyEdge(edge);
                addObject(rootNode, view.getEdgeSource(edge));
                addObject(rootNode, view.getEdgeTarget(edge));
                addEdge(edge, view.getEdgeSource(edge), view.getEdgeTarget(edge));
            }

            
            
            @Override
            public void onEdge(SBViewEdge edge) { 
                super.onEdge(edge);
                addObject(rootNode, view.getEdgeSource(edge));
                addObject(rootNode, view.getEdgeTarget(edge));
                addEdge(edge, view.getEdgeSource(edge), view.getEdgeTarget(edge));

            }
        });
        /*
        this.addListener(new GraphListener<SBViewNode, SBViewEdge>() {

            @Override
            public void onEntityEvent(SBEventType type, SBViewNode instance, SBViewNode parent) {
                System.out.println(type + ": " + parent.getEdge().getDisplayId() + " *- " + instance.getEdge().getDisplayId());
            }

            @Override
            public void onEdgeEvent(SBEventType type, SBViewEdge edge) {
                System.out.println(type + ": " + edge.getEdge());
            }
        });*/
        
        
        crawler.run(c);
        
    }

    protected void addObject(SBViewObject parent, SBViewObject node) {

        if(parent == null)
            return;
        
        if(graph.containsNode(node))
            return;
        
        // is the node expanded or not?
        
        graph.addNode(node, parent);
        
        registerNode(node);
      
    }
    
    protected void removeObject(SBViewObject object) {
        
        if(!graph.containsNode(object))
            return;
        
        // is the node expanded or not?
        
        graph.removeNode(object);
        
        deregisterNode(object);
        
    }
    
    public boolean addEdge(SBViewEdge edge, SBViewObject from, SBViewObject to) {
        if(graph.addEdge(from, to, edge)) {
            registerEdge(edge);
            return true;
        }
        return false;
    }
    
    @Override
    public boolean removeEdge(SBViewEdge edge) {
        if(graph.removeEdge(edge)) {
            deregisterEdge(edge);
            return true;
        }
        return false;
    }

    @Override
    public int getViewId() {
        return viewId;
    }

    @Override
    public SBCursor<SBViewObject, SBViewEdge> createCursor() {
        return new SBCursor.ASBCursor<>( getRoot(), this);
    }

    @Override
    public SBHCursor<SBViewObject, SBViewEdge> createHCursor() {
        return new DefaultHCursor(rootNode, this);
    }

    @Override
    public List<SBViewEdge> incomingEdges(SBViewObject node) {
        return graph.incomingEdges(node).stream()
                .map(e -> new SBViewEdge(
                        graph.getEdgeSource(e),
                        graph.getEdgeTarget(e),
                        e.getEdge(),
                        this))
                .collect(Collectors.toList());
    }

    @Override
    public List<SBViewEdge> outgoingEdges(SBViewObject node) {
        return graph.outgoingEdges(node).stream()
                .map(e -> new SBViewEdge(
                        graph.getEdgeSource(e),
                        graph.getEdgeTarget(e),
                        e.getEdge(),
                        this))
                .collect(Collectors.toList());
    }

    @Override
    public List<SBViewEdge> incomingEdges(SBViewObject node, String predicate) {
        URI p = URI.create(predicate);
        return incomingEdges(node).stream()
                .filter(e -> e.getEdge().getEdge().equals(p))
                .collect(Collectors.toList());
    }

    @Override
    public List<SBViewEdge> outgoingEdges(SBViewObject node, String predicate) {
        URI p = URI.create(predicate);
        return outgoingEdges(node).stream()
                .filter(e -> e.getEdge().getEdge().equals(p))
                .collect(Collectors.toList());
    }

    @Override
    public SBViewObject getRoot() {
        return view.getRoot();
    }

    @Override
    public SBViewObject getEdgeSource(SBViewEdge edge) {
        return graph.getEdgeSource(edge);
    }

    @Override
    public SBViewObject getEdgeTarget(SBViewEdge edge) {
        return graph.getEdgeTarget(edge);
    }
    
        @Override
    public SBViewObject getOriginalEdgeSource(SBViewEdge edge) {
        return graph.getOriginalEdgeSource(edge);
    }

    @Override
    public SBViewObject getOriginalEdgeTarget(SBViewEdge edge) {
        return graph.getOriginalEdgeTarget(edge);
    }


    @Override
    public List<SBViewEdge> incomingProxyEdges(SBViewObject node) {
        return graph.incomingProxyEdges(node).stream().collect(Collectors.toList());
    }


    @Override
    public List<SBViewEdge> outgoingProxyEdges(SBViewObject node) {
        return graph.outgoingProxyEdges(node).stream().collect(Collectors.toList());
    }
    
    @Override
    public Set<SBViewEdge> proxyEdgeSet() {
        return graph.proxyEdgeSet();
    }


    @Override
    public void expand(SBViewObject node) {
        graph.expand(node);
    }

    @Override
    public void contract(SBViewObject node) {
        graph.contract(node);
    }

    @Override
    public boolean isContracted(SBViewObject node) {
        return graph.isContracted(node);
    }
    
    @Override
    public SBViewObject getParent(SBViewObject node) {
        return graph.getParent(node);
    }

    @Override
    public void setParent(SBViewObject parent, SBViewObject node) {
        graph.setParent(parent, node);
    }

    @Override
    public List<SBViewObject> getChildren(SBViewObject node) {
        return graph.getChildren(node);
    }

    @Override
    public int getDepth(SBViewObject node) {
        return graph.getDepth(node);
    }

    @Override
    public SBModel getModel() {
        return view.getModel();
    }

    @Override
    public void addListener(HGraphListener<SBViewObject, SBViewEdge> listener) {
        graph.addListener(listener);
    }

    @Override
    public void removeListener(HGraphListener<SBViewObject, SBViewEdge> listener) {
        graph.removeListener(listener); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public SBHIterator<SBViewObject> hierarchyIterator(SBViewObject instance) {
        return graph.hierarchyIterator(instance);
    }

    @Override
    public void onEvent(SBEvent e) {

    }


    @Override
    public List<SBViewObject> getDescendants(SBViewObject node) {
        return graph.getDescendants(node);
    }

    @Override
    public boolean isAncestor(SBViewObject node, SBViewObject ancestor) {
        return graph.isAncestor(node, ancestor);
    }

    @Override
    public boolean isDescendant(SBViewObject node, SBViewObject descendant) {
        return graph.isDescendant(node, descendant);
    }

    @Override
    public boolean isVisible(SBViewObject node) {
        return graph.isVisible(node);
    }

    @Override
    public boolean containsNode(SBViewObject node) {
        return graph.containsNode(node);
    }
    
    @Override
    public Set<SBViewEdge> edgeSet() {
        return graph.edgeSet();
    }

    @Override
    public boolean hasSiblings(SBViewObject node) {
        return graph.hasSiblings(node);
    }

    @Override
    public List<SBViewObject> getSiblings(SBViewObject node) {
        return graph.getSiblings(node);
    }

    @Override
    public boolean removeNode(SBViewObject node) {
        return graph.removeNode(node);
    }

    @Override
    public boolean removeAllNodes(Collection<? extends SBViewObject> nodes) {
        return graph.removeAllNodes(nodes);
    }

    @Override
    public boolean addNode(SBViewObject instance, SBViewObject parent) {
        return graph.addNode(instance, parent);
    }

    @Override
    public boolean addNode(SBViewObject instance, SBViewObject parent, int index) {
        return graph.addNode(instance, parent, index);
    }

    @Override
    public boolean containsEdge(SBViewEdge edge) {
        return graph.containsEdge(edge);
    }

    @Override
    public boolean containsEdge(SBViewObject from, SBViewObject to) {
        return graph.containsEdge(from, to);
    }

    @Override
    public boolean addEdge(SBViewObject from, SBViewObject to, SBViewEdge edge) {
        return graph.addEdge(from, to, edge);
    }

    @Override
    public boolean isProxyEdge(SBViewEdge edge) {
        return graph.isProxyEdge(edge);
    }



}
