/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.view.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import uk.ac.ncl.icos.synbad.datadefinition.UriHelper;
import uk.ac.ncl.icos.view.api.SBModel;
import uk.ac.ncl.icos.datamodel.workspace.api.SBWorkspace;
import uk.ac.ncl.icos.datamodel.workspace.sbol.ModuleDefinition;
import uk.ac.ncl.icos.view.api.SBView;
import uk.ac.ncl.icos.model.SBModelListener;
import uk.ac.ncl.icos.model.entities.SBViewObject;
import uk.ac.ncl.icos.view.api.SBHView;
import uk.ac.ncl.intbio.core.datatree.NamespaceBinding;

/**
 *
 * @author owengilfellon
 */
public class DefaultModel implements SBModel {
        
    private final NamespaceBinding       namespace;
    private final SBWorkspace          workspace;
    private final ModuleDefinition        rootNode;

    private Integer viewCount = 0;
    
    private       SBView currentView;
    private       SBView mainView;
    private final HashMap<Integer, SBView> allViews;
    
    private final List<SBModelListener> listeners;


    public DefaultModel(ModuleDefinition rootNode, SBWorkspace workspace) {
        this(rootNode, workspace, UriHelper.synbad);
    }
    
    private void registerView(int viewId, SBView view) {
        allViews.put(viewId, view);
    }
    
    private void deregisterView(int viewId) {
        allViews.remove(viewId);
    }

    public DefaultModel(ModuleDefinition root, SBWorkspace workspace, NamespaceBinding namespace) {
        this.namespace = namespace;
        this.workspace = workspace;
        this.allViews = new HashMap<>();
        this.rootNode = root;
        this.mainView = new DefaultInternalView(
                viewCount++, 
                this);
        registerView(mainView.getViewId(), mainView);
        this.currentView = mainView;
        this.listeners = new ArrayList<>();
    }

    @Override
    public NamespaceBinding getNamespaceBinding() {
        return namespace;
    }

    @Override
    public SBWorkspace getWorkspace() {
        return workspace;
    }

    // ===============================================
    // Listenable methods
    // ===============================================

    @Override
    public Set<SBView> getViews() {
        return new HashSet<>(allViews.values());
    }

    @Override
    public ModuleDefinition getModelRoot() {
        return rootNode;
    }

    @Override
    public SBView getDefaultView() {
        return mainView;
    }

    @Override
    public SBView createView(SBView view) {
        SBView v = new DefaultView(viewCount++, view);
        registerView(v.getViewId(), v);
        return v;
    }

    @Override
    public SBView removeView(SBView view) {
        if(currentView == view)
            currentView = mainView;

        deregisterView(view.getViewId());
        
        for(SBModelListener listener : listeners) {
            listener.onRemoveView(this, view);
        }
        
        return view;
    }

    @Override
    public void addListener(SBModelListener listener) {
        listeners.add(listener);
    }

    @Override
    public void removeListener(SBModelListener listener) {
        listeners.remove(listener);
    }

    @Override
    public Set<SBHView> getHViews() {
        return allViews.values().stream()
                .filter(v -> v instanceof SBHView)
                .map(v -> (SBHView)v).collect(Collectors.toSet());
    }

    @Override
    public SBHView createHView(SBView view, SBViewObject root) {
        SBHView v = new DefaultHView(viewCount++, view);
        registerView(v.getViewId(), v);
        return v;
    }
    
    
} 
