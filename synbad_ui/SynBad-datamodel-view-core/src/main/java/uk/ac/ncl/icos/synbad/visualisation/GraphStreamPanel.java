/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.visualisation;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import java.net.URI;
import javax.swing.JPanel;
import javax.swing.Timer;
import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.SingleGraph;
import org.graphstream.ui.swingViewer.DefaultView;
import org.graphstream.ui.swingViewer.basicRenderer.SwingBasicGraphRenderer;
import org.graphstream.ui.view.Viewer;
import org.graphstream.ui.view.ViewerListener;
import org.graphstream.ui.view.ViewerPipe;
import uk.ac.ncl.icos.synbad.h.graph.api.GraphListener;
import uk.ac.ncl.icos.synbad.graph.api.SBCursor;
import uk.ac.ncl.icos.datamodel.workspace.sbol.Component;
import uk.ac.ncl.icos.datamodel.workspace.sbol.FunctionalComponent;
import uk.ac.ncl.icos.datamodel.workspace.sbol.Interaction;
import uk.ac.ncl.icos.datamodel.workspace.sbol.Module;
import uk.ac.ncl.icos.datamodel.workspace.sbol.ComponentDefinition;
import uk.ac.ncl.icos.datamodel.workspace.sbol.ModuleDefinition;
import uk.ac.ncl.icos.model.entities.SBViewEdge;
import uk.ac.ncl.icos.model.entities.SBViewObject;
import uk.ac.ncl.icos.synbad.event.SBEventType;
import uk.ac.ncl.icos.synbad.h.graph.api.HGraphListener;
import uk.ac.ncl.icos.synbad.h.graph.api.SBHCrawler;
import uk.ac.ncl.icos.synbad.h.graph.api.SBHCrawlerListener;
import uk.ac.ncl.icos.view.api.SBHView;

/**
 *
 * @author owengilfellon
 */
public class GraphStreamPanel extends JPanel implements ViewerListener, HGraphListener<SBViewObject, SBViewEdge> {

    private final ViewerPipe fromViewer;
    private final Viewer gsViewer;
    private final DefaultView gsView;
    private final Graph gsGraph;
    private final SBHView view;
    private boolean loop = true;
    /**
     * Creates new form HierarchicalGraphPanel
     */
    public GraphStreamPanel(SBHCrawler<SBViewObject, SBViewEdge> crawler, SBHView v) {
        System.setProperty("gs.ui.renderer", "org.graphstream.ui.j2dviewer.J2DGraphRenderer");
        
        initComponents();
        this.view = v;
        
        // Create graph
        
        gsGraph = new SingleGraph("Visualisation");
        
        // Style graph
        
        gsGraph.addAttribute("ui.stylesheet", 
                "graph {            fill-color: black; } " + 
                "node {             text-color: rgb(0, 120, 0); } "+
                "node.sbobject {    fill-color: rgb(0, 150, 0);  } " +
                "node:clicked {     fill-color: rgb(255, 255, 255);} "+
                "node.definition {  size: 20px; }" +
                "node.module {      fill-color: rgb(0, 255, 0); }" +
                "node.component {   fill-color: rgb(0, 0, 255); }" +
                "node.interaction { fill-color: rgb(255, 0, 255);  }" + 
                "edge {             text-color: rgb(0, 80, 0); }");
        
        // Populate graph
        
        addEntity(v.getRoot());
        crawler.addListener(new SBHCrawlerListener.SBHCrawlerListenerAdapter<SBViewObject, SBViewEdge>() {

            @Override
            public void onProxyEdge(SBViewEdge edge) {
                super.onEdge(edge);
                addEdge(edge);
            }

            @Override
            public void onEdge(SBViewEdge edge) {
                super.onEdge(edge);
                addEdge(edge);
            }

        });
        crawler.run(v.createHCursor());
        v.addListener(this);
        
        // Construct Views and Viewer
        
        gsViewer = new Viewer(gsGraph, Viewer.ThreadingModel.GRAPH_IN_ANOTHER_THREAD);
        gsViewer.setCloseFramePolicy(Viewer.CloseFramePolicy.HIDE_ONLY);
        gsViewer.enableAutoLayout();
        
        fromViewer = gsViewer.newViewerPipe();
        fromViewer.addViewerListener(this);
        //fromViewer.addSink(gsGraph);
        
        gsView = new DefaultView(gsViewer, TOOL_TIP_TEXT_KEY, new SwingBasicGraphRenderer());

        // View preferences
        
        gsView.setPreferredSize(new Dimension(400, 400));
        gsView.getCamera().setAutoFitView(true);
        //view.getCamera().setViewCenter(1, 0, 1);
        gsView.getCamera().setViewPercent(7.0);

        // Add and fill panel
        
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.FIRST_LINE_START;
        c.fill = GridBagConstraints.BOTH;
        c.gridx = 0; 
        c.gridy = 0;
        c.weightx = 1;
        c.weighty = 1;
        add(gsView, c);

        gsView.setVisible(true);

    }
    
    // ========= Instantiation of GraphStream objects ===============

    public void addEntity(SBViewObject instance) {

        String id = instance.getData().getIdentity().toASCIIString();
        if(gsGraph.getNode(id) != null) 
            return;

        Node n = gsGraph.addNode(id);
        if(instance.getData() instanceof ModuleDefinition) {
            n.addAttribute("ui.class", "moduledefinition, module, definition, sbobject");
        } else if ( instance.getData() instanceof Module ) {
            n.addAttribute("ui.class", "module, instance, sbobject");
        } else if (instance.getData() instanceof ComponentDefinition) {
            n.addAttribute("ui.class", "component, componentdefinition, definition, sbobject");
        } else if (instance.getData() instanceof Component) {
            n.addAttribute("ui.class", "component, instance, sbobject");
        } else if (instance.getData() instanceof FunctionalComponent ) {
            n.addAttribute("ui.class", "component, instance, functionalcomponent, sbobject");
        } else if(instance.getData() instanceof Interaction ) {
           n.addAttribute("ui.class", "interaction, sbobject");
        } else {
            n.addAttribute("ui.class", "sbobject ");
        }

        n.addAttribute("ui.label", instance.getData().getName());
        
    }
    
    public void removeEntity(SBViewObject instance) {
        if(gsGraph.getNode(instance.getData().getIdentity().toASCIIString()) != null) 
            gsGraph.removeNode(instance.getData().getIdentity().toASCIIString());
    }

    public void addEdge(SBViewEdge edge) {

        String from = view.getEdgeSource(edge).getData().getIdentity().toASCIIString();
        String to = view.getEdgeTarget(edge).getData().getIdentity().toASCIIString();

        addEntity(view.getEdgeSource(edge));
        addEntity(view.getEdgeTarget(edge));
                
        
            if( gsGraph.getEdge(from + "_" + to) == null) {
                Edge e = gsGraph.addEdge(from + "_" + to, from, to);
                e.addAttribute("ui.style", "fill-color: rgb(100, 100, 100); ");
                e.addAttribute("ui.label", edge.getEdge().getEdge().getFragment());
            }
    }
    
    public void removeEdge(SBViewEdge edge) {
        String from = view.getEdgeSource(edge).getData().getIdentity().toASCIIString();
        String to = view.getEdgeTarget(edge).getData().getIdentity().toASCIIString();

            if( gsGraph.getEdge(from + "_" + to) != null)
                gsGraph.removeEdge(from + "_" + to);
    }
    
    // =========== View events =============
    

    @Override
    public void onEntityEvent(SBEventType type, SBViewObject instance, SBViewObject parent) {
        if(type == SBEventType.ADDED)
            addEntity(instance);
        else if(type == SBEventType.REMOVED)
            removeEntity(instance);
    }

    @Override
    public void onEdgeEvent(SBEventType type, SBViewEdge edge) {
        if(type == SBEventType.ADDED)
            addEdge(edge);
        else if(type == SBEventType.REMOVED)
            removeEdge(edge);
    }

    @Override
    public void onProxyEdgeEvent(SBEventType type, SBViewEdge edge) {
        if(type == SBEventType.ADDED) {
            addEdge(edge);
        } else if(type == SBEventType.REMOVED) {
            removeEdge(edge);
        }
    }
    
    
   
    
    // =========== User input =============
  
    @Override
    public void viewClosed(String id) {
        loop = false;
    }

    @Override
    public void buttonPushed(String id) {
        view.findNodes(URI.create(id)).stream().forEach((SBViewObject n) -> {
                if(view.isContracted(n))
                    view.expand(n);
                else
                    view.contract(n);
        });
    }

    @Override
    public void buttonReleased(String id) {
        
        
        // if contracted, expand
        // else if expanded, contract
    }
    
    public void run() {
        
        
        int delay = 50;
    
        new Timer(delay, (ActionEvent evt) -> { 
            repaint(); 
            fromViewer.pump();
        }).start();
        
    }
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setLayout(new java.awt.GridBagLayout());
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables



}
