/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.model.entities;

import java.util.Collection;
import java.util.Objects;
import java.util.Set;
import uk.ac.ncl.icos.datamodel.workspace.api.SBEntity;
import uk.ac.ncl.icos.synbad.graph.api.SBObject;
import uk.ac.ncl.icos.synbad.graph.api.SBValue;
import uk.ac.ncl.icos.datamodel.workspace.objects.SynBadObject;
import uk.ac.ncl.icos.view.api.SBView;


/**
 *
 * @author owengilfellon
 */
public class SBViewObject implements SBObject, SBViewEntity<SBEntity> {
  
    private final SBEntity obj;
    private final SBView view;

    public SBViewObject(SBEntity obj, SBView view) {
        this.obj = obj;
        this.view = view;
    }
    
    public SBEntity getData() {
        return obj;
    }

    @Override
    public String toString() {
        return obj.getDisplayId();
    }

    @Override
    public SBView getView() {
        return view;
    }


    @Override
    public SBValue getValue(String predicate) {
        return obj.getValue(predicate);
    }

    @Override
    public Collection<SBValue> getValues(String predicate) {
        return obj.getValues(predicate);
    }
    
    
    @Override
    public <T> Collection<T> getValues(String predicate, Class<T> clazz) {
        return obj.getValues(predicate, clazz);
    }

    @Override
    public Set<String> getPredicates() {
        return obj.getPredicates();
    }
    
   

    @Override
    public boolean equals(Object obj) {
        if(obj == null)
             return false;
        if(obj == this)
             return true;
        if(!(obj instanceof SBViewObject))
             return false;

        SBViewObject node = (SBViewObject) obj;
        return node.getData().getIdentity().equals(this.getData().getIdentity());
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 53 * hash + Objects.hashCode(this.obj);
        return hash;
    }

    
    

}
