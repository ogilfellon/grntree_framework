/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.model.entities;

import java.util.Objects;
import uk.ac.ncl.icos.synbad.graph.api.SBEdge;
import uk.ac.ncl.icos.datamodel.workspace.objects.SynBadEdge;
import uk.ac.ncl.icos.datamodel.workspace.objects.SynBadObject;
import uk.ac.ncl.icos.view.api.SBView;

/**
 *
 * @author owengilfellon
 */
public class SBViewEdge implements SBViewEntity<SynBadEdge>, SBEdge {
    
    private final SynBadEdge edge;
    private final SBViewObject from;
    private final SBViewObject to;
    private final SBView view;

    public SBViewEdge(SBViewObject from, SBViewObject to, SynBadEdge edge, SBView view) {
        this.edge = edge;
        this.from = from;
        this.to = to;
        this.view = view;
    }


    @Override
    public SynBadEdge getEdge() {
        return edge;
    }

    @Override
    public String toString() {
        return edge.getEdge().toASCIIString() + " { " + from + " " + to + " }";
    }

    @Override
    public SBView getView() {
        return view;
    }
    
    
    @Override
    public boolean equals(Object obj) {
        if(obj == null)
             return false;
        if(obj == this)
             return true;
        if(!(obj instanceof SBViewEdge))
             return false;

        SBViewEdge edge = (SBViewEdge) obj;
        return 
                edge.getEdge().getEdge().equals(getEdge().getEdge());
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.edge.getEdge());
        hash = 89 * hash + Objects.hashCode(this.from);
        hash = 89 * hash + Objects.hashCode(this.to);
        return hash;
    }

   
    
    
}
