/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.view.crawlerlisteners;

import uk.ac.ncl.icos.synbad.graph.api.SBCursor;
import uk.ac.ncl.icos.view.api.SBViewCrawlerListener;
import uk.ac.ncl.icos.model.entities.SBViewEdge;
import uk.ac.ncl.icos.model.entities.SBViewObject;

/**
 *
 * @author owengilfellon
 */
public class CrawlerDebugger implements SBViewCrawlerListener {

    String s = "";

    @Override
    public void onEdge(SBViewEdge edge) {
        System.out.println(s + "   " + edge.getEdge().getEdge().toASCIIString());
    }

    @Override
    public void onEnded() {

    };

    @Override
    public void onInstance(SBViewObject instance, SBCursor<SBViewObject, SBViewEdge> cursor) {
        System.out.println(instance.getData().getDisplayId());
    }

    @Override
    public boolean terminate() { 
        return false; }

    @Override
    public boolean vetoEdge(SBViewEdge edge) {
       return false;
    }

}
