/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.model.entities;

import uk.ac.ncl.icos.view.api.SBView;

/**
 *
 * @author owengilfellon
 */
public interface SBViewEntity<T>  {
    
    public SBView getView();
    
}
