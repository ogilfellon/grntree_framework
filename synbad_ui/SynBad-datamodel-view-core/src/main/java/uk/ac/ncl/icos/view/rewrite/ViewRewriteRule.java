/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.view.rewrite;

import java.util.Set;
import java.util.stream.Collectors;
import uk.ac.ncl.icos.datamodel.graph.impl.DefaultHGraph;
import uk.ac.ncl.icos.model.entities.SBViewEdge;
import uk.ac.ncl.icos.model.entities.SBViewObject;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms2;
import uk.ac.ncl.icos.synbad.graph.api.SBEdge;
import uk.ac.ncl.icos.synbad.graph.api.SBObject;
import uk.ac.ncl.icos.synbad.h.graph.api.SBHGraph;

/**
 *
 * @author owengilfellon
 */
public interface ViewRewriteRule<N extends SBObject, E extends SBEdge> {
    
    public void apply(SBHGraph<N, E> g);
    
    public static class MergeInstanceAndDefinitionRule implements ViewRewriteRule<SBViewObject, SBViewEdge> {

        @Override
        public void apply(SBHGraph<SBViewObject, SBViewEdge> g) {
            Set<SBViewEdge> edgeSet = g.edgeSet().stream()
                    .filter(e -> e.getEdge().getEdge().toASCIIString()
                                .equals(SynBadTerms2.Sbol.definedBy) ||
                            e.getEdge().getEdge().toASCIIString()
                                .equals("http://www.synbad.org/definition"))
                    .collect(Collectors.toSet());
            for(SBViewEdge edge: edgeSet) {
                SBViewObject from = g.getEdgeSource(edge);
                SBViewObject to = g.getEdgeTarget(edge);
                g.setParent(from, to);
                g.contract(from);
            };
        }
    }    
}
