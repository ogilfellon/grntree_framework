/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.view.api;

import java.net.URI;
import java.util.Set;
import uk.ac.ncl.icos.model.entities.SBViewEdge;
import uk.ac.ncl.icos.model.entities.SBViewObject;
import uk.ac.ncl.icos.synbad.h.graph.api.GraphListener;
import uk.ac.ncl.icos.synbad.graph.api.SBEdgeProvider;
import uk.ac.ncl.icos.synbad.graph.api.SBCursor;

/**
 * Wraps around a graph instance, allowing read only access to the graph...
 * @author owengilfellon
 */
public interface SBView extends SBEdgeProvider<SBViewObject, SBViewEdge> {
    
    public SBModel getModel();
    
    public int getViewId();
    
    
    // Cursor
    
    public SBCursor<SBViewObject, SBViewEdge> createCursor();


    // Hierarchy and grouping

    public SBViewObject getRoot();
    
    public Set<SBViewObject> findNodes(URI identity);
    
    public Set<SBViewEdge> findEdges(URI predicate);
    
    // Listeners
    
    public void addListener(GraphListener<SBViewObject, SBViewEdge> listener);

    public void removeListener(GraphListener<SBViewObject, SBViewEdge> listener);

}
