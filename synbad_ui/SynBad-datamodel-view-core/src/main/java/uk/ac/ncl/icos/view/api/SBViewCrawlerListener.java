package uk.ac.ncl.icos.view.api;

import uk.ac.ncl.icos.synbad.graph.api.SBCrawlerListener;
import uk.ac.ncl.icos.model.entities.SBViewEdge;
import uk.ac.ncl.icos.model.entities.SBViewObject;

/**
 *
 * @author owengilfellon
 */
public interface SBViewCrawlerListener extends SBCrawlerListener<SBViewObject, SBViewEdge> {


    
}
