/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.view.impl;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import uk.ac.ncl.icos.datamodel.workspace.api.SBEntity;
import uk.ac.ncl.icos.synbad.h.graph.api.GraphListener;
import uk.ac.ncl.icos.datamodel.workspace.objects.SynBadEdge;
import uk.ac.ncl.icos.view.api.SBView;
import uk.ac.ncl.icos.model.entities.SBViewEdge;
import uk.ac.ncl.icos.model.entities.SBViewObject;

/**
 *
 * @author owengilfellon
 */
public abstract class AView implements SBView {

    private final List<GraphListener<SBViewObject, SBViewEdge>> listeners;
    private final Map<URI, Set<SBViewObject>> instanceNodes;
    private final Map<URI, Set<SBViewEdge>> instanceEdges;

    public AView() {
        this.instanceNodes = new HashMap<>();
        this.instanceEdges = new HashMap<>();
        this.listeners = new ArrayList<>();
    }
    
    public AView(Predicate<? super SBViewEdge> edgeFilter) {
        this.instanceNodes = new HashMap<>();
        this.instanceEdges = new HashMap<>();
        this.listeners = new ArrayList<>();
    }
    
    protected void registerNode(SBViewObject instance) {

        URI instanceId = instance.getData().getIdentity();
        
        if(!instanceNodes.containsKey(instanceId))
            instanceNodes.put(instanceId, new HashSet<>());
        
        instanceNodes.get(instanceId).add(instance);
       
        /*((EntityInstance)instance.getData()).getValue().getWorkspace().getDispatcher()
            .addFilter(((EntityInstance)instance.getData()).getValue().getIdentity(), this);   */
    }

    
    protected void deregisterNode(SBViewObject instance) {

        URI instanceId = instance.getData().getIdentity();
        
        instanceNodes.get(instanceId).remove(instance);
        
        if(instanceNodes.get(instanceId).isEmpty())
            instanceNodes.remove(instanceId);

        /*
        ((EntityInstance)instance.getData()).getValue().getWorkspace().getDispatcher()
            .removeFilter(((EntityInstance)instance.getData()).getValue().getIdentity(), this);*/
    }
    
    protected void registerEdge(SBViewEdge instance) {

        if(!instanceEdges.containsKey(instance.getEdge().getEdge())) {
            instanceEdges.put(instance.getEdge().getEdge(), new HashSet<>());
        };
        
        instanceEdges.get(instance.getEdge().getEdge()).add(instance);
        
        
        /*((EntityInstance)instance.getData()).getValue().getWorkspace().getDispatcher()
            .addFilter(((EntityInstance)instance.getData()).getValue().getIdentity(), this);   */
    }

    
    protected void deregisterEdge(SBViewEdge instance) {

        instanceEdges.get(instance.getEdge().getEdge()).remove(instance);
        
        if(instanceEdges.get(instance.getEdge().getEdge()).isEmpty())
            instanceEdges.remove(instance.getEdge().getEdge());

        /*
        ((EntityInstance)instance.getData()).getValue().getWorkspace().getDispatcher()
            .removeFilter(((EntityInstance)instance.getData()).getValue().getIdentity(), this);*/
    }
    
    @Override
    public Set<SBViewObject> findNodes(URI identity) {
        Set<SBViewObject> nodes = instanceNodes.get(identity);
        return nodes == null ? new HashSet<>() : nodes;
    }
    
    @Override
    public Set<SBViewEdge> findEdges(URI predicate) {
        Set<SBViewEdge> edges = instanceEdges.get(predicate);
        return edges == null ? new HashSet<>() : edges;
    }

    protected Set<SBViewObject> getViewNodes(SBEntity instance) {
        Set<SBViewObject> nodes = instanceNodes.get(instance.getIdentity());
        return nodes == null ? new HashSet<>() : nodes;
    }
    
    protected Set<SBViewEdge> getViewEdges(SynBadEdge edge) {
        
        
        Set<SBViewEdge> edges = instanceEdges.get(edge.getEdge()).stream()
                .filter(e -> getEdgeSource(e).getData().getIdentity().equals(getModel().getWorkspace().getEdgeSource(edge).getIdentity()) &&
                        getEdgeTarget(e).getData().getIdentity().equals(getModel().getWorkspace().getEdgeTarget(edge).getIdentity()))
                .collect(Collectors.toSet());
        return edges == null ? new HashSet<>() : edges;
    }

    public List<GraphListener<SBViewObject, SBViewEdge>> getListeners() {
        return listeners;
    }

    @Override
    public void addListener(GraphListener<SBViewObject, SBViewEdge> listener) {
        listeners.add(listener);
    }

    @Override
    public void removeListener(GraphListener<SBViewObject, SBViewEdge> listener) {
        listeners.remove(listener);
    }

}
