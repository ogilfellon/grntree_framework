/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.view.crawlerlisteners;

import uk.ac.ncl.icos.synbad.graph.api.SBCrawlerListener;
import uk.ac.ncl.icos.synbad.graph.api.SBCursor;
import uk.ac.ncl.icos.model.entities.SBViewEdge;
import uk.ac.ncl.icos.model.entities.SBViewObject;
import uk.ac.ncl.icos.synbad.h.graph.api.SBHCursor;

/**
 *
 * @author owengilfellon
 */
public class IteratorRule extends SBCrawlerListener.SBCrawlerListenerAdapter<SBViewObject, SBViewEdge> {

    @Override
    public void onInstance(SBViewObject instance, SBCursor<SBViewObject, SBViewEdge> cursor) {
        super.onInstance(instance, cursor); //To change body of generated methods, choose Tools | Templates.
        if(cursor instanceof SBHCursor)
            ((SBHCursor)cursor).expand(); 
    }
    
}
