/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.view.rewrite;

import uk.ac.ncl.icos.synbad.graph.api.SBGraph;
import uk.ac.ncl.icos.datamodel.graph.impl.DefaultGraph;

/**
 *
 * @author owengilfellon
 */
public interface NodeRewriteRule {
    
    public void getRemovePattern();
    public void getAddPattern();
    
    public class InstanceRewriteRule implements NodeRewriteRule{

        private static SBGraph pattern = new DefaultGraph();
        
        @Override
        public void getRemovePattern() {
    
            /*
                if(node has definition)
                    remove node, definition and edge
                    (store all dangling edges)
            */
        }

        @Override
        public void getAddPattern() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
        
    }
    
}
