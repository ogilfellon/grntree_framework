/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.view.impl;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;
import uk.ac.ncl.icos.datamodel.graph.impl.DefaultCrawler;
import uk.ac.ncl.icos.datamodel.graph.impl.DefaultGraph;
import uk.ac.ncl.icos.view.api.SBModel;
import uk.ac.ncl.icos.model.entities.SBViewEdge;
import uk.ac.ncl.icos.model.entities.SBViewObject;
import uk.ac.ncl.icos.synbad.event.SBEvent;
import uk.ac.ncl.icos.synbad.event.SBSubscriber;
import uk.ac.ncl.icos.synbad.graph.api.SBCrawler;
import uk.ac.ncl.icos.synbad.graph.api.SBCrawlerListener;
import uk.ac.ncl.icos.synbad.graph.api.SBCursor;
import uk.ac.ncl.icos.synbad.graph.api.SBGraph;
import uk.ac.ncl.icos.view.api.SBView;

/**
 *
 * @author owengilfellon
 */
public final class DefaultView extends AView implements SBView, SBSubscriber {

    private final SBView view;
    private final int viewId;
    private final SBGraph<SBViewObject, SBViewEdge> graph;
    
    public DefaultView(int viewId, SBView view) {
        this.graph = new DefaultGraph<>();
        this.view = view;
        this.viewId = viewId;
        SBCursor<SBViewObject, SBViewEdge> c = view.createCursor();
        SBCrawler<SBViewObject, SBViewEdge> crawler = new DefaultCrawler<>();
        crawler.addListener(new SBCrawlerListener.SBCrawlerListenerAdapter<SBViewObject, SBViewEdge>() {
            @Override
            public void onEdge(SBViewEdge edge) { 
                super.onEdge(edge);
                addObject(view.getEdgeSource(edge));
                addObject(view.getEdgeTarget(edge));
                addEdge(edge, view.getEdgeSource(edge), view.getEdgeTarget(edge));
            }
        });
        
        crawler.run(c);
        
    }

    protected void addObject(SBViewObject node) {

        if(graph.containsNode(node))
            return;
        
        // is the node expanded or not?
        
        graph.addNode(node);
        
        registerNode(node);
      
    }
    
    protected void removeObject(SBViewObject object) {
        
        if(!graph.containsNode(object))
            return;
        
        // is the node expanded or not?
        
        graph.removeNode(object);
        
        deregisterNode(object);
        
    }
    
    public boolean addEdge(SBViewEdge edge, SBViewObject from, SBViewObject to) {
        if(graph.addEdge(from, to, edge)) {
            registerEdge(edge);
            return true;
        }
        return false;
    }
    
  
    public boolean removeEdge(SBViewEdge edge) {
        if(graph.removeEdge(edge)) {
            deregisterEdge(edge);
            return true;
        }
        return false;
    }

    @Override
    public int getViewId() {
        return viewId;
    }

    @Override
    public SBCursor<SBViewObject, SBViewEdge> createCursor() {
        return new SBCursor.ASBCursor<>( getRoot(), this);
    }


    @Override
    public List<SBViewEdge> incomingEdges(SBViewObject node) {
        return graph.incomingEdges(node).stream()
                .map(e -> new SBViewEdge(
                        graph.getEdgeSource(e),
                        graph.getEdgeTarget(e),
                        e.getEdge(),
                        this))
                .collect(Collectors.toList());
    }

    @Override
    public List<SBViewEdge> outgoingEdges(SBViewObject node) {
        return graph.outgoingEdges(node).stream()
                .map(e -> new SBViewEdge(
                        graph.getEdgeSource(e),
                        graph.getEdgeTarget(e),
                        e.getEdge(),
                        this))
                .collect(Collectors.toList());
    }

    @Override
    public List<SBViewEdge> incomingEdges(SBViewObject node, String predicate) {
        URI p = URI.create(predicate);
        return incomingEdges(node).stream()
                .filter(e -> e.getEdge().getEdge().equals(p))
                .collect(Collectors.toList());
    }

    @Override
    public List<SBViewEdge> outgoingEdges(SBViewObject node, String predicate) {
        URI p = URI.create(predicate);
        return outgoingEdges(node).stream()
                .filter(e -> e.getEdge().getEdge().equals(p))
                .collect(Collectors.toList());
    }

    @Override
    public SBViewObject getRoot() {
        return view.getRoot();
    }

    @Override
    public SBViewObject getEdgeSource(SBViewEdge edge) {
        return graph.getEdgeSource(edge);
    }

    @Override
    public SBViewObject getEdgeTarget(SBViewEdge edge) {
        return graph.getEdgeTarget(edge);
    }

    @Override
    public SBModel getModel() {
        return view.getModel();
    }

    @Override
    public void onEvent(SBEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
 


}
