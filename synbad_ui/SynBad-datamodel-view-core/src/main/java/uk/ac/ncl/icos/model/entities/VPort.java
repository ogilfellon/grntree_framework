/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.model.entities;

import uk.ac.ncl.icos.view.api.SBModel;
import uk.ac.ncl.icos.datamodel.workspace.impl.PortRecord;
import uk.ac.ncl.icos.view.api.SBView;

/**
 *
 * @author owengilfellon
 */

public class VPort extends SBViewObject {

    public VPort(PortRecord instance, SBView view) {
        super(instance, view);
    }

    @Override
    public PortRecord getData() {
        return (PortRecord)super.getData();
    }

}
