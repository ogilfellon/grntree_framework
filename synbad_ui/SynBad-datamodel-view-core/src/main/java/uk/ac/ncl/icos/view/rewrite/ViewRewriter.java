/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.view.rewrite;

import java.util.ArrayList;
import java.util.List;
import uk.ac.ncl.icos.synbad.event.SBEventType;
import uk.ac.ncl.icos.datamodel.graph.impl.DefaultHGraph;
import uk.ac.ncl.icos.synbad.h.graph.api.GraphListener;
import uk.ac.ncl.icos.model.entities.SBViewEdge;
import uk.ac.ncl.icos.model.entities.SBViewObject;
import uk.ac.ncl.icos.synbad.h.graph.api.SBHGraph;

/**
 *
 * @author owengilfellon
 */
public class ViewRewriter<N extends SBViewObject, E extends SBViewEdge> implements GraphListener<N, E> {

    private final SBHGraph<N, E> graph;
    private final List<ViewRewriteRule<N, E>> rules;
    
    public ViewRewriter(SBHGraph<N, E> graph) {
        this.graph = graph;
        this.rules = new ArrayList<>();
    }
    
    public void apply() {
        for(ViewRewriteRule<N, E> rule : rules) {
            rule.apply(graph);
        }
    }
    
    
    // Listeners for interpreting additional changes to the graph

    @Override
    public void onEntityEvent(SBEventType type, N instance, N parent) {
        
    }

    @Override
    public void onEdgeEvent(SBEventType type, E edge) {
    
    }
    
}
