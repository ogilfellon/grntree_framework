/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.view.api;


import uk.ac.ncl.icos.synbad.h.graph.api.GraphListener;
import uk.ac.ncl.icos.synbad.h.graph.api.HGraphListener;
import uk.ac.ncl.icos.model.entities.SBViewEdge;
import uk.ac.ncl.icos.model.entities.SBViewObject;
import uk.ac.ncl.icos.synbad.h.graph.api.SBHGraph;
import uk.ac.ncl.icos.synbad.h.graph.api.SBHCursor;

/**
 * Wraps around a graph instance, allowing read only access to the graph...
 * @author owengilfellon
 */
public interface SBHView extends SBView, SBHGraph<SBViewObject, SBViewEdge> {
    
    public SBHCursor<SBViewObject, SBViewEdge> createHCursor();

    @Override
    public void addListener(HGraphListener<SBViewObject, SBViewEdge> listener);

    @Override
    public void removeListener(HGraphListener<SBViewObject, SBViewEdge> listener);
}
