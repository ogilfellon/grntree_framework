/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.visualisation;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.io.File;
import java.net.URI;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import javax.swing.JFrame;
import javax.swing.Timer;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.netbeans.junit.MockServices;
import org.openide.util.Lookup;
import uk.ac.ncl.icos.synbad.h.graph.api.GraphListener;
import uk.ac.ncl.icos.synbad.h.graph.api.HGraphListener;
import uk.ac.ncl.icos.datamodel.workspace.api.SBWorkspace;
import uk.ac.ncl.icos.datamodel.workspace.api.SBWorkspaceManager;
import uk.ac.ncl.icos.datamodel.workspace.api.rdf.DefaultWorkspaceManager;
import uk.ac.ncl.icos.datamodel.workspace.sbol.SbolFactory;
import uk.ac.ncl.icos.datamodel.workspace.sbol.ModuleDefinition;
import uk.ac.ncl.icos.view.api.SBModel;
import uk.ac.ncl.icos.model.entities.SBViewEdge;
import uk.ac.ncl.icos.model.entities.SBViewObject;
import uk.ac.ncl.icos.synbad.datadefinition.UriHelper;
import uk.ac.ncl.icos.synbad.event.SBEventType;
import uk.ac.ncl.icos.synbad.h.graph.api.SBHCrawler;
import uk.ac.ncl.icos.view.impl.DefaultHView;
import uk.ac.ncl.icos.view.api.SBHView;
import uk.ac.ncl.icos.datamodel.graph.impl.DefaultHCrawler;
import uk.ac.ncl.icos.view.impl.DefaultModel;

/**
 *
 * @author owengilfellon
 */
public class SBOLGraphTest {
    
    private final URI workspaceURI = UriHelper.synbad.namespacedUri("testWorkspace");
    private final SBWorkspaceManager m;
    private SBWorkspace ws;
    
    public SBOLGraphTest() {
        MockServices.setServices(DefaultWorkspaceManager.class,
            SbolFactory.ComponentDefinitionFactory.class,
            SbolFactory.ModuleDefinitionFactory.class,
            SbolFactory.FunctionalComponentFactory.class,
            SbolFactory.InteractionFactory.class,
            SbolFactory.ModuleFactory.class,
            SbolFactory.ParticipationFactory.class,
            SbolFactory.ComponentFactory.class,
            SbolFactory.CutFactory.class,
            SbolFactory.MapsToFactory.class,
            SbolFactory.RangeFactory.class,
            SbolFactory.SequenceAnnotationFactory.class,
            SbolFactory.SequenceConstraintFactory.class,
            SbolFactory.SequenceFactory.class);
        this.m = Lookup.getDefault().lookup(SBWorkspaceManager.class);
        setupWorkspace();
        
    }
    
    private void setupWorkspace() {
        
        ws = m.getWorkspace(workspaceURI);

        Set<String> sources = new HashSet<>(Arrays.asList(
            "ExampleSystem.xml",
            "subtilinReceiverPliaG1.xml",
            "SubtilinReceiver.xml"
        ));

        sources.stream().forEach((source) -> {
            ws.addRdf(new File(source));
        });
    }
  
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
     @After
    public void tearDown() {
        m.closeWorkspace(workspaceURI);
    }
    
  
    
    public static void main(String[] args) {
        SBOLGraphTest test = new SBOLGraphTest();
        test.testGraphVisualisation();
    }

    /**
     * Test of addInstance method, of class RDFGraphStreamPanel.
     */
   @Test
    public void testGraphVisualisation() {
        JFrame frame = new JFrame("Visualisation");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(new GridBagLayout());
        
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.FIRST_LINE_START;
        c.fill = GridBagConstraints.BOTH;
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 1;
        c.weighty = 1;

        ModuleDefinition def = ws.getObject(URI.create("http://www.synbad.org/System/1.0"), ModuleDefinition.class);
        //ModuleDefinition def = ws.getObject(URI.create("http://www.synbad.org/md/Node1947975572/1.0"), ModuleDefinition.class);
        //ModuleDefinition def = ws.getObject(URI.create("http://www.synbad.org/md/SpaR_PspaS/1.0"), ModuleDefinition.class);

        System.out.println("Processing Model...");
        
       // SBModel model = new DefaultModel(def, ws);
        
        // Do again... this should use cached objects and be quicker...
        
        System.out.println("Processing Model Again...");
        
        SBModel model2 = new DefaultModel(def, ws);
        
        SBHCrawler<SBViewObject, SBViewEdge> crawler = new DefaultHCrawler();
        SBHView view = new DefaultHView(91223123, model2.getDefaultView());
        view.addListener(new HGraphListener<SBViewObject, SBViewEdge>() {

            @Override
            public void onEntityEvent(SBEventType type, SBViewObject instance, SBViewObject parent) {
                System.out.println(type + ": " + instance + " to " + parent);
            }

            @Override
            public void onEdgeEvent(SBEventType type, SBViewEdge edge) {
                System.out.println(type + ": " + edge);
            }

            @Override
            public void onProxyEdgeEvent(SBEventType type, SBViewEdge edge) {
                System.out.println(type + ": " + edge);
            }
            
            
        });
        view.expand(view.getRoot());
       
        GraphStreamPanel panel = new GraphStreamPanel(crawler, view);
        frame.getContentPane().add(panel, c);
        frame.pack();
        frame.setVisible(true);
        panel.run();
    }
}
