/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.visualisation;

import com.google.common.collect.Sets;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import uk.ac.ncl.icos.datamodel.workspace.api.SBWorkspaceManager;
import uk.ac.ncl.icos.datamodel.workspace.api.SBWorkspace;
import java.net.URI;
import javax.swing.JFrame;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.netbeans.junit.MockServices;
import org.openide.util.Lookup;
import uk.ac.ncl.icos.datamodel.graph.impl.DefaultHCrawler;
import uk.ac.ncl.icos.datamodel.workspace.api.rdf.DefaultWorkspaceManager;
import uk.ac.ncl.icos.datamodel.workspace.objects.Identity;
import uk.ac.ncl.icos.datamodel.workspace.sbol.Component;
import uk.ac.ncl.icos.datamodel.workspace.sbol.ComponentDefinition;
import uk.ac.ncl.icos.datamodel.workspace.sbol.FunctionalComponent;
import uk.ac.ncl.icos.datamodel.workspace.sbol.Location;
import uk.ac.ncl.icos.datamodel.workspace.sbol.ModuleDefinition;
import uk.ac.ncl.icos.datamodel.workspace.sbol.SbolFactory;
import uk.ac.ncl.icos.datamodel.workspace.sbol.Sequence;
import uk.ac.ncl.icos.datamodel.workspace.sbol.SequenceAnnotation;
import uk.ac.ncl.icos.datamodel.workspace.sbol.SequenceConstraint;
import uk.ac.ncl.icos.model.entities.SBViewEdge;
import uk.ac.ncl.icos.model.entities.SBViewObject;
import uk.ac.ncl.icos.synbad.datadefinition.UriHelper;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentType;
import uk.ac.ncl.icos.synbad.event.SBEventType;
import uk.ac.ncl.icos.synbad.h.graph.api.HGraphListener;
import uk.ac.ncl.icos.synbad.h.graph.api.SBHCrawler;
import uk.ac.ncl.icos.view.api.SBHView;
import uk.ac.ncl.icos.view.api.SBModel;
import uk.ac.ncl.icos.view.impl.DefaultHView;
import uk.ac.ncl.icos.view.impl.DefaultModel;

/**
 *
 * @author owengilfellon
 */
public class CommandsGraph  {
    
    private final URI workspaceURI = UriHelper.synbad.namespacedUri("testCommands");
    private final SBWorkspaceManager m;
    private SBWorkspace ws;
    
    public CommandsGraph() {
         MockServices.setServices(DefaultWorkspaceManager.class,
            SbolFactory.ComponentDefinitionFactory.class,
            SbolFactory.ModuleDefinitionFactory.class,
            SbolFactory.FunctionalComponentFactory.class,
            SbolFactory.InteractionFactory.class,
            SbolFactory.ModuleFactory.class,
            SbolFactory.ParticipationFactory.class,
            SbolFactory.ComponentFactory.class,
            SbolFactory.CutFactory.class,
            SbolFactory.MapsToFactory.class,
            SbolFactory.RangeFactory.class,
            SbolFactory.SequenceAnnotationFactory.class,
            SbolFactory.SequenceConstraintFactory.class,
            SbolFactory.SequenceFactory.class);
        this.m = Lookup.getDefault().lookup(SBWorkspaceManager.class);
        setupWorkspace();
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
        m.closeWorkspace(workspaceURI);
    }
    
    
    private void setupWorkspace() {

        ws = m.getWorkspace(workspaceURI);
        
        // Create identity for module and component, instantiate Object creation
        // actions and issue to workspace.
        
        Identity moduleIdentity =  new Identity("http://www.synbad.org", "rootModule", "1.0");
        ws.perform(new ModuleDefinition.CreateModuleDefinitionAction(moduleIdentity));
        
        Identity compDef1Identity = new Identity("http://www.synbad.org", "componentDefinition", "1.0");
        ws.perform(new ComponentDefinition.CreateComponentDefinitionAction(compDef1Identity, Sets.newHashSet(ComponentType.DNA)));
       
        Identity compDef2Identity = new Identity("http://www.synbad.org", "componentDefinition2", "1.0");
        ws.perform(new ComponentDefinition.CreateComponentDefinitionAction(compDef2Identity, Sets.newHashSet(ComponentType.DNA)));
       
        Identity compDef3Identity = new Identity("http://www.synbad.org", "componentDefinition3", "1.0");
        ws.perform(new ComponentDefinition.CreateComponentDefinitionAction(compDef3Identity, Sets.newHashSet(ComponentType.DNA)));
     
        // Retrieve created objects
        
        ModuleDefinition mod = ws.getObject(moduleIdentity.getIdentity(), ModuleDefinition.class);
        mod.setName("A");
        mod.setDescription("The root module definition");
        
        ComponentDefinition comDef1 = ws.getObject(compDef1Identity.getIdentity(), ComponentDefinition.class);
        comDef1.setName("B");
        comDef1.setDescription("A component definition that contains two child components");
        
        ComponentDefinition comDef2 = ws.getObject(compDef2Identity.getIdentity(), ComponentDefinition.class);
        comDef2.setName("C");
        comDef2.setDescription("A component definition");
        
        ComponentDefinition comDef3 = ws.getObject(compDef3Identity.getIdentity(), ComponentDefinition.class);
        comDef3.setName("D");
        comDef3.setDescription("A component definition");

 
        assert(comDef1!=null);
        assert(comDef2!=null);
        assert(comDef3!=null);

        
        Identity sequence1Identity = new Identity("http://www.synbad.org", "sequence1", "1.0");
        ws.perform(new Sequence.CreateSequenceAction(sequence1Identity, "AAAAAAAAAAAAAAAA", "http://www.chem.qmul.ac.uk/iubmb/misc/naseq.html"));
        
        Identity sequence2Identity = new Identity("http://www.synbad.org", "sequence2", "1.0");
        ws.perform(new Sequence.CreateSequenceAction(sequence2Identity, "TTTTTTTTTTTTTTTT", "http://www.chem.qmul.ac.uk/iubmb/misc/naseq.html"));
        
        Sequence seq1 = ws.getObject(sequence1Identity.getIdentity(), Sequence.class);
        Sequence seq2 = ws.getObject(sequence2Identity.getIdentity(), Sequence.class);
        
        assert(seq1 != null);
        assert(seq2 != null);
        
        ws.perform(new ComponentDefinition.AddSequenceAction(comDef2.getIdentity(), seq1.getIdentity())); 
        ws.perform(new ComponentDefinition.AddSequenceAction(comDef3.getIdentity(), seq2.getIdentity()));
        
        // Instantiate ComponentDefinition as Components in ComponentDefinition
        
        
        Identity compInstanceIdentity = new Identity("http://www.synbad.org", "B_instance", "1.0");
        ws.perform(new ModuleDefinition.CreateFuncComponentAction(compInstanceIdentity, comDef1, mod));

        Identity compInstance1Identity = new Identity("http://www.synbad.org", "C_instance", "1.0");
        ws.perform(new ComponentDefinition.CreateComponentAction(compInstance1Identity, comDef1, comDef2));

        Identity compInstance2Identity = new Identity("http://www.synbad.org", "D_instance", "1.0");
        ws.perform(new ComponentDefinition.CreateComponentAction(compInstance2Identity, comDef1, comDef3));

        
        FunctionalComponent fc = ws.getObject(compInstanceIdentity.getIdentity(),FunctionalComponent.class);
        fc.setDescription("Instance of B in ModuleDefinition A");
        
        Component comp1 = ws.getObject(compInstance1Identity.getIdentity(),Component.class);
        comp1.setDescription("Instance of C in ComponentDefinition B");
        
        Component comp2 = ws.getObject(compInstance2Identity.getIdentity(),Component.class);
        comp2.setDescription("Instance of D in ComponentDefinition B");
        
        assert(fc != null);
        assert(comp1!=null);
        assert(comp2!=null);
        
        Identity location1Identity = new Identity("http://www.synbad.org", "componentInstance1_Location", "1.0");
        ws.perform(new Location.CreateRangeAction(location1Identity, 1, 100));
        
        Identity location2Identity = new Identity("http://www.synbad.org", "componentInstance2_Location", "1.0");
        ws.perform(new Location.CreateCutAction(location2Identity, 70));
        
        Location location1 = ws.getObject(location1Identity.getIdentity(), Location.class);
        Location location2 = ws.getObject(location2Identity.getIdentity(), Location.class);
        
        assert (location1 != null);
        assert (location2 != null);
        
        Identity annotation1Identity = new Identity("http://www.synbad.org", "componentInstance1_Annotation", "1.0");
        ws.perform(new ComponentDefinition.CreateSequenceAnnotationAction(annotation1Identity, comDef1, comp1, location1));
        Identity annotation2Identity = new Identity("http://www.synbad.org", "componentInstance2_Annotation", "1.0");
        ws.perform(new ComponentDefinition.CreateSequenceAnnotationAction(annotation2Identity, comDef1, comp2, location2));
        
        SequenceAnnotation ann1 = ws.getObject(annotation1Identity.getIdentity(), SequenceAnnotation.class);
        SequenceAnnotation ann2 = ws.getObject(annotation2Identity.getIdentity(), SequenceAnnotation.class);
        
        Identity constraintIdentity = new Identity("http://www.synbad.org", "componentInstance1_2Constraint", "1.0");
        ws.perform(new ComponentDefinition.CreateSequenceConstraintAction(constraintIdentity, comDef1, comp1, comp2, URI.create("http://sbols.org/v2#precedes")));
        SequenceConstraint c = ws.getObject(constraintIdentity.getIdentity(), SequenceConstraint.class);
        c.setDescription("Constraint:- C precedes D in B");

        assert(c!=null);

        //ws.getDispatcher().subscribe(SBEvent.class, new SBEventFilter.DefaultFilter(), this);
    }

        
    public static void main(String[] args) {
        CommandsGraph test = new CommandsGraph();
        test.testGraphVisualisation();
    }

    /**
     * Test of addInstance method, of class RDFGraphStreamPanel.
     */
   @Test
    public void testGraphVisualisation() {
        JFrame frame = new JFrame("Visualisation");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(new GridBagLayout());
        
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.FIRST_LINE_START;
        c.fill = GridBagConstraints.BOTH;
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 1;
        c.weighty = 1;

        ModuleDefinition def = ws.getObject(URI.create("http://www.synbad.org/rootModule/1.0"), ModuleDefinition.class);
        SBModel model2 = new DefaultModel(def, ws);
        SBHCrawler<SBViewObject, SBViewEdge> crawler = new DefaultHCrawler();
        SBHView view = new DefaultHView(91223123, model2.getDefaultView());
        view.addListener(new HGraphListener<SBViewObject, SBViewEdge>() {

            @Override
            public void onEntityEvent(SBEventType type, SBViewObject instance, SBViewObject parent) {
                System.out.println(type + ": " + instance + " to " + parent);
            }

            @Override
            public void onEdgeEvent(SBEventType type, SBViewEdge edge) {
                System.out.println(type + ": " + edge);
            }

            @Override
            public void onProxyEdgeEvent(SBEventType type, SBViewEdge edge) {
                System.out.println(type + ": " + edge);
            }
        });
        view.expand(view.getRoot());
        GraphStreamPanel panel = new GraphStreamPanel(crawler, view);
        frame.getContentPane().add(panel, c);
        frame.pack();
        frame.setVisible(true);
        panel.run();
    }

}
