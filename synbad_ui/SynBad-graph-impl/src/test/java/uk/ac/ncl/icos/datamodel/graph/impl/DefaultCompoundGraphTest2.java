/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.datamodel.graph.impl;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;

/**
 *
 * @author owengilfellon
 */
public class DefaultCompoundGraphTest2 {
    
    public DefaultCompoundGraphTest2() {
    }
    
       
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    /*
    
    private void printTree(DefaultCompoundGraph<String, String> graph) {
        
        SBHIterator<String> i = graph.hierarchyIterator("root");
        StringBuilder sb = new StringBuilder();
        while(i.hasNext()) {
            
            
            String s = i.next();
           
            sb.append(i.getDepth());
            for(int n = 0; n < i.getDepth(); n++) {
                sb.append("\t");
                
            }
            sb.append(s);
            sb.append(" E{ ");
            graph.incomingEdges(s).stream().forEach(e ->
                sb.append(e));
            
            sb.append(" | ");
            graph.outgoingEdges(s).stream().forEach(e ->
                sb.append(e));
            sb.append(" }\n");
        }
        
        System.out.println(sb.toString());        
    }
    
    public void populateGraph(DefaultCompoundGraph<String, String> graph) {
         
        graph.addNode("1", "root");
            graph.addNode("1.1", "1");
            graph.addNode("1.2", "1");
                graph.addNode("1.2.1", "1.2");
                graph.addNode("1.2.2", "1.2");
            graph.addNode("1.3", "1");
            graph.addNode("1.4", "1");
        graph.addNode("2", "root");
            graph.addNode("2.1", "2");
            graph.addNode("2.2", "2");
            graph.addNode("2.3", "2");
            
        graph.addEdge("1.1", "1.2", "1.1 -> 1.2");
        graph.addEdge("1.2.1", "1.1", "1.2.1 -> 1.1");
    }

    
    @Test
    public void testisAncestor() {
        DefaultCompoundGraph<String, String> graph = new DefaultCompoundGraph<>("root");
        populateGraph(graph);
        assert(graph.isAncestor("1.2.1", "root"));
        assert(graph.isAncestor("1.2.1", "1.2"));
        assert(graph.isAncestor("2.1", "root"));
        assert(!graph.isAncestor("root", "1"));
        assert(!graph.isAncestor("root", "root"));
    }
    
    @Test
    public void testisLeaf() {
        DefaultCompoundGraph<String, String> graph = new DefaultCompoundGraph<>("root");
        populateGraph(graph);
        assert(graph.isLeaf("2.1"));
        assert(graph.isLeaf("1.2.1"));
        assert(!graph.isLeaf("root"));
        assert(!graph.isLeaf("1.2"));
    }
    
    @Test
    public void testGetDepth() {
        DefaultCompoundGraph<String, String> graph = new DefaultCompoundGraph<>("root");
        populateGraph(graph);
        assert(graph.getDepth("root") == 0);
        assert(graph.getDepth("1")== 1);
        assert(graph.getDepth("2.1") == 2);
        assert(graph.getDepth("1.2.2") == 3);
    }
    
    @Test
    public void testisDescendant() {
        DefaultCompoundGraph<String, String> graph = new DefaultCompoundGraph<>("root");
        populateGraph(graph);
        assert(!graph.isDescendant("1.2.1", "root"));
        assert(!graph.isDescendant("1.2.1", "1.2"));
        assert(!graph.isDescendant("2.1", "root"));
        assert(graph.isDescendant("root", "1"));
        assert(graph.isDescendant("root", "2.1"));
        assert(graph.isDescendant("root", "1.2.1"));
        assert(!graph.isDescendant("root", "root"));
    }
    
    @Test
    public void testGetDescendants() {
        DefaultCompoundGraph<String, String> graph = new DefaultCompoundGraph<>("root");
        populateGraph(graph);
        assert(graph.getDescendants("root").size() == 11);
        assert(graph.getDescendants("1").size() == 6);
        assert(graph.getDescendants("2").size() == 3);
    }
    */
    
    
}
