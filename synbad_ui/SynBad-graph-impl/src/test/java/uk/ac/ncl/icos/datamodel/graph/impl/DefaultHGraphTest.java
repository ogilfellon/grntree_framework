/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.datamodel.graph.impl;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;

/**
 *
 * @author owengilfellon
 */
public class DefaultHGraphTest {
    
    public DefaultHGraphTest() {
    }
    
       
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
      /* 
    private void printTree(DefaultHGraph<String, String> graph) {
        
        SBHIterator<String> i = graph.visibleIterator("root");
        StringBuilder sb = new StringBuilder();
        while(i.hasNext()) {
            
            
            String s = i.next();
            sb.append(graph.isVisible(s) ? "*" : " ");
            sb.append(i.getDepth());
            for(int n = 0; n < i.getDepth(); n++) {
                sb.append("\t");
                
            }
            sb.append(s);
            sb.append(" E{ ");
            graph.incomingEdges(s).stream().forEach(e ->
                sb.append(e));
            
            sb.append(" | ");
            graph.outgoingEdges(s).stream().forEach(e ->
                sb.append(e));
            sb.append(" } P { ");
            graph.incomingProxyEdges(s).stream().forEach(e ->
                sb.append(e));
            sb.append(" | ");
            graph.outgoingProxyEdges(s).stream().forEach(e ->
                sb.append(e));
            sb.append(" }\n");
        }
        
        System.out.println(sb.toString());        
    }
    
 
    @Test
    public void testGetDescendants() {
        DefaultHGraph<String, String> graph = new DefaultHGraph<>("root");
        
        
        graph.addNode("1", "root");
            graph.addNode("1.1", "1");
            graph.addNode("1.2", "1");
                graph.addNode("1.2.1", "1.2");
                graph.addNode("1.2.2", "1.2");
            graph.addNode("1.3", "1");
            graph.addNode("1.4", "1");
        graph.addNode("2", "root");
            graph.addNode("2.1", "2");
            graph.addNode("2.2", "2");
            graph.addNode("2.3", "2");
            
        graph.addEdge("1.1", "1.2", "1.1 -> 1.2");
        graph.addEdge("1.2.1", "1.1", "1.2.1 -> 1.1");
           
        for(String s : graph.getDescendants("root")) {
            System.out.println(s);
        }    
        
        System.out.println(graph.getDescendants("root").stream().count());
        printTree(graph);
      
    }
*/
/*
    @Test
    public void testGetDescendantsWithContraction() {
        DefaultHGraph<String, String> graph = new DefaultHGraph<>("root");
        graph.addListener(new HGraphListener<String, String>() {

            @Override
            public void onProxyEdgeEvent(SBEventType type, String edge) {
                System.out.println("PROXY: " + type + " " + edge);
            }

            @Override
            public void onEntityEvent(SBEventType type, String instance, String parent) {
                System.out.println("ENTITY: " + type + " " + instance + " in " + parent);
            }

            @Override
            public void onEdgeEvent(SBEventType type, String edge) {
                System.out.println("EDGE: " + type + " " + edge);
            }
        });
        
        graph.addNode("1", "root");
            graph.addNode("1.1", "1");
            graph.addNode("1.2", "1");
                graph.addNode("1.2.1", "1.2");
                graph.addNode("1.2.2", "1.2");
            graph.addNode("1.3", "1");
            graph.addNode("1.4", "1");
        graph.addNode("2", "root");
            graph.addNode("2.1", "2");
            graph.addNode("2.2", "2");
            graph.addNode("2.3", "2");
            
        graph.addEdge("1.1", "1.2", "1.1 -> 1.2");
        graph.addEdge("1.2.1", "1.1", "1.2.1 -> 1.1");
        graph.addEdge("2", "1.2.2", "2 -> 1.2.2");
           
            
        int descendants = 11;
        
        assert(graph.getDescendants("root").size() == descendants);
        printTree(graph);
        
        // removes 1.2.1 and 1.2.2
        
        graph.contract("1.2");
        printTree(graph);
        assert(graph.getVisibleDescendants("root").size() == descendants - 2);
        assert(!graph.isVisible("1.2.1"));
        assert(!graph.isVisible("1.2.2"));
        
        
        // removes 1.1, 1.2, 1.3, 1.4

        graph.contract("1");
        assert(graph.getVisibleDescendants("root").size() == descendants - 6);
        assert(!graph.isVisible("1.2.1"));
        assert(!graph.isVisible("1.2.2"));
        assert(!graph.isVisible("1.1"));
        assert(!graph.isVisible("1.2"));
        assert(!graph.isVisible("1.3"));
        assert(!graph.isVisible("1.4"));
        printTree(graph);
        
        graph.contract("2");
        assert(graph.getVisibleDescendants("root").size() == descendants - 9);
        printTree(graph);        
// no effect as 1 is still contracted
        
        graph.expand("2");
        assert(graph.getVisibleDescendants("root").size() == descendants - 6);
        printTree(graph);        
        
        
        graph.expand("1.2");
        assert(graph.getVisibleDescendants("root").size() == descendants - 6);
        printTree(graph);
        // adds all descendants of 1, including the previously expanded 1.2
        
        graph.expand("1");
        assert(graph.getVisibleDescendants("root").size() == descendants);
        printTree(graph);
    }*/
    
}
