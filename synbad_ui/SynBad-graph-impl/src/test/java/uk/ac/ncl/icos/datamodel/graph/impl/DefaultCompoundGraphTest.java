/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.datamodel.graph.impl;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;

/**
 *
 * @author owengilfellon
 */
public class DefaultCompoundGraphTest {
    
    public DefaultCompoundGraphTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
/*
    @Test
    public void testCreateGraph() {
        SBCompoundGraph<String, String> g = new DefaultCompoundGraph("root");
        g.addListener(new HGraphListener<String, String>() {

            @Override
            public void onEntityEvent(SBEventType type, String instance, String parent) {
                System.out.println(type + ": " + instance + " " + parent);
            }

            @Override
            public void onProxyEdgeEvent(SBEventType type, String edge) {
                System.out.println(type + ": " + edge);
            }

            @Override
            public void onEdgeEvent(SBEventType type, String edge) {
                System.out.println(type + ": " + edge);
            }
            
        });
        
        g.addNode("Hello");
        g.addNode("World", "Hello");
        //g.removeNode("Hello");
        //g.addNode("Hello");
        //g.setParent("Hello", "World");
        g.addNode("!", "World");
        
        Iterator<String> i = g.hierarchyIterator("Hello");
        while(i.hasNext()) {
            String s = i.next();
            System.out.println(s);
            System.out.println(" - Desc(" + g.getDescendants(s).size() + ") Depth(" + g.getDepth(s) + ")");
            
        }
    }
    */
    
}
