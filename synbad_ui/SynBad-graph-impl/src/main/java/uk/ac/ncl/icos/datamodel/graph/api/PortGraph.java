 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.datamodel.graph.api;

import uk.ac.ncl.icos.synbad.h.graph.api.SBHGraph;


 /**
 * A hierarchical graph with ports defining valid composition (SVP view?)
 * @author owengilfellon
 */
public interface PortGraph extends SBHGraph {

    /*
        Event Source Type: Node { Instance, Port },  Edge {Structure Edge, Signal Edge}
        Event Type: Added, Removed
    */
    
    // Entities
    
    /*
    public EntityInstance getRoot();
    
    public Set<EntityInstance> getInstanceSet();
    public Set<EntityInstance> getInstances(SynBadObject definition);
    
  
    public EntityInstance getParent(EntityInstance instance);  
    public List<EntityInstance> getChildren(EntityInstance instance);

    public Set<PortRecord> getPortSet();
    public Set<PortRecord> getPorts(EntityInstance node);
    public Set<PortRecord> getPorts(IPort port);
 
    public EntityInstance getOwner(PortRecord port);
    public boolean hasProxy(PortRecord port);
    
    public PortRecord getProxy(PortRecord port);
    public Map<PortRecord, PortRecord> getConnectablePorts(EntityInstance from, EntityInstance to);
    
    public Iterable<IWire> getEdges(PortDirection direction, Instance node);


    
    public int getDepth(Object node);
    public EntityInstance getLowestCommonAncestor(EntityInstance from, EntityInstance to);
    // create crawler at "from" and crawl to root. set position to "to" and crawl until a known node is reached.
    public boolean canConnect(EntityInstance from, EntityInstance to);
    // load crawler with rules defining valid composition edges... 
    // if "to" is not found from a crawl of "from", then they cannot be connected.
*/

}
