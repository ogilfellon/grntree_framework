/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.datamodel.graph.impl;

import java.util.HashSet;
import uk.ac.ncl.icos.synbad.graph.api.EdgeDirection;
import uk.ac.ncl.icos.synbad.graph.api.SBCursor;
import uk.ac.ncl.icos.synbad.graph.api.SBEdge;
import uk.ac.ncl.icos.synbad.graph.api.SBObject;
import uk.ac.ncl.icos.synbad.h.graph.api.SBHCursor;
import uk.ac.ncl.icos.synbad.h.graph.api.SBHGraph;

/**
 *
 * @author owengilfellon
 */
public class DefaultHCursor<N extends SBObject, E extends SBEdge> extends SBCursor.ASBCursor<N,E> implements SBHCursor<N, E> {

    final SBHGraph<N,  E> view;
    
    public DefaultHCursor(N startNode, SBHGraph<N,  E> graph) {
        super(startNode, graph);
        this.view = graph;
    }

    @Override
    public Iterable<E> getEdges(EdgeDirection direction) {
        switch(direction) {
            case IN :
                return view.incomingEdges(positionStack.peek().getNode());
            case OUT :
                return view.outgoingEdges(positionStack.peek().getNode());
        }
        
        return new HashSet<>();
    }
    @Override
    public Iterable<E> getProxyEdges(EdgeDirection direction) {
        switch(direction) {
            case IN :
                return view.incomingProxyEdges(positionStack.peek().getNode());
            case OUT :
                return view.outgoingProxyEdges(positionStack.peek().getNode());
        }
        
        return new HashSet<>();
    }

    @Override
    public N getParent() {   
        return view.getParent(getCurrentPosition());
    }

    @Override
    public int getDepth() {
        return  view.getDepth(positionStack.peek().getNode());
    }

    @Override
    public void expand() {
         view.expand(positionStack.peek().getNode());
    }

    @Override
    public void collapse() {
         view.contract(positionStack.peek().getNode());
    }

    @Override
    public N getOriginalEdgeSource(E e) {
       return view.getEdgeSource(e);
    }

    @Override
    public N getOriginalEdgeTarget(E e) {
        return view.getEdgeTarget(e);
    }

    @Override
    public boolean isProxyEdge(E edge) {
        return view.isProxyEdge(edge);
    }

}
