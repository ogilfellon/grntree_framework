/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.datamodel.graph.impl;


import uk.ac.ncl.icos.synbad.h.graph.api.HGraphListener;
import uk.ac.ncl.icos.synbad.h.graph.api.GraphListener;
import uk.ac.ncl.icos.synbad.h.graph.api.SBHIterator;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import uk.ac.ncl.icos.synbad.graph.api.SBGraph;
import uk.ac.ncl.icos.datamodel.tree.ITree;
import uk.ac.ncl.icos.datamodel.tree.ITreeNode;
import uk.ac.ncl.icos.synbad.event.SBEventType;
import uk.ac.ncl.icos.synbad.graph.api.SBEdge;
import uk.ac.ncl.icos.synbad.graph.api.SBObject;
import uk.ac.ncl.icos.synbad.h.graph.api.SBCompoundGraph;

/**
 * 
 * @author owengilfellon
 */
public class DefaultCompoundGraph<N extends SBObject, E extends SBEdge> implements SBCompoundGraph<N, E> {

    private final static Logger LOGGER = Logger.getLogger(DefaultCompoundGraph.class.getName()); 
    
    private final SBGraph<N, E> graph;
    private final ITree<N> hierarchy;
    private final List<GraphListener<N, E>> listeners;

    public DefaultCompoundGraph(N root) {
        LOGGER.setLevel(Level.ALL);
        this.graph = new DefaultGraph<>();
        this.hierarchy = new ITree.ATree<>();
        this.listeners = new ArrayList<>();
        this.graph.addNode(root);
        this.hierarchy.setRootNode(new ITreeNode.ADataModelTreeNode<>(root));
    }

    // =========================================================================
    //                         Nodes
    // =========================================================================   
    
    @Override
    synchronized public boolean removeNode(N node) {
        
        if(node == null)
            throw new NullPointerException("Node cannot be null");

        if(!containsNode(node))
            return false;

        ITreeNode<N> treeNode = hierarchy.findNode(node);
        
        for(ITreeNode<N> n : getChildren(treeNode)) {
            removeNode(n.getData());
        }

        // TODO: Also explicitly remove edges now we are storing edge records
 
        if(treeNode.getParent().getChildren().remove(treeNode)) {
           graph.removeNode(node);
            dispatchNodeEvent(SBEventType.REMOVED, treeNode.getParent().getData(), node);
        }
        
        return false;
    }

    @Override
    synchronized public boolean removeAllNodes(Collection<? extends N> nodes) {
        
        boolean changed = false;
        
        for(N node : nodes) {
            if(removeNode(node)) {
                changed = true;
            }
        }
        
        return changed;
    } 
    
    @Override
    synchronized public boolean addNode(N instance, N parent) {
        
        if(instance == null)
            throw new NullPointerException("instance cannot be null");
        if(parent == null)
            throw new NullPointerException("parent cannot be null");
        if(!containsNode(parent))
            throw new NullPointerException("parent is not in graph");
        if(containsNode(instance))
            return false;
        
        return addNode(instance, parent, hierarchy.findNode(parent).getChildren().size());
    }
    
    @Override
    synchronized public boolean addNode(N instance, N parent, int index) {

        if(instance == null)
            throw new NullPointerException("instance cannot be null");
        if(parent == null)
            throw new NullPointerException("parent cannot be null");
        if(!containsNode(parent))
            throw new NullPointerException("parent is not in graph");
        if(containsNode(instance))
            return false;

        if(!graph.addNode(instance))
            return false;
        
        hierarchy.findNode(parent).addChild(index, new ITreeNode.ADataModelTreeNode<>(instance));
        dispatchNodeEvent(SBEventType.ADDED, parent, instance);
        return true;
    }

    @Override
    public boolean containsNode(N node) {
        return graph.nodeSet().contains(node);
    }
    
    @Override
    public boolean addNode(N node) {
       return addNode(node, getRoot());
    }

    @Override
    public Set<N> nodeSet() {
        return graph.nodeSet();
    }

    @Override
    public int degreeOf(N node) {
        return graph.degreeOf(node);
    }

    @Override
    public int inDegreeOf(N node) {
        return graph.inDegreeOf(node);
    }

    @Override
    public int outDegreeOf(N node) {
        return graph.outDegreeOf(node);
    }

    // =========================================================================
    //                                Edges
    // =========================================================================    
    
    
    @Override
    public List<E> incomingEdges(N node) {
        return graph.incomingEdges(node);
    }

    @Override
    public List<E> outgoingEdges(N node) {
         return graph.outgoingEdges(node);
    }
    
    
    @Override
    public E getEdge(N from, N to) {
        return graph.getEdge(from, to);
    }

    @Override
    public Set<E> getAllEdges(N node) {
        return graph.getAllEdges(node);
    }

    @Override
    public Set<E> getAllEdges(N sourceNode, N targetNode) {
        return graph.getAllEdges(sourceNode, targetNode);
    }

    
    @Override
    public Set<E> edgeSet() {
        return graph.edgeSet();
    }

    @Override
    public boolean containsEdge(E edge) {
        return edgeSet().contains(edge);
    }
    
    @Override
    public boolean containsEdge(N from, N to) {
        return graph.containsEdge(from, to);
    }
    
    
    @Override
    public N getEdgeSource(E edge) {
        return graph.getEdgeSource(edge);
    }

    @Override
    public N getEdgeTarget(E edge) {
        return graph.getEdgeTarget(edge);
    }
    

    @Override
    synchronized public boolean addEdge(N from, N to, E edge) {
        
        if(!graph.addEdge(from, to, edge))
            return false;
       
        dispatchEdgeEvent(SBEventType.ADDED, edge);
        
        return true;
    }
    
    @Override
    public boolean removeEdge(E edge) {
        if(!graph.removeEdge(edge)) 
            return false;

        dispatchEdgeEvent(SBEventType.REMOVED, edge);
        
        return true;
    }

    @Override
    public boolean removeAllEdges(Collection<? extends E> edges) {

        return graph.removeAllEdges(edges);

    }

    @Override
    public E removeEdge(N sourceNode, N targetNode) {
        return graph.removeEdge(sourceNode, targetNode);
    }

    @Override
    public Set<E> removeAllEdges(N sourceNode, N targetNode) {
        return graph.removeAllEdges(sourceNode, targetNode);
    }

    
    @Override
    public List<E> incomingEdges(N node, String predicate) {
        return graph.incomingEdges(node, predicate);
    }

    @Override
    public List<E> outgoingEdges(N node, String predicate) {
        return graph.outgoingEdges(node, predicate);
    }

    // =========================================================================
    //                               Hierarchy
    // =========================================================================  
    
    
    @Override
    public N getRoot() {
        return hierarchy.getRootNode().getData();
    }
    
    @Override
    public boolean isAncestor(N node, N ancestor) {

        if(node.equals(ancestor))
            return false;
        
        ITreeNode<N> record = hierarchy.findNode(node);
        
        while(record.getParent() != null){
            record = record.getParent();
            if(record.getData().equals(ancestor))
                return true;
        }
        
        return false;
        
    }
    
    @Override
    public void setParent(N parent, N node) {
        ITreeNode<N> currentParent = hierarchy.findNode(node).getParent();
        ITreeNode<N> parentNode = hierarchy.findNode(parent);
        ITreeNode<N> childNode = hierarchy.findNode(node);
        childNode.setParent(parentNode);
        dispatchNodeEvent(SBEventType.REMOVED, currentParent.getData(), node);
        dispatchNodeEvent(SBEventType.ADDED, parentNode.getData(), node);
    }
    
    
    
    @Override
    public N getParent(N instance) {
        if(instance == null)
            throw new NullPointerException("instance cannot be null");
        
        if(!containsNode(instance))
            throw new NullPointerException(instance + " not in graph");
        
        ITreeNode<N> n = hierarchy.findNode(instance).getParent();
        return null == n ? null : n.getData();
    }

    @Override
    public List<N> getChildren(N instance) {
        
        List<N> children = new ArrayList<>();
        
        if(hierarchy.findNode(instance) == null) 
            return children;
     
        hierarchy.findNode(instance).getChildren().stream()
                .forEach((n) -> children.add(n.getData()));
    
        return children;
    }
    
 
    private List<ITreeNode<N>> getChildren(ITreeNode<N> instance) {
        
        List<ITreeNode<N>> children = new ArrayList<>();
        
        if(hierarchy.findNode(instance.getData()) == null)
            return children;
        
        hierarchy.findNode(instance.getData()).getChildren().stream()
                .forEach(children::add);
        
        return children;
    }
    
    public boolean isLeaf(N node) {
        return getChildren(node).isEmpty();
    }

    @Override
    public boolean isDescendant(N ancestor, N descendant) {
        
        if(ancestor == null)
            throw new NullPointerException("ancestor cannot be null");
        
        if(descendant == null)
            throw new NullPointerException("descendant cannot be null");
        
        ITreeNode<N> tempNode = hierarchy.findNode(descendant);

        if(tempNode.getParent() == null) 
            return false;
        
        do {
            tempNode = tempNode.getParent();
            if(tempNode.getData().equals(ancestor))
                return true;
        } while (tempNode.getParent()!=null);
        
        return false;  
    }
    
    @Override
    public List<N> getDescendants(N instance) {
        
        List<N> descendants = new ArrayList<>();
        Iterator<N> i = hierarchyIterator(instance);
        // Skip root instance
        i.next();
        while(i.hasNext())
            descendants.add(i.next());
        return descendants;
    }
    
    
    @Override
    public boolean hasSiblings(N node) {
        if(getParent(node) == null)
            return false;
        
        return getChildren(getParent(node)).size() > 1;
    
    }

    @Override
    public List<N> getSiblings(N node) {
        if(getParent(node) == null)
            return new ArrayList<>();
        
        return getChildren(getParent(node)).stream()
                .filter(n -> !n.equals(node))
                .collect(Collectors.toList());
    }

  
    @Override
    public int getDepth(N node) {
        N parent = node;
        
        int depth = -1;

        while(parent != null) {
            parent = getParent(parent);
            depth++;
        }
        
        return depth;
    }
    
    // =========================================================================
    //                               Iteration
    // =========================================================================  

    private Iterator<N> allIterator(N instance) {
        ITree<N> tree = new ITree.ATree<>(hierarchy, hierarchy.findNode(instance));
        return tree.iterator();
    }

    @Override
    public SBHIterator<N> hierarchyIterator(N instance) {
        ITree<N> tree = new ITree.ATree<>(hierarchy, hierarchy.findNode(instance));
        return new CompoundIterator<>(tree);
    }
    
    // =========================================================================
    //                          Event dispatch
    // =========================================================================  
    
    public void dispatchNodeEvent(SBEventType type, N parent, N node) {
        for(GraphListener<N, E> listener : listeners) {
            listener.onEntityEvent(type, node, parent);
        }
    }
    
    public void dispatchEdgeEvent(SBEventType type, E edge) {
        for(GraphListener<N, E> listener : listeners) {
            listener.onEdgeEvent(type, edge);
        }
    }
    
    public void dispatchProxyEdgeEvent(SBEventType type, E edge) {
        for(GraphListener<N, E> listener : listeners) {
            if(listener instanceof HGraphListener)
                ((HGraphListener)listener).onProxyEdgeEvent(type, edge);
        }
    }
    
    // =========================================================================
    //                          Graph listeners
    // =========================================================================  
    


    @Override
    public void addListener(GraphListener<N, E> listener) {
        listeners.add(listener);
    }

    @Override
    public void removeListener(GraphListener<N, E> listener) {
        listeners.remove(listener);
    }


    // =========================================================================
    //                          Iterator
    // =========================================================================  

    public final class CompoundIterator<T> implements SBHIterator<T> {
        
        
        
        private final LinkedList<T> list = new LinkedList<>();
        private final Set<ITreeNode<T>> explored = new HashSet<>();
        private final ITreeNode<T> rootNode;
        private ITreeNode<T> currentNode = null;
        private final ITree<T> tree;
        
        private int index = -1;
        private int depth = 0;

        public CompoundIterator(ITree<T> tree) {
            rootNode = tree.getRootNode();
            list.add(rootNode.getData());
            this.tree = tree;
        }

        @Override
        public int getDepth()
        {
            if( currentNode == null)
                return 0;
            return currentNode.getDepth();
        }

        @Override
        public boolean hasNext() {

            if(currentNode == null && rootNode != null){
                LOGGER.log(Level.FINEST, "hasNext: rootNode");
                return true;
            }

            if(currentNode!=null) {
                if(!currentNode.getChildren().isEmpty() &&
                       !explored.containsAll(currentNode.getChildren())){
                    LOGGER.log(Level.FINEST, "hasNext: {0} has unexplored children", currentNode.getData());
                    return true;
                }

                if(currentNode.hasSiblings()) {
                    if(!explored.containsAll(currentNode.getParent().getChildren())){
                        LOGGER.log(Level.FINEST, "hasNext: {0} has unexplored siblings", currentNode.getData());
                        return true;
                    }     
                }

                if(currentNode.equals(tree.getRootNode())) {
                    LOGGER.log(Level.FINEST, "hasNoNext: atRootNode");
                    return false;
                }
                    
                
                ITreeNode<T> tempNode = currentNode;
                
                while(tempNode.getParent()!=null && !tempNode.equals(tree.getRootNode())) {
                    tempNode = tempNode.getParent();
                }  
                
                if(!explored.containsAll(tempNode.getChildren())) {
                        LOGGER.log(Level.FINEST, "hasNext: ancestor {0} has children", tempNode.getData());
                        return true;
                    }

                LOGGER.log(Level.FINEST, "hasNoNext: {0}", currentNode.getData());
            }
            


            return false;
        }

        @Override
        public T next() {

            // If we dont have a current node, set current node to root node
            
            if(currentNode == null && rootNode != null){
                currentNode = rootNode;
                
                if(!explored.contains(currentNode)) {
                    list.add(currentNode.getData());
                    explored.add(currentNode);
                    index++;
                }

                return currentNode.getData();
            }
            
            // If we have a current node

            if(currentNode!=null)
            {
                
                // first, if current node is unretracted, navigate through
                // unexplored children
                
                if(!currentNode.getChildren().isEmpty() && !explored.containsAll(currentNode.getChildren())){
                    ListIterator<ITreeNode<T>> it = currentNode.getChildren().listIterator();
                    index++;
                    
                    while(it.hasNext())
                    {
                        ITreeNode<T> n = it.next();

                        if(!explored.contains(n))
                        {
                            currentNode = n;
                            list.add(currentNode.getData());
                            explored.add(currentNode);
                            depth++;
                            
                            return n.getData();
                        }
                    }
                }
                
                // if there are no unexplored children, then move on to siblings

                if(currentNode.hasSiblings())
                {
                    if(!explored.containsAll(currentNode.getParent().getChildren())){
                        ListIterator<ITreeNode<T>> it = currentNode.getParent().getChildren().listIterator();
                        while(it.hasNext())
                        {
                            ITreeNode<T> n = it.next();

                            if(!explored.contains(n))
                            {
                                currentNode = n;
                                list.add(currentNode.getData());
                                explored.add(currentNode);
                                index++;
                                return n.getData();
                            }
                        }
                    }                   
                }
             
                // if there are no siblings, then retrace hierarchy to nearest
                // ancestor with unexplored children
                
               ITreeNode<T> tempNode = currentNode;

                while(tempNode.getParent()!=null && !tempNode.equals(tree.getRootNode())){

                    tempNode = tempNode.getParent();


                    if(!explored.containsAll(tempNode.getChildren())) {
                        depth--;
                        ListIterator<ITreeNode<T>> it = tempNode.getChildren().listIterator();
                        while(it.hasNext())
                        {
                            ITreeNode<T> n = it.next();

                            if(!explored.contains(n))
                            {
                                currentNode = n;
                                list.add(currentNode.getData());
                                explored.add(currentNode);
                                index++;
                                return n.getData();
                            }
                        }
                    }
                }
            }

            return null;
        }

        @Override
        public boolean hasPrevious() {
            return index > 0;
        }

        @Override
        public T previous() {
            index--;
            return list.get(index);
        }

        @Override
        public int nextIndex() {
            return index + 1;
        }

        @Override
        public int previousIndex() {
            return index - 1;
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public void set(T e) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public void add(T e) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }
}
