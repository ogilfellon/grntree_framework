 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.datamodel.graph.impl;

import uk.ac.ncl.icos.synbad.h.graph.api.GraphListener;
import java.util.ArrayList;
import uk.ac.ncl.icos.synbad.graph.api.SBGraph;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import org.jgrapht.graph.DirectedMultigraph;
import uk.ac.ncl.icos.synbad.event.SBEventType;
import uk.ac.ncl.icos.synbad.graph.api.SBEdge;
import uk.ac.ncl.icos.synbad.graph.api.SBObject;

/**
 *
 * @author owengilfellon
 */

public class DefaultGraph<N extends SBObject, E extends SBEdge> implements SBGraph<N, E> {

    private final DirectedMultigraph<N, E> graph;    
    private final List<GraphListener<N, E>> listeners;

    public DefaultGraph() {
        Class edgeClass = Object.class;
        this.graph = new DirectedMultigraph<>(edgeClass);
        listeners = new ArrayList<>();
    }
    
    private DefaultGraph(DirectedMultigraph<N, E> graph) {
        this.graph = graph;
        listeners = new ArrayList<>();
    }
    
    public void dispatchNodeEvent(SBEventType type, N node) {
        for(GraphListener listener : listeners) {
            listener.onEntityEvent(type, node, null);
        }
    }
    
    public void dispatchEdgeEvent(SBEventType type, E edge) {
        for(GraphListener listener : listeners) {
            listener.onEdgeEvent(type, edge);
        }
    }


    @Override
    public boolean addNode(N node) {
        if(!graph.addVertex(node))
            return false;
        
        dispatchNodeEvent(SBEventType.ADDED, node);
        
        return true;
    }

    
    @Override
    public boolean addEdge(N sourceNode, N targetNode, E edge) {
        if(!graph.addEdge(sourceNode, targetNode, edge))
            return false;
        
        dispatchEdgeEvent(SBEventType.ADDED, edge);
        
        return true;
    }
    
    @Override
    public Set<E> getAllEdges(N sourceNode, N targetNode) {
        return graph.getAllEdges(sourceNode, targetNode);
    }


    @Override
    public N getEdgeSource(E edge) {
        return graph.getEdgeSource(edge);
    }

    @Override
    public N getEdgeTarget(E edge) {
        return graph.getEdgeTarget(edge);
    }

    @Override
    public boolean containsEdge(E edge) {
        return graph.containsEdge(edge);
    }

    @Override
    public boolean containsNode(N node) {
        return graph.containsVertex(node);
    }

    @Override
    public int degreeOf(N node) {
        return graph.degreeOf(node);
    }

    @Override
    public Set<E> edgeSet() {
        return graph.edgeSet();
    }

    @Override
    public Set<E> getAllEdges(N node) {
        return graph.edgesOf(node);
    }

    @Override
    public int inDegreeOf(N node) {
        return graph.inDegreeOf(node);
    }

    @Override
    public List<E> incomingEdges(N node) {
        return graph.containsVertex(node) ? new ArrayList<>(graph.incomingEdgesOf(node)) : new ArrayList<>();
    }

    @Override
    public int outDegreeOf(N node) {
        return graph.outDegreeOf(node);
    }

    @Override
    public List<E> outgoingEdges(N node) {
        return graph.containsVertex(node) ? new ArrayList<>(graph.outgoingEdgesOf(node)) : new ArrayList<>();
    }

    @Override
    public E removeEdge(N sourceNode, N targetNode) {
        E edge  = graph.removeEdge(sourceNode, targetNode);
        if(edge == null)
            return null;
        
        dispatchEdgeEvent(SBEventType.REMOVED, edge);
        return edge;
    }

    @Override
    public boolean removeEdge(E edge) {
        if(!graph.removeEdge(edge))
            return false;
        
        dispatchEdgeEvent(SBEventType.REMOVED, edge);
        return true;
    }

    @Override
    public boolean removeNode(N node) {
        if(!graph.removeVertex(node))
            return false;
        
        dispatchNodeEvent(SBEventType.REMOVED, node);
        return true;
    }

    @Override
    public Set<N> nodeSet() {
        return graph.vertexSet();
    }

    @Override
    public boolean containsEdge(N sourceNode, N targetNode) {
        return graph.containsEdge(sourceNode, targetNode);
    }

    @Override
    public boolean removeAllEdges(Collection<? extends E> edges) {
        return graph.removeAllEdges(edges);
    }

    @Override
    public Set<E> removeAllEdges(N sourceNode, N targetNode) {
        Set<E> edges = graph.removeAllEdges(sourceNode, targetNode);
        
        for(E edge : edges) {
            dispatchEdgeEvent(SBEventType.REMOVED, edge);
        }
        
        return edges;
    }

    @Override
    public boolean removeAllNodes(Collection<? extends N> nodes) {
        if( !graph.removeAllVertices(nodes))
            return false;
        
        for(N node : nodes) {
            dispatchNodeEvent(SBEventType.REMOVED, node);
        }
        
        return true;
    }
    
    
    @Override
    public E getEdge(N from, N to) {
        return graph.getEdge(from, to);
    }


    @Override
    public List<E> incomingEdges(N node, String predicate) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<E> outgoingEdges(N node, String predicate) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void addListener(GraphListener<N, E> listener) {
        listeners.add(listener);
    }

    @Override
    public void removeListener(GraphListener<N, E> listener) {
        listeners.remove(listener);
    }

}