/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.datamodel.tree;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import uk.ac.ncl.icos.synbad.event.SBSubscriber;
import uk.ac.ncl.icos.synbad.event.SBEventSource;
import uk.ac.ncl.icos.synbad.event.SBEvent;
import uk.ac.ncl.icos.synbad.event.SBEventType;


/**
 *
 * @author owengilfellon
 */
public interface ITree<T> extends SBSubscriber, SBEventSource {

    public ITreeNode<T> getRootNode();
    public ITreeNode<T> findNode(T data);
    public void setRootNode(ITreeNode<T> rootNode);
    public Iterator<T> iterator();
    
    public class ATree<T> implements ITree<T> {
        
        private final Map<T, ITreeNode<T>> map = new HashMap<>();
        private final List<SBSubscriber> listeners = new ArrayList<>();
        private ITreeNode<T> rootNode = null;

        public ATree() {
        }

        public ATree(ITree<T> tree, ITreeNode<T> root) {
            ITreeNode<T> rootA = tree.findNode(root.getData());
            ITreeNode<T> rootB = new ITreeNode.ADataModelTreeNode<>(root.getData());
            setRootNode(rootB);
            addDescendants(tree, root.getData());
        }
        
        private void addDescendants(ITree<T> tree, T node) {
            
            ITreeNode<T> nodeA = tree.findNode(node);
            ITreeNode<T> nodeB = findNode(node);
            
            for(ITreeNode<T> childA : nodeA.getChildren()) {
                nodeB.addChild(new ITreeNode.ADataModelTreeNode<>(childA.getData()));
                addDescendants(tree, childA.getData());
            }
        }

        @Override
        public ITreeNode<T> getRootNode() {
            return rootNode;
        }
        
        @Override
        public ITreeNode<T> findNode(T data) {
            return map.get(data);
        }

        @Override
        public void setRootNode(ITreeNode<T> rootNode) {
            this.rootNode = rootNode;
            map.put(rootNode.getData(), rootNode);
            rootNode.addChangeListener(this);
        }

        @Override
        public void onEvent(SBEvent event) {
            if(event.getType() == SBEventType.ADDED) {
                addNodeChildren(rootNode);
                alert(event);
            }  else if(event.getType() == SBEventType.REMOVED) {
                removeNodeChildren(rootNode);
                alert(event);
            }
        }
        
        private void addNodeChildren(ITreeNode<T> node) {  
            for(ITreeNode<T> child : node.getChildren()) {
                map.put(child.getData(), child);
                addNodeChildren(child);
            }
        }
        
        private void removeNodeChildren(ITreeNode<T> node) {
            for(ITreeNode<T> child : node.getChildren()) {
                map.remove(child.getData());
                removeNodeChildren(child);
            }
        }
 
        void alert(SBEvent event) {
            for(SBSubscriber listener : listeners) {
                listener.onEvent(event);
            }
        }

        @Override
        public boolean addChangeListener(SBSubscriber listener) {
            return listeners.add(listener);
        }

        @Override
        public boolean removeChangeListener(SBSubscriber listener) {
            return listeners.remove(listener);
        }

        @Override
        public void clearChangeListeners() {
            listeners.clear();
        }

        @Override
        public Iterator<T> iterator() {
            return new PreOrderIterator<>(rootNode);
        }
        

        public Iterator<T> iterator(T node) {
            return new PreOrderIterator<>(findNode(node));
        }
    }
    
    public class PreOrderIterator<T> implements Iterator<T> {

        private final Set<ITreeNode<T>> explored = new HashSet<>();
        private final ITreeNode<T> rootNode;
        private ITreeNode<T> currentNode = null;
        private int depth = 0;

        public PreOrderIterator(ITreeNode<T> node) {
            rootNode = node;
        }

        public int getDepth()
        {
            return depth;
        }

        @Override
        public boolean hasNext() {

            if(currentNode == null && rootNode != null){
                return true;
            }

            if(currentNode!=null) {
                if(!currentNode.getChildren().isEmpty() && !explored.containsAll(currentNode.getChildren())){
                    return true;
                }

                if(currentNode.hasSiblings()) {
                    if(!explored.containsAll(currentNode.getParent().getChildren())){
                        return true;
                    }     
                }

                ITreeNode<T> tempNode = currentNode;

                while(tempNode.getParent() != null && tempNode.getParent() != rootNode) {

                    tempNode = tempNode.getParent();

                    if(!explored.containsAll(tempNode.getChildren())) {
                        return true;
                    }
                }  
            }

            return false;
        }

        @Override
        public T next() {

            if(currentNode == null && rootNode != null){
                currentNode = rootNode;
                explored.add(currentNode);
                return currentNode.getData();
            }

            if(currentNode!=null)
            {
                if(!currentNode.getChildren().isEmpty() && !explored.containsAll(currentNode.getChildren())){
                    ListIterator<ITreeNode<T>> it = currentNode.getChildren().listIterator();
                    while(it.hasNext())
                    {
                        ITreeNode<T> n = it.next();

                        if(!explored.contains(n))
                        {
                            currentNode = n;
                            explored.add(currentNode);
                            depth++;
                            return n.getData();
                        }
                    }
                }

                if(currentNode.hasSiblings())
                {
                    if(!explored.containsAll(currentNode.getParent().getChildren())){
                        ListIterator<ITreeNode<T>> it = currentNode.getParent().getChildren().listIterator();
                        while(it.hasNext())
                        {
                            ITreeNode<T> n = it.next();

                            if(!explored.contains(n))
                            {
                                currentNode = n;
                                explored.add(currentNode);
                                return n.getData();
                            }
                        }
                    }                   
                }

               ITreeNode<T> tempNode = currentNode;

                while(tempNode.getParent()!=null){

                    tempNode = tempNode.getParent();


                    if(!explored.containsAll(tempNode.getChildren())) {
                        depth--;
                        ListIterator<ITreeNode<T>> it = tempNode.getChildren().listIterator();
                        while(it.hasNext())
                        {
                            ITreeNode<T> n = it.next();

                            if(!explored.contains(n))
                            {
                                currentNode = n;
                                explored.add(currentNode);
                                return n.getData();
                            }
                        }
                    }
                }
            }

            return null;
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

    }

}
