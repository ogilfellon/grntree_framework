/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.events.impl;

import uk.ac.ncl.icos.synbad.event.SBEvent;
import uk.ac.ncl.icos.synbad.event.SBEventType;

/**
 *
 * @author owengilfellon
 */
public class DefaultEvent<S, O> implements SBEvent<S, O> {

    private final SBEventType type;
    private final S changeSource;
    private final O changeObject;

    public DefaultEvent(SBEventType type, S changeSource, O changeObject) {
        this.type = type;
        this.changeSource = changeSource;
        this.changeObject = changeObject;
    }

    @Override
    public SBEventType getType() {
        return type;
    }

    @Override
    public S getSource() {
        return changeSource;
    }

    @Override
    public O getObject() {
        return changeObject;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " " + type + " " + changeObject + " in " + changeSource;
    }
    
    
    

}
