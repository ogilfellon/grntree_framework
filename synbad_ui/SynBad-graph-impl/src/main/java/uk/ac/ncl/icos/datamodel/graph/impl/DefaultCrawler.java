/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.datamodel.graph.impl;


import java.util.HashSet;
import java.util.Set;
import uk.ac.ncl.icos.synbad.graph.api.EdgeDirection;
import uk.ac.ncl.icos.synbad.graph.api.SBCrawler;
import uk.ac.ncl.icos.synbad.graph.api.SBCursor;
import uk.ac.ncl.icos.synbad.graph.api.SBEdge;
import uk.ac.ncl.icos.synbad.graph.api.SBObject;

/**
 *
 * @author owengilfellon
 */
public class DefaultCrawler<N extends SBObject, E extends SBEdge> extends SBCrawler.SBCrawlerAdapter<N, E> {
    
    @Override
    public void run(SBCursor<N, E> cursor) {
        processNode(cursor, new HashSet<>());
        onEnded(); 
    }
  
    @Override
    public void processNode(SBCursor<N, E> cursor, Set<E> seenEdges) {

        if(terminate()) {
            return;
        }
        
        onInstance(cursor.getCurrentPosition(), cursor);
        
        for (E edge : cursor.getEdges(EdgeDirection.OUT)) {
            if(!seenEdges.contains(edge)) {
                processEdge(cursor, seenEdges, edge);
            }       
        }

        for (E edge : cursor.getEdges(EdgeDirection.IN)) {
            if(!seenEdges.contains(edge))
                processEdge(cursor, seenEdges, edge);
        }

    }
    
    @Override
    public void processEdge(SBCursor<N, E> cursor, Set<E> seenEdges, E edge) {

        seenEdges.add(edge);
        
        if(vetoEdge(edge))
            return;
        
        onEdge(edge);
        
        // TODO: Should work for to->from as well as from->to
        
        if(hasVisited(cursor, cursor.getEdgeTarget(edge)))
            return;

        cursor.moveTo(cursor.getEdgeTarget(edge));
        processNode(cursor, seenEdges);

    }
    
    private boolean hasVisited(SBCursor<N, E> cursor,Object instance) {
        for(N i : cursor.getPath()) {
            if(i.equals(instance)) {
                return true;
            }
                
        }
        return false;
    }
}
