/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.datamodel.graph.impl;

import java.util.HashSet;
import java.util.Set;
import uk.ac.ncl.icos.synbad.graph.api.SBCursor;
import uk.ac.ncl.icos.synbad.graph.api.EdgeDirection;
import uk.ac.ncl.icos.synbad.graph.api.SBEdge;
import uk.ac.ncl.icos.synbad.graph.api.SBObject;
import uk.ac.ncl.icos.synbad.h.graph.api.SBHCrawler;
import uk.ac.ncl.icos.synbad.h.graph.api.SBHCrawlerListener;
import uk.ac.ncl.icos.synbad.h.graph.api.SBHCursor;

/**
 *
 * @author owengilfellon
 */
public class DefaultHCrawler<N extends SBObject, E extends SBEdge>  extends SBHCrawlerListener.SBHCrawlerListenerAdapter<N, E> implements SBHCrawler<N, E> {
    
    private final Set<SBHCrawlerListener<N, E>> listeners = new HashSet<>();

    
    @Override
    public void addListener(SBHCrawlerListener<N, E> listener) {
        listeners.add(listener);
    }
    
    @Override
    public void removeListener(SBHCrawlerListener<N, E> listener) {
        listeners.remove(listener);
    }
    
    @Override
    public void run(SBHCursor<N, E> cursor) {
        processNode(cursor, new HashSet<>());
        onEnded(); 
    }


    @Override
    public void onEdge(E edge) {
        for(SBHCrawlerListener<N, E> listener : listeners) {
            listener.onEdge(edge);
        }
    }
    
    @Override
    public void onProxyEdge(E edge) {

        for(SBHCrawlerListener<N, E> listener : listeners) {
            listener.onProxyEdge(edge);
        }
    }

    @Override
    public void onEnded() {
        for(SBHCrawlerListener<N, E> listener : listeners) {
            listener.onEnded();
        }
    }

    @Override
        public void onInstance(N instance, SBCursor<N, E> cursor) {
        for(SBHCrawlerListener<N, E> listener : listeners) {
            listener.onInstance(instance, cursor);
        }
    }

    @Override
    public void processNode(SBHCursor<N, E> cursor, Set<E> seenEdges) {

        if(terminate()) {
            return;
        }
        
        onInstance(cursor.getCurrentPosition(), cursor);
        
        for (E edge : cursor.getEdges(EdgeDirection.OUT)) {
            if(!seenEdges.contains(edge)) {
                processEdge(cursor, seenEdges, edge);
            }       
        }

        for (E edge : cursor.getEdges(EdgeDirection.IN)) {
            if(!seenEdges.contains(edge))
                processEdge(cursor, seenEdges, edge);
        }
        
        for (E edge : cursor.getProxyEdges(EdgeDirection.OUT)) {
            if(!seenEdges.contains(edge)) {
                processEdge(cursor, seenEdges, edge);
            }       
        }

        for (E edge : cursor.getProxyEdges(EdgeDirection.IN)) {
            if(!seenEdges.contains(edge))
                processEdge(cursor, seenEdges, edge);
        }
    }

    
    @Override
    public void processEdge(SBHCursor<N, E> cursor, Set<E> seenEdges, E edge) {
                seenEdges.add(edge);
        
        if(vetoEdge(edge))
            return;
        
        
        
        if(!cursor.isProxyEdge(edge))
            onEdge(edge);
        else
            onProxyEdge(edge);
        
        
        // TODO: Should work for to->from as well as from->to
        
        if(hasVisited(cursor, cursor.getEdgeTarget(edge)))
            return;

        cursor.moveTo(cursor.getEdgeTarget(edge));
        processNode(cursor, seenEdges);
    }
    
       private boolean hasVisited(SBCursor<N, E> cursor,Object instance) {
        for(N i : cursor.getPath()) {
            if(i.equals(instance)) {
                return true;
            }
                
        }
        return false;
    }




}
