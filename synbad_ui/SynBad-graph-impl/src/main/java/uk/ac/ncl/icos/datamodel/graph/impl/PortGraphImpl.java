/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.datamodel.graph.impl;




public class PortGraphImpl { //extends AView implements SBSubscriber  {

/*
    private final CompoundGraph<VNode, VEdge> graph;
    private final Set<VNode> expandedNodes;
    private final Map<URI, Set<VPort>> instanceToPort;
    private final Map<URI, Set<VPort>> definitionToPort;
    private final Map<URI, Set<VEdge>> wireSet;

    private final Map<VNode, Set<VPort>> danglingProxies;
    private final Map<VNode, Set<IWire>> wiresInInstance;

    private final SignalEdgeFactory edgeFactory;
    
    private final VModel model;

     public PortGraphImpl(SynBadPObject root, VModel model) {
        this.edgeFactory = new SignalEdgeFactory();
        this.instanceToPort = new HashMap<>();
        this.definitionToPort = new HashMap<>();
        this.wireSet = new HashMap<>();

        this.expandedNodes = new HashSet<>();

        this.danglingProxies = new HashMap<>();
        this.wiresInInstance = new HashMap<>();
        this.model = model;
        
        EntityInstance instance = new EntityInstance(null, root, root.getDisplayID()+"_instance");
        VNode<SynBadPObject> rootNode = new VNode(instance, model);
        this.graph = new CompoundGraph<>(rootNode);
        registerNode(rootNode);
        
        addInstance(rootNode, null, 0);

    }
     
    private void registerPort(VPort port) {
         
        URI instanceId = port.getData().getIdentity();
        URI definitionId = port.getData().getValue().getIdentity();
        
        if(!instanceToPort.containsKey(instanceId)) {
            instanceToPort.put(instanceId, new HashSet<>());
        }
        
        instanceToPort.get(instanceId).add(port);
        
        if(!definitionToPort.containsKey(definitionId)) {
            definitionToPort.put(definitionId, new HashSet<>());
        }
        
        definitionToPort.get(definitionId).add(port);
        
    }
    
    private void deregisterPort(VPort port) {
        
        URI instanceId = port.getData().getIdentity();
        URI definitionId = port.getData().getValue().getIdentity();
        
        instanceToPort.get(instanceId).remove(port);
        
        if(instanceToPort.get(instanceId).isEmpty()) {
            instanceToPort.remove(instanceId);
        }
        
        definitionToPort.get(definitionId).remove(port);
        
        if(definitionToPort.get(definitionId).isEmpty()) {
            definitionToPort.remove(definitionId);
        }
    }

    private void addInstance(VNode instance, VNode parent, int index) {

        // If node is an EntityInstance, add it's port and store it's wires
        
        if(!(instance.getData() instanceof EntityInstance))
            throw new IllegalArgumentException("addInstance() cannot add " + instance.getData() );

        if(parent != null) {
            graph.addNode(instance, parent, index);
            registerNode(instance);

            for(ViewListener listener : getListeners()) {
                listener.onEntityEvent(EventType.ADD, instance, null);
            }
            
            new DomainEdge(SynBadTerms2.SynBadEntity
                    
                    .parent.getData().getIdentity(), instance.getData().getIdentity());
            
            addEdge(new VEdge(parent, instance, SignalEdgeFactory.hasChild(
                    parent.getData().getDisplayID() + "_child"+getChildren(parent).size(),
                    (EntityInstance)parent.getData(),
                    (EntityInstance)instance.getData()), model));
        }
        
        EntityInstance eInstance = (EntityInstance) instance.getData();

        for(IWire wire : eInstance.getValue().getWires()) {
            if(!wiresInInstance.containsKey(instance))
                wiresInInstance.put(instance, new HashSet<>());

            wiresInInstance.get(instance).add(wire);
        }
    }
    
    
    private void removeInstance(VNode instance) {

        if(instance == graph.getRoot()) 
             throw new NullPointerException("Cannot remove root");
        
        if(!(instance.getData() instanceof EntityInstance))
            throw new IllegalArgumentException("removeInstance() cannot remove " + instance.getData() );
      
        
        VNode parent = getParent(instance);
        
        for(VNode node : graph.getChildren(instance)) {
            if(node.getData() instanceof EntityInstance) removeInstance(node);
        }
        
        for(VEdge edge : CollectionUtils.union(graph.incomingEdgesOf(instance), graph.outgoingEdgesOf(instance))) {
            removeEdge(edge);
        }
        
        if(instance.getData().getValue() instanceof SynBadPObject) {
            for(IWire wire : ((SynBadPObject)instance.getData().getValue()).getWires()) {
                wiresInInstance.get(instance).remove(wire);
                if(wiresInInstance.get(instance).isEmpty())
                    wiresInInstance.remove(instance);
            }
        }

        graph.removeNode(instance);
        deregisterNode(instance);
        
        for(ViewListener listener : getListeners())
            listener.onEntityEvent(EventType.REMOVE, instance, parent);

    }
    
    private void addEdge(VEdge edge) {
        graph.addEdge(edge.getFrom(), edge.getTo(), edge);
        registerEdge(edge);
        
        SynBadObject wire = edge.getData().getValue();
        
        if(wire != null) {
            if(!wireSet.containsKey(wire.getIdentity()))
                wireSet.put(wire.getIdentity(), new HashSet<VEdge>());
            wireSet.get(wire.getIdentity()).add(edge);
               
        }
        
        for(ViewListener listener : getListeners())
            listener.onEdgeEvent(EventType.ADD, edge);
    }

    private void removeEdge(VEdge edge) {
        graph.removeEdge(edge);
        deregisterEdge(edge);
        
        SynBadObject wire = edge.getData().getValue();
        
        if(wire != null) {
            wireSet.get(wire.getIdentity()).remove(edge);
            
            if(wireSet.get(wire.getIdentity()).isEmpty())
               wireSet.remove(wire.getIdentity());
        }
        
        for(ViewListener listener : getListeners())
            listener.onEdgeEvent(EventType.REMOVE, edge);
    }
    
    
    private void addPort(VPort portNode, VNode ownerNode) {
        
        graph.addNode(portNode, ownerNode);
        
        registerPort(portNode);
        
        for(ViewListener listener : getListeners()) {
            listener.onEntityEvent(EventType.ADD, portNode, ownerNode);
        }
        
        VEdge viewEdge = new VEdge(ownerNode, portNode, SignalEdgeFactory.hasPort(
                "wire_" + graph.edgeSet().size(),
                (EntityInstance)ownerNode.getData(),
                portNode.getData()), model);
        
        addEdge(viewEdge);
        
        connectLowerPortToHigherProxies(portNode, getParent(ownerNode)); 

        if(portNode.getData().getValue().isProxy())
            storeDanglingProxy(portNode, ownerNode);

    }
    
    private void removePort(VPort portNode, VNode ownerNode) {

        for(VEdge edge : CollectionUtils.union(graph.incomingEdgesOf(portNode), graph.outgoingEdgesOf(portNode))) {
            if(edge.getData().getType() == ModelEdgeType.HAS_PORT)
                removeEdge(edge);
        }
        
        graph.removeNode(portNode);
        deregisterPort(portNode);
        
        for(ViewListener listener : getListeners()) {
            listener.onEntityEvent(EventType.REMOVE, portNode, ownerNode);
        }  

        if(portNode.getData().getValue().isProxy() && ownerNode != getRoot())
            removeDanglingProxies(portNode, getParent(ownerNode));

    }
    
    private void connectLowerPortToHigherProxies(VPort port, VNode ownersParent) {
       
        if(!danglingProxies.containsKey(ownersParent))
            return;
        
        Set<VPort> added = new HashSet<>();
        
        for(VPort danglingProxy : danglingProxies.get(ownersParent)) {
            
            ProxyPort proxyPort = (ProxyPort)danglingProxy.getData().getValue();
            
             if(proxyPort.getProxied() == port.getData().getValue()) {

                VEdge viewEdge = new VEdge(danglingProxy, port, SignalEdgeFactory.proxies(
                            "wire_" + graph.edgeSet().size(), 
                            danglingProxy.getData(),
                            port.getData()), model);
                    addEdge(viewEdge);
                    added.add(danglingProxy);
                }
            }
        
     
        for(VPort instance : added) {
           
          
    }
    
    private void storeDanglingProxy(VPort proxyPort, VNode owner) {

        if(proxyPort == null)
            throw new NullPointerException("proxy cannot be null");
        
        if(owner == null)
            throw new NullPointerException("owner cannot be null");
            
        if(danglingProxies.get(owner) == null) 
            danglingProxies.put(owner, new HashSet<>());
        
        danglingProxies.get(owner).add(proxyPort);

    }
    
    private void removeDanglingProxies(VNode proxyPort, VNode owner) {

        if(proxyPort == null)
            throw new NullPointerException("proxy cannot be null");
        
        if(owner == null)
            throw new NullPointerException("owner cannot be null");

        if(danglingProxies.get(owner) == null) 
           return;

        danglingProxies.get(owner).remove(proxyPort);
        
        if(danglingProxies.get(owner).isEmpty())
            danglingProxies.remove(owner);
    }

    private boolean areSibilings(VNode instance1, VNode instance2) {
        return getParent(instance1) == getParent(instance2);
    }

    @Override
    public List<VNode> getChildren(VNode instance) {
        
        List<VNode> instances = new ArrayList<>();
        
        for(VNode entityInstance: graph.getChildren(instance)) {
            instances.add(entityInstance);
        }
        
        return instances;
    }

    @Override
    public VNode getRoot() {
        return graph.getRoot();
    }

    @Override
    public VNode getParent(VNode node) {
        return graph.getParent(node);
    }

    public boolean hasProxy(VPort port) {
        return getProxy(port) != null;
    }

    public Set<VPort> getPorts(VNode node) {

        Set<VPort> ports = new HashSet<>();

        for(VEdge edge : graph.outgoingEdgesOf(node)) {
            if(edge.getData().getType() ==  ModelEdgeType.HAS_PORT)
                ports.add((VPort)edge.getTo());
        }

        return ports;
    }


    public VNode getOwner(VPort port) {
        for(VEdge edge : graph.incomingEdgesOf(port)) {
            if(edge.getData().getType() ==  ModelEdgeType.HAS_PORT)
                return edge.getFrom();
        }
        
        return null;
    }

    public VPort getProxy(VPort port) {
        for(VEdge edge : graph.incomingEdgesOf(port)) {
            if(edge.getData().getType() ==  ModelEdgeType.HAS_PORT)
                return (VPort)edge.getFrom();
        }
        
        return null;
    }

    public VNode getLowestCommonAncestor(VNode from, VNode to) {
        
        Map<VPort, VPort> portPairs = getConnectablePorts(from, to);
        if(portPairs.size() != 1)
            return null;
        
        VNode commonParent = null;
        VNode ancestor = getParent(from);
        VPort tempFrom = portPairs.keySet().iterator().next();
        VPort tempTo = portPairs.get(tempFrom);

        // follow proxies, not parents
        
        while(commonParent == null && ancestor!=null && tempFrom != null) {
            if(graph.isDescendant(ancestor, to))
                commonParent = ancestor;
            
            tempFrom = getProxy(tempFrom);
            if(tempFrom != null)
                ancestor = getParent(getOwner(tempFrom));
        }
        
        if(commonParent!=null)
            return null;
        
        ancestor = null;
        
        while(ancestor != commonParent && tempTo != null) {

            tempTo = getProxy(tempTo);
            if(tempTo != null)
                ancestor = getParent(getOwner(tempTo));
        }

        if(ancestor != commonParent)
            return null;
        
        return ancestor;
    }
    

    public Map<VPort, VPort> getConnectablePorts(VNode from, VNode to) {
        Map<VPort, VPort> fromTo = new HashMap<>();

        for(VPort outPort : getPorts(from)) {
            for(VPort inPort : getPorts(to)) {
                if(outPort.getData().getValue().isConnectable(inPort.getData().getValue()) &&
                        inPort.getData().getValue().isConnectable(outPort.getData().getValue())) {
                    fromTo.put(outPort, inPort);
                } 
            }
        }
        
        return fromTo;
    }

    public boolean canConnect(VNode from, VNode to) {
        return getLowestCommonAncestor(from, to) != null;
    }

    @Override
    public HViewCursor createCursor() {
        return new HViewCursor(this, graph.getRoot());
    }

    
     
    private void addWire(VNode ownerNode, IWire wire) {
        
        VPort from = null;
        VPort to = null;

        for(VPort portNode : instanceToPort.get(wire.getFrom().getIdentity())) {
            if(getParent(getOwner(portNode)) == ownerNode) from = portNode;
        }

        for(VPort portNode : instanceToPort.get(wire.getTo().getIdentity())) {
            if(getParent(getOwner(portNode)) == ownerNode) to = portNode;
        }

        if(from != null && to != null) {           
            VEdge edge = new VEdge(from, to, SignalEdgeFactory.wiredTo(
                        "wire_" + graph.edgeSet().size(), 
                        from.getData(),
                        to.getData(),
                        wire), model);
            addEdge(edge);
        }
    }
    
    private void removeWire(VNode ownerNode, IWire wire) {
        Set<VEdge> wires = wireSet.get(wire.getIdentity());
        for(VEdge edge : wires)
            removeEdge(edge);
    }

    @Override
    public Collection<VEdge> incomingEdges(VNode node) {
        List<VEdge> edges = new ArrayList<>();

        for(VEdge o : graph.incomingEdgesOf(node)) {
            edges.add(o);
        }

        return edges;
 
    }

    @Override
    public Collection<VEdge> outgoingEdges(VNode node) {
        List<VEdge> edges = new ArrayList<>();
        
        for(VEdge o : graph.outgoingEdgesOf(node)) {
            edges.add(o);
        }
        
        return edges;
    }

    @Override
    public int getDepth(VNode node) {
        return graph.getDepth(node);
    }

    @Override
    public Collection<VEdge> incomingEdges(VNode node, ModelEdgeType type) {
        List<VEdge> edges = new ArrayList<>();
        for(VEdge edge : incomingEdgesOf(node))
            if(edge.getData().getType() == type) edges.add(edge);
        return edges;
    }

    @Override
    public Collection<VEdge> outgoingEdges(VNode node, ModelEdgeType type) {
        List<VEdge> edges = new ArrayList<>();
        for(VEdge edge : outgoingEdgesOf(node))
            if(edge.getData().getType() == type) edges.add(edge);
        return edges;
    }
    
    @Override
    public void expand(VNode node) {
        
        if(!(node.getData() instanceof EntityInstance))
            throw new IllegalArgumentException("expand() cannot expand " + node.getData());
        
        if(expandedNodes.contains(node))
            return;

        int index = 0;
        EntityInstance instance = (EntityInstance)node.getData();     
        
        
        for(PortRecord portInstance : instance.getPorts()) {
              //  addPort(new VPort(portInstance, model), node);
            }
        
        for(EntityInstance child :  instance.getValue().getChildren()) {
            VNode childNode = new VNode(child, model);
            
            // TODO this can result in multiple listeners being added to single EntityInstance... rewrite this

            addInstance(childNode, node, index++);
            
            for(PortRecord portInstance : child.getPorts()) {
                addPort(new VPort(portInstance, model), childNode);
            }
        }

        if(wiresInInstance.containsKey(node)) {
            for(IWire wire : wiresInInstance.get(node)) {
                addWire(node, wire);
            }
        }
        
        expandedNodes.add(node);
    }
    
    @Override
    public void contract(VNode node) {
        if(!expandedNodes.contains(node))
            return;

        for (VNode child : graph.getChildren(node)) {
            if(child.getData() instanceof EntityInstance) {
                contract(child);
                for(VPort portNode : getPorts(child)) {
                    removePort(portNode, child);
                }
                removeInstance(child);
                
            }                
        }
        
        for(VPort portNode : getPorts(node)) {
            removePort(portNode, node);
        }

        expandedNodes.remove(node);
    }

    @Override
    public void onMessage(WorkspaceEvent e) {
        
        // Instances
        
        if(e instanceof WorkspaceEvent.InstanceEvent) {
            
            WorkspaceEvent.InstanceEvent evt = (WorkspaceEvent.InstanceEvent) e;
            
            if(evt.getObject().getIdentity().equals(getRoot().getData().getIdentity()))
                return;
            
            Set<VNode> parentNodes = getViewNodes(evt.getSource());
            
            if(parentNodes.isEmpty())
                    return;
            
            if(e.getEventType() == EventType.ADD) {
                for(VNode parentNode : getViewNodes(evt.getSource())) {
                    
                    if(!expandedNodes.contains(parentNode)) 
                        expand(parentNode);
                    
                    VNode node = new VNode(evt.getObject(), model);
                    
                    addInstance( node,
                        parentNode, 
                        evt.getIndex());
                   
                    expand(node);
                    
                }
            }
            
            else if (e.getEventType() == EventType.REMOVE) {
                Set<VNode> viewNodes = new HashSet<>(getViewNodes(evt.getObject()));
                for(VNode viewNode : viewNodes) {
                    
                    if(expandedNodes.contains(viewNode))
                        contract(viewNode);
                    removeInstance(viewNode);
                    
                  
                }
            }    
        } 
        
        // Ports do not work....

        // Ports
        
        else if (e instanceof WorkspaceEvent.PortEvent) {
   
            WorkspaceEvent.PortEvent evt = (WorkspaceEvent.PortEvent) e;
            
            if(e.getEventType() == EventType.ADD)
                
                for(VNode ownerNode : getViewNodes(evt.getSource())) {
                    EntityInstance instance = ((EntityInstance)ownerNode.getData());
                    addPort(new VPort(instance.getPortByDefinition(evt.getObject().getIdentity()), model), ownerNode);
                }
            
            else if (e.getEventType() == EventType.REMOVE) {
                
                for(VNode ownerNode : getViewNodes(evt.getSource())) {
                    for(VPort portNode : getPorts(ownerNode)) {
                        if(portNode.getData().getValue().getIdentity().equals(evt.getObject().getIdentity())) {
                            removePort(portNode, ownerNode);
                        }
                    }
                }
            }
        }
        
        // Wires
        
        else if (e instanceof WorkspaceEvent.WireEvent) {

            WorkspaceEvent.WireEvent evt = (WorkspaceEvent.WireEvent) e;
  
            if(e.getEventType() == EventType.ADD)
                
                for(VNode node : getViewNodes(evt.getSource())) {
                    
                    if(!expandedNodes.contains(node))
                        expand(node);
                    
                    addWire(node, evt.getObject());
                }
            
            else if(e.getEventType() == EventType.REMOVE)
                
                for(VNode node : getViewNodes(evt.getSource())) {
                    removeWire(node, evt.getObject());
                }
        }   
    }*/  


}
