/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.datamodel.tree;

import java.util.ArrayList;
import java.util.List;
import uk.ac.ncl.icos.synbad.events.impl.DefaultEvent;
import uk.ac.ncl.icos.synbad.event.SBSubscriber;
import uk.ac.ncl.icos.synbad.event.SBEventSource;
import uk.ac.ncl.icos.synbad.event.SBEvent;
import uk.ac.ncl.icos.synbad.event.SBEventType;


/**
 *
 * @author owengilfellon
 */
public interface ITreeNode<T> extends SBEventSource, SBSubscriber{
    
    public void setData(T data);
    public T getData();
    
    public void setParent(ITreeNode<T> parent);
    public ITreeNode<T> getParent();
    
    public boolean setChildren(List<ITreeNode<T>> children);
    public boolean addChild(ITreeNode<T> child);
    public void addChild(int index, ITreeNode<T> child);
    public List<ITreeNode<T>> getChildren();
    
    public boolean hasSiblings();
    
    public int getHeight();
    public int getDepth();

    public ITreeNode<T> duplicate();
    
    public class ADataModelTreeNode<T> implements ITreeNode<T> {

        private T data;
        private ITreeNode<T> parent = null;
        private final List<ITreeNode<T>> children = new ArrayList<>();
        private final List<SBSubscriber> listeners = new ArrayList<>();
        private boolean expanded;

        public ADataModelTreeNode(T data) {
            this.expanded = true;
            this.data = data;
        }

        @Override
        public void setData(T data) {
            this.data = data;
            alert(new DefaultEvent<>(SBEventType.MODIFIED, this, data));
        }

        @Override
        public T getData() {
            return data;
        }

        @Override
        public void setParent(ITreeNode<T> parent) {
            
            boolean removeFromParent = getParent() != null && getParent().getChildren().contains(this);
            boolean addToParent = parent != null && !parent.getChildren().contains(this);
            
            if(removeFromParent)
                getParent().getChildren().remove(this);
            if(addToParent)
                parent.addChild(this);
            
            this.parent = parent;
        }

        @Override
        public ITreeNode<T> getParent() {
            return parent;
        }

        @Override
        public boolean setChildren(List<ITreeNode<T>> children) {
            children.clear();
            for(ITreeNode<T> child : children) {
                addChild(child);
            }
           
            return true;
        }

        @Override
        public boolean addChild(ITreeNode<T> child) {
            boolean b = children.add(child);
            child.setParent(this);
            child.addChangeListener(this);
            alert(new DefaultEvent<>(SBEventType.ADDED, this, child));
            return b;
        }

        @Override
        public void addChild(int index, ITreeNode<T> child) {
            children.add(index, child);
            child.setParent(this);
            child.addChangeListener(this);
            alert(new DefaultEvent<>(SBEventType.ADDED, this, child));
        }

        @Override
        public List<ITreeNode<T>> getChildren() {
      
            return children;

        }

        @Override
        public int getHeight() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public int getDepth() {
            int depth = 0;
            ITreeNode<T> parent = getParent();
            while(parent != null) {
                depth++;
                parent = parent.getParent();
            }
            return depth;
        }


        @Override
        public ITreeNode<T> duplicate() {
            ITreeNode<T> duplicate = new ADataModelTreeNode<>(data);
            //duplicate.setData(data);
            duplicate.setChildren(children);
            duplicate.setParent(parent);
            return duplicate;
        }

        @Override
        public boolean hasSiblings() {
            if (parent == null) {
                return false;
            }
            return parent.getChildren().size() > 1;
        }
        
        public boolean getIsExpanded() {
            return expanded;
        }
        
        public void setIsExpanded(boolean expanded) {
            this.expanded = expanded;
        }

        @Override
        public boolean addChangeListener(SBSubscriber listener) {
            return listeners.add(listener);
        }

        @Override
        public boolean removeChangeListener(SBSubscriber listener) {
            return listeners.remove(listener);
        }

        @Override
        public void clearChangeListeners() {
            listeners.clear();
        }
        
        public void alert(SBEvent event)
        {
            for(SBSubscriber listener  : listeners) {
                listener.onEvent(event);
            }
        }

        @Override
        public void onEvent(SBEvent e) {
            alert(e);
        }
    }
}
