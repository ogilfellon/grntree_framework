/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.datamodel.graph.impl;


import uk.ac.ncl.icos.synbad.h.graph.api.HGraphListener;
import uk.ac.ncl.icos.synbad.h.graph.api.GraphListener;
import uk.ac.ncl.icos.synbad.h.graph.api.SBHIterator;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import uk.ac.ncl.icos.synbad.h.graph.api.SBHGraph;
import uk.ac.ncl.icos.synbad.event.SBEventType;
import uk.ac.ncl.icos.synbad.graph.api.SBEdge;
import uk.ac.ncl.icos.synbad.graph.api.SBObject;
import uk.ac.ncl.icos.synbad.h.graph.api.SBCompoundGraph;

/**
 * 
 * @author owengilfellon
 */
public class DefaultHGraph<N extends SBObject, E extends SBEdge> implements SBHGraph<N, E> {

    private final SBCompoundGraph<N, E> graph;
    private final List<HGraphListener<N, E>> listeners;
    private final Map<N, NodeRecord<N>> nodeRecords;
    private final Map<E, EdgeRecord> edgeRecords;

    public DefaultHGraph(N root) {
        this.graph = new DefaultCompoundGraph<>(root);
        this.listeners = new ArrayList<>();
        this.nodeRecords = new HashMap<>();
        this.edgeRecords = new HashMap<>();
        registerNode(new NodeRecord<>(root));
    }
    
    // =========================================================================
    //                Internal record management
    // =========================================================================  
    
  
    private void registerNode(NodeRecord<N> record) {
        if(!nodeRecords.containsKey(record.getNode()))
            nodeRecords.put(record.getNode(), record);
    }
    
    private void deregisterNode(N node) {
        if(nodeRecords.containsKey(node))
            nodeRecords.remove(node);
    }
    
    private void registerEdge(EdgeRecord record) {
        if(!edgeRecords.containsKey(record.getEdge()))
            edgeRecords.put(record.getEdge(), record);
    }
    
    private void deregisterEdge(E edge) {
        if(edgeRecords.containsKey(edge))
             edgeRecords.remove(edge);
    }
    
 
    
    // =========================================================================
    //                         Nodes
    // =========================================================================   
    
    @Override
    public boolean removeNode(N node) {
        
        if(node == null)
            throw new NullPointerException("Node cannot be null");

        if(!containsNode(node))
            return false;

        N parent = graph.getParent(node);
        
        if(graph.removeNode(node)) {
           deregisterNode(node);
           dispatchNodeEvent(SBEventType.REMOVED, parent, node);
        }
        
        return false;
    }

    @Override
    public boolean removeAllNodes(Collection<? extends N> nodes) {
        
        boolean changed = false;
        
        for(N node : nodes) {
            if(removeNode(node)) {
                changed = true;
            }
        }
        
        return changed;
    } 
    
    @Override
    public boolean addNode(N instance, N parent) {
        
        if(instance == null)
            throw new NullPointerException("instance cannot be null");
        if(parent == null)
            throw new NullPointerException("parent cannot be null");
        if(!containsNode(parent))
            throw new NullPointerException("parent is not in graph");
        if(containsNode(instance))
            return false;
        
        return addNode(instance, parent, graph.getChildren(parent).size());
    }

       @Override
    public boolean containsNode(N node) {
        return graph.containsNode(node) && isVisible(node);
    }
    
    public boolean addNode(N instance, N parent, int index) {

        if(instance == null)
            throw new NullPointerException("instance cannot be null");
        if(parent == null)
            throw new NullPointerException("parent cannot be null");
        if(!containsNode(parent))
            throw new NullPointerException("parent is not in graph");
        if(containsNode(instance))
            return false;
        
        
        if(!graph.addNode(instance, parent, index))
            return false;
        
        registerNode(new NodeRecord<>(instance));
        dispatchNodeEvent(SBEventType.ADDED, parent, instance);
        
        return true;
    }
    
    
    // =========================================================================
    //                                Edges
    // =========================================================================    
    
    @Override
    public Set<E> edgeSet() {
        return edgeRecords.values().stream()
                .filter(e -> isEdgeVisible(e.getEdge()))
                .filter(e -> !isProxyEdge(e.getEdge()))
                .map(EdgeRecord::getEdge).collect(Collectors.toSet());
    }
    
    @Override
    public Set<E> proxyEdgeSet() {
         return edgeRecords.values().stream()
                 .filter(e -> isEdgeVisible(e.getEdge()))
                 .filter(e -> isProxyEdge(e.getEdge()))
                 .map(EdgeRecord::getEdge).collect(Collectors.toSet());
    }

    @Override
    public boolean containsEdge(E edge) {
        return graph.edgeSet().contains(edge);
    }
    
    @Override
    public boolean containsEdge(N from, N to) {
        return graph.containsEdge(from, to) && isEdgeVisible(graph.getEdge(from, to));
    }

    @Override
    public boolean addEdge(N from, N to, E edge) {
        
        if(!graph.addEdge(from, to, edge))
            return false;
        registerEdge(new EdgeRecord(edge));
        
        // resolve source and target as required...

        edgeRecords.get(edge).resolveSource();
        edgeRecords.get(edge).resolveTarget();
        
        // dispatch event
        
        dispatchEdgeEvent(SBEventType.ADDED, edge);
        
        return true;
    }
    
    @Override
    public boolean removeEdge(E edge) {
        if(!graph.removeEdge(edge)) 
            return false;

        deregisterEdge(edge);
        
        dispatchEdgeEvent(SBEventType.REMOVED, edge);
        
        return true;
    }
    
    @Override
    public List<E> incomingEdges(N node) {

        if(!isVisible(node))
            return new ArrayList<>();
        
        return nodeRecords.get(node).getResolvedIncoming().stream()
                .filter(this::isEdgeVisible)
                .filter(e -> !isProxyEdge(e))
                .collect(Collectors.toList());
    }

    @Override
    public List<E> outgoingEdges(N node) {
        if(!isVisible(node))
            return new ArrayList<>();
        
        return nodeRecords.get(node).getResolvedOutgoing().stream()
                .filter(this::isEdgeVisible)
                .filter(e -> !isProxyEdge(e))
                .collect(Collectors.toList());
    }
    
    @Override
    public List<E> incomingEdges(N node, String predicate) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<E> outgoingEdges(N node, String predicate) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    

    
    // =========================================================================
    //                               Proxy Edges
    // =========================================================================  
    
    @Override
    public boolean isProxyEdge(E edge) {

        boolean sourceProxied = !graph.getEdgeSource(edge).equals(
                edgeRecords.get(edge).getResolvedSource());
        boolean targetProxied = !graph.getEdgeTarget(edge).equals(
                edgeRecords.get(edge).getResolvedTarget());

       return sourceProxied || targetProxied;

    }
    
    @Override
    public List<E> incomingProxyEdges(N node) {
        
        if(!isVisible(node))
            return new ArrayList<>();
        // any edge records whose targets resolve to node

        return nodeRecords.get(node).getResolvedIncoming().stream()
                .filter(this::isEdgeVisible)
                .filter(this::isProxyEdge)
                .collect(Collectors.toList());
       
    }

    @Override
    public List<E> outgoingProxyEdges(N node) {
   
        if(!isVisible(node))
            return new ArrayList<>();
        
        return nodeRecords.get(node).getResolvedOutgoing().stream()
                .filter(this::isEdgeVisible)
                .filter(this::isProxyEdge)
                .collect(Collectors.toList());
    }
    
    @Override
    public N getEdgeSource(E edge) {
        return edgeRecords.get(edge).getResolvedSource();
    }
    
    @Override
    public N getEdgeTarget(E edge) {
        return edgeRecords.get(edge).getResolvedTarget();
    }
    
    
    @Override
    public N getOriginalEdgeSource(E edge) {
        return graph.getEdgeSource(edge);
    }

    @Override
    public N getOriginalEdgeTarget(E edge) {
        return graph.getEdgeTarget(edge);
    }
    
    
    // =========================================================================
    //                               Hierarchy
    // =========================================================================  
    
    
    @Override
    public N getRoot() {
        return graph.getRoot();
    }
    
    @Override
    public boolean hasSiblings(N node) {
        return graph.hasSiblings(node);
    }

    @Override
    public List<N> getSiblings(N node) {
        return graph.getSiblings(node);
    }
    
    @Override
    public boolean isAncestor(N node, N ancestor) {
        return graph.isAncestor(node, ancestor);
    }
    
    private N lowestVisibleAncestor(N node) {

        // Stores current iterated node as we ascend the hierarchy
        
        N currentRecord = node;
       
        // Stores lowest visible ancestor
        
        N lowestVisibleAncestor = currentRecord;
        
        // Traverse the hierarchy
        
        while(graph.getParent(currentRecord) != null){
            currentRecord = graph.getParent(currentRecord);
            
            if(nodeRecords.get(currentRecord).isRetracted())
                lowestVisibleAncestor = currentRecord;
        }
        
        return lowestVisibleAncestor;
    }
    
    private boolean isEdgeVisible(E edge) {
        
        // if both source and targets resolve to same node, then no

        EdgeRecord r = edgeRecords.get(edge);
        return !r.getResolvedSource().equals(r.getResolvedTarget());
        
    }   

    @Override
    public boolean isVisible(N node) {

        N currentRecord = node;
        
        // Traverse hierarchy
        
        while(graph.getParent(currentRecord) != null){
            currentRecord = graph.getParent(currentRecord);
            
            // If an ancestor is retracted, node is not visible
            
            if(nodeRecords.get(currentRecord).isRetracted())
                return false;
        }
        
        return true;
    }

    @Override
    public void expand(N node) {
        
        NodeRecord<N> record = nodeRecords.get(node);

        if(!isVisible(record.getNode()) || !record.isRetracted())
            return;
        
        record.setIsRetracted(false); 

        // Resolve any proxy edges to their's node's new lowest visible ancestor

        Iterator<N> i = graph.hierarchyIterator(node);
        Set<E> edgesToResolve = new HashSet<>();

        while(i.hasNext()) {
            N n = i.next(); 
            edgesToResolve.addAll(graph.getAllEdges(n)); 
        }

        record.setIsRetracted(false);
        
        edgesToResolve.stream().forEach(this::resolveEdge);
        
        // Dispatch event for any descendants that are now visible
        
        for(N descendant : graph.getDescendants(node)) {
            if(isVisible(descendant)) {
                dispatchNodeEvent(SBEventType.ADDED, getParent(descendant), descendant);
            }
        }
    }
    
    @Override
    public void contract(N node) {
        
        //System.out.println("Contracting " + node);
        
        NodeRecord<N> record = nodeRecords.get(node);
        
        if(!isVisible(record.getNode()) || record.isRetracted())
            return;

        // Gather all edges to resolve;
        
        Set<E> edgesToResolve = new HashSet<>();
        Set<N> nodesToRemove = new HashSet<>();
        
        for(N descendant : graph.getDescendants(node)) {
            if(isVisible(descendant))
                nodesToRemove.add(descendant);

            if(isLeaf(descendant))
                edgesToResolve.addAll(graph.getAllEdges(descendant)); 
        }
        
        nodesToRemove.stream().forEach(
            n -> {dispatchNodeEvent(SBEventType.REMOVED, getParent(n), n);}
        );

        record.setIsRetracted(true);
        
        edgesToResolve.stream().forEach(this::resolveEdge);
    }
    
    private void resolveEdge(E e) {
        EdgeRecord r = edgeRecords.get(e);
            N oldResolvedSource = r.getResolvedSource();
            N oldResolvedTarget = r.getResolvedTarget();
            if(lowestVisibleAncestor(graph.getEdgeSource(e)) != oldResolvedSource ||
                lowestVisibleAncestor(graph.getEdgeTarget(e)) != oldResolvedTarget) {
                if(isEdgeVisible(e))
                   dispatchProxyEdgeEvent(SBEventType.REMOVED, e);
                r.resolveTarget();
                r.resolveSource();
                if(isEdgeVisible(e))
                    dispatchProxyEdgeEvent(SBEventType.ADDED, e);                
            }
        
    }

    @Override
    public boolean isContracted(N node) {
        return nodeRecords.get(node).isRetracted();
    }
    
    @Override
    public void setParent(N parent, N node) {
        N currentParent = graph.getParent(node);
        graph.setParent(parent, node);
        dispatchNodeEvent(SBEventType.REMOVED, currentParent, node);
        dispatchNodeEvent(SBEventType.ADDED, parent, node);
        graph.getAllEdges(node).stream().forEach(e -> {
            EdgeRecord edge =  edgeRecords.get(e);
            edge.resolveSource();
            edge.resolveTarget();
        });
    }
    
    @Override
    public N getParent(N instance) {
        if(instance == null)
            throw new NullPointerException("instance cannot be null");
        
        if(!containsNode(instance))
            throw new NullPointerException(instance + " not in graph");
        
        return graph.getParent(instance);
    }

    @Override
    public List<N> getChildren(N instance) {
        return graph.getChildren(instance);
    }
    
    public boolean isLeaf(N node) {
        return getChildren(node).isEmpty();
    }

    @Override
    public boolean isDescendant(N ancestor, N descendant) {
        
        if(ancestor == null)
            throw new NullPointerException("ancestor cannot be null");
        
        if(descendant == null)
            throw new NullPointerException("descendant cannot be null");
        
        return graph.isDescendant(ancestor, descendant);
    }
    

    @Override
    public List<N> getDescendants(N instance) {
        return graph.getDescendants(instance);
    }


    public List<N> getVisibleDescendants(N instance) {
        return graph.getDescendants(instance).stream()
                .filter(this::isVisible).collect(Collectors.toList());
    }
  
    @Override
    public int getDepth(N node) {
        return graph.getDepth(node);
    }
    
    // =========================================================================
    //                               Iteration
    // =========================================================================  



    public SBHIterator<N> visibleIterator(N instance) {
        return new HierarchyIterator(this, instance);
    }
    
    
    @Override
    public SBHIterator<N> hierarchyIterator(N instance) {
        return graph.hierarchyIterator(instance);
    }
    
    // =========================================================================
    //                          Event dispatch
    // =========================================================================  
    
    public void dispatchNodeEvent(SBEventType type, N parent, N node) {
        for(GraphListener<N, E> listener : listeners) {
            if(listener instanceof HGraphListener)
                ((HGraphListener)listener).onEntityEvent(type, node, parent);
        }
    }
    
    public void dispatchEdgeEvent(SBEventType type, E edge) {
        for(GraphListener<N, E> listener : listeners) {
            listener.onEdgeEvent(type, edge);
        }
    }
    
    public void dispatchProxyEdgeEvent(SBEventType type, E edge) {
        for(HGraphListener<N, E> listener : listeners) {
            listener.onProxyEdgeEvent(type, edge);
        }
    }
    
    // =========================================================================
    //                          Graph listeners
    // =========================================================================  
    
    public void addListener(HGraphListener<N, E> listener) {
        listeners.add(listener);
    }
    
    public void removeListener(HGraphListener<N, E> listener) {
        listeners.remove(listener);
    }





    // =========================================================================
    //             Internal classes for storing node and edge info
    // =========================================================================  
    
    private final class NodeRecord<T> {
        
        private final T node;
        private boolean isRetracted;
        private final Set<E> resolvedIncoming;
        private final Set<E> resolvedOutgoing;
 
        public NodeRecord(T node) {
            this.node = node;
            this.isRetracted = false;
            this.resolvedIncoming = new HashSet<>();
            this.resolvedOutgoing = new HashSet<>();
        }

        public T getNode() {
            return node;
        }

        public boolean isRetracted() {
            return isRetracted;
        }

        public void setIsRetracted(boolean isRetracted) {
            this.isRetracted = isRetracted;
        }
        
        public void addResolvedIncoming(E edge) {
            this.resolvedIncoming.add(edge);
        }
        
        public void removeResolvedIncoming(E edge) {
            this.resolvedIncoming.remove(edge);
        }
        
        public void addResolvedOutgoing(E edge) {
            this.resolvedOutgoing.add(edge);
        }
        
        public void removeResolvedOutgoing(E edge) {
            this.resolvedOutgoing.remove(edge);
        }

        public Set<E> getResolvedIncoming() {
            return resolvedIncoming;
        }

        public Set<E> getResolvedOutgoing() {
            return resolvedOutgoing;
        }
        
        @Override
        public boolean equals(Object obj) {
            if(obj == null)
                return false;
            if(obj == this)
                return true;
            if(!(obj instanceof NodeRecord))
                return false;
            
            NodeRecord nodeRecord = (NodeRecord) obj;
            return nodeRecord.getNode().equals(this.getNode());
        }

        @Override
        public int hashCode() {
            return getNode().hashCode();
        }
    }
    
    private final class EdgeRecord {
        
        private final E edge;
        private N resolvedSource;
        private N resolvedTarget;

        public EdgeRecord(E edge) {
            this.edge = edge;
            this.resolvedSource = graph.getEdgeSource(edge);
            this.resolvedTarget = graph.getEdgeTarget(edge);
        }

        public E getEdge() {
            return edge;
        }

        public N getResolvedSource() {
            return resolvedSource;
        }

        public N getResolvedTarget() {
            return resolvedTarget;
        }

        public void resolveSource() {
            nodeRecords.get(getResolvedSource()).removeResolvedOutgoing(edge);
            this.resolvedSource  = lowestVisibleAncestor(graph.getEdgeSource(edge));
            nodeRecords.get(getResolvedSource()).addResolvedOutgoing(edge);
        }

        public void resolveTarget() {
            nodeRecords.get(getResolvedTarget()).removeResolvedIncoming(edge);
            this.resolvedTarget  = lowestVisibleAncestor(graph.getEdgeTarget(edge));
            nodeRecords.get(getResolvedTarget()).addResolvedIncoming(edge);
        }

        @Override
        public String toString() {
            return "ER: " + edge + "\n   rSource: " + resolvedSource + " rTarget: " + resolvedTarget;
        }
    }
    
    // =========================================================================
    //                          Iterator
    // =========================================================================  

    public final class HierarchyIterator implements SBHIterator<N> {

        private final LinkedList<N> list = new LinkedList<>();
        private final Set<N> explored = new HashSet<>();
        private final N rootNode;
        private N currentNode = null;
        private final DefaultHGraph<N, E> graph;
        
        private int index = -1;
        private int depth = 0;

        public HierarchyIterator(DefaultHGraph<N, E> graph, N rootNode) {
            this.rootNode = rootNode;
            list.add(rootNode);
            this.graph = graph;
        }

        @Override
        public int getDepth()
        {
            if( currentNode == null)
                return 0;
            return graph.getDepth(currentNode);
        }

        @Override
        public boolean hasNext() {

            if(currentNode == null && rootNode != null){
                return true;
            }

            if(currentNode!=null) {
     
                
                if(!graph.getChildren(currentNode).isEmpty() &&
                        !explored.containsAll(graph.getChildren(currentNode)) &&
                        !isContracted(currentNode)){
                    return true;
                }

                if(graph.hasSiblings(currentNode)) {
                    if(!explored.containsAll(graph.getSiblings(currentNode))){
                        return true;
                    }     
                }

                if(currentNode.equals(rootNode)) {
                    return false;
                }

                N tempNode = currentNode;
                
                while(graph.getParent(tempNode)!=null && !tempNode.equals(rootNode)) {
                    tempNode = graph.getParent(tempNode);
                    if(!explored.containsAll(graph.getChildren(tempNode))) {
                        return true;
                    }
                }  
                
                
            }
            
            

            return false;
        }

        @Override
        public N next() {

            // If we dont have a current node, set current node to root node
            
            if(currentNode == null && rootNode != null){
                currentNode = rootNode;
                
                if(!explored.contains(currentNode)) {
                    list.add(currentNode);
                    explored.add(currentNode);
                    index++;
                }

                return currentNode;
            }
            
            // If we have a current node

            if(currentNode!=null)
            {
                
                // first, if current node is unretracted, navigate through
                // unexplored children
                
                if(!graph.getChildren(currentNode).isEmpty() &&
                        !explored.containsAll(graph.getChildren(currentNode)) &&
                        !isContracted(currentNode)){
                    ListIterator<N> it = graph.getChildren(currentNode).listIterator();
                    index++;
                    
                    while(it.hasNext())
                    {
                        N n = it.next();

                        if(!explored.contains(n))
                        {
                            currentNode = n;
                            list.add(currentNode);
                            explored.add(currentNode);
                            depth++;
                            
                            return n;
                        }
                    }
                }
                
                // if there are no unexplored children, then move on to siblings

                if(graph.hasSiblings(currentNode))
                {
                    if(!explored.containsAll(graph.getSiblings(currentNode))){
                        ListIterator<N> it = graph.getSiblings(currentNode).listIterator();
                        while(it.hasNext())
                        {
                            N n = it.next();

                            if(!explored.contains(n))
                            {
                                currentNode = n;
                                list.add(currentNode);
                                explored.add(currentNode);
                                index++;
                                return n;
                            }
                        }
                    }                   
                }
             
                // if there are no siblings, then retrace hierarchy to nearest
                // ancestor with unexplored children
                
               N tempNode = currentNode;

                while(graph.getParent(tempNode) !=null && !tempNode.equals(rootNode)){

                    tempNode = graph.getParent(tempNode);


                    if(!explored.containsAll(graph.getChildren(tempNode))) {
                        depth--;
                        ListIterator<N> it = graph.getChildren(tempNode).listIterator();
                        while(it.hasNext())
                        {
                            N n = it.next();

                            if(!explored.contains(n))
                            {
                                currentNode = n;
                                list.add(currentNode);
                                explored.add(currentNode);
                                index++;
                                return n;
                            }
                        }
                    }
                }
            }

            return null;
        }

        @Override
        public boolean hasPrevious() {
            return index > 0;
        }

        @Override
        public N previous() {
            index--;
            return list.get(index);
        }

        @Override
        public int nextIndex() {
            return index + 1;
        }

        @Override
        public int previousIndex() {
            return index - 1;
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public void set(N e) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public void add(N e) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }
}
