/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.datamodel.actions;

import java.util.ArrayList;
import uk.ac.ncl.icos.datamodel.workspace.api.SBRdfService;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import uk.ac.ncl.icos.synbad.event.SBEventType;
import uk.ac.ncl.icos.datamodel.workspace.api.SBDispatcher;
import uk.ac.ncl.icos.datamodel.workspace.api.SBEvents.StatementEvent;
import uk.ac.ncl.icos.datamodel.workspace.api.rdf.Statement;
import uk.ac.ncl.icos.datamodel.workspace.api.rdf.Statements;

/**
 *
 * @author owengilfellon
 */
public class StatementsAction extends SynBadCommand {

    private final CommandType type;
    private final Statements statements;

    public StatementsAction(CommandType type, Statements statements) {
        this.type = type;
        this.statements = statements;
    }
    
    public CommandType getType() {
        return type;
    }

    public List<Statement> getStatements() {
        return statements;
    }
 
    @Override
    public void perform(SBRdfService service) {
        if(type == CommandType.ADD)
            service.addStatements(statements);
        else if (type == CommandType.REMOVE)
            service.removeStatements(statements);
    }
    
    @Override
    public void undo(SBRdfService service) {
        if(type == CommandType.REMOVE)
            service.addStatements(statements);
        else if (type == CommandType.ADD)
            service.removeStatements(statements);
    }
    
    public  SBEventType getEventTypeFromCommandType(CommandType type) {
        switch(type) {
            case ADD:
                return SBEventType.ADDED;
            case MODIFY:
                return SBEventType.MODIFIED;
            case REMOVE:
                return SBEventType.REMOVED;
        }
        return null;     
    }
    
    
    @Override
    public void onPerformed(SBDispatcher dispatcher) {
        dispatcher.publish(new StatementEvent(
                getEventTypeFromCommandType(type),
                dispatcher.getWorkspace().getRepositoryId(),
                statements
        ));
    }
    
    public static class SetStatement extends SynBadCommand {

        private final Statements statements;
        private final List<StatementsAction> actions;
        protected List<Statement> removedStatements;
   
        
        public SetStatement(Statements statements) {
            this.actions = new ArrayList<>();
            this.statements = statements;
        }

        @Override
        public void perform(SBRdfService service) {
            if(actions.isEmpty()) {

                removedStatements = statements.stream()
                    .map(s -> service.getStatements(s.getSubject(), s.getPredicate(), null))
                    .filter(s-> !s.isEmpty())
                    .flatMap(s -> s.stream())
                    .distinct().collect(Collectors.toList());

                actions.add(new StatementsAction( CommandType.REMOVE, new Statements(removedStatements)));
            }
                
            actions.add(new StatementsAction(CommandType.ADD, statements));
            actions.stream().forEach(a -> a.perform(service));
        }

        @Override
        public void undo(SBRdfService service) {
            List<StatementsAction> reversed = new LinkedList<>(actions);
            Collections.reverse(reversed);
            reversed.stream().forEach(a -> a.undo(service));
        }

        public Statements getStatements() {
            return statements;
        }
        
        
        @Override
        public void onPerformed(SBDispatcher dispatcher) {
            actions.stream().forEach((SynBadCommand a) -> a.onPerformed(dispatcher));
        }
        

    }
}
