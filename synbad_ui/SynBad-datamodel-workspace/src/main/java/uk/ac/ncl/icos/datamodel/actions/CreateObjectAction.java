/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.datamodel.actions;

import java.net.URI;
import java.util.Arrays;
import java.util.stream.Collectors;
import uk.ac.ncl.icos.datamodel.workspace.api.SBDispatcher;
import uk.ac.ncl.icos.datamodel.workspace.api.SBObjectFactory;
import uk.ac.ncl.icos.datamodel.workspace.api.SBRdfService;
import uk.ac.ncl.icos.datamodel.workspace.api.rdf.Statement;
import uk.ac.ncl.icos.datamodel.workspace.api.rdf.Statements;
import uk.ac.ncl.icos.datamodel.workspace.objects.Identity;
import uk.ac.ncl.icos.datamodel.workspace.objects.SynBadObject;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms2;
import uk.ac.ncl.icos.synbad.graph.api.SBValue;

/**
 *
 * @author owengilfellon
 */
public abstract class CreateObjectAction<T extends SynBadObject> extends SynBadCommand {

    private final SynBadCommand actions;
    private final URI objectIdentity;
    private final URI parentIdentity;

    public CreateObjectAction(Identity identity, String rdfType, Statements statements, URI parentIdentity) {
        this.objectIdentity = identity.getIdentity();
        this.parentIdentity = parentIdentity;
        Statements filteredStatements = 
            new Statements(statements.stream().filter(
                s -> !s.getPredicate().equals(SynBadTerms2.Rdf.hasType))
            .collect(Collectors.toList()));
        filteredStatements.add(new Statement(identity.getIdentity().toASCIIString(),
                    SynBadTerms2.Rdf.hasType,
                    new SBValue(rdfType)));
        this.actions = new SynBadCommand.SynBadCommandMacro(Arrays.asList(SBObjectFactory.IdentityFactory.createIdentifiedAction(identity),
            new StatementsAction(SynBadCommand.CommandType.ADD,filteredStatements)));
    }

    public CommandType getType() {
        return CommandType.ADD;
    }

    @Override
    public void perform(SBRdfService service) {
        actions.perform(service);
    }

    @Override
    public void undo(SBRdfService service) {
        actions.undo(service);
    }

    @Override
    public void onPerformed(SBDispatcher dispatcher) {
        actions.onPerformed(dispatcher);
    }

    public URI getObjectIdentity() {
        return objectIdentity;
    }

    public URI getParentIdentity() {
        return parentIdentity;
    }
    
    public boolean hasParent() {
        return parentIdentity != null;
    }

}
