/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.datamodel.workspace.api.rdf;

import uk.ac.ncl.icos.synbad.graph.api.SBValue;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.function.BiFunction;
import org.openide.util.Exceptions;
import uk.ac.ncl.icos.datamodel.workspace.api.SBRdfService;
import uk.ac.ncl.icos.datamodel.workspace.objects.Identity;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms2;
import uk.ac.ncl.icos.datamodel.workspace.objects.SBValueProvider;

/**
 *
 * @author owengilfellon
 */

public interface EntityConstructor<T> extends BiFunction<String, SBRdfService, T> {

     public static class DefaultConstructor<T> implements EntityConstructor<T> {

        private final Class<T> clazz;
        
        public DefaultConstructor(Class<T> clazz) {
            this.clazz = clazz;
        }
        
        @Override
        public T apply(String t, SBRdfService u) {
            
  
            try {
                Identity identity = new IdentityConstructor().apply(t, u);

                SBValueProvider provider = new DefaultValueProvider(u.getWorkspaceId());
                
                u.getStatements(t, null, null).stream()
                   /* .filter((Statement s) -> {
                        return !s.getObject().isURI() ? 
                                true :
                                !Lookup.getDefault().lookupAll(SBObjectFactory.class).stream()
                                    .anyMatch(f ->  f.isEntity(
                                            s.getObject().asURI().toASCIIString(), u));
                    })*/
                        .forEach(s -> provider.addValue(s.getPredicate(), s.getObject()));

                
                T obj = clazz.getConstructor(Identity.class, URI.class, SBValueProvider.class).newInstance(identity, u.getWorkspaceId(), provider);
                
                if(obj == null)
                    System.err.println("No obj for " + t);
                
                return obj;
            } catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                Exceptions.printStackTrace(ex);
                return null;
            }
        }
    }
    
    public static class IdentityConstructor implements EntityConstructor<Identity> {

        @Override
        public Identity apply(String t, SBRdfService u) {
            
            

            String prefix = "";
            try {
                URI uri1 = new URI(t);
                prefix = uri1.getScheme() + "://" + uri1.getHost();
            } catch (URISyntaxException ex) {
                Exceptions.printStackTrace(ex);
            }

            SBValue displayId = u.getValue( t,
                SynBadTerms2.SbolIdentified.hasDisplayId);

            SBValue  version = u.getValue( t,
                SynBadTerms2.SbolIdentified.hasVersion);

            if(displayId != null && version != null) {
                String id = t.replaceAll(prefix+"/", "").replaceAll(prefix+"#", "").replaceAll("/"+version, "");
                return new Identity(prefix, id, version.asString());
            }
            else
                return null;
        }
    }
}
    

