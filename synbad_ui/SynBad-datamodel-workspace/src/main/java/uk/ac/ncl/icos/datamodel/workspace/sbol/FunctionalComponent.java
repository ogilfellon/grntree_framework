/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.datamodel.workspace.sbol;

import java.net.URI;
import uk.ac.ncl.icos.datamodel.workspace.objects.Identity;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms2;
import uk.ac.ncl.icos.datamodel.workspace.objects.SBValueProvider;

/**
 *
 * @author owengilfellon
 */
public class FunctionalComponent extends ComponentInstance {

    public FunctionalComponent(Identity identity, URI workspaceIdentity, SBValueProvider values) {
        super(identity, workspaceIdentity, values);
    }
    
    public SbolDirection getDirection() {
        return getValues(
            SynBadTerms2.SbolFunctionalComponent.hasDirection,
            URI.class).stream()
            .map(s -> {
                if(s.equals(URI.create("http://sbolstandard.org/v2#out")))
                   return SbolDirection.OUT;
                else if (s.equals(URI.create("http://sbolstandard.org/v2#in")))
                    return SbolDirection.IN;
                else if (s.equals(URI.create("http://sbolstandard.org/v2#inout")))
                    return SbolDirection.INOUT;
                else return SbolDirection.NONE;})
            .findFirst().orElse(null);
    }
    
     public SbolDirection getAccesss() {
        return getValues(
            SynBadTerms2.SbolFunctionalComponent.hasAccess,
            URI.class).stream()
            .map(s -> {
                if(s.equals(URI.create("http://sbolstandard.org/v2#out")))
                   return SbolDirection.OUT;
                else  
                    return SbolDirection.IN;
                })
            .findFirst().orElse(null);
    }

}
