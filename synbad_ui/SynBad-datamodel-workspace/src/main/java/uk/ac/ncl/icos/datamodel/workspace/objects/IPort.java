/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.datamodel.workspace.objects;

import java.net.URI;
import org.apache.commons.collections4.MultiValuedMap;
import uk.ac.ncl.icos.datamodel.workspace.impl.PortDirection;
import uk.ac.ncl.icos.datamodel.workspace.impl.PortState;
import uk.ac.ncl.icos.datamodel.workspace.impl.PortType;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms2;
import uk.ac.ncl.icos.synbad.graph.api.SBValue;

/**
 *
 * @author owengilfellon
 */
public abstract class IPort extends SynBadObject {
    
    public IPort(Identity identity, URI workspaceIdentity, SBValueProvider values) {
        super(identity, workspaceIdentity, values);
        
    }
    
    public SynBadObject getOwner() {
        return ws.incomingEdges(this, SynBadTerms2.SynBadEntity.hasPort)
            .stream().map(e -> ws.getObject(ws.getEdgeSource(e).getIdentity(), SynBadObject.class))
            .findFirst().orElse(null);
    }
    
    public void setIsPublic(boolean isPublic) {
        apply(new SetValueAction(getIdentity(), SynBadTerms.Port.portAccess.toString(), new SBValue(isPublic)));
    }

    public boolean isPublic() {
        return getValues(SynBadTerms2.SynBadPort.isPublic, Boolean.class)
            .stream().findFirst().orElse(false);
    }

    public abstract PortDirection getPortDirection();

    public abstract boolean isProxy();
    
    public abstract boolean isConnectable(IPort port);

    public abstract PortState getPortState();
    
    public abstract PortType getPortType();

}