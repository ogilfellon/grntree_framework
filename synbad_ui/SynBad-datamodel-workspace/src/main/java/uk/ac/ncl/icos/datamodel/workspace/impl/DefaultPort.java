/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.datamodel.workspace.impl;

import uk.ac.ncl.icos.datamodel.workspace.objects.Identity;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import java.net.URI;
import org.apache.commons.collections4.MultiValuedMap;
import uk.ac.ncl.icos.datamodel.workspace.api.SBRdfService;
import uk.ac.ncl.icos.datamodel.workspace.objects.SynBadObject;
import uk.ac.ncl.icos.datamodel.workspace.objects.IPort;
import uk.ac.ncl.icos.datamodel.workspace.api.rdf.SesameService;
import uk.ac.ncl.icos.synbad.datadefinition.IDataDefinitionManager;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms2;
import uk.ac.ncl.icos.datamodel.workspace.objects.SBValueProvider;
import uk.ac.ncl.icos.synbad.graph.api.SBValue;

/**
 *
 * @author owengilfellon
 */

public class DefaultPort extends IPort  {

    private static final IDataDefinitionManager dataDefinitionManager = new IDataDefinitionManager.DefaultDataDefinitionManager();
    
    
    public DefaultPort( Identity identity, URI workspaceIdentity, SBValueProvider values ) {
        super(identity, workspaceIdentity, values);
    }

    @Override
    public boolean isConnectable(IPort port) {
        
        // Unless port is a proxy, should not have same direction
        
        if(port.getPortDirection() == getPortDirection() && !port.isProxy())
            return false;

        if(port.getPortType() != getPortType())
            return false;
        
        if(port.getPortState() != getPortState())
            return false;

        return true;
    }

    @Override
    public PortDirection getPortDirection() {
        return getValues( SynBadTerms2.SynBadPort.hasDirection ).stream()
            .filter(v -> v.isURI())
            .filter(v -> { return dataDefinitionManager.containsDefinition(PortDirection.class, v.asURI().toASCIIString()); })
            .map(v -> { return dataDefinitionManager.getDefinition(PortDirection.class, v.asURI().toASCIIString()) ; })
            .findFirst().orElse(null);
    }
    
    
    @Override
    public PortType getPortType() {
        return getValues( SynBadTerms2.SynBadPort.hasPortType ).stream()
            .filter(v -> v.isURI())
            .filter(v -> { return dataDefinitionManager.containsDefinition(PortType.class, v.asURI().toASCIIString()); })
            .map(v -> { return dataDefinitionManager.getDefinition(PortType.class, v.asURI().toASCIIString()) ; })
            .findFirst().orElse(null);
    }

    @Override
    public PortState getPortState() {
        return getValues(SynBadTerms2.SynBadPort.hasPortState).stream()
            .filter(v -> v.isURI())
            .filter(v -> { return dataDefinitionManager.containsDefinition(PortType.class, v.asURI().toASCIIString()); })
            .map(v -> { return dataDefinitionManager.getDefinition(PortState.class, v.asURI().toASCIIString()) ; })
            .findFirst().orElse(null);
    }

    @Override
    public boolean isProxy() {
        return false;
    }
    
    @Override
    public String toString() {
        PortState state = getPortState();
        return getPortDirection() + ":" + getPortType() + (state!=null ? ":" + state : "");
    }

}