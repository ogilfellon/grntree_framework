/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.datamodel.workspace.api;

/**
 *
 * @author owengilfellon
 */
public class Source {
    
    private final String filePath;

    public Source(String filePath) {
        this.filePath = filePath;
    }

    public String getFilePath() {
        return filePath;
    }
    
}
