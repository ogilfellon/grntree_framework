/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.datamodel.workspace.sbol;

import java.net.URI;
import uk.ac.ncl.icos.datamodel.workspace.objects.Identity;
import uk.ac.ncl.icos.datamodel.workspace.objects.SBValueProvider;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms2;

/**
 *
 * @author owengilfellon
 */
public class ComponentInstance extends Pointer<ComponentDefinition> {

    public ComponentInstance(Identity identity, URI workspaceIdentity, SBValueProvider provider) {
        super(identity, workspaceIdentity, provider);
    }

    @Override
    public ComponentDefinition getDefinition() {
        return ws.outgoingEdges(this, 
                SynBadTerms2.Sbol.definedBy,
                ComponentDefinition.class).stream()
            .map(e -> ws.getObject(ws.getEdgeTarget(e).getIdentity(), ComponentDefinition.class))
            .findFirst().orElse(null);    
    }
}
