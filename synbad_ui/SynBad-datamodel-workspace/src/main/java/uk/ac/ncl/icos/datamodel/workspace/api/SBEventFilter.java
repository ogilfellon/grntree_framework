/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.datamodel.workspace.api;

import java.net.URI;
import uk.ac.ncl.icos.synbad.event.SBEvent;

/**
 *
 * @author owengilfellon
 */
public interface SBEventFilter {
    
    public boolean accept(SBEvent event);
    
    public static class DefaultFilter implements SBEventFilter {

        @Override
        public boolean accept(SBEvent event) {
            return true;
        }
    }
    
    public static class FilterById implements SBEventFilter {

        private final URI id;
        
        public FilterById(URI id) {
            this.id = id;
        }

        @Override
        public boolean accept(SBEvent event) {
            if(!(event.getSource() instanceof URI))
                return false;
            
            return ((URI)event.getSource()).equals(id);
        }
    }
}
