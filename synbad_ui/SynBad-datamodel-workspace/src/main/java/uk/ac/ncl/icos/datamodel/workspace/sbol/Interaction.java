/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.datamodel.workspace.sbol;

import java.net.URI;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import uk.ac.ncl.icos.datamodel.workspace.api.rdf.Statement;
import uk.ac.ncl.icos.datamodel.actions.StatementsAction;
import uk.ac.ncl.icos.datamodel.actions.SynBadCommand;
import uk.ac.ncl.icos.datamodel.workspace.api.rdf.Statements;
import uk.ac.ncl.icos.synbad.graph.api.SBValue;
import uk.ac.ncl.icos.datamodel.workspace.objects.SynBadObject;
import uk.ac.ncl.icos.datamodel.workspace.objects.Identity;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms2;
import uk.ac.ncl.icos.datamodel.workspace.objects.SBValueProvider;

/**
 *
 * @author owengilfellon
 */
public class Interaction extends SynBadObject implements NestedObject {
    
    private final Set<FunctionalComponent> participants = new HashSet<>();
   

    public Interaction(Identity identity, URI workspaceIdentity, SBValueProvider values) {
        super(identity, workspaceIdentity, values);
    }
    
    public Set<Participation> getParticipants() { 
        return ws.outgoingEdges(this, SynBadTerms2.SbolInteraction.hasParticipation, Participation.class).stream()    
            .map(e -> ws.getObject(ws.getEdgeTarget(e).getIdentity(), Participation.class))
            .collect(Collectors.toSet());
    }
    
    public void addParticipation(Participation participation) {
        apply(new AddParticipation(this, participation));
    }
    
    public void removeParticipation(Participation participation) {
        apply(new RemoveParticipation(this, participation));
    }


    @Override
    public void apply(SynBadCommand action) {
        if(action instanceof AddParticipation) {
            super.apply(action);
        } else if (action instanceof RemoveParticipation) {
            super.apply(action);
        }
    }
    
    // ------------------ Actions ---------------------------
    
    public static class AddParticipation extends StatementsAction {

        public AddParticipation(Interaction interaction, Participation participation) {
            super(SynBadCommand.CommandType.ADD, new Statements(Collections.singletonList(new Statement(
                    interaction.getIdentity().toASCIIString(),
                    SynBadTerms2.SbolInteraction.hasParticipation,
                    new SBValue(participation.getIdentity().toASCIIString()))))) ;
        }
    }
    
    public static class RemoveParticipation extends StatementsAction{

        public RemoveParticipation(Interaction interaction, Participation participation) {
            super(SynBadCommand.CommandType.REMOVE, new Statements(Collections.singletonList(new Statement(
                    interaction.getIdentity().toASCIIString(),
                    SynBadTerms2.SbolInteraction.hasParticipation,
                    new SBValue(participation.getIdentity().toASCIIString()))))) ;
        }
        
    }
    
    

    
    
}
