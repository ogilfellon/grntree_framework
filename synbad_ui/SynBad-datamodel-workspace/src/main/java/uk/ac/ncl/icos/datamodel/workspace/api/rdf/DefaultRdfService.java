/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.datamodel.workspace.api.rdf;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import uk.ac.ncl.icos.datamodel.workspace.api.SBRdfService;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import javax.xml.parsers.FactoryConfigurationError;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.model.impl.LinkedHashModel;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.MalformedQueryException;
import org.eclipse.rdf4j.query.QueryEvaluationException;
import org.eclipse.rdf4j.query.QueryLanguage;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.repository.RepositoryException;
import org.eclipse.rdf4j.repository.RepositoryResult;
import org.eclipse.rdf4j.repository.config.RepositoryConfig;
import org.eclipse.rdf4j.repository.config.RepositoryConfigException;
import org.eclipse.rdf4j.repository.manager.LocalRepositoryManager;
import org.eclipse.rdf4j.repository.manager.RepositoryManager;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.repository.sail.SailRepositoryConnection;
import org.eclipse.rdf4j.repository.sail.config.SailRepositoryConfig;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFHandlerException;
import org.eclipse.rdf4j.rio.RDFParseException;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.rio.rdfxml.util.RDFXMLPrettyWriter;
import org.eclipse.rdf4j.sail.memory.config.MemoryStoreConfig;
import org.eclipse.rdf4j.sail.model.SailModel;
import org.openide.util.Exceptions;

import org.sbolstandard.core2.SBOLConversionException;
import org.sbolstandard.core2.SBOLDocument;
import org.sbolstandard.core2.SBOLReader;
import org.sbolstandard.core2.SBOLValidationException;
import org.sbolstandard.core2.SBOLWriter;
import uk.ac.ncl.icos.synbad.datadefinition.UriHelper;

/**
 *
 * @author owengilfellon
 */
public class DefaultRdfService implements SBRdfService {

    private static RepositoryManager repositoryManager;
    private static final String REPOSITORY_DIRECTORY = "openRdfDb";
    private final URI workspaceId;
    private static final SailRepositoryConfig MEMORY_CONFIG = new SailRepositoryConfig(new MemoryStoreConfig());
    private final Model m;
    private final SailRepository r;

    static DefaultRdfService getService(String repositoryId) {

        try {
            
            URI workspaceId = new URI(repositoryId);
            if (repositoryManager == null) {
                repositoryManager = new LocalRepositoryManager(new File(REPOSITORY_DIRECTORY));
                repositoryManager.initialize();
                
            }
            
            if(!repositoryManager.hasRepositoryConfig(repositoryId))
                repositoryManager.addRepositoryConfig(new RepositoryConfig(repositoryId, MEMORY_CONFIG));
            
            SailRepository r = (SailRepository) repositoryManager.getRepository(repositoryId);

            if (!r.isInitialized()) {
                r.initialize();
            }

            return new DefaultRdfService(r, workspaceId);
        } catch (RepositoryException | RepositoryConfigException | URISyntaxException ex) {
            Exceptions.printStackTrace(ex);
            return null;
        } 
    }

    DefaultRdfService(SailRepository r, URI workspaceId) {
        this.r = r;
        this.m = new LinkedHashModel();
        this.workspaceId = workspaceId;
    }
    
    
    @Override
    public URI getWorkspaceId() {
        return workspaceId;
    };

    @Override
    public boolean addRdf(File xmlFile) {
        
        String id = "file:/" + xmlFile.getAbsolutePath();

        try {

            try (SailRepositoryConnection c = r.getConnection()) {
                //RDFParser parser = Rio.createParser(RDFFormat.RDFXML, ;
                c.begin();
                c.add(xmlFile, id, RDFFormat.RDFXML, c.getValueFactory().createIRI(id));
                c.commit();

                //FileInputStream input = new FileInputStream(xmlFile);
                //parser.parse(input, id);
            } catch (FileNotFoundException ex) {
                Exceptions.printStackTrace(ex);
            } catch (IOException | RDFParseException | RDFHandlerException ex) {
                Exceptions.printStackTrace(ex);
            }

            return true;
        } catch (RepositoryException ex) {
            Exceptions.printStackTrace(ex);
            return false;
        }
    }

    @Override
    public boolean addRdf(InputStream stream, URI sourceId) {

        try {
            SailRepositoryConnection c = r.getConnection();
            try {
                c.begin();
                c.add(stream, sourceId.toASCIIString(), RDFFormat.RDFXML);
                c.commit();
            } catch (RepositoryException | IOException | RDFParseException ex) {
                Exceptions.printStackTrace(ex);
                c.rollback();
            } finally {
                c.close();
            }
            
         return true;

        } catch (RepositoryException ex) {
            Exceptions.printStackTrace(ex);
            return false;
        }
    }



    @Override
    public boolean exportRdf(File f) {

        try {

            if (!f.exists()) {
                f.createNewFile();
            }

            if (!f.canWrite()) {
                f.delete();
                return false;
            }

            try {

                try (SailRepositoryConnection c = r.getConnection()) {

                    SBOLDocument document = null;
                    try {


                        ByteArrayOutputStream out = new ByteArrayOutputStream();
                        //FileOutputStream out = new FileOutputStream(f);
                        Optional<RDFFormat> format = Rio.getWriterFormatForMIMEType("application/rdf+xml");
                        //RDFWriter w = Rio.createWriter(RDFFormat.RDFXML, out);


                        c.begin();
                        Model m = new SailModel(c.getSailConnection(), true);

                        m.setNamespace(UriHelper.sbol.getPrefix(), UriHelper.sbol.getNamespaceURI());
                        m.setNamespace(UriHelper.synbad.getPrefix(), UriHelper.synbad.getNamespaceURI());
                        m.setNamespace(UriHelper.biopax.getPrefix(), UriHelper.biopax.getNamespaceURI());
                        m.setNamespace(UriHelper.dcterms.getPrefix(), UriHelper.dcterms.getNamespaceURI());
                        m.setNamespace(UriHelper.prov.getPrefix(), UriHelper.prov.getNamespaceURI());
                        m.setNamespace(UriHelper.so.getPrefix(), UriHelper.so.getNamespaceURI());
                        m.setNamespace(UriHelper.rdf.getPrefix(), UriHelper.rdf.getNamespaceURI());
                        m.setNamespace(UriHelper.rdfs.getPrefix(), UriHelper.rdfs.getNamespaceURI());
                        m.setNamespace(UriHelper.vpr.getPrefix(), UriHelper.vpr.getNamespaceURI());
                        c.commit();
                        RDFXMLPrettyWriter w = new RDFXMLPrettyWriter(out);
                        RDFXMLPrettyWriter screenWriter = new RDFXMLPrettyWriter(System.out);
                        Rio.write(m, w);
                        Rio.write(m, screenWriter);


                        document = SBOLReader.read(new ByteArrayInputStream(out.toByteArray()));
                        SBOLWriter.setKeepGoing(true);
                        SBOLWriter.write(document, f);

                    } catch (FactoryConfigurationError | SBOLValidationException | SBOLConversionException ex) {
                        Exceptions.printStackTrace(ex);
                        c.rollback();
                    }

                }

                return true;
            } catch (RepositoryException ex) {
                Exceptions.printStackTrace(ex);
                return false;
            }

        } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
            return false;
        }

    }

    @Override
    public boolean removeRdf(File f) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


    @Override
    public Set<Statement> getStatements(String subject, String predicate, uk.ac.ncl.icos.synbad.graph.api.SBValue object) {

        Set<Statement> statements = new HashSet<>();

        try {

            try (SailRepositoryConnection c = r.getConnection()) {

                StringBuilder select = new StringBuilder();
                StringBuilder where = new StringBuilder();

                if (subject == null || subject.isEmpty()) {
                    select.append(" ?subject");
                    where.append(" ?subject");
                } else {
                    where.append(" <").append(subject).append(">");
                }

                if (predicate == null || predicate.isEmpty()) {
                    select.append(" ?predicate");
                    where.append(" ?predicate");
                } else {
                    where.append(" <").append(predicate).append(">");
                }

                if (object == null) {
                    select.append(" ?object");
                    where.append(" ?object");
                } else {
                    where.append(" ").append(getSparQLValue(object));
                }

                String queryString = "SELECT " + select.toString() + " "

                        + " WHERE { " + where.toString() + " }";

                // System.out.println(queryString);
                TupleQuery tupleQuery
                        = c.prepareTupleQuery(QueryLanguage.SPARQL, queryString);
                TupleQueryResult result = tupleQuery.evaluate();
                while (result.hasNext()) {
                    BindingSet bindingSet = result.next();

                    String valueOfSubject = bindingSet.hasBinding("subject")
                            ? bindingSet.getValue("subject").stringValue()
                            : subject;

                    String valueOfPredicate = bindingSet.hasBinding("predicate")
                            ? bindingSet.getValue("predicate").stringValue()
                            : predicate;

                    uk.ac.ncl.icos.synbad.graph.api.SBValue valueOfObject = bindingSet.hasBinding("object")
                            ? parseValue(bindingSet.getValue("object").stringValue())
                            : object;

                    //System.out.println(valueOfSubject + " : " + valueOfPredicate + " : " + valueOfObject);
                    statements.add(new Statement(
                            valueOfSubject,
                            valueOfPredicate,
                            valueOfObject));
                }
            } catch (MalformedQueryException | QueryEvaluationException ex) {
                Exceptions.printStackTrace(ex);
            }

        } catch (RepositoryException ex) {
            Exceptions.printStackTrace(ex);
        }

        return statements;
    }
    
    @Override
    public Set<uk.ac.ncl.icos.synbad.graph.api.SBValue> getValues(String subject, String predicate) {
        return getStatements(subject, predicate, null).stream()
                .map(Statement::getObject)
                .collect(Collectors.toSet());

    }



    /*
     @Override
     public Set<uk.ac.ncl.icos.datamodel.workspace.api.rdf.Value> getValues(String subject, String predicate, URI... sources) {

     Set<uk.ac.ncl.icos.datamodel.workspace.api.rdf.Value> values
     = new HashSet<>();

     try {
     SailRepositoryConnection c = r.getConnection();
     try {

     String s = (subject == null || subject.isEmpty()) ? "?subject" : "<" + subject + ">";
     String p = (predicate == null || predicate.isEmpty()) ? "?predicate" : "<" + predicate + ">";

     String queryString = "SELECT ?literal  "
     + getNamedGraphs(urisToResources(c, sources))
     + " WHERE { "
     + s + " "
     + p
     + " ?literal FILTER(isLiteral(?literal))}";
     TupleQuery tupleQuery
     = c.prepareTupleQuery(QueryLanguage.SPARQL, queryString);
     TupleQueryResult result = tupleQuery.evaluate();
     while (result.hasNext()) {
     BindingSet bindingSet = result.next();
     Value valueOfLiteral = bindingSet.getValue("literal");
     values.add(parseValue(valueOfLiteral.stringValue()));
     }
     } catch (MalformedQueryException | QueryEvaluationException ex) {
     Exceptions.printStackTrace(ex);
     } finally {
     c.close();
     }
     } catch (RepositoryException ex) {
     Exceptions.printStackTrace(ex);
     }

     return values;
     }*/
    @Override
    public uk.ac.ncl.icos.synbad.graph.api.SBValue getValue(String subject, String predicate) {

        return getStatements(subject, predicate, null).stream()
                .map(Statement::getObject).findFirst().orElse(null);
    }

    @Override
    public boolean addStatements(List<Statement> statements) {
        try {
            SailRepositoryConnection c = r.getConnection();
            try {
                c.begin();
                ValueFactory f = c.getValueFactory();
                List<org.eclipse.rdf4j.model.Statement> toAdd = new ArrayList<>();

                for (Statement statement : statements) {
                    
                    Value v = getValue(statement.getObject(), f);
                    if (v == null) {
                        return false;
                    }
                    toAdd.add(f.createStatement(
                                    f.createIRI(statement.getSubject()),
                                    f.createIRI(statement.getPredicate()),
                                    v));

                }
                c.add(toAdd);

                c.commit();
                return true;
            } catch (RepositoryException ex) {
                c.rollback();
            } finally {
                c.close();
            }
        } catch (RepositoryException ex) {
            Exceptions.printStackTrace(ex);
        }
        return false;
    }

    @Override
    public boolean removeStatements(List<Statement> statements) {
        try {
            SailRepositoryConnection c = r.getConnection();
            try {
                c.begin();

                for (Statement statement : statements) {
                    ValueFactory f = c.getValueFactory();
                    Value v = getValue(statement.getObject(), f);
                    Resource r = f.createIRI(statement.getSubject());
                    org.eclipse.rdf4j.model.IRI p = f.createIRI(statement.getPredicate());
                    if (v == null) {
                        return false;
                    }

                   

                    RepositoryResult<org.eclipse.rdf4j.model.Statement> i
                            = c.getStatements(r, p, v, false);

                    if (!i.hasNext()) {
                        return false;
                    }

                    c.remove(i);
                }

                c.commit();
                return true;
            } catch (RepositoryException ex) {
                c.rollback();
            } finally {
                c.close();
            }
        } catch (RepositoryException ex) {
            Exceptions.printStackTrace(ex);
        }
        return false;
    }



    private String getSparQLValue(uk.ac.ncl.icos.synbad.graph.api.SBValue value) {

        if (value.isURI()) {
            return "<" + value.asURI().toASCIIString() + ">";
        } else if (value.isString()) {
            return "\"" + value.asString() + "\"";
        } else if (value.isBoolean() || value.isDouble() || value.isInt()) {
            return "\"" + value.getValue().toString() + "\"";
        } else {
            return null;
        }
    }

    private Value getValue(uk.ac.ncl.icos.synbad.graph.api.SBValue value, ValueFactory f) {
        if (value.isBoolean()) {
            return f.createLiteral(value.asBoolean());
        } else if (value.isDouble()) {
            return f.createLiteral(value.asDouble());
        } else if (value.isInt()) {
            return f.createLiteral(value.asInt());
        } else if (value.isString()) {
            return f.createLiteral(value.asString());
        } else if (value.isURI()) {
            return f.createIRI(value.asURI().toASCIIString());
        }

        return null;
    }

    private uk.ac.ncl.icos.synbad.graph.api.SBValue parseValue(String value) {

        if (value.equalsIgnoreCase("true") || value.equalsIgnoreCase("false")) {
            return new uk.ac.ncl.icos.synbad.graph.api.SBValue(Boolean.parseBoolean(value));
        }

        try {
            int i = Integer.parseInt(value);
            return new uk.ac.ncl.icos.synbad.graph.api.SBValue(i);
        } catch (NumberFormatException e) {

        }

        try {
            double d = Double.parseDouble(value);
            return new uk.ac.ncl.icos.synbad.graph.api.SBValue(d);
        } catch (NumberFormatException e) {

        }

        try {
            URI uri = new URI(value);
            if (uri.isAbsolute()) {
                return new uk.ac.ncl.icos.synbad.graph.api.SBValue(uri);
            }
        } catch (URISyntaxException e) {

        }

        return new uk.ac.ncl.icos.synbad.graph.api.SBValue(value);

    }

    private static String getNamedGraphs(Resource... contexts) {
        String fromNamed = "";
        for (Resource r : contexts) {
            fromNamed = fromNamed
                    .concat(" FROM ")
                    .concat("<").concat(r.stringValue()).concat("> ");
        }

        for (Resource r : contexts) {
            fromNamed = fromNamed
                    .concat(" FROM NAMED ")
                    .concat("<").concat(r.stringValue().concat("> "));
        }
        return fromNamed;
    }

    @Override
    public void shutdown() {
        try {
            r.shutDown();
        } catch (RepositoryException ex) {
            Exceptions.printStackTrace(ex);
        }
    }

}
