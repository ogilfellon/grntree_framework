/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.datamodel.workspace.impl;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 *
 * @author owengilfellon
 */
@XStreamAlias("PortDirection")
public enum PortDirection {
    IN, OUT;

    public static PortDirection fromString(String signalDirection) {
        if (signalDirection.equals("Input") || signalDirection.equals("IN") || signalDirection.equals("Modifier")) {
            return IN;
        } else if (signalDirection.equals("Output") || signalDirection.equals("OUT")) {
            return OUT;
        } 
        return null;
    }
    
}
