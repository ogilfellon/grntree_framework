/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.datamodel.workspace.objects;

import java.net.URI;
import java.util.*;
import java.util.stream.Collectors;
import org.openide.util.Lookup;
import uk.ac.ncl.icos.datamodel.workspace.api.rdf.Statement;
import uk.ac.ncl.icos.datamodel.actions.StatementsAction;
import uk.ac.ncl.icos.datamodel.actions.SynBadCommand;
import uk.ac.ncl.icos.datamodel.workspace.api.SBDispatcher;
import uk.ac.ncl.icos.datamodel.workspace.api.SBEntity;
import uk.ac.ncl.icos.datamodel.workspace.api.SBEventFilter;
import uk.ac.ncl.icos.datamodel.workspace.api.SBEvents.StatementEvent;
import uk.ac.ncl.icos.datamodel.workspace.api.SBWorkspace;
import uk.ac.ncl.icos.datamodel.workspace.api.SBWorkspaceManager;
import uk.ac.ncl.icos.datamodel.workspace.api.rdf.Statements;
import uk.ac.ncl.icos.synbad.graph.api.SBValue;
import uk.ac.ncl.icos.synbad.datadefinition.IDataDefinitionManager;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms2;
import uk.ac.ncl.icos.synbad.datadefinition.types.Role;
import uk.ac.ncl.icos.synbad.event.SBEvent;
import uk.ac.ncl.icos.synbad.event.SBEventType;
import uk.ac.ncl.icos.synbad.events.impl.DefaultEvent;

/**
 * SynBadObject is the root class for representing all domain objects within
 * SynBad. They are identified. They provide SynBadEdges, which represent
 * predicates between registered SynBadObjects.
 * 
 * @author owengilfellon
 */
public abstract class SynBadObject implements SBEntity {

    private static final IDataDefinitionManager dataDefinitionManager = new IDataDefinitionManager.DefaultDataDefinitionManager();

    private final Identity objectIdentity;
    private final URI workspaceIdentity;
    private final SBValueProvider values;
    protected final SBWorkspace ws;

    public SynBadObject(Identity identity, URI workspaceIdentity, SBValueProvider provider) {
        this.objectIdentity = identity;
        this.workspaceIdentity = workspaceIdentity;
        this.values = provider;
        this.ws = Lookup.getDefault().lookup(SBWorkspaceManager.class).getWorkspace(workspaceIdentity);
        this.ws.getDispatcher().subscribe(
                StatementEvent.class,
                (SBEvent event) -> ((StatementEvent)event).getObject().stream()
                    .anyMatch(s -> s.getSubject().equals(
                            identity.getIdentity().toASCIIString())),
                values);
    }

    @Override
    public String getPrefix() {
        return objectIdentity.getUriPrefix();
    }
    
    @Override
    public URI getIdentity() {
        return objectIdentity.getIdentity();
    }
    
    @Override
    public String getDisplayId() {
        return objectIdentity.getDisplayID();
    }
    
    @Override
    public String getVersion() {
        return objectIdentity.getVersion();
    }
    
    @Override
    public String getName() {
        SBValue v = getValue(SynBadTerms2.SbolIdentified.hasName);
        return v == null ? "" : v.asString();
    }
    
    @Override
    public URI getPersistentId() {
        return URI.create(objectIdentity.getUriPrefix() + "/" + objectIdentity.getDisplayID());
    }

    @Override
    public URI getWasDerivedFrom() {
        return getValues(SynBadTerms2.SbolIdentified.wasDerivedFrom, URI.class)
                .stream().findFirst().orElse(null);
    }

    @Override
    public String getDescription() {
        SBValue v = getValue(SynBadTerms2.SbolIdentified.hasDescription);
        return v == null ? "" : v.asString();
    }
    
    @Override
    public Collection<Role> getRoles() {
        return getValues(SynBadTerms2.Sbol.role).stream()
            .filter(s -> s.isURI())
            .filter(s -> dataDefinitionManager.containsDefinition(Role.class, s.asURI().toASCIIString()))
            .map(s-> dataDefinitionManager.getDefinition(Role.class, s.asURI().toASCIIString()))
            .collect(Collectors.toSet());
    }

    @Override
    public void setDescription(String description) {
        apply(new SetDescriptionAction(objectIdentity.getIdentity(), description));
    }

    @Override
    public void setName(String name) {
        apply(new SetNameAction(objectIdentity.getIdentity(), name));
    }

    @Override
    public void setWasDerivedFrom(URI derivedFrom) {
        apply(new SetDerivedFromAction(objectIdentity.getIdentity(), derivedFrom));
    }
    
    @Override
    public void setValue(String predicate, SBValue v) {
        apply(new SetValueAction(objectIdentity.getIdentity(), predicate, v));
    }
    
    @Override
    public void addValue(String predicate, SBValue v) {
        apply(new AddValueAction(getIdentity(), predicate, v));
    }

    @Override
    public void removeValues(String predicate) {
        getValues(predicate).stream().forEach(s -> apply(new RemoveValueAction(getIdentity(), predicate, s)));
    }
    
    @Override
    public void removeValue(String predicate, SBValue v) {
        apply(new RemoveValueAction(getIdentity(), predicate, v));
    }
    
    @Override
    public SBValue getValue(String predicate) {
        return getValues(predicate).stream().findFirst().orElse(null);
    }
    
    @Override
    public <T> Collection<T> getValues(String predicate, Class<T> clazz) {
        return values.getValues(predicate).stream().filter(v ->
            clazz.isAssignableFrom(v.getValue().getClass())
        ).map(v -> clazz.cast(v.getValue())).collect(Collectors.toSet());
    }
    
    @Override
    public Collection<SBValue> getValues(String predicate) {
        return values.getValues(predicate);
    }
    
    @Override
    public Set<String> getPredicates() {
        return values.getPredicates();
    }
    
    public void apply(SynBadCommand command) {
        ws.perform(command);
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null)
            return false;
        if(obj == this)
            return true;
        if(!(obj instanceof SynBadObject))
            return false;

        SynBadObject sbObj = (SynBadObject) obj;
        return sbObj.getIdentity().equals(objectIdentity.getIdentity());
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.objectIdentity.getIdentity());
        return hash;
    }
    
    public URI getWorkspace() {
        return workspaceIdentity;
    }
    
    // ====================================================================
    //                          Events
    // ====================================================================
    
    public static class DescriptionEvent extends DefaultEvent<URI, String> {

        public DescriptionEvent(SBEventType eventType, URI source, String description) {
            super(eventType, source, description);
        }
    }
    
    public static class NameEvent extends DefaultEvent<URI, String>{ 

        public NameEvent(SBEventType eventType, URI source, String name) {
            super(eventType, source, name);
        }
    }
    
    public static class WasDerivedFromEvent extends DefaultEvent<URI, URI> {

        public WasDerivedFromEvent(SBEventType eventType, URI source, URI derivedFrom) {
            super(eventType, source, derivedFrom);
        }
    }
    
    public static class ValueEvent extends DefaultEvent<URI, SBValue> {

        private final String predicate;
        
        public ValueEvent(SBEventType type, URI source, String predicate, SBValue value) {
            super(type, source, value);
            this.predicate = predicate;
        }

        public String getPredicate() {
            return predicate;
        }
    }

    // ====================================================================
    //                          Actions
    // ====================================================================
    
    public static class AddValueAction extends StatementsAction {
        
        private final URI source;
        private final String predicate;
        private final SBValue v;

        public AddValueAction(URI identity, String predicate, SBValue v) {
            super(CommandType.ADD, new Statements(Collections.singletonList(new Statement(
                identity.toASCIIString(),
                predicate,
                v))));
            
            this.predicate = predicate;
            this.v = v;
            this.source = identity;
        }
        
        public URI getSubject() {
            return source;
        }

        public String getPredicate() {
            return predicate;
        }

        public SBValue getValue() {
            return v;
        }
    }
    
    public static class RemoveValueAction extends StatementsAction {
        
        private final URI source;
        private final String predicate;
        private final SBValue v;

        public RemoveValueAction(URI identity, String predicate, SBValue v) {
            super(SynBadCommand.CommandType.REMOVE, new Statements(Collections.singletonList(new Statement(
                identity.toASCIIString(),
                predicate,
                v))));
            
            this.predicate = predicate;
            this.v = v;
            this.source = identity;
        }
        
        public URI getObject() {
            return source;
        }

        public String getPredicate() {
            return predicate;
        }

        public SBValue getValue() {
            return v;
        }

        @Override
        public void onPerformed(SBDispatcher dispatcher) {
            super.onPerformed(dispatcher); 
            dispatcher.publish(new ValueEvent(SBEventType.REMOVED, source, predicate, v));
        }
        
        
    }

    public static class SetValueAction extends StatementsAction.SetStatement {
        
        private final URI source;
        private final String predicate;
        private final SBValue v;

        public SetValueAction(URI identity, String predicate, SBValue v) {
            super(
                new Statements(Collections.singletonList(
                    new Statement(
                        identity.toASCIIString(),
                        predicate,
                        v))));
            
            this.source = identity;
            this.predicate = predicate;
            this.v = v;
        }
        
        public URI getSubject() {
            return source;
        }

        public String getPredicate() {
            return predicate;
        }

        public SBValue getValue() {
            return v;
        }
        
        @Override
        public void onPerformed(SBDispatcher dispatcher) {
            super.onPerformed(dispatcher); 
            
            removedStatements.stream().forEach(s -> 
                dispatcher.publish(new ValueEvent(SBEventType.REMOVED, URI.create(s.getSubject()), s.getPredicate(), s.getObject())));

            dispatcher.publish(new ValueEvent(SBEventType.ADDED, getSubject(), getPredicate(), getValue()));
            
        }
        
    }
    
    public static class SetNameAction extends SetValueAction {

        public SetNameAction(URI identity, String name) {
            super(  identity,
                    SynBadTerms2.SbolIdentified.hasName,
                    new SBValue(name));
        } 
    }
    
    public static class SetDescriptionAction extends SetValueAction {

        public SetDescriptionAction(URI identity, String description) {
            super(  identity,
                    SynBadTerms2.SbolIdentified.hasDescription,
                    new SBValue(description));
        } 
    }
    
    public static class SetDerivedFromAction extends SetValueAction {

         public SetDerivedFromAction(URI identity, URI derivedFrom) {
            super(  identity,
                    SynBadTerms2.SbolIdentified.wasDerivedFrom,
                    new SBValue(derivedFrom));
        }
    }
}
