/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.datamodel.workspace.api;

import uk.ac.ncl.icos.synbad.event.SBSubscriber;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import uk.ac.ncl.icos.synbad.event.SBEvent;

/**
 *
 * @author owengilfellon
 */
public interface SBDispatcher {
    
    public void publish(SBEvent event);
   
    public void subscribe(Class<? extends SBEvent> type, SBEventFilter filter, SBSubscriber subscriber);
    
    public void unsubscribe(Class<? extends SBEvent> type, SBEventFilter filter, SBSubscriber subscriber);
    
    public SBWorkspace getWorkspace();
    
    public class DefaultWorkspaceEventService implements SBDispatcher {
        
        private final Map<Class<? extends SBEvent>, Set<SubscriberRecord>> records = new HashMap<>();
        private final SBWorkspace workspace;
        
        public DefaultWorkspaceEventService(SBWorkspace workspace) {
            this.workspace = workspace;
        }
        
        @Override
        public SBWorkspace getWorkspace() {
            return workspace;
        }
        
        class SubscriberRecord {
            final Class<? extends SBEvent> clazz;
            final SBEventFilter filter;
            final SBSubscriber subscriber;

            public SubscriberRecord(Class<? extends SBEvent> clazz, SBEventFilter filter, SBSubscriber subscriber) {
                this.clazz = clazz;
                this.filter = filter;
                this.subscriber = subscriber;
            }
        }

        @Override
        public void publish(SBEvent event) {
    
            records.keySet().stream()
                .filter( clazz -> {
                    return clazz.isAssignableFrom(event.getClass());})
                .flatMap( clazz -> records.get(clazz).stream())
                .filter((SubscriberRecord record) -> {
                    return record.filter == null || record.filter.accept(event);})
                .collect(Collectors.toSet()).stream()
                .forEach((SubscriberRecord record) -> record.subscriber.onEvent(event));
        }
        
        @Override
        public void subscribe(Class<? extends SBEvent> type, SBEventFilter filter, SBSubscriber handler) {
            
            if(!records.containsKey(type))
                records.put(type, new HashSet<>());

            records.get(type).add(new SubscriberRecord(type, filter, handler));
        }
        
        @Override
        public void unsubscribe(Class<? extends SBEvent> type, SBEventFilter filter, SBSubscriber handler) {
            
            if(records.containsKey(type)) {
               records.get(type).removeAll(
                    records.get(type).stream()
                         .filter( r -> r.subscriber == handler)
                         .collect(Collectors.toSet()));
            }
        }
    } 
}
