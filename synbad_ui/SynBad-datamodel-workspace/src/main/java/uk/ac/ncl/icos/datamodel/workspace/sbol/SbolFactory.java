/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.datamodel.workspace.sbol;

import org.openide.util.lookup.ServiceProvider;
import uk.ac.ncl.icos.datamodel.workspace.api.SBObjectFactory;
import uk.ac.ncl.icos.datamodel.workspace.objects.Identity;
import uk.ac.ncl.icos.datamodel.workspace.api.SBRdfService;
import uk.ac.ncl.icos.datamodel.workspace.sbol.Location.Cut;
import uk.ac.ncl.icos.datamodel.workspace.sbol.Location.Range;
import uk.ac.ncl.icos.synbad.datadefinition.UriHelper;

/**
 *
 * @author owengilfellon
 */
public class SbolFactory {
 
    @ServiceProvider(service=SBObjectFactory.class)
    public static class ModuleDefinitionFactory extends SBObjectFactory.ADomainFactory<ModuleDefinition> {

        public ModuleDefinitionFactory() {
            super(ModuleDefinition.class, UriHelper.sbol.namespacedUri("ModuleDefinition"));
        }
    }
    
    @ServiceProvider(service=SBObjectFactory.class)
    public static class FunctionalComponentFactory extends SBObjectFactory.ADomainFactory<FunctionalComponent> {

        public FunctionalComponentFactory() {
            super(FunctionalComponent.class, UriHelper.sbol.namespacedUri("FunctionalComponent"));
        }
    }
    
    @ServiceProvider(service=SBObjectFactory.class)
    public static class ParticipationFactory extends SBObjectFactory.ADomainFactory<Participation> {

        public ParticipationFactory() {
            super(Participation.class, UriHelper.sbol.namespacedUri("Participation"));
        }
    }
    
    @ServiceProvider(service=SBObjectFactory.class)
    public static class InteractionFactory extends SBObjectFactory.ADomainFactory<Interaction> {

        public InteractionFactory() {
            super(Interaction.class, UriHelper.sbol.namespacedUri("Interaction"));
        }
    }
    
    @ServiceProvider(service=SBObjectFactory.class)
    public static class ModuleFactory extends SBObjectFactory.ADomainFactory<Module> {

        public ModuleFactory() {
            super(Module.class, UriHelper.sbol.namespacedUri("Module"));
        }
    }
    
    @ServiceProvider(service=SBObjectFactory.class)
    public static class ComponentFactory extends SBObjectFactory.ADomainFactory<Component> {

        public ComponentFactory() {
            super(Component.class, UriHelper.sbol.namespacedUri("Component"));
        }
    }
    
    @ServiceProvider(service=SBObjectFactory.class)
    public static class ComponentDefinitionFactory extends SBObjectFactory.ADomainFactory<ComponentDefinition> {

        public ComponentDefinitionFactory() {
            super(ComponentDefinition.class, UriHelper.sbol.namespacedUri("ComponentDefinition"));
        }
    }
    
    @ServiceProvider(service=SBObjectFactory.class)
    public static class SequenceFactory extends SBObjectFactory.ADomainFactory<Sequence> {

        public SequenceFactory() {
            super(Sequence.class, UriHelper.sbol.namespacedUri("Sequence"));
        }
    }
    
    @ServiceProvider(service=SBObjectFactory.class)
    public static class SequenceConstraintFactory extends SBObjectFactory.ADomainFactory<SequenceConstraint> {

        public SequenceConstraintFactory() {
            super(SequenceConstraint.class, UriHelper.sbol.namespacedUri("SequenceConstraint"));
        }
    }
     
    @ServiceProvider(service=SBObjectFactory.class)
    public static class SequenceAnnotationFactory extends SBObjectFactory.ADomainFactory<SequenceAnnotation> {

        public SequenceAnnotationFactory() {
            super(SequenceAnnotation.class, UriHelper.sbol.namespacedUri("SequenceAnnotation"));
        }
    }
    
    @ServiceProvider(service=SBObjectFactory.class)
    public static class RangeFactory extends SBObjectFactory.ADomainFactory<Range> {

        public RangeFactory() {
            super(Range.class, UriHelper.sbol.namespacedUri("Range"));
        }
    }
    
    @ServiceProvider(service=SBObjectFactory.class)
    public static class CutFactory extends SBObjectFactory.ADomainFactory<Cut> {

        public CutFactory() {
            super(Cut.class, UriHelper.sbol.namespacedUri("Cut"));
        }
    }
    
    @ServiceProvider(service=SBObjectFactory.class)
    public static class MapsToFactory extends SBObjectFactory.ADomainFactory<MapsTo> {

        public MapsToFactory() {
            super(MapsTo.class, UriHelper.sbol.namespacedUri("MapsTo"));
        }
    }
}
