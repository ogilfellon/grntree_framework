/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.datamodel.workspace.api.rdf;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import uk.ac.ncl.icos.datamodel.workspace.api.SBWorkspaceManager;
import uk.ac.ncl.icos.datamodel.workspace.api.SBWorkspace;
import org.openide.util.lookup.ServiceProvider;

/**
 *
 * @author owengilfellon
 */    
@ServiceProvider(service=SBWorkspaceManager.class)
public class DefaultWorkspaceManager implements SBWorkspaceManager {

    private final Map<URI, SBWorkspace> workspaces;

    public DefaultWorkspaceManager() {
        this.workspaces = new HashMap<>();
    }
 
    @Override
    public SBWorkspace getWorkspace(URI uri) {
        if(!workspaces.containsKey(uri)) {
            workspaces.put(uri, new DefaultWorkspace(uri));
        }

         return workspaces.get(uri);
    }

    @Override
    public void closeWorkspace(URI uri) {
        if(!workspaces.containsKey(uri))
            return;
        
        workspaces.get(uri).shutdown();
        workspaces.remove(uri);
    }

    @Override
    public boolean hasOpenWorkspace(URI uri) {
        return workspaces.containsKey(uri);
    }


    @Override
    public Set<URI> listWorkspaces() {
        return workspaces.keySet();
    }
    
    
}
