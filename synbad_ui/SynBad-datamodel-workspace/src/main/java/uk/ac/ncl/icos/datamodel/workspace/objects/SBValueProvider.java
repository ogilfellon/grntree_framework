/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.datamodel.workspace.objects;

import java.util.Collection;
import java.util.Set;
import uk.ac.ncl.icos.synbad.event.SBSubscriber;
import uk.ac.ncl.icos.synbad.graph.api.SBValue;

/**
 *
 * @author owengilfellon
 */
public interface SBValueProvider extends SBSubscriber {
    
    public Set<String> getPredicates();
    public Set<SBValue> getValues(String predicate);
    
    public void addValue(String predicate, SBValue value);
    public void setValues(String predicate, Collection<SBValue> value);
    public void removeValue(String predicate, SBValue value);
    public void clearValues(String predicate);
   
    
}
