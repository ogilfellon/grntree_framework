/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.datamodel.workspace.sbol;

import java.net.URI;
import org.apache.commons.collections4.MultiValuedMap;
import org.openide.util.Lookup;
import uk.ac.ncl.icos.datamodel.workspace.objects.Identity;
import uk.ac.ncl.icos.datamodel.workspace.objects.SynBadEdge;
import uk.ac.ncl.icos.datamodel.workspace.objects.SynBadObject;
import uk.ac.ncl.icos.synbad.datadefinition.IDataDefinitionManager;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms2;
import uk.ac.ncl.icos.synbad.datadefinition.types.ConstraintRestriction;
import uk.ac.ncl.icos.datamodel.workspace.objects.SBValueProvider;
import uk.ac.ncl.icos.synbad.graph.api.SBValue;

/**
 *
 * @author owengilfellon
 */
public class SequenceConstraint extends SynBadObject {

    private static final IDataDefinitionManager m = Lookup.getDefault().lookup(IDataDefinitionManager.class);
    
    public SequenceConstraint(Identity identity, URI workspaceIdentity, SBValueProvider values) {
        super(identity, workspaceIdentity, values);
    }

    public Component getSubject() {
        return ws.outgoingEdges(this, SynBadTerms2.SbolSequenceConstraint.hasSubject, Component.class).stream()
                .map((SynBadEdge e) -> ws.getObject(ws.getEdgeTarget(e).getIdentity(), Component.class)).findFirst().orElse(null);
    }

    public Component getObject() {
        return ws.outgoingEdges(this, SynBadTerms2.SbolSequenceConstraint.hasObject, Component.class).stream()
                .map((SynBadEdge e) -> ws.getObject(ws.getEdgeTarget(e).getIdentity(), Component.class)).findFirst().orElse(null);
    }

    public ConstraintRestriction getRestriction() {
        return getValues(SynBadTerms2.SbolSequenceConstraint.hasRestriction).stream()
                .filter(v -> v.isURI())
                .map(v ->  m.getDefinition(ConstraintRestriction.class, v.asURI().toASCIIString()))
                .findFirst().orElse(null);
    }

}
