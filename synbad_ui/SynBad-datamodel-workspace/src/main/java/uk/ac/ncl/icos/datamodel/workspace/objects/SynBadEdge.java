/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.datamodel.workspace.objects;

import uk.ac.ncl.icos.synbad.graph.api.SBEdge;
import java.net.URI;
import java.util.Objects;

/**
 *
 * @author owengilfellon
 */
public class SynBadEdge extends SBEdge.DefaultEdge<URI> {

        public SynBadEdge(URI uri) {
            super(uri);
        }

        @Override
        public String toString() {
            return getEdge().toASCIIString();
        }

}
