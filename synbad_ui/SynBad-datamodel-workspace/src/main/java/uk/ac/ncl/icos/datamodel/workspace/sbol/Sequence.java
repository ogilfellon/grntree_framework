/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.datamodel.workspace.sbol;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import uk.ac.ncl.icos.datamodel.actions.CreateObjectAction;
import uk.ac.ncl.icos.datamodel.workspace.api.rdf.Statement;
import uk.ac.ncl.icos.synbad.event.SBEventType;
import uk.ac.ncl.icos.datamodel.workspace.api.SBDispatcher;
import uk.ac.ncl.icos.datamodel.workspace.api.rdf.Statements;
import uk.ac.ncl.icos.synbad.graph.api.SBValue;
import uk.ac.ncl.icos.datamodel.workspace.objects.Identity;
import uk.ac.ncl.icos.datamodel.workspace.objects.SynBadObject;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms2;
import uk.ac.ncl.icos.datamodel.workspace.objects.SBValueProvider;
import uk.ac.ncl.icos.synbad.events.impl.DefaultEvent;

/**
 *
 * @author owengilfellon
 */
public class Sequence extends SynBadObject implements RootObject {
    
    public Sequence(Identity identity, URI workspaceIdentity, SBValueProvider values) {
        super(identity, workspaceIdentity, values);
    }
    
    public String getElements() {
        return getValue(SynBadTerms2.SbolSequence.hasElements).asString();
    }
    
    public URI getEncoding() {
        return getValue(SynBadTerms2.SbolSequence.hasEncoding).asURI();
    }

    @Override
    public List<NestedObject> getNestedObjects() {
        return new ArrayList<>();
    }
    
    
    
    // ================================
    //             Events
    // ================================
    
    public static class SequenceCreatedEvent extends DefaultEvent<URI, URI>{
        
        public SequenceCreatedEvent(URI sequenceIdentity) {
            super(SBEventType.ADDED, null, sequenceIdentity);
        }
    }
    
    public static class SequenceElementsSetEvent extends DefaultEvent<URI, String> {
        
        public SequenceElementsSetEvent(URI sequenceIdentity, String elements) {
            super(SBEventType.ADDED, sequenceIdentity, elements);
        }
    }
    
    public static class SequenceEncodingSetEvent extends DefaultEvent<URI, String> {
        
        public SequenceEncodingSetEvent(URI sequenceIdentity, String encoding) {
            super(SBEventType.ADDED, sequenceIdentity, encoding);
        }
    }
    
    // ================================
    //             Actions
    // ================================
    
    public static class CreateSequenceAction extends CreateObjectAction<Sequence> {

        public CreateSequenceAction( Identity identity, String elements, String encoding) {
            super(
                identity,
                "http://sbols.org/v2#Sequence",
                new Statements(Arrays.asList(
                    new Statement(
                        identity.getIdentity().toASCIIString(),
                        SynBadTerms2.SbolSequence.hasElements,
                        new SBValue(elements)),
                    new Statement(
                        identity.getIdentity().toASCIIString(),
                        SynBadTerms2.SbolSequence.hasEncoding,
                        new SBValue(encoding)))),
                null);
        }

        @Override
        public void onPerformed(SBDispatcher service) {
            super.onPerformed(service);
            service.publish(new SequenceCreatedEvent(getObjectIdentity()));
        }
    }
    
    public static class SetElementsAction extends SetValueAction {

        public SetElementsAction(URI sequenceId, String elements) {
            super(sequenceId, SynBadTerms2.SbolSequence.hasElements, new SBValue(elements));
        }

        @Override
        public void onPerformed(SBDispatcher dispatcher) {
            super.onPerformed(dispatcher);
            dispatcher.publish(new SequenceElementsSetEvent(getSubject(), getValue().asString()));
        }
    }
    
    public static class SetEncodingAction extends SetValueAction {

        public SetEncodingAction(URI sequenceId, String encoding) {
            super(sequenceId, SynBadTerms2.SbolSequence.hasEncoding, new SBValue(encoding));
        }

        @Override
        public void onPerformed(SBDispatcher dispatcher) {
            super.onPerformed(dispatcher);
            dispatcher.publish(new SequenceElementsSetEvent(getSubject(), getValue().asString()));
        }
    }
}
