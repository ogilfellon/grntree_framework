/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.sort;

/**
 *
 * @author owengilfellon
 */
public interface ReturnableAction<T> extends WorkspaceAction {
    
    public T performAndReturn();
    public T getValue();

    /*
    public static class CreateEntity implements ReturnableAction<SynBadObject> {

        private final IWorkspace workspace;
        private final SynBadObject entity;
        
        public CreateEntity(IWorkspace workspace, Identity identity) {
            this.entity = new SynBadObject(workspace, identity);
            this.workspace = workspace;
        }

        @Override
        public void perform() {
            if(!workspace.containsEntity(entity.getIdentity()))
                workspace.addEntity(entity);
        }

        @Override
        public void undo() {
            workspace.removeEntity(entity);
        }

        @Override
        public SynBadObject performAndReturn() {
            perform();
            return entity;
        }

        @Override
        public SynBadObject getValue() {
            return entity;
        }
    }
        
    public static class CreateChild implements ReturnableAction<EntityInstance> {

        private final SynBadObject parent;
        private final SynBadObject child;
        private EntityInstance childInstance;
        private final int index;
        
        public CreateChild(SynBadObject parent, SynBadObject child) {
            this(parent, child, parent.getChildren().size());
        }
        
        public CreateChild(SynBadObject parent, SynBadObject child, int index) {
            this.parent = parent;
            this.child = child;
            this.index = index;
        }

        @Override
        public void perform() {
            if(childInstance == null)
                this.childInstance = new EntityInstance(
                    parent,
                    child,
                    child.getDisplayID() + "_instance" +
                        parent.getChildren().stream()
                            .filter(i -> i.getValue() == child).count());
            parent.addChild(index, childInstance);
        }

        @Override
        public void undo() {
            parent.removeChild(childInstance);
        }

        @Override
        public EntityInstance performAndReturn() {
            perform();
            return childInstance;
        }

        @Override
        public EntityInstance getValue() {
            return childInstance;
        }
        
        

    }
    
    public static class CreateProxy implements ReturnableAction<IPort> {

        private final boolean isPublic;
        private final SynBadObject entity;
        private final IPort proxiedPort;
        private IPort proxyPort;
        private String proxyName;
        private final Annotation portAnnotation;
  
        public CreateProxy(SynBadObject entity, IPort proxiedPort, boolean isPublic) {
            this.entity = entity;
            this.proxiedPort = proxiedPort;
            portAnnotation =  new Annotation(SynBadTerms.Annotatable.entitytype, EntityType.Port.getUri());
            this.isPublic = isPublic;
        }

        @Override
        public void perform() {
            
            if(proxyPort == null)
            {
                long portCount = entity.getPorts().stream().filter(port -> {
                        return port.getPortDirection() == proxiedPort.getPortDirection() &&
                         port.getPortType() == proxiedPort.getPortType();}).count();
                String portId = entity.getDisplayID() + "_" + proxiedPort.getPortDirection()+ "_" + proxiedPort.getPortType() + "_" + portCount;  

                proxyPort = new ProxyPort(
                    entity,
                    portId + "_proxy",
                    entity.getVersion(),
                    proxiedPort.getIdentity(),
                    isPublic);
            }
            
            proxyName = proxyPort.toString();
            proxyPort.addAnnotation(SynBadTerms.Annotatable.entitytype,  portAnnotation);
            proxyPort.setName(proxyName);
            entity.addPort(proxyPort);
            
            
            
        }

        @Override
        public void undo() {
            entity.removePort(proxyPort);
             proxyPort.setName(proxiedPort.getDisplayID());
            proxyPort.removeAnnotation(SynBadTerms.Annotatable.entitytype,  portAnnotation);
           
            
        }

        @Override
        public IPort performAndReturn() {
            perform();
            return proxyPort;
        }

        @Override
        public IPort getValue() {
            return proxyPort;
        }
        
        
    }
    
    public static class CreateStatefulPort implements ReturnableAction<IPort> {

        private final SynBadObject entity;
        private  IPort port;
        private final PortDirection direction;
        private final PortType type;
        private final PortState state;
        private final boolean isPublic;
        private final Annotation entityType;
        private final Annotation portType;
        private final Annotation portState;
  
        public CreateStatefulPort(SynBadObject entity, PortDirection direction, PortType type, PortState state, boolean isPublic) {

            this.entity = entity;
            entityType = new Annotation(SynBadTerms.Annotatable.entitytype, EntityType.Port.getUri());
            portType = new Annotation(SynBadTerms.Port.portType,  type.getUri());
            portState = new Annotation(SynBadTerms.Port.portType,  state.getUri());
            this.state = state;
            this.direction = direction;
            this.type = type;
            this.isPublic = isPublic;
        }

        @Override
        public void perform() {
            if(port == null) {
                long portCount = entity.getPorts().stream().filter(port -> {
                        return port.getPortDirection() == direction &&
                        port.hasAnnotation(SynBadTerms.Port.portType, type.getUri());}).count();
                String portId = entity.getDisplayID() + "_" + direction + "_" + type + "_" + portCount;            
                port = new DefaultPort(entity, portId, entity.getVersion(), direction, null, isPublic);
            }
            entity.addPort(port);
            port.addAnnotation(SynBadTerms.Annotatable.entitytype, entityType );
            port.addAnnotation(SynBadTerms.Port.portType, portType);
            port.addAnnotation(SynBadTerms.Port.portType, portState);
            port.setName(port.toString());
        }

        @Override
        public void undo() {
            entity.removePort(port);
            port.setName("");
            port.addAnnotation(SynBadTerms.Port.portType, portType);
            port.addAnnotation(SynBadTerms.Annotatable.entitytype, entityType );
            
        }

        @Override
        public IPort performAndReturn() {
            perform();
            return port;
        }

        @Override
        public IPort getValue() {
            return port;
        }
        
        

    }
    
    public static class CreatePort implements ReturnableAction<IPort> {

        private final SynBadObject entity;
        private IPort port;
        private final PortDirection direction;
        private final PortType type;

        private final boolean isPublic;
        private  String name;
        private final Annotation entityType;
        private final Annotation portType;
  
        public CreatePort(SynBadObject entity, PortDirection direction, PortType type, boolean isPublic) {
            this.entity = entity;
            
            entityType = new Annotation(SynBadTerms.Annotatable.entitytype, EntityType.Port.getUri());
            portType = new Annotation(SynBadTerms.Port.portType,  type.getUri());


            this.direction = direction;
            this.type = type;
            this.isPublic = isPublic;
        }

        @Override
        public void perform() {
            
            if(port == null) {
                long portCount = entity.getPorts().stream().filter(port -> {
                        return port.getPortDirection() == direction &&
                        port.hasAnnotation(SynBadTerms.Port.portType, type.getUri());}).count();
                String portId = entity.getDisplayID() + "_" + direction + "_" + type + "_" + portCount;            
                port = new DefaultPort(entity, portId, entity.getVersion(), direction, null, isPublic);
            }
            
            
            port.setName(port.toString());
            port.addAnnotation(SynBadTerms.Annotatable.entitytype, entityType );
            port.addAnnotation(SynBadTerms.Port.portType, portType);
            entity.addPort(port);
        }

        @Override
        public void undo() {
           
            entity.removePort(port);
            port.removeAnnotation(SynBadTerms.Port.portType, portType);
            port.removeAnnotation(SynBadTerms.Annotatable.entitytype, entityType );
            port.setName(port.toString());
            
        }

        @Override
        public IPort performAndReturn() {
            perform();
            return port;
        }

        @Override
        public IPort getValue() {
            return port;
        }
        
        

    }
    
    public static class WirePorts implements ReturnableAction<IWire> {

        private final PortRecord from;
        private final PortRecord to;
        private final SynBadObject entity;
        private IWire wire;
  
        public WirePorts(SynBadObject entity, PortRecord from, PortRecord to) {
            this.entity = entity;
            this.from = from;
            this.to = to;
        }

        @Override
        public void perform() {
            
            if(!from.getValue().isConnectable(to.getValue()) || !to.getValue().isConnectable(from.getValue())){
                throw new IllegalArgumentException(from + " and " + to + " are not connectable");
            }
            
            if(this.wire == null)   
                 this.wire = new IWire(entity.getWorkspace(), from, to);
            
            entity.addWire(wire);
        }

        @Override
        public void undo() {
            entity.removeWire(wire);
        }

        @Override
        public IWire performAndReturn() {
            perform();
            return wire;
        }

        @Override
        public IWire getValue() {
            return wire;
        }
        
        

    }
    
     public static class WireChildren implements ReturnableAction<IWire> {

        private final SynBadObject entity;
        private IWire wire;
        private final EntityInstance from, to;
        private final PortType type;
  
        public WireChildren(SynBadObject entity, EntityInstance from, EntityInstance to, PortType type) {
            this.entity = entity;
            this.from = from;
            this.to = to;
            this.type = type;
        }

        @Override
        public void perform() {
            if(wire == null)
                wire = entity.wireChildren(from, to, type, null);
            else
                entity.addWire(wire);
        }

        @Override
        public void undo() {
            entity.removeWire(wire);
        }

        @Override
        public IWire performAndReturn() {
            perform();
            return wire;
        }

        @Override
        public IWire getValue() {
            return wire;
        }
        
        

    }*/

}
