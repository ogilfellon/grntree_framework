/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.datamodel.workspace.api.rdf;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdfxml.xmlinput.JenaReader;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFFormat;
import org.openide.util.Exceptions;
import org.sbolstandard.core2.SBOLConversionException;
import org.sbolstandard.core2.SBOLDocument;
import org.sbolstandard.core2.SBOLReader;
import org.sbolstandard.core2.SBOLValidationException;
import org.sbolstandard.core2.SBOLWriter;

/**
 *
 * @author owengilfellon
 */
public class JenaParser {
    
    
    public static Model getModel(SBOLDocument document) {
        
        Model model = ModelFactory.createDefaultModel();
        
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            SBOLWriter.write(document, out);
            JenaReader reader = new JenaReader();
            
            reader.read(model, new ByteArrayInputStream(out.toByteArray()), null);

        } catch (SBOLConversionException ex) {
            Exceptions.printStackTrace(ex);
        } 
        
        return model;
    }
    
    public static SBOLDocument getDocument(Model model) {

        SBOLDocument document = null;
        
        try {

            ByteArrayOutputStream out = new ByteArrayOutputStream();
            RDFDataMgr.write(out, model, RDFFormat.RDFXML_PLAIN);
            document = SBOLReader.read(new ByteArrayInputStream(out.toByteArray()));
            
        } catch (SBOLValidationException | IOException | SBOLConversionException ex) {
            Exceptions.printStackTrace(ex);
        } 
        
        return document;
    }
    
}
