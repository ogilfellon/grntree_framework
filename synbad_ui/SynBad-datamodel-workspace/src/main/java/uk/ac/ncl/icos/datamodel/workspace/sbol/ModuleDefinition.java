/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.datamodel.workspace.sbol;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import java.util.stream.Stream;
import java.util.stream.Collectors;
import uk.ac.ncl.icos.datamodel.actions.CreateObjectAction;
import uk.ac.ncl.icos.datamodel.workspace.api.rdf.Statement;
import uk.ac.ncl.icos.synbad.event.SBEventType;
import uk.ac.ncl.icos.datamodel.workspace.api.SBDispatcher;
import uk.ac.ncl.icos.datamodel.workspace.api.rdf.Statements;
import uk.ac.ncl.icos.synbad.graph.api.SBValue;
import uk.ac.ncl.icos.datamodel.workspace.objects.Identity;
import uk.ac.ncl.icos.datamodel.workspace.objects.SynBadObject;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms2;
import uk.ac.ncl.icos.datamodel.workspace.objects.SBValueProvider;
import uk.ac.ncl.icos.synbad.events.impl.DefaultEvent;

/**
 * ModuleDefinition represents a grouping of functional elements in an
 * engineered system.
 * @author owengilfellon
 */
public class ModuleDefinition extends SynBadObject implements Definition<ModuleDefinition> {

    public ModuleDefinition(Identity identity, URI workspaceIdentity, SBValueProvider values) {
        super(identity, workspaceIdentity, values);
    }

    @Override
    public List<NestedObject> getNestedObjects() {
        List<NestedObject> children = new ArrayList<>();
        children.addAll(getFunctionalComponents());
        children.addAll(getModules());
        children.addAll(getInteractions());
        return children;
    }

    
    public Set<FunctionalComponent> getFunctionalComponents() {
        return ws.outgoingEdges(
                this,
                SynBadTerms2.SbolModule.hasFunctionalComponent,
                FunctionalComponent.class).stream()
            .map(e -> ws.getObject(ws.getEdgeTarget(e).getIdentity(), FunctionalComponent.class))
            .collect(Collectors.toSet());
    }
    
    public Set<Module> getModules() {
         return ws.outgoingEdges(
                this,
                SynBadTerms2.SbolModule.hasModule,
                Module.class).stream()
            .map(e -> ws.getObject(ws.getEdgeTarget(e).getIdentity(), Module.class))
            .collect(Collectors.toSet());
    }
    
    public Set<Interaction> getInteractions() {
         return ws.outgoingEdges(
                this,
                SynBadTerms2.SbolModule.hasInteraction,
                Interaction.class).stream()
            .map(e -> ws.getObject(ws.getEdgeTarget(e).getIdentity(), Interaction.class))
            .collect(Collectors.toSet());
       
    }
    
    // ================================
    //             Events
    // ================================

    public static class ModuleDefinitionCreatedEvent extends DefaultEvent<URI, URI>{
            
        public ModuleDefinitionCreatedEvent(URI moduleDefinition) {
            super(SBEventType.ADDED, null, moduleDefinition);
        }
    }
    
    public static class FunctionalComponentCreatedEvent extends DefaultEvent<URI, URI>{
 
        public FunctionalComponentCreatedEvent(URI identity, URI componentDefinition, URI parentDefinition) {
            super(SBEventType.ADDED, parentDefinition, identity);
        }

    }
    
    public static class ModuleCreatedEvent extends DefaultEvent<URI, URI>{

        public ModuleCreatedEvent(URI moduleIdentity, URI moduleDefinition, URI ownerIdentity) {
            super(SBEventType.ADDED, ownerIdentity, moduleIdentity);
        }

    }
    
    public static class ModelAddedEvent extends DefaultEvent<URI, URI> {

        public ModelAddedEvent(URI modelIdentity, URI ownerIdentity) {
            super(SBEventType.ADDED, ownerIdentity, modelIdentity);
        }

    }
    
    public static class InteractionCreatedEvent extends DefaultEvent<URI, URI>{

        public InteractionCreatedEvent(URI interactionIdentity,  URI ownerIdentity) {
            super(SBEventType.ADDED, ownerIdentity, interactionIdentity);
        }
    }

    // ================================
    //             Actions
    // ================================

    public static class CreateModuleDefinitionAction extends CreateObjectAction<ModuleDefinition> {

        public CreateModuleDefinitionAction( Identity identity) {
            super(identity,
                    "http://sbols.org/v2#ModuleDefinition",
                    new Statements(),
                    null);
        }

        @Override
        public void onPerformed(SBDispatcher service) {
            super.onPerformed(service);
            service.publish(new ModuleDefinitionCreatedEvent(getObjectIdentity() ));
        }
    }
    
    public static class CreateModuleAction extends CreateObjectAction<Module> {

        private final URI definitionIdentity;

        public CreateModuleAction(Identity identity, ModuleDefinition definition, ModuleDefinition parent) {
             super(identity,
                    "http://sbols.org/v2#Module",
                    new Statements(Arrays.asList(
                        new Statement(
                            identity.getIdentity().toASCIIString(),
                            SynBadTerms2.Sbol.definedBy,
                            new SBValue(definition.getIdentity())),
                        new Statement(
                            parent.getIdentity().toASCIIString(),
                            SynBadTerms2.SbolModule.hasModule,
                            new SBValue(identity.getIdentity())))),
                    parent.getIdentity());

            this.definitionIdentity = definition.getIdentity();
             
        }

        @Override
        public void onPerformed(SBDispatcher service) {
            super.onPerformed(service);
            service.publish(new ModuleCreatedEvent(getObjectIdentity(), definitionIdentity, getParentIdentity()));
        }
        
    }
    
    public static class CreateFuncComponentAction extends CreateObjectAction<FunctionalComponent> {

        private final URI definition;

        public CreateFuncComponentAction(Identity identity, ComponentDefinition definition, ModuleDefinition parent) {
            super(identity,
                    "http://sbols.org/v2#FunctionalComponent",
                    new Statements(Arrays.asList(new Statement(
                        identity.getIdentity().toASCIIString(),
                        SynBadTerms2.Sbol.definedBy,
                        new SBValue(definition.getIdentity())),
                    new Statement(
                        parent.getIdentity().toASCIIString(),
                        SynBadTerms2.SbolModule.hasFunctionalComponent,
                        new SBValue(identity.getIdentity())))),
                    parent.getIdentity());
            
          
            this.definition = definition.getIdentity();
        }

        @Override
        public void onPerformed(SBDispatcher service) {
            super.onPerformed(service);
            service.publish(new FunctionalComponentCreatedEvent(getObjectIdentity(), definition, getParentIdentity()));
        }
    }
    
    public static class CreateInteractionAction extends CreateObjectAction<Interaction> {

        public CreateInteractionAction(Identity identity, ModuleDefinition owner, Identity participationIdentity, FunctionalComponent... participants) {
            super(identity,
                    "http://sbols.org/v2#Interaction",
                    new Statements(Stream.concat(
                        Arrays.asList(
                            new Statement(
                                identity.getIdentity().toASCIIString(),
                                SynBadTerms2.SbolInteraction.hasParticipation,
                                new SBValue(participationIdentity.getIdentity())),
                            new Statement(
                                owner.getIdentity().toASCIIString(),
                                SynBadTerms2.SbolModule.hasInteraction,
                                new SBValue(identity.getIdentity()))).stream(),
                        Arrays.stream(participants).map(f -> 
                            new Statement(
                                participationIdentity.getIdentity().toASCIIString(),
                                SynBadTerms2.SbolInteraction.participant,
                                new SBValue(f.getIdentity())))).collect(Collectors.toSet())),
                    owner.getIdentity());
            
        }

        @Override
        public void onPerformed(SBDispatcher service) {
            super.onPerformed(service);
            service.publish(new InteractionCreatedEvent(getObjectIdentity(), getParentIdentity()));
        }
    }
    
    public static class AddModelAction extends AddValueAction {
        

        public AddModelAction(URI modelIdentity, ModuleDefinition owner) {
            super(modelIdentity, SynBadTerms2.SbolModule.hasModel, new SBValue(modelIdentity));
        }

        @Override
        public void onPerformed(SBDispatcher service) {
            super.onPerformed(service);
            service.publish(new ModelAddedEvent( getValue().asURI(), getSubject() ));
        }
    }

}
