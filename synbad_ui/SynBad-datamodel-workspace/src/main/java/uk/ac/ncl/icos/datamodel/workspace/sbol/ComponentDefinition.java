/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.datamodel.workspace.sbol;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import uk.ac.ncl.icos.datamodel.actions.CreateObjectAction;
import uk.ac.ncl.icos.datamodel.workspace.api.rdf.Statement;
import uk.ac.ncl.icos.synbad.event.SBEventType;
import uk.ac.ncl.icos.datamodel.workspace.api.SBDispatcher;
import uk.ac.ncl.icos.datamodel.workspace.api.rdf.Statements;
import uk.ac.ncl.icos.synbad.graph.api.SBValue;
import uk.ac.ncl.icos.datamodel.workspace.objects.Identity;
import uk.ac.ncl.icos.datamodel.workspace.objects.SynBadObject;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms2;
import uk.ac.ncl.icos.synbad.datadefinition.types.Type;
import uk.ac.ncl.icos.datamodel.workspace.objects.SBValueProvider;
import uk.ac.ncl.icos.synbad.events.impl.DefaultEvent;

/**
 * ComponentDefinition represents a structural element in an engineered
 * system. e
 * @author owengilfellon
 */
public class ComponentDefinition extends SynBadObject implements Definition<ComponentDefinition>{

    public ComponentDefinition(Identity identity, URI workspaceIdentity, SBValueProvider values) {
        super(identity, workspaceIdentity, values);
    }

    public List<SynBadObject> getChildren() {
        List<SynBadObject> children = new ArrayList<>();
        children.addAll(getComponents());
        return children;
    }

    @Override
    public List<NestedObject> getNestedObjects() {
        return new ArrayList<>(getComponents());
    }
    
    
    
    public Set<Component> getComponents() {
        return ws.outgoingEdges(
                this,
                SynBadTerms2.SbolComponent.hasComponent,
                Component.class).stream()
            .map(e -> ws.getObject(ws.getEdgeTarget(e).getIdentity(), Component.class))
            .collect(Collectors.toSet());
    }
    
    public Set<Sequence> getSequences() {
        return ws.outgoingEdges(
                this,
                SynBadTerms2.SbolComponent.hasSequence,
                Sequence.class).stream()
            .map(e -> ws.getObject(ws.getEdgeTarget(e).getIdentity(), Sequence.class))
            .collect(Collectors.toSet());
    }
    public Set<SequenceAnnotation> getSequenceAnnotations() {
        return ws.outgoingEdges(
                this,
                SynBadTerms2.SbolComponent.hasSequenceAnnotation,
                SequenceAnnotation.class).stream()
            .map(e -> ws.getObject(ws.getEdgeTarget(e).getIdentity(), SequenceAnnotation.class))
            .collect(Collectors.toSet());
    }
    
    public Set<SequenceConstraint> getSequenceConstraints() {
        return ws.outgoingEdges(
                this,
                SynBadTerms2.SbolComponent.hasSequenceConstraint,
                SequenceConstraint.class).stream()
            .map(e -> ws.getObject(ws.getEdgeTarget(e).getIdentity(), SequenceConstraint.class))
            .collect(Collectors.toSet());
    }
    
    // ================================
    //             Events
    // ================================
    
    public static class ComponentDefinitionCreatedEvent extends DefaultEvent<URI, URI>{
            
        public ComponentDefinitionCreatedEvent(URI workspaceId, URI componentDefinition) {
            super(SBEventType.ADDED, workspaceId, componentDefinition);
        }
    }
    
    public static class ComponentCreatedEvent extends DefaultEvent<URI, URI>{
        
        public ComponentCreatedEvent(URI componentIdentity, URI componentDefinition, URI ownerIdentity) {
            super(SBEventType.ADDED, ownerIdentity, componentIdentity);
        }
    }

    public static class SequenceAddedEvent extends DefaultEvent<URI, URI>{

        public SequenceAddedEvent(URI sequenceIdentity, URI componentDefinition) {
            super(SBEventType.ADDED, componentDefinition, sequenceIdentity);
        }
    }
   
    public static class SequenceConstraintCreatedEvent extends DefaultEvent<URI, URI>{

        public SequenceConstraintCreatedEvent(URI sequenceConstraintIdentity, URI componentDefinition) {
            super(SBEventType.ADDED, componentDefinition, sequenceConstraintIdentity);
        }
    }
    
    public static class SequenceAnnotationCreatedEvent extends DefaultEvent<URI, URI>{

         public SequenceAnnotationCreatedEvent(URI sequenceAnnotationIdentity, URI componentDefinition) {
             super(SBEventType.ADDED, componentDefinition, sequenceAnnotationIdentity);
         }
    }

    // ================================
    //             Actions
    // ================================
    
    
    public static class CreateComponentDefinitionAction extends CreateObjectAction<ComponentDefinition> {

        public CreateComponentDefinitionAction( Identity identity, Set<Type> types) {
            super(  identity,
                    "http://sbols.org/v2#ComponentDefinition",
                    new Statements(types.stream().map(t -> new Statement(
                            identity.getIdentity().toASCIIString(),
                            SynBadTerms2.Sbol.type,
                            new SBValue(t.getUri()))).collect(Collectors.toList())),
                    null);
        }

        @Override
        public void onPerformed(SBDispatcher service) {
            super.onPerformed(service);
            service.publish(new ComponentDefinitionCreatedEvent(null, getObjectIdentity()));
        }
    }

    public static class CreateComponentAction extends CreateObjectAction<Component> {
        
        private final URI componentDefinition;

        public CreateComponentAction(Identity identity, ComponentDefinition parent, ComponentDefinition definition) {
            super(  identity,
                    "http://sbols.org/v2#Component",
                    new Statements(Arrays.asList(new Statement(
                            identity.getIdentity().toASCIIString(),
                            SynBadTerms2.Sbol.definedBy,
                            new SBValue(definition.getIdentity())),
                        new Statement(
                            parent.getIdentity().toASCIIString(),
                            SynBadTerms2.SbolComponent.hasComponent,
                            new SBValue(identity.getIdentity())))),
                    parent.getIdentity());
            
            this.componentDefinition = definition.getIdentity();
        }

        @Override
        public void onPerformed(SBDispatcher service) {
            super.onPerformed(service);
            service.publish(new ComponentCreatedEvent(getObjectIdentity(), componentDefinition, getParentIdentity()));
        }
    }
    
    public static class AddSequenceAction extends AddValueAction {

        public AddSequenceAction(URI componentDefinition, URI sequence) {
            super(componentDefinition, SynBadTerms2.SbolComponent.hasSequence, new SBValue(sequence));
        }

        @Override
        public void onPerformed(SBDispatcher service) {
            super.onPerformed(service);
            service.publish(new SequenceAddedEvent(getValue().asURI(), getSubject()));
        }
    }
    
    public static class CreateSequenceConstraintAction extends CreateObjectAction<SequenceConstraint> {
        
        public CreateSequenceConstraintAction(Identity identity, ComponentDefinition owner, Component subject, Component object, URI restriction) {
            super(  identity,
                    "http://sbols.org/v2#SequenceConstraint",
                    new Statements(Arrays.asList(
                        new Statement(
                            identity.getIdentity().toASCIIString(),
                            SynBadTerms2.SbolSequenceConstraint.hasRestriction,
                            new SBValue(restriction)),
                        new Statement(
                            identity.getIdentity().toASCIIString(),
                            SynBadTerms2.SbolSequenceConstraint.hasObject,
                            new SBValue(object.getIdentity())),
                        new Statement(
                            identity.getIdentity().toASCIIString(),
                            SynBadTerms2.SbolSequenceConstraint.hasSubject,
                            new SBValue(subject.getIdentity())),
                        new Statement(
                            owner.getIdentity().toASCIIString(),
                            SynBadTerms2.SbolComponent.hasSequenceConstraint,
                            new SBValue(identity.getIdentity())))),
                    owner.getIdentity());
        
        }

        @Override
        public void onPerformed(SBDispatcher service) {
            super.onPerformed(service);
            service.publish(new SequenceConstraintCreatedEvent(getObjectIdentity(), getParentIdentity()));
        }
    }
    
    
    public static class CreateSequenceAnnotationAction extends CreateObjectAction<SequenceAnnotation> {
        
        public CreateSequenceAnnotationAction(Identity identity, ComponentDefinition owner, Component component, Location... locations) {
            super(  identity,
                "http://sbols.org/v2#SequenceAnnotation",
                new Statements(Stream.concat(
                    Stream.of(
                        new Statement(
                            owner.getIdentity().toASCIIString(),
                            SynBadTerms2.SbolComponent.hasSequenceAnnotation,
                            new SBValue(identity.getIdentity())),
                        new Statement(
                            identity.getIdentity().toASCIIString(),
                            SynBadTerms2.SbolSequenceAnnotation.positionsComponent,
                            new SBValue(component.getIdentity()))), 
                    Arrays.stream(locations).map(
                        l ->  new Statement(
                            identity.getIdentity().toASCIIString(),
                            SynBadTerms2.SbolSequenceAnnotation.hasLocation,
                            new SBValue(l.getIdentity())))).collect(Collectors.toList())),
                owner.getIdentity());
        }
        
        public CreateSequenceAnnotationAction(Identity identity, ComponentDefinition owner, Location... locations) {
            super(  identity,
                "http://sbols.org/v2#SequenceAnnotation",
                new Statements(Stream.concat(
                    Stream.of(
                        new Statement(
                            owner.getIdentity().toASCIIString(),
                            SynBadTerms2.SbolComponent.hasSequenceAnnotation,
                            new SBValue(identity.getIdentity()))), 
                    Arrays.stream(locations).map(
                        l ->  new Statement(
                            identity.getIdentity().toASCIIString(),
                            SynBadTerms2.SbolSequenceAnnotation.hasLocation,
                            new SBValue(l.getIdentity())))).collect(Collectors.toList())),

                owner.getIdentity());
        }
        
        @Override
        public void onPerformed(SBDispatcher service) {
            super.onPerformed(service);
            service.publish(new SequenceAnnotationCreatedEvent(getObjectIdentity(), getParentIdentity()));
        }
    }

}
