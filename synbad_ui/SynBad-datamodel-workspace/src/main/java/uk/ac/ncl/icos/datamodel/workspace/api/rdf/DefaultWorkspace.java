/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.datamodel.workspace.api.rdf;

import uk.ac.ncl.icos.synbad.graph.api.SBValue;
import uk.ac.ncl.icos.datamodel.actions.SynBadCommand;
import uk.ac.ncl.icos.datamodel.actions.RemoveObjectAction;
import uk.ac.ncl.icos.datamodel.actions.CreateObjectAction;
import uk.ac.ncl.icos.datamodel.workspace.api.SBObjectFactory;
import uk.ac.ncl.icos.datamodel.workspace.api.SBRdfService;
import uk.ac.ncl.icos.datamodel.workspace.api.SBWorkspace;
import java.io.File;
import java.io.InputStream;
import java.net.URI;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.openide.util.Lookup;
import uk.ac.ncl.icos.datamodel.graph.impl.DefaultGraph;
import uk.ac.ncl.icos.datamodel.workspace.api.SBObjectFactory.IdentityFactory;
import uk.ac.ncl.icos.datamodel.workspace.api.SBCommandManager;
import uk.ac.ncl.icos.datamodel.workspace.api.SBDispatcher;
import uk.ac.ncl.icos.datamodel.workspace.api.SBEntity;
import uk.ac.ncl.icos.synbad.graph.api.SBCursor;
import uk.ac.ncl.icos.synbad.graph.api.SBEdgeProvider;
import uk.ac.ncl.icos.datamodel.workspace.objects.Identity;
import uk.ac.ncl.icos.datamodel.workspace.objects.SynBadEdge;
import uk.ac.ncl.icos.datamodel.workspace.objects.SynBadObject;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms2;
import uk.ac.ncl.icos.synbad.graph.api.SBGraph;

/**
 *
 * @author owengilfellon
 */

public class DefaultWorkspace implements SBWorkspace {

    private final URI workspaceId;
   
    // Event dispatcher, triplestore service, and command manager.
    
    private final SBDispatcher dispatcher;
    private final SBRdfService rdfService;
    private final SBCommandManager commander;

    private final Map<Class<? extends SBEntity>, SBObjectFactory<? extends SBEntity>> factories;
    
    // Caches for retrieved objects and edges
    
    private final SBGraph<SBEntity, SynBadEdge> graphCache;
    private final Map<URI, SBEntity> objectCache;
    private final Map<Statement, SynBadEdge> edgeCache;
 
    DefaultWorkspace(URI workspaceId) {
        this.workspaceId = workspaceId;
        
        this.rdfService = DefaultRdfService.getService(workspaceId.toASCIIString());
        this.dispatcher = new SBDispatcher.DefaultWorkspaceEventService(this);
        this.commander = new SBCommandManager.DefaultCommandManager(workspaceId, rdfService, dispatcher);
        
        this.objectCache = new HashMap<>();
        this.edgeCache = new HashMap<>();
        this.graphCache = new DefaultGraph<>();
        
        this.factories = new HashMap<>();
        
        Lookup.getDefault().lookupAll(SBObjectFactory.class).stream().forEach((factory) -> registerDomainFactory(factory));
    }

    private Identity getCachedIdentity(URI uri) {
        SBEntity o = objectCache.get(uri);
        return o == null ? null : new Identity(
                o.getPrefix(), 
                o.getDisplayId(),
                o.getVersion());
    }

    @Override
    public void addRdf(File file) {
        rdfService.addRdf(file);
    }

    @Override
    public <T extends SBEntity> SBObjectFactory<T> getFactory(Class<T> clazz) {
        return(SBObjectFactory<T>)factories.get(clazz);
    }

    @Override
    public Set<SBObjectFactory<? extends SBEntity>> getFactories() {
        return new HashSet<>(factories.values());
    }
 
    private void registerDomainFactory(SBObjectFactory factory) {
        if(factories.containsKey(factory.getEntityClass()))
            return;
        factories.put(factory.getEntityClass(), factory);
    }

    @Override
    public boolean containsIdentity(URI identity) {    
        
        if(objectCache.containsKey(identity))
            return true;

        Set<String> predicates = rdfService.getStatements(
                identity.toASCIIString(),
                null,
                null).stream()
            .map(Statement::getPredicate).collect(Collectors.toSet());
        
        return predicates.contains(SynBadTerms2.SbolIdentified.hasDisplayId) &&
                predicates.contains(SynBadTerms2.SbolIdentified.hasVersion) &&
                predicates.contains(SynBadTerms2.SbolIdentified.hasPersistentIdentity);
    }
    
    
    @Override
    public Identity getIdentity(URI identity) {
        
        if(objectCache.containsKey(identity))
            return getCachedIdentity(identity);
        
        IdentityFactory f = new IdentityFactory();
        
        if(f.isEntity(identity.toASCIIString(), rdfService))
            return f.getEntity(identity.toASCIIString(), rdfService);

        return null;
    }

    @Override
    public Set<Identity> getIdentities() {
        
        Set<Identity> identities = new HashSet<>();
        
        IdentityFactory f = new IdentityFactory();

        identities.addAll(objectCache.keySet().stream().map(this::getCachedIdentity).collect(Collectors.toList()));

        identities.addAll(rdfService.getStatements(null, SynBadTerms2.Rdf.hasType, null).stream()
                .filter(s -> !objectCache.containsKey(URI.create(s.getSubject())))
                .filter(s -> f.isEntity(s.getSubject(), rdfService))
                .map(Statement::getSubject)
                .map(s -> f.getEntity(s, rdfService))
                .collect(Collectors.toSet()));
        
        return identities;
        
    }

    @Override
    public Set<Identity> getIdentities(URI persistentIdentity) {
        IdentityFactory f = new IdentityFactory();
        return rdfService.getStatements(null,
            SynBadTerms2.SbolIdentified.hasPersistentIdentity, 
            new SBValue(persistentIdentity)).stream()
                .filter(s -> s.getObject().isURI())
                .filter(s -> f.isEntity(s.getObject().asURI().toASCIIString(), rdfService))
                .map(Statement::getSubject)
                .collect(Collectors.toSet()).stream()
                .map(s -> f.getEntity(s, rdfService))
                .collect(Collectors.toSet());
    }

    

    @Override
    public Collection<Class<? extends SBEntity>> getObjectClasses() {
        return factories.keySet();
    }

    @Override
    public Collection<Class<? extends SBEntity>> getObjectClasses(URI identity) {
        return factories.values().stream()
            .filter(f -> f.isEntity(identity.toASCIIString(), rdfService))
            .filter(f-> SBEntity.class.isAssignableFrom(f.getEntityClass()))
            .map(f -> (Class<? extends SBEntity>)f.getEntityClass())
            .collect(Collectors.toSet());
    }
    
    private <T extends SBEntity> boolean isObject(URI identity, Class<T> clazz) {

        T obj = retrieveObject(identity, clazz);
        return obj != null;
    }
    
    private <T extends SBEntity> boolean isEdge(Statement statement) {
        
        if(edgeCache.containsKey(statement))
            return true;

        return statement.getObject().isURI() &&
                isObject(statement.getObject().asURI(), SBEntity.class) &&
                isObject(URI.create(statement.getSubject()), SBEntity.class);

    }
  
    private <T extends SBEntity> T retrieveObject(URI identity, Class<T> clazz) {

        if(objectCache.containsKey(identity)) {
            SBEntity o = objectCache.get(identity);
            if(clazz.isAssignableFrom(o.getClass())) {
                return clazz.cast(o);
            }
        }

        for(Class fClass : factories.keySet()) {
            if(clazz.isAssignableFrom(fClass)) {
                if(factories.get(fClass).isEntity(identity.toASCIIString(), rdfService)) {
                    T object = clazz.cast(factories.get(fClass).getEntity(identity.toASCIIString(), rdfService));
                    objectCache.put(identity, object);
                    graphCache.addNode(object);
                    return object;
                }    
            }
        }
        
        return null;
    }

    private SynBadEdge retrieveEdge(Statement statement) {

        if(edgeCache.containsKey(statement))
            return edgeCache.get(statement);
        
        if(isEdge(statement)) {
            SynBadEdge e =  new SynBadEdge(URI.create(statement.getPredicate()));
            edgeCache.put(statement, e);
            graphCache.addEdge(
                    retrieveObject(URI.create(statement.getSubject()), SynBadObject.class),
                    retrieveObject(statement.getObject().asURI(), SynBadObject.class),
                    e);
            return e;
        }
               
        return null;
    }

    @Override
    public <T extends SBEntity> boolean containsObject(URI identity, Class<T> clazz) {
        return isObject(identity, clazz);
    }

    @Override
    public <T extends SBEntity> T getObject(URI identity, Class<T> clazz) {
        return retrieveObject(identity, clazz);
    }

    @Override
    public <T extends SBEntity> Collection<T> getObjects(Class<T> clazz) {  
    
        // TO DO: Update to be able to supply super classes.
        
        Map<String, T> matches = new HashMap<>();

        objectCache.keySet().stream().filter(key -> clazz.isAssignableFrom(objectCache.get(key).getClass())).forEach(key -> matches.put(key.toASCIIString(), clazz.cast(objectCache.get(key))));
   
        rdfService.getStatements(null, SynBadTerms2.SbolIdentified.hasDisplayId, null).stream()
                .map(Statement::getSubject)
                .filter(s -> !matches.containsKey(s))
                .map(URI::create)
                .filter(s -> isObject(s, clazz))
                .map(s -> retrieveObject(s, clazz))
                .forEach(o -> matches.put(o.getIdentity().toASCIIString(), o));
        
        return matches.values();
    }
    
    /**
     * Returns Collection of {@link SynBadEdge}, that represent predicates
     * between URIs that are recognised as {@link SynBadObject}s.
     * 
     * @param <T>
     * @param subject
     * @param sources
     * @return 
     */
    @Override
    public List<SynBadEdge> outgoingEdges(SBEntity subject) {
       
        return rdfService.getStatements(
            subject.getIdentity().toASCIIString(),
            null, 
            null).stream()
                .filter(this::isEdge)
                .map(this::retrieveEdge)
                .collect(Collectors.toList());
    }

    /**
     * 
     * @param <T>
     * @param subject
     * @param sources
     * @return 
     */
    @Override
    public List<SynBadEdge> outgoingEdges(SBEntity subject, String predicate) {
        
        // after finding a URI, check if already exists before testing factories...
          
        return rdfService.getStatements(
            subject.getIdentity().toASCIIString(),
            predicate, 
            null).stream()
                .filter(s -> s.getPredicate().equals(predicate))
                .filter(this::isEdge)
                .map(this::retrieveEdge)
                .collect(Collectors.toList());
    }
    
    @Override
    public List<SynBadEdge> incomingEdges(SBEntity node) {
        
        // after finding a URI, check if already exists before testing factories...
        
        return rdfService.getStatements(null,
            null, 
            new SBValue(node.getIdentity())).stream()
                .filter(this::isEdge)
                .map(this::retrieveEdge)
                .collect(Collectors.toList());
    }

    @Override
    public List<SynBadEdge> incomingEdges(SBEntity node, String predicate) {
        
        // after finding a URI, check if already exists before testing factories...
        
        return rdfService.getStatements(null,
            predicate, 
            new SBValue(node.getIdentity())).stream()
                .filter(s -> s.getPredicate().equals(predicate))
                .filter(this::isEdge)
                .map(this::retrieveEdge)
                .collect(Collectors.toList());
    }


    @Override
    public <T extends SBEntity, V extends SBEntity> Collection<SynBadEdge> outgoingEdges(T subject, String predicate, Class<V> objectClass) {

        // after finding a URI, check if already exists before testing factories...
        
        return rdfService.getStatements(
            subject.getIdentity().toASCIIString(),
            predicate, 
            null).stream()
                .filter(s -> s.getObject().isURI())
                .filter(this::isEdge)
                .filter(s -> isObject(s.getObject().asURI(), objectClass))
                .map(this::retrieveEdge)
                .collect(Collectors.toSet());
    }
    
    @Override
    public <V extends SBEntity> List<SynBadEdge> incomingEdges(SBEntity subject, String predicate, Class<V> objectClass) {

        // after finding a URI, check if already exists before testing factories...
        
        return rdfService.getStatements(null,
            predicate,
            new SBValue(subject.getIdentity())).stream()
                .filter(this::isEdge)
                .filter(s -> isObject(s.getObject().asURI(), objectClass))
                .map(this::retrieveEdge)
                .collect(Collectors.toList());

    }

    @Override
    public SBEntity getEdgeSource(SynBadEdge edge) {
        return graphCache.getEdgeSource(edge);
    }

    @Override
    public SBEntity getEdgeTarget(SynBadEdge edge) {
        return graphCache.getEdgeTarget(edge);
    }
    
    

    @Override
    public SBCursor<SBEntity, SynBadEdge> getCursor(SBEntity startingNode) {
        return new SBCursor.ASBCursor<>(startingNode, new SBEdgeProvider<SBEntity, SynBadEdge>(){

            @Override
            public SBEntity getEdgeSource(SynBadEdge edge) {
                return DefaultWorkspace.this.getEdgeSource(edge);
            }

            @Override
            public SBEntity getEdgeTarget(SynBadEdge edge) {
                return DefaultWorkspace.this.getEdgeTarget(edge);
            }
            
            @Override
            public List<SynBadEdge> incomingEdges(SBEntity node) {
                return outgoingEdges(node).stream().filter(e -> node.getIdentity().equals(getEdgeSource(e).getIdentity())).collect(Collectors.toList());
            }

            @Override
            public List<SynBadEdge> incomingEdges(SBEntity node, String predicate) {
                return outgoingEdges(node, predicate).stream().filter(e -> node.getIdentity().equals(getEdgeTarget(e).getIdentity())).collect(Collectors.toList());
            }

            @Override
            public List<SynBadEdge> outgoingEdges(SBEntity node) {
                return DefaultWorkspace.this.outgoingEdges(node).stream().filter(e -> node.getIdentity().equals(getEdgeSource(e).getIdentity())).collect(Collectors.toList());
            }

            @Override
            public List<SynBadEdge> outgoingEdges(SBEntity node, String predicate) {
                return DefaultWorkspace.this.outgoingEdges(node, predicate).stream().filter(e -> node.getIdentity().equals(getEdgeTarget(e).getIdentity())).collect(Collectors.toList());
            }
            
        });
    }

    
    public void apply(SynBadCommand action) {
        if(action instanceof CreateObjectAction) {
            
        } else if (action instanceof RemoveObjectAction) {
            
        }
    }

    @Override
    public void shutdown() {
        rdfService.shutdown();
    }

    @Override
    public void addRdf(InputStream stream, URI sourceId) {
        rdfService.addRdf(stream, sourceId);
    }

    @Override
    public URI getRepositoryId() {
        return workspaceId;
    }

    @Override
    public void perform(SynBadCommand command) {
        commander.perform(command, rdfService);
    }

    @Override
    public void undo() {
        if(commander.hasUndo())
            commander.undo();
    }

    @Override
    public void redo() {
        if(commander.hasRedo())
            commander.redo();
    }

    @Override
    public SBDispatcher getDispatcher() {
        return dispatcher;
    }
    



}
