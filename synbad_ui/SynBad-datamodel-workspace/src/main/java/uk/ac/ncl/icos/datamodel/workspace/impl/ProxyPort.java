/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.datamodel.workspace.impl;

import uk.ac.ncl.icos.datamodel.workspace.objects.Identity;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import uk.ac.ncl.icos.datamodel.workspace.objects.IPort;
import java.net.URI;
import org.apache.commons.collections4.MultiValuedMap;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms2;
import uk.ac.ncl.icos.datamodel.workspace.objects.SBValueProvider;
import uk.ac.ncl.icos.synbad.graph.api.SBValue;

/**
 *
 * @author owengilfellon
 */
@XStreamAlias("ProxyPort")
public class ProxyPort  extends IPort {
    
    private final IPort port;

    public ProxyPort(Identity identity, URI workspaceIdentity, SBValueProvider values) {
        super( identity, workspaceIdentity, values);
        this.port = ws.outgoingEdges(this, SynBadTerms2.SynBadPort.proxies, IPort.class)
                .stream().map(e -> ws.getObject(ws.getEdgeTarget(e).getIdentity(), IPort.class))
                .findFirst().orElse(null);
    }

    @Override
    public PortDirection getPortDirection() {
        return port.getPortDirection();
    }
    
    @Override
    public PortType getPortType() {
        return port.getPortType();
    }

    @Override
    public PortState getPortState() {
        return port.getPortState();
    }

    @Override
    public boolean isProxy() {
        return true;
    }

    public IPort getProxied() {
        return port;
    }

    @Override
    public String toString() {
        PortState state = getPortState();
        return getPortDirection() + ":" + getPortType() + (state!=null ? ":" + state : "");
    }

    @Override
    public boolean isConnectable(IPort port) {
        return this.port.isConnectable(port);
    }

} 