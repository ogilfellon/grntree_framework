/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.datamodel.workspace.objects;

import java.net.URI;
import java.util.Objects;

/**
 *
 * @author owengilfellon
 */
public final class Identity  {
    
    private final URI identity;
    private final URI persistentId;
    private final String uriPrefix;
    private final String displayId;
    private final String version;

    

    public Identity(String uriPrefix, String displayId, String version) {
        this.identity = URI.create(uriPrefix + "/" +  displayId + "/" + version);
        this.persistentId = URI.create(uriPrefix + "/" +  displayId);
        this.uriPrefix = uriPrefix;
        this.displayId = displayId;
        this.version = version;
    }

    public URI getIdentity() {
        return identity;
    }

    public URI getPersistentId() {
        return persistentId;
    }

    public String getUriPrefix() {
        return uriPrefix;
    }

    public String getDisplayID() {
        return displayId;
    }

    public String getVersion() {
        return version;
    }

    @Override
        public boolean equals(Object obj) {
            if(obj == null)
                return false;
            if(obj == this)
                return true;
            if(!(obj instanceof Identity))
                return false;
            
            Identity id = (Identity) obj;
            return id.getIdentity().equals(getIdentity());
        }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 71 * hash + Objects.hashCode(this.identity);
        return hash;
    }

}
