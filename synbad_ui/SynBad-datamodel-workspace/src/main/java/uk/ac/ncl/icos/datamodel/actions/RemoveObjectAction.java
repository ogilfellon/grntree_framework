/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.datamodel.actions;

import uk.ac.ncl.icos.datamodel.workspace.objects.SynBadObject;

/**
 *
 * @author owengilfellon
 */
public abstract class RemoveObjectAction<T extends SynBadObject> extends SynBadCommand {

//    private final static IRdfService ws = SesameService.getService();

    public RemoveObjectAction(Class<T> clazz) {
        
    }
    
    public CommandType getType() {
        return CommandType.REMOVE;
    }

    
}
