/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.sort;

/**
 *
 * @author owengilfellon
 */
public interface WorkspaceAction {

    public void perform();
    public void undo();

    /*
    public static class Macro implements WorkspaceAction {

        private final LinkedList<WorkspaceAction> commands;
        
        public Macro(WorkspaceAction... actions) {
            this.commands = new LinkedList<>();
            commands.addAll(Arrays.asList(actions));
        }

        @Override
        public void perform() {
            commands.stream().forEach((WorkspaceAction action) -> action.perform());
        }

        @Override
        public void undo() {
            commands.descendingIterator().forEachRemaining((WorkspaceAction action) -> action.undo());
        }
    }
    
    public static class AddEntity implements WorkspaceAction{

        private final IWorkspace workspace;
        private final SynBadObject entity;
        
        public AddEntity(IWorkspace workspace, SynBadObject entity) {
            this.entity = entity;
            this.workspace = workspace;
        }

        @Override
        public void perform() {
            if(!workspace.containsEntity(entity.getIdentity()))
                workspace.addEntity(entity);
        }

        @Override
        public void undo() {
            workspace.removeEntity(entity);
        }
    }

    
    public static class RemoveEntity implements WorkspaceAction {

        private final IWorkspace workspace;
        private final SynBadObject entity;
        
        public RemoveEntity(IWorkspace workspace, SynBadObject entity) {
            this.entity = entity;
            this.workspace = workspace;
        }

        @Override
        public void perform() {
            workspace.removeEntity(entity);
        }

        @Override
        public void undo() {
            workspace.addEntity(entity);
        }

    }
    

    public static class RemoveChild implements WorkspaceAction {

        private final SynBadObject parent;
        private final EntityInstance child;
        private final int index;
        
        public RemoveChild(SynBadObject parent, EntityInstance child, int index) {
            this.parent = parent;
            this.child = child;
            this.index = index;
        }

        @Override
        public void perform() {
           // parent.removeChild(child);
        }

        @Override
        public void undo() {
            //parent.addChild(index, child);
        }
    }

    public static class RemovePort implements WorkspaceAction {

        private final PEntity entity;
        private final IPort port;
  
        public RemovePort(SynBadObject entity, IPort port) {
            this.entity = entity;
            this.port = port;
        }

        @Override
        public void perform() {
            entity.removePort(port);
        }

        @Override
        public void undo() {
            entity.addPort(port);
        }

    }

    public static class RemoveWire implements WorkspaceAction {

        private final SynBadObject entity;
        private final IWire wire;
  
        public RemoveWire(SynBadObject entity, IWire wire) {
            this.entity = entity;
            this.wire = wire;
        }

        @Override
        public void perform() {
            entity.removeWire(wire);
        }

        @Override
        public void undo() {
            entity.addWire(wire);
        }

    }
    
    public static class AddAnnotation implements WorkspaceAction {

        private final SynBadObject annotatable;
        private final Annotation annotation;
  
        public AddAnnotation(SynBadObject entity, Annotation annotation) {
            this.annotatable = entity;
            this.annotation = annotation;
        }

        @Override
        public void perform() {
            annotatable.addAnnotation(annotation.getQName(), annotation);
        }

        @Override
        public void undo() {
            annotatable.removeAnnotation(annotation.getQName(), annotation);
        }

    }
    
    public static class RemoveAnnotation implements WorkspaceAction {

        private final SynBadObject annotatable;
        private final Annotation annotation;
  
        public RemoveAnnotation(SynBadObject entity, Annotation annotation) {
            this.annotatable = entity;
            this.annotation = annotation;
        }

        @Override
        public void perform() {
            annotatable.removeAnnotation(annotation.getQName(), annotation);
        }

        @Override
        public void undo() {
            annotatable.addAnnotation(annotation.getQName(), annotation);
        }

    }
    
    public static class SetDescription implements WorkspaceAction {

        private final IIdentified identified;
        private final String oldDescription;
        private final String newDescription;
  
        public SetDescription(IIdentified identified, String description){
            this.identified = identified;
            this.oldDescription = identified.getDescription();
            this.newDescription = description;
        }

        @Override
        public void perform() {
            identified.setDescription(newDescription);
        }

        @Override
        public void undo() {
            identified.setDescription(oldDescription);
        }

    }
    
    public static class SetName implements WorkspaceAction {

        private final IIdentified identified;
        private final String oldName;
        private final String newName;
  
        public SetName(IIdentified identified, String name){
            this.identified = identified;
            this.oldName = identified.getName();
            this.newName = name;
        }

        @Override
        public void perform() {
            identified.setName(newName);
        }

        @Override
        public void undo() {
            identified.setName(oldName);
        }

    }
    
    public static class SetWasDerivedFrom implements WorkspaceAction {

        private final IIdentified identified;
        private final URI oldDerivedFrom;
        private final URI newDerivedFrom;
  
        public SetWasDerivedFrom(IIdentified identified, URI newDerivedFrom){
            this.identified = identified;
            this.oldDerivedFrom = identified.getWasDerivedFrom();
            this.newDerivedFrom = newDerivedFrom;
        }

        @Override
        public void perform() {
            identified.setWasDerivedFrom(newDerivedFrom);
        }

        @Override
        public void undo() {
            identified.setWasDerivedFrom(oldDerivedFrom);
        }

    }*/
}
