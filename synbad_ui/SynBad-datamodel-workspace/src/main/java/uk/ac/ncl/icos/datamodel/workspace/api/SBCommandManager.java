/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.datamodel.workspace.api;

import java.net.URI;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import org.openide.util.Lookup;
import org.openide.util.lookup.ServiceProvider;
import uk.ac.ncl.icos.datamodel.actions.SynBadCommand;

/**
 *
 * @author owengilfellon
 */
public interface SBCommandManager {
    
    public void perform(SynBadCommand action, SBRdfService service);
     
    public boolean hasUndo();
    public boolean hasRedo();
    
    public void undo();
    public void redo();

    public class DefaultCommandManager implements SBCommandManager {

        private final SBDispatcher eventService;
        private final URI workspaceIdentity;
        private final List<SynBadCommand> actions;
        private int currentIndex;
        private final SBRdfService service;
        
        public DefaultCommandManager(URI workspaceIdentity, SBRdfService service, SBDispatcher dispatcher) {
            this.actions = new LinkedList<>();
            this.currentIndex = -1;
            this.service = service;
            this.workspaceIdentity = workspaceIdentity;
            this.eventService = dispatcher;
        }
        
        @Override
        public void perform(SynBadCommand action, SBRdfService service) {
  
            if(hasRedo()) {
                Iterator<SynBadCommand> i = actions.listIterator(currentIndex + 1);
                while(i.hasNext()) {
                    i.next();
                    i.remove();
                }
            }
            
            actions.add(action);
            actions.get(++currentIndex).perform(service);
            actions.get(currentIndex).onPerformed(eventService);
        }

        @Override
        public boolean hasUndo() {
            return currentIndex >= 0;
        }

        @Override
        public boolean hasRedo() {
            return currentIndex < actions.size()-1;
        }

        @Override
        public void undo() {
            if(hasUndo())
                actions.get(currentIndex--).undo(service);
        }

        @Override
        public void redo() {
            if(hasRedo())
                actions.get(++currentIndex).perform(service);
        }

    }
    
}