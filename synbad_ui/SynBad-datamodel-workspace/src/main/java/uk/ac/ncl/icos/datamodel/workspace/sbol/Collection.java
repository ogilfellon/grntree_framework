/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.datamodel.workspace.sbol;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.sbolstandard.core2.TopLevel;
import uk.ac.ncl.icos.datamodel.workspace.api.SBEntity;
import uk.ac.ncl.icos.datamodel.workspace.objects.Identity;
import uk.ac.ncl.icos.datamodel.workspace.objects.SBValueProvider;
import uk.ac.ncl.icos.datamodel.workspace.objects.SynBadObject;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms2;

/**
 *
 * @author owengilfellon
 */
public class Collection extends SynBadObject implements RootObject {

    public Collection(Identity identity, URI workspaceIdentity, SBValueProvider provider) {
        super(identity, workspaceIdentity, provider);
    }
    
    public List<SBEntity> getMembers() {
        return ws.outgoingEdges(this, SynBadTerms2.SbolCollecton.hasMember, SBEntity.class)
            .stream().map(e -> ws.getEdgeTarget(e)).collect(Collectors.toList());
    }

    @Override
    public List<NestedObject> getNestedObjects() {
        return new ArrayList<>();
    }
        
    
}
