/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.datamodel.factories.persistence;


import uk.ac.ncl.icos.synbad.datadefinition.types.Role;
import uk.ac.ncl.icos.synbad.datadefinition.types.Type;

/**
 *
 * @author owengilfellon
 */
public class SparqlHelper {
    
    public static String withRole(Role role) {
        return "PREFIX sbol: <http://sbols.org/v2#>" +
                " SELECT ?x ?y "
                + "WHERE { ?x sbol:persistentIdentity ?y . " 
                + "?x sbol:role <"+ role.getUri().toASCIIString() +"> }";
    }
    
    public static String withType(Type type) {
        return "PREFIX sbol: <http://sbols.org/v2#>" +
                " SELECT ?x ?y "
                + "WHERE { ?x sbol:persistentIdentity ?y . " 
                + "?x sbol:type <"+ type.getUri().toASCIIString() +"> }";
    }
    
    public static String withPersistentIdentity(String persistentId) {
        return "PREFIX sbol: <http://sbols.org/v2#>" +
                " SELECT ?x "
                + "WHERE { ?x sbol:persistentIdentity <" + persistentId + ">" ;
    }
 
}
