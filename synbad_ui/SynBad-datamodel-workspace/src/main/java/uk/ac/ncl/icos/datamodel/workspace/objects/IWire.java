/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.datamodel.workspace.objects;

import java.net.URI;
import org.apache.commons.collections4.MultiValuedMap;
import uk.ac.ncl.icos.datamodel.workspace.api.SBRdfService;
import uk.ac.ncl.icos.datamodel.workspace.api.SBWorkspace;
import uk.ac.ncl.icos.datamodel.workspace.impl.PortRecord;
import uk.ac.ncl.icos.synbad.graph.api.SBValue;

/**
 *
 * @author owengilfellon
 */
public class IWire extends SynBadObject {

    private final PortRecord from;
    private final PortRecord to;

    public IWire(Identity identity, PortRecord from, PortRecord to, URI workspaceId, SBValueProvider values) {
        super(identity, workspaceId, values);
        this.from = from;
        this.to = to;
    }

    public PortRecord getFrom() {
        return from;
    }

    public PortRecord getTo() {
        return to;
    }

    @Override
    public String toString() {
        return from.getOwner() + "." + from + 
                "->" + to.getOwner() + "." + to;
    }

}
