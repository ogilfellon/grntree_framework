/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.datamodel.workspace.sbol;

import java.net.URI;
import uk.ac.ncl.icos.datamodel.workspace.objects.Identity;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms2;
import uk.ac.ncl.icos.datamodel.workspace.objects.SBValueProvider;

/**
 *
 * @author owengilfellon
 */
public class Module extends Pointer<ModuleDefinition> {

    public Module(Identity identity, URI workspaceIdentity, SBValueProvider values) {
        super(identity, workspaceIdentity, values);
    }

    @Override
    public ModuleDefinition getDefinition() {
        return ws.outgoingEdges(this, 
                    SynBadTerms2.Sbol.definedBy,
                    ModuleDefinition.class).stream()
                .map(e -> ws.getObject(ws.getEdgeTarget(e).getIdentity(), ModuleDefinition.class))
                .findFirst().orElse(null);
    }
}
