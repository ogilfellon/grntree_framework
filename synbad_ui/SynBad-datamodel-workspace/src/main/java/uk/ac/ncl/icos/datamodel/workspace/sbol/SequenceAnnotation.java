/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.datamodel.workspace.sbol;

import java.net.URI;
import uk.ac.ncl.icos.datamodel.workspace.objects.Identity;
import uk.ac.ncl.icos.datamodel.workspace.objects.SynBadEdge;
import uk.ac.ncl.icos.datamodel.workspace.objects.SynBadObject;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms2;
import uk.ac.ncl.icos.datamodel.workspace.objects.SBValueProvider;

/**
 *
 * @author owengilfellon
 */
public class SequenceAnnotation extends SynBadObject {

    public SequenceAnnotation(Identity identity, URI workspaceIdentity, SBValueProvider values) {
        super(identity, workspaceIdentity, values);
    }
    
    public Location getLocation() {
        
         return ws.outgoingEdges(this, SynBadTerms2.SbolSequenceAnnotation.hasLocation, Location.class).stream()
                .map((SynBadEdge e) -> ws.getObject(ws.getEdgeTarget(e).getIdentity(), Location.class)).findFirst().orElse(null);
    }
    
    public Component getComponent() {
        return ws.outgoingEdges(this, SynBadTerms2.SbolComponent.hasComponent, Component.class).stream()
                .map((SynBadEdge e) -> ws.getObject(ws.getEdgeTarget(e).getIdentity(), Component .class)).findFirst().orElse(null);
    }
    
    
    
}
