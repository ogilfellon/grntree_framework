/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.datamodel.workspace.api.rdf;

import java.net.URI;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.openide.util.Lookup;
import uk.ac.ncl.icos.synbad.event.SBEventType;
import uk.ac.ncl.icos.datamodel.workspace.api.SBEvents.StatementEvent;
import uk.ac.ncl.icos.datamodel.workspace.api.SBWorkspace;
import uk.ac.ncl.icos.datamodel.workspace.api.SBWorkspaceManager;
import uk.ac.ncl.icos.datamodel.workspace.objects.SynBadObject;
import uk.ac.ncl.icos.datamodel.workspace.objects.SBValueProvider;
import uk.ac.ncl.icos.synbad.event.SBEvent;
import uk.ac.ncl.icos.synbad.graph.api.SBValue;

/**
 *
 *
 * @author owengilfellon
 */
 

public class DefaultValueProvider implements SBValueProvider {

    private final SBWorkspace ws;
    private final Map<String, Set<SBValue>> values;

    public DefaultValueProvider(URI workspaceIdentity) {
        this.values = new HashMap<>();
        this.ws = Lookup.getDefault().lookup(SBWorkspaceManager.class)
                .getWorkspace(workspaceIdentity);
    }

    @Override
    public Set<String> getPredicates() {
        return values.keySet();
    }

    @Override
    public Set<SBValue> getValues(String predicate) {
        return values.containsKey(predicate) ? values.get(predicate) : new HashSet<>();
    }

    @Override
    public void addValue(String predicate, SBValue value) {
        if(!values.containsKey(predicate))
            values.put(predicate, new HashSet<>());

        values.get(predicate).add(value);
    }

    @Override
    public void setValues(String predicate, Collection<SBValue> valueCollection) {
        clearValues(predicate);
        values.put(predicate, new HashSet<>());
        values.get(predicate).addAll(valueCollection);
    }

    @Override
    public void removeValue(String predicate, SBValue value) {

        if(!values.containsKey(predicate))
            return;

        values.get(predicate).remove(value);

        if(values.get(predicate).isEmpty())
            values.remove(predicate);

    }

    @Override
    public void clearValues(String predicate) {
        values.remove(predicate);
    }

    @Override
    public void onEvent(SBEvent e) {
        if(e instanceof StatementEvent) {
            StatementEvent event = (StatementEvent) e;
            
            for(Statement s : event.getObject()) {
                if(event.getType() == SBEventType.ADDED) {
                    if(!s.getObject().isURI() || !ws.containsObject(s.getObject().asURI(), SynBadObject.class)) {
                        addValue(s.getPredicate(), s.getObject());
                    }
                } else if (event.getType() == SBEventType.REMOVED) {
                    if(!s.getObject().isURI() || !ws.containsObject(s.getObject().asURI(), SynBadObject.class)) {
                        removeValue(s.getPredicate(), s.getObject());
                    }
                }
            }
        }
    }

}