/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.datamodel.workspace.impl;

import java.net.URI;
import org.apache.commons.collections4.MultiValuedMap;
import uk.ac.ncl.icos.datamodel.workspace.objects.Identity;
import uk.ac.ncl.icos.datamodel.workspace.objects.SynBadObject;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms2;
import uk.ac.ncl.icos.datamodel.workspace.objects.SBValueProvider;
import uk.ac.ncl.icos.synbad.graph.api.SBValue;

/**
 *
 * @author owengilfellon
 */

public class PortRecord extends SynBadObject{

    public PortRecord(Identity identity, URI workspaceIdentity, SBValueProvider values) {
        super(identity, workspaceIdentity, values);

    }
       
    public SynBadObject getOwner() {
        return ws.incomingEdges(this, SynBadTerms2.SynBadEntity.hasPort)
            .stream().map(e -> ws.getObject(ws.getEdgeSource(e).getIdentity(), SynBadObject.class))
            .findFirst().orElse(null);
    }

    public boolean isWired() {
        /*for(IWire wire : owner.getParent().getWires()) {
            if(wire.getFrom().getIdentity().equals(getIdentity())
                    || wire.getTo().getIdentity().equals(getIdentity()))
                return true;
        }*/
        return false;
    }
    
}

