/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.datamodel.factories.persistence;

/**
 *
 * @author owengilfellon
 */
public class XmlParser {
    
    /*
    public static String getXml(IWorkspace workspace) {
        XStream xstream = new XStream();
        xstream.autodetectAnnotations(true);
        xstream.alias("URI", URI.class);
        xstream.alias("QName", QName.class);
        xstream.registerConverter(new UriConverter());
        xstream.registerConverter(new QNameConverter());
     //   xstream.registerConverter(new IdentifiedConverter());
        //xstream.setMode(XStream.XPATH_ABSOLUTE_REFERENCES);
        return xstream.toXML(workspace);
    }
    
    public static IWorkspace getWorkspace(String xml) {
        XStream xstream = new XStream();
        xstream.autodetectAnnotations(true);
        xstream.alias("Workspace",   Workspace.class);
        xstream.alias("EntityDefinition", SynBadPObject.class);
        xstream.alias("URI", URI.class);
        xstream.alias("QName", QName.class);
        xstream.registerConverter(new UriConverter());
        xstream.registerConverter(new QNameConverter());
        //xstream.setMode(XStream.XPATH_ABSOLUTE_REFERENCES);
        return (IWorkspace) xstream.fromXML(xml);
    }
}

class IdentifiedConverter implements Converter{

    @Override
    public boolean canConvert(Class type) {
        return type == IIdentified.class;
    }

    @Override
    public void marshal(Object source, HierarchicalStreamWriter writer, MarshallingContext context) {
        IIdentified identified = (IIdentified) source;
        
        writer.startNode("identity");
        writer.setValue(identified.getIdentity().toASCIIString());
        
        URI wasDerivedFrom = identified.getWasDerivedFrom();
        String displayID = identified.getDisplayID();
        String name = identified.getName();
        String version = identified.getVersion();
        String description = identified.getDescription();
        
        if(wasDerivedFrom != null ) {
            writer.startNode("wasDerivedFrom");
            writer.setValue(wasDerivedFrom.toASCIIString());
            writer.endNode();
        }
        
        if(displayID != null && !displayID.equals("")) {
            writer.startNode("displayID");
            writer.setValue(displayID);
            writer.endNode();
        }
        
        if(name != null && !name.equals("")) {
            writer.startNode("name");
            writer.setValue(name);
            writer.endNode();
        }
        
        if(version != null && !version.equals("")) {
            writer.startNode("version");
            writer.setValue(version);
            writer.endNode();
        }
        
        if(description != null && !description.equals("")) {
            writer.startNode("description");
            writer.setValue(description);
            writer.endNode();
        }
            
    }

    @Override
    public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
        return null;
        
    }
}

class UriConverter implements Converter{

    @Override
    public boolean canConvert(Class type) {
        return type == URI.class;
    }

    @Override
    public void marshal(Object source, HierarchicalStreamWriter writer, MarshallingContext context) {
        URI uri = (URI) source;
        writer.setValue(uri.toASCIIString());
    }

    @Override
    public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
        return URI.create(reader.getValue());
    }
}

class QNameConverter implements Converter{

    @Override
    public boolean canConvert(Class type) {
        return type == QName.class;
    }

    @Override
    public void marshal(Object source, HierarchicalStreamWriter writer, MarshallingContext context) {
        QName qname = (QName) source;
        writer.addAttribute("ns", qname.getNamespaceURI());
        writer.addAttribute("localPart", qname.getLocalPart());
        writer.addAttribute("prefix", qname.getPrefix());
    }

    @Override
    public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
        return new QName(reader.getAttribute("ns"),
                reader.getAttribute("localPart"),
                reader.getAttribute("prefix"));
    }*/
}
