/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.datamodel.workspace.api;

import java.io.File;
import java.io.InputStream;
import java.net.URI;
import java.util.List;
import java.util.Set;
import uk.ac.ncl.icos.datamodel.workspace.api.rdf.Statement;
import uk.ac.ncl.icos.synbad.graph.api.SBValue;

/**
 * RdfService abstracts an RDF triple store.
 * 
 * @author owengilfellon
 */
public interface SBRdfService {

    public URI getWorkspaceId();
    
    // ========= Loading and unloading files ==========
    
    public boolean addRdf(InputStream stream, URI sourceId);
    
    public boolean addRdf(File f);
    
    public boolean exportRdf(File f);
    
    public boolean removeRdf(File f);
    
    // ========== Accessors =============
   

    public Set<Statement> getStatements(String subject, String predicate, SBValue object);
 
    public Set<SBValue> getValues(String subject, String predicate);
    
    public SBValue getValue(String subject, String predicate);
    
    // ======== Mutators ========
    
    public boolean addStatements(List<Statement> statement);
    
    public boolean removeStatements(List<Statement> statement);

    public void shutdown();
    
}
