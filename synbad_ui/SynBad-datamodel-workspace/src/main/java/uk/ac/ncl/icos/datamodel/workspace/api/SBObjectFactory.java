/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.datamodel.workspace.api;

import java.net.URI;
import java.util.Arrays;
import org.openide.util.Lookup;
import uk.ac.ncl.icos.datamodel.actions.StatementsAction;
import uk.ac.ncl.icos.datamodel.actions.SynBadCommand;
import uk.ac.ncl.icos.datamodel.workspace.api.rdf.EntityConstructor;
import uk.ac.ncl.icos.datamodel.workspace.api.rdf.EntityConstructor.DefaultConstructor;
import uk.ac.ncl.icos.datamodel.workspace.api.rdf.EntityConstructor.IdentityConstructor;
import uk.ac.ncl.icos.datamodel.workspace.api.rdf.EntityIdentifier;
import uk.ac.ncl.icos.datamodel.workspace.api.rdf.EntityIdentifier.IdentifyByType;
import uk.ac.ncl.icos.datamodel.workspace.api.rdf.Statement;
import uk.ac.ncl.icos.datamodel.workspace.api.rdf.Statements;
import uk.ac.ncl.icos.synbad.graph.api.SBValue;
import uk.ac.ncl.icos.datamodel.workspace.objects.Identity;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms2;
import uk.ac.ncl.icos.synbad.datadefinition.UriHelper;

/**
 * Domain Factories are used by a Workspace to identify and retrieve 
 * domain objects from an RDFService.
 * @author owengilfellon
 */
public interface SBObjectFactory<T> {
    
    public boolean isEntity(String uri, SBRdfService service);
    public T getEntity(String uri, SBRdfService c);
    public Class<T> getEntityClass();
    public URI getType();
 
    public static class IdentityFactory extends ADomainFactory<Identity> {

        public IdentityFactory() {
            super(  Identity.class,
                    UriHelper.sbol.namespacedUri("Identity"),
                    (String t, SBRdfService u) -> { return 
                        u.getValue( t,SynBadTerms2.SbolIdentified.hasDisplayId) != null &&
                        u.getValue( t, SynBadTerms2.SbolIdentified.hasVersion) != null;},
                    new IdentityConstructor());}

        public static StatementsAction createIdentifiedAction(Identity identity) {
            return new StatementsAction(
                SynBadCommand.CommandType.ADD, new Statements(Arrays.asList(new Statement(identity.getIdentity().toASCIIString(), SynBadTerms2.SbolIdentified.hasDisplayId, new SBValue(identity.getDisplayID())),
                    new Statement(identity.getIdentity().toASCIIString(), SynBadTerms2.SbolIdentified.hasVersion, new SBValue(identity.getVersion())),
                    new Statement(identity.getIdentity().toASCIIString(), SynBadTerms2.SbolIdentified.hasPersistentIdentity, new SBValue(identity.getPersistentId())))));
        }
    }

    public class ADomainFactory<T>  implements SBObjectFactory<T> {

        private final URI type;
        private final Class<T> clazz;
        private final EntityIdentifier identifier;
        private final EntityConstructor<T> retriever;
        
        public ADomainFactory(Class<T> clazz, URI type) {
            this(clazz, type, new IdentifyByType(type, clazz), new DefaultConstructor(clazz));
        }
        
        public ADomainFactory(Class<T> clazz, URI type, EntityIdentifier identifier, EntityConstructor<T> retriever) {
            this.identifier = identifier;
            this.retriever = retriever;
            this.clazz = clazz;
            this.type = type;
        }

        @Override
        public boolean isEntity(String uri, SBRdfService service) {
            return identifier.test(uri, service);
        }

        @Override
        public T getEntity(String uri, SBRdfService c) {
            return retriever.apply(uri, c);
        }

        @Override
        public Class<T> getEntityClass() {
            return clazz;
        }

        @Override
        public URI getType() {
            return type;
        }
    }
}
