/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.datamodel.factories.persistence;

/**
 *
 * @author owengilfellon
 */
public class RdfParser {
 /*   
    public static Model getModel(SBOLDocument document) {
        
        Model model = new LinkedHashModelFactory().createEmptyModel();
        
        try {
            RDFParser parser = Rio.createParser(RDFFormat.RDFXML);
            parser.setRDFHandler(new StatementCollector(model));
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            SBOLWriter.write(document, out);
            parser.parse(new ByteArrayInputStream(out.toByteArray()), "");
        } catch (IOException | RDFParseException | RDFHandlerException | XMLStreamException | FactoryConfigurationError | CoreIoException ex) {
            Exceptions.printStackTrace(ex);
        }
        
        return model;
    }
    
    public static SBOLDocument getDocument(Model model) {

        SBOLDocument document = null;
        
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            RDFFormat format = Rio.getWriterFormatForMIMEType("application/rdf+xml").get();
            Rio.write(model, out, format);
            document = SBOLReader.read(new ByteArrayInputStream(out.toByteArray()));
        } catch (CoreIoException | XMLStreamException | FactoryConfigurationError | SBOLValidationException ex) {
            Exceptions.printStackTrace(ex);
        }
        
        return document;
    }
    
    public static void testSPARQL(SBOLDocument document) {
        
        Model model = getModel(document);
        
        Repository repository = new SailRepository(new MemoryStore());
        repository.initialize();
        repository.getConnection().add(model);
        
        String queryString = "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "
                + "PREFIX sbol: <http://sbols.org/v2#> "
                + "SELECT ?id "
                + "WHERE { "
                + "?id sbol:role <http://virtualparts.org/interaction-module>"
                + " }";
               // + "?proxied ?a ?e }"; 
        TupleQuery tupleQuery = repository.getConnection().prepareTupleQuery(QueryLanguage.SPARQL, queryString);
        TupleQueryResult result = tupleQuery.evaluate();
        
        int index = 0;
        
        try {
            while (result.hasNext()) { 
                BindingSet bindingSet = result.next();

             //   Value valueOfE = bindingSet.getValue("e");
           //     Value valueOfA = bindingSet.getValue("a");
                Value valueOfV = bindingSet.getValue("id");
  

                System.out.println("V: " + valueOfV.stringValue());
             //   System.out.println("A: " + valueOfA.stringValue());
              //  System.out.println("E: " + valueOfE.stringValue()); 
   
                System.out.println(); 
                
                index++;
            }
            
            System.out.println("Statements: " + index);
        }
        finally {
            result.close();
        }
        
       
    }
   */ 
}
