/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.datamodel.workspace.api.rdf;

import java.util.ArrayList;
import java.util.Collection;

/**
 *
 * @author owengilfellon
 */
public class Statements extends ArrayList<Statement> {

    public Statements() {
        super();
    }
    
    public Statements(Collection<? extends Statement> c) {
        super(c);
    }

    
}
