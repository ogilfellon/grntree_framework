/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.datamodel.actions;

import uk.ac.ncl.icos.datamodel.workspace.api.SBRdfService;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import uk.ac.ncl.icos.datamodel.workspace.api.SBDispatcher;

/**
 *
 * @author owengilfellon
 */
public abstract class SynBadCommand {
    
    public static enum CommandType {
        ADD,
        REMOVE,
        MODIFY
    }
    
    public abstract void perform(SBRdfService service);
    
    public abstract void undo(SBRdfService service);
    
    public abstract void onPerformed(SBDispatcher dispatcher);
    

    public class SynBadCommandMacro extends SynBadCommand {
        
        private final List<SynBadCommand> actions;

        public SynBadCommandMacro(List<SynBadCommand> actions) {
            this.actions = new ArrayList<>(actions);
        }

        @Override
        public void perform(SBRdfService service) {
            actions.stream().forEach((SynBadCommand a) -> a.perform(service));
        }

        
        @Override
        public void undo(SBRdfService service) {
            List<SynBadCommand> reversed = new LinkedList<>(actions);
            Collections.reverse(reversed);
            reversed.stream().forEach(a -> a.undo(service));
        }

        @Override
        public void onPerformed(SBDispatcher dispatcher) {
            actions.stream().forEach((SynBadCommand a) -> a.onPerformed(dispatcher));
        }
    }
}
