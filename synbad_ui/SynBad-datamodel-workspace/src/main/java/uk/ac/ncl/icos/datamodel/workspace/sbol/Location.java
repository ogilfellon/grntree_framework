/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.datamodel.workspace.sbol;

import java.net.URI;
import java.util.Arrays;
import uk.ac.ncl.icos.datamodel.actions.CreateObjectAction;
import uk.ac.ncl.icos.datamodel.workspace.api.SBDispatcher;
import uk.ac.ncl.icos.datamodel.workspace.api.rdf.Statement;
import uk.ac.ncl.icos.datamodel.workspace.api.rdf.Statements;
import uk.ac.ncl.icos.synbad.graph.api.SBValue;
import uk.ac.ncl.icos.datamodel.workspace.objects.Identity;
import uk.ac.ncl.icos.datamodel.workspace.objects.SynBadObject;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms2;
import uk.ac.ncl.icos.datamodel.workspace.objects.SBValueProvider;
import uk.ac.ncl.icos.synbad.datadefinition.UriHelper;
import uk.ac.ncl.icos.synbad.event.SBEventType;
import uk.ac.ncl.icos.synbad.events.impl.DefaultEvent;

/**
 *
 * @author owengilfellon
 */
public abstract class Location extends SynBadObject {

    public Location(Identity identity, URI workspaceIdentity, SBValueProvider values) {
        super(identity, workspaceIdentity, values);
    }
    
    public URI getOrientation() {
        SBValue v = getValue(SynBadTerms2.SbolLocation.hasOrientation);
        return v.isURI() ? v.asURI() : null;
    }
    
    public void setOrientation() {
        
    }
    
    public static class SetOrientationEvent extends DefaultEvent<URI, URI> {

        public SetOrientationEvent(SBEventType type, URI changeSource, URI changeObject) {
            super(type, changeSource, changeObject);
        }
        
    }
    
    
    public static class SetOrientationAction extends SetValueAction {

        public SetOrientationAction(Location location, URI orientaton) {
            super(  location.getIdentity(),
                    SynBadTerms2.SbolLocation.hasOrientation,
                    new SBValue(orientaton));
        }

        @Override
        public void onPerformed(SBDispatcher dispatcher) {
            super.onPerformed(dispatcher); //To change body of generated methods, choose Tools | Templates.
            dispatcher.publish(new SetOrientationEvent(SBEventType.ADDED, getSubject(), getValue().asURI()));
        }

    }
    
    // ================================
    //             Range
    // ================================
    
    public static class Range extends Location {

        public Range(Identity identity, URI workspaceIdentity, SBValueProvider values) {
            super(identity, workspaceIdentity, values);
        }
        
        public int getStart() {
            SBValue v = getValue(SynBadTerms2.SbolLocation.rangeStart);
            return v.isInt() ? v.asInt() : -1;
        }
        
        public int getEnd() {
            SBValue v = getValue(SynBadTerms2.SbolLocation.rangeEnd) ;
            return v.isInt() ? v.asInt() : -1;
        }      
    }

    public static class CreateRangeAction extends CreateObjectAction<Range> {

        public CreateRangeAction(Identity identity, int start, int end) {
            super(  identity,
                    UriHelper.sbol.namespacedUri("Range").toASCIIString(),
                    new Statements(Arrays.asList(
                        new Statement(
                            identity.getIdentity().toASCIIString(),
                            SynBadTerms2.SbolLocation.rangeStart,
                            new SBValue(start)),
                        new Statement(
                            identity.getIdentity().toASCIIString(),
                            SynBadTerms2.SbolLocation.rangeEnd,
                            new SBValue(end))    
                    )),
                    null);  
        }

    }
    
    // ================================
    //             Cut
    // ================================
    
    public static class Cut extends Location {

        public Cut(Identity identity, URI workspaceIdentity, SBValueProvider values) {
            super(identity, workspaceIdentity, values);
        }
        
        public int getCutAt() {
            SBValue v = getValue(SynBadTerms2.SbolLocation.cutAt) ;
            return v.isInt() ? v.asInt() : -1;
        }
    }

     
    
    public static class CreateCutAction extends CreateObjectAction<Cut> {

        public CreateCutAction(Identity identity, int at) {
            super(  identity,
                    UriHelper.sbol.namespacedUri("Cut").toASCIIString(),
                    new Statements(Arrays.asList(
                        new Statement(
                            identity.getIdentity().toASCIIString(),
                            SynBadTerms2.SbolLocation.cutAt,
                            new SBValue(at))    
                    )),
                    null);  
            
            
        }

    }
}
