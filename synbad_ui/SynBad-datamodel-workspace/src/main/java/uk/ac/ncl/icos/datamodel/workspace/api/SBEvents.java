/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.datamodel.workspace.api;

import java.net.URI;
import uk.ac.ncl.icos.synbad.event.SBEventType;
import uk.ac.ncl.icos.datamodel.workspace.api.rdf.Statements;
import uk.ac.ncl.icos.datamodel.workspace.objects.IPort;
import uk.ac.ncl.icos.datamodel.workspace.objects.IWire;
import uk.ac.ncl.icos.synbad.events.impl.DefaultEvent;

/**
 *
 * @author owengilfellon
 */
public interface SBEvents {

    public static class StatementEvent extends DefaultEvent<URI, Statements> {

        public StatementEvent(SBEventType eventType, URI source, Statements statements) {
            super(eventType, source, statements);
        }
    } 

    public static class PortEvent extends DefaultEvent<URI, IPort> {

        public PortEvent(SBEventType eventType, URI source, IPort object) {
            super(eventType, source, object);
        }
    }

    public static class WireEvent extends DefaultEvent<URI, IWire> {

        public WireEvent(SBEventType eventType, URI source, IWire object) {
            super(eventType, source, object);
        }
    }

    /*
    public class EntityEvent extends AEvent<RdfWorkspace, SynBadObject> {

        public EntityEvent(EventType eventType, RdfWorkspace source, SynBadObject object) {
            super(eventType, source, object);
        }

    } 

    public class InstanceEvent extends AEvent<SynBadObject, EntityInstance> {

        private final int index;
        
        public InstanceEvent(EventType eventType, SynBadObject source, EntityInstance object) {
            super(eventType, source, object);
            index = source.getChildren().size()-1;
        }
        
        public InstanceEvent(EventType eventType, SynBadObject source, EntityInstance object, int index) {
            super(eventType, source, object);
            this.index = index;
        }

        public int getIndex() {
            return index;
        }
    }*/





    /**/
}
