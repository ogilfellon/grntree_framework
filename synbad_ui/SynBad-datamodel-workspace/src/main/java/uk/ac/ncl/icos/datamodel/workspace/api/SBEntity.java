/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.datamodel.workspace.api;

import java.net.URI;
import java.util.Collection;
import uk.ac.ncl.icos.synbad.datadefinition.types.Role;
import uk.ac.ncl.icos.synbad.graph.api.SBObject;
import uk.ac.ncl.icos.synbad.graph.api.SBValue;

/**
 *
 * @author owengilfellon
 */
public interface SBEntity extends SBObject {

    public String getPrefix();
    
    public URI getIdentity();
    
    public String getDisplayId();
    
    public String getVersion();
    
    public String getName();
    
    public URI getPersistentId();

    public URI getWasDerivedFrom();

    public String getDescription();
    
    public Collection<Role> getRoles();

    public void setDescription(String description);

    public void setName(String name);

    public void setWasDerivedFrom(URI derivedFrom);
    
    public void setValue(String predicate, SBValue v);
    
    public void addValue(String predicate, SBValue v);

    public void removeValues(String predicate);
    
    public void removeValue(String predicate, SBValue v);
    
}
