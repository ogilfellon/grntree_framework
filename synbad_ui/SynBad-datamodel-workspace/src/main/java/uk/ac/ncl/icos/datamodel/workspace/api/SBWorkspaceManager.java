/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.datamodel.workspace.api;

import java.net.URI;
import java.util.Set;

/**
 *
 * @author owengilfellon
 */


public interface SBWorkspaceManager {
    
    public SBWorkspace getWorkspace(URI uri);
        
    public void closeWorkspace(URI uri);

    public boolean hasOpenWorkspace(URI uri);
    
    public Set<URI> listWorkspaces();

    
}
