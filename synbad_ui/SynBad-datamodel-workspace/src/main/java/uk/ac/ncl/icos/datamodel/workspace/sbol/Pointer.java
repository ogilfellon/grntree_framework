/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.datamodel.workspace.sbol;

import java.net.URI;
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;
import uk.ac.ncl.icos.datamodel.actions.CreateObjectAction;
import uk.ac.ncl.icos.datamodel.workspace.api.SBDispatcher;
import uk.ac.ncl.icos.datamodel.workspace.api.rdf.Statement;
import uk.ac.ncl.icos.datamodel.workspace.api.rdf.Statements;
import uk.ac.ncl.icos.datamodel.workspace.objects.Identity;
import uk.ac.ncl.icos.datamodel.workspace.objects.SBValueProvider;
import uk.ac.ncl.icos.datamodel.workspace.objects.SynBadObject;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms2;
import uk.ac.ncl.icos.synbad.event.SBEventType;
import uk.ac.ncl.icos.synbad.events.impl.DefaultEvent;
import uk.ac.ncl.icos.synbad.graph.api.SBValue;

/**
 *
 * @author owengilfellon
 */
public abstract class Pointer<T extends Definition> extends SynBadObject implements NestedObject {

    public Pointer(Identity identity, URI workspaceIdentity, SBValueProvider provider) {
        super(identity, workspaceIdentity, provider);
    }

    public abstract T getDefinition();
    
    public Set<MapsTo> getMapsTo() {
        return ws.outgoingEdges(
                this,
                SynBadTerms2.SbolModule.hasModule,
                MapsTo.class).stream()
            .map(e -> ws.getObject(ws.getEdgeTarget(e).getIdentity(), MapsTo.class))
            .collect(Collectors.toSet());
    }
    
    public MapsTo getMapsTo(URI mapsToIdentity) {
        return ws.outgoingEdges(
                this,
                SynBadTerms2.SbolModule.hasModule,
                MapsTo.class).stream()
            .map(e -> ws.getObject(ws.getEdgeTarget(e).getIdentity(), MapsTo.class))
            .filter(m -> m.getIdentity().equals(mapsToIdentity))
            .findFirst().orElse(null);
    }
    
    public void createMapsTo(   Identity identity,
                                URI refinement,
                                Pointer remoteInstance,
                                Pointer localInstance) {
        apply(new CreateMapsToAction(identity, localInstance, refinement, remoteInstance, localInstance));
    }
    
    public void removeMapsTo(URI mapsToIdentity) {
        
    }
    
    // ================================
    //             Events
    // ================================
    
    public static class MapsToCreatedEvent extends DefaultEvent<URI, URI>{

        public MapsToCreatedEvent(URI mapsToIdentity, URI ownerIdentity) {
            super(SBEventType.ADDED, ownerIdentity, mapsToIdentity);
        }
    }
    
    // ================================
    //            Commands
    // ================================

    public static class CreateMapsToAction extends CreateObjectAction<MapsTo> {
        
        public CreateMapsToAction(Identity identity, Pointer owner,
                URI refinement,
                Pointer remoteInstance,
                Pointer localInstance
        ) {
             super( identity,
                    "http://sbols.org/v2#MapsTo",
                    new Statements(Arrays.asList(
                        new Statement( 
                            owner.getIdentity().toASCIIString(),
                            SynBadTerms2.MapsTo.hasMapsTo,
                            new SBValue(identity.getIdentity())),
                        new Statement(
                            identity.getIdentity().toASCIIString(),
                            SynBadTerms2.MapsTo.hasRefinement,
                            new SBValue(refinement)),
                        new Statement(
                            identity.getIdentity().toASCIIString(),
                            SynBadTerms2.MapsTo.hasRemoteInstance,
                            new SBValue(remoteInstance.getIdentity())),
                        new Statement(
                            owner.getIdentity().toASCIIString(),
                            SynBadTerms2.MapsTo.hasLocalInstance,
                            new SBValue(localInstance.getIdentity())))),
                    owner.getIdentity());
             
             
        }
        
        @Override
        public void onPerformed(SBDispatcher service) {
            super.onPerformed(service);
            service.publish(new MapsToCreatedEvent(getObjectIdentity(), getParentIdentity()));
        }
    }

}
