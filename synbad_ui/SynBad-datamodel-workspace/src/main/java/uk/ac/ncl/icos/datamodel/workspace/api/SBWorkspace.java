/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.datamodel.workspace.api;

import uk.ac.ncl.icos.synbad.graph.api.SBCursor;
import java.io.File;
import java.io.InputStream;
import java.net.URI;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import uk.ac.ncl.icos.datamodel.actions.SynBadCommand;
import uk.ac.ncl.icos.datamodel.workspace.objects.Identity;
import uk.ac.ncl.icos.datamodel.workspace.objects.SynBadEdge;
import uk.ac.ncl.icos.datamodel.workspace.objects.SynBadObject;
import uk.ac.ncl.icos.synbad.graph.api.SBEdgeProvider;

/**
 * Workspace abstracts the statements from an RDFService into objects
 * and edges. It also provides classes for issuing commands and dispatching
 * events.
 * @author owengilfellon
 */
public interface SBWorkspace extends SBEdgeProvider<SBEntity, SynBadEdge>{
    
    public URI getRepositoryId();
    
    // =======================
    
    public void addRdf(File file);
    
    public void addRdf(InputStream stream, URI sourceId);
   
    // ======== Identities ==========
    
    public boolean containsIdentity(URI identity);
    
    public Identity getIdentity(URI identity);
    
    public Set<Identity> getIdentities();

    public Set<Identity> getIdentities(URI persistentIdentity);
    
    // ======== Object Factories ==========
    
    public <T extends SBEntity> SBObjectFactory<T> getFactory(Class<T> clazz);
    
    public Set<SBObjectFactory<? extends SBEntity>> getFactories();
    
    // ============ Objects =============

    public <T extends SBEntity> boolean containsObject(URI identity, Class<T> clazz);
    
    public Collection<Class<? extends SBEntity>> getObjectClasses();
    
    public Collection<Class<? extends SBEntity>> getObjectClasses(URI identity);
    
    public <T extends SBEntity> T getObject(URI identity, Class<T> clazz);
    
    public <T extends SBEntity> Collection<T> getObjects(Class<T> clazz);

    // ======= Edges ==========
    
    public <T extends SBEntity, V extends SBEntity> Collection<SynBadEdge> outgoingEdges(T subject, String predicate, Class<V> objectClass);

    public <V extends SBEntity> List<SynBadEdge> incomingEdges(SBEntity subject, String predicate, Class<V> objectClass);
    
    // ======== Commands and Events ==========
    
    public SBCursor<SBEntity, SynBadEdge> getCursor(SBEntity startingNode);
    
    public SBDispatcher getDispatcher();
    
    public void perform(SynBadCommand command);
    
    public void undo();
    
    public void redo();
    
    public void shutdown();

}
