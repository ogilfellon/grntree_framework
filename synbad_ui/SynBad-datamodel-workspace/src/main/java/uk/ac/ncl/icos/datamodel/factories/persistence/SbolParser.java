/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.datamodel.factories.persistence;

import java.util.logging.Logger;

import uk.ac.ncl.icos.synbad.datadefinition.IDataDefinitionManager;

public class SbolParser 
{
    
    private static final Logger logger = Logger.getLogger(SbolParser.class.getName());
    private static final IDataDefinitionManager dataDefinitionManager = new IDataDefinitionManager.DefaultDataDefinitionManager();
    
    // ===================================================================
    //  Export SBOL
    // ===================================================================
    
    private SbolParser() {
    }
    
    /*
    
    public static SBOLDocument write(SesameService source) throws SBOLValidationException {
        
        SBOLDocument document = new SBOLDocument();
        
        document.setDefaultURIprefix(UriHelper.synbad.getNamespaceURI());
        document.addNamespace(UriHelper.synbad.withLocalPart("synbad"));
        
        Map<URI, ModuleDefinition> moduleDefinitions = new HashMap<>();
        Map<URI, ComponentDefinition> componentDefinitions = new HashMap<>();
        Map<SynBadPObject, Set<IWire>> entityToWire = new HashMap<>();
        
        // ==========================================================
        // Create Definitions
        // ==========================================================

        for(SynBadPObject entity : source.getEntities()) {
            if(entity.hasAnnotation(SynBadTerms.Annotatable.entitytype, EntityType.Module.getUri())) {
                moduleDefinitions.put(entity.getIdentity(), createModuleDefinition(document, entity));
                if(!entity.getWires().isEmpty()) {
                    entityToWire.put(entity, new HashSet<>());
                    Iterator<IWire> i = entity.getWires().iterator();
                    while(i.hasNext()) { entityToWire.get(entity).add(i.next()); }
                }
            } else if (entity.hasAnnotation(SynBadTerms.Annotatable.entitytype, EntityType.Component.getUri()))
                componentDefinitions.put(entity.getIdentity(), createComponentDefinition(document, entity));
        }

        // ==========================================================
        // Create instances within definitions
        // ==========================================================

        for(SynBadPObject parentEntity : source.getEntities()) {

            if(parentEntity.hasAnnotation(SynBadTerms.Annotatable.entitytype, EntityType.Module.getUri()) ) {
                
                ModuleDefinition parentDefinition = moduleDefinitions.get(parentEntity.getIdentity());

                for(EntityInstance childInstance : parentEntity.getChildren()) {
                    
                    SynBadPObject childDefinition = childInstance.getValue();

                    // For interactions, create definition and instance, otherwise create instance
                    
                    if (childDefinition.hasAnnotation(SynBadTerms.Annotatable.entitytype, EntityType.Interaction.getUri()))  {
                        Interaction interaction = createInteractionDefinition(document, parentDefinition, childInstance);
                        createPortAnnotations(childDefinition, interaction, document);
                    } else {
                        Identified identified = (childDefinition.hasAnnotation(SynBadTerms.Annotatable.entitytype, EntityType.Component.getUri())) ?
                            parentDefinition.createFunctionalComponent(childInstance.getDisplayID(),
                                    AccessType.PUBLIC,
                                    componentDefinitions.get(childDefinition.getIdentity()).getIdentity(),
                                    DirectionType.INOUT) :
                            parentDefinition.createModule(childInstance.getDisplayID(),
                                    moduleDefinitions.get(childDefinition.getIdentity()).getIdentity());
   
                        createPortAnnotations(childInstance, identified, document); 
                    } 
                }
                
                // If module has wires, add as annotations
                
                
                
                if(entityToWire.containsKey(parentEntity)) {
                    for(IWire wire : entityToWire.get(parentEntity)) {
                        createWireAnnotation(wire, parentDefinition);

                    }
                }
            }
        }

        //GenericTopLevel
        //Collection
        //Sequence

        return document;
    
    }
    
    
    public static IWorkspace read(SBOLDocument document, String uriPrefix, String displayId, String version)
    {
        return read(document, new Workspace(uriPrefix, displayId, version));
    }
    
    
    public static IWorkspace read(SBOLDocument document, IWorkspace workspace)
    {
        List<QName> namespaces = document.getNamespaces();
        Set<Sequence> sequences = document.getSequences();
       
        Map<URI, SynBadPObject> entities = new HashMap<>();
        
        for(ComponentDefinition definition : document.getComponentDefinitions()) {
            entities.put(definition.getIdentity(), parseComponentDefinition(definition, document, workspace));
        }

        for(ModuleDefinition definition : document.getModuleDefinitions()) {
            if(!isInteraction(definition))
                entities.put(definition.getIdentity(), parseModuleDefinition(document, definition, workspace));
        }

        
        //for(Collection collection : document.getCollections()) { }
        
        //for(Sequence sequence : document.getSequences()) { }
        
        
        // Parse instances within modules
        
        for(ModuleDefinition definition : document.getModuleDefinitions()) {

            SynBadPObject parentEntity = entities.get(definition.getIdentity());
            
            // Add child entities
            
            // TODO: Fix the ordering - SBOL stores as unordered set
            
            for(Module module : definition.getModules()) {
                SynBadPObject entity = workspace.getEntity(module.getDefinitionURI());
                if(entity.hasAnnotation(SynBadTerms.Annotatable.entitytype, EntityType.Module.getUri()))
                    parentEntity.addChild(entity);
            }

            for(FunctionalComponent component : definition.getFunctionalComponents()) {

                SynBadPObject entity = workspace.getEntity(component.getDefinitionURI());
                if(entity.hasAnnotation(SynBadTerms.Annotatable.entitytype, EntityType.Component.getUri()))
                    parentEntity.addChild(entity);
                
                //DirectionType directionType = component.getDirection();
                //AccessType accessType = component.getAccess();
                //Set<MapsTo> mapsTo = component.getMapsTos();
            }
            
            // Add interactions

            for(Interaction interaction : definition.getInteractions()) {

                for(Annotation annotation : interaction.getAnnotations()) {
                    if(annotation.getQName().equals(UriHelper.synbad.withLocalPart("definition"))) {
                        ModuleDefinition interactionDefinition = document.getModuleDefinition(annotation.getURIValue());
                        SynBadPObject entity = workspace.createEntity(
                                new Identity(interactionDefinition.getIdentity().getScheme() + "://" +
                                    interactionDefinition.getIdentity().getAuthority()+"#", 
                                    parentEntity.getDisplayID(),
                                    interactionDefinition.getDisplayId(),
                                    interactionDefinition.getVersion()));
                        entities.put(interaction.getIdentity(), entity);
                        entity.addAnnotation(SynBadTerms.Annotatable.entitytype, 
                            new Annotation(SynBadTerms.Annotatable.entitytype, EntityType.Interaction.getUri()));
                        parentEntity.addChild(entity);
                    }
                }
                
                // If no wires, could derive some from participations?
                
                //Set<Participation> participations = interaction.getParticipations();
            }
        }
        
        // Port parsing

        Set<Identified> definitionsWithPorts = new HashSet<>();
        definitionsWithPorts.addAll(document.getComponentDefinitions());
        
        for(ModuleDefinition definition : document.getModuleDefinitions()) {   
            definitionsWithPorts.addAll(definition.getInteractions());
            if(!isInteraction(definition)) {
                definitionsWithPorts.add(definition);
            }
        }
        
        // For all ports, add to maps.
        
        Map<URI, IPort> concretePorts = new HashMap<>();
        Map<URI, List<Annotation>> portToProxies = new HashMap<>();
        Map<URI, SynBadPObject> portToOwner = new HashMap<>();
        Map<URI, List<Annotation>> entityToWire = new HashMap<>();
        
        for(Identified definition : definitionsWithPorts) {    
            for(Annotation annotation : definition.getAnnotations()) {  
                
                // if is proxy, defer creation
                
                if(isPort(annotation) && isProxyPort(annotation)) { 
                    URI proxiedUri = getProxiedPort(annotation);
                    if(!portToProxies.containsKey(proxiedUri)) {
                        portToProxies.put(proxiedUri, new ArrayList<>());
                    }
                    portToProxies.get(proxiedUri).add(annotation);
                    portToOwner.put(annotation.getNestedIdentity(), entities.get(definition.getIdentity()));
                }
                
                // if is port, create now
                
                else if (isPort(annotation)) {
                    IPort port =  parsePortAnnotations( entities.get(definition.getIdentity()), annotation);
                    concretePorts.put(port.getIdentity(), port);
                    if(!portToProxies.containsKey(port.getIdentity())) {
                        portToProxies.put(port.getIdentity(), new ArrayList<>());
                    }
                    portToOwner.put(port.getIdentity(), port.getOwner());
                }
               
                if(isWire(annotation)) {
                    if(entityToWire.get(definition.getIdentity()) == null)
                        entityToWire.put(definition.getIdentity(), new ArrayList<>());
                    entityToWire.get(definition.getIdentity()).add(annotation);
                }
            }
        }
        
        // Create proxy ports recursively from leaf -> root
        
        for(URI uri : concretePorts.keySet()) {
            parseProxyPorts(uri, portToProxies, portToOwner);
        }

        int readwires = 0;
        
        for(URI uri : entityToWire.keySet()) {
            SynBadPObject entity = workspace.getEntity(uri);
            for(Annotation annotation : entityToWire.get(uri)) {
                
                parseWire(entity, annotation); 
                readwires++;

            }
        }
        
        
        
        int workspaceWires = 0;
        
        for(SynBadPObject entity : workspace.getEntities()) {
            for(IWire wire : entity.getWires()) {
                workspaceWires++;
            }
        }
        
        System.out.println(readwires + ":" + workspaceWires);


        return workspace;
    }


    private static ModuleDefinition createModuleDefinition(SBOLDocument document, SynBadPObject module) throws SBOLValidationException {

        ModuleDefinition moduleDefinition = document.getModuleDefinition(module.getIdentity());
        
        if(moduleDefinition != null)
            return moduleDefinition;

        moduleDefinition = document.createModuleDefinition(UriHelper.synbad.getNamespaceURI(), module.getDisplayID(), module.getVersion());
       
        for(Annotation roleAnnotation : module.getAnnotations(SynBadTerms.Annotatable.role)) { moduleDefinition.addRole(roleAnnotation.getURIValue()); }

        moduleDefinition.setName(module.getName());
        if(!module.getDescription().equals(""))
            moduleDefinition.setDescription(module.getDescription());
        
        createIdentifiedData(document, module, moduleDefinition);

        createPortAnnotations(module, moduleDefinition, document);

        return moduleDefinition;
    }
    
    private static ComponentDefinition createComponentDefinition(SBOLDocument document, SynBadPObject component) throws SBOLValidationException {

        if (!component.hasAnnotation(SynBadTerms.Annotatable.entitytype, EntityType.Component.getUri()))
            return null;
        
        Set<URI> typeSet = new HashSet<>(); 
        
        for(Annotation typeAnnotation : component.getAnnotations(SynBadTerms.Annotatable.type)) {typeSet.add(typeAnnotation.getURIValue()); }
        
        ComponentDefinition componentDefinition = document.createComponentDefinition(UriHelper.synbad.getNamespaceURI(), component.getDisplayID(), component.getVersion(), typeSet);
        componentDefinition.setName(component.getName());
        if(!component.getDescription().equals(""))
            componentDefinition.setDescription(component.getDescription());
        
        //definition.setWasDerivedFrom(.......);
        
        createIdentifiedData(document, component, componentDefinition);
        
        createPortAnnotations(component, componentDefinition, document);

        return componentDefinition;
    }
    
    private static Interaction createInteractionDefinition(SBOLDocument document, ModuleDefinition parentDefinition, EntityInstance interactionInstance) throws SBOLValidationException {
        
        ModuleDefinition interactionDefinition = createModuleDefinition(document, interactionInstance.getValue());
        interactionDefinition.createAnnotation(SynBadTerms.Annotatable.type, EntityType.Interaction.getUri());
        
        Set<URI> types = new HashSet<>();
        for(Annotation role : interactionInstance.getValue().getAnnotations(SynBadTerms.Annotatable.role)) { types.add(role.getURIValue()); }
     
        Interaction interaction = parentDefinition.createInteraction(interactionInstance.getDisplayID(), types);
        interaction.createAnnotation(UriHelper.synbad.withLocalPart("definition"), interactionDefinition.getIdentity());
        
        return interaction;
    }
    

    private static void createIdentifiedData(SBOLDocument document, SynBadObject propertied, Identified identified) throws SBOLValidationException {
        String name = propertied.getName() == null ? propertied.getDisplayID() : propertied.getName();
        String description = propertied.getDescription() == null ? "" : propertied.getDescription();
        
        if(propertied.getWasDerivedFrom() != null)
            identified.setWasDerivedFrom(propertied.getWasDerivedFrom());
        
        identified.setName(name);
        identified.setDescription(description);
        
         for(Annotation a : propertied.getAnnotations()) {
            
            if( !isPort(a) &&
                !isProxyPort(a) &&
                !isPortInstance(a) &&
                !isWire(a) && 
                !identified.getAnnotations().stream()
                    .anyMatch(annotation -> annotation.getQName().equals(a.getQName()))) {
                
                if(!document.getNamespaces().contains(a.getQName())) {
                    document.addNamespace(a.getQName());
                }
                
                if(a.isBooleanValue()) {
                    identified.createAnnotation(a.getQName(), a.getBooleanValue());
                } else if (a.isDoubleValue()) {
                    identified.createAnnotation(a.getQName(), a.getDoubleValue());
                } else if (a.isIntegerValue()) {
                    identified.createAnnotation(a.getQName(), a.getIntegerValue());
                } else if (a.isNestedAnnotations()) {
                    identified.createAnnotation(a.getQName(), a.getNestedQName(), a.getNestedIdentity(), a.getAnnotations());
                } else if (a.isStringValue()) {
                    identified.createAnnotation(a.getQName(), a.getStringValue());
                } else {
                    identified.createAnnotation(a.getQName(), a.getURIValue());
                }
            }
            
            
        }

    }
    

    private static void createWireAnnotation(IWire wire, ModuleDefinition definition) throws SBOLValidationException {
        
        PortRecord from = wire.getFrom();
        PortRecord to = wire.getTo();
    
        */
    
    
        /* ======== This was commented out originally....
        List<Annotation> wireDetails = new ArrayList<>();

        String name = wire.getName() == null ? wire.getDisplayID() : wire.getName();
        String description = wire.getDescription() == null ? "" : wire.getDescription();
        URI wasDerivedFrom = wire.getWasDerivedFrom();

        wireDetails.add(new Annotation(SynBadTerms.Annotatable.name, name));
        if(!description.equals("")) wireDetails.add(new Annotation(SynBadTerms.Annotatable.description, description));
        if(wasDerivedFrom!=null) wireDetails.add(new Annotation(SynBadTerms.Annotatable.derivedFrom, wasDerivedFrom));

    
        
    
        // ensure this maps correctly to instance in SBOL
        */
    
    /*
        definition.createAnnotation(SynBadTerms.Types.wire,
            SynBadTerms.Types.wire,
            wire.getIdentity(),
            Arrays.asList(
                new Annotation( SynBadTerms.Wire.from, from.getIdentity()),
                new Annotation( SynBadTerms.Wire.to, to.getIdentity())));

    }
    

    private static void createPortAnnotations(SynBadPObject synbadEntity, Identified sbolEntity, SBOLDocument document) throws SBOLValidationException {

        for(IPort port : synbadEntity.getPorts()) {
            
            List<Annotation> portdetails = new ArrayList<>();
            
            String name = port .getName() == null ? port.getDisplayID() : port.getName();
            String description = port.getDescription() == null ? "" : port.getDescription();
            URI wasDerivedFrom = port.getWasDerivedFrom();
            
            portdetails.add(new Annotation(UriHelper.synbad.withLocalPart("name"), name));
            if(!description.equals("")) portdetails.add(new Annotation(SynBadTerms.Annotatable.description, description));
            if(wasDerivedFrom!=null) portdetails.add(new Annotation(SynBadTerms.Annotatable.derivedFrom, wasDerivedFrom));
            portdetails.add(new Annotation(SynBadTerms.Port.portAccess, port.isPublic()));

            // TODO: currently, proxy references definition - should reference instance?
            
            if(port.isProxy()) {
               IPort proxied = ((ProxyPort)port).getProxied();
               portdetails.add(new Annotation(
                       SynBadTerms.Port.proxies,
                       proxied.getIdentity())); 
            } else {
                portdetails.add(new Annotation(
                        SynBadTerms.Port.portDirection,
                        port.getPortDirection().toString()));

                for(Annotation type : port.getAnnotations(SynBadTerms.Port.portType)) { portdetails.add(type); }

                for(Annotation state : port.getAnnotations(SynBadTerms.Port.portState)) { portdetails.add(state); }
            }
            
            QName qname = port.isProxy() ? SynBadTerms.Port.proxyPort : SynBadTerms.Port.defaultPort;
            sbolEntity.createAnnotation(
                    SynBadTerms.Types.port,
                    qname, 
                    port.getIdentity(),
                    portdetails);

        }

    }
    
    private static void createPortAnnotations(EntityInstance synbadInstance, Identified sbolEntity, SBOLDocument document) throws SBOLValidationException {

        for(PortRecord port : synbadInstance.getPorts()) {
            List<Annotation> portdetails = new ArrayList<>();            
            String name = port.getName() == null ? port.getDisplayID() : port.getName();
            String description = port.getDescription() == null ? "" : port.getDescription();
            URI wasDerivedFrom = port.getWasDerivedFrom();
            portdetails.add(new Annotation(SynBadTerms.Types.port, name));
            
            if(!description.equals("")) portdetails.add(new Annotation(SynBadTerms.Annotatable.description, description));
            if(wasDerivedFrom!=null) portdetails.add(new Annotation(SynBadTerms.Annotatable.derivedFrom, wasDerivedFrom));

            portdetails.add(new Annotation(
                    UriHelper.synbad.withLocalPart("portDefinition"),
                    port.getValue().getIdentity()));

            sbolEntity.createAnnotation(SynBadTerms.Types.port,
                    UriHelper.synbad.withLocalPart("PortInstance"), 
                    port.getIdentity(),
                    portdetails);

        }

    }

    // ===================================================================
    //  Import SBOL
    // ===================================================================


    private static SynBadPObject parseComponentDefinition(ComponentDefinition definition, SBOLDocument document, IWorkspace workspace) {

        SynBadPObject e = workspace.getEntity(definition.getIdentity());
        
        if(e != null) {
            if(e.hasNestedAnnotation(SynBadTerms.Annotatable.entitytype, EntityType.Component.getUri()))
                return  e;
        }
        
        SynBadPObject entityDefinition =  workspace.createEntity(
                new Identity(
                        definition.getIdentity().getScheme() + "://" + definition.getIdentity().getAuthority()+"#",
                        definition.getDisplayId(),
                        definition.getVersion()));
                
 
        addIdentifiedData(entityDefinition, definition);

        for(URI type : definition.getTypes()) {
            entityDefinition.addAnnotation(SynBadTerms.Annotatable.type,
                    new Annotation(SynBadTerms.Annotatable.type, type));
        }

        for(URI role : definition.getRoles()) {
            entityDefinition.addAnnotation(SynBadTerms.Annotatable.role,
                    new Annotation(SynBadTerms.Annotatable.role, role));
        }

        // === Add Sequence ===

        //Sequence sequence = definition.getSequences();
        // The sequence annotations and constraints position sibling components
        // in tree
        
        Set<SequenceAnnotation> sequenceAnnotations = definition.getSequenceAnnotations();
        for(SequenceAnnotation annotation : sequenceAnnotations) { }

        // Constraints are regenerated based on order in editor

        Set<SequenceConstraint> constraints = definition.getSequenceConstraints();
        for(SequenceConstraint constraint : constraints) { }
        // Ports on components are stored as annotations. Add to EntityDefinition.

        return entityDefinition;
    }
    
    private static SynBadPObject parseModuleDefinition(SBOLDocument document, ModuleDefinition definition, IWorkspace workspace) {

        SynBadPObject e = workspace.getEntity(definition.getIdentity());
        if(e != null) {
            if(e.hasNestedAnnotation(SynBadTerms.Annotatable.entitytype, EntityType.Module.getUri()))
                return  e;
        }

        SynBadPObject entityDefinition = workspace.createEntity(
            new Identity(
                definition.getIdentity().getScheme() + "://" +
                definition.getIdentity().getAuthority() +"#",
                definition.getDisplayId(),
                definition.getVersion()));
   
        
        addIdentifiedData(entityDefinition, definition);
        
        
        for(URI role : definition.getRoles()) {
            entityDefinition.addAnnotation(SynBadTerms.Annotatable.role,
                    new Annotation(SynBadTerms.Annotatable.role, role));
        }

        
        Set<Model> definitionModels = definition.getModels();

        // Ports on modules will be stored as annotations. Unrecognised annotations
        // stored and preserved.

        return entityDefinition;
    }
    
    private static void addIdentifiedData(SynBadObject propertied, Identified identified) {
        String name = identified.getName() == null ? identified.getDisplayId() : identified.getName();
        String description = identified.getDescription() == null ? "" : identified.getDescription();
       
        propertied.setName(name);
        propertied.setVersion(identified.getVersion());
        propertied.setDescription(description);
        
          for(Annotation a : identified.getAnnotations()) {
            if(!isPort(a) && !isPortInstance(a) && !isProxyPort(a) && !isWire(a))
            propertied.addAnnotation(a.getQName(), a);
        }
        
        
        if(identified.getWasDerivedFrom() != null)
            propertied.setWasDerivedFrom(identified.getWasDerivedFrom());
    }

    private static IPort parsePortAnnotations(SynBadPObject module, Annotation annotation) {

        boolean isPublic = false;
        PortDirection direction = null;
        PortType type = null;
        PortState state = null;

        for(Annotation portAnnotation : annotation.getAnnotations()) {
            if(portAnnotation.getQName().equals(SynBadTerms.Port.portDirection))
                direction = PortDirection.fromString(portAnnotation.getStringValue());
            else if(portAnnotation.getQName().equals(SynBadTerms.Port.portType))
                type = dataDefinitionManager.getDefinition(PortType.class, portAnnotation.getURIValue().toASCIIString());
            else if(portAnnotation.getQName().equals(SynBadTerms.Port.portState))
                state = dataDefinitionManager.getDefinition(PortState.class, portAnnotation.getURIValue().toASCIIString());
            else if(portAnnotation.getQName().equals(SynBadTerms.Port.portAccess)) {
                isPublic = portAnnotation.isBooleanValue() ? 
                        portAnnotation.getBooleanValue() :
                        portAnnotation.getStringValue().equals("true");}
        }

        IPort port = module.createPort(isPublic, direction, type, state, null);
        addPortDetails(port, annotation);
        
        return port;
    }
    
    private static IPort parseProxyPortAnnotations(SynBadPObject module, Annotation annotation) {

        URI proxiedPortIdentity = null;
        boolean isPublic = false;

        for(Annotation portAnnotation : annotation.getAnnotations()) {
            if(portAnnotation.getQName().equals(SynBadTerms.Port.proxies)) {
                proxiedPortIdentity = portAnnotation.getURIValue();}
            else if(portAnnotation.getQName().equals(SynBadTerms.Port.portAccess)) {
                isPublic = portAnnotation.isBooleanValue() ? 
                    portAnnotation.getBooleanValue() :
                    portAnnotation.getStringValue().equals("true");}
        }

        IPort port = module.createPort(proxiedPortIdentity, isPublic);
        addPortDetails(port, annotation);
        
        return port;
    }
    
    private static void addPortDetails(IPort port, Annotation annotation) {
        for(Annotation portAnnotation : annotation.getAnnotations()) {
           if(portAnnotation.getQName().equals(SynBadTerms.Annotatable.name)) {
                port.setName(portAnnotation.getStringValue());}
            else if(portAnnotation.getQName().equals(SynBadTerms.Annotatable.description)) {
                port.setDescription(portAnnotation.getStringValue());}
            else if(portAnnotation.getQName().equals(SynBadTerms.Annotatable.derivedFrom)) {
                port.setWasDerivedFrom(portAnnotation.getURIValue());}           
        }
    }
     
    private static void parseProxyPorts(URI uri, Map<URI, List<Annotation>> ports, Map<URI, SynBadPObject> portToOwner) {
        
        for(Annotation annotation : ports.get(uri)) {
           
            SynBadPObject entity = portToOwner.get(annotation.getNestedIdentity());

            // add port defined by annotation
            
            parseProxyPortAnnotations(entity, annotation);
            
            // recursively add ports that proxy that port

            if(ports.containsKey(annotation.getNestedIdentity()))
                parseProxyPorts(annotation.getNestedIdentity(), ports, portToOwner);
        }
    }
    
    private static void parseWire(SynBadPObject module, Annotation wire) {

        URI from = null;
        URI to = null;
        
        for(Annotation wireAnnotation : wire.getAnnotations()) {
            if(wireAnnotation.getQName().equals(SynBadTerms.Wire.from))
                from = wireAnnotation.getURIValue();
            else if (wireAnnotation.getQName().equals(SynBadTerms.Wire.to))
                to = wireAnnotation.getURIValue();
        }
        
        if(from == null || to == null)
            throw new NullPointerException("Could not find portInstance IDs in wire annotation");
            
        PortRecord fromInstance = null;
        PortRecord toInstance = null;

        for(EntityInstance child : module.getChildren()) {
            if(child.containsPort(from))
                fromInstance = child.getPort(from);
            if(child.containsPort(to))
                toInstance = child.getPort(to);
        }

        
        if(fromInstance == null || toInstance == null) {
            throw new NullPointerException("Could not find portInstances in wire annotation");
        }
            
        // Use of automatically generated Port IDs is causing trouble where Entities have >1 in or out port
        
           
        if(module.wireChildren(fromInstance, toInstance) == null) {
            System.out.println("Couldn't add " +  fromInstance + " -> " + toInstance);
        }
    }
  
 
    
    // Helper methods
    
    private static boolean isPort(Annotation annotation) {
        return annotation.isNestedAnnotations() && annotation.getQName().equals(SynBadTerms.Types.port);
    }
    
    private static boolean isWire(Annotation annotation) {
        return annotation.isNestedAnnotations() && annotation.getQName().equals(SynBadTerms.Types.wire);
    }
    
    private static boolean isProxyPort(Annotation annotation) {
        return annotation.isNestedAnnotations() && annotation.getNestedQName().equals(SynBadTerms.Port.proxyPort);
    }
    
    private static boolean isPortInstance(Annotation annotation) {
        return annotation.isNestedAnnotations()
                && annotation.getNestedQName().equals(UriHelper.synbad.withLocalPart("PortInstance"));
    }
    
    private static URI getProxiedPort(Annotation annotation) {
        for(Annotation portAnnotation : annotation.getAnnotations()) {
            if(portAnnotation.getQName().equals(SynBadTerms.Port.proxies)) {
                return portAnnotation.getURIValue();
            }
        }
        
        return null;
    }
    
    private static boolean isPublicPort(Annotation annotation) {
        for(Annotation portAnnotation : annotation.getAnnotations()) {
           if(portAnnotation.getQName().equals(SynBadTerms.Port.portAccess)) {
                return true;
           }
        }
        
        return false;
    }
    
    private static boolean isInteraction(ModuleDefinition definition) {
        for(Annotation annotation : definition.getAnnotations()) {
            if( annotation.getQName().equals(SynBadTerms.Annotatable.type) &&
                annotation.getURIValue().equals(EntityType.Interaction.getUri()))
                    return true;
        }
        
        return false;
    }
    */

}