/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.datamodel.workspace.sbol;

import java.net.URI;
import java.util.Arrays;
import uk.ac.ncl.icos.datamodel.actions.CreateObjectAction;
import uk.ac.ncl.icos.datamodel.workspace.api.SBDispatcher;
import uk.ac.ncl.icos.datamodel.workspace.api.rdf.Statement;
import uk.ac.ncl.icos.datamodel.workspace.api.rdf.Statements;
import uk.ac.ncl.icos.datamodel.workspace.objects.Identity;
import uk.ac.ncl.icos.datamodel.workspace.objects.SBValueProvider;
import uk.ac.ncl.icos.datamodel.workspace.objects.SynBadObject;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms2;
import uk.ac.ncl.icos.synbad.event.SBEventType;
import uk.ac.ncl.icos.synbad.events.impl.DefaultEvent;
import uk.ac.ncl.icos.synbad.graph.api.SBValue;

/**
 *
 * @author owengilfellon
 */
public class Model extends SynBadObject {

    public Model(Identity identity, URI workspaceIdentity, SBValueProvider provider) {
        super(identity, workspaceIdentity, provider);
    }
    
    public URI getSource() {
        return getValue(SynBadTerms2.SbolModel.source).asURI();
    }
    
    public URI getLanguage() {
        return getValue(SynBadTerms2.SbolModel.language).asURI();
    }
    
    public URI getFramework() {
        return getValue(SynBadTerms2.SbolModel.framework).asURI();
    }
    
    public static class ModelCreatedEvent extends DefaultEvent<URI, URI> {

        public ModelCreatedEvent(SBEventType type, URI changeObject) {
            super(type, null, changeObject);
        }
    }
    
    public static class CreateModelAction extends CreateObjectAction<Model> {

        public CreateModelAction( Identity identity, URI source, URI language, URI framework) {
            super(  identity,
                    "http://sbols.org/v2#Model",
                    new Statements( Arrays.asList(
                        new Statement(
                            identity.getIdentity().toASCIIString(),
                            SynBadTerms2.SbolModel.source,
                            new SBValue(source)),
                        new Statement(
                            identity.getIdentity().toASCIIString(),
                            SynBadTerms2.SbolModel.language,
                            new SBValue(language)),
                        new Statement(
                            identity.getIdentity().toASCIIString(),
                            SynBadTerms2.SbolModel.framework,
                            new SBValue(framework)))),
                    null);
        }

        @Override
        public void onPerformed(SBDispatcher service) {
            super.onPerformed(service);
            service.publish(new ModelCreatedEvent(SBEventType.ADDED, getObjectIdentity()));
        }
    }
    
}
