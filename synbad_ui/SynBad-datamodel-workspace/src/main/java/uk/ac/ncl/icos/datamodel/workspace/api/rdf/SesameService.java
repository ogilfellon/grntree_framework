/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.datamodel.workspace.api.rdf;

import uk.ac.ncl.icos.synbad.graph.api.SBValue;
import java.io.File;
import java.io.InputStream;
import java.net.URI;
import java.util.List;
import java.util.Set;
import uk.ac.ncl.icos.datamodel.workspace.api.SBRdfService;

/**
 *
 * @author owengilfellon
 */
public class SesameService implements SBRdfService {

    @Override
    public boolean addRdf(InputStream stream, URI sourceId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean addRdf(File f) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean exportRdf(File f) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean removeRdf(File f) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Set<Statement> getStatements(String subject, String predicate, SBValue object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Set<SBValue> getValues(String subject, String predicate) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public SBValue getValue(String subject, String predicate) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean addStatements(List<Statement> statement) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean removeStatements(List<Statement> statement) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void shutdown() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


    /*
    private static RepositoryManager repositoryManager;
    private static final String REPOSITORY_DIRECTORY = "openRdfDb";
    private static final String REPOSITORY_ID = "synBadRepository";
    


    private static final SailRepositoryConfig MEMORY_CONFIG = new SailRepositoryConfig(new MemoryStoreConfig());

    private final SailRepository r;
    private final Set<Source> sources;

    static SesameService getService() {
        return getService(REPOSITORY_ID);
    }

    static SesameService getService(String repositoryId) {

        try {
            if (repositoryManager == null) {
                repositoryManager = new LocalRepositoryManager(new File(REPOSITORY_DIRECTORY));
                repositoryManager.initialize();
                repositoryManager.addRepositoryConfig(new RepositoryConfig(repositoryId, MEMORY_CONFIG));
            }
            SailRepository r = (SailRepository) repositoryManager.getRepository(repositoryId);

            if (!r.isInitialized()) {
                r.initialize();
            }

            SesameService service = new SesameService(r);
            return service;
        } catch (RepositoryException | RepositoryConfigException ex) {
            Exceptions.printStackTrace(ex);
            return null;
        }
    }

    SesameService(SailRepository r) {
        this.r = r;
        this.sources = new HashSet<>();
    }

    @Override
    public Source addRdf(File xmlFile) {
        
        String id = "file:/" + xmlFile.getAbsolutePath();

        try {
            SailRepositoryConnection c = r.getConnection();
            try {
                c.begin();
                c.add(xmlFile, null, RDFFormat.RDFXML, c.getValueFactory().createURI(id));
                c.commit();
            } catch (RepositoryException | IOException | RDFParseException ex) {
                Exceptions.printStackTrace(ex);
                c.rollback();
            } finally {
                c.close();
            }

            Source s = new Source(id);
            sources.add(s);
            return s;
        } catch (RepositoryException ex) {
            Exceptions.printStackTrace(ex);
            return null;
        }
    }

    @Override
    public Source addRdf(InputStream stream, String sourceName) {

        try {
            SailRepositoryConnection c = r.getConnection();
            try {
                c.begin();
                c.add(stream, null, RDFFormat.RDFXML, c.getValueFactory().createURI(sourceName));
                c.commit();
            } catch (RepositoryException | IOException | RDFParseException ex) {
                Exceptions.printStackTrace(ex);
                c.rollback();
            } finally {
                c.close();
            }
            
            Source s = new Source(sourceName);
            sources.add(s);
            return s;

        } catch (RepositoryException ex) {
            Exceptions.printStackTrace(ex);
            return null;
        }
    }



    @Override
    public boolean exportRdf(Source source, File f) {

        try {

            if (!f.exists()) {
                f.createNewFile();
            }

            if (!f.canWrite()) {
                f.delete();
                return false;
            }

            try {
                SailRepositoryConnection c = r.getConnection();

                try {
                    //FileOutputStream out = new FileOutputStream(f);
                  //  RDFXMLWriter w = new RDFXMLPrettyWriter(out);
                    
                    SBOLDocument document = null;
                    try {
                        //ByteArrayOutputStream out = new ByteArrayOutputStream();
                        
                        FileOutputStream out = new FileOutputStream(f);
                        RDFXMLWriter w = new RDFXMLPrettyWriter(out);
                        
                        //RDFWriter w = Rio.createWriter(RDFFormat.TURTLE, out);
                        c.export(w, urisToResources(c, new URI[]{URI.create(source.getFilePath())}));
                        
                       // document = SBOLReader.read(new ByteArrayInputStream(out.toByteArray()));
                        //SBOLWriter.setKeepGoing(true);
                       // SBOLWriter.write(document, f);
                    } catch (FactoryConfigurationError  ex) {
                        Exceptions.printStackTrace(ex);
                    }
                   
                } finally {
                    c.close();
                }
                
                
                


                return true;
            } catch (RepositoryException ex) {
                Exceptions.printStackTrace(ex);
                return false;
            }

        } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
            return false;
        }

    }

    @Override
    public boolean removeRdf(File f) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Set<Source> getSources() {

        return sources;
    }

    @Override
    public Set<Statement> getStatements(String subject, String predicate, uk.ac.ncl.icos.datamodel.workspace.api.rdf.Value object, URI... sources) {

        Set<Statement> statements = new HashSet<>();

        try {

            SailRepositoryConnection c = r.getConnection();
            try {

                StringBuilder select = new StringBuilder();
                StringBuilder where = new StringBuilder();

                if (subject == null || subject.isEmpty()) {
                    select.append(" ?subject");
                    where.append(" ?subject");
                } else {
                    where.append(" <").append(subject).append(">");
                }

                if (predicate == null || predicate.isEmpty()) {
                    select.append(" ?predicate");
                    where.append(" ?predicate");
                } else {
                    where.append(" <").append(predicate).append(">");
                }

                if (object == null) {
                    select.append(" ?object");
                    where.append(" ?object");
                } else {
                    where.append(" ").append(getSparQLValue(object));
                }

                String queryString = "SELECT " + select.toString() + " "
                        + getNamedGraphs(urisToResources(c, sources))
                        + " WHERE { " + where.toString() + " }";

               // System.out.println(queryString);
                TupleQuery tupleQuery
                        = c.prepareTupleQuery(QueryLanguage.SPARQL, queryString);
                TupleQueryResult result = tupleQuery.evaluate();
                while (result.hasNext()) {
                    BindingSet bindingSet = result.next();

                    String valueOfSubject = bindingSet.hasBinding("subject")
                            ? bindingSet.getValue("subject").stringValue()
                            : subject;

                    String valueOfPredicate = bindingSet.hasBinding("predicate")
                            ? bindingSet.getValue("predicate").stringValue()
                            : predicate;

                    uk.ac.ncl.icos.datamodel.workspace.api.rdf.Value valueOfObject = bindingSet.hasBinding("object")
                            ? parseValue(bindingSet.getValue("object").stringValue())
                            : object;

                    //System.out.println(valueOfSubject + " : " + valueOfPredicate + " : " + valueOfObject);
                    statements.add(new Statement(
                            valueOfSubject,
                            valueOfPredicate,
                            valueOfObject));
                }
            } catch (MalformedQueryException | QueryEvaluationException ex) {
                Exceptions.printStackTrace(ex);
            } finally {
                c.close();
            }

        } catch (RepositoryException ex) {
            Exceptions.printStackTrace(ex);
        }

        return statements;
    }

    @Override
    public Set<uk.ac.ncl.icos.datamodel.workspace.api.rdf.Value> getValues(String subject, String predicate, URI... sources) {;

        return getStatements(subject, predicate, null, sources).stream()
                .map(statement -> statement.getObject())
                .collect(Collectors.toSet());

    }

    @Override
    public uk.ac.ncl.icos.datamodel.workspace.api.rdf.Value getValue(String subject, String predicate, URI... sources) {

        return getStatements(subject, predicate, null, sources).stream()
                .map(statement -> statement.getObject()).findFirst().orElse(null);
    }

    @Override
    public boolean addStatements(List<Statement> statements, URI... source) {
        try {
            SailRepositoryConnection c = r.getConnection();
            try {
                c.begin();

                for (Statement statement : statements) {
                    ValueFactory f = c.getValueFactory();
                    Value v = getValue(statement.getObject(), f);
                    if (v == null) {
                        return false;
                    }
                    org.openrdf.model.Statement s
                            = f.createStatement(
                                    f.createURI(statement.getSubject()),
                                    f.createURI(statement.getPredicate()),
                                    v);
                    Resource[] contexts = Arrays.asList(source).stream()
                            .map(uri -> c.getValueFactory()
                                    .createURI(uri.toASCIIString()))
                            .collect(Collectors.toList()).toArray(new Resource[]{});
                    c.add(s, contexts);
                }

                c.commit();
                return true;
            } catch (RepositoryException ex) {
                c.rollback();
            } finally {
                c.close();
            }
        } catch (RepositoryException ex) {
            Exceptions.printStackTrace(ex);
        }
        return false;
    }

    @Override
    public boolean removeStatements(List<Statement> statements, URI... sources) {
        try {
            SailRepositoryConnection c = r.getConnection();
            try {
                c.begin();

                for (Statement statement : statements) {
                    ValueFactory f = c.getValueFactory();
                    Value v = getValue(statement.getObject(), f);
                    Resource r = f.createURI(statement.getSubject());
                    org.openrdf.model.URI p = f.createURI(statement.getPredicate());
                    if (v == null) {
                        return false;
                    }

                    Resource[] contexts = Arrays.asList(sources).stream()
                            .map(uri -> c.getValueFactory()
                                    .createURI(uri.toASCIIString()))
                            .collect(Collectors.toList()).toArray(new Resource[]{});

                    RepositoryResult<org.openrdf.model.Statement> i
                            = c.getStatements(r, p, v, true, contexts);

                    if (!i.hasNext()) {
                        return false;
                    }

                    c.remove(i, contexts);
                }

                c.commit();
                return true;
            } catch (RepositoryException ex) {
                c.rollback();
            } finally {
                c.close();
            }
        } catch (RepositoryException ex) {
            Exceptions.printStackTrace(ex);
        }
        return false;
    }

    private Resource[] urisToResources(SailRepositoryConnection c, URI[] uris) {
        return Arrays.asList(uris).stream()
                .map((URI uri) -> {
                    return c.getValueFactory().createURI(uri.toASCIIString());
                })
                .collect(Collectors.toSet())
                .toArray(new Resource[]{});
    }

    private String getSparQLValue(uk.ac.ncl.icos.datamodel.workspace.api.rdf.Value value) {

        if (value.isURI()) {
            return "<" + value.asURI().toASCIIString() + ">";
        } else if (value.isString()) {
            return "\"" + value.asString() + "\"";
        } else if (value.isBoolean() || value.isDouble() || value.isInt()) {
            return "\"" + value.getValue().toString() + "\"";
        } else {
            return null;
        }
    }

    private Value getValue(uk.ac.ncl.icos.datamodel.workspace.api.rdf.Value value, ValueFactory f) {
        if (value.isBoolean()) {
            return f.createLiteral(value.asBoolean());
        } else if (value.isDouble()) {
            return f.createLiteral(value.asDouble());
        } else if (value.isInt()) {
            return f.createLiteral(value.asInt());
        } else if (value.isString()) {
            return f.createLiteral(value.asString());
        } else if (value.isURI()) {
            return f.createURI(value.asURI().toASCIIString());
        }

        return null;
    }

    private uk.ac.ncl.icos.datamodel.workspace.api.rdf.Value parseValue(String value) {

        if (value.equalsIgnoreCase("true") || value.equalsIgnoreCase("false")) {
            return new uk.ac.ncl.icos.datamodel.workspace.api.rdf.Value(Boolean.parseBoolean(value));
        }

        try {
            int i = Integer.parseInt(value);
            return new uk.ac.ncl.icos.datamodel.workspace.api.rdf.Value(i);
        } catch (NumberFormatException e) {

        }

        try {
            double d = Double.parseDouble(value);
            return new uk.ac.ncl.icos.datamodel.workspace.api.rdf.Value(d);
        } catch (NumberFormatException e) {

        }

        try {
            URI uri = new URI(value);
            if (uri.isAbsolute()) {
                return new uk.ac.ncl.icos.datamodel.workspace.api.rdf.Value(uri);
            }
        } catch (URISyntaxException e) {

        }

        return new uk.ac.ncl.icos.datamodel.workspace.api.rdf.Value(value);

    }

    private static String getNamedGraphs(Resource... contexts) {
        String fromNamed = "";
        for (Resource r : contexts) {
            fromNamed = fromNamed
                    .concat(" FROM ")
                    .concat("<").concat(r.stringValue()).concat("> ");
        }

        for (Resource r : contexts) {
            fromNamed = fromNamed
                    .concat(" FROM NAMED ")
                    .concat("<").concat(r.stringValue().concat("> "));
        }
        return fromNamed;
    }

    @Override
    public void shutdown() {
        try {
            r.shutDown();
        } catch (RepositoryException ex) {
            Exceptions.printStackTrace(ex);
        }
    }*/

    @Override
    public URI getWorkspaceId() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
