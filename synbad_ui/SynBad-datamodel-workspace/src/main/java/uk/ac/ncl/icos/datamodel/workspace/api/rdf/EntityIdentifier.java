/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.datamodel.workspace.api.rdf;

import java.net.URI;
import java.util.function.BiPredicate;
import uk.ac.ncl.icos.datamodel.workspace.api.SBRdfService;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms2;

/**
 *
 * @author owengilfellon
 */
public interface EntityIdentifier extends BiPredicate<String, SBRdfService> {

   public static class IdentifyByType implements EntityIdentifier {

        private final URI type;
        private final Class clazz;
        
        public IdentifyByType(URI type, Class clazz) {
            this.type = type;
            this.clazz = clazz;
        }
        
        @Override
        public boolean test(String t, SBRdfService u) {

            return u.getStatements(t, SynBadTerms2.Rdf.hasType, null).stream()
                .filter(s -> s.getObject().isURI())
                .anyMatch(s -> s.getObject().asURI().toASCIIString().equals(type.toASCIIString()));
        }
        
    } 
}

