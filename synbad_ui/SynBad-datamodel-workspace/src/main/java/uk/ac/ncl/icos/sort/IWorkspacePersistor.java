/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.sort;

import uk.ac.ncl.icos.datamodel.workspace.api.rdf.SesameService;

/**
 *
 * @author owengilfellon
 */
public interface IWorkspacePersistor<T> {
    
    public SesameService read(T source);
    public T write(SesameService source);
    
}
