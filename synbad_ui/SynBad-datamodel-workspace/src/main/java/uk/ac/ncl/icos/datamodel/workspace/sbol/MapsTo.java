/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.datamodel.workspace.sbol;

import java.net.URI;
import uk.ac.ncl.icos.datamodel.workspace.objects.SynBadObject;
import uk.ac.ncl.icos.datamodel.workspace.objects.Identity;
import uk.ac.ncl.icos.datamodel.workspace.objects.SBValueProvider;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms2;

/**
 *
 * @author owengilfellon
 */
public class MapsTo extends SynBadObject {
    
    public static enum MapsToRefinement{ useLocal, useRemote, verifyIdentical}

    public MapsTo(Identity identity, URI workspaceIdentity, SBValueProvider values) {
        super(identity, workspaceIdentity, values);
    }
    
    public URI getRefinement() {
        return getValue(SynBadTerms2.MapsTo.hasRefinement).asURI();
    }
    
    public URI getRemoteInstance() {
        return getValue(SynBadTerms2.MapsTo.hasRemoteInstance).asURI();
    }
    
    public URI getLocalInstance() {
        return getValue(SynBadTerms2.MapsTo.hasLocalInstance).asURI();
    }
}
