/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.datamodel.workspace.impl;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import org.apache.jena.rdf.model.Model;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openide.util.Exceptions;
import org.sbolstandard.core2.AccessType;
import org.sbolstandard.core2.Component;
import org.sbolstandard.core2.ComponentDefinition;
import org.sbolstandard.core2.DirectionType;
import org.sbolstandard.core2.FunctionalComponent;
import org.sbolstandard.core2.ModuleDefinition;
import org.sbolstandard.core2.RefinementType;
import org.sbolstandard.core2.RestrictionType;
import org.sbolstandard.core2.SBOLConversionException;
import org.sbolstandard.core2.SBOLDocument;
import org.sbolstandard.core2.SBOLValidate;
import org.sbolstandard.core2.SBOLValidationException;
import org.sbolstandard.core2.SBOLWriter;
import uk.ac.ncl.icos.datamodel.workspace.api.rdf.JenaParser;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentRole;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentType;

/**
 *
 * @author owengilfellon
 */
public class SBOLTests {
    
    public SBOLTests() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of createEntity method, of class Workspace.
     */
    @Test
    public void testSBOL() throws SBOLValidationException, SBOLConversionException {
        
        SBOLDocument document = new SBOLDocument();
        ComponentDefinition tu = document.createComponentDefinition(
                "http://www.synbad.org", "tu", "1.0",
                new HashSet<>(Collections.singletonList(ComponentType.DNA.getUri())));
        tu.addRole(ComponentRole.Generic.getUri());
        
        ComponentDefinition promoter = document.createComponentDefinition(
                "http://www.synbad.org", "promoter", "1.0",
                new HashSet<>(Collections.singletonList(ComponentType.DNA.getUri())));
        promoter.addRole(ComponentRole.Promoter.getUri());
        
        ComponentDefinition rbs = document.createComponentDefinition(
                "http://www.synbad.org", "rbs", "1.0",
                new HashSet<>(Collections.singletonList(ComponentType.DNA.getUri())));
        rbs.addRole(ComponentRole.RBS.getUri());
        
        ComponentDefinition cds = document.createComponentDefinition(
                "http://www.synbad.org", "cds", "1.0",
                new HashSet<>(Collections.singletonList(ComponentType.DNA.getUri())));
        cds.addRole(ComponentRole.CDS.getUri());
        
        ComponentDefinition terminator = document.createComponentDefinition(
                "http://www.synbad.org", "terminator", "1.0",
                new HashSet<>(Collections.singletonList(ComponentType.DNA.getUri())));
        terminator.addRole(ComponentRole.Terminator.getUri());
        
        Component tuPromoter = tu.createComponent("promoterInstance", AccessType.PUBLIC, promoter.getPersistentIdentity());
        Component tuRBS = tu.createComponent("rbsInstance", AccessType.PUBLIC, rbs.getPersistentIdentity());
        Component tuCDS = tu.createComponent("cdsInstance", AccessType.PUBLIC, cds.getPersistentIdentity());
        Component tuTer = tu.createComponent("terminatorInstance", AccessType.PUBLIC, terminator.getPersistentIdentity());
       
        
        tu.createSequenceConstraint("cons1", RestrictionType.PRECEDES, tuPromoter.getPersistentIdentity(), tuRBS.getPersistentIdentity());
        tu.createSequenceConstraint("cons2", RestrictionType.PRECEDES, tuRBS.getPersistentIdentity(), tuCDS.getPersistentIdentity());
        tu.createSequenceConstraint("cons3", RestrictionType.PRECEDES, tuCDS.getPersistentIdentity(), tuTer.getPersistentIdentity());
        
        ModuleDefinition module = document.createModuleDefinition("http://www.synbad.org", "module", "1.0");

        FunctionalComponent tuInstance = module.createFunctionalComponent("tuInstance", AccessType.PUBLIC, tu.getPersistentIdentity(), DirectionType.INOUT);
        FunctionalComponent promoterInstance = module.createFunctionalComponent("promoterInstance", AccessType.PUBLIC, promoter.getPersistentIdentity(), DirectionType.INOUT);
        FunctionalComponent rbsInstance = module.createFunctionalComponent("rbsInstance", AccessType.PUBLIC, rbs.getPersistentIdentity(), DirectionType.INOUT);
        FunctionalComponent cdsInstance = module.createFunctionalComponent("cdsInstance", AccessType.PUBLIC, cds.getPersistentIdentity(), DirectionType.INOUT);
        FunctionalComponent terInstance = module.createFunctionalComponent("terInstance", AccessType.PUBLIC, terminator.getPersistentIdentity(), DirectionType.INOUT);

        tuInstance.createMapsTo("maps1", RefinementType.VERIFYIDENTICAL, promoterInstance.getPersistentIdentity(), tuPromoter.getPersistentIdentity());
        tuInstance.createMapsTo("maps2", RefinementType.VERIFYIDENTICAL, rbsInstance.getPersistentIdentity(), tuRBS.getPersistentIdentity());
        tuInstance.createMapsTo("maps3", RefinementType.VERIFYIDENTICAL, cdsInstance.getPersistentIdentity(), tuCDS.getPersistentIdentity());
        tuInstance.createMapsTo("maps4", RefinementType.VERIFYIDENTICAL, terInstance.getPersistentIdentity(), tuTer.getPersistentIdentity());
        
        SBOLValidate.validateSBOL(document, true, true, true);
        for(String s : SBOLValidate.getErrors()) {
            System.err.println(s);
        }
        
        try {
 
            Model result = JenaParser.getModel(document);
            SBOLDocument docOut = JenaParser.getDocument(result);
            SBOLWriter.setKeepGoing(true);
            SBOLWriter.write(docOut, "ThereAndBackAgain.xml");

        } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
        }
        
        //SBOLWriter.write(document, System.out);
        
        /*
        for(FunctionalComponent c : module.getFunctionalComponents()) {
            for(MapsTo mapsTo : c.getMapsTos()) {
                System.out.println(mapsTo.getLocal().getPersistentIdentity() + " " + mapsTo.getRefinement() + " " + mapsTo.getRemote().getPersistentIdentity());
            }
        }    
        */
    
    }
    
}
