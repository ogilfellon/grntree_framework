/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.datamodel.workspace.api.rdf;

import com.google.common.collect.Sets;
import uk.ac.ncl.icos.datamodel.workspace.api.SBWorkspaceManager;
import uk.ac.ncl.icos.datamodel.workspace.api.SBObjectFactory;
import uk.ac.ncl.icos.datamodel.workspace.api.SBWorkspace;
import java.io.File;
import java.net.URI;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.netbeans.junit.MockServices;
import org.openide.util.Lookup;
import uk.ac.ncl.icos.datamodel.graph.impl.DefaultCrawler;
import uk.ac.ncl.icos.datamodel.workspace.api.SBEntity;
import uk.ac.ncl.icos.datamodel.workspace.api.SBEventFilter;
import uk.ac.ncl.icos.datamodel.workspace.api.SBEvents;
import uk.ac.ncl.icos.datamodel.workspace.objects.Identity;
import uk.ac.ncl.icos.datamodel.workspace.objects.SynBadEdge;
import uk.ac.ncl.icos.datamodel.workspace.objects.SynBadObject;
import uk.ac.ncl.icos.datamodel.workspace.sbol.Component;
import uk.ac.ncl.icos.datamodel.workspace.sbol.ComponentDefinition;
import uk.ac.ncl.icos.datamodel.workspace.sbol.FunctionalComponent;
import uk.ac.ncl.icos.datamodel.workspace.sbol.Location;
import uk.ac.ncl.icos.datamodel.workspace.sbol.ModuleDefinition;
import uk.ac.ncl.icos.datamodel.workspace.sbol.SequenceAnnotation;
import uk.ac.ncl.icos.datamodel.workspace.sbol.SequenceConstraint;
import uk.ac.ncl.icos.synbad.datadefinition.UriHelper;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentType;
import uk.ac.ncl.icos.synbad.event.SBEvent;
import uk.ac.ncl.icos.synbad.event.SBSubscriber;
import uk.ac.ncl.icos.synbad.graph.api.SBCrawlerListener;
import uk.ac.ncl.icos.synbad.graph.api.SBCursor;
import uk.ac.ncl.icos.synbad.graph.api.SBValue;

/**
 *
 * @author owengilfellon
 */
public class DefaultWorkspaceTest implements SBSubscriber {
    
    private final URI workspaceURI = UriHelper.synbad.namespacedUri("testWorkspace");
    private final SBWorkspaceManager m;
    private SBWorkspace ws;
    
    public DefaultWorkspaceTest() {
        MockServices.setServices(DefaultWorkspaceManager.class);
        m = Lookup.getDefault().lookup(SBWorkspaceManager.class);
        setupWorkspace();
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
        m.closeWorkspace(workspaceURI);
    }
    
    
    private void setupWorkspace() {

        ws = m.getWorkspace(workspaceURI);
        
        Set<String> sources = new HashSet<>(Arrays.asList(
            "ExampleSystem.xml",
            "subtilinReceiverPliaG1.xml"  
        ));

        
        for(String source : sources) {
            ws.addRdf(new File(source));
        }
        
        ws.getDispatcher().subscribe(SBEvent.class, new SBEventFilter.DefaultFilter(), this);
    }
    
    // ============================================================
    //                         Identities
    // ============================================================
    
    @Test
    public void testGetIdentities() {
        System.out.print("Getting identities...");
        Set<Identity> identities = ws.getIdentities();
        System.out.println(" done");
        assert(!identities.isEmpty());
        // All Identities are retrievable by getIdentity
    }
    @Test
    public void testGetIdentity() {
        
        Set<Identity> identities = ws.getIdentities();
        assert(  identities.stream()
               .map(i -> ws.getIdentity(i.getIdentity()))
               .count() == identities.size());
    }
    
    @Test
    public void testContainsIdentity() {
        
        Set<Identity> identities = ws.getIdentities();
        assert(  identities.stream()
               .map(i -> ws.getIdentity(i.getIdentity()))
               .filter(i -> ws.containsIdentity(i.getIdentity()))
               .count() == identities.size());
    }
    
    // ============================================================
    //                         Objects
    // ============================================================
    
        /**
     * Test of getSources method, of class IRdfWorkspace.
     */
    @Test
    public void testGetObject() {

        for(SBObjectFactory<? extends SBEntity> f : ws.getFactories()) {
           // System.out.print("Getting " + f.getType().toASCIIString() + "...");
            ws.getObjects(f.getEntityClass()).stream().map(m -> (SBEntity) m).forEach(
                    o -> {
                        assert (ws.getObject(o.getIdentity(), f.getEntityClass()) != null);
                    }
            );
          //  System.out.println(" done");
        }
    }
    
      /**
     * Test of getSources method, of class IRdfWorkspace.
     */
    @Test
    public void testContainsObject() {

        for(SBObjectFactory<? extends SBEntity> f : ws.getFactories()) {

            ws.getObjects(f.getEntityClass()).stream().map(m -> (SBEntity) m).forEach(
                    o -> ws.containsObject(o.getIdentity(), o.getClass())
            );
        }

    }

    
    private void debugSynBadObject(SBEntity o) {
        System.out.println("===============================================");
        System.out.println(o.getName());
        System.out.println("===============================================");
        System.out.println(o.getIdentity().toASCIIString());
        System.out.println(o.getPersistentId().toASCIIString());
        System.out.println(o.getDisplayId());
        System.out.println(o.getDescription());

        for(String p : o.getPredicates()) {
            for(SBValue v : o.getValues(p)) {
                System.out.println(" - " + p + " : " + v);
            }
            
        }
        
        ws.incomingEdges(o).stream().forEach(e -> System.out.println(" I: " + e.getEdge().toASCIIString() + " from " + ws.getEdgeSource(e)));
        ws.outgoingEdges(o).stream().forEach(e -> System.out.println(" O: " + e.getEdge().toASCIIString() + " to " + ws.getEdgeTarget(e)));
    }
    
    // ============================================================
    //                          Edges
    // ============================================================
    

    @Test
    public void testGetEdges() {
        
        for(SBObjectFactory<? extends SBEntity> f : ws.getFactories()) {

            ws.getObjects(f.getEntityClass()).stream()
                .map(object -> ws.outgoingEdges(object))
                .flatMap(edges -> edges.stream())
                .forEach(
                    edge -> { 
                        //System.out.println(edge);
                    }
            );
        }
    }
    
    @Test
    public void testGetEdgesWithPredicate() {
        
     for(SBObjectFactory<? extends SBEntity> f : ws.getFactories()) {
            //System.out.println(f.getType());
            ws.getObjects(f.getEntityClass()).stream()
                .map(m -> (SBEntity) m)
                .forEach(
                    o -> {
                        Collection<SynBadEdge> edges = ws.outgoingEdges(o);
                        //assert(!edges.isEmpty());
                      //  assert(o.getPredicates().stream().flatMap(p -> ws.outgoingEdges(o, p).stream()).count() == edges.size());
                    }
                );
        }
    }
    
    // ============================================================
    //                          Traversal
    // ============================================================
    
    @Test
    public void testCrawler() {
        ModuleDefinition system = ws.getObject(URI.create("http://www.synbad.org/System/1.0"), ModuleDefinition.class);
        SBCursor<SBEntity, SynBadEdge> cursor = ws.getCursor(system);
        DefaultCrawler<SBEntity, SynBadEdge> crawler = new DefaultCrawler<>();
        crawler.addListener(new SBCrawlerListener.SBCrawlerListenerAdapter<SBEntity, SynBadEdge>() {

            @Override
            public void onInstance(SBEntity instance, SBCursor<SBEntity, SynBadEdge> cursor) {
               // System.out.println("I: " + instance);
            }

            @Override
            public void onEdge(SynBadEdge edge) {
                //System.out.println("E: " + edge);
            }
            
        }) ;

        crawler.run(cursor);


    }
    
    // ============================================================
    //                          Events
    // ============================================================

    @Override
    public void onEvent(SBEvent e) {
        if(e instanceof ModuleDefinition.ModuleDefinitionCreatedEvent) {
            ModuleDefinition.ModuleDefinitionCreatedEvent ev = (ModuleDefinition.ModuleDefinitionCreatedEvent) e;
            ModuleDefinition d = ws.getObject(ev.getObject(), ModuleDefinition.class);
           // System.out.println("MD: " + d.getDisplayId());
        } else if(e instanceof ComponentDefinition.ComponentDefinitionCreatedEvent) {
            ComponentDefinition.ComponentDefinitionCreatedEvent ev = (ComponentDefinition.ComponentDefinitionCreatedEvent) e;
            ComponentDefinition d = ws.getObject(ev.getObject(), ComponentDefinition.class);
            //System.out.println("CD: " + d.getDisplayId());
        } else if (e instanceof SynBadObject.NameEvent) {
            //System.out.println("Name: " + ((SBEvents.NameEvent)e).getObject());
        }
    }

}
