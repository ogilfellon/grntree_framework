/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.datamodel.workspace.api.rdf;

import com.google.common.collect.Sets;
import uk.ac.ncl.icos.datamodel.workspace.api.SBWorkspaceManager;
import uk.ac.ncl.icos.datamodel.workspace.api.SBWorkspace;
import java.net.URI;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.netbeans.junit.MockServices;
import org.openide.util.Lookup;
import uk.ac.ncl.icos.datamodel.graph.impl.DefaultCrawler;
import uk.ac.ncl.icos.datamodel.workspace.api.SBEventFilter;
import uk.ac.ncl.icos.datamodel.workspace.objects.Identity;
import uk.ac.ncl.icos.datamodel.workspace.objects.SynBadEdge;
import uk.ac.ncl.icos.datamodel.workspace.objects.SynBadObject;
import uk.ac.ncl.icos.datamodel.workspace.sbol.Component;
import uk.ac.ncl.icos.datamodel.workspace.sbol.ComponentDefinition;
import uk.ac.ncl.icos.datamodel.workspace.sbol.FunctionalComponent;
import uk.ac.ncl.icos.datamodel.workspace.sbol.Location;
import uk.ac.ncl.icos.datamodel.workspace.sbol.ModuleDefinition;
import uk.ac.ncl.icos.datamodel.workspace.sbol.Sequence;
import uk.ac.ncl.icos.datamodel.workspace.sbol.SequenceAnnotation;
import uk.ac.ncl.icos.datamodel.workspace.sbol.SequenceConstraint;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms2;
import uk.ac.ncl.icos.synbad.datadefinition.UriHelper;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentType;
import uk.ac.ncl.icos.synbad.event.SBEvent;
import uk.ac.ncl.icos.synbad.event.SBSubscriber;
import uk.ac.ncl.icos.synbad.graph.api.SBCrawlerListener;
import uk.ac.ncl.icos.synbad.graph.api.SBCursor;
import uk.ac.ncl.icos.synbad.graph.api.SBValue;

/**
 *
 * @author owengilfellon
 */
public class CommandsTest implements SBSubscriber {

    private final URI workspaceURI = UriHelper.synbad.namespacedUri("testCommands");
    private final SBWorkspaceManager m;
    private SBWorkspace ws;

    public CommandsTest() {
        MockServices.setServices(DefaultWorkspaceManager.class);
        m = Lookup.getDefault().lookup(SBWorkspaceManager.class);
        setupWorkspace();
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
        m.closeWorkspace(workspaceURI);
    }

    private void setupWorkspace() {

        ws = m.getWorkspace(workspaceURI);
        ws.getDispatcher().subscribe(SBEvent.class, new SBEventFilter.DefaultFilter(), this);

    }

    @Test
    public void testNameAndDescriptionActions() {
        Identity cdId1 = new Identity("http://www.synbad.org", "cd1", "1.0");
        ws.perform(new ComponentDefinition.CreateComponentDefinitionAction(cdId1, Sets.newHashSet(ComponentType.DNA)));
       
        ComponentDefinition cd1 = ws.getObject(cdId1.getIdentity(), ComponentDefinition.class);
        assert (cd1.getVersion().equals("1.0"));
        assert (cd1.getIdentity().equals(URI.create("http://www.synbad.org/cd1/1.0")));

        // Test methods

        cd1.setName("ChangeByMethod");
        assert (cd1.getName().equals("ChangeByMethod"));
        cd1.setDescription("ChangeByMethod");
        assert (cd1.getDescription().equals("ChangeByMethod"));

        // Test helper actions
        
        ws.perform(new SynBadObject.SetNameAction(cdId1.getIdentity(), "ChangeBySetNameCommand"));
        assert (cd1.getName().equals("ChangeBySetNameCommand"));
        ws.perform(new SynBadObject.SetDescriptionAction(cdId1.getIdentity(), "ChangeBySetDescriptionCommand"));
        assert (cd1.getDescription().equals("ChangeBySetDescriptionCommand"));
        
        // Test Set Value Actions
        
        ws.perform(new SynBadObject.SetValueAction(cd1.getIdentity(), SynBadTerms2.SbolIdentified.hasName, new SBValue("ChangeBySetValueCommand")));
        assert (cd1.getName().equals("ChangeBySetValueCommand"));
        ws.perform(new SynBadObject.SetValueAction(cd1.getIdentity(), SynBadTerms2.SbolIdentified.hasDescription, new SBValue("ChangeBySetValueCommand")));
        assert (cd1.getDescription().equals("ChangeBySetValueCommand"));
        
    }

    @Test
    public void testComponentDefinitionActions() {

        // Create identity for module and component, instantiate Object creation
        // actions and issue to workspace.
        Identity cdId1 = new Identity("http://www.synbad.org", "cd1", "1.0");
        Identity cdId2 = new Identity("http://www.synbad.org", "cd2", "1.0");
        Identity cdId3 = new Identity("http://www.synbad.org", "cd3", "1.0");

        ws.perform(new ComponentDefinition.CreateComponentDefinitionAction(cdId1, Sets.newHashSet(ComponentType.DNA)));
        ws.perform(new ComponentDefinition.CreateComponentDefinitionAction(cdId2, Sets.newHashSet(ComponentType.DNA)));
        ws.perform(new ComponentDefinition.CreateComponentDefinitionAction(cdId3, Sets.newHashSet(ComponentType.DNA)));

        ComponentDefinition cd1 = ws.getObject(cdId1.getIdentity(), ComponentDefinition.class);
        ComponentDefinition cd2 = ws.getObject(cdId2.getIdentity(), ComponentDefinition.class);
        ComponentDefinition cd3 = ws.getObject(cdId3.getIdentity(), ComponentDefinition.class);

        assert (cd1 != null);
        assert (cd2 != null);
        assert (cd3 != null);

        Identity seq1Id = new Identity("http://www.synbad.org", "sequence1", "1.0");
        Identity seq2Id = new Identity("http://www.synbad.org", "sequence2", "1.0");
        Identity seq3Id = new Identity("http://www.synbad.org", "sequence3", "1.0");

        ws.perform(new Sequence.CreateSequenceAction(seq1Id, "AAAAAAAAAAAAAAAATTTTTTTTTTTTTTTT", "http://www.chem.qmul.ac.uk/iubmb/misc/naseq.html"));
        ws.perform(new Sequence.CreateSequenceAction(seq2Id, "AAAAAAAAAAAAAAAA", "http://www.chem.qmul.ac.uk/iubmb/misc/naseq.html"));
        ws.perform(new Sequence.CreateSequenceAction(seq3Id, "TTTTTTTTTTTTTTTT", "http://www.chem.qmul.ac.uk/iubmb/misc/naseq.html"));

        Sequence seq1 = ws.getObject(seq1Id.getIdentity(), Sequence.class);
        Sequence seq2 = ws.getObject(seq2Id.getIdentity(), Sequence.class);
        Sequence seq3 = ws.getObject(seq3Id.getIdentity(), Sequence.class);

        assert (seq1 != null);
        assert (seq2 != null);
        assert (seq3 != null);

        ws.perform(new ComponentDefinition.AddSequenceAction(cd1.getIdentity(), seq1.getIdentity()));
        ws.perform(new ComponentDefinition.AddSequenceAction(cd2.getIdentity(), seq2.getIdentity()));
        ws.perform(new ComponentDefinition.AddSequenceAction(cd3.getIdentity(), seq3.getIdentity()));

        assert (cd1.getSequences().contains(seq1));
        assert (cd2.getSequences().contains(seq2));
        assert (cd3.getSequences().contains(seq3));

        assert (ws.outgoingEdges(cd1, SynBadTerms2.SbolComponent.hasSequence, Sequence.class).size() == 1);
        assert (ws.outgoingEdges(cd2, SynBadTerms2.SbolComponent.hasSequence, Sequence.class).size() == 1);
        assert (ws.outgoingEdges(cd3, SynBadTerms2.SbolComponent.hasSequence, Sequence.class).size() == 1);

        // Instantiate ComponentDefinition as Components in ComponentDefinition
        Identity c1Id = new Identity("http://www.synbad.org", "component1", "1.0");
        Identity c2Id = new Identity("http://www.synbad.org", "component2", "1.0");

        ws.perform(new ComponentDefinition.CreateComponentAction(c1Id, cd1, cd2));
        ws.perform(new ComponentDefinition.CreateComponentAction(c2Id, cd1, cd3));

        Component c1 = ws.getObject(c1Id.getIdentity(), Component.class);
        Component c2 = ws.getObject(c2Id.getIdentity(), Component.class);

        assert (c1 != null);
        assert (c2 != null);

        assert (cd1.getComponents().contains(c1));
        assert (cd1.getComponents().contains(c2));
        
        assert (cd1.getNestedObjects().contains(c1));
        assert (cd1.getNestedObjects().contains(c2));

        assert (ws.outgoingEdges(cd1, SynBadTerms2.SbolComponent.hasComponent, Component.class).size() == 2);

        Identity location1Identity = new Identity("http://www.synbad.org", "componentInstance1_Location", "1.0");
        ws.perform(new Location.CreateRangeAction(location1Identity, 1, 100));

        Identity location2Identity = new Identity("http://www.synbad.org", "componentInstance2_Location", "1.0");
        ws.perform(new Location.CreateCutAction(location2Identity, 70));

        Location location1 = ws.getObject(location1Identity.getIdentity(), Location.class);
        Location location2 = ws.getObject(location2Identity.getIdentity(), Location.class);

        assert (location1 != null);
        assert (location2 != null);

        Identity annotation1Identity = new Identity("http://www.synbad.org", "componentInstance1_Annotation", "1.0");
        ws.perform(new ComponentDefinition.CreateSequenceAnnotationAction(annotation1Identity, cd1, c1, location1));
        Identity annotation2Identity = new Identity("http://www.synbad.org", "componentInstance2_Annotation", "1.0");
        ws.perform(new ComponentDefinition.CreateSequenceAnnotationAction(annotation2Identity, cd1, c2, location2));

        SequenceAnnotation ann1 = ws.getObject(annotation1Identity.getIdentity(), SequenceAnnotation.class);
        SequenceAnnotation ann2 = ws.getObject(annotation2Identity.getIdentity(), SequenceAnnotation.class);

        Identity constraintIdentity = new Identity("http://www.synbad.org", "componentInstance1_2Constraint", "1.0");
        ws.perform(new ComponentDefinition.CreateSequenceConstraintAction(constraintIdentity, cd1, c1, c2, URI.create("http://sbols.org/v2#precedes")));
        SequenceConstraint c = ws.getObject(constraintIdentity.getIdentity(), SequenceConstraint.class);
        c.setDescription("Constraint:- C precedes D in B");

        assert (c != null);

        //ws.getDispatcher().subscribe(SBEvent.class, new SBEventFilter.DefaultFilter(), this);
    }

    @Test
    public void testCreateObjects() {

        // Create identity for module and component, instantiate Object creation
        // actions and issue to workspace.
        Identity moduleIdentity = new Identity("http://www.synbad.org", "rootModule", "1.0");
        ws.perform(new ModuleDefinition.CreateModuleDefinitionAction(moduleIdentity));

        Identity compDef1Identity = new Identity("http://www.synbad.org", "componentDefinition", "1.0");
        ws.perform(new ComponentDefinition.CreateComponentDefinitionAction(compDef1Identity, Sets.newHashSet(ComponentType.DNA)));

        Identity compDef2Identity = new Identity("http://www.synbad.org", "componentDefinition2", "1.0");
        ws.perform(new ComponentDefinition.CreateComponentDefinitionAction(compDef2Identity, Sets.newHashSet(ComponentType.DNA)));

        Identity compDef3Identity = new Identity("http://www.synbad.org", "componentDefinition3", "1.0");
        ws.perform(new ComponentDefinition.CreateComponentDefinitionAction(compDef3Identity, Sets.newHashSet(ComponentType.DNA)));

        // Retrieve created objects
        ModuleDefinition mod = ws.getObject(moduleIdentity.getIdentity(), ModuleDefinition.class);
        mod.setName("A");
        mod.setDescription("The root module definition");

        ComponentDefinition comDef1 = ws.getObject(compDef1Identity.getIdentity(), ComponentDefinition.class);
        comDef1.setName("B");
        comDef1.setDescription("A component definition that contains two child components");

        ComponentDefinition comDef2 = ws.getObject(compDef2Identity.getIdentity(), ComponentDefinition.class);
        comDef2.setName("C");
        comDef2.setDescription("A component definition");

        ComponentDefinition comDef3 = ws.getObject(compDef3Identity.getIdentity(), ComponentDefinition.class);
        comDef3.setName("D");
        comDef3.setDescription("A component definition");

        assert (comDef1 != null);
        assert (comDef2 != null);
        assert (comDef3 != null);

        Identity sequence1Identity = new Identity("http://www.synbad.org", "sequence1", "1.0");
        ws.perform(new Sequence.CreateSequenceAction(sequence1Identity, "AAAAAAAAAAAAAAAA", "http://www.chem.qmul.ac.uk/iubmb/misc/naseq.html"));

        Identity sequence2Identity = new Identity("http://www.synbad.org", "sequence2", "1.0");
        ws.perform(new Sequence.CreateSequenceAction(sequence2Identity, "TTTTTTTTTTTTTTTT", "http://www.chem.qmul.ac.uk/iubmb/misc/naseq.html"));

        Sequence seq1 = ws.getObject(sequence1Identity.getIdentity(), Sequence.class);
        Sequence seq2 = ws.getObject(sequence2Identity.getIdentity(), Sequence.class);

        assert (seq1 != null);
        assert (seq2 != null);

        ws.perform(new ComponentDefinition.AddSequenceAction(comDef2.getIdentity(), seq1.getIdentity()));
        ws.perform(new ComponentDefinition.AddSequenceAction(comDef3.getIdentity(), seq2.getIdentity()));

        // Instantiate ComponentDefinition as Components in ComponentDefinition
        Identity compInstanceIdentity = new Identity("http://www.synbad.org", "B_instance", "1.0");
        ws.perform(new ModuleDefinition.CreateFuncComponentAction(compInstanceIdentity, comDef1, mod));

        Identity compInstance1Identity = new Identity("http://www.synbad.org", "C_instance", "1.0");
        ws.perform(new ComponentDefinition.CreateComponentAction(compInstance1Identity, comDef1, comDef2));

        Identity compInstance2Identity = new Identity("http://www.synbad.org", "D_instance", "1.0");
        ws.perform(new ComponentDefinition.CreateComponentAction(compInstance2Identity, comDef1, comDef3));

        FunctionalComponent fc = ws.getObject(compInstanceIdentity.getIdentity(), FunctionalComponent.class);
        fc.setDescription("Instance of B in ModuleDefinition A");

        Component comp1 = ws.getObject(compInstance1Identity.getIdentity(), Component.class);
        comp1.setDescription("Instance of C in ComponentDefinition B");

        Component comp2 = ws.getObject(compInstance2Identity.getIdentity(), Component.class);
        comp2.setDescription("Instance of D in ComponentDefinition B");

        assert (fc != null);
        assert (comp1 != null);
        assert (comp2 != null);

        Identity location1Identity = new Identity("http://www.synbad.org", "componentInstance1_Location", "1.0");
        ws.perform(new Location.CreateRangeAction(location1Identity, 1, 100));

        Identity location2Identity = new Identity("http://www.synbad.org", "componentInstance2_Location", "1.0");
        ws.perform(new Location.CreateCutAction(location2Identity, 70));

        Location location1 = ws.getObject(location1Identity.getIdentity(), Location.class);
        Location location2 = ws.getObject(location2Identity.getIdentity(), Location.class);

        assert (location1 != null);
        assert (location2 != null);

        Identity annotation1Identity = new Identity("http://www.synbad.org", "componentInstance1_Annotation", "1.0");
        ws.perform(new ComponentDefinition.CreateSequenceAnnotationAction(annotation1Identity, comDef1, comp1, location1));
        Identity annotation2Identity = new Identity("http://www.synbad.org", "componentInstance2_Annotation", "1.0");
        ws.perform(new ComponentDefinition.CreateSequenceAnnotationAction(annotation2Identity, comDef1, comp2, location2));

        SequenceAnnotation ann1 = ws.getObject(annotation1Identity.getIdentity(), SequenceAnnotation.class);
        SequenceAnnotation ann2 = ws.getObject(annotation2Identity.getIdentity(), SequenceAnnotation.class);

        assert (ann1 != null);
        assert (ann2 != null);
        
        assert(ann1.getComponent() == comp1 && ann1.getComponent().equals(comp1));
        assert(ann2.getComponent() == comp2 && ann2.getComponent().equals(comp2));
        
        assert(ann1.getLocation() == location1 && ann1.getLocation().equals(location1));
        assert(ann2.getLocation() == location2 && ann2.getLocation().equals(location2));

        Identity constraintIdentity = new Identity("http://www.synbad.org", "componentInstance1_2Constraint", "1.0");
        ws.perform(new ComponentDefinition.CreateSequenceConstraintAction(constraintIdentity, comDef1, comp1, comp2, URI.create("http://sbols.org/v2#precedes")));
        SequenceConstraint c = ws.getObject(constraintIdentity.getIdentity(), SequenceConstraint.class);

        assert (c != null);
        assert (c.getSubject() == comp1 && c.getSubject().equals(comp1));
        assert (c.getObject() == comp2 && c.getObject().equals(comp2));

        //ws.getDispatcher().subscribe(SBEvent.class, new SBEventFilter.DefaultFilter(), this);
    }

    private void debugSynBadObject(SynBadObject o) {
        System.out.println("===============================================");
        System.out.println(o.getName());
        System.out.println("===============================================");
        System.out.println(o.getIdentity().toASCIIString());
        System.out.println(o.getPersistentId().toASCIIString());
        System.out.println(o.getDisplayId());
        System.out.println(o.getDescription());

        for (String p : o.getPredicates()) {
            for (SBValue v : o.getValues(p)) {
                System.out.println(" - " + p + " : " + v);
            }

        }

        ws.incomingEdges(o).stream().forEach(e -> System.out.println(" I: " + e.getEdge().toASCIIString() + " from " + ws.getEdgeSource(e)));
        ws.outgoingEdges(o).stream().forEach(e -> System.out.println(" O: " + e.getEdge().toASCIIString() + " to " + ws.getEdgeTarget(e)));
    }

    // ============================================================
    //                          Events
    // ============================================================
    @Override
    public void onEvent(SBEvent e) {
        //System.out.println(e.getClass().getSimpleName() + " " + e.getType() + " " + e.getObject() + " in " + e.getSource());
    }

}
