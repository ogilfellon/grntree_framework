/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.datamodel.workspace.sbol.toplevel;

import uk.ac.ncl.icos.datamodel.workspace.objects.SynBadObject;
import uk.ac.ncl.icos.datamodel.workspace.sbol.ComponentDefinition;
import java.io.File;
import java.net.URI;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openide.util.Lookup;
import uk.ac.ncl.icos.datamodel.workspace.api.SBWorkspace;
import uk.ac.ncl.icos.datamodel.workspace.api.SBWorkspaceManager;
import uk.ac.ncl.icos.datamodel.workspace.sbol.ModuleDefinition;
import uk.ac.ncl.icos.synbad.datadefinition.UriHelper;
import uk.ac.ncl.icos.synbad.datadefinition.types.Role;

/**
 *
 * @author owengilfellon
 */
public class ComponentDefinitionTest {

    
    private final URI workspaceURI = UriHelper.synbad.namespacedUri("testWorkspace");
    private final SBWorkspaceManager m;
    private SBWorkspace ws;
    
    public ComponentDefinitionTest() {
        m = Lookup.getDefault().lookup(SBWorkspaceManager.class);
        setupWorkspace();
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
        m.closeWorkspace(workspaceURI);
    }
    
    
    private void setupWorkspace() {

        ws = m.getWorkspace(workspaceURI);
        
        Set<String> sources = new HashSet<>(Arrays.asList(
            "ExampleSystem.xml",
            "subtilinReceiverPliaG1.xml"  
        ));

        
        for(String source : sources) {
            ws.addRdf(new File(source));
        }
    }
    

    /**
     * Test of getFunctionalComponents method, of class ModuleDefinition.
     */
    @Test
    public void testGetComponents() {
        Set<URI> childUris = ws.getObjects(ComponentDefinition.class).stream()
                .flatMap(m -> m.getChildren().stream())
                .map(SynBadObject::getIdentity).collect(Collectors.toSet());
        
        Set<URI> componentUris = ws.getObjects(ComponentDefinition.class).stream()
                .flatMap(m -> m.getComponents().stream())
                .map(SynBadObject::getIdentity).collect(Collectors.toSet());

        assert(childUris.containsAll(componentUris));

    }
    
            /**
     * Test of getFunctionalComponents method, of class ModuleDefinition.
     */
    @Test
    public void testGetRoles() {
        assert(ws.getObjects(ComponentDefinition.class).stream()
                .flatMap(m -> m.getRoles().stream())
                .count() > 0);

    }


    
}
