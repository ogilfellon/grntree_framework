/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.datamodel.workspace.sbol.toplevel;

import uk.ac.ncl.icos.datamodel.workspace.sbol.ComponentDefinition;
import java.io.File;
import java.net.URI;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openide.util.Lookup;
import uk.ac.ncl.icos.datamodel.workspace.api.SBWorkspace;
import uk.ac.ncl.icos.datamodel.workspace.api.SBWorkspaceManager;
import uk.ac.ncl.icos.datamodel.workspace.sbol.Component;
import uk.ac.ncl.icos.synbad.datadefinition.UriHelper;

/**
 *
 * @author owengilfellon
 */
public class ComponentTest {
    
    private final URI workspaceURI = UriHelper.synbad.namespacedUri("testWorkspace");
    private final SBWorkspaceManager m;
    private SBWorkspace ws;
    
    public ComponentTest() {
        m = Lookup.getDefault().lookup(SBWorkspaceManager.class);
        setupWorkspace();
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
        m.closeWorkspace(workspaceURI);
    }
    
    
    private void setupWorkspace() {

        ws = m.getWorkspace(workspaceURI);
        
        Set<String> sources = new HashSet<>(Arrays.asList(
            "ExampleSystem.xml",
            "subtilinReceiverPliaG1.xml"  
        ));

        
        for(String source : sources) {
            ws.addRdf(new File(source));
        }
    }
    

    /**
     * Test of getFunctionalComponents method, of class ModuleDefinition.
     */
    @Test
    public void testGetDefinition() {
        ws.getObjects(Component.class).stream().forEach(
            c -> {
                assert(c.getDefinition() != null);
                assert(ws.containsObject(
                        c.getDefinition().getIdentity(), ComponentDefinition.class));
            }
        );

    }

    
}
