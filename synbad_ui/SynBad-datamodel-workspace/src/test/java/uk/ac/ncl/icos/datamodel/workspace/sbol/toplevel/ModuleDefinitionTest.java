/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.datamodel.workspace.sbol.toplevel;

import uk.ac.ncl.icos.datamodel.workspace.objects.SynBadObject;
import uk.ac.ncl.icos.datamodel.workspace.sbol.ModuleDefinition;
import java.io.File;
import java.net.URI;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openide.util.Lookup;
import uk.ac.ncl.icos.datamodel.workspace.api.SBEntity;
import uk.ac.ncl.icos.datamodel.workspace.api.SBWorkspace;
import uk.ac.ncl.icos.synbad.graph.api.SBValue;
import uk.ac.ncl.icos.datamodel.workspace.api.SBWorkspaceManager;
import uk.ac.ncl.icos.synbad.datadefinition.UriHelper;

/**
 *
 * @author owengilfellon
 */
public class ModuleDefinitionTest {
    
    private final URI workspaceURI = UriHelper.synbad.namespacedUri("testWorkspace");
    private final SBWorkspaceManager m;
    private SBWorkspace ws;
    
    public ModuleDefinitionTest() {
        m = Lookup.getDefault().lookup(SBWorkspaceManager.class);
        setupWorkspace();
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
        m.closeWorkspace(workspaceURI);
    }
    
    
    private void setupWorkspace() {

        ws = m.getWorkspace(workspaceURI);
        
        Set<String> sources = new HashSet<>(Arrays.asList(
            "ExampleSystem.xml",
            "subtilinReceiverPliaG1.xml"  
        ));

        
        for(String source : sources) {
            ws.addRdf(new File(source));
        }
    }
    
     
    /*
     * Test of getSources method, of class IRdfWorkspace.
    */
    @Test
    public void testSetName() {
        ws.getObjects(ModuleDefinition.class).stream().forEach(
            o -> {o.setName(o.getDisplayId() + "_newName");
            assert(o.getName().equals(o.getDisplayId() + "_newName"));}
        );
    }


    @Test
    public void testSetDerivedFrom() {

         ws.getObjects(ModuleDefinition.class).stream().forEach(
            o -> {
                URI derivedFrom = URI.create("http://www.synbad.org/anotherModuleDef");
                o.setWasDerivedFrom(derivedFrom);
                assert(o.getWasDerivedFrom().equals(derivedFrom));
            }
        );
    }    /**/
    
    @Test
    public void testSetDescription() {

          ws.getObjects(ModuleDefinition.class).stream().forEach(
            o -> {
                String original = o.getDescription();
                o.setDescription(original + "!");
                assert(o.getDescription().equals(original + "!"));}
        );
    }
    
    @Test
    public void testSetValue() {

        ws.getObjects(ModuleDefinition.class).stream().forEach(o -> {
                SBValue v = new SBValue("Property Value");
                o.setValue("http://www.synbad.org/myproperty", v);
                assert(o.getValue("http://www.synbad.org/myproperty").equals(v));}
        );
    }
    



    /**
     * Test of getFunctionalComponents method, of class ModuleDefinition.
     */
    @Test
    public void testGetFunctionalComponents() {
        Set<URI> childUris = ws.getObjects(ModuleDefinition.class).stream()
                .flatMap(m -> m.getNestedObjects().stream())
                .map(SBEntity::getIdentity).collect(Collectors.toSet());
        
        Set<URI> fcUris = ws.getObjects(ModuleDefinition.class).stream()
                .flatMap(m -> m.getFunctionalComponents().stream())
                .map(SynBadObject::getIdentity).collect(Collectors.toSet());

        assert(childUris.containsAll(fcUris));

    }

    /**
     * Test of getModules method, of class ModuleDefinition.
     */
    @Test
    public void testGetModules() {
        Set<URI> childUris = ws.getObjects(ModuleDefinition.class).stream()
            .flatMap(m -> m.getNestedObjects().stream())
            .map(SBEntity::getIdentity).collect(Collectors.toSet());
        
        Set<URI> moduleUris = ws.getObjects(ModuleDefinition.class).stream()
            .flatMap(m -> m.getModules().stream())
            .map(SynBadObject::getIdentity).collect(Collectors.toSet());

        assert(childUris.containsAll(moduleUris));
    }

    /**
     * Test of getInteractions method, of class ModuleDefinition.
     */
    @Test
    public void testGetInteractions() {
        Set<URI> childUris = ws.getObjects(ModuleDefinition.class).stream()
            .flatMap(m -> m.getNestedObjects().stream())
            .map(SBEntity::getIdentity).collect(Collectors.toSet());
        
        Set<URI> interactionsUris = ws.getObjects(ModuleDefinition.class).stream()
            .flatMap(m -> m.getInteractions().stream())
            .map(SynBadObject::getIdentity).collect(Collectors.toSet());

        assert(childUris.containsAll(interactionsUris));
    }
    
}
