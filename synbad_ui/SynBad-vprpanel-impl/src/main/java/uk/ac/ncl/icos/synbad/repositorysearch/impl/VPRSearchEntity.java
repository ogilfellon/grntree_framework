/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.repositorysearch.impl;

import org.openide.util.lookup.ServiceProvider;
import uk.ac.ncl.icos.synbad.repositorysearch.api.InteractionType;
import uk.ac.ncl.icos.synbad.repositorysearch.api.Organism;
import uk.ac.ncl.icos.synbad.repositorysearch.api.PartType;
import uk.ac.ncl.icos.synbad.repositorysearch.api.PropertyName;
import uk.ac.ncl.icos.synbad.repositorysearch.api.RepositorySearchEntity;
import uk.ac.ncl.icos.synbad.repositorysearch.api.SVPLeafNode;

import uk.ac.ncl.intbio.virtualparts.entity.SearchEntity;

/**
 *
 * @author owengilfellon
 */
@ServiceProvider(service = RepositorySearchEntity.class)
public class VPRSearchEntity implements RepositorySearchEntity {
    
    private SearchEntity entity;
    private SVPLeafNode interactsWith;
    private InteractionType hasInteraction;

    public VPRSearchEntity() {
        entity = new SearchEntity();
    }
    
    @Override
    public void setSearchText(String searchText) { entity.setSearchText(searchText); }

    @Override
    public void setOrganism(Organism organism) { entity.setOrganism(organism.toString()); }

    @Override
    public void setPartType(PartType partType) { entity.setPartType(partType.toString()); }

    @Override
    public void setPropertyName(PropertyName propertyName) { entity.setPropertyName(null); }

    @Override
    public void setPropertyValue(String propertyValue) { entity.setPropertyValue(propertyValue); }

    @Override
    public void setExactPropertyValueMatch(boolean exactPropertyValueMatch) { entity.setExactPropertyValueMatch(exactPropertyValueMatch);}

    @Override
    public void setIncludeGeneticElements(boolean includeGeneticElements) { entity.setIncludeGeneticElements(includeGeneticElements); }
    
    @Override
    public void setInteractsWith(SVPLeafNode node) { this.interactsWith = node; }

    @Override
    public void setHasInteractions(InteractionType interactionType) { this.hasInteraction = interactionType;}

    @Override
    public String getSearchText() { return entity.getSearchText(); }

    @Override
    public Organism getOrganism() { return Organism.fromString(entity.getOrganism()); }

    @Override
    public PartType getPartType() { return (PartType.fromString(entity.getPartType())); }

    @Override
    public PropertyName getPropertyName() { return (PropertyName.fromString(entity.getPropertyName())); }

    @Override
    public String getPropertyValue() { return entity.getPropertyValue(); }

    @Override
    public SVPLeafNode getInteractsWith() { return interactsWith; }

    @Override
    public InteractionType getHasInteractions() { return hasInteraction; }

    @Override
    public boolean getExactPropertyValueMatch() { return entity.getExactPropertyValueMatch(); }

    @Override
    public boolean getIncludeGeneticElements() { return entity.getIncludeGeneticElements();}

}
