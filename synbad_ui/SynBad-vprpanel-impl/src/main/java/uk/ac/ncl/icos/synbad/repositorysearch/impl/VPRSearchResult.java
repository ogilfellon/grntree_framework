/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.repositorysearch.impl;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.List;
import org.openide.util.lookup.ServiceProvider;
import uk.ac.ncl.icos.synbad.repositorysearch.api.RepositorySearchResults;
import uk.ac.ncl.intbio.virtualparts.entity.Part;

/**
 *
 * @author owengilfellon
 */
@ServiceProvider(service = RepositorySearchResults.class)
public class VPRSearchResult implements RepositorySearchResults {
    
    private final static String ADD_CHILD = "ADD";
    private final static String LABEL_SEARCH = "LABEL";
    
    PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);
    List<Part> parts = new ArrayList<>();
    String label = "";
    
    @Override
    public void setSearchName(String name) {
        String oldLabel = this.label;
        this.label = name;
        changeSupport.firePropertyChange(LABEL_SEARCH, oldLabel, label);
    }
 
    @Override
    public void addParts(List<Part> parts) {
        for(Part part : parts) {
            addPart(part);
        }
    }
    
    @Override
    public void addPart(Part part) {
        int size = parts.size();
        parts.add(part);
        changeSupport.firePropertyChange(ADD_CHILD, size, parts.size());
    }
    
    @Override
    public List<Part> getParts()
    {
        return parts;
    }
    
    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener)
    {
        changeSupport.addPropertyChangeListener(listener);
    }
    
    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener)
    {
        changeSupport.removePropertyChangeListener(listener);
    }

    @Override
    public String getSearchName() {
        return label;
    }

}
