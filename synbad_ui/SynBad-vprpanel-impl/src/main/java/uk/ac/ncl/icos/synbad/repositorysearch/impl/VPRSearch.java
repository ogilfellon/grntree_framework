/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.repositorysearch.impl;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Observable;
import javax.swing.SwingWorker;
import org.openide.util.Exceptions;
import org.openide.util.RequestProcessor;
import org.openide.util.lookup.ServiceProvider;
import uk.ac.ncl.icos.synbad.repositorysearch.api.Organism;
import uk.ac.ncl.icos.synbad.repositorysearch.api.PartType;
import uk.ac.ncl.icos.synbad.repositorysearch.api.PropertyName;
import uk.ac.ncl.icos.synbad.repositorysearch.api.RepositorySearch;
import uk.ac.ncl.icos.synbad.repositorysearch.api.RepositorySearchEntity;
import uk.ac.ncl.icos.synbad.repositorysearch.api.RepositorySearchResults;
import uk.ac.ncl.intbio.virtualparts.PartsHandler;
import uk.ac.ncl.intbio.virtualparts.entity.Part;
import uk.ac.ncl.intbio.virtualparts.entity.SearchEntity;

/**
 *
 * @author owengilfellon
 */

@ServiceProvider(service = RepositorySearch.class)
public class VPRSearch implements RepositorySearch {
    
    private RepositorySearchEntity searchEntity = null;
    private String searchURL = "";

    @Override
    public void setSearchEntity(RepositorySearchEntity searchEntity) {
        this.searchEntity = searchEntity;
    }

    @Override
    public void setSearchURL(String repositoryURL) {
        this.searchURL = repositoryURL;
    }
    
    @Override
    public RepositorySearchResults search() throws IllegalStateException {

        if(searchEntity == null || searchURL.equals("")) {
            throw new IllegalStateException("SearchEntity and SearchURL must be set");
        }

        Date date = new Date();
        SimpleDateFormat format = new SimpleDateFormat("E yyyy.MM.dd-hh:mm:ss");
        RepositorySearchResults result = new VPRSearchResult();
        SearchWorker search = new SearchWorker(searchURL, searchEntity, result);
        result.setSearchName("Search: " + format.format(date));
        RequestProcessor.getDefault().post(search);
        return result;   
    }
    
    private class SearchWorker extends SwingWorker
    {
        private final RepositorySearchEntity entity;
        private final RepositorySearchResults result;
        private final PartsHandler handler;

        public SearchWorker(String searchURL, RepositorySearchEntity entity, RepositorySearchResults result)
        {
            this.entity = entity;
            this.result = result;
            this.handler = new PartsHandler(searchURL);
        }
        
        @Override
        protected Object doInBackground() throws Exception {
            try {
                
                String partType = entity.getPartType() == PartType.None ? null : entity.getPartType().toString();
                String organism = entity.getOrganism() == Organism.None ? null : entity.getOrganism().toString();
                String propertyName = entity.getPropertyName() == PropertyName.none ? null : entity.getPartType().toString();
                
                SearchEntity searchEntity = new SearchEntity(
                        entity.getSearchText(),
                        partType,
                        organism,
                        propertyName,
                        entity.getPropertyValue(),
                        entity.getExactPropertyValueMatch(),
                        entity.getIncludeGeneticElements());
                
                int pages = handler.GetPartsSummary(searchEntity).getPageCount();
                for(int i = 1; i<= pages; i++)
                {
                    List<Part> parts = handler.GetParts(i, searchEntity).getParts();
                    if(parts != null && parts.size() > 0) {
                            result.addParts(parts);
                    }
                    
                }
            } catch (IOException ex) {
                Exceptions.printStackTrace(ex);
            }
            
            return null;
        
        }
    }
}
