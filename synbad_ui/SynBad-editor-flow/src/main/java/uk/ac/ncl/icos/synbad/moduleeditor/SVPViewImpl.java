package uk.ac.ncl.icos.synbad.moduleeditor;

import java.awt.Color;
import java.awt.Point;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DragGestureEvent;
import java.awt.dnd.DragGestureListener;
import java.awt.dnd.DragSource;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.netbeans.api.visual.action.ActionFactory;
import org.netbeans.api.visual.action.SelectProvider;
import org.netbeans.api.visual.action.WidgetAction;
import org.netbeans.api.visual.layout.Layout;
import org.netbeans.api.visual.layout.LayoutFactory;
import org.netbeans.api.visual.layout.SceneLayout;
import org.netbeans.api.visual.model.ObjectScene;
import org.netbeans.api.visual.widget.LayerWidget;
import org.netbeans.api.visual.widget.Widget;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;
import uk.ac.ncl.icos.synbad.commandmanager.ICommandManager;

import uk.ac.ncl.icos.synbad.moduleeditor.lookfeel.GroupLookFeel;
import uk.ac.ncl.icos.synbad.svpfragment.nodes.VSynBadObjectNode;
import uk.ac.ncl.icos.datamodel.workspace.api.IEntity;

/**
 *
 * @author owengilfellon
 */
public class SVPViewImpl extends ObjectScene implements Lookup.Provider {
    
    private final String DELETEACTION = "deleteAction";
    
    private final Lookup lkp;
    private final InstanceContent ic;
    private final VSynBadObjectNode moduleNode;

    private final ICommandManager commandManager;
    private final LayerWidget mainLayer;
    private final LayerWidget connectionLayer;
  //  private final ModuleWidget rootModule;
    private final SceneLayout sceneLayout;
    private final WidgetAction selectAction = ActionFactory.createSelectAction(new SVPSelectProvider());
/*
    public ModuleWidget getRootModule() {
        
        return rootModule;
    }*/

    public SVPViewImpl(VSynBadObjectNode moduleNode)
    {
        this.moduleNode = moduleNode;
        GroupLookFeel lookfeel = new GroupLookFeel();
        setLookFeel(lookfeel);
        if(lookfeel.getBackground() instanceof Color)
            setBackground(((Color)lookfeel.getBackground(25)));

        this.commandManager = Lookup.getDefault().lookup(ICommandManager.class);

        this.ic = new InstanceContent();
        this.lkp = new AbstractLookup(ic);
        this.mainLayer = new LayerWidget(this);        
        this.connectionLayer = new LayerWidget(this);
 
        addChild(mainLayer);
        addChild(connectionLayer);

        //refreshInteractions();
        
        Layout layout = LayoutFactory.createHorizontalFlowLayout(LayoutFactory.SerialAlignment.JUSTIFY, 3);       
       
        createView();
        sceneLayout = LayoutFactory.createDevolveWidgetLayout(mainLayer, layout, false);
        IEntity module = moduleNode.getLookup().lookup(IEntity.class);
        assert(module!=null);


       // rootModule = setDesign(view);
        
        sceneLayout.invokeLayout(); 
 
        DragSource.getDefaultDragSource().createDefaultDragGestureRecognizer(getView(), DnDConstants.ACTION_COPY_OR_MOVE, new DragGestureListener() {
 
            @Override
            public void dragGestureRecognized (DragGestureEvent dge) {

                Node node = null;
                Widget w = null;
                
                for(Object o:getObjects()) {
                    w = findWidget(o);
                    if(w.getState().isHovered()) {
                        node=(Node)o;
                    }
                }

                if(node!=null && w.getParentWidget()!=null) {
                     w.removeFromParent();  
                //    if(node instanceof VEntityNode)
                       // dge.startDrag(null, node.getIcon(BeanInfo.ICON_COLOR_32x32), new Point(0, 0), new EntityTransferable(), null);
               
                       //  dge.startDrag(null, new ModuleInstanceTransferable((ModuleInstanceNode)node));
                }
            }
        });
             /**/
        /*

        createActions("none");
        createActions("promoter").addAction(new PrototypeAdder("promoter"));
        createActions("operator").addAction(new PrototypeAdder("operator"));
        createActions("rbs").addAction(new PrototypeAdder("rbs"));
        createActions("cds").addAction(new PrototypeAdder("cds"));
        createActions("terminator").addAction(new PrototypeAdder("terminator"));
        createActions("shim").addAction(new PrototypeAdder("shim"));
        
        getActions().addAction(ActionFactory.createZoomAction());
*/
    
        
        
    //  getActions("none").addAction(ActionFactory.createAcceptAction(new SVPAcceptProvider()));
   //      setActiveTool("none");
        
      //  getActions().addAction(ActionFactory.createAcceptAction(new SVPAcceptProvider()));
    }   

    @Override
    public Lookup getLookup()
    {
        return lkp;
    }
    
    public WidgetAction getSelectAction() {
        return selectAction;
    }

    
    public void setSelection(Node node)
    {
        List<Node> c = new ArrayList<>();
        if(node!=null)
            c.add(node);
        ic.set(c, null);
    }
    /*
    private ModuleWidget setDesign(IDataModelView view)
    {
        ModuleInstanceNode min = new ModuleInstanceNode(view, view.getGraph().getRoot());
        assert(min!=null);
        ModuleWidget w = new ModuleWidget(this, min);
        mainLayer.addChild(w);
        addObject(moduleNode.getLookup().lookup(IEntity.class), w);
        return w;
    }*/

    
    private class SVPSelectProvider implements SelectProvider {

        @Override
        public boolean isAimingAllowed (Widget widget, Point localLocation, boolean invertSelection) {
            return false;
        }

        @Override
        public boolean isSelectionAllowed (Widget widget, Point localLocation, boolean invertSelection) {
            return findObject (widget) != null;
        }

        @Override
        public void select (Widget widget, Point localLocation, boolean invertSelection) {
            Object object = findObject (widget);
            setFocusedObject (object);
            //setSelection(widget.getLookup().lookup(Node.class));
            if (object != null) {
                if (! invertSelection  &&  getSelectedObjects ().contains (object))
                    return;
                userSelectionSuggested (Collections.singleton (object), invertSelection);
            } else
                userSelectionSuggested (Collections.emptySet (), invertSelection);
            
            getView().requestFocusInWindow();
        }
        /*
        @Override
            public void select(Widget widget, Point localLocation, boolean invertSelection) {
                Set set = new HashSet();
                set.add(getLookup().lookup(Node.class).getLookup().lookup(GRNTreeNode.class));
                ((ObjectScene)getScene()).setSelectedObjects(set);
             ((ObjectScene)getScene()).setFocusedObject(
                         getLookup().lookup(Node.class).getLookup().lookup(GRNTreeNode.class));   
                scene.setFocusedWidget(widget);
                
            }*/
    }

    @Override
    protected void paintWidget() {
        
        super.paintWidget(); //To change body of generated methods, choose Tools | Templates.
    }

    public LayerWidget getConnectionLayer() {
        return connectionLayer;
    }

    public LayerWidget getMainLayer() {
        return mainLayer;
    }

}
