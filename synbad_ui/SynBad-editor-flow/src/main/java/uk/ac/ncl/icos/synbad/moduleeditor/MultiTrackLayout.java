/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.moduleeditor;

import java.awt.Rectangle;
import java.util.List;
import org.netbeans.api.visual.layout.Layout;
import org.netbeans.api.visual.widget.Widget;

/**
 *
 * @author owengilfellon
 */
public class MultiTrackLayout implements Layout {
    
    private int rows = 0;
    private int track_height = 80;
    private int track_gap = 20;
    private int group_gap = 20;
    
    @Override
    public void layout (Widget widget) {
        for (Widget child : widget.getChildren ()) {
            child.resolveBounds (null, null);
        }
    }

    @Override
    public boolean requiresJustification (Widget widget) {
        return true;
    }

    @Override
    public void justify (Widget widget) {
        Rectangle bounds = widget.getClientArea();
        List<Widget> children = widget.getChildren();
        int index = 0;
        int row = 0;
        for(Widget w : children) {
            index++;
            
            w.resolveBounds(null, new Rectangle(0, row * (track_height + track_gap), bounds.width, track_height));
        }

    }
}
