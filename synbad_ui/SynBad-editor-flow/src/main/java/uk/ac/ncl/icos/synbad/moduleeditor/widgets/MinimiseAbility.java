/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.moduleeditor.widgets;

/**
 *
 * @author owengilfellon
 */
public interface MinimiseAbility {
    
    public void collapseWidget();
    public void expandWidget();
    public boolean isCollapsed();
    
}
