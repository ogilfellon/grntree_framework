/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.moduleeditor.lookfeel;

import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Paint;
import java.awt.Rectangle;
import org.netbeans.api.visual.border.Border;

/**
 *
 * @author owengilfellon
 */
public class CurvedBorder implements Border {

    private  Paint background;
    
    public CurvedBorder(Paint background) {
       this.background = background;
    }

    @Override
    public Insets getInsets() {
        return new Insets(0, 0, 0, 0);
    }

    @Override
    public void paint(Graphics2D gr, Rectangle bounds) {
        gr.setPaint(background);
        gr.drawRoundRect(bounds.x, bounds.y, bounds.width, bounds.height, 20, 20);

    }

    @Override
    public boolean isOpaque() {
        return true;
    }
    
}
