/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.moduleeditor;

import org.openide.util.lookup.ServiceProvider;
import org.openide.windows.TopComponent;
import uk.ac.ncl.icos.synbad.datamodel.visualisation.HierarchicalGraphView;
import uk.ac.ncl.icos.synbad.svpfragment.nodes.EntityViewProvider;
import uk.ac.ncl.icos.view.api.View;

/**
 *
 * @author owengilfellon
 */
@ServiceProvider(service=EntityViewProvider.class)
public class DefaultModuleEditorProvider implements EntityViewProvider {

    @Override
    public TopComponent getView(View view) {
        return new HierarchicalGraphView(view);
        //return new NewHGraphView(moduleNode);
    }

    @Override
    public String getName() {
        return "Flow Editor";
    }
    
    
    
}
