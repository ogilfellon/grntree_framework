/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.visualisation.javafx;

import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Group;
import javafx.scene.PerspectiveCamera;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.transform.Rotate;

/**
 *
 * @author owengilfellon
 */
public class JavaFxPanel extends JFXPanel {

    public JavaFxPanel() {
        super();
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                initFX(JavaFxPanel.this);
            }
       });
    }

    private void initFX(JFXPanel fxPanel) {
        // This method is invoked on the JavaFX thread
        Scene scene = createScene();
        fxPanel.setScene(scene);
    }

    private Scene createScene() {

        Group  root  =  new  Group();
        Scene  scene  =  new  Scene(root, Color.ALICEBLUE);
        scene.setCamera(new PerspectiveCamera());
        Text  text  =  new  Text();
        
        Rotate r = new Rotate(10, 10, 10, 10);
        text.getTransforms().add(r);

        
        text.setX(40);
        text.setY(100);
        text.setFont(new Font(25));
        text.setText("Welcome JavaFX!");

        root.getChildren().add(text);
      

        return (scene);
    }
   
    
}
