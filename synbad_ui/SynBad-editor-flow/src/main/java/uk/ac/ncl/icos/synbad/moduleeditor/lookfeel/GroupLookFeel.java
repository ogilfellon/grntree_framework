/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.moduleeditor.lookfeel;

import java.awt.Color;
import java.awt.Paint;
import org.netbeans.api.visual.border.Border;
import org.netbeans.api.visual.border.BorderFactory;
import org.netbeans.api.visual.laf.LookFeel;
import org.netbeans.api.visual.model.ObjectState;

/**
 *
 * @author owengilfellon
 */
public class GroupLookFeel extends LookFeel {

    @Override
    public Paint getBackground() {   
        return new Color(210, 210, 235);
    }
    
    public Color getBackground(int increment) {
        int r =  ((Color)getBackground()).getRed() + increment <= 255 ? ((Color)getBackground()).getRed() + increment : 255;
        int g =  ((Color)getBackground()).getGreen() + increment <= 255 ? ((Color)getBackground()).getGreen() + increment : 255;
        int b =  ((Color)getBackground()).getBlue() + increment <= 255 ? ((Color)getBackground()).getBlue() + increment : 255;
        //int a =  ((Color)getBackground()).getAlpha();
        return new Color(r, g, b);
    }

    @Override
    public Color getForeground() {
        return new Color(150, 150, 170);
    }
    
    public Color getForeground(int increment) {
        int r =  getForeground().getRed() + increment <= 255 ? getForeground().getRed() + increment : 255;
        int g =  getForeground().getGreen() + increment <= 255 ? getForeground().getGreen() + increment : 255;
        int b =  getForeground().getBlue() + increment <= 255 ? getForeground().getBlue() + increment : 255;
        return new Color(r, g, b);
    }

    @Override
    public Border getBorder(ObjectState state) {
        if(state.isSelected()) {
            return new CurvedBorder(getForeground().brighter());
        } else {
            return new CurvedBorder(getForeground());
        }
        
    }

    @Override
    public Border getMiniBorder(ObjectState state) {
        if(!state.isSelected()) {
            return BorderFactory.createEmptyBorder(5);
        } else {
            return new CurvedBorder(getBackground());
        }
    }

    @Override
    public boolean getOpaque(ObjectState state) {
        return true;
    }

    @Override
    public Color getLineColor(ObjectState state) {
        return getForeground();
    }

    @Override
    public Paint getBackground(ObjectState state) {
        if(state.isHovered()) {
            return getBackground(20);
        } else if (state.isSelected()) {
            return getBackground(40);
        } else {
            return getBackground();
        }
    }

    @Override
    public Color getForeground(ObjectState state) {
       if(state.isHovered()) {
            return getForeground(20);
        } else if (state.isSelected()) {
            return getForeground(40);
        } else {
            return getForeground();
        }
    }

    @Override
    public int getMargin() {
        return 2;
    }
}
