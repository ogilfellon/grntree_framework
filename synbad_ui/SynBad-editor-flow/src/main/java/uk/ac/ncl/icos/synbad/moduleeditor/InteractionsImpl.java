package uk.ac.ncl.icos.synbad.moduleeditor;

import java.awt.Color;
import java.awt.Point;
import java.util.Collections;
import org.netbeans.api.visual.action.ActionFactory;
import org.netbeans.api.visual.action.SelectProvider;
import org.netbeans.api.visual.action.WidgetAction;
import org.netbeans.api.visual.layout.Layout;
import org.netbeans.api.visual.layout.LayoutFactory;
import org.netbeans.api.visual.layout.SceneLayout;
import org.netbeans.api.visual.model.ObjectScene;
import org.netbeans.api.visual.widget.LayerWidget;
import org.netbeans.api.visual.widget.Widget;
import org.openide.util.Lookup;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;
import uk.ac.ncl.icos.datamodel.workspace.api.IDataModelEdge;
import uk.ac.ncl.icos.view.api.ViewCrawler;
import uk.ac.ncl.icos.view.api.VModel;
import uk.ac.ncl.icos.view.api.ViewCursor;
import uk.ac.ncl.icos.view.api.View;
import uk.ac.ncl.icos.view.impl.VModelImpl;
import uk.ac.ncl.icos.view.impl.ViewCrawlerAdapter;
import uk.ac.ncl.icos.view.impl.IteratorRule;
import uk.ac.ncl.icos.datamodel.workspace.api.IInstance;
import uk.ac.ncl.icos.synbad.moduleeditor.lookfeel.GroupLookFeel;
import uk.ac.ncl.icos.datamodel.workspace.api.IEntity;
import uk.ac.ncl.icos.datamodel.workspace.api.Annotatable;

/**
 *
 * @author owengilfellon
 */
public class InteractionsImpl extends ObjectScene implements Lookup.Provider {
    
    private final String DELETEACTION = "deleteAction";
    
    private final Lookup lkp;
    private final IEntity obj;


    private final LayerWidget groupLayer;
    private final LayerWidget interactionWidget;
    private final InstanceContent ic;
    private final SceneLayout sceneLayout;
    
    private final WidgetAction selectAction = ActionFactory.createSelectAction(new SVPSelectProvider());
   
    public InteractionsImpl(IEntity  obj)
    {
        GroupLookFeel lookfeel = new GroupLookFeel();
        setLookFeel(lookfeel);
        if(lookfeel.getBackground() instanceof Color)
            setBackground(((Color)lookfeel.getBackground(25)));

        this.obj = obj;

        this.ic = new InstanceContent();
        this.lkp = new AbstractLookup(ic);
        this.groupLayer = new LayerWidget(this);        
        this.interactionWidget = new LayerWidget(this);
        addChild(groupLayer);
        addChild(interactionWidget);
       
        setDesign();
        //refreshInteractions();
        
        Layout layout = LayoutFactory.createHorizontalFlowLayout(LayoutFactory.SerialAlignment.JUSTIFY, 3);       
        sceneLayout = LayoutFactory.createDevolveWidgetLayout(groupLayer, layout, false);
        sceneLayout.invokeLayout(); 
        createView();
        
        /*

        createActions("none");
        createActions("promoter").addAction(new PrototypeAdder("promoter"));
        createActions("operator").addAction(new PrototypeAdder("operator"));
        createActions("rbs").addAction(new PrototypeAdder("rbs"));
        createActions("cds").addAction(new PrototypeAdder("cds"));
        createActions("terminator").addAction(new PrototypeAdder("terminator"));
        createActions("shim").addAction(new PrototypeAdder("shim"));
        
        getActions().addAction(ActionFactory.createZoomAction());
*/
        /*
        DragSource.getDefaultDragSource().createDefaultDragGestureRecognizer(
            getView(), DnDConstants.ACTION_COPY_OR_MOVE, new DragGestureListener() {
 
            @Override
            public void dragGestureRecognized (DragGestureEvent dge) {

                GRNTreeNode node = null;
                
                for(Object o:getObjects()) {
                    Widget w = findWidget(o);
                    if(w.getState().isHovered()) {
                        node=(GRNTreeNode)o;
                    }
                }

                if(node!=null) {
                    if(node instanceof LeafNode) {
                        SVPLeafNode n = new SVPLeafNode(node.duplicate());
                        removePart((LeafNode)node);
                        dge.startDrag(null, n.getIcon(BeanInfo.ICON_COLOR_32x32), new Point(0, 0), new PartTransferable(n), null);
                    } else if (node instanceof BranchNode){
                        SVPBranchNode n = new SVPBranchNode(node.duplicate());
                        removeGroup((BranchNode)node);
                        dge.startDrag(null, new GroupTransferable(n));
                    }
                }
            }
        });*/
        
         //getActions("none").addAction(ActionFactory.createAcceptAction(new SVPAcceptProvider()));
         setActiveTool("none");
    }   

    @Override
    public Lookup getLookup()
    {
        return lkp;
    }
    
    private void setDesign()
    {
        

    }
    
    
    
    /*
    
    private List<LeafNode> getPartsAndSpeciesByName(String name)
    {
        List<LeafNode> parts = new ArrayList<>();
        for(Object o  : getObjects()) {
            if(o instanceof LeafNode) {
                if(((LeafNode)o).getName().equalsIgnoreCase(name)) {
                    parts.add((LeafNode)o);
                } else {
                    LeafNode node = (LeafNode) o;
                    List<Interaction> interactions = SVPManager.getSVPManager().getInternalEvents(node.getSVP());
                    for(Interaction interaction : interactions) {
                        for(InteractionPartDetail ipd : interaction.getPartDetails()) {
                            if(ipd.getPartName().equals(name) &&
                                    ipd.getInteractionRole().equals("Output")) {
                                parts.add(node);
                            }
                        }
                    }
                }
            }
        }
        return parts;
    }

    public WidgetAction getSelectAction() {
        return selectAction;
    }

    
    public void setSelection(Node node)
    {
        List<Node> c = new ArrayList<>();
        if(node!=null)
            c.add(node);
        ic.set(c, null);
    }
    private IEntityNode getGroupFromTransferable(Transferable transferable) {
        Object o = null;
        try {
            o = transferable.getTransferData(GroupFlavor.GROUP_FLAVOR);
        } catch (IOException | UnsupportedFlavorException ex) {
            System.out.println("DataFlavor not valid");
        }
        
        if(o instanceof SVPBranchNode)
            return (SVPBranchNode)o;
        else return null;
    }


    private void refreshInteractions()
    {   
        
        Set<SVPInteractionNode> oldInteractionNodes = new HashSet<>();
        GRNTree tree = obj.getLookup().lookup(GRNTree.class);
        Iterator<GRNTreeNode> i = tree.getPreOrderIterator();
        Set<Interaction> interactions = new HashSet<>();
 
        for(Object o : getObjects()) {
            if(o instanceof SVPInteractionNode) {
                oldInteractionNodes.add((SVPInteractionNode)o);
            }
        }

        for(SVPInteractionNode node : oldInteractionNodes) {
            removeInteractionNode(node);
        }
        
        while(i.hasNext()) {
            GRNTreeNode n = i.next();
            interactions.addAll(tree.getInteractions(n));
        }
        
        for(Interaction interaction : interactions) {
            
            List<LeafNode> from = new ArrayList<>();
            List<LeafNode> to = new ArrayList<>();
            List<InteractionPartDetail> ipds = interaction.getPartDetails();
            
            if(interaction.getInteractionType().equals("Phosphorylation") && ipds != null) {
                
                for(InteractionPartDetail ipd : ipds) {
                    
                    if(ipd.getInteractionRole().equals("Input") && ipd.getPartForm().equals("Phosphorylated")) {
                        from.addAll( getPartsAndSpeciesByName(ipd.getPartName()));
                    }
                    
                    else if (ipd.getInteractionRole().equals("Input") && ipd.getPartForm().equals("Default")) {
                        to.addAll( getPartsAndSpeciesByName(ipd.getPartName()));
                    }
                }
            }
            
            else if ((interaction.getInteractionType().contains("activation") || 
                    interaction.getInteractionType().contains("repression")) && ipds!=null) {
                
                for(InteractionPartDetail ipd : ipds) {
                    
                    if(ipd.getInteractionRole().equals("Modifier")) {
                        from.addAll( getPartsAndSpeciesByName(ipd.getPartName()));
                    } 
                    
                    for(String s :interaction.getParts()) {
                        if(!s.equals(ipd.getPartName())) {
                             to.addAll( getPartsAndSpeciesByName(s));
                        }
                    }
                }
            }

            if( !from.isEmpty() && !to.isEmpty()) {
                for(LeafNode node_from : from) {
                    for(LeafNode node_to : to) {
                        SVPInteractionNode node = new SVPInteractionNode(interaction);
                        if(getObjects().contains(node_from) && getObjects().contains(node_to) && !getObjects().contains(node)) {
                            Widget w = addInteraction(node);
                            attachEdgeSourceAnchor(node, node_from);
                            attachEdgeTargetAnchor(node, node_to);  
                        }
                    }
                }
            }   
        }
        
        validate();
    }
    
    private void modified()
    {
        if (getLookup().lookup(FileFragmentSaveable.class) == null) {
            ic.add(new FileFragmentSaveable(obj));
        }
    }
    
  
    public SynbadSbolDataObject  getDataObject() {
        return obj;
    }
    
    private void internalAddNode(GRNTreeNode node, GRNTreeNode parent)
    {
        try {
            parent.addNode(node);
            modified();
        } catch (Exception ex) {
            Exceptions.printStackTrace(ex);
        }
    }
    
    private void internalAddNode(int index, GRNTreeNode node, GRNTreeNode parent)
    {
        try {
            parent.addNode(index, node);
            modified();
        } catch (Exception ex) {
            Exceptions.printStackTrace(ex);
        }
    }
    
    private void internalRemoveNode(GRNTreeNode node)
    {
        node.getParent().removeNode(node);
        modified();
    }
    
    public Widget addGroup(BranchNode node, GroupWidget parent)
    {
        Widget w = new GroupWidget(this, new SVPBranchNode(node)); 
        addObject(node, w);
        
        if(parent==null) {
             groupLayer.addChild(w);
             internalAddNode(node, (BranchNode)obj.getLookup().lookup(GRNTree.class).getRootNode());
        }
           
        else {
            parent.addComponent(w);
            internalAddNode(node, (BranchNode)findObject(parent));
        }
        
        refreshInteractions();
        return w;
    }
    
    public Widget addGroup(int index,BranchNode node, GroupWidget parent)
    {
        Widget w = new GroupWidget(this, new SVPBranchNode(node));
        addObject(node, w);
        
        if(parent==null) {
             groupLayer.addChild(w);
             internalAddNode(node, (BranchNode)obj.getLookup().lookup(GRNTree.class).getRootNode());
        }
           
        else {
            parent.addComponent(index, w);
            internalAddNode(index, node, (BranchNode)findObject(parent));
        }
           
        refreshInteractions();
        return w;
    }
    
    public void removeGroup(BranchNode node)
    {
        GroupWidget group = (GroupWidget)findWidget(node);
        List<GRNTreeNode> components = new ArrayList<>();
        
        for(Widget w : group.getComponents()) {
            if(w instanceof GroupWidget) {
                components.add(w.getLookup().lookup(Node.class).getLookup().lookup(GRNTreeNode.class));
            } else {
                components.add(w.getLookup().lookup(Node.class).getLookup().lookup(GRNTreeNode.class));
            }
        }
        
        for(GRNTreeNode n : components) {
            if(n.isBranchNode()) {
                removeGroup((BranchNode)n);
            } else if (n.isLeafNode()) {
                removePart((LeafNode)n);
            }
        }
        
        group.removeFromParent();
        removeObject(node);
        internalRemoveNode(node);
        refreshInteractions();
    }
    
    
    public void removePart(LeafNode node)
    {
        Widget w = findWidget(node);
        removeObject(node);
        w.removeFromParent();
        internalRemoveNode(node);
        refreshInteractions();
    }
    
    public void addSpecies(SVPLeafNode node, SVPBranchNode parent, int index)
    {
        
    }

    protected Widget addInteraction(SVPInteractionNode edge) {
        InteractionWidget widget = new InteractionWidget(this, edge);
        //widget.setRouter(RouterFactory.createFreeRouter());
        widget.setRouter(RouterFactory.createOrthogonalSearchRouter(groupLayer));
        LabelWidget label = new LabelWidget (this, edge.getLookup().lookup(Interaction.class).getName());
        label.setBackground(Color.WHITE);
        label.setOpaque(true);
        widget.addChild(label);
        widget.setConstraint(label, LayoutFactory.ConnectionWidgetLayoutAlignment.TOP_CENTER, 0.5f); 
        interactionWidget.addChild(widget);
        addObject(edge, widget);
        return widget;
    }
    
    public void removeInteractionNode(SVPInteractionNode edge) {
        Widget w = findWidget(edge);
        if(w != null) {
            w.removeFromParent();
            removeObject(edge);
        }
    }
 

    protected void attachEdgeSourceAnchor(SVPInteractionNode edge,LeafNode sourcePin) {
        InteractionWidget c = (InteractionWidget) findWidget(edge);
        Widget widget = findWidget(sourcePin);
        Anchor a = AnchorFactory.createDirectionalAnchor(widget, AnchorFactory.DirectionalAnchorKind.VERTICAL, 4);
        //VMDNodeAnchor a = new VMDNodeAnchor(widget, false);
        c.setSourceAnchor(a);
    }

    protected void attachEdgeTargetAnchor(SVPInteractionNode edge,  LeafNode targetPin) {
        InteractionWidget c = (InteractionWidget) findWidget(edge);
        Widget widget = findWidget(targetPin);
        Anchor a = AnchorFactory.createDirectionalAnchor(widget, AnchorFactory.DirectionalAnchorKind.VERTICAL, 4);
        //VMDNodeAnchor a = new VMDNodeAnchor(widget, false);
        c.setTargetAnchor(a);
    }*/
    
   
    /*
    class WindowSizeListener implements ComponentListener
    {
        private final Scene scene;

        public WindowSizeListener(Scene scene) {
            this.scene = scene;
        }

        @Override
        public void componentResized(ComponentEvent e) {          
            for(Widget w : getChildren()) {
                w.setPreferredSize(new Dimension(e.getComponent().getWidth(), e.getComponent().getHeight()));
            }
        }

        @Override
        public void componentMoved(ComponentEvent e) {
        }

        @Override
        public void componentShown(ComponentEvent e) {
        }

        @Override
        public void componentHidden(ComponentEvent e) {
        }
    }*/
    
    private class SVPSelectProvider implements SelectProvider {

        @Override
        public boolean isAimingAllowed (Widget widget, Point localLocation, boolean invertSelection) {
            return false;
        }

        @Override
        public boolean isSelectionAllowed (Widget widget, Point localLocation, boolean invertSelection) {
            return findObject (widget) != null;
        }

        @Override
        public void select (Widget widget, Point localLocation, boolean invertSelection) {
            Object object = findObject (widget);
            setFocusedObject (object);
            //setSelection(widget.getLookup().lookup(Node.class));
            if (object != null) {
                if (! invertSelection  &&  getSelectedObjects ().contains (object))
                    return;
                userSelectionSuggested (Collections.singleton (object), invertSelection);
            } else
                userSelectionSuggested (Collections.emptySet (), invertSelection);
            
            getView().requestFocusInWindow();
        }
        /*
        @Override
            public void select(Widget widget, Point localLocation, boolean invertSelection) {
                Set set = new HashSet();
                set.add(getLookup().lookup(Node.class).getLookup().lookup(GRNTreeNode.class));
                ((ObjectScene)getScene()).setSelectedObjects(set);
             ((ObjectScene)getScene()).setFocusedObject(
                         getLookup().lookup(Node.class).getLookup().lookup(GRNTreeNode.class));   
                scene.setFocusedWidget(widget);
                
            }*/
    }
    /*
      private SVPIEntityInstanceNode getPartFromTransferable(Transferable transferable) {
        Object o = null;
        try {
            o = transferable.getTransferData(PartFlavor.PART_FLAVOR);
        } catch (IOException | UnsupportedFlavorException ex) {
            System.out.println("DataFlavor not valid");
        }
        
        if(o instanceof SVPPartNode) {
            try {
                return new SVPIEntityInstanceNode( new SVPParser(obj.getDefinitionManager())
                        .convert(((SVPPartNode)o).getLookup().lookup(Part.class)));
            } catch (Exception ex) {
                Exceptions.printStackTrace(ex);
                return null;
            }
        }
            
        else return null;
    }
      
    */
    

 /*
     private class SVPAcceptProvider implements AcceptProvider {
        
        @Override
        public ConnectorState isAcceptable(Widget widget, Point point, Transferable transferable) {
            SVPIEntityNode part = getPartFromTransferable(transferable);
            //SVPBranchNode group = getGroupFromTransferable(transferable);
            if (part != null ) {
                return ConnectorState.ACCEPT; } else { return ConnectorState.REJECT; }
        }
       

        @Override
        public void accept(Widget widget, final Point point, Transferable transferable) {

            Node o = getPartFromTransferable(transferable);
            
         if(o == null) {
                o = getGroupFromTransferable(transferable);
            }

            if(getScene() instanceof InteractionsImpl) {
                
                IEntity module = o.getLookup().lookup(EntityDefinition.class).getEntityClass() == EntityClass.IEntity ? 
                        (IEntity)o.getLookup().lookup(EntityDefinition.class) :
                        null;
                
                if(module != null) {
                    
                    
                    addPart(module, groupLayer);
                    ((ObjectScene)getScene()).setFocusedObject((LeafNode)treenode);
                    ((ObjectScene)getScene()).setSelectedObjects(set);
                } else if (o instanceof SVPBranchNode) {
                    GroupWidget groupWidget = (GroupWidget)addGroup(0, (BranchNode)o.getLookup().lookup(GRNTreeNode.class), null);
                    groupWidget.setPreferredLocation(point);
                    ((ObjectScene)getScene()).setFocusedObject((BranchNode)treenode);
                    ((ObjectScene)getScene()).setSelectedObjects(set);
                }
                 
                getScene().getView().requestFocusInWindow();
            }
        }
    } */  
}
