/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.moduleeditor.widgets;

import java.awt.BasicStroke;
import java.awt.Color;
import org.netbeans.api.visual.anchor.AnchorShape;
import org.netbeans.api.visual.model.ObjectScene;
import org.netbeans.api.visual.model.ObjectState;
import org.netbeans.api.visual.widget.ConnectionWidget;
import org.netbeans.api.visual.widget.Scene;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;
import org.openide.util.lookup.ProxyLookup;
import uk.ac.ncl.icos.synbad.moduleeditor.InteractionsImpl;
import uk.ac.ncl.icos.synbad.svpfragment.old.SVPInteractionInstanceNode;
import uk.ac.ncl.intbio.virtualparts.entity.Interaction;

/**
 *
 * @author owengilfellon
 */
public class InteractionWidget extends ConnectionWidget implements Lookup.Provider {

    private final SVPInteractionInstanceNode node;
    private final Lookup lkp;
    private final InteractionsImpl scene;
    
    
    public InteractionWidget(Scene scene, SVPInteractionInstanceNode node) {
        super(scene);
        this.scene = (InteractionsImpl) scene;
        this.node = node;
        lkp = new ProxyLookup(super.getLookup(), Lookups.singleton(node));
        
        String interactionType = node.getLookup().lookup(Interaction.class).getInteractionType();
        
        if(interactionType.contains("Phosphorylation")) {
            this.setForeground(Color.BLUE);
            this.setLineColor(Color.BLUE);
            this.setTargetAnchorShape(AnchorShape.TRIANGLE_FILLED);
            this.setStroke(new BasicStroke(2, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND, 1, new float[]{5f,5f}, 0));
        } else if (interactionType.contains("activation")) {
            this.setForeground(Color.GREEN);
            this.setLineColor(Color.GREEN);
            this.setStroke(new BasicStroke(2));
            this.setTargetAnchorShape(AnchorShape.TRIANGLE_FILLED);
        } else if (interactionType.contains("repression")) {
            this.setForeground(Color.RED);
            this.setLineColor(Color.RED);
            this.setStroke(new BasicStroke(2));
        } 

        this.setControlPointCutDistance(3);
        //this.getActions().addAction(ActionFactory.createAddRemoveControlPointAction());
        //this.getActions().addAction(ActionFactory.createFreeMoveControlPointAction());
                
        this.getActions().addAction(((ObjectScene)scene).createSelectAction());
    }

    @Override
    public void notifyStateChanged(ObjectState previousState, ObjectState state) {
        if(state.isSelected()) {
           // scene.setSelection(node);
        }
    }
 
    @Override
    public Lookup getLookup() {
       return lkp;
    }
}
