/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.moduleeditor.widgets;

import java.awt.Color;
import org.netbeans.api.visual.border.BorderFactory;
import org.netbeans.api.visual.widget.Scene;
import org.netbeans.api.visual.widget.Widget;
import uk.ac.ncl.icos.synbad.moduleeditor.MultiTrackLayout;

/**
 *
 * @author owengilfellon
 */
public class InterfaceWidget extends Widget {
    

    public InterfaceWidget(Scene scene) {
        super(scene);
        this.setLayout(new MultiTrackLayout());
        for(int i=0; i<3; i++)
            this.addChild(new InterfaceLayerWidget(scene));
        setBorder(BorderFactory.createRoundedBorder(10, 10, Color.LIGHT_GRAY, Color.gray)); 
    }

}
