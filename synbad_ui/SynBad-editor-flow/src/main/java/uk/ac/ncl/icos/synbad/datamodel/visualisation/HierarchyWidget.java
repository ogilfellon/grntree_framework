/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.datamodel.visualisation;

import org.netbeans.api.visual.action.ActionFactory;
import org.netbeans.api.visual.model.StateModel;
import org.openide.util.Lookup;
import uk.ac.ncl.icos.synbad.editor.core.AHierarchicalGraphScene;
import uk.ac.ncl.icos.synbad.editor.core.EntityWidget;
import uk.ac.ncl.icos.synbad.svpfragment.nodes.VSynBadObjectNode;

/**
 *
 * @author owengilfellon
 */
public class HierarchyWidget extends EntityWidget<HierarchyWidget> implements  StateModel.Listener, Lookup.Provider {

    private final AHierarchicalGraphScene scene;

    public HierarchyWidget(AHierarchicalGraphScene scene, VSynBadObjectNode entity) {
        super(scene, entity);
        this.scene = scene;
//        getActions().addAction(ActionFactory.createAcceptAction(new EntityAcceptProvider()));
    }

    @Override
    public void addWidget(HierarchyWidget widget)
    {
        getEntitiesLayer().addChild(widget);
    }

}
