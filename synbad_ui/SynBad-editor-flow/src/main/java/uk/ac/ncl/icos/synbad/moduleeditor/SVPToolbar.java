/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.moduleeditor;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JSeparator;
import javax.swing.JToggleButton;
import javax.swing.SwingConstants;
import org.openide.awt.DropDownButtonFactory;
import org.openide.util.ImageUtilities;

/**
 *
 * @author owengilfellon
 */
public class SVPToolbar extends JPanel {

    private final List<JToggleButton> buttons = new ArrayList<>();
    //private final List<JMenuItem> buttons = new ArrayList<>();
    private final InteractionsImpl scene;
    
    public SVPToolbar(final InteractionsImpl scene) {
        
        this.scene = scene;

        setLayout(new FlowLayout(FlowLayout.LEFT, 5, 0));
        this.add(new JSeparator(SwingConstants.VERTICAL));
        
        // Add Select Tool
        
        //JPopupMenu selectPopupMenu = new JPopupMenu();
        
        Icon dragIcon = ImageUtilities.image2Icon(ImageUtilities.loadImage("uk/ac/ncl/icos/synbad/images/icon.gif"));
        Icon moveIcon = ImageUtilities.image2Icon(ImageUtilities.loadImage("uk/ac/ncl/icos/synbad/images/icon.gif"));
        
        JToggleButton dragMenuItem = new JToggleButton(dragIcon);
        JMenuItem moveMenuItem = new JMenuItem(moveIcon);
       
        
        /*
        selectPopupMenu.add(dragMenuItem);
        selectPopupMenu.add(moveMenuItem);
        */
        this.add(dragMenuItem);
        
        //JToggleButton selectDropButton = DropDownButtonFactory.createDropDownToggleButton(dragIcon, selectPopupMenu);
        //dragMenuItem.addActionListener(new DropButtonListener(selectDropButton));
        
        dragMenuItem.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                scene.setActiveTool("none");
            }
        });
        /*moveMenuItem.addActionListener(new DropButtonListener(selectDropButton));
        moveMenuItem.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                scene.setActiveTool("none");
            }
        });
        */
        addButton(dragMenuItem);
        
        // Add Prototype Part Tool

        //final JPopupMenu partPopupMenu = new JPopupMenu();

        Icon promoterIcon = ImageUtilities.image2Icon(ImageUtilities.loadImage("uk/ac/ncl/icos/synbad/synbad/svpfragment/promoter16.png"));
        Icon operatorIcon = ImageUtilities.image2Icon(ImageUtilities.loadImage("uk/ac/ncl/icos/synbad/synbad/svpfragment/operator16.png"));
        Icon rbsIcon = ImageUtilities.image2Icon(ImageUtilities.loadImage("uk/ac/ncl/icos/synbad/synbad/svpfragment/rbs16.png"));
        Icon cdsIcon = ImageUtilities.image2Icon(ImageUtilities.loadImage("uk/ac/ncl/icos/synbad/synbad/svpfragment/cds16.png"));
        Icon terminatorIcon = ImageUtilities.image2Icon(ImageUtilities.loadImage("uk/ac/ncl/icos/synbad/synbad/svpfragment/terminator16.png"));

        //JToggleButton partDropButton = DropDownButtonFactory.createDropDownToggleButton(promoterIcon, partPopupMenu);
        JToggleButton promoterMenuItem = new JToggleButton(promoterIcon);
        JToggleButton operatorMenuItem = new JToggleButton(operatorIcon);
        JToggleButton rbsMenuItem = new JToggleButton(rbsIcon);
        JToggleButton cdsMenuItem = new JToggleButton(cdsIcon);
        JToggleButton terminatorMenuItem = new JToggleButton(terminatorIcon);
        
        //promoterMenuItem.addActionListener(new DropButtonListener(partDropButton));
        promoterMenuItem.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                scene.setActiveTool("promoter");
            }
        });
        
        //operatorMenuItem.addActionListener(new DropButtonListener(partDropButton));
        operatorMenuItem.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                scene.setActiveTool("operator");
            }
        });
        
        //rbsMenuItem.addActionListener(new DropButtonListener(partDropButton));
        rbsMenuItem.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                scene.setActiveTool("rbs");
            }
        });
        
        //cdsMenuItem.addActionListener(new DropButtonListener(partDropButton));
        cdsMenuItem.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                scene.setActiveTool("cds");
            }
        });
        
        //terminatorMenuItem.addActionListener(new DropButtonListener(partDropButton));
        terminatorMenuItem.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                scene.setActiveTool("terminator");
            }
        });
        
        /*
        partPopupMenu.add(promoterMenuItem);
        partPopupMenu.add(operatorMenuItem);
        partPopupMenu.add(rbsMenuItem);
        partPopupMenu.add(cdsMenuItem);
        partPopupMenu.add(terminatorMenuItem);*/
        
        this.add(new JSeparator(JSeparator.VERTICAL));
        
        this.add(promoterMenuItem);
        this.add(operatorMenuItem);
        this.add(rbsMenuItem);
        this.add(cdsMenuItem);
        this.add(terminatorMenuItem);

        addButton(promoterMenuItem);
        addButton(operatorMenuItem);
        addButton(rbsMenuItem);
        addButton(cdsMenuItem);
        addButton(terminatorMenuItem);
        
        // Add Protein Interactions Tool
        /*
        JPopupMenu proteinInteractionsPopupMenu = new JPopupMenu();
        Icon phosphorylationIcon = ImageUtilities.image2Icon(ImageUtilities.loadImage("uk/ac/ncl/icos/synbad/images/icon.gif"));
        JMenuItem phosphorylationMenuItem = new JMenuItem(phosphorylationIcon);
        proteinInteractionsPopupMenu.add(phosphorylationMenuItem);
        JToggleButton proteinInteractionsDropButton = DropDownButtonFactory.createDropDownToggleButton(dragIcon, proteinInteractionsPopupMenu);
        phosphorylationMenuItem.addActionListener(new DropButtonListener(proteinInteractionsDropButton));
        
        this.add(new JSeparator(SwingConstants.VERTICAL));
        
        addButton(proteinInteractionsDropButton);
        
        // Add Regulation Interactions Tool
        
        JPopupMenu regulationPopupMenu = new JPopupMenu();
        Icon negativeRegulationIcon = ImageUtilities.image2Icon(ImageUtilities.loadImage("uk/ac/ncl/icos/synbad/images/icon.gif"));
        Icon positiveRegulationIcon = ImageUtilities.image2Icon(ImageUtilities.loadImage("uk/ac/ncl/icos/synbad/images/icon.gif"));
        JMenuItem negativeRegulationMenuItem = new JMenuItem(negativeRegulationIcon);
        JMenuItem positiveRegulationMenuItem = new JMenuItem(positiveRegulationIcon);
        regulationPopupMenu.add(negativeRegulationMenuItem);
        regulationPopupMenu.add(positiveRegulationMenuItem);
        JToggleButton regulationDropButton = DropDownButtonFactory.createDropDownToggleButton(dragIcon, regulationPopupMenu);
        negativeRegulationMenuItem.addActionListener(new DropButtonListener(regulationDropButton));
        positiveRegulationMenuItem.addActionListener(new DropButtonListener(regulationDropButton));
        */  
        //addButton(regulationDropButton);
        
        for(JToggleButton button : buttons) {
            
            button.addItemListener(new ItemListener() {
                @Override
                public void itemStateChanged(ItemEvent e) {
                    if(e.getStateChange() == ItemEvent.SELECTED) {
                        for(JToggleButton button : buttons) {
                            if(!button.equals(e.getSource())) {
                                button.setSelected(false);
                            }
                        }
                    }
                }
            });
        }
    }
    
    private void addButton(JToggleButton button) {
        add(button);
        buttons.add(button);
    }

    
    public void deactivateButtons()
    {
        for(JToggleButton button : buttons) {
            button.setSelected(false);
        }
    }
    
    
    
    class DropButtonListener implements ActionListener {
    
        private final JToggleButton button;

        public DropButtonListener(JToggleButton button) {
            this.button = button;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            if(e.getSource() instanceof JMenuItem) {
                deactivateButtons();
                JMenuItem item = (JMenuItem)e.getSource();
                button.setIcon(item.getIcon());
                button.setSelected(true);
            }
        }
    }
}


