/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.moduleeditor.widgets;

import org.netbeans.api.visual.layout.LayoutFactory;
import org.netbeans.api.visual.widget.Scene;
import org.netbeans.api.visual.widget.Widget;
import uk.ac.ncl.icos.synbad.moduleeditor.MultiTrackLayout;

/**
 *
 * @author owengilfellon
 */
public class TrackWidget extends Widget {
    
    private SequenceWidget sequenceWidget;
    private InterfaceWidget interfaceWidget;

    public TrackWidget(Scene scene) {
        super(scene);
        sequenceWidget = new SequenceWidget(scene);
        interfaceWidget = new InterfaceWidget(scene);
       
        this.setLayout(new MultiTrackLayout());
        addChild(sequenceWidget);
        addChild(interfaceWidget);
    }
}
