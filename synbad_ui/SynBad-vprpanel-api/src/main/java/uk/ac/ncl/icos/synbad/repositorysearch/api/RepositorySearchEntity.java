/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.repositorysearch.api;


/**
 *
 * @author owengilfellon
 */
public interface RepositorySearchEntity {
       
    public void setSearchText(String searchText);
    public void setOrganism(Organism organism);
    public void setPartType(PartType partType);
    public void setPropertyName(PropertyName propertyName);
    public void setPropertyValue(String propertyValue);
    public void setExactPropertyValueMatch(boolean exactPropertyValueMatch);
    public void setIncludeGeneticElements(boolean includeGeneticElements);
    public void setInteractsWith(SVPLeafNode node);
    public void setHasInteractions(InteractionType interactionType);

    public String getSearchText();
    public Organism getOrganism();
    public PartType getPartType(); 
    public PropertyName getPropertyName();
    public String getPropertyValue();
    public SVPLeafNode getInteractsWith();
    public InteractionType getHasInteractions();
    public boolean getExactPropertyValueMatch();
    public boolean getIncludeGeneticElements();
}


