/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.repositorysearch.api;

/**
 *
 * @author owengilfellon
 */

public interface RepositorySearch {
    
    public void setSearchURL(String repositoryURL);
    public void setSearchEntity(RepositorySearchEntity searchEntity);
    public RepositorySearchResults search();
    
}
