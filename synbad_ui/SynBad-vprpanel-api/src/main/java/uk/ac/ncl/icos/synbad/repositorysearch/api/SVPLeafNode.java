/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.repositorysearch.api;

import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.util.lookup.Lookups;
import uk.ac.ncl.icos.datamodel.workspace.objects.SynBadObject;



/**
 *
 * @author owengilfellon
 */
public class SVPLeafNode extends AbstractNode {
        
    public SVPLeafNode(SynBadObject definition) {
        super(Children.LEAF, Lookups.singleton(definition));
    }
    
}
