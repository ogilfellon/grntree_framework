/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.repositorysearch.api;

/**
 *
 * @author owengilfellon
 */
public enum Organism {
    
    BacillusSubtilis168("Bacillus Subtilis 168"),
    EscherichiaColi("Escherichia Coli"),
    None("None");
    
    private String text;
    
    private Organism(String text)
    {
        this.text = text;
    }
    
    public static Organism fromString(String text)
    {
        if (text != null) {
            for (Organism o : Organism.values()) {
                if (text.equalsIgnoreCase(o.text)) {
                  return o;
                }
            }
        }
        
        return Organism.None;
    }

    @Override
    public String toString() {
        return text;
    }
    
    
}
