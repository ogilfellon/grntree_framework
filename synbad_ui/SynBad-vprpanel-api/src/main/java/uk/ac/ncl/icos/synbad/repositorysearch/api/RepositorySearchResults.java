/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.repositorysearch.api;

import java.beans.PropertyChangeListener;
import java.util.List;
import uk.ac.ncl.intbio.virtualparts.entity.Part;

/**
 *
 * @author owengilfellon
 */
public interface RepositorySearchResults {

    public void setSearchName(String name);
    public void addParts(List<Part> part);
    public void addPart(Part part);
    
    public String getSearchName(); 
    public List<Part> getParts();
   
    public void addPropertyChangeListener(PropertyChangeListener listener);
    public void removePropertyChangeListener(PropertyChangeListener listener);
    
}
