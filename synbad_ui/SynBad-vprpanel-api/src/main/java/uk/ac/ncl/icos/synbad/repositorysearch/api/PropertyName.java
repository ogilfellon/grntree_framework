/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.repositorysearch.api;

/**
 *
 * @author owengilfellon
 */
public enum PropertyName {
    
    data_source("data_source"),
    located_in("located_in"),
    participates_in("participates_in"),
    has_function("has_function"),
    type("type"),
    none("none");
    
    private String text;
    
    private PropertyName(String text)
    {
        this.text = text;
    }
    
    public static PropertyName fromString(String text)
    {
        if (text != null) {
            for (PropertyName p : PropertyName.values()) {
                if (text.equalsIgnoreCase(p.text)) {
                  return p;
                }
            }
        }
        
            return PropertyName.none;
        
    }

    @Override
    public String toString() {
        return text;
    }
    
}
