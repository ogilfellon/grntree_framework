/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svpfragment.nodes;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.netbeans.junit.MockServices;
import org.openide.nodes.Node;
import org.openide.nodes.Node.Property;
import org.openide.nodes.Node.PropertySet;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import uk.ac.ncl.icos.datamodel.workspace.api.SBWorkspace;
import uk.ac.ncl.icos.datamodel.workspace.api.SBWorkspaceManager;
import uk.ac.ncl.icos.datamodel.workspace.api.rdf.DefaultWorkspaceManager;
import uk.ac.ncl.icos.datamodel.workspace.sbol.Definition;
import uk.ac.ncl.icos.datamodel.workspace.sbol.ModuleDefinition;
import uk.ac.ncl.icos.datamodel.workspace.sbol.Pointer;
import uk.ac.ncl.icos.datamodel.workspace.sbol.SbolFactory;
import uk.ac.ncl.icos.synbad.datadefinition.UriHelper;
import uk.ac.ncl.icos.view.api.SBModel;
import uk.ac.ncl.icos.view.impl.DefaultModel;

/**
 *
 * @author owengilfellon
 */
public class EntityNodeTest {

    private final SBModel model;
     private final URI workspaceURI = UriHelper.synbad.namespacedUri("testWorkspace");
    private final SBWorkspaceManager m;
    private SBWorkspace ws;
    
    public EntityNodeTest() {
        MockServices.setServices(DefaultWorkspaceManager.class,
            SbolFactory.ComponentDefinitionFactory.class,
            SbolFactory.ModuleDefinitionFactory.class,
            SbolFactory.FunctionalComponentFactory.class,
            SbolFactory.InteractionFactory.class,
            SbolFactory.ModuleFactory.class,
            SbolFactory.ParticipationFactory.class,
            SbolFactory.ComponentFactory.class,
            SbolFactory.CutFactory.class,
            SbolFactory.MapsToFactory.class,
            SbolFactory.RangeFactory.class,
            SbolFactory.SequenceAnnotationFactory.class,
            SbolFactory.SequenceConstraintFactory.class,
            SbolFactory.SequenceFactory.class);
        this.m = Lookup.getDefault().lookup(SBWorkspaceManager.class);
        this.model = setupWorkspace();
        
    }
    
    public static void main(String[] args) {
        try {
            EntityNodeTest t = new EntityNodeTest();
            t.testModelNodes();
            t.testDefinitionNodes();
            t.testPointerNodes();
            
            //t.TestEntityNode();
        } catch (IllegalAccessException | InvocationTargetException ex) {
            Exceptions.printStackTrace(ex);
        }
    }
    
    private SBModel setupWorkspace() {
        
        ws = m.getWorkspace(workspaceURI);

        Set<String> sources = new HashSet<>(Arrays.asList(
            "ExampleSystem.xml"
        ));

        sources.stream().forEach((source) -> {
            ws.addRdf(new File(source));
        });
        
        return new DefaultModel(ws.getObject(URI.create("http://www.synbad.org/System/1.0"), ModuleDefinition.class), ws);
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    

    public void testModelNodes() {
        System.out.println("==================== Model ======================");
        SBModelNode n = new SBModelNode(model);
        System.out.println(n);
        for(Enumeration<Node> e  = n.getChildren().nodes(); e.hasMoreElements();)  {
                System.out.println("\t + " + e.nextElement());
            }
    }
    

    public void testDefinitionNodes() throws IllegalAccessException, InvocationTargetException {
        for(Definition definition : model.getWorkspace().getObjects(Definition.class)) {
            SBDefinitionNode n = new SBDefinitionNode(definition);
            System.out.println(n.getName());
            for(Enumeration<Node> e  = n.getChildren().nodes(); e.hasMoreElements();)  {
                System.out.println("\t + " + e.nextElement());
            }
            for(PropertySet p : n.getPropertySets()) {
                for(Property property : p.getProperties()) {
                    System.out.println("\t - " + property.getValue() + " : " + property.getDisplayName() + " : " + property.getShortDescription());
                }
            }
            //assert(n.getName().equals(definition.getDisplayId()));
        }
    }
    

    public void testPointerNodes() throws IllegalAccessException, InvocationTargetException {
        for(Pointer pointer : model.getWorkspace().getObjects(Pointer.class)) {
            SBPointerNode n = new SBPointerNode(pointer);
            System.out.println(n.getName());
            for(PropertySet p : n.getPropertySets()) {
                for(Property property : p.getProperties()) {
                    System.out.println(property.getValue() + " : " + property.getDisplayName() + " : " + property.getShortDescription());
                }
            }
            //assert(n.getName().equals(pointer.getDisplayId()));
        }
    }

  /*
    @Test
    public void TestEntityNode() {
        JFrame frame = new JFrame("EntityNodes");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(new GridBagLayout());
        
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.FIRST_LINE_START;
        c.fill = GridBagConstraints.BOTH;
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 1;
        c.weighty = 1;

        TopComponent tc = new ExplorerTopComponent();
        
        frame.setMinimumSize(new Dimension(800, 600));
        frame.getContentPane().add(tc, c);
        tc.setVisible(true);
        frame.pack();
        frame.setVisible(true);
    }
    
    public final class ExplorerTopComponent extends TopComponent implements ExplorerManager.Provider {

        private transient final ExplorerManager manager;
        private final JScrollPane scrollPane;
        
        public ExplorerTopComponent() {
            manager = new ExplorerManager();
            manager.setRootContext(new AbstractNode(Children.LEAF));
            associateLookup(ExplorerUtils.createLookup(manager, getActionMap()));
            scrollPane = new BeanTreeView();
            setLayout(new BorderLayout());
            add(scrollPane, BorderLayout.CENTER);
        }

        @Override
        public ExplorerManager getExplorerManager() {
            return manager;
        }
    }*/

}
