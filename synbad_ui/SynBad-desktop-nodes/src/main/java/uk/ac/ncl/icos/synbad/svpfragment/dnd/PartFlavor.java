/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svpfragment.dnd;

import java.awt.datatransfer.DataFlavor;
import uk.ac.ncl.icos.synbad.svpfragment.nodes.VprPartNode;

/**
 *
 * @author owengilfellon
 */
public class PartFlavor extends DataFlavor {
    
    public static final DataFlavor PART_FLAVOR = new PartFlavor();

    public PartFlavor() {
         super(VprPartNode.class, "SVPData");
    }
    
}
