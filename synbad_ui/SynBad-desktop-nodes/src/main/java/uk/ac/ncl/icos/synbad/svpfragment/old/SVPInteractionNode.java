/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svpfragment.old;

import javax.swing.Action;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.PropertySupport;
import org.openide.nodes.Sheet;
import org.openide.util.Exceptions;
import org.openide.util.lookup.Lookups;
import org.openide.util.lookup.ProxyLookup;
import uk.ac.ncl.intbio.virtualparts.entity.Interaction;

/**
 *
 * @author owengilfellon
 */
public class SVPInteractionNode extends AbstractNode {
    
    private final Interaction interaction;
    
    public SVPInteractionNode(Interaction interaction) {     
        super(Children.LEAF, new ProxyLookup(Lookups.fixed(interaction)));
        setName(interaction.getName());
        setDisplayName(interaction.getName());
        this.interaction = interaction;
    }
 
    @Override
    protected Sheet createSheet() {

        Sheet sheet = Sheet.createDefault();
        Sheet.Set set = Sheet.createPropertiesSet();
        
        try {
            Property nameProp = new PropertySupport.Reflection(interaction, String.class, "getName", null);
            nameProp.setName("name");
            nameProp.setDisplayName("Name");
            nameProp.setValue("suppressCustomEditor", Boolean.TRUE);
            set.put(nameProp);

            Property interactionType = new PropertySupport.Reflection(interaction, String.class, "getInteractionType", null);
            interactionType.setName("interactionType");
            interactionType.setDisplayName("Type");
            interactionType.setValue("suppressCustomEditor", Boolean.TRUE);
            set.put(interactionType);
          
            Property freeTextProp = new PropertySupport.Reflection(interaction, String.class, "getFreeTextMath", null);
            freeTextProp.setName("freetext");
            freeTextProp.setDisplayName("Free Text Math");
            freeTextProp.setValue("suppressCustomEditor", Boolean.TRUE);
            set.put(freeTextProp);

        } catch (NoSuchMethodException ex) {
            Exceptions.printStackTrace(ex);
        }
        

        sheet.put(set);
        return sheet;

    }
    
    @Override
    public Action[] getActions(boolean context) {
        return new Action[] {};
    }
    
    @Override
    public boolean canDestroy() {
        return true;
    }   
}