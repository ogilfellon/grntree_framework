/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svpfragment.nodes;

import java.awt.event.ActionEvent;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.Action;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.lookup.Lookups;
import uk.ac.ncl.icos.synbad.event.SBSubscriber;
import uk.ac.ncl.icos.datamodel.workspace.sbol.Definition;
import uk.ac.ncl.icos.datamodel.workspace.sbol.NestedObject;
import uk.ac.ncl.icos.synbad.event.SBEvent;

public class SBDefinitionNode extends SBTopLevelNode  {

    public SBDefinitionNode(Definition definition) {
        super(Children.LEAF, Lookups.fixed(definition));
    }
    
    
    @Override
    public Action getPreferredAction() {
        return new AbstractAction("Open view") {
            @Override
            public void actionPerformed(ActionEvent e) {   
                /*
                IEntity definition = getLookup().lookup(IEntity.class);
                VModel model = new VModelImpl(definition,  definition.getWorkspace());
                EntityViewProvider provider = Lookup.getDefault().lookup(EntityViewProvider.class);                   
                TopComponent window = provider.getView(model.getDefaultView());
                window.open();
                window.requestActive();*/
            }
        };
    }   

    public static class DefinitionChildFactory extends ChildFactory<NestedObject> implements SBSubscriber {

        private final Definition definition;
       

        public DefinitionChildFactory(Definition definition) {
            this.definition = definition;

//            workspace.getDispatcher().registerHandler(WorkspaceEvent.InstanceEvent.class , this);
 //           workspace.getWorkspace().getDispatcher().addFilter(entity.getIdentity(), this);
        }

        @Override
        protected synchronized boolean createKeys(List<NestedObject> toPopulate) {
            return toPopulate.addAll(definition.getNestedObjects());
        }

        @Override
        protected Node createNodeForKey(NestedObject key) {
            return new SBNestedNode(key);
        }
        
        @Override
        public void onEvent(SBEvent e) {
            //refresh(true);
        }

    }
}
