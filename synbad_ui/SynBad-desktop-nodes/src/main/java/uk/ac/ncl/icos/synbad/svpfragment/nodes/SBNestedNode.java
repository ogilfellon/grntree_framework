/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svpfragment.nodes;

import org.openide.nodes.Children;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;
import uk.ac.ncl.icos.datamodel.workspace.sbol.NestedObject;

public class SBNestedNode extends SBEntityNode {

    public SBNestedNode(Children children, Lookup lookup) {
        super(children, lookup);
    }
    
    public SBNestedNode(NestedObject object) {
        super(Children.LEAF, Lookups.singleton(object));
    }

}
