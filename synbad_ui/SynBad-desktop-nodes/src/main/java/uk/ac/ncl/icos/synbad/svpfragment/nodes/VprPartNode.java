/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svpfragment.nodes;

import java.awt.Image;
import java.awt.datatransfer.Transferable;
import java.awt.dnd.DnDConstants;
import java.io.IOException;
import java.util.List;
import javax.swing.Action;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.NodeTransfer;
import org.openide.nodes.PropertySupport;
import org.openide.nodes.Sheet;
import org.openide.util.Exceptions;
import org.openide.util.Utilities;
import org.openide.util.datatransfer.PasteType;
import org.openide.util.lookup.Lookups;
import org.openide.util.lookup.ProxyLookup;
import uk.ac.ncl.icos.synbad.datadefinition.IDataDefinitionManager;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentRole;

import uk.ac.ncl.icos.synbad.svpfragment.dnd.PartTransferable;
import uk.ac.ncl.intbio.virtualparts.entity.Part;

/**
 *
 * @author owengilfellon
 */
public class VprPartNode extends AbstractNode {
    
    IDataDefinitionManager manager = new IDataDefinitionManager.DefaultDataDefinitionManager();

    public VprPartNode(Part part) {     
        super(Children.LEAF, new ProxyLookup(Lookups.singleton(part)));
        String s = createName();
        setName(s);
        setDisplayName(s);
        super.setIconBaseWithExtension(getIconBase());
    }

    private String getIconBase()
    {
        ComponentRole role = manager.getDefinition(ComponentRole.class, getLookup().lookup(Part.class).getType());
        if(role!=null) {
            switch (role) {
                case Promoter:  return "uk/ac/ncl/icos/synbad/synbad/svpfragment/promoter16.png";
                case RBS:  return "uk/ac/ncl/icos/synbad/synbad/svpfragment/rbs16.png";
                case Operator: return "uk/ac/ncl/icos/synbad/synbad/svpfragment/operator16.png";
                case CDS: return "uk/ac/ncl/icos/synbad/synbad/svpfragment/cds16.png";
                case Terminator: return "uk/ac/ncl/icos/synbad/synbad/svpfragment/terminator16.png";
                case Shim: return "uk/ac/ncl/icos/synbad/synbad/svpfragment/shim16.png";
                case Generic: return "uk/ac/ncl/icos/synbad/synbad/svpfragment/userdefined16.png";
            }
        }
        return null;
    }
    
    private String createName()
    {
        Part part = getLookup().lookup(Part.class);
        String displayName;
        
        if(part.getDisplayName() != null && !(part.getDisplayName().equals(""))) {
            displayName = part.getDisplayName();
        } else {
            displayName = part.getName();
        }

        return displayName;
    }

    @Override
    public Image getIcon(int type) {
        return super.getIcon(type);
    }

    @Override
    public String getHtmlDisplayName() {
        return getName();
        /*+ ":<font color='0000ff'>" + 
               ((LeafNode)getLookup().lookup(GRNTreeNode.class)).getSVP().getType()
                + "</font>";*/
    }
    
    @Override
    protected Sheet createSheet() {

        Sheet sheet = Sheet.createDefault();
        Sheet.Set set = Sheet.createPropertiesSet();
        
        try {
            Part part = getLookup().lookup(Part.class);

            Property typeProp = new PropertySupport.Reflection(part, String.class, "getType", null);
            typeProp.setName("type");
            typeProp.setDisplayName("Type");
            typeProp.setValue("suppressCustomEditor", Boolean.TRUE);
            set.put(typeProp);
            /*
            Property metaTypeProp = new PropertySupport.Reflection(node.getSVP(), String.class, "getMetaType", null);
            metaTypeProp.setName("metaType");
            metaTypeProp.setDisplayName("Meta Type");
            metaTypeProp.setValue("suppressCustomEditor", Boolean.TRUE);
            set.put(metaTypeProp);
            */
            Property designMethodProp = new PropertySupport.Reflection(part, String.class, "getDesignMethod", null);
            designMethodProp.setName("designMethod");
            designMethodProp.setDisplayName("Design Method");
            designMethodProp.setValue("suppressCustomEditor", Boolean.TRUE);
            set.put(designMethodProp);
            
            Property organismProp = new PropertySupport.Reflection(part, String.class, "getOrganism", null);
            organismProp.setName("organism");
            organismProp.setDisplayName("Organism");
            organismProp.setValue("suppressCustomEditor", Boolean.TRUE);
            set.put(organismProp);
            /*
            Property statusProp = new PropertySupport.Reflection(node.getSVP(), String.class, "getStatus", null);
            statusProp.setName("status");
            statusProp.setDisplayName("Status");
            statusProp.setValue("suppressCustomEditor", Boolean.TRUE);
            set.put(statusProp);
            */
            sheet.put(set);
            
        } catch (NoSuchMethodException ex) {
            Exceptions.printStackTrace(ex);
        }
        
        return sheet;
    }

    @Override
    public PasteType getDropType(Transferable t, final int action, int index) {
        final Node dropNode = NodeTransfer.node( t, DnDConstants.ACTION_COPY_OR_MOVE+NodeTransfer.CLIPBOARD_CUT );
        
        if( null != dropNode ) {
            
            final Part part = dropNode.getLookup().lookup(Part.class);
            
            if(  null != part ){
                
                return new PasteType() {
                    
                    @Override
                    public Transferable paste() throws IOException {
                        getChildren().add(new Node[] { new VprPartNode(part) } );
                        if( (action & DnDConstants.ACTION_MOVE) != 0 ) {
                            dropNode.getParentNode().getChildren().remove( new Node[] {dropNode} );
                        }
                        return null;
                    }
                };
            }
        }
        return null;
    }
    
    @Override
    public Transferable drag() throws IOException {
        return new PartTransferable(new VprPartNode(getLookup().lookup(Part.class)));
    }
    
    @Override
    public Cookie getCookie(Class clazz) {
        Children ch = getChildren();
        
        if (clazz.isInstance(ch)) {
            return (Cookie) ch;
        }
        
        return super.getCookie(clazz);
    }
    
    @Override
    protected void createPasteTypes(Transferable t, List s) {
        super.createPasteTypes(t, s);
        PasteType paste = getDropType( t, DnDConstants.ACTION_COPY, -1 );
        if( null != paste )
            s.add( paste );
    }
    
    @Override
    public Action[] getActions(boolean context) {
        
        List<? extends Action> actions = Utilities.actionsForPath("Actions/CustomCategory");
        return actions.toArray( new Action[actions.size()] );
        
    }
    
    @Override
    public boolean canDestroy() {
        return true;
    }   
}
