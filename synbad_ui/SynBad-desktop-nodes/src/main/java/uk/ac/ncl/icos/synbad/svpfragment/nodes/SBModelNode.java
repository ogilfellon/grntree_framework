/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svpfragment.nodes;

import java.awt.Image;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import javax.swing.Action;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.PropertySupport;
import org.openide.nodes.Sheet;
import org.openide.util.Utilities;
import org.openide.util.lookup.Lookups;
import uk.ac.ncl.icos.model.SBModelListener;
import uk.ac.ncl.icos.view.api.SBModel;
import uk.ac.ncl.icos.view.api.SBView;

/**
 *
 * @author owengilfellon
 */
public class SBModelNode extends AbstractNode {

    
    
    public SBModelNode(SBModel model) {
        super(Children.create(new ViewFactory(model), true), Lookups.singleton(model));        
        //super(Children.LEAF, Lookups.singleton(model));        
        setName(model.getModelRoot().getDisplayId() + " Model");
        super.setIconBaseWithExtension(getIconBase());
    }

    private String getIconBase()
    {
        return "uk/ac/ncl/icos/synbad/project/nodeicons/userdefined16.png";
    }

    @Override
    public Image getIcon(int type) {
        return super.getIcon(type);
    }

    @Override
    public String getHtmlDisplayName() {
        return getName();
        /*+ ":<font color='0000ff'>" + 
               ((LeafNode)getLookup().lookup(GRNTreeNode.class)).getSVP().getType()
                + "</font>";*/
    }
    
    /*
    @Override
    protected Sheet createSheet() {
        Sheet sheet = Sheet.createDefault();
        Sheet.Set set = Sheet.createPropertiesSet();
        final SBView view = getLookup().lookup(SBView.class);
        set.put(new PropertySupport.ReadOnly<Integer>("id", Integer.class, "ID", "View ID") {
            
            @Override
            public Integer getValue() throws IllegalAccessException, InvocationTargetException {
                return new Integer(view.getViewId());
            }
        });
        sheet.put(set);
        return sheet;
    }

    
    @Override
    public Node.Cookie getCookie(Class clazz) {
        Children ch = getChildren();
        
        if (clazz.isInstance(ch)) {
            return (Node.Cookie) ch;
        }
        
        return super.getCookie(clazz);
    }
    
    @Override
    public Action[] getActions(boolean context) {
        
        List<? extends Action> actions = Utilities.actionsForPath("Actions/ModuleNode");
        return actions.toArray( new Action[actions.size()] );
        
    }
    
    @Override
    public boolean canDestroy() {
        return true;
    }   */


    public static class ViewFactory extends ChildFactory<SBView> implements SBModelListener {

        private final SBModel model;
        
        public ViewFactory(SBModel model) {
            this.model = model;
            this.model.addListener(this);
        }

        @Override
        protected synchronized boolean createKeys(List<SBView> toPopulate) {
             return toPopulate.addAll(model.getViews());
        }

        @Override
        protected Node createNodeForKey(SBView key) {
            return new SBViewNode(key);
        }

        @Override
        public void onCreateView(SBModel model, SBView view) {
            refresh(true);
        }

        @Override
        public void onRemoveView(SBModel model, SBView view) {
            refresh(true);
        }

        @Override
        public void onSetCurrentView(SBModel model, SBView currentView) {}

        
    }
}
