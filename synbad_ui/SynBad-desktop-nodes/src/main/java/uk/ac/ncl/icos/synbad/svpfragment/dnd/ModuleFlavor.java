/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svpfragment.dnd;

import java.awt.datatransfer.DataFlavor;
import uk.ac.ncl.icos.synbad.svpfragment.nodes.SBDefinitionNode;

/**
 *
 * @author owengilfellon
 */
public class ModuleFlavor extends DataFlavor {
    
    public static final DataFlavor MODULE_FLAVOR = new ModuleFlavor();

    public ModuleFlavor() {
         super(SBDefinitionNode.class, "ModuleData");
    }
    
}
