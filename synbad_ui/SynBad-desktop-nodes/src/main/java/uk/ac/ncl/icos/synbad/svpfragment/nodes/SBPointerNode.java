/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svpfragment.nodes;

import org.openide.nodes.Children;
import org.openide.util.lookup.Lookups;

import uk.ac.ncl.icos.datamodel.workspace.sbol.Pointer;

public class SBPointerNode extends SBNestedNode  {

    public SBPointerNode(Pointer instance) {
        super(Children.LEAF, Lookups.fixed(instance));  
    }
   

}
