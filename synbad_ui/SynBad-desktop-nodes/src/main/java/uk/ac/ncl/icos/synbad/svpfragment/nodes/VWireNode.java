/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svpfragment.nodes;

import java.awt.Image;
import org.openide.nodes.Children;
import org.openide.util.lookup.Lookups;
import uk.ac.ncl.icos.synbad.graph.api.SBEdge;
import uk.ac.ncl.icos.view.api.SBView;

/**
 *
 * @author owengilfellon
 */
public class VWireNode extends PropertiedNode {

    private final SBView view;
    
    public VWireNode(SBEdge edge) {
        super(Children.LEAF, Lookups.fixed(edge, edge.getEdge()));
        setName(edge.toString());
        super.setIconBaseWithExtension(getIconBase());
        view = null;
    }
    
    public VWireNode(SBEdge instance, SBView view) {
        super(Children.LEAF, Lookups.fixed(instance, view, instance));
        setName(instance.toString());
        super.setIconBaseWithExtension(getIconBase());
        this.view = view;
    }

    private String getIconBase()
    {
        return "uk/ac/ncl/icos/synbad/project/nodeicons/userdefined16.png";
    }

    @Override
    public Image getIcon(int type) {
        return super.getIcon(type);
    }

    @Override
    public String getHtmlDisplayName() {
        SBEdge edge = getLookup().lookup(SBEdge.class);
        String signal = "<font color='0000ff'><i>Wire: </i></font>" ;
        return signal + getName();
    }

}
