/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svpfragment.nodes;

import java.awt.Image;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;
import uk.ac.ncl.icos.datamodel.workspace.impl.PortRecord;
import uk.ac.ncl.icos.datamodel.workspace.objects.IPort;
import uk.ac.ncl.icos.synbad.datadefinition.IDataDefinitionManager;

/**
 *
 * @author owengilfellon
 */
public class PortNode extends PropertiedNode  {
    
    private static final IDataDefinitionManager manager = Lookup.getDefault().lookup(IDataDefinitionManager.class);

    public PortNode(PortRecord instance) {
        super(Children.LEAF, Lookups.singleton(instance));
    }

    @Override
    public Image getIcon(int type) {
        return super.getIcon(type);
    }

    @Override
    public String getHtmlDisplayName() {
         /*IPort port = getLookup().lookup(PortRecord.class).getValue();
        String signal = port.isProxy() ? "<font color='0000ff'><i>ProxyPort: </i></font>" : "<font color='0000ff'><i>Port: </i></font>";
        String direction = "<font color='000000'>" + port.getPortDirection() + ":</font>";
       String type = "<font color='000000'>" + manager.getDefinition(PortType.class, port.getAnnotations(SynBadTerms.Port.portType).iterator().next().getURIValue().toASCIIString()) + "</font>";

        String state = port.hasAnnotation(SynBadTerms.Port.portState) 
                ? ":<font color='000000'>" + manager.getDefinition(PortState.class, port.getAnnotations(SynBadTerms.Port.portState).iterator().next().getURIValue().toASCIIString())+ "</font>"
                : "";
       
        return signal + direction;// + type + state; */
        return super.toString();
    }
 
    @Override
    public Node.Cookie getCookie(Class clazz) {
        Children ch = getChildren();
        
        if (clazz.isInstance(ch)) {
            return (Node.Cookie) ch;
        }
        
        return super.getCookie(clazz);
    }
  
    
    @Override
    public boolean canDestroy() {
        return true;
    }   

}
