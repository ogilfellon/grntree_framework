/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svpfragment.dnd;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import uk.ac.ncl.icos.synbad.svpfragment.nodes.SBEntityNode;

/**
 *
 * @author owengilfellon
 */
public class EntityNodeTransferable implements Transferable {

    private final SBEntityNode entityNode;
    
    public EntityNodeTransferable(SBEntityNode node) {
        this.entityNode = node;
    }

    @Override
    public DataFlavor[] getTransferDataFlavors() {
        DataFlavor[] flavors = { ModuleFlavor.MODULE_FLAVOR };
        return flavors;
    }

    @Override
    public boolean isDataFlavorSupported(DataFlavor flavor) {
        return flavor == ModuleFlavor.MODULE_FLAVOR;
    }

    @Override
    public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException {
        return entityNode;
    }
    
}
