/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svpfragment.dnd;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import uk.ac.ncl.icos.synbad.svpfragment.nodes.VprPartNode;

/**
 *
 * @author owengilfellon
 */
public class PartTransferable implements Transferable {

    VprPartNode part = null;
    
    public PartTransferable(VprPartNode part) {
        this.part = part;
    }

    @Override
    public DataFlavor[] getTransferDataFlavors() {
        DataFlavor[] flavors = { PartFlavor.PART_FLAVOR };
        return flavors;
    }

    @Override
    public boolean isDataFlavorSupported(DataFlavor flavor) {
        return flavor == PartFlavor.PART_FLAVOR;
    }

    @Override
    public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException {
        return part;
    }
    
}
