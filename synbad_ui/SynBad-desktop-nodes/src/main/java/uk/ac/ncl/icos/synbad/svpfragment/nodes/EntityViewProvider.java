package uk.ac.ncl.icos.synbad.svpfragment.nodes;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



import org.openide.windows.TopComponent;
import uk.ac.ncl.icos.view.api.SBView;


/**
 *
 * @author owengilfellon
 */
public interface EntityViewProvider {
    
    public TopComponent getView(SBView view);
    public String getName();
    
}
