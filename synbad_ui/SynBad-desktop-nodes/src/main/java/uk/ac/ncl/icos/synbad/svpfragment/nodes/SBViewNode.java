/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svpfragment.nodes;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.Action;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.PropertySupport;
import org.openide.nodes.Sheet;
import org.openide.util.Lookup;
import org.openide.util.Utilities;
import org.openide.util.lookup.Lookups;
import org.openide.windows.TopComponent;
import uk.ac.ncl.icos.view.api.SBView;

/**
 *
 * @author owengilfellon
 */
public class SBViewNode extends AbstractNode {

    
    public SBViewNode(SBView view) {
        super(Children.LEAF, Lookups.singleton(view));        
        setName(view.getRoot().getData().getDisplayId() + " View");
        super.setIconBaseWithExtension(getIconBase());
    }

    private String getIconBase()
    {
        return "uk/ac/ncl/icos/synbad/project/nodeicons/userdefined16.png";
    }

    @Override
    public Image getIcon(int type) {
        return super.getIcon(type);
    }

    @Override
    public String getHtmlDisplayName() {
        return getName();
    }
    /*
    @Override
    protected Sheet createSheet() {
        Sheet sheet = Sheet.createDefault();
        Sheet.Set set = Sheet.createPropertiesSet();
        final SBView view = getLookup().lookup(SBView.class);
        set.put(new PropertySupport.ReadOnly<Integer>("id", Integer.class, "ID", "View ID") {
            
            @Override
            public Integer getValue() throws IllegalAccessException, InvocationTargetException {
                return new Integer(view.getViewId());
            }
        });
        sheet.put(set);
        return sheet;
    }

    
    @Override
    public Node.Cookie getCookie(Class clazz) {
        Children ch = getChildren();
        
        if (clazz.isInstance(ch)) {
            return (Node.Cookie) ch;
        }
        
        return super.getCookie(clazz);
    }
    
   
    @Override
    public Action[] getActions(boolean context) {
        List<? extends Action> actions = Utilities.actionsForPath("Actions/ModuleNode");
        return actions.toArray( new Action[actions.size()] );
    }
    
    @Override
    public boolean canDestroy() {
        return true;
    }   



    @Override
    public Action getPreferredAction() {
        return new AbstractAction("Open view") {
            @Override
            public void actionPerformed(ActionEvent e) {    
                SBView view = getLookup().lookup(SBView.class);
                EntityViewProvider provider = Lookup.getDefault().lookup(EntityViewProvider.class);                   
                TopComponent window = provider.getView(view);
                window.open();
                window.requestActive();
            }
        };
    } */  
}
