/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svpfragment.dnd;

import java.awt.datatransfer.DataFlavor;
import uk.ac.ncl.icos.synbad.svpfragment.nodes.VprPartNode;

/**
 *
 * @author owengilfellon
 */
public class SVPModuleFlavor extends DataFlavor {
    
    public static final DataFlavor SVPMODULE_FLAVOR = new SVPModuleFlavor();

    public SVPModuleFlavor() {
         super(SVPModuleFlavor.class, "SVPModuleData");
    }
    
}
