/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svpfragment.properties;

import java.lang.reflect.InvocationTargetException;
import org.openide.nodes.PropertySupport;
import uk.ac.ncl.icos.datamodel.workspace.impl.PortRecord;

/**
 *
 * @author owengilfellon
 */
public class PortProperty extends PropertySupport.ReadOnly<PortRecord> { 

    private PortRecord signal;
    
    public PortProperty(PortRecord signal, String name, String displayName, String shortDescription) {
        super(name, PortRecord.class, displayName, shortDescription);
        this.signal = signal;
    }

    @Override
    public PortRecord getValue() throws IllegalAccessException, InvocationTargetException {
        return signal;
    }
}
