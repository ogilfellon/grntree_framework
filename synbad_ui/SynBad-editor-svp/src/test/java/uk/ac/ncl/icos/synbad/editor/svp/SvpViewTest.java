/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.editor.svp;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.netbeans.junit.MockServices;
import org.sbolstandard.core2.SBOLValidationException;
import uk.ac.ncl.icos.synbad.datadefinition.IDataDefinitionManager;
import uk.ac.ncl.icos.synbad.datadefinition.IDataDefinitionProvider;
import uk.ac.ncl.icos.synbad.signalgraph.impl.SbolTypeProvider;
import uk.ac.ncl.icos.model.VModel;
import uk.ac.ncl.icos.workspace.factories.ExampleFactory;

/**
 *
 * @author owengilfellon
 */
public class SvpViewTest {
    
    
    private final VModel model;

    public static void main(String[] args) throws SBOLValidationException {
        SvpViewTest test = new SvpViewTest();
        test.testGraphVisualisation();
    }

    public SvpViewTest() throws SBOLValidationException {
        MockServices.setServices(
                IDataDefinitionManager.DefaultDataDefinitionManager.class,
                IDataDefinitionProvider.DefaultDefinitionProvider.class,
                SbolTypeProvider.class);
        model = ExampleFactory.setupTemplateModel();
    }
  
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }


    @Test
    public void testGraphVisualisation() {
        JFrame frame = new JFrame("Visualisation");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(new GridBagLayout());
        
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.FIRST_LINE_START;
        c.fill = GridBagConstraints.BOTH;
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 1;
        c.weighty = 1;

        final SvpScene scene = new SvpScene(model.getCurrentView());
        JScrollPane pane = new JScrollPane();
        pane.setViewportView(scene.createView());
        frame.setMinimumSize(new Dimension(800, 600));
        frame.getContentPane().add(pane, c);
        
        pane.setVisible(true);
        frame.pack();
        frame.setVisible(true);
    }
}
