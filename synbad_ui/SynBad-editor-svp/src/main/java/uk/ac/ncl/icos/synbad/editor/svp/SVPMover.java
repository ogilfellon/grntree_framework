/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.editor.svp;

import java.awt.Point;
import java.util.Collection;
import java.util.HashSet;
import org.netbeans.api.visual.action.MoveProvider;
import org.netbeans.api.visual.widget.Widget;
import uk.ac.ncl.icos.synbad.editor.core.AHierarchicalGraphScene;


/**
 *
 * @author owengilfellon
 */
public class SVPMover implements MoveProvider {

    private Point point = null;
    private AHierarchicalGraphScene scene;

    public SVPMover(AHierarchicalGraphScene scene) {
        this.scene = scene;
    }

    @Override
    public void movementStarted(Widget widget) {

        if(widget instanceof SvpWidget) {
            SvpWidget w = (SvpWidget) widget;
            
            SvpModuleWidget parent = (SvpModuleWidget)w.getParentWidget()
                    .getParentWidget().getParentWidget()
                    .getParentWidget().getParentWidget()
                    .getParentWidget().getParentWidget();
            parent.removeWidget(w);
   
            scene.getMovementLayer().addChild(widget);
            widget.setPreferredLocation(point);
        }
    }

    @Override
    public void movementFinished(Widget widget) {
        
        scene.getMovementLayer().removeChild(widget);
        Collection<SvpModuleWidget> newParents = new HashSet<>();
        Point newPos = null;

        for(Object node : scene.getNodes()) {

            Widget w =  scene.findWidget(node);

            if(w != widget && w.isHitAt(w.convertSceneToLocal(point))) {
                newPos = w.convertSceneToLocal(point);
                SvpModuleWidget found = scene.findWidget(node) instanceof SvpModuleWidget
                        ? (SvpModuleWidget)scene.findWidget(node)
                        : null;
                if(found!=null)
                    newParents.add(found);

            }
        }

        if(!newParents.isEmpty()) {
            
            newParents.iterator().next().addWidget((SvpWidget)widget);
            
            return;
        }

        scene.getEntitiesLayer().addChild(widget);
    }

    @Override
    public Point getOriginalLocation(Widget widget) {
        point = widget.getParentWidget().convertLocalToScene(widget.getLocation());
        return point;
    }

    @Override
    public void setNewLocation(Widget widget, Point location) {
        point = location;
        widget.setPreferredLocation(location);
    }
        
    
}
