/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.moduleeditor.widgets.svpmodulewidget;

import java.awt.Point;
import javax.swing.JMenu;
import javax.swing.JPopupMenu;
import org.netbeans.api.visual.action.PopupMenuProvider;
import org.netbeans.api.visual.widget.Widget;
import uk.ac.ncl.icos.datamodel.workspace.impl.EntityInstance;

/**
 *
 * @author owengilfellon
 */
public class SVPModulePopUpProvider implements PopupMenuProvider{

    EntityInstance module;

    public SVPModulePopUpProvider(EntityInstance module) {
        this.module = module;
    }
    
    @Override
    public JPopupMenu getPopupMenu(Widget widget, Point localLocation) {

        JPopupMenu menu = new JPopupMenu("SVP Menu");

        
        JMenu importSignal = new JMenu("Import Signal");
        JMenu exportSignal = new JMenu("Export Signal");

        // ......

        
        if(exportSignal.getItemCount() > 0)
            menu.add(exportSignal);
        if(importSignal.getItemCount() > 0)
            menu.add(importSignal);
      
        return menu;
    }
    
}
