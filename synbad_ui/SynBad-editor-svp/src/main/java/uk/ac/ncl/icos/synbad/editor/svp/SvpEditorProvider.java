/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.editor.svp;

import org.openide.util.lookup.ServiceProvider;
import org.openide.windows.TopComponent;
import uk.ac.ncl.icos.synbad.svpfragment.nodes.EntityViewProvider;
import uk.ac.ncl.icos.view.api.View;

/**
 *
 * @author owengilfellon
 */
@ServiceProvider(service=EntityViewProvider.class)
public class SvpEditorProvider implements EntityViewProvider {

    @Override
    public TopComponent getView(View view) {
        return new SvpView(view);
        //return new NewHGraphView(moduleNode);
    }

    @Override
    public String getName() {
        return "Svp View";
    }
    
    
    
}
