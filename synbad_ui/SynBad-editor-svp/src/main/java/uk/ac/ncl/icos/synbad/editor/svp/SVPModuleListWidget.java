/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.editor.svp;

import java.awt.Dimension;
import java.util.List;
import javax.swing.JPanel;
import org.netbeans.api.visual.action.ActionFactory;
import org.netbeans.api.visual.layout.LayoutFactory;
import org.netbeans.api.visual.model.ObjectState;
import org.netbeans.api.visual.widget.ComponentWidget;
import org.netbeans.api.visual.widget.Scene;
import org.netbeans.api.visual.widget.Widget;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;
import org.openide.util.lookup.ProxyLookup;
import uk.ac.ncl.icos.synbad.editor.core.DeleteAction;

import uk.ac.ncl.icos.synbad.moduleeditor.widgets.svpmodulewidget.GroupBracket;

/**
 *
 * @author owengilfellon
 */
public final class SVPModuleListWidget extends Widget
{
    private final Lookup lookup;
    private final ComponentWidget header;
    private final JPanel headerPanel;
    private final JPanel footerPanel;
    private final Widget footer;
    private final Widget componentList;

    public SVPModuleListWidget(final Scene scene) {
        
        super(scene);
        
        this.lookup = new ProxyLookup(Lookups.fixed(this, scene));
        setLayout(LayoutFactory.createHorizontalFlowLayout(LayoutFactory.SerialAlignment.CENTER, 3));

        this.headerPanel = new GroupBracket(20, GroupBracket.HEADER);
        this.headerPanel.setSize(new Dimension(20, 80));
        this.headerPanel.setOpaque(false);
        this.header = new ComponentWidget(scene, headerPanel);
        this.header.setPreferredSize(new Dimension(20,80));
        addChild(header);
        
        this.componentList = new Widget(scene);
        this.componentList.setLayout(LayoutFactory.createHorizontalFlowLayout(LayoutFactory.SerialAlignment.JUSTIFY, 3));
        componentList.setOpaque(false);
        addChild(componentList);
       
        this.footerPanel = new GroupBracket(20, GroupBracket.FOOTER);
        this.footerPanel.setSize(new Dimension(20, 80));
        this.footerPanel.setOpaque(false);
        this.footer = new ComponentWidget(scene, footerPanel);
        this.footer.setPreferredSize(new Dimension(20,80));
        addChild(footer);
        
        getActions().addAction(scene.createWidgetHoverAction());
        getActions().addAction(new DeleteAction());
        getActions().addAction(ActionFactory.createMoveAction());
        //getActions("none").addAction(((InteractionsImpl)getScene()).getSelectAction());
        //getActions("none").addAction(ActionFactory.createAcceptAction(new SVPAcceptProvider()));
        
        setBorder(getLookup().lookup(Scene.class).getLookFeel().getBorder(ObjectState.createNormal()));
    }

    @Override
    public Lookup getLookup() {
        return lookup;
    }

    public void addComponent(Widget widget)
    {
        componentList.addChild(widget);
    }
    
    public void addComponent(int index, Widget widget)
    {
        componentList.addChild(index, widget);
    }

    public List<Widget> getComponents()
    {
        return componentList.getChildren();
    }
    
    public void removeComponent(Widget widget)
    {
        componentList.removeChild(widget);
    }
    
    public boolean isEmpty() {
        return componentList.getChildren().isEmpty();
    }
}
