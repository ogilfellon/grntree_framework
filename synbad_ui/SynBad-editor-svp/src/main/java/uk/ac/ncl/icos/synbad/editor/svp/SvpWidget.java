/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.editor.svp;

import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.RoundRectangle2D;
import org.netbeans.api.visual.action.ActionFactory;
import org.netbeans.api.visual.border.BorderFactory;
import org.netbeans.api.visual.model.ObjectScene;
import org.netbeans.api.visual.model.ObjectState;
import org.netbeans.api.visual.widget.Scene;
import org.netbeans.api.visual.widget.general.IconNodeWidget;

import org.openide.util.ImageUtilities;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentRole;
import uk.ac.ncl.icos.datamodel.workspace.impl.EntityInstance;
import uk.ac.ncl.icos.datamodel.workspace.objects.SynBadObject;
import uk.ac.ncl.icos.synbad.datadefinition.IDataDefinitionManager;
import uk.ac.ncl.icos.synbad.editor.core.AHierarchicalGraphScene;
import uk.ac.ncl.icos.synbad.svpfragment.nodes.VSynBadObjectNode;
import uk.ac.ncl.icos.model.entities.VNode;

/**
 *
 * @author owengilfellon
 */

public class SvpWidget extends IconNodeWidget  {
    
    private static final Image promoter = ImageUtilities.loadImage("uk/ac/ncl/icos/synbad/synbad/svpfragment/promoter.png");
    private static final Image rbs = ImageUtilities.loadImage("uk/ac/ncl/icos/synbad/synbad/svpfragment/translational-start-site.png");
    private static final Image operator = ImageUtilities.loadImage("uk/ac/ncl/icos/synbad/synbad/svpfragment/operator.png");
    private static final Image cds = ImageUtilities.loadImage("uk/ac/ncl/icos/synbad/synbad/svpfragment/cds.png");
    private static final Image terminator = ImageUtilities.loadImage("uk/ac/ncl/icos/synbad/synbad/svpfragment/terminator.png");
    private static final Image shim = ImageUtilities.loadImage("uk/ac/ncl/icos/synbad/synbad/svpfragment/shim24.png");
    private static final Image userdefined = ImageUtilities.loadImage("uk/ac/ncl/icos/synbad/synbad/svpfragment/userdefined.png");

    private boolean minimised = false;
    private final VSynBadObjectNode node;
    private final EntityInstance instance;
    private final Scene scene;
    
    private static final IDataDefinitionManager manager = new IDataDefinitionManager.DefaultDataDefinitionManager();

    public SvpWidget(Scene scene, VSynBadObjectNode node) 
    {
        super(scene);
        this.scene = scene;
        this.node = node;
        this.instance = (EntityInstance)node.getLookup().lookup(VNode.class).getData();

        setIcon(instance.getValue());
        setLabel(instance.getValue().getName());
        setBackground(instance.getValue());
        
        getLabelWidget().setBorder(BorderFactory.createEmptyBorder(5));
        getLabelWidget().setBackground(scene.getLookFeel().getBackground());
       // getActions().addAction(new DeleteAction());
        getActions().addAction(((ObjectScene)scene).createObjectHoverAction());
        getActions().addAction(((ObjectScene)getScene()).createSelectAction());

        
        
        getActions().addAction(ActionFactory.createMoveAction(
            ActionFactory.createFreeMoveStrategy(),
            new SVPMover((AHierarchicalGraphScene)getScene())));

    }

    private void setIcon(SynBadObject definition)
    {
        ComponentRole role = definition.getRoles().stream()
                .filter(r -> r instanceof ComponentRole)
                .map( r -> (ComponentRole) r ).findFirst().orElse(null);

        if(role == null)
            return;
        
        switch (role) {
            case Promoter:
                setImage(promoter);
                break;
            case RBS:
                setImage(rbs);
                break;
            case CDS:
                setImage(cds);
                break;
            case Operator:
                setImage(operator);
                break;
            case Terminator:
                setImage(terminator);
                break;
            case Shim:
                setImage(shim);
                break;
            case Generic:
                setImage(userdefined);
                break;
        }

    }

    
    
    private void setBackground(SynBadObject definition)
    {
        ComponentRole role = definition.getRoles().stream()
                .filter(r -> r instanceof ComponentRole)
                .map( r -> (ComponentRole) r ).findFirst().orElse(null);

        if(role == null)
            return;
        
        switch (role) {
            case Promoter:
                setBackground(Color.GREEN);
                break;
            case RBS:
                setBackground(Color.ORANGE);
                break;
            case CDS:
                setBackground(Color.BLUE);
                break;
            case Operator:
                setBackground(Color.YELLOW);
                break;
            case Terminator:
                setBackground(Color.RED);
                break;
            case Shim:
                 setBackground(Color.CYAN);
                break;
            case Generic:
                setBackground(Color.PINK);
                break;
        }
        
    } 


    @Override
    public void notifyStateChanged(ObjectState previousState, ObjectState state) {
        if(state.isSelected()) {
            getLabelWidget().setOpaque(true);
             getLabelWidget().setBorder(BorderFactory.createRoundedBorder(5, 5, (Color)getScene().getLookFeel().getBackground(
                    ObjectState.createNormal().deriveObjectHovered(
                            state.isSelected())), Color.gray));
        } else if (state.isWidgetAimed()) {
            getLabelWidget().setOpaque(true);
            getLabelWidget().setBackground(Color.RED);
        }
        else if(state.isWidgetHovered()) {
            getLabelWidget().setOpaque(true);
            getLabelWidget().setBorder(BorderFactory.createRoundedBorder(5, 5, (Color)getScene().getLookFeel().getBackground(
                    ObjectState.createNormal().deriveObjectHovered(
                            state.isHovered())), Color.gray));
        }
        else {
            getLabelWidget().setOpaque(false);
            getLabelWidget().setBorder(BorderFactory.createEmptyBorder(5));
        }
    }
    
    @Override
    protected void paintWidget() {
        super.paintWidget();
        Color gradientColor1 =  getBackground() instanceof Color ? (Color)getBackground() : null;
        Color gradientColor2 =  gradientColor1 != null ? new Color(gradientColor1.getRed(),
                gradientColor1.getGreen(),
                gradientColor1.getBlue(),
                gradientColor1.getAlpha() - 100) : null;
        
        if(gradientColor1 != null && gradientColor2 != null) {
            GradientPaint gradient = new GradientPaint(0,0, gradientColor1,0, (getBounds().height), gradientColor2);
            Graphics2D g2 = getGraphics();
            g2.setPaint(gradient);
            g2.fill(new RoundRectangle2D.Float(0, 0, getBounds().width, getBounds().height, 20, 20));
        }
    }

}
