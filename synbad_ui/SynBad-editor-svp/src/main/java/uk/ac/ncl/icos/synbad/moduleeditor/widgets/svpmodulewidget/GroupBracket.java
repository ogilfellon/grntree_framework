/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.moduleeditor.widgets.svpmodulewidget;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.Area;
import java.awt.geom.RoundRectangle2D;
import javax.swing.JPanel;

/**
 *
 * @author owengilfellon
 */
public class GroupBracket extends JPanel {
    
    public static int HEADER = 0;
    public static int FOOTER = 1;
    final private int curve;
    final private int orientation;
    private boolean hovered = false;

    public GroupBracket(int curve, int orientation) {
        assert orientation >= 0 && orientation <=1;
        this.curve = curve; 
        this.orientation = orientation;
    }

    public void setHovered(boolean hovered) {
        this.hovered = hovered;
    }
   
 
    @Override
    public void paint(Graphics g) {
        
        
      
        
        
        Graphics2D g2 = (Graphics2D)g;
        Shape positiveSpace;
        Shape negativeSpace;
        g2.setPaint(hovered ? getBackground().brighter() : getBackground());
        
        if(orientation == HEADER) {
            positiveSpace = new RoundRectangle2D.Double(
                    getBounds().x,
                    getBounds().y,
                    getBounds().width,
                    getBounds().height,
                    curve, curve);
            negativeSpace = new RoundRectangle2D.Double(
                    getBounds().x + 10,
                    getBounds().y,
                    getBounds().width,
                    getBounds().height, 
                    curve, curve);
            setBounds(getBounds().x,
                    getBounds().y,
                    20,
                    getBounds().height);
        } else {          
            positiveSpace = new RoundRectangle2D.Double(
                    getBounds().x,
                    getBounds().y,
                    getBounds().width,
                    getBounds().height,
                    curve, curve);
            negativeSpace = new RoundRectangle2D.Double(
                    getBounds().x - 10,
                    getBounds().y,
                    getBounds().width,
                    getBounds().height,
                    curve, curve);
            setBounds(getBounds().x,
                    getBounds().y,
                    20,
                    getBounds().height);
        }
        
        Area positiveArea = new Area(positiveSpace);
        Area negativeArea = new Area(negativeSpace);
        positiveArea.subtract(negativeArea);
        g2.fill(positiveArea);
    }
}
