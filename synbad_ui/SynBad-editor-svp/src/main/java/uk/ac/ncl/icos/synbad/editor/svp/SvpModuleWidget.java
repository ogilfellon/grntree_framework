/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.editor.svp;

import org.netbeans.api.visual.action.ActionFactory;
import org.netbeans.api.visual.model.StateModel;
import org.openide.util.Lookup;
import uk.ac.ncl.icos.synbad.editor.core.EntityWidget;
import uk.ac.ncl.icos.synbad.moduleeditor.dnd.PartAcceptProvider;
import uk.ac.ncl.icos.synbad.svpfragment.nodes.VSynBadObjectNode;

/**
 *
 * @author owengilfellon
 */
public class SvpModuleWidget extends EntityWidget<SvpWidget> implements  StateModel.Listener, Lookup.Provider {

    private final SVPModuleListWidget listWidget;

    public SvpModuleWidget(SvpScene scene, VSynBadObjectNode entity) {
        super(scene, entity);
        listWidget = new SVPModuleListWidget(scene);
        getActions().addAction(ActionFactory.createAcceptAction(new PartAcceptProvider(scene.getWorkspace())));
    }

    @Override
    public void addWidget(SvpWidget widget) {
        if(listWidget.isEmpty())
            getEntitiesLayer().addChild(listWidget);
        listWidget.addComponent(widget);
    }
    
    public void addWidget(SvpModuleWidget widget) {
        getEntitiesLayer().addChild(widget);
    }
    
    public void removeWidget(SvpWidget widget) {
        listWidget.removeComponent(widget);
        if(listWidget.isEmpty())
            getEntitiesLayer().removeChild(listWidget);
    }
    
    public void removeWidget(SvpModuleWidget widget) {
        getEntitiesLayer().removeChild(widget);
    }
    
    /*
    public class SvpModuleLayout implements Layout {


        @Override
        public void layout (Widget widget) {
            for (Widget child : widget.getChildren ()) {
                child.resolveBounds (null, null);
            }
        }

        @Override
        public boolean requiresJustification (Widget widget) {
            return true;
        }

        @Override
        public void justify (Widget widget) {
            
            int top_margin = 30;
            int track_margin = 30;
            
            Rectangle bounds = widget.getClientArea();
            List<Widget> children = ((SvpModuleWidget)widget).getEntitiesLayer().getChildren();

            if(!listWidget.isEmpty())
                listWidget.resolveBounds(null, new Rectangle(0, top_margin, widget.getClientArea().width, widget.getClientArea().height));
            
            for(Widget w : children) {
                if(!(w instanceof SVPModuleListWidget)) {
                     w.resolveBounds(null, new Rectangle());
                } 
               
            }
        }
    }*/
}
