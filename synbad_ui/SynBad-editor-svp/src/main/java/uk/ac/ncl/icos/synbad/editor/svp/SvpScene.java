/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.editor.svp;

import uk.ac.ncl.icos.synbad.datamodel.visualisation.widgetaction.ToolchainManager;
import java.awt.Color;
import java.awt.Dimension;
import java.util.HashMap;
import java.util.Map;
import org.netbeans.api.visual.anchor.Anchor;
import org.netbeans.api.visual.anchor.AnchorFactory;
import org.netbeans.api.visual.anchor.AnchorShape;
import org.netbeans.api.visual.anchor.AnchorShapeFactory;
import org.netbeans.api.visual.router.Router;
import org.netbeans.api.visual.router.RouterFactory;
import org.netbeans.api.visual.widget.ConnectionWidget;
import org.netbeans.api.visual.widget.Widget;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;
import org.openide.util.lookup.ProxyLookup;
import uk.ac.ncl.icos.datamodel.events.EventType;
import uk.ac.ncl.icos.datamodel.workspace.api.IWorkspace;

import uk.ac.ncl.icos.datamodel.workspace.impl.EntityInstance;
import uk.ac.ncl.icos.datamodel.workspace.impl.PortDirection;
import uk.ac.ncl.icos.datamodel.workspace.impl.PortRecord;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.datadefinition.types.EntityType;
import uk.ac.ncl.icos.synbad.datadefinition.types.ModelEdgeType;
import uk.ac.ncl.icos.synbad.datadefinition.types.ModuleRole;
import uk.ac.ncl.icos.synbad.editor.core.AHierarchicalGraphScene;
import uk.ac.ncl.icos.synbad.editor.core.PinWidget;
import uk.ac.ncl.icos.synbad.svpfragment.nodes.VSynBadObjectNode;
import uk.ac.ncl.icos.synbad.svpfragment.nodes.VPortNode;
import uk.ac.ncl.icos.synbad.svpfragment.nodes.VWireNode;
import uk.ac.ncl.icos.model.entities.VEdge;
import uk.ac.ncl.icos.model.entities.VNode;
import uk.ac.ncl.icos.model.entities.VPort;
import uk.ac.ncl.icos.view.api.HView;
import uk.ac.ncl.icos.view.api.View;

/**
 *
 * @author owengilfellon
 */
public class SvpScene extends AHierarchicalGraphScene {
    
    private final Map<VNode, VSynBadObjectNode> entityNodeMap = new HashMap<>();
    private final Map<VPort, VPortNode> portNodeMap = new HashMap<>();
    private final Map<VEdge, VWireNode> wireNodeMap = new HashMap<>();

    public SvpScene( HView view ) {
        super(view);
        setMinimumSize(new Dimension(800, 600));
        addNode(new VSynBadObjectNode(view.getRoot()));
        getPriorActions().addAction(new ToolchainManager());
        setActiveTool("default");
        view.addListener(this);
    }

    @Override
    public Lookup getLookup() {
        return new ProxyLookup(super.getLookup(), Lookups.fixed(getDataModelView()));
    }

    @Override
    protected Widget attachNodeWidget(VSynBadObjectNode n) {
        
        VNode vNode = n.getLookup().lookup(VNode.class);
        
        
        
        Widget e = null;
        
        if(vNode.getData().getValue().hasAnnotation(SynBadTerms.Annotatable.role, ModuleRole.SVPPart.getUri())) {
            e = new SvpWidget(this, n);
            Widget parent = findWidget(entityNodeMap.get(getDataModelView().getView().getParent(vNode)));
            if(parent!=null && parent instanceof SvpModuleWidget) {
               ((SvpModuleWidget)parent).addWidget((SvpWidget)e);
            }
        } else if (vNode.getData().getValue().hasAnnotation(SynBadTerms.Annotatable.entitytype, EntityType.Module.getUri())) {
            e = new SvpModuleWidget(this, n);
            if(vNode == getDataModelView().getView().getRoot()) {
                getEntitiesLayer().addChild(e);
                getEntitiesLayer().revalidate();
            } else {
                Widget parent = findWidget(entityNodeMap.get(getDataModelView().getParent(vNode)));
                if(parent!=null && parent instanceof SvpModuleWidget) {
                   ((SvpModuleWidget)parent).addWidget((SvpModuleWidget)e);
                }
            }
        }

        entityNodeMap.put(n.getLookup().lookup(VNode.class), n);
        return  e;
    }

    @Override
    protected void detachNodeWidget(VSynBadObjectNode node, Widget widget) {
        super.detachNodeWidget(node, widget);
        entityNodeMap.remove(node.getLookup().lookup(VNode.class));
    }

    @Override
    protected Widget attachPinWidget(VSynBadObjectNode n, VPortNode p) {
        Widget owner = findWidget(n);
        
        if(owner != null && owner instanceof SvpModuleWidget) {
            portNodeMap.put(p.getLookup().lookup(VPort.class), p);
            return ((SvpModuleWidget)owner).addPin(p);
        }

        return null;
    }

    @Override
    protected void detachPinWidget(VPortNode pin, Widget widget) {
        super.detachPinWidget(pin, widget); 
        portNodeMap.remove(pin.getLookup().lookup(VPort.class));
    }

    @Override
    protected Widget attachEdgeWidget(VWireNode e) {
        
        HView view = getDataModelView();
        
        VEdge vEdge = e.getLookup().lookup(VEdge.class);
        IDataModelEdge edge = vEdge.getData();

        ConnectionWidget w = new ConnectionWidget(this);
      
        if((vEdge.getFrom() instanceof VPort) && (vEdge.getTo() instanceof VPort)) {

            VPort fromPort =  (isTypeOf(ModelEdgeType.PROXY, vEdge)
                    && ((VPort)vEdge.getFrom()).getData().getValue().getPortDirection() == PortDirection.OUT)
                    ? (VPort)vEdge.getTo() : 
                    (VPort)vEdge.getFrom();
            
            
            VPort toPort =   (isTypeOf(ModelEdgeType.PROXY, vEdge)
                     && fromPort.getData().getValue().getPortDirection() == PortDirection.OUT)
                     ? (VPort)vEdge.getFrom() : 
                    (VPort)vEdge.getTo();


            VNode from = view.getParent(fromPort);
            VNode to = view.getParent(toPort);

            if(from != null && to != null) {

                boolean isFromParent = view.getParent(to) == from;
                boolean isToParent = view.getParent(from) == to;

                VSynBadObjectNode parentNode = 
                        !isFromParent ?
                        entityNodeMap.get(view.getParent(from)) : 
                        entityNodeMap.get(from);
                
                VSynBadObjectNode fromNode = entityNodeMap.get(view.getParent(fromPort));
                VSynBadObjectNode toNode = entityNodeMap.get(view.getParent(toPort));
                
                Widget w1 = findWidget(fromNode);
                Widget w2 = findWidget(toNode);

                Anchor fromAnchor = (w1 instanceof SvpModuleWidget) ?
                    ((SvpModuleWidget)findWidget(fromNode)).getAnchor(fromPort, isFromParent) :
                    AnchorFactory.createDirectionalAnchor(w1, AnchorFactory.DirectionalAnchorKind.VERTICAL, 5);

                Anchor toAnchor = (w2 instanceof SvpModuleWidget) ?
                    ((SvpModuleWidget)findWidget(toNode)).getAnchor(toPort, isToParent) :
                    AnchorFactory.createDirectionalAnchor(w2, AnchorFactory.DirectionalAnchorKind.VERTICAL, 5);
    
                w.setSourceAnchor(fromAnchor);
                w.setTargetAnchor(toAnchor);
                
                w.setTargetAnchorShape(AnchorShape.TRIANGLE_FILLED);
                w.setLineColor(Color.WHITE);
                
                Router r = RouterFactory.createOrthogonalSearchRouter(((SvpModuleWidget)findWidget(parentNode)).getEntitiesLayer());

                w.setRouter(r);
                w.setControlPointCutDistance(10);
                getConnectionsLayer().addChild(w);
            }
         }
        
        wireNodeMap.put(vEdge, e);

        return w;
    }

    @Override
    protected void attachEdgeSourceAnchor(VWireNode e, VPortNode p, VPortNode p1) {
        if(p1 != null) {
            IDataModelEdge edge = e.getLookup().lookup(VEdge.class).getData();

            PortRecord from = (PortRecord)edge.getFrom();
            PortRecord to = (PortRecord)edge.getTo();
            EntityInstance fromOwner = from.getOwner();
            EntityInstance toOwner = to.getOwner();

            boolean internal = false;

            if (fromOwner.getValue().getChildren().contains(toOwner) ||
                    toOwner.getValue().getChildren().contains(fromOwner))
                internal = true;
            
            VPort vPort1 = p1.getLookup().lookup(VPort.class);

            Anchor sourceAnchor = ((SvpModuleWidget)findWidget(vPort1.getData().getOwner())).getAnchor(vPort1, internal);
            ConnectionWidget edgeWidget = (ConnectionWidget) findWidget (e);
            edgeWidget.setSourceAnchor (sourceAnchor);
            ((PinWidget)findWidget(p1)).setIsConnected(true);
            
        } else if (p != null) {
            ((PinWidget)findWidget(p)).setIsConnected(false);
        }

        if(p1 == null) {
            ConnectionWidget edgeWidget = (ConnectionWidget) findWidget (e);
            getConnectionsLayer().removeChild(edgeWidget);
        }
    }

    @Override
    protected void attachEdgeTargetAnchor(VWireNode e, VPortNode p, VPortNode p1) { 
        if(p1 != null) {
            
            IDataModelEdge edge = e.getLookup().lookup(VEdge.class).getData();

            PortRecord from = (PortRecord)edge.getFrom();
            PortRecord to = (PortRecord)edge.getTo();
            EntityInstance fromOwner = from.getOwner();
            EntityInstance toOwner = to.getOwner();

            boolean internal = false;

            if (fromOwner.getValue().getChildren().contains(toOwner) ||
                    toOwner.getValue().getChildren().contains(fromOwner))
                internal = true;
            
            VPort vPort1 = p1.getLookup().lookup(VPort.class);
            
            Anchor targetAnchor = ((SvpModuleWidget)findWidget(vPort1.getData().getOwner())).getAnchor(vPort1, internal);
            
            ConnectionWidget edgeWidget = (ConnectionWidget) findWidget (e);
            edgeWidget.setTargetAnchor(targetAnchor);
            edgeWidget.setTargetAnchorShape(AnchorShapeFactory.createTriangleAnchorShape(10, true, false));
            ((PinWidget)findWidget(p1)).setIsConnected(true);
        } else if (p != null) {
            ((PinWidget)findWidget(p)).setIsConnected(false);
        }
    }

    @Override
    protected void detachEdgeWidget(VWireNode edge, Widget widget) {
        super.detachEdgeWidget(edge, widget); 
        wireNodeMap.remove(edge.getLookup().lookup(VEdge.class));
    }
   
    @Override
    public void onEntityEvent(EventType type, VNode instance, VNode parent) {
        
        if(instance.getData() instanceof EntityInstance) {
            if(type == EventType.ADDED) {
                if(!entityNodeMap.containsKey(instance))
                    addNode(new VSynBadObjectNode(instance));
            } else if(type == EventType.REMOVED) {
                if(entityNodeMap.containsKey(instance))
                    removeNode(entityNodeMap.get(instance));
            }
        } else if (instance instanceof VPort) {
            if(type == EventType.ADDED) {
                if(!portNodeMap.containsKey(instance))
                    addPin(entityNodeMap.get(parent), new VPortNode((VPort)instance));
            } else if (type == EventType.REMOVED) {
                if(portNodeMap.containsKey(instance))
                {
                    VPortNode port = portNodeMap.get((VPort)instance);
                    
                    // Because SVPs don't have Port Widgets
                
                    if(findWidget(port)!=null)
                        removePin(port);
                }
            }
        }
        
        revalidate();
    }

    @Override
    public void onEdgeEvent(EventType type, VEdge edge) {

        if(type == EventType.ADDED) {
            if(isTypeOf(ModelEdgeType.PROXY, edge) || 
                    isTypeOf(ModelEdgeType.WIRE, edge) && !isCisWiring(edge)) {
                if(!wireNodeMap.containsKey(edge))
                    addEdge(new VWireNode(edge));
            }   
        }
        else if (type == EventType.REMOVED) {
            if(isTypeOf(ModelEdgeType.PROXY, edge) || 
                    isTypeOf(ModelEdgeType.WIRE, edge) && !isCisWiring(edge)) {
                if(wireNodeMap.containsKey(edge))
                    removeEdge(wireNodeMap.get(edge));
            }
        }

        revalidate();     
    }
    
    private boolean isCisWiring(VEdge edge) {
        EntityInstance from = ((PortRecord)edge.getFrom().getData()).getOwner();
        EntityInstance to = ((PortRecord)edge.getTo().getData()).getOwner();
        return from.getValue().hasAnnotation(SynBadTerms.Annotatable.role, ModuleRole.SVPPart.getUri())
                && to.getValue().hasAnnotation(SynBadTerms.Annotatable.role, ModuleRole.SVPPart.getUri());
    }
    
    private boolean isTypeOf(ModelEdgeType type, VEdge edge) {
        return edge.getData().getType() == type;
    }
    
    public IWorkspace getWorkspace() {
        return getDataModelView().getRoot().getData().getWorkspace();
    }
 
}
