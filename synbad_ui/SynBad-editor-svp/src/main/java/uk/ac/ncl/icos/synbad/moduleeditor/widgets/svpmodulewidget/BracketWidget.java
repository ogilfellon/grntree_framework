/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.moduleeditor.widgets.svpmodulewidget;

import java.awt.Component;
import org.netbeans.api.visual.laf.LookFeel;
import org.netbeans.api.visual.model.ObjectState;
import org.netbeans.api.visual.widget.ComponentWidget;
import org.netbeans.api.visual.widget.Scene;

/**
 *
 * @author owengilfellon
 */
public class BracketWidget extends ComponentWidget {

    private LookFeel feel;
    
    public BracketWidget(Scene scene, Component component, LookFeel feel) {
        super(scene, component);
        this.feel = feel;
    }

    @Override
    protected void notifyStateChanged(ObjectState previousState, ObjectState state) {
        if(!previousState.isHovered() && state.isHovered()) {
            getComponent().setBackground(feel.getForeground(ObjectState.createNormal().deriveObjectHovered(true)));
        } else {
            if(previousState.isHovered() && !state.isHovered()) {
            getComponent().setBackground(feel.getForeground(ObjectState.createNormal().deriveObjectHovered(false)));
            }
        }
    }
    
    
    
}
