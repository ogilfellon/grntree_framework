/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.parsers;

/**
 *
 * @author owengilfellon
 */
public class SbolRdfTests {

   /* private static VModel model;
    private static SBOLDocument document;
    private static Model rdfModel;
    private static Repository repository;

    
    @BeforeClass
    public static void setUpClass() throws SBOLValidationException {
        model = ExampleFactory.setupTemplateModel();
        MockServices.setServices(IDataDefinitionProvider.DefaultDefinitionProvider.class, SbolTypeProvider.class);
        document = SbolParser.write(model.getWorkspace());
        //rdfModel = RdfParser.getModel(document);
        repository = new SailRepository(new MemoryStore());
        repository.initialize();
        repository.getConnection().add(rdfModel);
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
        @Test
    public void testEntityRetrieval() throws FileNotFoundException, XMLStreamException, FactoryConfigurationError, CoreIoException, SBOLValidationException {

        String queryString = "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "
                + "PREFIX sbol: <http://sbols.org/v2#> "
                + "SELECT ?id ?a ?b "
                + "WHERE { "
                + "?id ?a ?b . "
                + "?id rdf:type <http://www.synbad.org#Port>"
                + " }";
        
        TupleQuery tupleQuery = repository.getConnection().prepareTupleQuery(QueryLanguage.SPARQL, queryString);
        TupleQueryResult result = tupleQuery.evaluate();
        
        String displayId = null;
        String version = null;
        String name = null;
        String description = null;
        String portAccess = null;
        String portDirection = null;
        String portState = null;
        String portType = null;
        String type = null;

        try {
            while (result.hasNext()) { 
                BindingSet bindingSet = result.next();
                Value a = bindingSet.getValue("a");
                Value b = bindingSet.getValue("b");
                Value id = bindingSet.getValue("id");
               if(a.stringValue().equals("http://sbols.org/v2#displayId")) {
                    displayId = b.stringValue();
                } else if(a.stringValue().equals("http://sbols.org/v2#version")) {
                    version = b.stringValue();
                } else if(a.stringValue().equals("http://purl.org/dc/terms/title")) {
                    name = b.stringValue();
                } else if(a.stringValue().equals("http://purl.org/dc/terms/description")) {
                    description = b.stringValue();
                } else if(a.stringValue().equals("http://www.synbad.org#portAccess")) {
                   portAccess = b.toString();
                } else if(a.stringValue().equals("http://www.synbad.org#portDirection")) {
                   portDirection = b.stringValue();
                } else if(a.stringValue().equals("http://www.synbad.org#portState")) {
                   portState = b.stringValue();
                } else if(a.stringValue().equals("http://www.synbad.org#portType")) {
                   portType= b.stringValue();
                } else if(a.stringValue().equals("http://www.w3.org/1999/02/22-rdf-syntax-ns#type")) {
                   type = b.stringValue();
                }/* */
                
              //  System.out.println(id + " : " + a.stringValue() + " :" + b.stringValue());
            /*}
        }
        finally {
            result.close();
        }
        /*
        if(displayId!=null && version !=null) {
            IEntity newEntity = new AEntity(new Workspace(), UriHelper.synbad.getNamespaceURI(), displayId, version);
            if(name!=null) newEntity.setName(name);
            if(description!=null) newEntity.setDescription(description);
            System.out.println();
        }*/
        
        
    }
/*
    @Test
    public void testSvpRetrieval() throws FileNotFoundException, XMLStreamException, FactoryConfigurationError, CoreIoException, SBOLValidationException {

        String queryString = "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "
                + "PREFIX sbol: <http://sbols.org/v2#> "
                + "SELECT ?id "
                + "WHERE { "
                + "?id sbol:role <http://virtualparts.org/svp-module>"
                + " }";
        TupleQuery tupleQuery = repository.getConnection().prepareTupleQuery(QueryLanguage.SPARQL, queryString);
        TupleQueryResult result = tupleQuery.evaluate();
        
        Set<URI> svpsFromRdf = new HashSet<>();
        
        for(IEntity entity : model.getWorkspace().getEntities()) {
            if(entity.hasAnnotation(SynBadTerms.Annotatable.role, ModuleRole.SVPPart.getUri())) {
                svpsFromRdf.add(entity.getIdentity());
            }
        }

        try {
            while (result.hasNext()) { 
                BindingSet bindingSet = result.next();
                Value id = bindingSet.getValue("id");
                assert(svpsFromRdf.contains(URI.create(id.stringValue())));
            }
        }
        finally {
            result.close();
        }
    }
    
    @Test
    public void testPortRetrieval() throws FileNotFoundException, XMLStreamException, FactoryConfigurationError, CoreIoException, SBOLValidationException {

        String queryString = "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "+
                "PREFIX synbad: <http://www.synbad.org#> " +
                "SELECT  ?portId " +
                "WHERE { " +
                "?id synbad:port ?portId " +
                ". ?portId rdf:type synbad:Port }";
        
        TupleQuery tupleQuery = repository.getConnection().prepareTupleQuery(QueryLanguage.SPARQL, queryString);
        TupleQueryResult result = tupleQuery.evaluate();
        
        Set<URI> workspacePortIds = new HashSet<>();
        Set<URI> rdfPortIds = new HashSet<>();
        
        for(IEntity entity : model.getWorkspace().getEntities()) {
            for(IPort port : entity.getPorts()) {
                if(!port.isProxy()) {
                    workspacePortIds.add(port.getIdentity());
                }
            }
        }

        try {
            while (result.hasNext()) { 
                BindingSet bindingSet = result.next();
                Value portId = bindingSet.getValue("portId");
                URI uri = URI.create(portId.stringValue());
                rdfPortIds.add(uri);
                assert(workspacePortIds.contains(uri));
            }
        }
        finally {
            result.close();
        }
        
        assert(workspacePortIds.size() == rdfPortIds.size());
    }
    
    @Test
    public void testProxyPortRetrieval() throws FileNotFoundException, XMLStreamException, FactoryConfigurationError, CoreIoException, SBOLValidationException {

        String queryString = "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "+
                "PREFIX synbad: <http://www.synbad.org#> " +
                "SELECT  ?portId " +
                "WHERE { " +
                "?id synbad:port ?portId " +
                ". ?portId rdf:type synbad:ProxyPort }";
        TupleQuery tupleQuery = repository.getConnection().prepareTupleQuery(QueryLanguage.SPARQL, queryString);
        TupleQueryResult result = tupleQuery.evaluate();
        
        Set<URI> workspacePortIds = new HashSet<>();
        Set<URI> rdfPortIds = new HashSet<>();
        
        for(IEntity entity : model.getWorkspace().getEntities()) {
            for(IPort port : entity.getPorts()) {
                if(port.isProxy()) {
                    workspacePortIds.add(port.getIdentity());
                }
            }
        }

        try {
            while (result.hasNext()) { 
                BindingSet bindingSet = result.next();
                Value portId = bindingSet.getValue("portId");
                URI uri = URI.create(portId.stringValue());
                rdfPortIds.add(uri);
                assert(workspacePortIds.contains(uri));
            }
        }
        finally {
            result.close();
        }
        
        assert(workspacePortIds.size() == rdfPortIds.size());
    }
    
    @Test
    public void testOther() throws FileNotFoundException, XMLStreamException, FactoryConfigurationError, CoreIoException, SBOLValidationException {

        String queryString = "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "+
                "PREFIX synbad: <http://www.synbad.org#> " +
                "SELECT  ?a ?b ?c " +
                "WHERE { ?a ?b ?c }";
        
        TupleQuery tupleQuery = repository.getConnection().prepareTupleQuery(QueryLanguage.SPARQL, queryString);
        TupleQueryResult result = tupleQuery.evaluate();

        try {
            while (result.hasNext()) { 
                BindingSet bindingSet = result.next();
                Value a = bindingSet.getValue("a");
                Value b = bindingSet.getValue("b");
                Value c = bindingSet.getValue("c");
                
                System.out.println(a + " : " + b + " : " + c);
                
            }
        }
        finally {
            result.close();
        }

    }
    */



