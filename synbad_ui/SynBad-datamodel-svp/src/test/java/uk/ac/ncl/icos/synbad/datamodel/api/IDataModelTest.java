/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.datamodel.api;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import uk.ac.ncl.icos.datamodel.workspace.api.SBWorkspace;
import uk.ac.ncl.icos.synbad.datadefinition.UriHelper;
import uk.ac.ncl.icos.datamodel.workspace.objects.SynBadPObject;
import uk.ac.ncl.icos.datamodel.workspace.objects.IPort;
import uk.ac.ncl.icos.datamodel.workspace.impl.EntityInstance;
import uk.ac.ncl.icos.datamodel.workspace.impl.Identity;
import uk.ac.ncl.icos.datamodel.workspace.impl.PortDirection;
import uk.ac.ncl.icos.datamodel.workspace.impl.Workspace;
import uk.ac.ncl.icos.synbad.signalgraph.impl.SynBadPortType;
import uk.ac.ncl.icos.view.api.View;
import uk.ac.ncl.icos.model.VModel;
import uk.ac.ncl.icos.view.api.View;
import uk.ac.ncl.icos.view.api.ViewCrawler;
import uk.ac.ncl.icos.view.api.HViewCursor;
import uk.ac.ncl.icos.view.impl.AsciiTree;
import uk.ac.ncl.icos.view.impl.IteratorRule;
import uk.ac.ncl.icos.view.impl.VModelImpl;

/**
 *
 * @author owengilfellon
 */
public class IDataModelTest {
   

    public IDataModelTest() {

    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getNamespaceBinding method, of class IDataModel.
     */
    @Test
    public void testDataModel() {
        
        SBWorkspace workspace = new Workspace(UriHelper.synbad.getPrefix(), "testWorkspace", "1.0");
        SynBadPObject entityA = workspace.createEntity(new Identity(UriHelper.synbad.getNamespaceURI(), "EntityA", "1.0"));
        SynBadPObject entityB = workspace.createEntity(new Identity(UriHelper.synbad.getNamespaceURI(), "EntityB", "1.0"));
        EntityInstance instanceB = entityA.addChild(entityB);

        assert(instanceB != null);
        assert(!entityA.getChildren(entityB.getIdentity()).isEmpty());
        
        assert(workspace.containsEntity(entityA.getIdentity()));
        assert(workspace.containsEntity(entityB.getIdentity()));
        
        assert(workspace.containsEntity(entityA.getIdentity()));
        assert(workspace.containsEntity(entityB.getIdentity()));
        
        IPort port = entityB.createPort(true, PortDirection.OUT, SynBadPortType.PoPS, null, null);

        assert(port.getOwner().equals(entityB));
        assert(port.getOwner().getIdentity().equals(entityB.getIdentity()));
        assert(entityB.getPort(p -> p.getPortDirection() == PortDirection.OUT) == port);
        assert(workspace.getPort(entityB.getPort(p -> p.getPortDirection() == PortDirection.OUT).getIdentity()) == port);

        IPort proxyPort = entityA.createPort(port.getIdentity(), true);
        
        assert(proxyPort.getOwner().equals(entityA));
        assert(proxyPort.getOwner().getIdentity().equals(entityA.getIdentity()));
        assert(entityA.getPort(p -> p.getPortDirection() == PortDirection.OUT) == proxyPort);
        assert(workspace.getPort(entityA.getPort(p -> p.getPortDirection() == PortDirection.OUT).getIdentity()) == proxyPort);
        
        
        VModel model = new VModelImpl(entityA, workspace);
        
        assert(model.getModelRoot().getData().getValue().equals(entityA));
        assert(model.getModelRoot().getData().getValue().getIdentity().equals(entityA.getIdentity()));
        
        View<View> view = model.getDefaultView();
        HViewCursor cursor = view.getView().createCursor();
        
        ViewCrawler crawler = new ViewCrawler();
        crawler.addListener(new IteratorRule());
        crawler.addListener(new AsciiTree());
        crawler.run(cursor);


    }

}
