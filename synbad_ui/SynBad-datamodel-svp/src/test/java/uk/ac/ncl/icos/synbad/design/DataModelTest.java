/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.design;

import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import org.junit.Test;
import uk.ac.ncl.icos.datamodel.workspace.objects.IPort;
import uk.ac.ncl.icos.datamodel.workspace.objects.IWire;
import uk.ac.ncl.icos.datamodel.workspace.api.SBWorkspace;
import uk.ac.ncl.icos.synbad.event.SBSubscriber;
import uk.ac.ncl.icos.datamodel.events.api.WorkspaceEvent;
import uk.ac.ncl.icos.datamodel.workspace.impl.EntityInstance;
import uk.ac.ncl.icos.datamodel.workspace.impl.PortRecord;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.datadefinition.types.EntityType;
import uk.ac.ncl.icos.model.VModel;
import uk.ac.ncl.icos.workspace.factories.ExampleFactory;

/**
 *
 * @author owengilfellon
 */
public class DataModelTest {

    @Test
    public void testWorkspace() {
        
        VModel m = ExampleFactory.setupTemplateModel();
        SBWorkspace workspace = m.getWorkspace();
        
        
        
        SBSubscriber handler = new SBSubscriber() {

            @Override
            public void onMessage(WorkspaceEvent e) {
                System.out.println(
                    e.getClass().getSimpleName() + " : " +
                    e.getEventType() + " : " + 
                    e.getSource().getName() + " : " + 
                    e.getObject());
            }
        };

        workspace.getDispatcher().registerHandler(
           
           // workspace.getIdentity(),
            WorkspaceEvent.class,
            handler
        );
        
       // workspace.getDispatcher().addFilter( workspace.getEntity(e -> e.getName().equals("ExampleSystem")).getIdentity(), handler);
     
        assert(workspace.getEntity(e -> e.hasAnnotation(SynBadTerms.Annotatable.entitytype, EntityType.Module.getUri())) != null);
        assert(workspace.getEntity(e -> e.hasAnnotation(SynBadTerms.Annotatable.entitytype, EntityType.Component.getUri())) != null);
        assert(workspace.getEntity(e -> e.hasAnnotation(SynBadTerms.Annotatable.entitytype, EntityType.Interaction.getUri())) != null);
        
        workspace.getEntities().stream().forEach(e -> {

            assert(workspace.containsEntity(e.getIdentity()));
            
            // Test search for ports
            
            Collection<IPort> ports = e.getPorts();
            Collection<IPort> portQuery = workspace.getPorts().stream().parallel()
                    .filter(p -> p.getOwner() == e)
                    .collect(Collectors.toSet());
            
            assert(ports.containsAll(portQuery));
            assert(portQuery.containsAll(ports));
            
            // Test contains
            
            portQuery.stream().parallel().forEach( p -> {
                assert(workspace.containsPort(p.getIdentity()));});  
            
            // Test Port Instances

            /**/
            // Test search for instances
            
            Collection<EntityInstance> instances = e.getChildren();
            Collection<EntityInstance> instanceQuery = workspace.getInstances().stream().parallel()
                    .filter(i -> i.getParent() == e)
                    .collect(Collectors.toSet());
            
            assert(instances.containsAll(instanceQuery));
            assert(instanceQuery.containsAll(instances));
            
             // Test contains
            
            instanceQuery.stream().forEach( p -> {
                assert(workspace.containsInstance(p.getIdentity())); });   
            
            // Test search for wires
            
            Collection<IWire> wires = e.getWires();
            Collection<IWire> wireQuery = workspace.getWires().stream().parallel()
                    .filter(w -> w.getFrom().getOwner().getParent() == e)
                    .collect(Collectors.toSet());
            
            assert(wires.containsAll(wireQuery));
            assert(wireQuery.containsAll(wires));
            
            // Test contains
            
            wireQuery.stream().forEach( p -> {
                assert(workspace.containsWire(p.getIdentity())); });  
            
            
        });
/*        
        workspace.getDispatcher().registerHandler(WorkspaceEvent.PortEvent.class, new IWorkspaceHandler() {

            @Override
            public void onMessage(WorkspaceEvent e) {
                System.out.println(e.getEventType() + " : " + e.getSource().getName() + " : " + e.getObject());
            }
        });
  */    
        System.out.println("----------");
        
        while(workspace.getCommander().hasUndo()) {
            workspace.getCommander().undo();
        }
        
        System.out.println("----------");
        
        while(workspace.getCommander().hasRedo()) {
            workspace.getCommander().redo();
        }  /**/
        
         System.out.println("----------");

    }

}
