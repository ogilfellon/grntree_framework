/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.parsers;

import java.io.FileNotFoundException;
import java.io.IOException;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLStreamException;
import uk.ac.ncl.icos.datamodel.factories.persistence.SbolParser;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.netbeans.junit.MockServices;
import org.sbolstandard.core2.ComponentDefinition;
import org.sbolstandard.core2.ModuleDefinition;
import org.sbolstandard.core2.SBOLDocument;
import org.sbolstandard.core2.SBOLValidationException;
import org.sbolstandard.core2.SBOLWriter;
import uk.ac.ncl.icos.datamodel.workspace.objects.SynBadPObject;
import uk.ac.ncl.icos.datamodel.workspace.objects.IPort;
import uk.ac.ncl.icos.datamodel.workspace.objects.IWire;
import uk.ac.ncl.icos.datamodel.workspace.api.SBWorkspace;
import uk.ac.ncl.icos.datamodel.workspace.impl.EntityInstance;
import uk.ac.ncl.icos.synbad.datadefinition.IDataDefinitionProvider;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.datadefinition.UriHelper;
import uk.ac.ncl.icos.synbad.datadefinition.types.EntityType;
import uk.ac.ncl.icos.synbad.signalgraph.impl.SbolTypeProvider;
import uk.ac.ncl.icos.model.VModel;
import uk.ac.ncl.icos.workspace.factories.ExampleFactory;
import uk.ac.ncl.intbio.core.io.CoreIoException;

/**
 *
 * @author owengilfellon
 */
public class SbolParserTest {
    
    private final VModel model;
    private final SBWorkspace originalWorkspace;
    private final SBOLDocument document;
    private final SBWorkspace importedDocument;
    
    public SbolParserTest() throws SBOLValidationException, XMLStreamException, FactoryConfigurationError, CoreIoException {
        MockServices.setServices(IDataDefinitionProvider.DefaultDefinitionProvider.class, SbolTypeProvider.class);
        model = ExampleFactory.setupTemplateModel();
        originalWorkspace = model.getWorkspace();
        document = SbolParser.write(model.getWorkspace());
        // SBOLWriter.write(document, System.out);
        importedDocument = SbolParser.read(document, UriHelper.synbad.getPrefix(), "sbolTest", "1.0");
    }

    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
      @Test
    public void testDocument(){
      //  document.getGenericTopLevels().iterator().next().
    }
    


    @Test
    public void testImport() throws SBOLValidationException, XMLStreamException, FactoryConfigurationError, CoreIoException {

        int originalPortDefinitions = 0;
        int originalPortInstances = 0;
        int originalWires = 0;
        int importedPortDefinitions = 0;
        int importedPortInstances = 0;
        int importedWires = 0;
        
        for(SynBadPObject entity : originalWorkspace.getEntities()) {
            
            for(IWire wire : entity.getWires()) {
                originalWires++;
            }
            
            for(IPort port : entity.getPorts()) {
                originalPortDefinitions++;
            }
            
            for(EntityInstance instance : entity.getChildren()) {
                originalPortInstances += instance.getPorts().size();
            }
        }
        
        for(SynBadPObject entity : importedDocument.getEntities()) {
            
            for(IWire wire : entity.getWires()) {
                importedWires++;
            }
            
            for(IPort port : entity.getPorts()) {
                importedPortDefinitions++;
            }
            
            for(EntityInstance instance : entity.getChildren()) {
                importedPortInstances += instance.getPorts().size();
            }
            
            if(entity.hasAnnotation(SynBadTerms.Annotatable.entitytype, EntityType.Module.getUri())) {           
                ModuleDefinition definition = document.getModuleDefinition(entity.getIdentity());
                assert(definition != null);
                for(EntityInstance instance : entity.getChildren()) {
                    if (instance.getValue().hasAnnotation(SynBadTerms.Annotatable.entitytype, EntityType.Module.getUri())) {
                        assert(definition.getModule(instance.getDisplayID()) != null);
                        assert(definition.getModule(instance.getIdentity()) != null);
                    } else if(instance.getValue().hasAnnotation(SynBadTerms.Annotatable.entitytype, EntityType.Component.getUri())) {
                        assert(definition.getFunctionalComponent(instance.getDisplayID()) != null);
                        assert(definition.getFunctionalComponent(instance.getIdentity()) != null);
                    } else if (instance.getValue().hasAnnotation(SynBadTerms.Annotatable.entitytype, EntityType.Interaction.getUri())) {
                        assert(definition.getInteraction(instance.getDisplayID()) != null);
                        assert(definition.getInteraction(instance.getIdentity()) != null);
                    }                  
                }
            } else if(entity.hasAnnotation(SynBadTerms.Annotatable.entitytype, EntityType.Component.getUri())) {
                ComponentDefinition definition = document.getComponentDefinition(entity.getIdentity());
                assert(definition != null);
                for(EntityInstance instance : entity.getChildren()) {
                    assert(definition.getComponent(instance.getDisplayID()) != null);
                    assert(definition.getComponent(instance.getIdentity()) != null);
                }
            }
        }
        
        assert(originalPortDefinitions == importedPortDefinitions);
        assert(originalPortInstances == importedPortInstances);
        assert(originalWires == importedWires);
    }
    
    @Test
    public void testImportedEqualsOriginal() {
        
        for(SynBadPObject originalEntity : originalWorkspace.getEntities()) {
            assert(importedDocument.containsEntity(originalEntity.getIdentity()));
            assert(importedDocument.containsEntity(originalEntity.getIdentity()));
            SynBadPObject importedEntity = importedDocument.getEntity(originalEntity.getIdentity());
            assert(importedEntity != null);
            assert(importedEntity.getEntityType() == originalEntity.getEntityType());
            assert(importedEntity.getDescription().equals(originalEntity.getDescription()));
            assert(importedEntity.getDisplayID().equals(originalEntity.getDisplayID()));
            assert(importedEntity.getIdentity().equals(originalEntity.getIdentity()));
            assert(importedEntity.getName().equals(originalEntity.getName()));
            assert(importedEntity.getPersistentIdentity().equals(originalEntity.getPersistentIdentity()));
        }    
    }
    
    @Test
    public void testOriginalEqualsImported() {
        for(SynBadPObject importedEntity : importedDocument.getEntities()) {
            assert(originalWorkspace.containsEntity(importedEntity.getIdentity()));
            assert(originalWorkspace.containsEntity(importedEntity.getIdentity()));
            SynBadPObject originalEntity = originalWorkspace.getEntity(importedEntity.getIdentity());
            assert(originalEntity != null);            
            assert(importedEntity.getEntityType() == originalEntity.getEntityType());
            assert(importedEntity.getDescription().equals(originalEntity.getDescription()));
            assert(importedEntity.getDisplayID().equals(originalEntity.getDisplayID()));
            assert(importedEntity.getIdentity().equals(originalEntity.getIdentity()));
            assert(importedEntity.getName().equals(originalEntity.getName()));
            assert(importedEntity.getPersistentIdentity().equals(originalEntity.getPersistentIdentity()));
        }
    }
    
    @Test
    public void testOutputXml() throws XMLStreamException, FactoryConfigurationError, CoreIoException, IOException {
         SBOLWriter.write(document, "ExampleSystem.xml");
    }
}
