/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.datamodel.parser;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.Timer;
import uk.ac.ncl.icos.synbad.parsers.SVPParser;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.netbeans.junit.MockServices;
import org.openide.util.Exceptions;
import org.sbml.jsbml.SBMLDocument;
import org.sbolstandard.core2.SBOLDocument;
import uk.ac.ncl.icos.datamodel.workspace.objects.SynBadPObject;
import uk.ac.ncl.icos.model.VModel;
import uk.ac.ncl.icos.synbad.datadefinition.IDataDefinitionProvider;
import uk.ac.ncl.icos.synbad.datadefinition.UriHelper;
import uk.ac.ncl.icos.datamodel.workspace.api.SBWorkspace;
import uk.ac.ncl.icos.datamodel.workspace.impl.Identity;
import uk.ac.ncl.icos.datamodel.workspace.impl.Workspace;
import uk.ac.ncl.icos.svpmanager.SVPManager;
import uk.ac.ncl.icos.synbad.parsers.PartBundle;
import uk.ac.ncl.icos.synbad.signalgraph.impl.SbolTypeProvider;
import uk.ac.ncl.icos.synbad.signalgraph.impl.SynBadPortType;
import uk.ac.ncl.icos.synbad.visualisation.GraphStreamPanel;
import uk.ac.ncl.icos.view.api.View;
import uk.ac.ncl.icos.view.api.View;
import uk.ac.ncl.icos.view.api.ViewCrawler;
import uk.ac.ncl.icos.view.impl.CrawlerDebugger;
import uk.ac.ncl.icos.view.impl.VModelImpl;
import uk.ac.ncl.intbio.virtualparts.entity.Interaction;
import uk.ac.ncl.intbio.virtualparts.entity.Part;
import uk.ac.ncl.icos.synbad.svp.rdf.Svp;
import uk.ac.ncl.synbad.svp.debugger.SvpDebugger;

/**
 *
 * @author owengilfellon
 */
public class SVPParserTest {
    
    private final SBWorkspace workspace;
    private VModel manager;
    private final SVPParser parser;
    
    public SVPParserTest() {
        MockServices.setServices(IDataDefinitionProvider.DefaultDefinitionProvider.class,SbolTypeProvider.class);
        workspace = new Workspace(UriHelper.synbad.getPrefix(), "testWorkspace", "1.0");
        parser = new SVPParser(workspace);
    }
    
    public static void main(String[] args) {
        SVPParserTest test = new SVPParserTest();
        try {
            test.testConvert_Part();
        } catch (Exception ex) {
            Exceptions.printStackTrace(ex);
        }
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

   

    
    /**
     * Test of convert method, of class SVPParser.
     */
    @Test
    public void testConvert_Part() throws Exception {

        SVPManager m = SVPManager.getSVPManager();
        SynBadPObject tu = workspace.createEntity(new Identity(UriHelper.synbad.getNamespaceURI(), "tu", "1.0"));
        tu.setName("TU");
        
        Part p1 = m.getConstPromoter();
        Part r1 =  m.getRBS();
        
        Part c1 = m.getPart("SpaK");
        Part r2 = m.getRBS();
        Part c2 = m.getPart("SpaR");
        Part t = m.getTerminator();
 
        Svp inducible = parser.read(p1);
        Svp rbs1 = parser.read(r1);
        Svp spak = parser.read(c1);
        Svp rbs2 = parser.read(r2);
        Svp spar = parser.read(c2);
        Svp terminator = parser.read(t);
        
        //PartBundle pb_p1 = parser.write(inducible); 
        PartBundle pb_r1 = parser.write(rbs1);
        //PartBundle pb_c1 = parser.write(spak);
        // * PartBundle pb_r2 = parser.write(rbs2);
       // PartBundle pb_c2 = parser.write(spar);
        
        
        /*List<Interaction> i = m.getInternalEvents(r1);
        SBMLDocument doc = m.createPartDocument(r1, i);
        SvpDebugger.debugBundles(Arrays.asList(
               new PartBundle(r1, i, doc),
               pb_r1), System.out);
*/
        
        
      
        tu.addChildren(inducible, 
       rbs1, spak, rbs2, spar, terminator);
        tu.wireChildren(
                tu.getChild(i -> i.getValue() == inducible),
                tu.getChild(i -> i.getValue() == rbs1),
                SynBadPortType.PoPS,
                null);
        tu.wireChildren(
                tu.getChild(i -> i.getValue() == inducible),
                tu.getChild(i -> i.getValue() == rbs2),
                SynBadPortType.PoPS,
                null);
        tu.wireChildren(
                tu.getChild(i -> i.getValue() == rbs1),
                tu.getChild(i -> i.getValue() == spak),
                SynBadPortType.RiPS,
                null);
        tu.wireChildren(
                tu.getChild(i -> i.getValue() == rbs2),
                tu.getChild(i -> i.getValue() == spar),
                SynBadPortType.RiPS,
                null);  
        /**/

      SvpDebugger.debugEntities(workspace.getEntities(), System.out);


       
        
       /*
         
        VModel dataModel = new VModelImpl(tu, workspace);
        View<VGraph> modelView = dataModel.getMainView();
        ViewCrawler crawler = new ViewCrawler();
        crawler.addListener(new CrawlerDebugger());
        crawler.run(modelView.getView().createCursor());
        
        renderModel(dataModel);     
        */
    }
    
    public void renderModel(VModel model) {
        JFrame frame = new JFrame("Visualisation");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(new GridBagLayout());
        
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.FIRST_LINE_START;
        c.fill = GridBagConstraints.BOTH;
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 1;
        c.weighty = 1;
        View view = model.getCurrentView();
        ViewCrawler crawler = new ViewCrawler();
        //crawler.addListener(new CrawlerDebugger());
        final GraphStreamPanel panel = new GraphStreamPanel(crawler, view);
        frame.getContentPane().add(panel, c);
        frame.pack();
        frame.setVisible(true);
        
        int delay = 10; //milliseconds
        ActionListener taskPerformer = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                panel.repaint();
            }
        };
        new Timer(delay, taskPerformer).start();
        

    }
}
