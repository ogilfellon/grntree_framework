/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.design;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.Timer;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.netbeans.junit.MockServices;

import uk.ac.ncl.icos.synbad.datadefinition.IDataDefinitionProvider;
import uk.ac.ncl.icos.synbad.visualisation.GraphStreamPanel;
import uk.ac.ncl.icos.model.VModel;
import uk.ac.ncl.icos.view.api.ViewCrawler;
import uk.ac.ncl.icos.workspace.factories.ExampleFactory;

/**
 *
 * @author owengilfellon
 */
public class GraphStreamPanelTest {


    private GraphStreamPanel panel;
    private final VModel model;

    public static void main(String[] args) {
        GraphStreamPanelTest test = new GraphStreamPanelTest();
        test.testGraphVisualisation();
    }

    public GraphStreamPanelTest() {
        MockServices.setServices(IDataDefinitionProvider.DefaultDefinitionProvider.class);
        model = ExampleFactory.setupTemplateModel();
    }

    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    @Test
    public void testParser() {
   
    }

    @Test
    public void testGraphVisualisation() {
        JFrame frame = new JFrame("Visualisation");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(new GridBagLayout());
        
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.FIRST_LINE_START;
        c.fill = GridBagConstraints.BOTH;
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 1;
        c.weighty = 1;

        ViewCrawler crawler = new ViewCrawler();
        //crawler.addListener(new SVPCrawlerRule());
        
        panel = new GraphStreamPanel(crawler, model.getCurrentView());
        frame.getContentPane().add(panel, c);
        frame.pack();
        frame.setVisible(true);
        
        /**/
        
         
         
        int delay = 50; //milliseconds
    
        new Timer(delay, (ActionEvent evt) -> { panel.repaint(); }).start();
            /**/
        ActionListener undoer = new ActionListener() {
            
            private boolean undo = model.getWorkspace().getCommander().hasUndo();
            
            @Override
            public void actionPerformed(ActionEvent evt) {
               
                if(undo) {

                    model.getWorkspace().getCommander().undo();
                    
                    if(!model.getWorkspace().getCommander().hasUndo())
                        undo = false;
                    
                } else {
                    
                    model.getWorkspace().getCommander().redo();

                    if(!model.getWorkspace().getCommander().hasRedo())
                        undo = true;   
                }
            }
        };
        new Timer(10, undoer).start();
        

    }
    
}
