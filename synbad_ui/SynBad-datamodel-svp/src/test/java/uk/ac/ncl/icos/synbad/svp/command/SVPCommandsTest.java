/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.command;

import java.util.Arrays;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import uk.ac.ncl.icos.datamodel.workspace.objects.SynBadPObject;
import uk.ac.ncl.icos.datamodel.workspace.api.SBWorkspace;
import uk.ac.ncl.icos.synbad.event.SBSubscriber;
import uk.ac.ncl.icos.datamodel.actions.ReturnableAction;
import uk.ac.ncl.icos.datamodel.actions.WorkspaceAction;
import uk.ac.ncl.icos.datamodel.events.api.WorkspaceEvent;
import uk.ac.ncl.icos.datamodel.workspace.impl.Identity;
import uk.ac.ncl.icos.datamodel.workspace.impl.Workspace;
import uk.ac.ncl.icos.synbad.datadefinition.UriHelper;

/**
 *
 * @author owengilfellon
 */
public class SVPCommandsTest {
    
    public SVPCommandsTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testSVPCommands() {
        
        SBWorkspace workspace = new Workspace(UriHelper.synbad.getPrefix(), "CommandTests", "1.0");
        workspace.getDispatcher().registerHandler(WorkspaceEvent.class, new SBSubscriber.MessageLogger());

        // Create commands
        
        List<ReturnableAction<SynBadPObject>> actions = Arrays.asList(        
            new SVPCommands.CreatePromoter(workspace, new Identity(UriHelper.synbad.getPrefix(), "PspaRK", "1.0")),
            new SVPCommands.CreateRBS(workspace, new Identity(UriHelper.synbad.getPrefix(), "RBS_SpaK", "1.0")),
            new SVPCommands.CreateCDS(workspace, new Identity(UriHelper.synbad.getPrefix(), "SpaK", "1.0"), 0.5, 0.5),
            new SVPCommands.CreateRBS(workspace, new Identity(UriHelper.synbad.getPrefix(), "RBS_SpaR", "1.0")),
            new SVPCommands.CreateCDS(workspace, new Identity(UriHelper.synbad.getPrefix(), "SpaR", "1.0"), 0.5, 0.5),
            new SVPCommands.CreateTerminator(workspace, new Identity(UriHelper.synbad.getPrefix(), "Terminator", "1.0")));

        
      
        // Perform commands
        
        for(ReturnableAction<SynBadPObject> command : actions) {
            workspace.getCommander().perform(command);
            workspace.getCommander().perform(new WorkspaceAction.AddEntity(workspace, command.getValue()));
        }
        
        // Undo and redo
        
        while(workspace.getCommander().hasUndo()) {
            workspace.getCommander().undo();
        }
        
        while(workspace.getCommander().hasRedo()) {
            workspace.getCommander().redo();
        }
        
        System.out.println();
    }
    
}
