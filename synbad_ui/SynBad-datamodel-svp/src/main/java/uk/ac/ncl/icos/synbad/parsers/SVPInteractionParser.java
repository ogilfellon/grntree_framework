/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.parsers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.sbolstandard.core2.Annotation;
import uk.ac.ncl.icos.synbad.datadefinition.IDataDefinitionManager;
import uk.ac.ncl.icos.synbad.datadefinition.types.InteractionRole;
import uk.ac.ncl.icos.synbad.datadefinition.types.ModuleRole;
import uk.ac.ncl.icos.datamodel.workspace.objects.SynBadPObject;
import uk.ac.ncl.icos.datamodel.workspace.objects.IPort;
import uk.ac.ncl.icos.datamodel.workspace.api.SBWorkspace;
import uk.ac.ncl.icos.datamodel.workspace.impl.EntityInstance;
import uk.ac.ncl.icos.datamodel.workspace.impl.Identity;
import uk.ac.ncl.icos.datamodel.workspace.impl.PortDirection;
import uk.ac.ncl.icos.datamodel.workspace.impl.PortRecord;
import uk.ac.ncl.icos.datamodel.workspace.impl.PortType;
import uk.ac.ncl.icos.svpmanager.SVPManager;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.datadefinition.UriHelper;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentType;
import uk.ac.ncl.icos.synbad.datadefinition.types.EntityType;
import uk.ac.ncl.icos.synbad.datadefinition.types.Role;
import uk.ac.ncl.icos.synbad.signalgraph.impl.SynBadPortState;
import uk.ac.ncl.icos.synbad.signalgraph.impl.SynBadPortType;
import uk.ac.ncl.icos.workspace.factories.AnnotationFactory;
import uk.ac.ncl.icos.workspace.factories.ComponentFactory;
import uk.ac.ncl.intbio.core.datatree.NamespaceBinding;
import uk.ac.ncl.intbio.virtualparts.entity.Constraint;
import uk.ac.ncl.intbio.virtualparts.entity.Interaction;
import uk.ac.ncl.intbio.virtualparts.entity.InteractionPartDetail;
import uk.ac.ncl.intbio.virtualparts.entity.Parameter;
import uk.ac.ncl.intbio.virtualparts.entity.Part;
import uk.ac.ncl.synbad.svp.Component;
import uk.ac.ncl.icos.synbad.svp.rdf.SvpInteraction;
import uk.ac.ncl.synbad.svp.debugger.SvpDebugger;

/**
 *
 * @author owengilfellon
 */
public class SVPInteractionParser {

    private final SBWorkspace workspace;
    private final NamespaceBinding namespace = UriHelper.vpr;
    private final IDataDefinitionManager definitionManager = new IDataDefinitionManager.DefaultDataDefinitionManager();

    
    public SVPInteractionParser(SBWorkspace workspace)
    {
        this.workspace = workspace;
    }

    public SvpInteraction read(Interaction interaction)
    {

        //SvpDebugger.debugInteraction(interaction, System.out);
        
        // create interaction
        
        InteractionRole role = definitionManager.getDefinition(InteractionRole.class, interaction.getInteractionType());
        SvpInteraction svpInteraction = new SvpInteraction(workspace, new Identity(UriHelper.vpr.getNamespaceURI(), interaction.getName(), "1.0"), role);
        workspace.addEntity(svpInteraction);
        
        // populate with properties
        
        getInteractionProperties(svpInteraction, interaction);
        
        // add interaction entities
        
        addInteractions(svpInteraction, interaction);
        
        // add component entities
        
        addComponents(svpInteraction, interaction);

        // wire children
        
        wireChildren(svpInteraction);
        
        // export proxies
        
     
        for(EntityInstance entity : svpInteraction.getChildren()) {
            for(PortRecord port : entity.getPorts()) {
                if(!port.isWired()) {
                    svpInteraction.createPort(port.getValue().getIdentity(), true);
                }
            }
        }


        return svpInteraction;
    }
    
    private void wireChildren(SvpInteraction svpInteraction) {
        Set<PortRecord> outports = new HashSet<>();
        Set<PortRecord> inports = new HashSet<>();
        
        for(EntityInstance entity : svpInteraction.getChildren()) {
            for(PortRecord port : entity.getPorts()) {
                if(port.getValue().getPortDirection() == PortDirection.IN) {
                    inports.add(port);
                } else {
                    outports.add(port);
                }
            }
        }
  
        for(PortRecord from : outports) {
            for(PortRecord to : inports) {
                if(from.getValue().isConnectable(to.getValue()) && to.getValue().isConnectable(from.getValue()) &&
                        from.getOwner() != to.getOwner()) {
                    svpInteraction.wireChildren(from, to);
                }
            }
        }

    }

    public Interaction write(SvpInteraction source) {

        Interaction interaction = new Interaction();
        
        
        
        createInteractionProperties(source, interaction);
        
        return interaction;

    }
    
    private void addInteractions(SvpInteraction parent, Interaction interaction) {
        
        SynBadPObject i = workspace.createEntity(new Identity(UriHelper.vpr.getNamespaceURI(), parent.getDisplayID(), interaction.getName(), "1.0"));
        i.addAnnotation(SynBadTerms.Annotatable.entitytype, 
            new Annotation(SynBadTerms.Annotatable.entitytype,  EntityType.Interaction.getUri()));
        i.addAnnotation(SynBadTerms.Annotatable.role, 
            new Annotation(SynBadTerms.Annotatable.role, definitionManager.getDefinition(InteractionRole.class, interaction.getInteractionType()).getUri()));

        // add Ports
        
        for(InteractionPartDetail ipd : interaction.getPartDetails()) {
            PortType type = getType(ipd.getMathName());
            if(type!=null)
                i.createPort(true, PortDirection.fromString(ipd.getInteractionRole()),type , SynBadPortState.valueOf(ipd.getPartForm()), null);
        }
        
        for(Parameter p : interaction.getParameters()) {
            PortType type = getType(p.getParameterType());
            if(type!=null)
                i.createPort(true, PortDirection.fromString(p.getScope()), type , null, null);
        }
        
        parent.addChild(i);
        
    }
    
    private Map<String, Component> addComponents(SvpInteraction interaction, Interaction internalInteraction) {

        Map<String, Component> components = new HashMap<>();
        
        if (internalInteraction.getPartDetails() == null)
            return components;
        
        
        Map<String, Component> inputs = new HashMap<>();
        Map<String, Component> outputs = new HashMap<>();

        for(InteractionPartDetail detail : internalInteraction.getPartDetails()) {
            String id = detail.getPartForm().equals("Phosphorylated") ?
                    detail.getPartName() + "_Phosphorylated" :
                    detail.getPartName();

            Part part = SVPManager.getSVPManager().getPart(detail.getPartName());
            String form = detail.getPartForm();
            
            ComponentType partType = definitionManager.getDefinition(ComponentType.class, part.getType());
            ComponentType type = partType == null ? ComponentType.Protein : partType;
            Component component = null;

            if(!components.containsKey(id)) {
                switch(type) {
                    case DNA:
                        component = ComponentFactory.getDna(workspace, namespace.getNamespaceURI(), part.getName(), definitionManager.getDefinition(Role.class, part.getType()));
                        break;
                    case Protein:
                        component = ComponentFactory.getProtein(workspace, namespace.getNamespaceURI(), part.getName(), form.equals("Phosphorylated") ? SynBadPortState.Phosphorylated : SynBadPortState.Default);
                        break;
                }

                if(component == null)
                    component = ComponentFactory.getSmallMolecule(workspace, namespace.getNamespaceURI(), part.getName());
            }
            
            if(component!=null) {
                if(hasOnlyOutput(part.getName(), SynBadPortState.valueOf(form), internalInteraction.getPartDetails()) &&
                        isProduction(internalInteraction))
                    interaction.addChild(component);
                 components.put(id, component);
            }
               
        } 

        return components;
    }
     
    private void getInteractionProperties(SvpInteraction svpInteraction, Interaction interaction) {


        svpInteraction.addAnnotation(SynBadTerms.Annotatable.role, new Annotation(SynBadTerms.Annotatable.role, ModuleRole.SVPInteraction.getUri()));
        
        if(interaction.getInteractionType() != null)
            svpInteraction.addAnnotation(SynBadTerms.Annotatable.type, new Annotation(SynBadTerms.Annotatable.type,interaction.getInteractionType()));
        
        if(interaction.getName() != null)
            svpInteraction.setName(interaction.getName());
        
        if(interaction.getDescription() != null)
            svpInteraction.setDescription(interaction.getDescription());
        
        if(interaction.getMathName() != null)
            svpInteraction.addAnnotation(SynBadTerms.VPR.mathName, new Annotation(SynBadTerms.VPR.mathName, interaction.getMathName()));
        
        if(interaction.getFreeTextMath() != null)
            svpInteraction.addAnnotation(SynBadTerms.VPR.freeTextMath, new Annotation(SynBadTerms.VPR.freeTextMath, interaction.getFreeTextMath()));

        if(interaction.getIsInternal() != null)
            svpInteraction.addAnnotation(SynBadTerms.VPR.isInternal, new Annotation(SynBadTerms.VPR.isInternal, interaction.getIsInternal()));

        if(interaction.getIsReaction() != null)
            svpInteraction.addAnnotation(SynBadTerms.VPR.isReaction, new Annotation(SynBadTerms.VPR.isReaction, interaction.getIsReaction()));

        if(interaction.getIsReversible() != null)
            svpInteraction.addAnnotation(SynBadTerms.VPR.isReversible, new Annotation(SynBadTerms.VPR.isReversible, interaction.getIsReversible()));

        
        if(interaction.getParts() != null) {
            for(String s :  interaction.getParts()) {
                svpInteraction.addAnnotation(SynBadTerms.VPR.participant, new Annotation(SynBadTerms.VPR.participant, s));
            }
            
        }
        
        if(interaction.getParameters() != null) {
            for(Parameter p : interaction.getParameters()) {

                 Annotation parameter = AnnotationFactory.createParameter(svpInteraction.getIdentity(),
                        p.getName(),
                        p.getParameterType(),
                        p.getValue(),
                        p.getScope(),
                        p.getEvidenceType());
                
                Annotation existing = svpInteraction.getAnnotation(parameter.getQName(), parameter.getNestedIdentity());
                if(existing != null) svpInteraction.removeAnnotation( existing.getQName(), existing);

                svpInteraction.addAnnotation(parameter.getQName(), parameter);
            }
        }

        if(interaction.getPartDetails()!=null) {
            
            for(InteractionPartDetail detail : interaction.getPartDetails()) {

                Annotation partDetail = AnnotationFactory.createInteractionPartDetail(svpInteraction.getIdentity(),
                        detail.getPartName(),
                        detail.getPartForm(),
                        detail.getNumberOfMolecules(),
                        detail.getMathName(),
                        detail.getInteractionRole());
                
                Annotation existing = svpInteraction.getAnnotation(partDetail.getQName(), partDetail.getNestedIdentity());
                if(existing != null) svpInteraction.removeAnnotation( existing.getQName(), existing);

                svpInteraction.addAnnotation(partDetail.getQName(), partDetail);
            }
        } 
    }

     private void createInteractionProperties(SvpInteraction entity, Interaction interaction) {

        interaction.setName(entity.getDisplayID());
        interaction.setDescription(entity.getDescription());
        
        Iterator<Annotation> i = entity.getAnnotations(SynBadTerms.VPR.mathName).iterator();
        if(i.hasNext()) 
            interaction.setMathName(i.next().getStringValue());
    
        i = entity.getAnnotations(SynBadTerms.Annotatable.type).iterator();
        if(i.hasNext()) interaction.setInteractionType(i.next().getStringValue());
        
        i = entity.getAnnotations(SynBadTerms.VPR.freeTextMath).iterator();
        if(i.hasNext()) interaction.setFreeTextMath(i.next().getStringValue());

        i = entity.getAnnotations(SynBadTerms.VPR.isInternal).iterator();
        if(i.hasNext()) interaction.setIsInternal(i.next().getBooleanValue());
        
        i = entity.getAnnotations(SynBadTerms.VPR.isReaction).iterator();
        if(i.hasNext()) interaction.setIsReaction(i.next().getBooleanValue());
        
        i = entity.getAnnotations(SynBadTerms.VPR.isReversible).iterator();
        if(i.hasNext()) interaction.setIsReversible(i.next().getBooleanValue());
        
        if(!entity.getAnnotations(SynBadTerms.VPR.participant).isEmpty()) {
            
            List<String> parts = new ArrayList<>();
            
            entity.getAnnotations(SynBadTerms.VPR.participant).stream().forEach((p) -> {
                parts.add(p.getStringValue());
            });
            
             interaction.setParts(parts);
        }
        
        if(!entity.getAnnotations(SynBadTerms.VPR.parameter).isEmpty()) {
            for(Annotation p : entity.getAnnotations(SynBadTerms.VPR.parameter)) {
                interaction.AddParameter(AnnotationFactory.getParameter(p));
            }
        }
        
        if(!entity.getAnnotations(SynBadTerms.VPR.partDetail).isEmpty()) {
            for(Annotation p : entity.getAnnotations(SynBadTerms.VPR.partDetail)) {
                interaction.AddPartDetail(AnnotationFactory.getInteractionPartDetail(p));
            }
        } else {
            interaction.setPartDetails(new ArrayList<>());
        }
        
        

    }
     
     private boolean hasDirection(String speciesName, SynBadPortState state, Collection<InteractionPartDetail> ipd, PortDirection direction)
     {
         for(InteractionPartDetail pd : ipd) {
             if( pd.getPartName().equals(speciesName) &&
                    state == SynBadPortState.valueOf(pd.getPartForm()) && 
                    direction == PortDirection.fromString(pd.getInteractionRole()))
                return true;
         }
         
         return false;
     }   
     
     private boolean hasOnlyOutput(String speciesName, SynBadPortState state, Collection<InteractionPartDetail> ipd) {
         return hasDirection(speciesName, state, ipd, PortDirection.OUT) && !hasDirection(speciesName, state, ipd, PortDirection.IN);
     }
     
     
     private PortType getType(String string) {
        if(string.contains("PoPS"))
            return SynBadPortType.PoPS;
        if(string.contains("RiPS"))
            return SynBadPortType.RiPS;
        if(string.contains("mRNA"))
            return SynBadPortType.mRNA;
        if(string.contains("Protein") || string.contains("Species"))
            return SynBadPortType.Protein;
        if(string.contains("EnvironmentConstant"))
            return SynBadPortType.SmallMolecule;
        return null;
     }

     
     private boolean isProduction(Interaction interaction) {
        String type = interaction.getInteractionType();
        if(type.contains("Production") || type.contains("Phosphorylation") ||
                type.contains("Formation"))
            return true;
        return false;
     }

}
