/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.workspace.factories;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import uk.ac.ncl.icos.datamodel.workspace.objects.SynBadPObject;
import uk.ac.ncl.icos.datamodel.workspace.objects.IPort;
import uk.ac.ncl.icos.datamodel.workspace.api.SBWorkspace;
import uk.ac.ncl.icos.datamodel.workspace.impl.Identity;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentRole;
import uk.ac.ncl.icos.synbad.svp.rdf.Svp;

/**
 *
 * @author owengilfellon
 */
public class SvpBuilder {
    
    private SBWorkspace workspace;
    private Identity identity;
    private ComponentRole type;
    private final List<SynBadPObject> children = new ArrayList<>();
    private final List<IPort> toproxy = new ArrayList<>();
    
    public void setWorkspace(SBWorkspace workspace) {
        this.workspace = workspace;
    }
    
    public void addInstanceOf(SynBadPObject entity) {
        this.children.add(entity);
    }
    
    public void addProxyOf(IPort port) {
        this.toproxy.add(port);
    }
    
    public void setRole(ComponentRole role) {
        this.type = role;
    }
    
    public void setIdentity(Identity identity) {
        this.identity = identity;
    }
    
    public Svp build() {
        Svp svp = new Svp(workspace, identity, type);
        
        for(SynBadPObject entity : children) {
            svp.addChild(entity);
        }
        
        for(IPort port : toproxy) {
            svp.createPort(port.getIdentity(), true);
        }
        
        return svp;
    }
    
}
