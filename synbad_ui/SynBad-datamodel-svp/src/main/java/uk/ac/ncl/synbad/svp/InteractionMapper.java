package uk.ac.ncl.synbad.svp;

import uk.ac.ncl.icos.synbad.svp.rdf.Svp;
import uk.ac.ncl.icos.synbad.svp.rdf.SvpInteraction;
import uk.ac.ncl.icos.svpmanager.SVPManager;

import java.io.Serializable;
import java.util.*;
import uk.ac.ncl.icos.datamodel.workspace.objects.SynBadPObject;
import uk.ac.ncl.icos.datamodel.workspace.impl.EntityInstance;

/**
 * Created by owengilfellon on 27/02/2014.
 */
public class InteractionMapper {

    private final SVPManager m = SVPManager.getSVPManager();
    private final Map<SvpWrapper, SvpRecord> nodeRecords;

    public InteractionMapper()
    {
        nodeRecords = new HashMap<>();
    }

    /**
     * Returns true if the part represented by the leaf node is in the tree
     * @param node
     * @return 
     */
    public boolean containsNode(Svp node)
    {
        return nodeRecords.containsKey(new SvpWrapper(node));
    }

    /**
     * Returns all interactions within the tree
     * @return 
     */
    public Set<SvpInteraction> getInteractions() {
        Set<SvpInteraction> interactions = new HashSet<>();
        for(SvpWrapper nw : nodeRecords.keySet())  {
            Set<InteractionRecord> interactionRecords = nodeRecords.get(nw).interactions;
            for(InteractionRecord record : interactionRecords ) {
                interactions.add(record.interaction);
            }
        }
        return interactions;
    }

    /**
     * Returns the interactions within the tree involving the supplied leaf node.
     * @param node
     * @return 
     */
    public Set<SvpInteraction> getInteractions(Svp node){
        Set<SvpInteraction> interactions = new HashSet<>();
        Set<InteractionRecord> interactionRecords = nodeRecords.get(new SvpWrapper(node)).interactions;
        for(InteractionRecord ir:interactionRecords) {
            interactions.add(ir.interaction);
        }
        return interactions;
    }

    /**
     * Returns the parts in the tree associated with a given interaction
     * @param interaction
     * @return 
     */
    public List<SynBadPObject> getNodes(SvpInteraction interaction) {
        List<SynBadPObject> parts = new ArrayList<>();
        InteractionRecord ir = new InteractionRecord(interaction);
        for(SvpWrapper nw : nodeRecords.keySet())  {
            if(nodeRecords.get(nw).interactions.contains(ir)) {
                parts.add(nw.node);
            }
        }
        return parts;
    }
    
    public List<SynBadPObject> getNodes(String name) {
        List<SynBadPObject> parts = new ArrayList<>();
        for(SvpWrapper nw : nodeRecords.keySet())  {
            if(nw.node.getDisplayID().equals(name)) {
                for(SynBadPObject n : nodeRecords.get(nw).parents) {
                    for(EntityInstance child : n.getChildren()) {
                        if(child.getValue() instanceof Svp && child.getDisplayID().equalsIgnoreCase(name)) {
                            parts.add(child.getValue());
                        }
                    }
                }
            }
        }
        return parts;
    }
    
    /**
     * Returns the leaf nodes that interact with the given leaf node
     * @param node
     * @return 
     */
    public List<SynBadPObject> getInteractingNodes(Svp node)
    {
        SvpWrapper nw = new SvpWrapper(node);

        List<SynBadPObject> nodes = new ArrayList<>();
        if(!nodeRecords.containsKey(nw)) {
            return nodes;
        }

        Set<String> identifiers = new HashSet<>();
        Set<InteractionRecord> interactions = nodeRecords.get(nw).interactions;

        for(InteractionRecord interaction:interactions) {
        /*    for(String partName:interaction.interaction.getParts()) {
                if (!partName.equals(node.getDisplayID())) {
                    identifiers.add(partName);
                }
            }
       */ }

        for(SvpWrapper n:nodeRecords.keySet()) {
            if(identifiers.contains(n.node.getDisplayID())) {
                nodes.add(n.node);
            }
        }

        return nodes;
    }
    
    
    public boolean addInteraction(SvpInteraction interaction)
    {
     return false;
    }
    
    public boolean removeInteraction(SvpInteraction interaction) 
    {
        return false;
    }


    private void removeSvp(SvpWrapper node)
    {
        SvpRecord nr = nodeRecords.get(node);
       // nr.parents.remove(node.node.getParent());

        if(nr.parents.isEmpty()) {

            nodeRecords.remove(node);
        }
    }

    private void addSvp(SvpWrapper node)
    {
        // If the node doesn't exist in records, add it

        if(!nodeRecords.containsKey(node)) {
           SvpRecord nr =  new SvpRecord();
           nodeRecords.put(node, nr);
        }
    }
    
    void detectInteractions(SvpWrapper node) {

    }

    public void debug() {

        String format = "%-20s %-13s %-10s %n";
        String divider = "--------------------------------------------------";
        String subformat = "%-30s %30s %n";
        System.out.println(divider);
        System.out.printf(format, "Part", "Interactions", "Instances");
        System.out.println(divider);

        for(SvpWrapper n:nodeRecords.keySet())  {
            System.out.printf(format, n, nodeRecords.get(n).interactions.size(), nodeRecords.get(n).parents.size());
        }
    }

    
    private class SvpRecord {
        private final Set<InteractionRecord> interactions = new HashSet<>();
        private final List<SynBadPObject> parents = new ArrayList<>();
    }

    private class InteractionRecord {

        private final SvpInteraction interaction;

        public InteractionRecord(SvpInteraction interaction) {
            this.interaction = interaction;
        }

        @Override
        public boolean equals(Object obj) {
            if(obj == null)
                return false;
            if(obj == this)
                return true;
            if(!(obj instanceof InteractionRecord))
                return false;

            InteractionRecord ir = (InteractionRecord) obj;

            return ir.interaction.getDisplayID().equals(this.interaction.getDisplayID());
        }


        @Override
        public int hashCode() {
            return interaction.getIdentity().hashCode();
        }
    }

    private class SvpWrapper implements Serializable {

        private final Svp node;
        
        public SvpWrapper(Svp node)
        {
            this.node = node;
        }

        @Override
        public boolean equals(Object obj)
        {
            if(obj == null)
                return false;
            if(obj == this)
                return true;
            if(!(obj instanceof SvpWrapper))
                return false;
            Svp p = ((Svp) obj);

            return p.getIdentity().equals(node.getIdentity());
        }

        @Override
        public int hashCode() {
            return node.getIdentity().hashCode();
        }

        @Override
        public String toString() {
            return node.toString();
        }
    }

}
