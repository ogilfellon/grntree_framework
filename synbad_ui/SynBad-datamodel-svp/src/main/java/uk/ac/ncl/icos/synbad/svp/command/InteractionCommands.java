/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.command;

import org.sbolstandard.core2.Annotation;
import uk.ac.ncl.icos.synbad.datadefinition.types.EntityType;
import uk.ac.ncl.icos.synbad.datadefinition.types.InteractionRole;
import uk.ac.ncl.icos.synbad.signalgraph.impl.SynBadPortType;
import uk.ac.ncl.icos.datamodel.workspace.objects.SynBadPObject;
import uk.ac.ncl.icos.datamodel.workspace.api.SBWorkspace;
import uk.ac.ncl.icos.datamodel.actions.ReturnableAction;
import uk.ac.ncl.icos.datamodel.actions.WorkspaceAction;
import uk.ac.ncl.icos.datamodel.workspace.objects.SynBadPObject;
import uk.ac.ncl.icos.datamodel.workspace.impl.Identity;
import uk.ac.ncl.icos.datamodel.workspace.impl.PortDirection;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;

/**
 *
 * @author owengilfellon
 */
public class InteractionCommands {

    public static class CreateMrnaProduction implements ReturnableAction<SynBadPObject> {

        private final SynBadPObject entity;
        private final WorkspaceAction setup;
        
        public CreateMrnaProduction(SBWorkspace workspace, Identity identity) {
            this.entity = new SynBadPObject(workspace, identity);
            this.setup = new WorkspaceAction.Macro(
                new WorkspaceAction.AddEntity(workspace, entity),
                new WorkspaceAction.SetName(entity, identity.getName()),
                new WorkspaceAction.AddAnnotation(entity, new Annotation(SynBadTerms.Annotatable.entitytype,  EntityType.Interaction.getUri())),
                new WorkspaceAction.AddAnnotation(entity, new Annotation(SynBadTerms.Annotatable.role, InteractionRole.mRNAProduction.getUri())),
                new ReturnableAction.CreatePort(entity, PortDirection.IN, SynBadPortType.PoPS, true),
                new ReturnableAction.CreatePort(entity, PortDirection.OUT, SynBadPortType.mRNA, true));
        }

        @Override
        public SynBadPObject performAndReturn() {
            perform();
            return entity;
        }

        @Override
        public void perform() {
            setup.perform();
        }

        @Override
        public void undo() {
            if(setup != null) {
                setup.undo();
            }
        }
        
                @Override
        public SynBadPObject getValue() {
            return entity;
        }
    }
    
    public static class CreateProteinProduction implements ReturnableAction<SynBadPObject> {

        private final SynBadPObject entity;
        private final WorkspaceAction setup;
        
        public CreateProteinProduction(SBWorkspace workspace, Identity identity) {
            entity = new SynBadPObject(workspace, identity);
            setup = new WorkspaceAction.Macro(
                new WorkspaceAction.AddEntity(workspace, entity),
                new WorkspaceAction.SetName(entity, identity.getName()),
                new WorkspaceAction.AddAnnotation(entity, new Annotation(SynBadTerms.Annotatable.entitytype,  EntityType.Interaction.getUri())),
                new WorkspaceAction.AddAnnotation(entity, new Annotation(SynBadTerms.Annotatable.role, InteractionRole.ProteinProduction.getUri())),
                new ReturnableAction.CreatePort(entity, PortDirection.IN, SynBadPortType.RiPS, true),
                new ReturnableAction.CreatePort(entity, PortDirection.IN, SynBadPortType.DNA, true),
                new ReturnableAction.CreatePort(entity, PortDirection.OUT, SynBadPortType.Protein, true));
        }

        @Override
        public SynBadPObject performAndReturn() {
            perform();
            return entity;
        }

        @Override
        public void perform() {
            setup.perform();
        }

        @Override
        public void undo() {
            if(setup != null) {
                setup.undo();
            }
        }
        
                @Override
        public SynBadPObject getValue() {
            return entity;
        }
    }
     
    public static class CreateProteinDegradation implements ReturnableAction<SynBadPObject> {

        private final SynBadPObject entity;
        private final WorkspaceAction setup;
        
        public CreateProteinDegradation(SBWorkspace workspace, Identity identity) {
            entity = new SynBadPObject(workspace, identity);
            setup = new WorkspaceAction.Macro(
                new WorkspaceAction.AddEntity(workspace, entity),
                new WorkspaceAction.SetName(entity, identity.getName()),
                new WorkspaceAction.AddAnnotation(entity, new Annotation(SynBadTerms.Annotatable.entitytype,  EntityType.Interaction.getUri())),
                new WorkspaceAction.AddAnnotation(entity, new Annotation(SynBadTerms.Annotatable.role, InteractionRole.Degradation.getUri())),
                new ReturnableAction.CreatePort(entity, PortDirection.IN, SynBadPortType.Protein, true));
        }

        @Override
        public SynBadPObject performAndReturn() {
            perform();
            return entity;
        }

        @Override
        public void perform() {
            setup.perform();
        }

        @Override
        public void undo() {
            if(setup != null) {
                setup.undo();
            }
        }
        
                @Override
        public SynBadPObject getValue() {
            return entity;
        }
        
    }

    public static class CreatePopsProduction implements ReturnableAction<SynBadPObject> {

        private final SynBadPObject entity;
        private final WorkspaceAction setup;
        
        public CreatePopsProduction(SBWorkspace workspace, Identity identity) {
            this.entity = new SynBadPObject(workspace, identity);
            this.setup = new WorkspaceAction.Macro(
                new WorkspaceAction.AddEntity(workspace, entity),
                new WorkspaceAction.SetName(entity, identity.getName()),
                new WorkspaceAction.AddAnnotation(entity, new Annotation(SynBadTerms.Annotatable.entitytype,  EntityType.Interaction.getUri())),
                new WorkspaceAction.AddAnnotation(entity, new Annotation(SynBadTerms.Annotatable.role, InteractionRole.PoPSProduction.getUri())),
                new ReturnableAction.CreatePort(entity, PortDirection.IN, SynBadPortType.DNA, true),
                new ReturnableAction.CreatePort(entity, PortDirection.OUT, SynBadPortType.PoPS, true));
        }

        @Override
        public SynBadPObject performAndReturn() {
            perform();
            return entity;
        }

        @Override
        public void perform() {
            setup.perform();
        }

        @Override
        public void undo() {
            if(setup != null) {
                setup.undo();
            }
        } 
        
        @Override
        public SynBadPObject getValue() {
            return entity;
        }
    }
    
    public static class CreateRipsProduction implements ReturnableAction<SynBadPObject> {

        private final SynBadPObject entity;
        private final WorkspaceAction setup;
        
        public CreateRipsProduction(SBWorkspace workspace, Identity identity) {
            this.entity = new SynBadPObject(workspace, identity);
            this.setup = new WorkspaceAction.Macro(
                new WorkspaceAction.AddEntity(workspace, entity),
                new WorkspaceAction.SetName(entity, identity.getName()),
                new WorkspaceAction.AddAnnotation(entity, new Annotation(SynBadTerms.Annotatable.entitytype,  EntityType.Interaction.getUri())),
                new WorkspaceAction.AddAnnotation(entity, new Annotation(SynBadTerms.Annotatable.role, InteractionRole.RiPSProduction.getUri())),
                new ReturnableAction.CreatePort(entity, PortDirection.IN, SynBadPortType.mRNA, true),
                new ReturnableAction.CreatePort(entity, PortDirection.OUT, SynBadPortType.RiPS, true));
        }

        @Override
        public SynBadPObject performAndReturn() {
            perform();
            return entity;
        }

        @Override
        public void perform() {
            setup.perform();
        }

        @Override
        public void undo() {
            if(setup != null) {
                setup.undo();
            }
        }
        
        @Override
        public SynBadPObject getValue() {
            return entity;
        }
    }
}

