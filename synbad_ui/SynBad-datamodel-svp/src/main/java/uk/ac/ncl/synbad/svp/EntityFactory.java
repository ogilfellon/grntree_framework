/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.synbad.svp;

import java.net.URI;
import java.net.URISyntaxException;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;
import org.openide.util.Exceptions;
import org.openide.util.lookup.ServiceProvider;
import uk.ac.ncl.icos.datamodel.workspace.api.rdf.IDomainFactory;
import uk.ac.ncl.icos.datamodel.workspace.objects.SynBadObject;
import uk.ac.ncl.icos.datamodel.workspace.objects.SynBadObject;
import uk.ac.ncl.icos.datamodel.workspace.api.rdf.RDFService;
import uk.ac.ncl.icos.datamodel.workspace.api.RdfHelper;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms2;

/**
 *
 * @author owengilfellon
 */
@ServiceProvider(service=IDomainFactory.class)
public class EntityFactory extends IDomainFactory.ADomainFactory<SynBadObject> {

    

    @Override
    public SynBadObject getEntity(String uri, RDFService service) {

        Model m = service.getModel();
        Resource r = m.getResource(uri);
        String prefix = "";
        try {
            URI uri1 = new URI(r.getURI());
            prefix = uri1.getScheme() + "://" + uri1.getHost();
        } catch (URISyntaxException ex) {
            Exceptions.printStackTrace(ex);
        }
  
        String displayId = RdfHelper.getLiteral(r, SynBadTerms2.SbolIdentified.hasDisplayId, String.class);
        String version = RdfHelper.getLiteral(r, SynBadTerms2.SbolIdentified.hasVersion, String.class);
        String description = RdfHelper.getLiteral(r, SynBadTerms2.SbolIdentified.hasDescription, String.class);
        String name = RdfHelper.getLiteral(r, SynBadTerms2.SbolIdentified.hasName, String.class);

        SynBadObject entity = new SynBadObject(null, prefix, displayId, version);
        entity.setDescription(description);
        entity.setName(name);
        
        return entity;
    }

    @Override
    public Class<?> getEntityClass() {
        return SynBadObject.class;
    }

    @Override
    public String getType() {
        return "Entity";
    }
    
    @Override
    public String getSuperType() {
        return "DomainObject";
    }

    @Override
    public String getLabel() {
        return "Entity";
    }

    @Override
    public String getComment() {
        return "Entity";
    }


}
