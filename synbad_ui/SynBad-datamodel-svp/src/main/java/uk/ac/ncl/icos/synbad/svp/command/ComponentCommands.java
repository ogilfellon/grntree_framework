/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.command;


import org.sbolstandard.core2.Annotation;
import uk.ac.ncl.icos.datamodel.workspace.api.SBWorkspace;
import uk.ac.ncl.icos.datamodel.workspace.impl.PortDirection;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentType;
import uk.ac.ncl.icos.synbad.datadefinition.types.EntityType;
import uk.ac.ncl.icos.synbad.datadefinition.types.Role;
import uk.ac.ncl.icos.synbad.signalgraph.impl.SynBadPortType;
import uk.ac.ncl.icos.datamodel.workspace.objects.SynBadPObject;
import uk.ac.ncl.icos.datamodel.actions.ReturnableAction;
import uk.ac.ncl.icos.datamodel.actions.WorkspaceAction;
import uk.ac.ncl.icos.datamodel.workspace.objects.SynBadPObject;
import uk.ac.ncl.icos.datamodel.workspace.impl.Identity;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;


/**
 *
 * @author owengilfellon
 */
public class ComponentCommands {
 
    public static class CreateDna implements ReturnableAction<SynBadPObject> {

        private final SynBadPObject entity;
        private final WorkspaceAction setup;
        
        public CreateDna(SBWorkspace workspace, Identity identity, Role role) {    
            this.entity = new SynBadPObject(workspace, identity);
            this.setup = new WorkspaceAction.Macro(
                new WorkspaceAction.AddEntity(workspace, entity),
                new WorkspaceAction.AddAnnotation(entity, new Annotation(SynBadTerms.Annotatable.entitytype,  EntityType.Component.getUri())),
                new WorkspaceAction.AddAnnotation(entity, new Annotation(SynBadTerms.Annotatable.type, ComponentType.DNA.getUri())),
                new WorkspaceAction.AddAnnotation(entity, new Annotation(SynBadTerms.Annotatable.role, role.getUri())),
                new ReturnableAction.CreatePort(entity, PortDirection.OUT, SynBadPortType.DNA, true));
        }

        @Override
        public SynBadPObject performAndReturn() {
            perform();
            return entity;
        }

        @Override
        public void perform() {
            setup.perform();
        }

        @Override
        public void undo() {
            if(setup != null)
                setup.undo();
        }
        
                @Override
        public SynBadPObject getValue() {
            return entity;
        }
    }
 
    public static class CreateMrna implements ReturnableAction<SynBadPObject> {

        private final SynBadPObject entity;
        private final WorkspaceAction setup;
        
        public CreateMrna(SBWorkspace workspace, Identity identity) {
            this.entity = new SynBadPObject(workspace, identity);
            this.setup = new WorkspaceAction.Macro(
                new WorkspaceAction.AddEntity(workspace, entity),
                new WorkspaceAction.AddAnnotation(entity, new Annotation(SynBadTerms.Annotatable.entitytype,  EntityType.Component.getUri())),
                new WorkspaceAction.AddAnnotation(entity, new Annotation(SynBadTerms.Annotatable.type, ComponentType.mRNA.getUri())),
                new ReturnableAction.CreatePort(entity, PortDirection.IN, SynBadPortType.mRNA, true),
                new ReturnableAction.CreatePort(entity, PortDirection.OUT, SynBadPortType.mRNA, true));
        }

        @Override
        public SynBadPObject performAndReturn() {
            perform();
            return entity;
        }

        @Override
        public void perform() {
            setup.perform();
        }

        @Override
        public void undo() {
            if(setup != null)
                setup.undo();
        }
        
                @Override
        public SynBadPObject getValue() {
            return entity;
        }
    }

    public static class CreateProtein implements ReturnableAction<SynBadPObject> {

        private final SynBadPObject entity;
        private final WorkspaceAction setup;
        
        public CreateProtein(SBWorkspace workspace, Identity identity) {
            this.entity = new SynBadPObject(workspace, identity);
            this.setup = new WorkspaceAction.Macro(
                new WorkspaceAction.AddEntity(workspace, entity),
                new WorkspaceAction.AddAnnotation(entity, new Annotation(SynBadTerms.Annotatable.entitytype,  EntityType.Component.getUri())),
                new WorkspaceAction.AddAnnotation(entity, new Annotation(SynBadTerms.Annotatable.type, ComponentType.Protein.getUri())),
                new ReturnableAction.CreatePort(entity, PortDirection.IN, SynBadPortType.Protein, true),
                new ReturnableAction.CreatePort(entity, PortDirection.OUT, SynBadPortType.Protein, true));
        }

        @Override
        public SynBadPObject performAndReturn() {
            perform();
            return entity;
        }

        @Override
        public void perform() {
            setup.perform();
        }

        @Override
        public void undo() {
            if(setup != null) {
                setup.undo();
            }
        }
        
                @Override
        public SynBadPObject getValue() {
            return entity;
        }
    }

    public static class CreateSmallMolecule implements ReturnableAction<SynBadPObject> {

        private final SynBadPObject entity;
        private final WorkspaceAction setup;

        public CreateSmallMolecule(SBWorkspace workspace, Identity identity) {
            this.entity = new SynBadPObject(workspace, identity);
            this.setup = new WorkspaceAction.Macro(
                new WorkspaceAction.AddEntity(workspace, entity),
                new WorkspaceAction.AddAnnotation(entity, new Annotation(SynBadTerms.Annotatable.entitytype,  EntityType.Component.getUri())),
                new WorkspaceAction.AddAnnotation(entity, new Annotation(SynBadTerms.Annotatable.type, ComponentType.SmallMolecule.getUri())),
                new ReturnableAction.CreatePort(entity, PortDirection.IN, SynBadPortType.SmallMolecule, true),
                new ReturnableAction.CreatePort(entity, PortDirection.OUT, SynBadPortType.SmallMolecule, true));
        }

        @Override
        public SynBadPObject performAndReturn() {
            perform();
            return entity;
        }

        @Override
        public void perform() {
            setup.perform();
        }

        @Override
        public void undo() {
            if(setup != null) {
                setup.undo();
            }
        }

        @Override
        public SynBadPObject getValue() {
            return entity;
        }
        
        
    }
}
