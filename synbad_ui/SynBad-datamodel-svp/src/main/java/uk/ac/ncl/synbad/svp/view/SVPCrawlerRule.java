/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.synbad.svp.view;

import uk.ac.ncl.icos.datamodel.workspace.impl.EntityInstance;
import uk.ac.ncl.icos.datamodel.workspace.impl.PortRecord;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.datadefinition.types.EntityType;
import uk.ac.ncl.icos.synbad.datadefinition.types.ModelEdgeType;
import uk.ac.ncl.icos.synbad.datadefinition.types.ModuleRole;
import uk.ac.ncl.icos.view.api.IViewCrawlerListener;
import uk.ac.ncl.icos.model.entities.VEdge;
import uk.ac.ncl.icos.model.entities.VNode;
import uk.ac.ncl.icos.view.api.HViewCursor;

/**
 *
 * @author owengilfellon
 */
public class SVPCrawlerRule implements IViewCrawlerListener {

    @Override
    public void onEdge(VEdge edge) {}

    @Override
    public void onEnded() {}

    @Override
    public void onInstance(VNode instance, HViewCursor cursor) {}

    @Override
    public boolean terminate() {
        return false;
    }

    @Override
    public boolean vetoEdge(VEdge edge) {
        return !edgeConnectsSVPs(edge);
    }
    
    private boolean edgeConnectsSVPs(VEdge edge) {
        
        if(isTypeOf(ModelEdgeType.WIRE, edge)) {
            
            // Wire should connect two SVP / SVP Interactions
            
            PortRecord from = (PortRecord)edge.getFrom().getData();
            
            PortRecord to = (PortRecord)edge.getTo().getData();
  
            return (isSvp(from.getOwner()) || isModule(from.getOwner())) && 
                    (isSvp(to.getOwner()) || isModule(to.getOwner()));
            
        } else if(isTypeOf(ModelEdgeType.HAS_CHILD, edge)) {
            
            // The child should be a module, or an SVP, unless is an SVP
            
            EntityInstance parent = (EntityInstance)edge.getFrom().getData();
            
            EntityInstance child = (EntityInstance)edge.getTo().getData();     
            
            return isModule(parent) && !isSvp(parent) && (isModule(child) || isSvp(child));
            
        
        } else if(isTypeOf(ModelEdgeType.PROXY, edge)) {
            
            // The proxied port should belong to a SVP or module (hide SVP internals)
            
            PortRecord proxyPort = (PortRecord)edge.getFrom().getData();
            
            PortRecord proxiedPort = (PortRecord)edge.getTo().getData();
  
            return (isSvp(proxiedPort.getOwner()) || isModule(proxiedPort.getOwner())) && !isSvp(proxyPort.getOwner());
            
            
        } else if(isTypeOf(ModelEdgeType.HAS_PORT, edge)) {
            
            // The port should belong to a SVP or module

            EntityInstance owner = (EntityInstance)edge.getFrom().getData();
         
            return (isSvp(owner) || isModule(owner));
            
        }
        
        return false;
    }
    
    private boolean isTypeOf(ModelEdgeType type, VEdge edge) {
        return edge.getData().getType() == type;
    }
    
    private boolean isSvp(EntityInstance instance) {
        return  instance.getValue().hasAnnotation(SynBadTerms.Annotatable.role, ModuleRole.SVPInteraction.getUri()) || 
                instance.getValue().hasAnnotation(SynBadTerms.Annotatable.role, ModuleRole.SVPPart.getUri());
    }
    
    private boolean isModule(EntityInstance instance) {
        return instance.getValue().hasAnnotation(SynBadTerms.Annotatable.entitytype, EntityType.Module.getUri());
    }
    
    

    
    
}
