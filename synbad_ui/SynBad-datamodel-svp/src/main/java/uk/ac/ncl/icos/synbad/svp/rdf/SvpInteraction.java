/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.rdf;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import org.sbolstandard.core2.Annotation;
import uk.ac.ncl.icos.datamodel.workspace.api.SBWorkspace;
import uk.ac.ncl.icos.datamodel.workspace.objects.IPort;
import uk.ac.ncl.icos.datamodel.workspace.objects.SynBadObject;
import uk.ac.ncl.icos.datamodel.workspace.objects.Identity;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.datadefinition.types.EntityType;
import uk.ac.ncl.icos.synbad.datadefinition.types.InteractionRole;
import uk.ac.ncl.icos.synbad.datadefinition.types.ModuleRole;
import uk.ac.ncl.icos.synbad.graph.api.SBValue;
import uk.ac.ncl.icos.workspace.factories.AnnotationFactory;
import uk.ac.ncl.intbio.virtualparts.entity.InteractionPartDetail;
import uk.ac.ncl.intbio.virtualparts.entity.Parameter;

/**
 *
 * @author owengilfellon
 */
public class SvpInteraction extends SynBadObject {
    
    private final InteractionRole role;
    private final Set<Participation> participants;

    public SvpInteraction(Identity identity, InteractionRole role) {
        super(identity);
        this.role = role;
        this.participants = new HashSet<>();
    }
    
    // TODO: Need terms for representing participation roles in interactions
    
    public Participation addParticipant(SynBadObject participant, IPort port, int stoichiometry) {
        
        
        Participation p = new Participation(this, participant, port, stoichiometry);
        participants.add(p);
        //getWorkspace().getDispatcher().dispatch(
        //        new SvpEvents.ParticipationEvent(EventType.ADDED, this, p));
        return p;
    }
    
    public void addParticipant(Participation participation) {
        participants.add(participation);
        //getWorkspace().getDispatcher().dispatch(
        //        new SvpEvents.ParticipationEvent(EventType.ADDED, this, participation));
    }
    
    public boolean removeParticipation(Participation participation) {
        if(!participants.remove(participation))
            return false;
        
        //getWorkspace().getDispatcher().dispatch(
        //        new SvpEvents.ParticipationEvent(EventType.REMOVED, this, participation));
        
        return true;
    }

    // TODO: Return a collection of Participation objects
    
    public Collection<Participation> getParticipants() {
        return Collections.unmodifiableCollection(participants);
    }
    
    public InteractionRole getRole() {
        return role;
    }
    
     private String getStringFromValue(String predicate) {
        SBValue v = getValue(predicate);
        
        if(v == null || !v.isString())
            return "";
        
        else return v.asString();
    }
    
     
      private Boolean getBooleanFromValue(String predicate) {
        SBValue v = getValue(predicate);
        
        if(v == null || !v.isBoolean())
            return null;
        
        else return v.asBoolean();
    }
    

    public String getMathName() {
        return getStringFromValue(SynBadTerms.VPR.mathName.toString());
    }
    
    public String getFreeTextMath() {
        return getStringFromValue(SynBadTerms.VPR.freeTextMath.toString());
    }
    
    public boolean isInternal() {
        Boolean b = getBooleanFromValue(SynBadTerms.VPR.isInternal.toString());
        return b == null ? false : b;
    }
    
    public boolean isReaction() {
        Boolean b = getBooleanFromValue(SynBadTerms.VPR.isReaction.toString());
        return b == null ? false : b;
    }
    
    public boolean isReversible() {
        Boolean b = getBooleanFromValue(SynBadTerms.VPR.isReversible.toString());
        return b == null ? false : b;
    }
    
    public Collection<InteractionPartDetail> getInteractionPartDetails() {
        
        Set<InteractionPartDetail> ipds = new HashSet<>();
        /*
        for(Annotation annotation : getAnnotations(SynBadTerms.VPR.partDetail)) {
            ipds.add(AnnotationFactory.getInteractionPartDetail(annotation));
        }*/
        
        return ipds;
    } 
    
    public Collection<Parameter> getParameters() {
        
        Set<Parameter> parameters = new HashSet<>();
        /*
        for(Annotation annotation : getAnnotations(SynBadTerms.VPR.partDetail)) {
            parameters.add(AnnotationFactory.getParameter(annotation));
        }*/
        
        return parameters;
    } 
       

}
