/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.command;

import java.net.URI;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Stack;
import org.sbolstandard.core2.Annotation;
import uk.ac.ncl.icos.datamodel.workspace.impl.PortDirection;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentRole;
import uk.ac.ncl.icos.synbad.datadefinition.types.EntityType;
import uk.ac.ncl.icos.synbad.datadefinition.types.InteractionRole;
import uk.ac.ncl.icos.synbad.datadefinition.types.ModuleRole;
import uk.ac.ncl.icos.synbad.signalgraph.impl.SynBadPortState;
import uk.ac.ncl.icos.synbad.signalgraph.impl.SynBadPortType;
import uk.ac.ncl.icos.datamodel.workspace.objects.SynBadPObject;
import uk.ac.ncl.icos.datamodel.workspace.objects.IPort;
import uk.ac.ncl.icos.datamodel.workspace.api.SBWorkspace;
import uk.ac.ncl.icos.datamodel.actions.ReturnableAction;
import uk.ac.ncl.icos.datamodel.actions.WorkspaceAction;
import uk.ac.ncl.icos.datamodel.workspace.objects.SynBadPObject;
import uk.ac.ncl.icos.datamodel.workspace.impl.EntityInstance;
import uk.ac.ncl.icos.datamodel.workspace.impl.Identity;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;

/**
 *
 * @author owengilfellon
 */
public class SVICommands {
    
    public static class CreateProteinProduction implements ReturnableAction<SynBadPObject> {

        private final SBWorkspace workspace;
        private final Identity identity;
        private SynBadPObject entity;
        private final double km;

        private final Stack<WorkspaceAction> commands = new Stack<>();


        public CreateProteinProduction(SBWorkspace workspace, Identity identity, double km) {
            this.identity = identity;
            this.workspace = workspace;
            this.km = km;
            
        }

        @Override
        public SynBadPObject performAndReturn() {
            perform();
            return entity;
        }
        
        private void firstTimeSetup() {

            entity = ((ReturnableAction<SynBadPObject>)commands.push(
                new CreateEntity(workspace, identity))).performAndReturn();
            
            commands.push(new WorkspaceAction.Macro(
                new WorkspaceAction.AddAnnotation(entity, new Annotation(SynBadTerms.Annotatable.entitytype,  EntityType.Module.getUri())),
                new WorkspaceAction.AddAnnotation(entity, new Annotation(SynBadTerms.Annotatable.role, InteractionRole.ProteinProduction.getUri())),
                new WorkspaceAction.AddAnnotation(entity, new Annotation(SynBadTerms.Annotatable.role, ModuleRole.SVPInteraction.getUri())),
                new WorkspaceAction.AddAnnotation(entity, new Annotation(SynBadTerms.VPR.parameter, SynBadTerms.VPR.parameter, URI.create(entity.getIdentity() + "/Parameter/Km"), Arrays.asList(
                    new Annotation(SynBadTerms.Annotatable.name, "Km"),
                    new Annotation(SynBadTerms.Annotatable.type, "Km"),
                    new Annotation(SynBadTerms.VPR.parameterScope, "Local"),
                    new Annotation(SynBadTerms.VPR.propertyValue, km)))),
                new WorkspaceAction.AddAnnotation(entity, new Annotation(SynBadTerms.VPR.parameter, SynBadTerms.VPR.parameter, URI.create(entity.getIdentity() + "/Parameter/n"), Arrays.asList(
                    new Annotation(SynBadTerms.Annotatable.name, "n"),
                    new Annotation(SynBadTerms.Annotatable.type, "n"),
                    new Annotation(SynBadTerms.VPR.propertyValue, 1.0)))))).perform();

            SynBadPObject dna = ((ReturnableAction<SynBadPObject>)commands.push(new ComponentCommands.CreateDna(workspace, new Identity(identity.getUriPrefix(), identity.getDisplayID()+"_dna", identity.getVersion()), ComponentRole.CDS))).performAndReturn();
            SynBadPObject protein = ((ReturnableAction<SynBadPObject>)commands.push(new ComponentCommands.CreateProtein(workspace, new Identity(identity.getUriPrefix(), identity.getDisplayID()+"_protein", identity.getVersion())))).performAndReturn();
            SynBadPObject production = ((ReturnableAction<SynBadPObject>)commands.push(new InteractionCommands.CreateProteinProduction(workspace, new Identity(identity.getUriPrefix(), identity.getDisplayID()+"_proteinProduction", identity.getVersion())))).performAndReturn();

            EntityInstance iDna = ((ReturnableAction<EntityInstance>)commands.push(new ReturnableAction.CreateChild(entity, dna))).performAndReturn();
            EntityInstance iProtein = ((ReturnableAction<EntityInstance>)commands.push(new ReturnableAction.CreateChild(entity, protein))).performAndReturn();
            EntityInstance iProduction = ((ReturnableAction<EntityInstance>)commands.push(new ReturnableAction.CreateChild(entity, production))).performAndReturn();

            commands.push(new ReturnableAction.WireChildren(entity, iDna, iProduction, SynBadPortType.DNA)).perform();
            commands.push(new ReturnableAction.WireChildren(entity, iProduction, iProtein, SynBadPortType.Protein)).perform();
            commands.push(new ReturnableAction.CreateProxy(entity, protein.getPort(port -> port.getPortDirection() == PortDirection.OUT), true)).perform();
            
            production.getPorts().stream()
                .filter(p -> p.getPortDirection() == PortDirection.IN)
                .filter(p -> p.getPortType() == SynBadPortType.RiPS)
                .forEach(p -> commands.push(new ReturnableAction.CreateProxy(entity, p, true)).perform());
        }
        
        @Override
        public void perform() {

            if(entity == null) {
                firstTimeSetup();
            } else {
                LinkedList l = new LinkedList(commands);
                Iterator<WorkspaceAction> i = l.iterator();
                while(i.hasNext()) {
                    i.next().perform();
                }
            }
            

        }
   
        @Override
        public void undo() {

            LinkedList l = new LinkedList(commands);
            Iterator<WorkspaceAction> i = l.descendingIterator();
            while(i.hasNext()) {
                i.next().undo();
            }

        }
        
                @Override
        public SynBadPObject getValue() {
            return entity;
        }
        
    }
    
    public static class CreateProteinDegradation implements ReturnableAction<SynBadPObject> {

        private final SBWorkspace workspace;
        private final Identity identity;
        private final double kd;

        private final Stack<WorkspaceAction> commands = new Stack<>();
        
        private SynBadPObject entity;

        public CreateProteinDegradation(SBWorkspace workspace, Identity identity, double kd) {
            this.identity = identity;
            this.workspace = workspace;
            this.kd = kd;
        }

        @Override
        public SynBadPObject performAndReturn() {
            perform();
            return entity;
        }
        
        private void firstTimeSetup()
        {
            entity = ((ReturnableAction<SynBadPObject>)commands.push(new ReturnableAction.CreateEntity(this.workspace, identity))).performAndReturn();
            SynBadPObject degradation = ((ReturnableAction<SynBadPObject>)commands.push(new InteractionCommands.CreateProteinProduction(workspace, new Identity(identity.getUriPrefix(), identity.getDisplayID()+"_proteinDegradation", identity.getVersion())))).performAndReturn();
            commands.push(new ReturnableAction.CreateChild(entity, degradation, 0)).perform();
            commands.push(new WorkspaceAction.Macro(
                new WorkspaceAction.AddAnnotation(entity, new Annotation(SynBadTerms.Annotatable.entitytype,  EntityType.Module.getUri())),
                new WorkspaceAction.AddAnnotation(entity, new Annotation(SynBadTerms.Annotatable.role, InteractionRole.Degradation.getUri())),
                new WorkspaceAction.AddAnnotation(entity, new Annotation(SynBadTerms.Annotatable.role, ModuleRole.SVPInteraction.getUri())),
                new WorkspaceAction.AddAnnotation(entity, new Annotation(SynBadTerms.VPR.parameter, SynBadTerms.VPR.parameter, URI.create(entity.getIdentity() + "/Parameter/Kd"), Arrays.asList(
                    new Annotation(SynBadTerms.Annotatable.name, "Kd"),
                    new Annotation(SynBadTerms.Annotatable.type, "Kd"),
                    new Annotation(SynBadTerms.VPR.parameterScope, "Local"),
                    new Annotation(SynBadTerms.VPR.propertyValue, kd)))))).perform();
            commands.push(new ReturnableAction.CreateProxy(entity, degradation.getPort((IPort port) -> port.getPortDirection() == PortDirection.IN), true)).perform();
        }
        
        @Override
        public void perform() {

            if(entity == null) {
                firstTimeSetup();
            } else {
                LinkedList l = new LinkedList(commands);
                Iterator<WorkspaceAction> i = l.iterator();
                while(i.hasNext()) {
                    i.next().perform();
                }
            }
            

        }
   
        @Override
        public void undo() {

            LinkedList l = new LinkedList(commands);
            Iterator<WorkspaceAction> i = l.descendingIterator();
            while(i.hasNext()) {
                i.next().undo();
            }

        }
        
                @Override
        public SynBadPObject getValue() {
            return entity;
        }
    }

    public static class CreatePopsProduction implements ReturnableAction<SynBadPObject> {

        private final SBWorkspace workspace;
        private final Identity identity;
        private final double ktr;

        private final Stack<WorkspaceAction> commands = new Stack<>();
        
        private SynBadPObject entity;

        public CreatePopsProduction(SBWorkspace workspace, Identity identity, double ktr) {
            this.identity = identity;
            this.workspace = workspace;
            this.ktr = ktr;
        }

        @Override
        public SynBadPObject performAndReturn() {
            perform();
            return entity;
        }
        
        private void firstTimeSetup()
        {
            entity = ((ReturnableAction<SynBadPObject>)commands.push(
                    new ReturnableAction.CreateEntity(this.workspace, identity))).performAndReturn();
            
            commands.push(new WorkspaceAction.Macro(
                new WorkspaceAction.AddAnnotation(entity, new Annotation(SynBadTerms.Annotatable.entitytype,  EntityType.Module.getUri())),
                new WorkspaceAction.AddAnnotation(entity, new Annotation(SynBadTerms.Annotatable.role, InteractionRole.Degradation.getUri())),
                new WorkspaceAction.AddAnnotation(entity, new Annotation(SynBadTerms.Annotatable.role, ModuleRole.SVPInteraction.getUri())),
                new WorkspaceAction.AddAnnotation(entity, new Annotation(SynBadTerms.VPR.parameter, SynBadTerms.VPR.parameter, URI.create(entity.getIdentity() + "/Parameter/Ktr"), Arrays.asList(
                    new Annotation(SynBadTerms.Annotatable.name, "ktr"),
                    new Annotation(SynBadTerms.Annotatable.type, "ktr"),
                    new Annotation(SynBadTerms.VPR.parameterScope, "Local"),
                    new Annotation(SynBadTerms.VPR.propertyValue, ktr)))))).perform();
            
            SynBadPObject popsProduction = ((ReturnableAction<SynBadPObject>)commands.push(
                    new InteractionCommands.CreatePopsProduction(workspace, new Identity(identity.getUriPrefix(), identity.getDisplayID()+"_popsProduction", identity.getVersion())))).performAndReturn();
            SynBadPObject dna = ((ReturnableAction<SynBadPObject>)commands.push(
                    new ComponentCommands.CreateDna(workspace, new Identity(identity.getUriPrefix(), identity.getDisplayID()+"_dna", identity.getVersion()), ComponentRole.Promoter))).performAndReturn();
            
            commands.push( new ReturnableAction.CreateChild(entity, popsProduction)).perform();
            commands.push( new ReturnableAction.CreateChild(entity, dna)).perform();
            commands.push( new ReturnableAction.CreateProxy(entity, popsProduction.getPort((IPort port) -> port.getPortDirection() == PortDirection.OUT), true)).perform();
            /*
            commands.push(new ReturnableAction.WirePorts(entity,
                    iDna.getPort(dna.getPort(p -> p.getPortDirection() == PortDirection.OUT).getIdentity()),
                    production.getPort(popsProduction.getPort( p -> p.getPortDirection() == PortDirection.IN).getIdentity())));
                    */
            
            System.out.println();
        }
        
        @Override
        public void perform() {

            if(entity == null) {
                firstTimeSetup();
            } else {
                LinkedList l = new LinkedList(commands);
                Iterator<WorkspaceAction> i = l.iterator();
                while(i.hasNext()) {
                    i.next().perform();
                }
            }
            

        }
   
        @Override
        public void undo() {

            LinkedList l = new LinkedList(commands);
            Iterator<WorkspaceAction> i = l.descendingIterator();
            while(i.hasNext()) {
                i.next().undo();
            }

        }
        
                @Override
        public SynBadPObject getValue() {
            return entity;
        }
        
    }
    
    public static class CreateMrnaProduction implements ReturnableAction<SynBadPObject> {

        private final SBWorkspace workspace;
        private final Identity identity;
        private final Stack<WorkspaceAction> commands = new Stack<>();
        private SynBadPObject entity;

        public CreateMrnaProduction(SBWorkspace workspace, Identity identity) {
            this.identity = identity;
            this.workspace = workspace;
        }

        @Override
        public SynBadPObject performAndReturn() {
            perform();
            return entity;
        }
        
        private void firstTimeSetup()
        {
            entity = ((ReturnableAction<SynBadPObject>)commands.push(new ReturnableAction.CreateEntity(this.workspace, identity))).performAndReturn();
            SynBadPObject mrnaProduction = ((ReturnableAction<SynBadPObject>)commands.push(new InteractionCommands.CreateMrnaProduction(workspace, new Identity(identity.getUriPrefix(), identity.getDisplayID()+"_mrnaProduction", identity.getVersion())))).performAndReturn();
            SynBadPObject mrna = ((ReturnableAction<SynBadPObject>)commands.push(new ComponentCommands.CreateMrna(workspace, new Identity(identity.getUriPrefix(), identity.getDisplayID()+"_mrnaComp", identity.getVersion())))).performAndReturn();
            EntityInstance production = ((ReturnableAction<EntityInstance>)commands.push(new ReturnableAction.CreateChild(entity, mrnaProduction))).performAndReturn();
            EntityInstance iMrna = ((ReturnableAction<EntityInstance>)commands.push(new ReturnableAction.CreateChild(entity, mrna))).performAndReturn();
            
            commands.push(new WorkspaceAction.Macro(
                new WorkspaceAction.AddAnnotation(entity, new Annotation(SynBadTerms.Annotatable.entitytype,  EntityType.Module.getUri())),
                new WorkspaceAction.AddAnnotation(entity, new Annotation(SynBadTerms.Annotatable.role, InteractionRole.Degradation.getUri())),
                new WorkspaceAction.AddAnnotation(entity, new Annotation(SynBadTerms.Annotatable.role, ModuleRole.SVPInteraction.getUri())))).perform();
            commands.push(new ReturnableAction.CreateProxy(entity, mrnaProduction.getPort((IPort port) -> port.getPortDirection() == PortDirection.IN), true)).perform();
            commands.push(new ReturnableAction.CreateProxy(entity, mrna.getPort((IPort port) -> port.getPortDirection() == PortDirection.OUT), true)).perform();
            /*commands.push(new ReturnableAction.WirePorts(entity,
                        iMrna.getPort(mrna.getPort(p -> p.getPortDirection() == PortDirection.OUT).getIdentity()),
                        production.getPort(mrnaProduction.getPort(p -> p.getPortDirection() == PortDirection.IN).getIdentity()))).perform();
        */
        }
        
        @Override
        public void perform() {

            if(entity == null) {
                firstTimeSetup();
            } else {
                LinkedList l = new LinkedList(commands);
                Iterator<WorkspaceAction> i = l.iterator();
                while(i.hasNext()) {
                    i.next().perform();
                }
            }
            

        }
   
        @Override
        public void undo() {

            LinkedList l = new LinkedList(commands);
            Iterator<WorkspaceAction> i = l.descendingIterator();
            while(i.hasNext()) {
                i.next().undo();
            }

        }
        
                @Override
        public SynBadPObject getValue() {
            return entity;
        }
        
    }
    
    public static class CreateRipsProduction implements ReturnableAction<SynBadPObject> {

        private final SBWorkspace workspace;
        private final Identity identity;
        private final Stack<WorkspaceAction> commands = new Stack<>();
        private SynBadPObject entity;

        public CreateRipsProduction(SBWorkspace workspace, Identity identity) {
            this.identity = identity;
            this.workspace = workspace;
        }

        @Override
        public SynBadPObject performAndReturn() {
            perform();
            return entity;
        }
        
        private void firstTimeSetup()
        {
            entity = ((ReturnableAction<SynBadPObject>)commands.push(new ReturnableAction.CreateEntity(this.workspace, identity))).performAndReturn();
            SynBadPObject ripsProduction = ((ReturnableAction<SynBadPObject>)commands.push(new InteractionCommands.CreateRipsProduction(workspace, new Identity(identity.getUriPrefix(), identity.getDisplayID()+"_ripsProduction", identity.getVersion())))).performAndReturn();
          //  IEntity dna = ((ReturnableAction<IEntity>)commands.push(new ComponentCommands.CreateDna(workspace, new Identity(identity.getUriPrefix(), identity.getDisplayID()+"_dna"), ComponentRole.RBS))).performAndReturn();
            EntityInstance production = ((ReturnableAction<EntityInstance>)commands.push(new ReturnableAction.CreateChild(entity, ripsProduction))).performAndReturn();
            //commands.push(new ReturnableAction.CreateChild(entity, dna)).perform();
            
            commands.push(new WorkspaceAction.Macro(
                new WorkspaceAction.AddAnnotation(entity, new Annotation(SynBadTerms.Annotatable.entitytype,  EntityType.Module.getUri())),
                new WorkspaceAction.AddAnnotation(entity, new Annotation(SynBadTerms.Annotatable.role, InteractionRole.RiPSProduction.getUri())),
                new WorkspaceAction.AddAnnotation(entity, new Annotation(SynBadTerms.Annotatable.role, ModuleRole.SVPInteraction.getUri())))).perform();
            
            commands.push(new ReturnableAction.CreateProxy(entity, ripsProduction.getPort((IPort port) -> port.getPortDirection() == PortDirection.OUT), true)).perform();
            commands.push(new ReturnableAction.CreateProxy(entity, ripsProduction.getPort((IPort port) -> port.getPortDirection() == PortDirection.IN), true)).perform();

        
        }
        
        @Override
        public void perform() {

            if(entity == null) {
                firstTimeSetup();
            } else {
                LinkedList l = new LinkedList(commands);
                Iterator<WorkspaceAction> i = l.iterator();
                while(i.hasNext()) {
                    i.next().perform();
                }
            }
            

        }
   
        @Override
        public void undo() {

            LinkedList l = new LinkedList(commands);
            Iterator<WorkspaceAction> i = l.descendingIterator();
            while(i.hasNext()) {
                i.next().undo();
            }

        }
        
                @Override
        public SynBadPObject getValue() {
            return entity;
        }
        
    }

    public static class CreateSmallMoleculePhosphorylation implements ReturnableAction<SynBadPObject> {

        private final SynBadPObject entity;
        private final WorkspaceAction setup;

        public CreateSmallMoleculePhosphorylation(SBWorkspace workspace, Identity identity) {
            this.entity = new SynBadPObject(workspace, identity);
            this.setup = new WorkspaceAction.Macro(
                new WorkspaceAction.AddEntity(workspace, entity),
                new WorkspaceAction.AddAnnotation(entity, new Annotation(SynBadTerms.Annotatable.entitytype,  EntityType.Module.getUri())),
                new WorkspaceAction.AddAnnotation(entity, new Annotation(SynBadTerms.Annotatable.role, InteractionRole.Phosphorylation.getUri())),
                new WorkspaceAction.AddAnnotation(entity, new Annotation(SynBadTerms.Annotatable.role, ModuleRole.SVPInteraction.getUri())),
                new ReturnableAction.CreatePort(entity, PortDirection.IN, SynBadPortType.SmallMolecule, true),
                new ReturnableAction.CreateStatefulPort(entity, PortDirection.IN, SynBadPortType.Protein, SynBadPortState.Default, true),
                new ReturnableAction.CreateStatefulPort(entity, PortDirection.OUT, SynBadPortType.Protein, SynBadPortState.Phosphorylated, true)
            );
        }

        @Override
        public SynBadPObject performAndReturn() {
            perform();
            return entity;
        }
        
        @Override
        public void perform() {
            setup.perform();
        }
   
        @Override
        public void undo() {
            if(setup != null)
                setup.undo();
        }
        
                @Override
        public SynBadPObject getValue() {
            return entity;
        }
    }
 
    public static class CreatePhosphorylation implements ReturnableAction<SynBadPObject> {

        private final SynBadPObject entity;
        private final WorkspaceAction setup;

        public CreatePhosphorylation(SBWorkspace workspace, Identity identity) {
            this.entity = new SynBadPObject(workspace, identity);
            this.setup = new WorkspaceAction.Macro(
                new WorkspaceAction.AddEntity(workspace, entity),
                new WorkspaceAction.AddAnnotation(entity, new Annotation(SynBadTerms.Annotatable.entitytype,  EntityType.Module.getUri())),
                new WorkspaceAction.AddAnnotation(entity, new Annotation(SynBadTerms.Annotatable.role, InteractionRole.Phosphorylation.getUri())),
                new WorkspaceAction.AddAnnotation(entity, new Annotation(SynBadTerms.Annotatable.role, ModuleRole.SVPInteraction.getUri())),
                new ReturnableAction.CreateStatefulPort(entity, PortDirection.IN, SynBadPortType.Protein, SynBadPortState.Phosphorylated, true),
                new ReturnableAction.CreateStatefulPort(entity, PortDirection.OUT, SynBadPortType.Protein, SynBadPortState.Default, true),
                new ReturnableAction.CreateStatefulPort(entity, PortDirection.IN, SynBadPortType.Protein, SynBadPortState.Default, true),
                new ReturnableAction.CreateStatefulPort(entity, PortDirection.OUT, SynBadPortType.Protein, SynBadPortState.Phosphorylated, true)
            );
        }

        @Override
        public SynBadPObject performAndReturn() {
            perform();
            return entity;
        }
        
        @Override
        public void perform() {
            setup.perform();
        }
   
        @Override
        public void undo() {
            if(setup != null)
                setup.undo();
        }
        
                @Override
        public SynBadPObject getValue() {
            return entity;
        }
    }
    
    public static class CreateDephosphorylation implements ReturnableAction<SynBadPObject> {
        
        private final SynBadPObject entity;
        private final WorkspaceAction setup;

        public CreateDephosphorylation(SBWorkspace workspace, Identity identity) {
            this.entity = new SynBadPObject(workspace, identity);
            this.setup = new WorkspaceAction.Macro(
                new WorkspaceAction.AddAnnotation(entity, new Annotation(SynBadTerms.Annotatable.entitytype,  EntityType.Module.getUri())),
                new WorkspaceAction.AddAnnotation(entity, new Annotation(SynBadTerms.Annotatable.role, InteractionRole.Dephosphorylation.getUri())),
                new WorkspaceAction.AddAnnotation(entity, new Annotation(SynBadTerms.Annotatable.role, ModuleRole.SVPInteraction.getUri())),
                new ReturnableAction.CreateStatefulPort(entity, PortDirection.IN, SynBadPortType.Protein, SynBadPortState.Phosphorylated, true),
                new ReturnableAction.CreateStatefulPort(entity, PortDirection.OUT, SynBadPortType.Protein, SynBadPortState.Default, true));
        }

        @Override
        public SynBadPObject performAndReturn() {
            perform();
            return entity;
        }
        
        @Override
        public void perform() {
            setup.perform();
        }
   
        @Override
        public void undo() {
            setup.undo();
        }
        
                @Override
        public SynBadPObject getValue() {
            return entity;
        }
        
    }
    
    public static class CreatePopsModulation implements ReturnableAction<SynBadPObject> {

        private final SBWorkspace workspace;
        private final Identity identity;
        private final Stack<WorkspaceAction> commands = new Stack<>();
        
        private final Annotation type;
        private final Annotation role;
        private final Annotation svpType;
        
        private final SynBadPortType portType;
        private final SynBadPortState portState;
        
        
        private SynBadPObject entity;

        public CreatePopsModulation(SBWorkspace workspace, Identity identity, SynBadPortType modulator, SynBadPortState modulatorState) {
            this.identity = identity;
            this.workspace = workspace;
            this.type = new Annotation(SynBadTerms.Annotatable.entitytype,  EntityType.Module.getUri());
            this.role = new Annotation(SynBadTerms.Annotatable.role, InteractionRole.PoPSModulation.getUri());
            this.svpType = new Annotation(SynBadTerms.Annotatable.role, ModuleRole.SVPInteraction.getUri());
            this.portType = modulator;
            this.portState = modulatorState;
        }

        @Override
        public SynBadPObject performAndReturn() {
            perform();
            return entity;
        }
        
        private void firstTimeSetup()
        {
            entity = ((ReturnableAction<SynBadPObject>) commands.push(
                new ReturnableAction.CreateEntity(this.workspace, identity))).performAndReturn();
    
            commands.push(new WorkspaceAction.Macro(
                new WorkspaceAction.AddAnnotation(entity, type),
                new WorkspaceAction.AddAnnotation(entity, role),
                new WorkspaceAction.AddAnnotation(entity, svpType),
                new ReturnableAction.CreatePort(entity, PortDirection.IN, SynBadPortType.PoPS, true),
                new ReturnableAction.CreateStatefulPort(entity, PortDirection.OUT, SynBadPortType.Protein, SynBadPortState.Default, true),
                new ReturnableAction.CreateStatefulPort(entity, PortDirection.IN, portType, portState, true))).perform();
        
        
            SynBadPObject dna = ((ReturnableAction<SynBadPObject>) commands.push(
                new ComponentCommands.CreateDna(workspace, new Identity(identity.getUriPrefix(), identity.getDisplayID()+"_popsModulation", identity.getVersion()), ComponentRole.Operator))).performAndReturn();
        
            commands.push(new ReturnableAction.CreateChild(entity, dna)).perform();
        
        
        }
        
        @Override
        public void perform() {

            if(entity == null) {
                firstTimeSetup();
            } else {
                LinkedList l = new LinkedList(commands);
                Iterator<WorkspaceAction> i = l.iterator();
                while(i.hasNext()) {
                    i.next().perform();
                }
            }
            

        }
   
        @Override
        public void undo() {

            LinkedList l = new LinkedList(commands);
            Iterator<WorkspaceAction> i = l.descendingIterator();
            while(i.hasNext()) {
                i.next().undo();
            }

        }
        
                @Override
        public SynBadPObject getValue() {
            return entity;
        }
    }
}
