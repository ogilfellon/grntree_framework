/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.signalgraph.impl;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import uk.ac.ncl.icos.datamodel.workspace.impl.PortType;
import java.net.URI;
import uk.ac.ncl.icos.synbad.datadefinition.UriHelper;

/**
 *
 * @author owengilfellon
 */
public enum SynBadPortType implements PortType {

    DNA(UriHelper.biopax.namespacedUri("signal/dnaRegion").toASCIIString()),
    mRNA(UriHelper.so.namespacedUri("signal/mrna").toASCIIString()),
    PoPS(UriHelper.synbad.namespacedUri("signal/pops").toASCIIString()),
    RiPS(UriHelper.synbad.namespacedUri("signal/rips").toASCIIString()),
    Protein(UriHelper.synbad.namespacedUri("signal/protein").toASCIIString()),
    Complex(UriHelper.synbad.namespacedUri("signal/complex").toASCIIString()),
    SmallMolecule(UriHelper.synbad.namespacedUri("signal/smallMolecule").toASCIIString());


    @Override
    public URI getUri() {
        return uri;
    }
    private final URI uri;
    

    private SynBadPortType(String uri) {
        this.uri = URI.create(uri);
    }   


}

