/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.rdf;

import uk.ac.ncl.icos.datamodel.workspace.objects.SynBadObject;
import uk.ac.ncl.icos.datamodel.workspace.objects.IPort;

/**
 *
 * @author owengilfellon
 */
public class Participation {
  
    private final SvpInteraction interaction;
    private final SynBadObject identity;
    private final IPort port;
    private final int stoichiometry;

    public Participation(SvpInteraction interaction, SynBadObject identity, IPort port, int stoichiometry) {
        this.interaction = interaction;
        this.identity = identity;
        this.port = port;
        this.stoichiometry = stoichiometry;
    }
    
    public SvpInteraction getInteraction() {
        return interaction;
    }
    
    public SynBadObject getParticipant() {
        return identity;
    }

    public IPort getParticipantPort() {
        return port;
    }
    
    public int getStoichiometry() {
        return stoichiometry;
    }
}
