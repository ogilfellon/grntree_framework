/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.parsers;

import org.sbml.jsbml.SBMLDocument;
import uk.ac.ncl.intbio.virtualparts.PartsHandler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import uk.ac.ncl.intbio.virtualparts.entity.Interaction;
import uk.ac.ncl.intbio.virtualparts.entity.Part;

/**
 *
 * @author owengilfellon
 */
public class PartBundle  {


    private List<Interaction> internalInteractions = new ArrayList<>();
    private Part part;
    protected SBMLDocument document = null;
    protected Map<Interaction, SBMLDocument> interactions = new HashMap<>();
    private static final String SVP_REPOSITORY_URL = "http://sbol.ncl.ac.uk:8081";
    protected final PartsHandler PART_HANDLER = new PartsHandler(SVP_REPOSITORY_URL);

    /**
     * Constructor for a Compilable encapsulating an SVP with no interactions with other SVPs in model
     * @param svp
     * @param document SBMLDocument for the SVP
     */
    public PartBundle(Part part, List<Interaction> internalInteractions, SBMLDocument document)
    {
        this.document = document;
        this.internalInteractions = internalInteractions;
        this.part = part;
    }

    /**
     * Constructor for a Compilable encapsulating an SVP with interactions with other SVPs in model
     * @param svp
     * @param document SBMLDocument for the SVP
     * @param interactions Interactions with other SVPs, mapping Interactions to SBMLDocuments
     */
    public PartBundle(Part part, List<Interaction> internalInteractions, SBMLDocument document, Map<Interaction, SBMLDocument> interactions)
    {
        this.document = document;
        this.interactions = interactions;
        this.internalInteractions = internalInteractions;
        this.part = part;
    }

    public Part getPart() { return part; }

    public SBMLDocument getPartDocument() {
        return document;
    }

    public Map<Interaction, SBMLDocument> getInteractions() {
        return interactions;
    }
    
    public List<Interaction> getInternalInteractions() {
        return internalInteractions;
    }



}
