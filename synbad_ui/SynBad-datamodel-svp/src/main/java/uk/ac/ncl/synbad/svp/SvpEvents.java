/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.synbad.svp;

import uk.ac.ncl.icos.synbad.svp.rdf.Participation;
import uk.ac.ncl.icos.synbad.svp.rdf.SvpInteraction;
import uk.ac.ncl.icos.synbad.svp.rdf.Sequence;
import static uk.ac.ncl.icos.synbad.datadefinition.types.EntityType.Component;


/**
 *
 * @author owengilfellon
 */
public class SvpEvents {
    
    public static class ParticipationEvent extends AEvent<SvpInteraction, Participation> {

        public ParticipationEvent(EventType eventType, SvpInteraction source, Participation object) {
            super(eventType, source, object);
        }

    }
    
     public static class SequenceEvent extends AEvent<Component, Sequence> {

        public SequenceEvent(EventType eventType, Component source, Sequence sequence) {
            super(eventType, source, sequence);
        }
        
    }
 
}
