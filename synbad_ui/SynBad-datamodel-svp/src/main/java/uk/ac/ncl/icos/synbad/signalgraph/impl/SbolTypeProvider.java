/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.signalgraph.impl;

import org.openide.util.lookup.ServiceProvider;
import uk.ac.ncl.icos.synbad.datadefinition.IDataDefinitionManager;
import uk.ac.ncl.icos.synbad.datadefinition.IDataDefinitionProvider;

/**
 *
 * @author owengilfellon
 */
@ServiceProvider(service = IDataDefinitionProvider.class)
public class SbolTypeProvider implements IDataDefinitionProvider {

    @Override
    public void addDefinitions(IDataDefinitionManager manager) {
        for(SynBadPortState state : SynBadPortState.values()) {
            manager.addDefinition(state);
        }
        
        for(SynBadPortType type : SynBadPortType.values()) {
            manager.addDefinition(type);
        }
    }
}
