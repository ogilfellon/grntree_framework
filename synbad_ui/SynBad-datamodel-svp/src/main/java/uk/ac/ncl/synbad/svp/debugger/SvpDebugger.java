/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.synbad.svp.debugger;

import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Collection;
import org.sbolstandard.core2.Annotation;
import uk.ac.ncl.icos.datamodel.workspace.objects.SynBadPObject;
import uk.ac.ncl.icos.datamodel.workspace.objects.IPort;
import uk.ac.ncl.icos.datamodel.workspace.objects.IWire;
import uk.ac.ncl.icos.datamodel.workspace.impl.EntityInstance;
import uk.ac.ncl.icos.synbad.parsers.PartBundle;
import uk.ac.ncl.intbio.virtualparts.entity.Constraint;
import uk.ac.ncl.intbio.virtualparts.entity.Interaction;
import uk.ac.ncl.intbio.virtualparts.entity.InteractionPartDetail;
import uk.ac.ncl.intbio.virtualparts.entity.Parameter;
import uk.ac.ncl.intbio.virtualparts.entity.Part;
import uk.ac.ncl.intbio.virtualparts.entity.Property;

/**
 *
 * @author owengilfellon
 */
public class SvpDebugger {
    
    public static void debugBundles(Collection<PartBundle> bundles, OutputStream os) {
        for(PartBundle bundle : bundles)
            debugBundle(bundle, os);
    }
    
    public static void debugBundle(PartBundle bundle, OutputStream os) {
        debugPart(bundle.getPart(), os);
        for(Interaction interaction : bundle.getInternalInteractions()) {
            debugInteraction(interaction, os);
        }
    }
    
    public static void debugParts(Collection<Part> parts, OutputStream os) {
        for(Part part : parts) {
            debugPart(part, os);
        }
    }
    
    public static void debugPart(Part part, OutputStream os) {
        PrintWriter writer = new PrintWriter(os);
        writer.println("============================================");
        writer.println(part.getName());
        writer.println("============================================");
        
       
        writer.printf("%-15s: %s\n", "DisplayName", part.getDisplayName());
        writer.printf("%-15s: %s\n", "Description", part.getDescription());
        writer.printf("%-15s: %s\n", "DesignMethod", part.getDesignMethod());
        writer.printf("%-15s: %s\n", "MetaType", part.getMetaType());
        writer.printf("%-15s: %s\n", "Organism", part.getOrganism());
        writer.printf("%-15s: %s\n", "Sequence", part.getSequence());
        writer.printf("%-15s: %s\n", "Status", part.getStatus());
        writer.printf("%-15s: %s\n", "Type", part.getType());
        
        
        
        
        for(Property p : part.getProperties()) {
            writer.printf("%-15s: %-10s %s\n", p.getName(), p.getValue(), p.getDescription());
        }
        
        
        
        writer.flush();

    }
    
    public static void debugInteraction(Interaction interaction, OutputStream os) {
        PrintWriter writer = new PrintWriter(os);
        
       
        
        writer.println("============================================");
        writer.println(interaction.getName());
        writer.println("============================================");
        writer.printf("%-15s: %s\n", "Description", interaction.getDescription());
        writer.printf("%-15s: %s\n", "Type", interaction.getInteractionType());
        writer.printf("%-15s: %s\n", "MathName", interaction.getMathName());
        writer.printf("%-15s: %s\n", "FreeTextMath", interaction.getFreeTextMath());
        writer.printf("%-15s: %s\n", "IsInternal", interaction.getIsInternal());
        writer.printf("%-15s: %s\n", "IsReaction", interaction.getIsReaction());
        writer.printf("%-15s: %s\n", "IsReversible", interaction.getIsReversible());
        
    
        if(interaction.getParts() != null) {
            for(String s : interaction.getParts()) {
                writer.printf("%-15s: %s\n", "Part",s);
            }
        }

        if(interaction.getParameters() != null) {
        
            for(Parameter p : interaction.getParameters()) {
                writer.printf("%-15s: %s\n", "Parameter", p.getName());
                writer.printf("\t%-15s: %s\n", "Type", p.getParameterType());
                writer.printf("\t%-15s: %f\n", "Value", p.getValue());
                writer.printf("\t%-15s: %s\n", "Scope", p.getScope());
                writer.printf("\t%-15s: %s\n", "Evidence", p.getEvidenceType());
            }
        }
        
        if(interaction.getPartDetails()!=null) {
            for(InteractionPartDetail p : interaction.getPartDetails()) {
                writer.println("IPD: ");
                writer.printf("\t%-15s: %s\n", "Name", p.getPartName());
                writer.printf("\t%-15s: %s\n", "Form", p.getPartForm());
                writer.printf("\t%-15s: %s\n", "Role", p.getInteractionRole());
                writer.printf("\t%-15s: %d\n", "Stoichiometery", p.getNumberOfMolecules());
                writer.printf("\t%-15s: %s\n", "Evidence", p.getMathName());
            }
        }
        
        if(interaction.getConstraints()!=null) {
            for(Constraint c : interaction.getConstraints()) {
                writer.println("Constraint: ");
                writer.printf("\t%-15s: %s\n", "Qualifier", c.getQualifier());
                writer.printf("\t%-15s: %s\n", "SourceID", c.getSourceID());
                writer.printf("\t%-15s: %s\n", "SourceType", c.getSourceType());
                writer.printf("\t%-15s: %s\n", "TargetId", c.getTargetID());
                writer.printf("\t%-15s: %s\n", "TargetType", c.getTargetType());
            }
        }
        
        writer.flush();

    }

    
    public static void debugEntities(Collection<SynBadPObject> entities, OutputStream os) {
        for(SynBadPObject entity : entities) {
            debugEntity(entity, os);
        }
    }
    
    public static void debugEntity(SynBadPObject entity, OutputStream os) {
        PrintWriter writer = new PrintWriter(os);
        writer.println("============================================");
        writer.println(entity.getIdentity().toASCIIString());
        writer.println("============================================");
        writer.printf("%-15s: %s\n", "Name", entity.getName());
        writer.printf("%-15s: %s\n", "Description", entity.getDescription());
        
        for(EntityInstance child : entity.getChildren()) {
             writer.printf("%-15s: %s\n", "Child", child.getValue().getName());
        }
        
        for(IPort port : entity.getPorts()) {
            writer.printf("%-15s: %s\n", "Port", port.toString());
        }
        
        for(IWire wire : entity.getWires()) {
            writer.printf("%-15s: %s\n", "Wire", wire.toString());
        }
        
        for(Annotation a : entity.getAnnotations()) {
            debugAnnotation(a, writer, 0);
        }
        
        writer.flush();

    }
    
    public static void debugAnnotation(Annotation a, PrintWriter writer, int depth) {

        String format = "";
        
        for(int i = 0; i<depth; i++) {
            format +="\t";
        }
        
        format += "%-15s: %s\n";
        
        if(a.isBooleanValue()) {
            writer.printf(format, a.getQName().getLocalPart(), "[boolean] " + a.getBooleanValue());
        } else if (a.isDoubleValue()) {
            writer.printf(format, a.getQName().getLocalPart(), "[double] " + a.getDoubleValue());
        } else if (a.isIntegerValue()) {
            writer.printf(format, a.getQName().getLocalPart(), "[integer] " + a.getIntegerValue());
        } else if (a.isStringValue()) {
            writer.printf(format, a.getQName().getLocalPart(), "[string] " + a.getStringValue());
        } else if (a.isNestedAnnotations()) {
            writer.printf(format, a.getQName().getLocalPart(), "[annotation] " + a.getNestedIdentity().toASCIIString());
            for(Annotation nestedAnnotation : a.getAnnotations()) {
                debugAnnotation(nestedAnnotation, writer, depth+1);
            }
        } else {
            writer.printf(format, a.getQName().getLocalPart(), "[uri] " + a.getURIValue().toASCIIString());
        }
        
        writer.flush();
    }
    
}
