/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.command;

import java.net.URI;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Stack;
import org.sbolstandard.core2.Annotation;
import uk.ac.ncl.icos.datamodel.workspace.objects.IPort;
import uk.ac.ncl.icos.datamodel.workspace.impl.PortDirection;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentRole;
import uk.ac.ncl.icos.synbad.datadefinition.types.EntityType;
import uk.ac.ncl.icos.synbad.datadefinition.types.ModuleRole;
import uk.ac.ncl.icos.synbad.signalgraph.impl.SynBadPortState;
import uk.ac.ncl.icos.synbad.signalgraph.impl.SynBadPortType;
import uk.ac.ncl.icos.datamodel.workspace.objects.SynBadPObject;
import uk.ac.ncl.icos.datamodel.workspace.api.SBWorkspace;
import uk.ac.ncl.icos.datamodel.actions.ReturnableAction;
import uk.ac.ncl.icos.datamodel.actions.WorkspaceAction;
import uk.ac.ncl.icos.datamodel.workspace.impl.EntityInstance;
import uk.ac.ncl.icos.datamodel.workspace.impl.Identity;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;

/**
 *
 * @author owengilfellon
 */
public class SVPCommands {
    
    public static class CreatePromoter implements ReturnableAction<SynBadPObject> {

        private final SBWorkspace workspace;
        private final Identity identity;
        private final Stack<WorkspaceAction> commands = new Stack<>();
        
        private final Annotation type;
        private final Annotation role;
        private final Annotation svpType;

        private SynBadPObject entity;

        public CreatePromoter(SBWorkspace workspace, Identity identity) {
            this.identity = identity;
            this.workspace = workspace;
            this.type = new Annotation(SynBadTerms.Annotatable.entitytype,  EntityType.Module.getUri());
            this.role = new Annotation(SynBadTerms.Annotatable.role, ComponentRole.Promoter.getUri());
            this.svpType = new Annotation(SynBadTerms.Annotatable.role, ModuleRole.SVPPart.getUri());
        }

        @Override
        public SynBadPObject performAndReturn() {
            perform();
            return entity;
        }
        
        private void firstTimeSetup()
        {
            entity = ((ReturnableAction<SynBadPObject>) commands.push(
               new ReturnableAction.CreateEntity(workspace, identity))).performAndReturn();
            
            SynBadPObject dna = ((ReturnableAction<SynBadPObject>) commands.push(
                new ComponentCommands.CreateDna(workspace, new Identity(identity.getUriPrefix(), identity.getDisplayID() + "_dna", identity.getVersion()), ComponentRole.Promoter))).performAndReturn();
            
            SynBadPObject popsProduction = ((ReturnableAction<SynBadPObject>) commands.push(
                new SVICommands.CreatePopsProduction(workspace, new Identity(identity.getUriPrefix(), identity.getDisplayID() + "_popsProduction", identity.getVersion()), 1.0))).performAndReturn();

            commands.push(new WorkspaceAction.Macro(
                new WorkspaceAction.AddAnnotation(entity, type),
                new WorkspaceAction.AddAnnotation(entity, role),
                new WorkspaceAction.AddAnnotation(entity, svpType),
                new ReturnableAction.CreatePort(entity, PortDirection.IN, SynBadPortType.PoPS, true),
                new ReturnableAction.CreateChild(entity, dna),
                new ReturnableAction.CreateChild(entity, popsProduction))).perform();

            commands.push(new ReturnableAction.CreateProxy(entity, popsProduction.getPort((IPort port) -> port.getPortDirection() == PortDirection.OUT), true)).perform();
        }
        
        @Override
        public void perform() {

            if(entity == null) {
                firstTimeSetup();
            } else {
                LinkedList l = new LinkedList(commands);
                Iterator<WorkspaceAction> i = l.iterator();
                while(i.hasNext()) {
                    i.next().perform();
                }
            }
            

        }
   
        @Override
        public void undo() {

            LinkedList l = new LinkedList(commands);
            Iterator<WorkspaceAction> i = l.descendingIterator();
            while(i.hasNext()) {
                i.next().undo();
            }

        }
        
                @Override
        public SynBadPObject getValue() {
            return entity;
        }
        
    }
    
    public static class CreateInduciblePromoter implements ReturnableAction<SynBadPObject> {

        private final SBWorkspace workspace;
        private final Identity identity;
        private final Stack<WorkspaceAction> commands = new Stack<>();
        
        private final Annotation type;
        private final Annotation role;
        private final Annotation svpType;
        
        private final SynBadPortType modulator;
        private final SynBadPortState modulatorState;

        private SynBadPObject entity;

        public CreateInduciblePromoter(SBWorkspace workspace, Identity identity, SynBadPortType modulator, SynBadPortState modulatorState) {
            this.identity = identity;
            this.workspace = workspace;
            this.type = new Annotation(SynBadTerms.Annotatable.entitytype,  EntityType.Module.getUri());
            this.role = new Annotation(SynBadTerms.Annotatable.role, ComponentRole.Promoter.getUri());
            this.svpType = new Annotation(SynBadTerms.Annotatable.role, ModuleRole.SVPPart.getUri());
            this.modulator = modulator;
            this.modulatorState = modulatorState;
        }

        @Override
        public SynBadPObject performAndReturn() {
            perform();
            return entity;
        }
        
        private void firstTimeSetup()
        {
            entity = ((ReturnableAction<SynBadPObject>) commands.push(
               new ReturnableAction.CreateEntity(workspace, identity))).performAndReturn();
            SynBadPObject dna = ((ReturnableAction<SynBadPObject>) commands.push(new ComponentCommands.CreateDna(workspace, new Identity(identity.getUriPrefix(), identity.getDisplayID()+"_dna", identity.getVersion()), ComponentRole.Promoter))).performAndReturn();
            SynBadPObject popsProduction = ((ReturnableAction<SynBadPObject>) commands.push(new SVICommands.CreatePopsProduction(workspace, new Identity(identity.getUriPrefix(), identity.getDisplayID()+"_popsProduction", identity.getVersion()), 1.0))).performAndReturn();
            SynBadPObject popsModulation = ((ReturnableAction<SynBadPObject>) commands.push(new SVICommands.CreatePopsModulation(workspace, new Identity(identity.getUriPrefix(), identity.getDisplayID()+"_popsModulation", identity.getVersion()),  modulator, modulatorState))).performAndReturn();
            EntityInstance iProduction = ((ReturnableAction<EntityInstance>) commands.push(
                new ReturnableAction.CreateChild(entity, popsProduction))).performAndReturn();
            EntityInstance iModulation = ((ReturnableAction<EntityInstance>) commands.push(
                new ReturnableAction.CreateChild(entity, popsModulation))).performAndReturn();

            commands.push(new WorkspaceAction.Macro(
                new WorkspaceAction.AddAnnotation(entity, type),
                new WorkspaceAction.AddAnnotation(entity, role),
                new WorkspaceAction.AddAnnotation(entity, svpType),
                new ReturnableAction.CreatePort(entity, PortDirection.IN, SynBadPortType.PoPS, true),
                new ReturnableAction.CreateProxy(entity, popsModulation.getPort((IPort port) -> port.getPortDirection() == PortDirection.IN), true),
                new ReturnableAction.CreateProxy(entity, popsModulation.getPort((IPort port) -> port.getPortDirection() == PortDirection.OUT), true),
                new ReturnableAction.WirePorts(entity, iProduction.getPort(null), iModulation.getPort(null)))).perform();
        }
        
        @Override
        public void perform() {

            if(entity == null) {
                firstTimeSetup();
            } else {
                LinkedList l = new LinkedList(commands);
                Iterator<WorkspaceAction> i = l.iterator();
                while(i.hasNext()) {
                    i.next().perform();
                }
            }
            

        }
   
        @Override
        public void undo() {

            LinkedList l = new LinkedList(commands);
            Iterator<WorkspaceAction> i = l.descendingIterator();
            while(i.hasNext()) {
                i.next().undo();
            }

        }
        
        @Override
        public SynBadPObject getValue() {
            return entity;
        }
        
    }
    
    public static class CreateMrna implements ReturnableAction<SynBadPObject> {

        private final SBWorkspace workspace;
        private final Identity identity;
        private final Stack<WorkspaceAction> commands = new Stack<>();
        
        private final Annotation type;
        private final Annotation role;
        private final Annotation svpType;

        private SynBadPObject entity;

        public CreateMrna(SBWorkspace workspace, Identity identity) {
            this.identity = identity;
            this.workspace = workspace;
            this.type = new Annotation(SynBadTerms.Annotatable.entitytype,  EntityType.Module.getUri());
            this.role = new Annotation(SynBadTerms.Annotatable.role, ComponentRole.mRNA.getUri());
            this.svpType = new Annotation(SynBadTerms.Annotatable.role, ModuleRole.SVPPart.getUri());
        }

        @Override
        public SynBadPObject performAndReturn() {
            perform();
            return entity;
        }
        
        private void firstTimeSetup()
        {
            entity = ((ReturnableAction<SynBadPObject>) commands.push(
               new ReturnableAction.CreateEntity(workspace, identity))).performAndReturn();
            SynBadPObject mrnaProduction = ((ReturnableAction<SynBadPObject>) commands.push(new SVICommands.CreateMrnaProduction(workspace, new Identity(identity.getUriPrefix(), identity.getDisplayID()+"_mRNAProduction", identity.getVersion())))).performAndReturn();

            commands.push(new WorkspaceAction.Macro(
                new WorkspaceAction.AddAnnotation(entity, type),
                new WorkspaceAction.AddAnnotation(entity, role),
                new WorkspaceAction.AddAnnotation(entity, svpType),
                new ReturnableAction.CreateChild(entity, mrnaProduction))).perform();
            
            commands.push(new ReturnableAction.CreateProxy(entity, mrnaProduction.getPort((IPort port) -> port.getPortDirection() == PortDirection.IN), true)).perform();
            commands.push(new ReturnableAction.CreateProxy(entity, mrnaProduction.getPort((IPort port) -> port.getPortDirection() == PortDirection.OUT), true)).perform();
        }
        
        @Override
        public void perform() {

            if(entity == null) {
                firstTimeSetup();
            } else {
                LinkedList l = new LinkedList(commands);
                Iterator<WorkspaceAction> i = l.iterator();
                while(i.hasNext()) {
                    i.next().perform();
                }
            }
        }
   
        @Override
        public void undo() {

            LinkedList l = new LinkedList(commands);
            Iterator<WorkspaceAction> i = l.descendingIterator();
            while(i.hasNext()) {
                i.next().undo();
            }
        }
        
                @Override
        public SynBadPObject getValue() {
            return entity;
        }
    }
    
    public static class CreateOperator implements ReturnableAction<SynBadPObject> {

        private final SBWorkspace workspace;
        private final Identity identity;
        private final Stack<WorkspaceAction> commands = new Stack<>();
        
        private final Annotation type;
        private final Annotation role;
        private final Annotation svpType;

        private final SynBadPortType modulator;
        private final SynBadPortState modulatorState;

        private SynBadPObject entity;

        public CreateOperator(SBWorkspace workspace, Identity identity, SynBadPortType modulator, SynBadPortState modulatorState) {
            this.identity = identity;
            this.workspace = workspace;
            this.type = new Annotation(SynBadTerms.Annotatable.entitytype,  EntityType.Module.getUri());
            this.role = new Annotation(SynBadTerms.Annotatable.role, ComponentRole.Promoter.getUri());
            this.svpType = new Annotation(SynBadTerms.Annotatable.role, ModuleRole.SVPPart.getUri());
            this.modulator = modulator;
            this.modulatorState = modulatorState;
        }


        @Override
        public SynBadPObject performAndReturn() {
            perform();
            return entity;
        }
        
        private void firstTimeSetup()
        {
            entity = ((ReturnableAction<SynBadPObject>) commands.push(
               new ReturnableAction.CreateEntity(workspace, identity))).performAndReturn();
            SynBadPObject popsModulation = ((ReturnableAction<SynBadPObject>) commands.push(new SVICommands.CreatePopsModulation(workspace, new Identity(identity.getUriPrefix(), identity.getDisplayID()+"_popsModulation", identity.getVersion()), modulator, modulatorState))).performAndReturn();

            commands.push(new WorkspaceAction.Macro(
                new WorkspaceAction.AddAnnotation(entity, type),
                new WorkspaceAction.AddAnnotation(entity, role),
                new WorkspaceAction.AddAnnotation(entity, svpType),
                new ReturnableAction.CreateChild(entity, popsModulation),
                new ReturnableAction.CreateProxy(entity, popsModulation.getPort((IPort port) -> port.getPortDirection() == PortDirection.IN), true),
                new ReturnableAction.CreateProxy(entity, popsModulation.getPort((IPort port) -> port.getPortDirection() == PortDirection.OUT), true))).perform();
        }
        
        @Override
        public void perform() {

            if(entity == null) {
                firstTimeSetup();
            } else {
                LinkedList l = new LinkedList(commands);
                Iterator<WorkspaceAction> i = l.iterator();
                while(i.hasNext()) {
                    i.next().perform();
                }
            }
        }
   
        @Override
        public void undo() {

            LinkedList l = new LinkedList(commands);
            Iterator<WorkspaceAction> i = l.descendingIterator();
            while(i.hasNext()) {
                i.next().undo();
            }
        }
        
                @Override
        public SynBadPObject getValue() {
            return entity;
        }
    }
    
    public static class CreateRBS implements ReturnableAction<SynBadPObject> {

        private final SBWorkspace workspace;
        private final Identity identity;
        private final Stack<WorkspaceAction> commands = new Stack<>();
        
        private final Annotation type;
        private final Annotation role;
        private final Annotation svpType;

        private SynBadPObject entity;

        public CreateRBS(SBWorkspace workspace, Identity identity) {
            this.identity = identity;
            this.workspace = workspace;
            this.type = new Annotation(SynBadTerms.Annotatable.entitytype,  EntityType.Module.getUri());
            this.role = new Annotation(SynBadTerms.Annotatable.role, ComponentRole.RBS.getUri());
            this.svpType = new Annotation(SynBadTerms.Annotatable.role, ModuleRole.SVPPart.getUri());
        }


        @Override
        public SynBadPObject performAndReturn() {
            perform();
            return entity;
        }
        
        private void firstTimeSetup()
        {
            entity = ((ReturnableAction<SynBadPObject>) commands.push(
               new ReturnableAction.CreateEntity(workspace, identity))).performAndReturn();
            SynBadPObject mrnaSvp = ((ReturnableAction<SynBadPObject>) commands.push(
                new CreateMrna(workspace, new Identity(identity.getUriPrefix(), identity.getDisplayID()+ "_mRNA", identity.getVersion())))).performAndReturn();
            SynBadPObject ripsProd = ((ReturnableAction<SynBadPObject>) commands.push(new SVICommands.CreateRipsProduction(workspace, new Identity(identity.getUriPrefix(), identity.getDisplayID()+"_ripsProduction", identity.getVersion())))).performAndReturn();
            EntityInstance iMrna = ((ReturnableAction<EntityInstance>) commands.push(
                new ReturnableAction.CreateChild(entity, mrnaSvp))).performAndReturn();
             EntityInstance iRips = ((ReturnableAction<EntityInstance>) commands.push(
                new ReturnableAction.CreateChild(entity, ripsProd))).performAndReturn();
            
            
            commands.push(new WorkspaceAction.Macro(
                new WorkspaceAction.AddAnnotation(entity, type),
                new WorkspaceAction.AddAnnotation(entity, role),
                new WorkspaceAction.AddAnnotation(entity, svpType),
                new ReturnableAction.WireChildren(entity,
                        iMrna,
                        iRips, SynBadPortType.mRNA))).perform();
        
            commands.push(new ReturnableAction.CreateProxy(entity, mrnaSvp.getPort((IPort port) -> port.getPortDirection() == PortDirection.IN), true)).perform();
            commands.push(new ReturnableAction.CreateProxy(entity, ripsProd.getPort((IPort port) -> port.getPortDirection() == PortDirection.OUT), true)).perform();
        
        }
        
        @Override
        public void perform() {

            if(entity == null) {
                firstTimeSetup();
            } else {
                LinkedList l = new LinkedList(commands);
                Iterator<WorkspaceAction> i = l.iterator();
                while(i.hasNext()) {
                    i.next().perform();
                }
            }
        }
   
        @Override
        public void undo() {

            LinkedList l = new LinkedList(commands);
            Iterator<WorkspaceAction> i = l.descendingIterator();
            while(i.hasNext()) {
                i.next().undo();
            }
        }
        
                @Override
        public SynBadPObject getValue() {
            return entity;
        }
    }
    
    public static class CreateCDS implements ReturnableAction<SynBadPObject> {

        private final SBWorkspace workspace;
        private final Identity identity;
        private final Stack<WorkspaceAction> commands = new Stack<>();
        private final Annotation type, role, svpType;
        private final double km, kd;

        private SynBadPObject entity;

        public CreateCDS(SBWorkspace workspace, Identity identity, double km, double kd) {
            this.identity = identity;
            this.workspace = workspace;
            this.type = new Annotation(SynBadTerms.Annotatable.entitytype,  EntityType.Module.getUri());
            this.role = new Annotation(SynBadTerms.Annotatable.role, ComponentRole.CDS.getUri());
            this.svpType = new Annotation(SynBadTerms.Annotatable.role, ModuleRole.SVPPart.getUri());
            this.km = km;
            this.kd = kd;
        }


        @Override
        public SynBadPObject performAndReturn() {
            perform();
            return entity;
        }
        
        private void firstTimeSetup()
        {
            entity = ((ReturnableAction<SynBadPObject>) commands.push(
                    new ReturnableAction.CreateEntity(workspace, identity))).performAndReturn();
            SynBadPObject proteinProduction = ((ReturnableAction<SynBadPObject>) commands.push(
                    new SVICommands.CreateProteinProduction(workspace, new Identity(identity.getUriPrefix(), identity.getDisplayID()+"_proteinProduction", identity.getVersion()), km))).performAndReturn();
            SynBadPObject proteinDegradation = ((ReturnableAction<SynBadPObject>) commands.push(
                    new SVICommands.CreateProteinDegradation(workspace, new Identity(identity.getUriPrefix(), identity.getDisplayID()+"_proteinDegradation", identity.getVersion()), kd))).performAndReturn();
            EntityInstance iProd = ((ReturnableAction<EntityInstance>) commands.push(
                    new ReturnableAction.CreateChild(entity, proteinProduction))).performAndReturn();
            EntityInstance iDeg = ((ReturnableAction<EntityInstance>) commands.push(
                    new ReturnableAction.CreateChild(entity, proteinDegradation))).performAndReturn();
            
            commands.push(new WorkspaceAction.Macro(
                new WorkspaceAction.AddAnnotation(entity, type),
                new WorkspaceAction.AddAnnotation(entity, role),
                new WorkspaceAction.AddAnnotation(entity, svpType),
                new ReturnableAction.WireChildren(entity,
                        iProd, iDeg, SynBadPortType.Protein))).perform();
            
            IPort in = proteinProduction.getPort((IPort port) -> port.getPortDirection() == PortDirection.IN);
            IPort out = proteinProduction.getPort((IPort port) -> port.getPortDirection() == PortDirection.OUT);
            
            commands.push(new ReturnableAction.CreateProxy(entity, in, true)).perform();
            commands.push(new ReturnableAction.CreateProxy(entity, out, true)).perform();
        }
        
        @Override
        public void perform() {

            if(entity == null) {
                firstTimeSetup();
            } else {
                LinkedList l = new LinkedList(commands);
                Iterator<WorkspaceAction> i = l.iterator();
                while(i.hasNext()) {
                    i.next().perform();
                }
            }
        }
   
        @Override
        public void undo() {

            LinkedList l = new LinkedList(commands);
            Iterator<WorkspaceAction> i = l.descendingIterator();
            while(i.hasNext()) {
                i.next().undo();
            }
        }
        
                @Override
        public SynBadPObject getValue() {
            return entity;
        }
    }
        

     public static class CreateTerminator implements ReturnableAction<SynBadPObject> {

        private final SBWorkspace workspace;
        private final Identity identity;
        private final Stack<WorkspaceAction> commands = new Stack<>();
        private final Annotation type, role, svpType;

        private SynBadPObject entity;

        public CreateTerminator(SBWorkspace workspace, Identity identity) {
            this.identity = identity;
            this.workspace = workspace;
            this.type = new Annotation(SynBadTerms.Annotatable.entitytype,  EntityType.Module.getUri());
            this.role = new Annotation(SynBadTerms.Annotatable.role, ComponentRole.Terminator.getUri());
            this.svpType = new Annotation(SynBadTerms.Annotatable.role, ModuleRole.SVPPart.getUri());
        }


        @Override
        public SynBadPObject performAndReturn() {
            perform();
            return entity;
        }
        
        private void firstTimeSetup()
        {
            entity = ((ReturnableAction<SynBadPObject>) commands.push(
               new ReturnableAction.CreateEntity(workspace, identity))).performAndReturn();
            
            SynBadPObject dna = ((ReturnableAction<SynBadPObject>) commands.push(new ComponentCommands.CreateDna(workspace, new Identity(identity.getUriPrefix(), identity.getDisplayID()+"_dna", identity.getVersion()), ComponentRole.Terminator))).performAndReturn();
         
            commands.push(new WorkspaceAction.Macro(
                new ReturnableAction.CreateChild(entity, dna),
                new WorkspaceAction.AddAnnotation(entity, type),
                new WorkspaceAction.AddAnnotation(entity, role),
                new WorkspaceAction.AddAnnotation(entity, svpType))).perform();
        }
        
        @Override
        public void perform() {

            if(entity == null) {
                firstTimeSetup();
            } else {
                LinkedList l = new LinkedList(commands);
                Iterator<WorkspaceAction> i = l.iterator();
                while(i.hasNext()) {
                    i.next().perform();
                }
            }
        }
   
        @Override
        public void undo() {

            LinkedList l = new LinkedList(commands);
            Iterator<WorkspaceAction> i = l.descendingIterator();
            while(i.hasNext()) {
                i.next().undo();
            }
        }
        
        @Override
        public SynBadPObject getValue() {
            return entity;
        }
    }
}
