/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.rdf;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import uk.ac.ncl.icos.datamodel.workspace.objects.SynBadObject;
import uk.ac.ncl.icos.datamodel.workspace.objects.Identity;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentRole;
import uk.ac.ncl.icos.synbad.graph.api.SBValue;
import uk.ac.ncl.intbio.virtualparts.entity.Property;

/**
 *
 * @author owengilfellon
 */
public class Svp extends SynBadObject {
    
    private final ComponentRole role;

    public Svp(Identity identity, ComponentRole role) {
        super(identity);
        this.role = role;
    }
    
    public ComponentRole getRole() {
        return role;
    }
    
    public Collection<SvpInteraction> getInternalInteractions() { 
        return getWorkspace()
                .getEdges(this, "needInternalInteractionUri", SvpInteraction.class)
                .stream().map(e -> ws.getObject(e.getTo(), SvpInteraction.class))
                .collect(Collectors.toSet());

    }
    
    
    private String getStringFromValue(String predicate) {
        SBValue v = getValue(predicate);
        
        if(v == null || !v.isString() || v.asString().isEmpty())
            return null;
        
        else return v.asString();
    }
    
    public Sequence getSequence() {
        String sequence = getStringFromValue(SynBadTerms.VPR.sequence.toString());
        return sequence == null ? new Sequence("") : new Sequence(sequence);
    }
   
    public String getOrganism() {
        String organism = getStringFromValue(SynBadTerms.VPR.organism.toString());
        return organism == null ? "" : organism;
    }
    
    public Collection<Property> getProperties() {
        
        Set<Property> properties = new HashSet<>();
        
        // SynBadTerms.VPR.property
            // SynBadTerms.VPR.propertyName
            // SynBadTerms.VPR.propertyValue
            // SynBadTerms.VPR.propertydescription)) {
                
        
        return properties;
    }
    
}
