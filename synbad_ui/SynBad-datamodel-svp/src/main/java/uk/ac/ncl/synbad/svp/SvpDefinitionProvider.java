package uk.ac.ncl.synbad.svp;

import org.openide.util.lookup.ServiceProvider;
import uk.ac.ncl.icos.synbad.datadefinition.IDataDefinitionManager;
import uk.ac.ncl.icos.synbad.datadefinition.IDataDefinitionProvider;
import uk.ac.ncl.icos.synbad.signalgraph.impl.SynBadPortState;
import uk.ac.ncl.icos.synbad.signalgraph.impl.SynBadPortType;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author owengilfellon
 */

@ServiceProvider( service = IDataDefinitionProvider.class)
public class SvpDefinitionProvider implements IDataDefinitionProvider {

    @Override
    public void addDefinitions(IDataDefinitionManager manager) {

        // Add defined terms

        for(SynBadPortType type : SynBadPortType.values()) {
            manager.addDefinition(type);
        }

        for(SynBadPortState state : SynBadPortState.values()) {
            manager.addDefinition(state);
        }
    }

}
    
