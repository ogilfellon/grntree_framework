/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.workspace.factories;

import java.net.URI;
import java.util.Arrays;
import org.sbolstandard.core2.Annotation;
import uk.ac.ncl.icos.datamodel.workspace.impl.PortDirection;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentRole;
import uk.ac.ncl.icos.synbad.datadefinition.types.InteractionRole;
import uk.ac.ncl.icos.synbad.signalgraph.impl.SynBadPortState;
import uk.ac.ncl.icos.synbad.signalgraph.impl.SynBadPortType;
import uk.ac.ncl.icos.datamodel.workspace.objects.SynBadPObject;
import uk.ac.ncl.icos.datamodel.workspace.objects.IPort;
import uk.ac.ncl.icos.datamodel.workspace.api.SBWorkspace;
import uk.ac.ncl.icos.datamodel.workspace.impl.EntityInstance;
import uk.ac.ncl.icos.datamodel.workspace.impl.Identity;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.intbio.virtualparts.entity.Interaction;
import uk.ac.ncl.icos.synbad.svp.rdf.SvpInteraction;

/**
 *
 * @author owengilfellon
 */
public class SVIFactory {
    
    private static SvpInteraction getInteraction(SBWorkspace workspace, Identity identity, InteractionRole role) {
        SynBadPObject e = workspace.getEntity(identity.getIdentity());
        if(e instanceof SvpInteraction)
            return (SvpInteraction) e;
        else {
             SvpInteraction interaction = new  SvpInteraction(workspace, identity, role);
             workspace.addEntity(interaction);
             return interaction;
        }
    }

    
    public static SvpInteraction getProteinProduction(SBWorkspace workspace, String uriPrefix, String name, String version, double km, double n) {
        
        String id = name + "_Production";
        
        Identity identity = new Identity(uriPrefix, id, version);
        SvpInteraction proteinProduction = getInteraction(workspace, identity, InteractionRole.ProteinProduction);

        proteinProduction.setName(id);

        proteinProduction.addAnnotation(SynBadTerms.VPR.parameter,
                new Annotation( SynBadTerms.VPR.parameter, SynBadTerms.VPR.parameter, URI.create(proteinProduction.getIdentity() + "/Parameter/Km"), Arrays.asList(
                        new Annotation(SynBadTerms.VPR.parameterName, "Km"),
                        new Annotation(SynBadTerms.VPR.parameterType, "Km"),
                        new Annotation(SynBadTerms.VPR.parameterScope, "Local"),
                        new Annotation(SynBadTerms.VPR.parameterValue, km))));

        proteinProduction.addAnnotation(SynBadTerms.VPR.parameter,
                new Annotation(SynBadTerms.VPR.parameter, SynBadTerms.VPR.parameter, URI.create(proteinProduction.getIdentity() + "/Parameter/n"), Arrays.asList(
                    new Annotation(SynBadTerms.VPR.parameterName, "n"),
                    new Annotation(SynBadTerms.VPR.parameterType, "n"),
                    new Annotation(SynBadTerms.VPR.parameterScope, "Local"),
                    new Annotation(SynBadTerms.VPR.parameterValue, n))));

        SynBadPObject dna = ComponentFactory.getDna(workspace, uriPrefix, id, ComponentRole.CDS);
        SynBadPObject protein = ComponentFactory.getProtein(workspace, uriPrefix, id, SynBadPortState.Default);
        SynBadPObject proteinProductionInteraction = InteractionFactory.getProteinProduction(workspace, proteinProduction, uriPrefix, id);
        
        EntityInstance productionInstance = proteinProduction.addChild(proteinProductionInteraction);
        
        proteinProduction.wireChildren(proteinProduction.addChild(dna), productionInstance, SynBadPortType.DNA, null);
        proteinProduction.wireChildren(productionInstance, proteinProduction.addChild(protein), SynBadPortType.Protein, SynBadPortState.Default);
        
        proteinProductionInteraction.getPorts().stream()
                .filter(port -> port.getPortDirection() == PortDirection.IN)
                .filter(port -> port.hasAnnotation(SynBadTerms.Port.portType, SynBadPortType.RiPS.getUri()))
                    .forEach(p -> proteinProduction.createPort(p.getIdentity(), true));

        proteinProduction.createPort(protein.getPort((IPort port) -> port.getPortDirection() == PortDirection.OUT).getIdentity(), true);
        
        
        
        return proteinProduction;
    }

    
    public static SvpInteraction getProteinDegradation(SBWorkspace workspace, String uriPrefix, String name, String version, double kd) {
        
        String id = name + "_Degradation";
        
        Identity identity = new Identity(uriPrefix, id, version);
        SvpInteraction proteinDegradation = getInteraction(workspace, identity, InteractionRole.Degradation);
        
        proteinDegradation.setName(id);
        
       
        proteinDegradation.addAnnotation(SynBadTerms.VPR.parameter,
                new Annotation( SynBadTerms.VPR.parameter, SynBadTerms.VPR.parameter, URI.create(proteinDegradation.getIdentity() + "/Parameter/Kd"), Arrays.asList(
                        new Annotation(SynBadTerms.VPR.parameterName, "Kd"),
                        new Annotation(SynBadTerms.VPR.parameterType, "Kd"),
                        new Annotation(SynBadTerms.VPR.parameterScope, "Local"),
                        new Annotation(SynBadTerms.VPR.parameterValue, kd))));

        
        SynBadPObject degradation = InteractionFactory.getProteinDegradation(workspace, proteinDegradation, uriPrefix, id);
  
        proteinDegradation.addChild(degradation);
        proteinDegradation.createPort( degradation.getPort((IPort port) -> port.getPortDirection() == PortDirection.IN).getIdentity(), true);
        
        
        return proteinDegradation;
    }
    
    public static SvpInteraction getPoPSProduction(SBWorkspace workspace, String uriPrefix, String name, String version, double ktr, double n) {   
        
        String id = name + "_PoPSProduction";
        Identity identity = new Identity(uriPrefix, id, version);
        
        SvpInteraction popsProduction = getInteraction(workspace, identity, InteractionRole.PoPSProduction);

        popsProduction.setName(id);

        popsProduction.addAnnotation(SynBadTerms.VPR.parameter,
            AnnotationFactory.createParameter(popsProduction.getIdentity(), "n", "n", n, "Local", ""));
        
        popsProduction.addAnnotation(SynBadTerms.VPR.parameter,
            AnnotationFactory.createParameter(popsProduction.getIdentity(), "ktr", "ktr", ktr, "Local", ""));
        
        popsProduction.addAnnotation(SynBadTerms.VPR.parameter,
            AnnotationFactory.createParameter(popsProduction.getIdentity(), "PoPSOutput", "PoPS", null, "Output", null));

        //synbadInteraction.addPort(pf.getPort(synbadInteraction, URI.create(synbadInteraction.getIdentity().toString() + "/Port/In/DNA"), Direction.IN, SynBadPortType.DNA));

        SynBadPObject dna = ComponentFactory.getDna(workspace, uriPrefix, id, ComponentRole.Promoter);
        SynBadPObject popsProductionInteraction = InteractionFactory.getPoPSProduction(workspace, popsProduction, uriPrefix, id);

        popsProduction.wireChildren(popsProduction.addChild(dna), popsProduction.addChild(popsProductionInteraction), SynBadPortType.DNA, null);
        popsProduction.createPort(popsProductionInteraction.getPort((IPort port) -> port.getPortDirection().equals(PortDirection.OUT)).getIdentity(), true);
       
        return popsProduction;
    }
    
    public static SvpInteraction getmRNAProduction(SBWorkspace workspace, String uriPrefix, String name, String version) {
        String id = name + "_mRNAProduction";
        Identity identity = new Identity(uriPrefix, id, version);
        SvpInteraction mRNAProduction = getInteraction(workspace, identity, InteractionRole.mRNAProduction);

        
        mRNAProduction.setName(id);

        //synbadInteraction.addPort(pf.getPort(synbadInteraction, URI.create(synbadInteraction.getIdentity().toString() + "/Port/In/DNA"), Direction.IN, SynBadPortType.DNA));
        
        SynBadPObject mRNA = ComponentFactory.getmRNA(workspace, uriPrefix, id);
        SynBadPObject mRNAProductionInteraction = InteractionFactory.getmRNAProduction(workspace, mRNAProduction, uriPrefix, id);

        //mRNAProduction.addChild(mRNA);
        //mRNAProduction.addChild(mRNAProductionInteraction);
        
        mRNAProduction.wireChildren(mRNAProduction.addChild(mRNAProductionInteraction), mRNAProduction.addChild(mRNA), SynBadPortType.mRNA, null);
        
        mRNAProduction.createPort(mRNAProductionInteraction.getPort((IPort port) -> port.getPortDirection() == PortDirection.IN).getIdentity(), true);
        mRNAProduction.createPort(mRNA.getPort((IPort port) -> port.getPortDirection() == PortDirection.OUT).getIdentity(), true);
        
        
        
        return mRNAProduction;
    }
    
    public static SvpInteraction getRiPSProduction(SBWorkspace workspace, String uriPrefix, String name, String version, double ktl) {
        
        String id = name + "_RiPSProduction";
        Identity identity = new Identity(uriPrefix, id, version);
        SvpInteraction ripsProduction = getInteraction(workspace, identity, InteractionRole.RiPSProduction);

        ripsProduction.setName(id);
       
        SynBadPObject dna = ComponentFactory.getDna(workspace, uriPrefix, id, ComponentRole.RBS);
        SynBadPObject ripsProductionInteraction = InteractionFactory.getRiPSProduction(workspace, ripsProduction, uriPrefix, id);
        
        ripsProduction.addChild(dna);
        ripsProduction.addChild(ripsProductionInteraction);

        ripsProduction.createPort(ripsProductionInteraction.getPort((IPort port) -> port.getPortDirection() == PortDirection.IN).getIdentity(), true);
        ripsProduction.createPort(ripsProductionInteraction.getPort((IPort port) -> port.getPortDirection() == PortDirection.OUT).getIdentity(), true);

        ripsProduction.addAnnotation(SynBadTerms.VPR.parameter,
            AnnotationFactory.createParameter(ripsProduction.getIdentity(), "mRNAInput", "mRNA", null, "Input", null));

        ripsProduction.addAnnotation(SynBadTerms.VPR.parameter,
            AnnotationFactory.createParameter(ripsProduction.getIdentity(), "RiPSOutput", "RiPS", null, "Output", null));
        
        ripsProduction.addAnnotation(SynBadTerms.VPR.parameter,
            AnnotationFactory.createParameter(ripsProduction.getIdentity(), "ktl", "ktl", ktl, null, null));
        
        ripsProduction.addAnnotation(SynBadTerms.VPR.parameter,
            AnnotationFactory.createParameter(ripsProduction.getIdentity(), "volume", "volume", 1.0, "Global", null));
        
        return ripsProduction;
    }
    
    public static SvpInteraction getSmallMoleculePhosphorylation(SBWorkspace workspace, String uriPrefix, String name, String version, double kf) {
        
        String id = name + "_Phosphorylation";
        Identity identity = new Identity(uriPrefix, id, version);
        SvpInteraction phosphorylation = getInteraction(workspace, identity, InteractionRole.Phosphorylation);

        phosphorylation.setName(id);
        
        phosphorylation.addAnnotation(SynBadTerms.VPR.parameter,
            AnnotationFactory.createParameter(phosphorylation.getIdentity(), "kf", "kf", kf, "Local", null));
        
        phosphorylation.createPort(true, PortDirection.IN, SynBadPortType.SmallMolecule, SynBadPortState.Default, null);
        phosphorylation.createPort(true, PortDirection.OUT, SynBadPortType.SmallMolecule, SynBadPortState.Default, null);
        
        phosphorylation.createPort(true, PortDirection.IN, SynBadPortType.Protein, SynBadPortState.Default, null);
        phosphorylation.createPort(true, PortDirection.OUT, SynBadPortType.Protein, SynBadPortState.Phosphorylated, null);
        
        
        return phosphorylation;
    }
    
    public static SvpInteraction getPhosphorylation(SBWorkspace workspace, String uriPrefix, String name, String version, double kf) {
        
        Identity identity = new Identity(uriPrefix, name, version);
        SvpInteraction phosphorylation = getInteraction(workspace, identity, InteractionRole.Phosphorylation);

        phosphorylation.setName(name + "_Phosphorylation");

        phosphorylation.addAnnotation(SynBadTerms.VPR.parameter,
            AnnotationFactory.createParameter(phosphorylation.getIdentity(), "kf", "kf", kf, "Local", null));
        
        phosphorylation.createPort(true, PortDirection.IN, SynBadPortType.Protein, SynBadPortState.Default, null);
        phosphorylation.createPort(true, PortDirection.OUT, SynBadPortType.Protein, SynBadPortState.Phosphorylated, null);
        
        phosphorylation.createPort(true, PortDirection.IN, SynBadPortType.Protein, SynBadPortState.Phosphorylated, null);
        phosphorylation.createPort(true, PortDirection.OUT, SynBadPortType.Protein, SynBadPortState.Default, null);
        
        
        return phosphorylation;
    }
    
    public static SvpInteraction getDephosphorylation(SBWorkspace workspace, String uriPrefix, String name, String version, double kdeP) {
        
        String id = name + "_Dephosphorylation";
        Identity identity = new Identity(uriPrefix, id, version);
        SvpInteraction dephosphorylation = getInteraction(workspace, identity, InteractionRole.Dephosphorylation);
        dephosphorylation.setName(id);
      
        dephosphorylation.addAnnotation(SynBadTerms.VPR.parameter,
            AnnotationFactory.createParameter(dephosphorylation.getIdentity(), "kdeP", "kdeP", kdeP, "Local", null));
        
        dephosphorylation.createPort(true, PortDirection.IN, SynBadPortType.Protein, SynBadPortState.Phosphorylated, null);
        dephosphorylation.createPort(true, PortDirection.OUT, SynBadPortType.Protein, SynBadPortState.Default, null);

        return dephosphorylation;
    }
    
    public static SvpInteraction getPoPSModulation(SBWorkspace workspace, String uriPrefix, String name, String version, SynBadPortType modulator, SynBadPortState modulatorState) {
        
        String id = name + "_PoPSModulation";
        Identity identity = new Identity(uriPrefix, id, version);
        SvpInteraction popsModulation = getInteraction(workspace, identity, InteractionRole.PoPSModulation);

        popsModulation.setName(id);
        
        popsModulation.createPort(true, PortDirection.IN, modulator, modulatorState, null);
        popsModulation.createPort(true, PortDirection.IN, SynBadPortType.PoPS, null, null);
        popsModulation.createPort(true, PortDirection.OUT, SynBadPortType.PoPS, null, null);
        
        SynBadPObject dna = ComponentFactory.getDna(workspace, uriPrefix, id, ComponentRole.Operator);
        popsModulation.addChild(dna);
        
        
        return popsModulation;
    }


}
