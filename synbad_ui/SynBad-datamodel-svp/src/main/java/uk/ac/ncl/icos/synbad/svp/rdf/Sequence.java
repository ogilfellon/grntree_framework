/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.rdf;

/**
 *
 * @author owengilfellon
 */
public class Sequence {
    
    private final String sequence;

    public Sequence(String sequence) {
        this.sequence = sequence;
    }

    public String getSequence() {
        return sequence;
    }
    
    public int getLength() {
        return sequence.length();
    }
    
    
    
}
