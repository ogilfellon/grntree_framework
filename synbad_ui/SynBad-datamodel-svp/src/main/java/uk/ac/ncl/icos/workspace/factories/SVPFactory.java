/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.workspace.factories;

import uk.ac.ncl.icos.datamodel.workspace.objects.IPort;
import uk.ac.ncl.icos.datamodel.workspace.impl.PortDirection;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentRole;
import uk.ac.ncl.icos.synbad.signalgraph.impl.SynBadPortState;
import uk.ac.ncl.icos.synbad.signalgraph.impl.SynBadPortType;
import uk.ac.ncl.icos.datamodel.workspace.api.SBWorkspace;
import uk.ac.ncl.icos.datamodel.workspace.impl.EntityInstance;
import uk.ac.ncl.icos.datamodel.workspace.objects.Identity;
import uk.ac.ncl.icos.synbad.parsers.SVPParser;
import uk.ac.ncl.intbio.virtualparts.entity.Part;
import uk.ac.ncl.icos.synbad.svp.rdf.Svp;

/**
 * This should be refactored - used by Workspaces to map data behind the scenes.
 * @author owengilfellon
 */
public class SVPFactory {


    public static Svp getSVP(SBWorkspace workspace, Part part) {
        return new SVPParser(workspace).read(part);
    }

    public static Svp getPromoter(SBWorkspace workspace, String uriPrefix, String name, String version, double ktr, double n) {

        Svp svp = new Svp(
                new Identity(uriPrefix, name, version),
                ComponentRole.Promoter);

        svp.setName(name);
        
        EntityInstance popsProduction = svp.addChild(SVIFactory.getPoPSProduction(workspace, uriPrefix, name, version, ktr, n));

        svp.createPort(popsProduction.getValue().getPort((IPort port)
                -> port.getPortDirection() == PortDirection.OUT
                && port.getPortType().getUri().equals(SynBadPortType.PoPS.getUri())).getIdentity(), true);

        return svp;
    }
    
    public static Svp getInduciblePromoter(SBWorkspace workspace, String uriPrefix, String name, String version, double ktr, double n, SynBadPortType modulator, SynBadPortState modulatorState) {

        
        Svp svp = new Svp(workspace, new Identity(uriPrefix, name, version), ComponentRole.Promoter);
        workspace.addEntity(svp);
        svp.setName(name);
        
        EntityInstance popsProduction = svp.addChild(SVIFactory.getPoPSProduction(workspace, uriPrefix, name, version, ktr, n));
        EntityInstance popsModulation = svp.addChild(SVIFactory.getPoPSModulation(workspace, uriPrefix, name, version, modulator, modulatorState));

        svp.wireChildren(popsProduction, popsModulation, SynBadPortType.PoPS, null);

        svp.createPort( popsModulation.getValue().getPort((IPort port)
                -> port.getPortDirection() == PortDirection.IN
                && port.getPortType().getUri().equals(modulator.getUri())).getIdentity(), true);
        
        svp.createPort(popsModulation.getValue().getPort((IPort port)
                -> port.getPortDirection() == PortDirection.OUT
                && port.getPortType().getUri().equals(SynBadPortType.PoPS.getUri())).getIdentity(), true);

        return svp;
    }
    
    public static Svp getmRNA(SBWorkspace workspace, String uriPrefix, String name, String version) {

        String id = name + "_mrnaSVP";
        
        Svp svp = new Svp(workspace, new Identity(uriPrefix, id, version), ComponentRole.mRNA);
        workspace.addEntity(svp);
        
        SynBadObject mrnaProduction = SVIFactory.getmRNAProduction(workspace, uriPrefix, id, version);

        svp.setName(id);
        svp.addChild(mrnaProduction);
        svp.createPort(mrnaProduction.getPort((IPort port) -> port.getPortDirection() == PortDirection.IN).getIdentity(), true);
        svp.createPort(mrnaProduction.getPort((IPort port) -> port.getPortDirection() == PortDirection.OUT).getIdentity(), true);
        
        // add properties
        
    

        return svp;
    }
    
    public static Svp getOperator(SBWorkspace workspace, String uriPrefix, String name, String version, SynBadPortType modulator, SynBadPortState modulatorState) {

        
        Svp svp = new Svp(workspace, new Identity(uriPrefix, name, version), ComponentRole.Operator);
        workspace.addEntity(svp);
        SynBadObject popsModulation = SVIFactory.getPoPSModulation(workspace, uriPrefix, name, version, modulator, modulatorState);
        svp.setName(name);
        svp.addChild(popsModulation);
        svp.createPort(popsModulation.getPort((IPort port) -> port.getPortDirection() == PortDirection.IN).getIdentity(), true);
        svp.createPort(popsModulation.getPort((IPort port) -> port.getPortDirection() == PortDirection.OUT).getIdentity(), true);

        return svp;
    }
    
    public static Svp getRBS(SBWorkspace workspace, String uriPrefix, String name, String version, double ktl) {

        Svp svp = new Svp(workspace, new Identity(uriPrefix, name, version), ComponentRole.RBS);
        workspace.addEntity(svp);
        svp.setName(name);
        
        SynBadObject mRNA = getmRNA(workspace, uriPrefix, name, version);
        SynBadObject ripsProduction = SVIFactory.getRiPSProduction(workspace, uriPrefix, name, version, ktl);        
        EntityInstance mRNAInstance = svp.addChild(mRNA);
        EntityInstance ripsInstance = svp.addChild(ripsProduction);
        svp.wireChildren(mRNAInstance, ripsInstance, SynBadPortType.mRNA, null);
        svp.createPort(mRNA.getPort((IPort port) -> port.getPortDirection() == PortDirection.IN).getIdentity(), true);
        svp.createPort(ripsProduction.getPort((IPort port) -> port.getPortDirection() == PortDirection.OUT &&
                port.getPortType() == SynBadPortType.RiPS).getIdentity(), true);
        
        return svp;
    }
    
    public static Svp getCDS(SBWorkspace workspace, String uriPrefix, String name, String version, double km, double n, double kd) {

        Svp svp = new Svp(workspace, new Identity(uriPrefix, name, version), ComponentRole.CDS);
        workspace.addEntity(svp);
        SynBadObject proteinProduction = SVIFactory.getProteinProduction(workspace, uriPrefix, name, version, km, n);
        SynBadObject proteinDegradation = SVIFactory.getProteinDegradation(workspace, uriPrefix, name, version, kd);
        
        svp.setName(name);
        svp.wireChildren(svp.addChild(proteinProduction), svp.addChild(proteinDegradation), SynBadPortType.Protein, null);
        
        proteinProduction.getPorts().stream()
            .filter(port -> port.getPortDirection() == PortDirection.OUT)
            .filter(port -> port.getPortType().getUri().equals(SynBadPortType.Protein.getUri())).forEach(
                port -> svp.createPort(port.getIdentity(), true));               

        proteinProduction.getPorts().stream()
            .filter(port -> port.getPortDirection() == PortDirection.IN)
            .filter(port -> port.getPortType().getUri().equals(SynBadPortType.RiPS.getUri())).forEach(
                port -> svp.createPort(port.getIdentity(), true));  

        return svp;
    }
    
    public static Svp getTerminator(SBWorkspace workspace, String uriPrefix, String name, String version) {

        Svp svp = new Svp(workspace, new Identity(uriPrefix, name, version), ComponentRole.Terminator);
        workspace.addEntity(svp);
        SynBadObject dna = ComponentFactory.getDna(workspace, uriPrefix, name, ComponentRole.Terminator);
        svp.setName(name);
        svp.addChild(dna);

        return svp;
    }
}
