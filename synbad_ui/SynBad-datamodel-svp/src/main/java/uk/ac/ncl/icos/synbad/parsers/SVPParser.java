/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.parsers;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.sbolstandard.core2.Annotation;
import uk.ac.ncl.icos.synbad.datadefinition.IDataDefinitionManager;
import uk.ac.ncl.icos.synbad.datadefinition.UriHelper;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentRole;
import uk.ac.ncl.icos.datamodel.workspace.objects.SynBadPObject;
import uk.ac.ncl.icos.datamodel.workspace.api.SBWorkspace;
import uk.ac.ncl.icos.datamodel.workspace.impl.EntityInstance;
import uk.ac.ncl.icos.datamodel.workspace.impl.Identity;
import uk.ac.ncl.icos.datamodel.workspace.impl.PortDirection;
import uk.ac.ncl.icos.datamodel.workspace.impl.PortRecord;
import uk.ac.ncl.icos.svpmanager.SVPManager;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.datadefinition.types.InteractionRole;
import uk.ac.ncl.icos.workspace.factories.ComponentFactory;
import uk.ac.ncl.intbio.core.datatree.NamespaceBinding;
import uk.ac.ncl.intbio.virtualparts.entity.Interaction;
import uk.ac.ncl.intbio.virtualparts.entity.Part;
import uk.ac.ncl.intbio.virtualparts.entity.Property;
import uk.ac.ncl.icos.synbad.svp.rdf.Svp;
import uk.ac.ncl.icos.synbad.svp.rdf.SvpInteraction;
import uk.ac.ncl.synbad.svp.debugger.SvpDebugger;

/**
 *
 * @author owengilfellon
 */
public class SVPParser {
    

    private final NamespaceBinding namespace = UriHelper.vpr;
    private final SBWorkspace workspace;
    private final IDataDefinitionManager definitionManager = new IDataDefinitionManager.DefaultDataDefinitionManager();
    private final SVPInteractionParser interactionParser;


    public SVPParser(SBWorkspace workspace)
    {
        interactionParser = new SVPInteractionParser(workspace);
        this.workspace = workspace;
    }

    public Svp read(Part part)
    {
        
        //SvpDebugger.debugPart(part, System.out);
        // create svp
        
        ComponentRole c = definitionManager.getDefinition(ComponentRole.class, part.getType());
        Svp svp = new Svp(workspace, new Identity(namespace.getNamespaceURI(), part.getName(), "1.0"), c);
        workspace.addEntity(svp);

        // populate with properties

      
        readPartProperties(svp, part);

        // create SvpInteractions

        for(Interaction svpInteraction : SVPManager.getSVPManager().getInternalEvents(part)) {
            svp.addChild(interactionParser.read(svpInteraction));
        }
        
        // Wire SvpInteractions

        wireChildren(svp);
        
        // Export proxies
        
        for(EntityInstance entity : svp.getChildren()) {
            for(PortRecord port : entity.getPorts()) {
                if(!port.isWired()) {
                    svp.createPort(port.getValue().getIdentity(), true);
                }
            }
        } 
        
        svp.addChild(ComponentFactory.getDna(workspace, UriHelper.vpr.getNamespaceURI(), part.getName(), c));
    
        return svp;
        
    }
    
      private void wireChildren(Svp svp) {
        Set<PortRecord> outports = new HashSet<>();
        Set<PortRecord> inports = new HashSet<>();
        
        for(EntityInstance entity : svp.getChildren()) {
            for(PortRecord port : entity.getPorts()) {
                if(port.getValue().getPortDirection() == PortDirection.IN) {
                    inports.add(port);
                } else {
                    outports.add(port);
                }
            }
        }
  
        for(PortRecord from : outports) {
            for(PortRecord to : inports) {
                
                if(from.getOwner().getValue() instanceof SvpInteraction) {
                    
                    SvpInteraction si = (SvpInteraction) from.getOwner().getValue();
                    
                    if( from.getValue().isConnectable(to.getValue()) &&
                    from.getOwner() != to.getOwner() &&
                    isProduction(si)) {
                        svp.wireChildren(from, to);
                    }   
                }

                
            }
        }

    }

    private void readPartProperties(SynBadPObject entity, Part part) {
   
        entity.setName(part.getDisplayName() != null ? part.getDisplayName() : part.getName());

        entity.addAnnotation(SynBadTerms.VPR.partType,
            new Annotation(SynBadTerms.VPR.partType, part.getType()));
      
        entity.setDescription(part.getDescription() != null ? part.getDescription() : "");
   
        entity.addAnnotation(SynBadTerms.VPR.sequence, 
            new Annotation(SynBadTerms.VPR.sequence, part.getSequence() != null ? part.getSequence().replace("![CDATA[", "").replace("]]", "").replace("\n", "") : ""));

        entity.addAnnotation(SynBadTerms.VPR.sequenceUri, 
            new Annotation(SynBadTerms.VPR.sequenceUri, part.getSequenceURI() != null ? part.getSequenceURI() : ""));

        entity.addAnnotation(SynBadTerms.VPR.organism, 
            new Annotation(SynBadTerms.VPR.organism, part.getOrganism() != null ?  part.getOrganism() : ""));
  
        entity.addAnnotation(SynBadTerms.VPR.designMethod, 
            new Annotation(SynBadTerms.VPR.designMethod, part.getDesignMethod() != null ?  part.getDesignMethod() : ""));
  
        entity.addAnnotation(SynBadTerms.VPR.metaType, 
            new Annotation(SynBadTerms.VPR.metaType, part.getMetaType() != null ?  part.getMetaType() : ""));

        entity.addAnnotation(SynBadTerms.VPR.status, 
            new Annotation(SynBadTerms.VPR.status, part.getStatus() != null ?  part.getStatus() : ""));

        if(part.getProperties()==null)
            return;

        for(Property property : part.getProperties()) {
            List<Annotation> properties = new ArrayList<>();
            properties.add(new Annotation(SynBadTerms.VPR.propertyName, property.getName() != null ? property.getName() : ""));
            properties.add(new Annotation(SynBadTerms.VPR.propertyValue, property.getValue() != null ? property.getValue() : ""));
            properties.add(new Annotation(SynBadTerms.VPR.propertydescription, property.getDescription() != null ? property.getDescription() : ""));
            
            Annotation a = new Annotation( SynBadTerms.VPR.property,
                    SynBadTerms.VPR.property,
                    URI.create(entity.getIdentity() + "/Property/" + property.getName().replaceAll(" ", "")),
                    properties);
            
            entity.addAnnotation(SynBadTerms.VPR.property, a);
            
        }
  
    }
    
    public PartBundle write(Svp part)
    {
        ComponentRole c = null;

        Part parsed = new Part();

        // Create basic properties about part, and convert properties in Part object

        createPartProperties(parsed, part);

        // Create internal interactions
        
        List<Interaction> interactions = new ArrayList<>();
  
        for(EntityInstance e : part.getChildren()) {
            if(e.getValue() instanceof SvpInteraction) {
                interactions.add(interactionParser.write((SvpInteraction)e.getValue()));
            }
        }
        
        if(interactions.isEmpty())
            return new PartBundle(parsed, interactions, null);
        else
            return new PartBundle( parsed, interactions,
                SVPManager.getSVPManager().createPartDocument(parsed, interactions));
    }

    private void createPartProperties(Part part, Svp svp) {

        part.setName(svp.getDisplayID() != null ? svp.getDisplayID() : "xxxxxxxxxxxxxxxxx");
        part.setDisplayName(svp.getName() != null ? svp.getName() : "");
        part.setDescription(svp.getDescription() != null ? svp.getDescription() : "");
        part.setSequence("![CDATA["+svp.getSequence().getSequence()+"]]");
        part.setOrganism(svp.getOrganism());
        if(!svp.getAnnotations(SynBadTerms.VPR.designMethod).isEmpty())
            part.setDesignMethod(svp.getAnnotations(SynBadTerms.VPR.designMethod).iterator().next().getStringValue());
        if(!svp.getAnnotations(SynBadTerms.VPR.metaType).isEmpty())
            part.setMetaType(svp.getAnnotations(SynBadTerms.VPR.metaType).iterator().next().getStringValue());
        if(!svp.getAnnotations(SynBadTerms.VPR.status).isEmpty()) {
            String status = svp.getAnnotations(SynBadTerms.VPR.status).iterator().next().getStringValue();
            if(!status.equals(""))
                 part.setStatus(status);
        }
        if(!svp.getAnnotations(SynBadTerms.VPR.partType).isEmpty())
            part.setType(svp.getAnnotations(SynBadTerms.VPR.partType).iterator().next().getStringValue());
         
        if(svp.getAnnotations(SynBadTerms.VPR.property).isEmpty())
            return;

        for(Annotation annotation : svp.getAnnotations(SynBadTerms.VPR.property)) {
            Property property = new Property();
            for(Annotation a: annotation.getAnnotations()) {
                if(a.getQName().equals(SynBadTerms.VPR.propertyName)) {
                    property.setName(a.getStringValue());
                } else if(a.getQName().equals(SynBadTerms.VPR.propertyValue)) {  
                    property.setValue(a.getStringValue()); 
                } else if(a.getQName().equals(SynBadTerms.VPR.propertydescription)) {
                    property.setDescription(a.getStringValue()); 
                }
            }
            
            part.AddProperty(property);
        }
  
    }
    
    private boolean isProduction(SvpInteraction interaction) {
        InteractionRole type = interaction.getRole();
        
        if(type.toString().contains("Production") || type.toString().contains("Phosphorylation") ||
                type.toString().contains("Formation"))
            return true;
        return false;
     }
}
