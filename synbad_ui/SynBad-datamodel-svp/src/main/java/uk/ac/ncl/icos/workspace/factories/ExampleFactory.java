/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.workspace.factories;

import org.sbolstandard.core2.Annotation;
import uk.ac.ncl.icos.synbad.datadefinition.UriHelper;
import uk.ac.ncl.icos.synbad.datadefinition.types.EntityType;
import uk.ac.ncl.icos.synbad.datadefinition.types.InteractionRole;
import uk.ac.ncl.icos.synbad.signalgraph.impl.SynBadPortState;
import uk.ac.ncl.icos.synbad.signalgraph.impl.SynBadPortType;
import uk.ac.ncl.icos.datamodel.workspace.objects.SynBadPObject;
import uk.ac.ncl.icos.datamodel.workspace.objects.IPort;
import uk.ac.ncl.icos.datamodel.workspace.impl.PortDirection;
import uk.ac.ncl.icos.datamodel.workspace.impl.EntityInstance;
import uk.ac.ncl.icos.datamodel.workspace.impl.Identity;
import uk.ac.ncl.icos.datamodel.workspace.impl.Workspace;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.model.VModel;
import uk.ac.ncl.icos.view.impl.VModelImpl;
import uk.ac.ncl.intbio.core.datatree.NamespaceBinding;

/**
 *
 * 
 * 
 * @author owengilfellon
 */
public class ExampleFactory {
    
    
    public static VModel setupTemplateModel() {
        
        // Set up factories
 
        NamespaceBinding namespace = UriHelper.synbad;
        Workspace workspace = new Workspace(UriHelper.synbad.getNamespaceURI(), "ExampleWorkspace", "1.0");
        workspace.setName("Example workspace");
        workspace.setDescription("Contains the entities representing a subtilin receiver.");
        
        /*workspace.getDispatcher().registerHandler(WorkspaceEvent.class, (WorkspaceEvent e) -> {
            System.out.println(
                    e.getClass().getSimpleName() + " : " +
                            e.getEventType() + " : " +
                            e.getSource().getName() + " : " +
                            e.getObject());
        });*/
        // 

                // Create system module, to compose subtilin receiver and reporter
        
        SynBadPObject system = workspace.createEntity(new Identity(namespace.getNamespaceURI(), "System", "1.0"));
        system.setName("ExampleSystem");
        system.setDescription("Demonstrating the composition of a subtilin receiver module, and a reporter module");
        system.addAnnotation( SynBadTerms.Annotatable.entitytype,
                new Annotation(SynBadTerms.Annotatable.entitytype, EntityType.Module.getUri()));

        // Create subtilin receiver module
        
        SynBadPObject senderreceiver = workspace.createEntity(new Identity( namespace.getNamespaceURI(), "SubtilinReceiver", "1.0"));
        senderreceiver.setName("Subtilin Receiver");
        senderreceiver.setDescription("Demonstrating the composition of a subtilin receiver from SVPs");
        senderreceiver.addAnnotation(SynBadTerms.Annotatable.entitytype, 
                new Annotation(SynBadTerms.Annotatable.entitytype, EntityType.Module.getUri()));
        
        EntityInstance senderreceiverinstance = system.addChild(senderreceiver);
        
        // Create SVPs and add to subtilin receiver

        EntityInstance pspark = senderreceiver.addChild(SVPFactory.getPromoter(workspace, namespace.getNamespaceURI(), "PspaRK", "1.0", 0.5, 1));
        EntityInstance rbs_spak = senderreceiver.addChild(SVPFactory.getRBS(workspace, namespace.getNamespaceURI(), "RBS_SpaK", "1.0", 0.5));
        EntityInstance spak = senderreceiver.addChild(SVPFactory.getCDS(workspace, namespace.getNamespaceURI(), "SpaK", "1.0", 0.5, 1, 0.5));
        EntityInstance rbs_spar = senderreceiver.addChild(SVPFactory.getRBS(workspace, namespace.getNamespaceURI(), "RBS_SpaR", "1.0", 0.5));
        EntityInstance spar = senderreceiver.addChild(SVPFactory.getCDS(workspace, namespace.getNamespaceURI(), "SpaR", "1.0", 0.5, 1, 0.5));
        EntityInstance ter = senderreceiver.addChild(SVPFactory.getTerminator(workspace, namespace.getNamespaceURI(), "Terminator", "1.0"));
        EntityInstance pspas = senderreceiver.addChild(SVPFactory.getInduciblePromoter(workspace, namespace.getNamespaceURI(), "PspaS", "1.0", 0.5, 1, SynBadPortType.Protein, SynBadPortState.Phosphorylated));
        EntityInstance spaK_phos_spaR = senderreceiver.addChild(SVIFactory.getPhosphorylation(workspace, namespace.getNamespaceURI(), "Phosphorylation", "1.0", 0.01));

            // Encapsulate subtilin and phosphorylation of SpaK in SpaK SVP

            SynBadPObject subtilin = ComponentFactory.getSmallMolecule(workspace, namespace.getNamespaceURI(), "Subtilin");
            SynBadPObject spak_subtilin = SVIFactory.getSmallMoleculePhosphorylation(workspace, namespace.getNamespaceURI(), "SpaK_Subtilin", "1.0", 0.01);

            EntityInstance subtilinInstance =  spak.getValue().addChild(subtilin);
            EntityInstance spakSubtilinInstance = spak.getValue().addChild(spak_subtilin);
            EntityInstance proteinProduction = spak.getValue().getChild((EntityInstance e)
                -> e.getValue().hasAnnotation(SynBadTerms.Annotatable.role, InteractionRole.ProteinProduction.getUri()));

            spak.getValue().wireChildren(subtilinInstance, spakSubtilinInstance, SynBadPortType.SmallMolecule, null);
            spak.getValue().wireChildren(proteinProduction, spakSubtilinInstance, SynBadPortType.Protein, SynBadPortState.Default);

            IPort spakOut = spak_subtilin.getPort((IPort port)
                -> port.getPortDirection() == PortDirection.OUT
                && port.hasAnnotation(SynBadTerms.Port.portType, SynBadPortType.Protein.getUri())
                && port.hasAnnotation(SynBadTerms.Port.portState, SynBadPortState.Phosphorylated.getUri()));

            spak.getValue().createPort(spakOut.getIdentity(), true);

        senderreceiver.wireChildren(pspark, rbs_spak, SynBadPortType.PoPS, null);
        senderreceiver.wireChildren(rbs_spak, spak, SynBadPortType.RiPS, null);
        senderreceiver.wireChildren(pspark, rbs_spar, SynBadPortType.PoPS, null);
        senderreceiver.wireChildren(rbs_spar, spar, SynBadPortType.RiPS, null);
        senderreceiver.wireChildren(spak, spaK_phos_spaR, SynBadPortType.Protein, SynBadPortState.Phosphorylated);
        senderreceiver.wireChildren(spar, spaK_phos_spaR, SynBadPortType.Protein, SynBadPortState.Default);
        senderreceiver.wireChildren(spaK_phos_spaR, pspas, SynBadPortType.Protein, SynBadPortState.Phosphorylated);

        // Proxy PoPS from PspaS

        IPort popsout = pspas.getValue().getPort((IPort port)
                -> port.getPortDirection() == PortDirection.OUT
                && port.hasAnnotation(SynBadTerms.Port.portType, SynBadPortType.PoPS.getUri()));

        senderreceiver.createPort(popsout.getIdentity(), true);

        // Create reporter module
        
        SynBadPObject reporter = workspace.createEntity(new Identity(namespace.getNamespaceURI(), "Reporter", "1.0"));
        reporter.setName("Reporter");
        reporter.setDescription("Demonstrating the composition of a reporter of PoPS from SVPs");
        reporter.addAnnotation( SynBadTerms.Annotatable.entitytype,
                new Annotation(SynBadTerms.Annotatable.entitytype, EntityType.Module.getUri()));
        
        EntityInstance reporterInstance = system.addChild(reporter);
        
        EntityInstance rbs_spas = reporter.addChild(SVPFactory.getRBS(workspace, namespace.getNamespaceURI(), "RBS_SpaS", "1.0", 0.5));
        EntityInstance gfp = reporter.addChild(SVPFactory.getCDS(workspace, namespace.getNamespaceURI(), "GFP", "1.0", 0.5, 1, 0.5));
        EntityInstance ter2 = reporter.addChild(SVPFactory.getTerminator(workspace, namespace.getNamespaceURI(), "Terminator2", "1.0"));
        
        reporter.wireChildren(rbs_spas, gfp, SynBadPortType.RiPS, null);
      
        IPort popsIn = rbs_spas.getValue().getPort((IPort port)
            -> port.hasAnnotation(SynBadTerms.Port.portType, SynBadPortType.PoPS.getUri())
            && port.getPortDirection() == PortDirection.IN);

        reporter.createPort(popsIn.getIdentity(), true);
        
        system.wireChildren(senderreceiverinstance, reporterInstance, SynBadPortType.PoPS, null);
       

        return new VModelImpl(system, workspace);
    }    
}
