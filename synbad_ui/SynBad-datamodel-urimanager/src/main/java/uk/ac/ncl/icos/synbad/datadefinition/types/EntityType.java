/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.datadefinition.types;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import java.net.URI;
import uk.ac.ncl.icos.synbad.datadefinition.UriHelper;

/**
 *
 * @author owengilfellon
 */

public enum EntityType implements Type {

    Component(UriHelper.synbad.namespacedUri("/definitionType/component").toASCIIString()),
    Sequence(UriHelper.synbad.namespacedUri("/definitionType/sequence").toASCIIString()),
    Module(UriHelper.synbad.namespacedUri("/definitionType/module").toASCIIString()),
    Interaction(UriHelper.synbad.namespacedUri("/definitionType/interaction").toASCIIString()),
    Property(UriHelper.synbad.namespacedUri("/definitionTypes/property").toASCIIString()),
    Port(UriHelper.synbad.namespacedUri("/definitionType/port").toASCIIString());
   
    private final URI uri;
    
    @Override
    public URI getUri() {
        return uri;
    }

    private EntityType(String uri) {
        this.uri = URI.create(uri);
    }   


}
