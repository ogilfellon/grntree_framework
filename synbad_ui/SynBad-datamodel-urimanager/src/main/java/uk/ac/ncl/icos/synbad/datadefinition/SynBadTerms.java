/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.datadefinition;

import javax.xml.namespace.QName;



public class SynBadTerms {

    public static class Annotatable {
        public static final QName name = UriHelper.synbad.withLocalPart("name");
        public static final QName description = UriHelper.synbad.withLocalPart("description");
        public static final QName derivedFrom = UriHelper.synbad.withLocalPart("derivedFrom");
        public static final QName version = UriHelper.synbad.withLocalPart("version");
        public static final QName entitytype = UriHelper.synbad.withLocalPart("entitytype");
        public static final QName role = UriHelper.synbad.withLocalPart("role");
        public static final QName type = UriHelper.synbad.withLocalPart("type");
        public static final QName persistantIdentity = UriHelper.synbad.withLocalPart("persistentIdentity");
        public static final QName displayIdentity = UriHelper.synbad.withLocalPart("displayIdentity");
    }
    
    public static class Wire {
        public static final QName from = UriHelper.synbad.withLocalPart("from");
        public static final QName to = UriHelper.synbad.withLocalPart("to");
    }
    
    public static class Types {
        public static final QName DOType = UriHelper.synbad.withLocalPart("dotype");
        public static final QName entity = UriHelper.synbad.withLocalPart("entity");
        public static final QName instance = UriHelper.synbad.withLocalPart("instance");
        public static final QName wire = UriHelper.synbad.withLocalPart("wire");
        public static final QName port = UriHelper.synbad.withLocalPart("port");
    }
    
    public static class Instance {
        public static final QName portInstance = UriHelper.synbad.withLocalPart("portInstance");
    }
    
    public static class Port {
        
        public static final QName defaultPort = UriHelper.synbad.withLocalPart("Port");
        public static final QName proxyPort = UriHelper.synbad.withLocalPart("ProxyPort");
       
        public static final QName version = UriHelper.synbad.withLocalPart("version");
        public static final QName portDirection = UriHelper.synbad.withLocalPart("portDirection");
        public static final QName proxies = UriHelper.synbad.withLocalPart("proxies");
        public static final QName portAccess = UriHelper.synbad.withLocalPart("portAccess");
        public static final QName portType = UriHelper.synbad.withLocalPart("portType");
        public static final QName portState = UriHelper.synbad.withLocalPart("portState");
    }
    
    public static class ModelEdge {
        public static final QName edgeType = UriHelper.synbad.withLocalPart("edgeType");
    }
    
    public static class VPR {
        public static final QName organism = UriHelper.vpr.withLocalPart("organism");
        public static final QName sequence = UriHelper.vpr.withLocalPart("sequence");
        public static final QName sequenceUri = UriHelper.vpr.withLocalPart("sequenceUri");
        public static final QName designMethod = UriHelper.vpr.withLocalPart("designMethod");
        public static final QName metaType = UriHelper.vpr.withLocalPart("metaType");
        public static final QName status = UriHelper.vpr.withLocalPart("status");
        public static final QName partType = UriHelper.vpr.withLocalPart("partType");
        
// Part properties
        
        public static final QName property = UriHelper.vpr.withLocalPart("property");
        public static final QName propertyName = UriHelper.vpr.withLocalPart("name");
        public static final QName propertyValue = UriHelper.vpr.withLocalPart("value");
        public static final QName propertydescription = UriHelper.vpr.withLocalPart("description");
        
        // Interaction parameter
        
        public static final QName parameter = UriHelper.vpr.withLocalPart("parameter");
        public static final QName parameterName = UriHelper.vpr.withLocalPart("parameterName");
        public static final QName parameterType = UriHelper.vpr.withLocalPart("parameterType");
        public static final QName parameterValue = UriHelper.vpr.withLocalPart("parameterValue");
        public static final QName parameterScope = UriHelper.vpr.withLocalPart("scope");
        public static final QName parameterEvidence = UriHelper.vpr.withLocalPart("evidenceType");
        
        public static final QName partDetail = UriHelper.vpr.withLocalPart("interactionPartDetail");
        public static final QName partDetailName = UriHelper.vpr.withLocalPart("partName");
        public static final QName partDetailForm = UriHelper.vpr.withLocalPart("partForm");
        public static final QName partDetailStoichiometry = UriHelper.vpr.withLocalPart("stoichiometry");
        public static final QName partDetailMathName = UriHelper.vpr.withLocalPart("mathName");
        public static final QName partDetailRole = UriHelper.vpr.withLocalPart("interactionRole");

        public static final QName freeTextMath = UriHelper.vpr.withLocalPart("freeTextMath");
        public static final QName mathName = UriHelper.vpr.withLocalPart("mathName");
        
        public static final QName isInternal = UriHelper.vpr.withLocalPart("isInternal");
        public static final QName isReaction = UriHelper.vpr.withLocalPart("isReaction");
        public static final QName isReversible = UriHelper.vpr.withLocalPart("isReversible");
        
        public static final QName participant = UriHelper.vpr.withLocalPart("participant");


        
    }

}
