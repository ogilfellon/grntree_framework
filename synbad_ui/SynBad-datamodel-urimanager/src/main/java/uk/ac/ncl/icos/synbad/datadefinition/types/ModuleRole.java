/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.datadefinition.types;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import java.net.URI;
import uk.ac.ncl.icos.synbad.datadefinition.UriHelper;

/**
 *
 * @author owengilfellon
 */
@XStreamAlias("ModuleRole")
public enum ModuleRole implements Role{
    
    SVPInteraction(UriHelper.vpr.namespacedUri("interaction-module").toASCIIString()),
    SVPPart(UriHelper.vpr.namespacedUri("svp-module").toASCIIString());
   
    private final URI uri;
    
    @Override
    public URI getUri() {
        return uri;
    }
    
    private ModuleRole(String uri) {
        this.uri = URI.create(uri);
    }  

}
