/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.datadefinition.types;

import java.net.URI;
import uk.ac.ncl.icos.synbad.datadefinition.IDataDefinition;

/**
 *
 * @author owengilfellon
 */
 public enum ConstraintRestriction implements IDataDefinition {
        
    PRECEDES("http://sbols.org/v2#precedes"),
    SAME_ORIENTATION_AS("http://sbols.org/v2#sameOrientationAs"),
    OPPOSITE_ORIENTATION_AS("http://sbols.org/v2#oppositeOrientationAs");

    private final URI uri;

    private ConstraintRestriction(String uri) {
        this.uri = URI.create(uri);
    }

    @Override
    public URI getUri() {
        return uri;
    }

}
