/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.datadefinition;

import java.net.URI;

/**
 * Provides an interface for enum constants describing type and roles.
 * @author owengilfellon
 */
public interface IDataDefinition {
    
    public URI getUri();
//    
}
