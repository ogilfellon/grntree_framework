/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.datadefinition;

import java.net.URI;
import static uk.ac.ncl.intbio.core.datatree.Datatree.NamespaceBinding;
import uk.ac.ncl.intbio.core.datatree.NamespaceBinding;


/**
 *
 * @author gokselmisirli
 */
public class UriHelper {
    
    public static final NamespaceBinding sbol = NamespaceBinding("http://sbols.org/v2#", "sbol");
    public static final NamespaceBinding vpr = NamespaceBinding("http://virtualparts.org/", "vpr");
    public static final NamespaceBinding dcterms = NamespaceBinding("http://purl.org/dc/terms/", "dcterms");
    public static final NamespaceBinding biopax = NamespaceBinding("http://www.biopax.org/release/biopax-level3.owl#", "biopax");
    public static final NamespaceBinding so = NamespaceBinding("http://identifiers.org/so/", "so");
    public static final NamespaceBinding synbad = NamespaceBinding("http://www.synbad.org/", "synbad");
    public static final NamespaceBinding rdf = NamespaceBinding("http://www.w3.org/1999/02/22-rdf-syntax-ns#", "rdf");
    public static final NamespaceBinding rdfs = NamespaceBinding("http://www.w3.org/2000/01/rdf-schema#", "rdfs");
    public static final NamespaceBinding prov = NamespaceBinding("http://www.w3.org/ns/prov#", "prov");
    
    public static URI getInteractionURI(String parentId, String interactionID)
    {
        return URI.create(parentId + "/" + interactionID);
    }
}
