/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.datadefinition.types;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import java.net.URI;
import uk.ac.ncl.icos.synbad.datadefinition.UriHelper;

/**
 *
 * @author owengilfellon
 */

public enum InteractionRole implements Role {
    
    ComplexFormation(UriHelper.synbad.namespacedUri("Complex+formation").toASCIIString()),
    ComplexDisassociation(UriHelper.synbad.namespacedUri("Complex+disassociation").toASCIIString()),
    
    TranscriptionalRepression(UriHelper.synbad.namespacedUri("Transcriptional+repression").toASCIIString()),
    TranscriptionActivation(UriHelper.synbad.namespacedUri("Transcriptional+activation").toASCIIString()),
   
    PoPSProduction(UriHelper.synbad.namespacedUri("PoPS+Production").toASCIIString()),
    PoPSModulation(UriHelper.synbad.namespacedUri("PoPSModulation").toASCIIString()),
    RiPSProduction(UriHelper.synbad.namespacedUri("RiPS+Production").toASCIIString()),
    ProteinProduction(UriHelper.synbad.namespacedUri("Protein+Production").toASCIIString()),
    mRNAProduction(UriHelper.synbad.namespacedUri("Translation").toASCIIString()),
    
    Phosphorylation(UriHelper.synbad.namespacedUri("Phosphorylation").toASCIIString()),
    Dephosphorylation(UriHelper.synbad.namespacedUri("Dephosphorylation").toASCIIString()),
    
    Degradation(UriHelper.synbad.namespacedUri("Degradation").toASCIIString());

    private final URI uri;
    
    @Override
    public URI getUri() {
        return uri;
    }

    private InteractionRole(String uri) {
        this.uri = URI.create(uri); 
    }
}
