/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.datadefinition.types;

import java.net.URI;
import uk.ac.ncl.icos.synbad.datadefinition.IDataDefinition;
import uk.ac.ncl.icos.synbad.datadefinition.UriHelper;

/**
 *
 * @author owengilfellon
 */

public enum ModelEdgeType implements IDataDefinition {

    HAS_CHILD(UriHelper.synbad.namespacedUri("childEdge").toASCIIString()),
    HAS_PORT(UriHelper.synbad.namespacedUri("portEdge").toASCIIString()),
    WIRE(UriHelper.synbad.namespacedUri("wireEdge").toASCIIString()),
    PROXY(UriHelper.synbad.namespacedUri("proxyEdge").toASCIIString());
    
    private final URI uri;
    
    @Override
    public URI getUri() {
        return uri;
    }
    
    private ModelEdgeType(String uri) {
        this.uri = URI.create(uri);
    }  
    
}
