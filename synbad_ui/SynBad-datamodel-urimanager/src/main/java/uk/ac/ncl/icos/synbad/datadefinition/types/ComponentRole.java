/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.datadefinition.types;

import java.net.URI;
import uk.ac.ncl.icos.synbad.datadefinition.UriHelper;

/**
 *
 * @author owengilfellon
 */
public enum ComponentRole implements Role {

    Generic(UriHelper.so.namespacedUri("SO:0000110").toASCIIString()),
    Promoter(UriHelper.so.namespacedUri("SO:0000167").toASCIIString()),
    RBS(UriHelper.so.namespacedUri("SO:0000139").toASCIIString()),
    CDS(UriHelper.so.namespacedUri("SO:0000316").toASCIIString()),
    Terminator(UriHelper.so.namespacedUri("SO:0000141").toASCIIString()),
    Operator(UriHelper.so.namespacedUri("SO:0000057").toASCIIString()),
    Gene(UriHelper.so.namespacedUri("SO:0000704").toASCIIString()),
    Shim(UriHelper.so.namespacedUri("SO:0000997").toASCIIString()),
    mRNA(UriHelper.so.namespacedUri("SO:0000234").toASCIIString()),
    RNA(UriHelper.so.namespacedUri("SO:0000356").toASCIIString()),
    SVPInteraction(UriHelper.vpr.namespacedUri("interaction-module").toASCIIString()),
    SVPPart(UriHelper.vpr.namespacedUri("svp-module").toASCIIString());
   
    private final URI uri;
    
    @Override
    public URI getUri() {
        return uri;
    }

    private ComponentRole(String uri) {
        this.uri = URI.create(uri);
    }  

}

