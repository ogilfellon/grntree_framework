package uk.ac.ncl.icos.synbad.datadefinition;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.openide.util.Lookup;
import org.openide.util.lookup.ServiceProvider;

/**
 *
 * @author owengilfellon
 */
public interface IDataDefinitionManager {

    public void addDefinition(IDataDefinition dataDefinition);
    public void addSynonym(String synonym, IDataDefinition dataDefinition);
    public boolean containsIdentity(String identifier);
    public <T> boolean containsDefinition(Class<T> clazz, String id);
    public <T> T getDefinition(Class<T> clazz, String identifier);

    @ServiceProvider(service=IDataDefinitionManager.class)
    public class DefaultDataDefinitionManager implements IDataDefinitionManager {

        private final Map<String, IDataDefinition> idToDefinition = new HashMap<>();
        private final Map<Class<?>, Set<IDataDefinition>> classToDefinitions = new HashMap<>();

        public DefaultDataDefinitionManager() {
            Collection<? extends IDataDefinitionProvider> c = Lookup.getDefault().lookupAll(IDataDefinitionProvider.class);
            for(IDataDefinitionProvider provider : c) {
                provider.addDefinitions(this);
            }
        }

        @Override
        public void addDefinition(IDataDefinition dataDefinition) {
            Class<?> clazz = dataDefinition.getClass();
            if(!classToDefinitions.containsKey(clazz)) {
                classToDefinitions.put(clazz, new HashSet<IDataDefinition>());
            }
            classToDefinitions.get(clazz).add(dataDefinition);
            
            idToDefinition.put(dataDefinition.getUri().toASCIIString(), dataDefinition);
        }
        
        @Override
        public void addSynonym(String synonym, IDataDefinition dataDefinition) {
            Class<?> clazz = dataDefinition.getClass();
            if(!classToDefinitions.containsKey(clazz)) {
                addDefinition(dataDefinition);
            }

            idToDefinition.put(synonym, dataDefinition);
        }
        
        @Override
        public boolean containsIdentity(String id) {
            return idToDefinition.containsKey(id);
        }

        @Override
        public <T> boolean containsDefinition(Class<T> clazz, String id) {
            return containsIdentity(id) &&
                    clazz.isAssignableFrom(idToDefinition.get(id).getClass());
        }
        
        @Override
        public <T> T getDefinition(Class<T> clazz, String identifier) {

            IDataDefinition dataDefinition = idToDefinition.get(identifier);
            
            if(clazz.isAssignableFrom(dataDefinition.getClass())) {
                return clazz.cast(dataDefinition);
            }
            
            return null;
        }
    }
}
