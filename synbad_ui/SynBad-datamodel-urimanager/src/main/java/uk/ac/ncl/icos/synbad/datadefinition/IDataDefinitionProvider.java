/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.datadefinition;


import org.openide.util.lookup.ServiceProvider;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentRole;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentType;
import uk.ac.ncl.icos.synbad.datadefinition.types.ConstraintRestriction;
import uk.ac.ncl.icos.synbad.datadefinition.types.EntityType;
import uk.ac.ncl.icos.synbad.datadefinition.types.InteractionRole;
import uk.ac.ncl.icos.synbad.datadefinition.types.LocationOrientation;
import uk.ac.ncl.icos.synbad.datadefinition.types.ModuleRole;

/**
 *
 * @author owengilfellon
 */
public interface IDataDefinitionProvider {
    
    public void addDefinitions(IDataDefinitionManager manager);
    
    @ServiceProvider( service = IDataDefinitionProvider.class)
    public class DefaultDefinitionProvider implements IDataDefinitionProvider {

        @Override
        public void addDefinitions(IDataDefinitionManager manager) {
            
            // Add defined terms
            
            for(ComponentRole role : ComponentRole.values()) {
                manager.addDefinition(role);
            }

            for(ComponentType type : ComponentType.values()) {
                manager.addDefinition(type);
            }
            
            for(ModuleRole role : ModuleRole.values()) {
                manager.addDefinition(role);
            }

            for(InteractionRole role : InteractionRole.values()) {
                manager.addDefinition(role);
            }
            
            for(EntityType type : EntityType.values()) {
                manager.addDefinition(type);
            }
          
            for(ConstraintRestriction r: ConstraintRestriction.values()) {
                manager.addDefinition(r);
            }
            
            for(LocationOrientation o: LocationOrientation.values()) {
                manager.addDefinition(o);
            }
            
            // Add synonyms
            
            // VPR Part Types
            
            manager.addSynonym("Promoter", ComponentRole.Promoter);
            manager.addSynonym("RBS", ComponentRole.RBS);
            manager.addSynonym("FunctionalPart", ComponentRole.CDS);
            manager.addSynonym("Operator", ComponentRole.Operator);
            manager.addSynonym("Terminator", ComponentRole.Terminator);
            manager.addSynonym("Shim", ComponentRole.Shim);
            
            manager.addSynonym("Environment Constant", ComponentType.EnvironmentConstant);
            
            // VPR SBOL2.0 Interaction Types
            
            manager.addSynonym("Complex formation", InteractionRole.ComplexFormation);
            manager.addSynonym("Transcriptional repression", InteractionRole.TranscriptionalRepression);
            manager.addSynonym("Transcriptional activation", InteractionRole.TranscriptionActivation);
            manager.addSynonym("PoPS Production", InteractionRole.PoPSProduction);
            manager.addSynonym("RiPS Production", InteractionRole.RiPSProduction);
            manager.addSynonym("Phosphorylation", InteractionRole.Phosphorylation);
            manager.addSynonym("Dephosphorylation", InteractionRole.Dephosphorylation);
            manager.addSynonym("Degradation", InteractionRole.Degradation);
            manager.addSynonym("OperatorPoPSModulation", InteractionRole.PoPSModulation);
            manager.addSynonym("Protein Production", InteractionRole.ProteinProduction);
        
        }
        
    }
    
}
