/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.datadefinition;

public class SynBadTerms2 {
    
    public static class Rdf {
        public static final String hasType = UriHelper.rdf.namespacedUri("type").toASCIIString();
    }
    
    public static class RdfType {
        public static final String stringLiteral = "http://www.w3.org/2001/XMLSchema#string";
        public static final String integerLiteral = "http://www.w3.org/2001/XMLSchema#integer";
        public static final String doubleLiteral = "http://www.w3.org/2001/XMLSchema#double";
        public static final String booleanLiteral = "http://www.w3.org/2001/XMLSchema#boolean";
    }

    public static class SbolIdentified {
        public static final String hasName = UriHelper.dcterms.namespacedUri("title").toASCIIString();
        public static final String hasDescription = UriHelper.dcterms.namespacedUri("description").toASCIIString();
        public static final String wasDerivedFrom = UriHelper.prov.namespacedUri("wasDerivedFrom").toASCIIString();
        public static final String hasVersion = UriHelper.sbol.namespacedUri("version").toASCIIString();
        public static final String hasPersistentIdentity = UriHelper.sbol.namespacedUri("persistentIdentity").toASCIIString();
        public static final String hasDisplayId = UriHelper.sbol.namespacedUri("displayId").toASCIIString();
        //public static final String entitytype = UriHelper.synbad.withLocalPart("entitytype");
    }
    
    public static class Sbol {
        public static final String role = UriHelper.sbol.namespacedUri("role").toASCIIString();
        public static final String type = UriHelper.sbol.namespacedUri("type").toASCIIString();  
         public static final String definedBy = UriHelper.sbol.namespacedUri("definition").toASCIIString();
    }
    
    public static class SbolComponent {
        public static final String hasSequence = UriHelper.sbol.namespacedUri("sequence").toASCIIString();
        public static final String hasType = UriHelper.sbol.namespacedUri("type").toASCIIString();
        public static final String hasRole = UriHelper.sbol.namespacedUri("role").toASCIIString();
        public static final String hasComponent = UriHelper.sbol.namespacedUri("component").toASCIIString();
        public static final String hasSequenceAnnotation = UriHelper.sbol.namespacedUri("sequenceAnnotation").toASCIIString();
        public static final String hasSequenceConstraint = UriHelper.sbol.namespacedUri("sequenceConstraint").toASCIIString();
    }
    
    public static class SbolFunctionalComponent {
        public static final String hasAccess = UriHelper.sbol.namespacedUri("access").toASCIIString();
        public static final String hasDirection = UriHelper.sbol.namespacedUri("direction").toASCIIString();
    }
    
    public static class SbolModule {
         public static final String hasModel = UriHelper.sbol.namespacedUri("module").toASCIIString();
        public static final String hasModule = UriHelper.sbol.namespacedUri("module").toASCIIString();
        public static final String hasFunctionalComponent = UriHelper.sbol.namespacedUri("functionalComponent").toASCIIString();
        public static final String hasInteraction = UriHelper.sbol.namespacedUri("interaction").toASCIIString();
    }
    
    public static class SbolInteraction {
        public static final String hasParticipation = UriHelper.sbol.namespacedUri("participation").toASCIIString();
        public static final String participant = UriHelper.sbol.namespacedUri("participant").toASCIIString();
    }
    
    public static class SbolSequence {
        public static final String hasElements = UriHelper.sbol.namespacedUri("elements").toASCIIString();
        public static final String hasEncoding = UriHelper.sbol.namespacedUri("sequence").toASCIIString();
    }

    public static class SbolSequenceConstraint {
        public static final String hasRestriction = UriHelper.sbol.namespacedUri("restriction").toASCIIString();
        public static final String hasSubject = UriHelper.sbol.namespacedUri("subject").toASCIIString();
        public static final String hasObject = UriHelper.sbol.namespacedUri("object").toASCIIString();
    }
    
    public static class SbolSequenceAnnotation {
        public static final String hasLocation = UriHelper.sbol.namespacedUri("location").toASCIIString();
        public static final String positionsComponent = UriHelper.sbol.namespacedUri("component").toASCIIString();
    }
    
    public static class SbolLocation {
        public static final String hasOrientation = UriHelper.sbol.namespacedUri("orientation").toASCIIString();
        public static final String rangeStart = UriHelper.sbol.namespacedUri("start").toASCIIString();
        public static final String rangeEnd = UriHelper.sbol.namespacedUri("end").toASCIIString();
        public static final String cutAt = UriHelper.sbol.namespacedUri("at").toASCIIString();   
    }
    
     public static class SbolCollecton {
        public static final String hasMember = UriHelper.sbol.namespacedUri("member").toASCIIString();
    }
    
    public static class MapsTo {
        public static final String hasMapsTo = UriHelper.sbol.namespacedUri("mapsTo").toASCIIString();
        public static final String hasRefinement = UriHelper.sbol.namespacedUri("refinement").toASCIIString();
        public static final String hasLocalInstance = UriHelper.sbol.namespacedUri("local").toASCIIString();
        public static final String hasRemoteInstance = UriHelper.sbol.namespacedUri("remote").toASCIIString();
    }
 
     public static class SbolModel {
        public static final String source = UriHelper.sbol.namespacedUri("source").toASCIIString();
        public static final String language = UriHelper.sbol.namespacedUri("language").toASCIIString();
        public static final String framework = UriHelper.sbol.namespacedUri("framework").toASCIIString();
    }
     
    public static class SynBadEntity {
        public static final String hasPort = UriHelper.synbad.namespacedUri("port").toASCIIString();
        public static final String hasWire = UriHelper.synbad.namespacedUri("wire").toASCIIString();
        public static final String hasChild = UriHelper.synbad.namespacedUri("child").toASCIIString();
    }
    
    public static class SynBadPort {
        public static final String isPublic = UriHelper.synbad.namespacedUri("portAccess").toASCIIString();
        public static final String proxies = UriHelper.synbad.namespacedUri("proxies").toASCIIString();
        public static final String hasDirection = UriHelper.synbad.namespacedUri("portDirection").toASCIIString();
        public static final String hasPortType = UriHelper.synbad.namespacedUri("portType").toASCIIString();
        public static final String hasPortState = UriHelper.synbad.namespacedUri("portState").toASCIIString();
    }
    
    public static class SynBadWire {
        public static final String isPublic = UriHelper.synbad.namespacedUri("portAccess").toASCIIString();
        public static final String proxies = UriHelper.synbad.namespacedUri("proxies").toASCIIString();
    }

/*
    
    public static class Wire {
        public static final String from = UriHelper.synbad.withLocalPart("from");
        public static final String to = UriHelper.synbad.withLocalPart("to");
    }
    
    public static class Types {
        public static final String DOType = UriHelper.synbad.withLocalPart("dotype");
        public static final String entity = UriHelper.synbad.withLocalPart("entity");
        public static final String instance = UriHelper.synbad.withLocalPart("instance");
        public static final String wire = UriHelper.synbad.withLocalPart("wire");
        public static final String port = UriHelper.synbad.withLocalPart("port");
    }
    
    public static class Instance {
        public static final String portInstance = UriHelper.synbad.withLocalPart("portInstance");
    }
    
    public static class Port {
        
        public static final String defaultPort = UriHelper.synbad.withLocalPart("Port");
        public static final String proxyPort = UriHelper.synbad.withLocalPart("ProxyPort");
       
        public static final String version = UriHelper.synbad.withLocalPart("version");
        public static final String portDirection = UriHelper.synbad.withLocalPart("portDirection");
        public static final String proxies = UriHelper.synbad.withLocalPart("proxies");
        public static final String portAccess = UriHelper.synbad.withLocalPart("portAccess");
        public static final String portType = UriHelper.synbad.withLocalPart("portType");
        public static final String portState = UriHelper.synbad.withLocalPart("portState");
    }
    
    public static class ModelEdge {
        public static final String edgeType = UriHelper.synbad.withLocalPart("edgeType");
    }
    
    public static class VPR {
        public static final String organism = UriHelper.vpr.withLocalPart("organism");
        public static final String sequence = UriHelper.vpr.withLocalPart("sequence");
        public static final String sequenceUri = UriHelper.vpr.withLocalPart("sequenceUri");
        public static final String designMethod = UriHelper.vpr.withLocalPart("designMethod");
        public static final String metaType = UriHelper.vpr.withLocalPart("metaType");
        public static final String status = UriHelper.vpr.withLocalPart("status");
        public static final String partType = UriHelper.vpr.withLocalPart("partType");
        
// Part properties
        
        public static final String property = UriHelper.vpr.withLocalPart("property");
        public static final String propertyName = UriHelper.vpr.withLocalPart("name");
        public static final String propertyValue = UriHelper.vpr.withLocalPart("value");
        public static final String propertydescription = UriHelper.vpr.withLocalPart("description");
        
        // Interaction parameter
        
        public static final String parameter = UriHelper.vpr.withLocalPart("parameter");
        public static final String parameterName = UriHelper.vpr.withLocalPart("parameterName");
        public static final String parameterType = UriHelper.vpr.withLocalPart("parameterType");
        public static final String parameterValue = UriHelper.vpr.withLocalPart("parameterValue");
        public static final String parameterScope = UriHelper.vpr.withLocalPart("scope");
        public static final String parameterEvidence = UriHelper.vpr.withLocalPart("evidenceType");
        
        public static final String partDetail = UriHelper.vpr.withLocalPart("interactionPartDetail");
        public static final String partDetailName = UriHelper.vpr.withLocalPart("partName");
        public static final String partDetailForm = UriHelper.vpr.withLocalPart("partForm");
        public static final String partDetailStoichiometry = UriHelper.vpr.withLocalPart("stoichiometry");
        public static final String partDetailMathName = UriHelper.vpr.withLocalPart("mathName");
        public static final String partDetailRole = UriHelper.vpr.withLocalPart("interactionRole");

        public static final String freeTextMath = UriHelper.vpr.withLocalPart("freeTextMath");
        public static final String mathName = UriHelper.vpr.withLocalPart("mathName");
        
        public static final String isInternal = UriHelper.vpr.withLocalPart("isInternal");
        public static final String isReaction = UriHelper.vpr.withLocalPart("isReaction");
        public static final String isReversible = UriHelper.vpr.withLocalPart("isReversible");
        
        public static final String participant = UriHelper.vpr.withLocalPart("participant");


        
    }*/

}
