package uk.ac.ncl.icos.synbad.datadefinition.types;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import java.net.URI;
import uk.ac.ncl.icos.synbad.datadefinition.UriHelper;

/**
 *
 * @author owengilfellon
 */
@XStreamAlias("ComponentType")
public enum ComponentType implements Type {

    DNA(UriHelper.biopax.namespacedUri("DnaRegion")),
    mRNA(UriHelper.biopax.namespacedUri("RnaRegion")),
    Protein(UriHelper.biopax.namespacedUri("Protein")),
    Complex(UriHelper.biopax.namespacedUri("Protein")),
    SmallMolecule(UriHelper.biopax.namespacedUri("SmallMolecule")),
    EnvironmentConstant(UriHelper.biopax.namespacedUri("EnvironmentConstant"));

    private final URI uri;

    @Override
    public URI getUri() {
        return uri;
    }

    private ComponentType(URI uri) {
        this.uri = uri;
    }   

}
