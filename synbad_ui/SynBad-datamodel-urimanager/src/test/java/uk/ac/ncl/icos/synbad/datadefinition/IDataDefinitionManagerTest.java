/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.datadefinition;

import java.util.HashSet;
import java.util.Set;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.netbeans.junit.MockServices;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentRole;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentType;
import uk.ac.ncl.icos.synbad.datadefinition.types.InteractionRole;
import uk.ac.ncl.icos.synbad.datadefinition.types.Role;
import uk.ac.ncl.icos.synbad.datadefinition.types.Type;

/**
 *
 * @author owengilfellon
 */
public class IDataDefinitionManagerTest {
    
    public IDataDefinitionManagerTest() {
        MockServices.setServices(IDataDefinitionProvider.DefaultDefinitionProvider.class);
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of containsDefinition method, of class IDataDefinitionManager.
     */
    @org.junit.Test
    public void testContainsDefinition() {
        
        // Set up definition manager
        
        MockServices.setServices(IDataDefinitionProvider.DefaultDefinitionProvider.class);
        IDataDefinitionManager definitionManager = new IDataDefinitionManager.DefaultDataDefinitionManager();
        
        // Get some ids to work with
        
        Set<String> uris = new HashSet<>();
        
        for(Role role : ComponentRole.values()) {
            uris.add(role.getUri().toASCIIString());
        }
        
        for(Role role : InteractionRole.values()) {
            uris.add(role.getUri().toASCIIString());
        }

        for(String uri : uris) {
            
            // Test retrieval of constants using URIs

            assert(definitionManager.getDefinition(Role.class, uri) != null);
            assert(definitionManager.getDefinition(Type.class, uri) == null);

        }
        
        // Test retrieval of constants using synonyms

        assert(definitionManager.getDefinition(ComponentRole.class, "Promoter") != null);
        assert(definitionManager.getDefinition(ComponentRole.class, "RBS") != null);
        assert(definitionManager.getDefinition(ComponentRole.class, "FunctionalPart") != null);

            
        assert(definitionManager.getDefinition(InteractionRole.class, "Phosphorylation") != null);
        assert(definitionManager.getDefinition(InteractionRole.class, "PoPS Production") != null);
           

    }

    
}
