/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.h.graph.api;

import uk.ac.ncl.icos.synbad.event.SBEventType;


/**
 *
 * @author owengilfellon
 */
public interface GraphListener<N, E> {
    
    public void onEntityEvent(SBEventType type, N instance, N parent);
    
    public void onEdgeEvent(SBEventType type, E edge);
    
}
