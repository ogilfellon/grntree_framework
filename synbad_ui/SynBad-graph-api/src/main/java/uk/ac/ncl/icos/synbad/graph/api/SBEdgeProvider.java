/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph.api;

import java.util.List;

/**
 *
 * @author owengilfellon
 */
public interface SBEdgeProvider<N, E> {
    
    public N getEdgeSource(E edge);
            
    public N getEdgeTarget(E edge);
    
    List<E> incomingEdges(N node);
    
    List<E> outgoingEdges(N node);
    
    List<E> incomingEdges(N node, String predicate);
    
    List<E> outgoingEdges(N node, String predicate);
}
