/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.h.graph.api;

import java.util.List;
import uk.ac.ncl.icos.synbad.graph.api.SBEdge;
import uk.ac.ncl.icos.synbad.graph.api.SBGraph;
import uk.ac.ncl.icos.synbad.graph.api.SBObject;

/**
 * A graph structure in which nodes are hierarchically arranged in a tree. All
 * nodes must have parents, except for the root node.
 * @author owengilfellon
 */
public interface SBCompoundGraph<N extends SBObject, E extends SBEdge> extends SBGraph<N, E>{

    // ==========================
    //          Nodes
    // ==========================
    
    /**
     * Adds the supplied node as a child of the root node.
     * @param node
     * @return 
     */
    @Override
    public boolean addNode(N node);

    public boolean addNode(N instance, N parent);
    
    public boolean addNode(N instance, N parent, int index);
    
    // ==========================
    //          Hierarchy
    // ==========================
    
    /**
     * Returns the root of the graph's hierarchy.
     * @return 
     */
    public N getRoot();
  
    public N getParent(N node);
    
    public void setParent(N parent, N node);

    public List<N> getChildren(N node);

    public List<N> getDescendants(N node);
    
    public boolean isDescendant(N node, N descendant); 

    public boolean isAncestor(N node, N ancestor);
    
    public int getDepth(N node);

    public SBHIterator<N> hierarchyIterator(N instance);
    
    public boolean hasSiblings(N node);
    
    public List<N> getSiblings(N node);

}
