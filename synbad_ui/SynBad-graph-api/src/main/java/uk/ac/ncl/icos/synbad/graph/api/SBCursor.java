/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph.api;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;

/**
 *
 * @author owengilfellon
 */
public interface SBCursor<N extends SBObject, E extends SBEdge> {

    public Iterable<N> getPath();

    public N getCurrentPosition();

    public N pop();

    public void placeAt(N instance);

    public void moveTo(N instance);

    public void moveVia(E edge);

    public Iterable<E> getEdges(EdgeDirection direction);
    
    public N getEdgeSource(E edge);
    
    public N getEdgeTarget(E edge);
    
    public static class CursorPosition<N> {

        private final N instance;

        public CursorPosition(N instance) {
            this.instance = instance;
        }

        public N getNode() {
            return instance;
        }
    }
    
    public class ASBCursor<N extends SBObject, E extends SBEdge> implements SBCursor<N, E> {

        protected final Stack<CursorPosition<N>> positionStack = new Stack();
        protected final SBEdgeProvider<N, E> provider;

        public ASBCursor(N startNode, SBEdgeProvider<N, E> provider) {
            this.provider = provider;
            placeAt(startNode);
        }


        @Override
        public Iterable<N> getPath() {

            List<N> path = new ArrayList<>();

            for(Iterator<CursorPosition<N>> i = positionStack.iterator(); i.hasNext();) {
                path.add(i.next().instance);
            }

            return path;
        }


        @Override
        public N getCurrentPosition() {
            return positionStack.peek().getNode();
        }

        @Override
        public N pop() {
            return positionStack.pop().getNode();
        }


        @Override
        public void placeAt(N instance) { 
            positionStack.clear();
            positionStack.push(new CursorPosition(instance));
         }


        @Override
        public void moveTo(N instance) {
            // if // graph contains edge between this and that....

            positionStack.push(new CursorPosition(instance));
        }


        @Override
        public void moveVia(E edge) {

            // get edge's To from graph?

            if(getCurrentPosition() == provider.getEdgeSource(edge)) {
               positionStack.add(new CursorPosition(provider.getEdgeTarget(edge))); 
            } else if(getCurrentPosition() == provider.getEdgeTarget(edge)) {
               positionStack.add(new CursorPosition(provider.getEdgeSource(edge))); 
            } else {
               throw new IllegalStateException(edge.toString() + " is not incident to " + getCurrentPosition().toString());
            }
        }

        @Override
        public Iterable<E> getEdges(EdgeDirection direction) {
            switch(direction) {
                case IN:
                    return provider.incomingEdges(positionStack.peek().getNode());
                case OUT:
                    return provider.outgoingEdges(positionStack.peek().getNode());
            }
            
            return new HashSet<>();
            
        }

        @Override
        public N getEdgeSource(E edge) {
            return provider.getEdgeSource(edge);
        }

        @Override
        public N getEdgeTarget(E edge) {
            return provider.getEdgeTarget(edge);
        }
        
        

    }

}
