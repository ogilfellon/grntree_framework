package uk.ac.ncl.icos.synbad.graph.api;


/**
 *
 * @author owengilfellon
 */
public interface SBCrawlerListener<N extends SBObject, E extends SBEdge> {

    public void onEdge(E edge);

    public void onEnded();

    public void onInstance(N instance, SBCursor<N, E> cursor);

    public boolean terminate();

    public boolean vetoEdge(E edge);
    
    
    public abstract class SBCrawlerListenerAdapter<N extends SBObject, E extends SBEdge> implements SBCrawlerListener<N, E>{

        @Override
        public void onEdge(E edge) { }

        @Override
        public void onEnded() { }

        @Override
        public void onInstance(N instance, SBCursor<N, E> cursor) { }

        @Override
        public boolean terminate() { return false; }

        @Override
        public boolean vetoEdge(E edge) { return false; }
        
    }
}
