/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.h.graph.api;


import uk.ac.ncl.icos.synbad.graph.api.EdgeDirection;
import uk.ac.ncl.icos.synbad.graph.api.SBCursor;
import uk.ac.ncl.icos.synbad.graph.api.SBEdge;
import uk.ac.ncl.icos.synbad.graph.api.SBObject;

/**
 *
 * @author owengilfellon
 */
public interface SBHCursor<N extends SBObject, E extends SBEdge> extends SBCursor<N, E> {

    public Iterable<E> getProxyEdges(EdgeDirection direction);
    
    public N getOriginalEdgeSource(E edge);
    
    public N getOriginalEdgeTarget(E edge);
    
    public boolean isProxyEdge(E edge);

    public N getParent();

    public int getDepth();

    public void expand();
    
    public void collapse();
}
