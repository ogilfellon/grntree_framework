/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph.api;

import java.util.Collection;
import java.util.Set;


/**
 *
 * @author owengilfellon
 */
public interface SBObject {

    public Set<String> getPredicates();
    
    public SBValue getValue(String predicate);
    
    public Collection<SBValue> getValues(String predicate) ;
    
    public <T> Collection<T> getValues(String predicate, Class<T> clazz);
    

}
