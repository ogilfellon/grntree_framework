/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph.api;

import java.util.List;

/**
 *
 * @author owengilfellon
 */
public interface SBHEdgeProvider<N extends SBObject, E extends SBEdge> extends SBEdgeProvider<N, E> {
    
    public N getOriginalEdgeSource(E edge);
            
    public N getOriginalEdgeTarget(E edge);
    
    List<E> incomingProxyEdges(N node);
    
    List<E> outgoingProxyEdges(N node);
  
}
