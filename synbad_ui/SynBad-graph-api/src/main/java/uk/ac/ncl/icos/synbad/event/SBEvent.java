/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.event;

/**
 * Base interface for all events in SynBad. 
 * 
 * @author owengilfellon
 */
public interface SBEvent<S, O> {
    
    public SBEventType getType();
    public S getSource();
    public O getObject();

}
