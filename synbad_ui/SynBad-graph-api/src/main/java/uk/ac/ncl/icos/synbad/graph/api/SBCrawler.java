/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph.api;


import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author owengilfellon
 */
public interface SBCrawler<N extends SBObject, E extends SBEdge> extends SBCrawlerListener<N, E> {
    
    public void addListener(SBCrawlerListener<N,E> listener);
    
    public void removeListener(SBCrawlerListener<N,E> listener);
    
    public void run(SBCursor<N,E> cursor);

    void processNode(SBCursor<N, E> cursor, Set<E> seenEdges);
    
    void processEdge(SBCursor<N,E> cursor, Set<E> seenEdges, E edge);
    
    public abstract class SBCrawlerAdapter<N extends SBObject, E extends SBEdge> implements SBCrawler<N, E> {
        
        private final Set<SBCrawlerListener<N, E>> listeners = new HashSet<>();
    

        public void addListener(SBCrawlerListener<N, E> listener) {
            listeners.add(listener);
        }

        public void removeListener(SBCrawlerListener<N, E> listener) {
            listeners.remove(listener);
        }

        @Override
        public void onEdge(E edge) {
            for(SBCrawlerListener<N, E> listener : listeners) {
                listener.onEdge(edge);
            }
        }

        @Override
        public void onEnded() {
            for(SBCrawlerListener<N, E> listener : listeners) {
                listener.onEnded();
            }
        }

        @Override
        public void onInstance(N instance, SBCursor<N, E> cursor) {
            for(SBCrawlerListener<N, E> listener : listeners) {
                listener.onInstance(instance, cursor);
            }
        }

        @Override
        public boolean terminate() {

            boolean terminate = false;

            for(SBCrawlerListener<N, E> listener : listeners) {
                if(listener.terminate())
                    terminate = true;
            }

            return terminate;
        }

        @Override
        public boolean vetoEdge(E edge) {

            boolean veto = false;

            for(SBCrawlerListener<N, E> listener : listeners) {
                if(listener.vetoEdge(edge))
                    veto = true;
            }

            return veto;
        }
        
    }

}
