/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph.api;

/**
 *
 * @author owengilfellon
 */
public interface SBEdge<N> {
  
    // get Data Definition object
    
    public N getEdge();

    public class DefaultEdge<N>  implements SBEdge< N>{
   
        private final N edge;

        public DefaultEdge(N edge) {
            this.edge = edge;
        }

        @Override
        public N getEdge() {
            return edge;
        }

    }
    
}
