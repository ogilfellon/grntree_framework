/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.h.graph.api;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import uk.ac.ncl.icos.synbad.graph.api.SBEdge;
import uk.ac.ncl.icos.synbad.graph.api.SBHEdgeProvider;
import uk.ac.ncl.icos.synbad.graph.api.SBObject;

/**
 *
 * @author owengilfellon
 */
public interface SBHGraph<N extends SBObject, E extends SBEdge> extends SBHEdgeProvider<N, E> {

    
    public N getRoot();
    
    public boolean containsNode(N node);
    
    public boolean removeNode(N node);
    
    public boolean removeAllNodes(Collection<? extends N> nodes);
    
    public boolean addNode(N instance, N parent);
    
    public boolean addNode(N instance, N parent, int index);
            
    // Contraction / Expansion
    
    public boolean isVisible(N node);
    
    public boolean isContracted(N node);
    
    public void expand(N node);
    
    public void contract(N node);
    
    
    public boolean containsEdge(E edge);
    
    public boolean containsEdge(N from, N to);
    
    public boolean addEdge(N from, N to, E edge);
    
    public boolean isProxyEdge(E edge);
    
    public boolean removeEdge(E edge);

   // public boolean removeEdge(E edge);
            
    public Set<E> edgeSet();
    
    public Set<E> proxyEdgeSet();

    // Hierarchy methods
    
    public N getParent(N node);
    
    public void setParent(N parent, N node);
    
    //

    public List<N> getChildren(N node);

    public List<N> getDescendants(N node);
    
    public boolean isDescendant(N node, N descendant); 
    
    public boolean hasSiblings(N node);
    
    public List<N> getSiblings(N node);
    
    
    public boolean isAncestor(N node, N ancestor);
    
    public int getDepth(N node);
    
    // Iteration
    
    public SBHIterator<N> hierarchyIterator(N instance);
    
    public void addListener(HGraphListener<N, E> listener);
    
    public void removeListener(HGraphListener<N, E> listener);
    

    
}
