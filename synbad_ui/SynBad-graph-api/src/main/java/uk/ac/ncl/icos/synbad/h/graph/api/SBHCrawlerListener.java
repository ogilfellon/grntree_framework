package uk.ac.ncl.icos.synbad.h.graph.api;

import uk.ac.ncl.icos.synbad.graph.api.SBCrawlerListener;
import uk.ac.ncl.icos.synbad.graph.api.SBCursor;
import uk.ac.ncl.icos.synbad.graph.api.SBEdge;
import uk.ac.ncl.icos.synbad.graph.api.SBObject;


/**
 *
 * @author owengilfellon
 */
public interface SBHCrawlerListener<N extends SBObject, E extends SBEdge> extends SBCrawlerListener<N,E>{

    public void onProxyEdge(E edge);
    
    
    public abstract class SBHCrawlerListenerAdapter<N extends SBObject, E extends SBEdge> implements SBHCrawlerListener<N, E>
    {

        @Override
        public void onProxyEdge(E edge) {}

        @Override
        public void onEdge(E edge) {}

        @Override
        public void onEnded() {}

        @Override
        public void onInstance(N instance, SBCursor<N, E> cursor) {}

        @Override
        public boolean terminate() {return false;}

        @Override
        public boolean vetoEdge(E edge) {return false;}
        
    }
    
    
}
