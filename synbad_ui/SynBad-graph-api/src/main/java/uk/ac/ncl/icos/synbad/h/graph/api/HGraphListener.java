/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.h.graph.api;

import uk.ac.ncl.icos.synbad.event.SBEventType;


/**
 *
 * @author owengilfellon
 */
public interface HGraphListener<N, E> extends GraphListener<N, E> {

    public void onProxyEdgeEvent(SBEventType type, E edge);
    
}
