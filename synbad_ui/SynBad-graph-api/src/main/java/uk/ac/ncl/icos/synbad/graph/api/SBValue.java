/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph.api;

import java.net.URI;
import java.util.Objects;
import javax.xml.namespace.QName;

/**
 *
 * @author owengilfellon
 */
public class SBValue {
    
    private final Object value;
    private boolean isDouble = false;
    private boolean isBoolean = false;
    private boolean isInt = false;
    private boolean isString = false;
    private boolean isURI = false;
    private boolean isQName = false;

    public SBValue(boolean value) {
        this.value = value;
        this.isBoolean = true;
    }
    
    public SBValue(double value) {
        this.value = value;
        this.isDouble = true;
    }
    
    public SBValue(int value) {
        this.value = value;
        this.isInt = true;
    }
    
    public SBValue(String value) {
        this.value = value;
        this.isString = true;
    }
    
    public SBValue(URI value) {
        this.value = value;
        this.isURI = true;
    }
    
    public SBValue(QName value) {
        this.value = value;
        this.isQName = true;
    }
    
    public boolean asBoolean() {
        return (boolean) value;
    }
    
    public double asDouble() {
        return (double) value;
    }
        
    public int asInt() {
        return (int) value;
    }
            
    public String asString() {
        if(value instanceof String)
            return (String) value;
        else
            return value.toString();
    }
                
    public URI asURI() {
        return (URI) value;
    }
    
    public QName asQName() {
        return (QName) value;
    }

    public boolean isBoolean() {
        return isBoolean;
    }

    public boolean isDouble() {
        return isDouble;
    }

    public boolean isInt() {
        return isInt;
    }

    public boolean isQName() {
        return isQName;
    }

    public boolean isString() {
        return isString;
    }

    public boolean isURI() {
        return isURI;
    }

    public Object getValue() {
        return value;
    }

    @Override
    public String toString() {
        if(isURI)
            return asURI().toASCIIString();
        else
            return value.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null)
            return false;
        if(obj == this)
            return true;
        if(!(obj instanceof SBValue))
            return false;
        SBValue v = (SBValue) obj;
        return v.getValue().equals(value);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(value);
        return hash;
    }
    
    
    
    
}
