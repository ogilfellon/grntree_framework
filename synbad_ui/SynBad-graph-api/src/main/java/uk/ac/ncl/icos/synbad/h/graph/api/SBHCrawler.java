/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.h.graph.api;

import java.util.Set;
import uk.ac.ncl.icos.synbad.graph.api.SBEdge;
import uk.ac.ncl.icos.synbad.graph.api.SBObject;

/**
 *
 * @author owengilfellon
 */
public interface SBHCrawler<N extends SBObject, E extends SBEdge> extends  SBHCrawlerListener<N, E> {

    public void addListener(SBHCrawlerListener<N,E> listener);
    
    public void removeListener(SBHCrawlerListener<N,E> listener);
    
    public void run(SBHCursor<N,E> cursor);

    void processNode(SBHCursor<N, E> cursor, Set<E> seenEdges);
    
    void processEdge(SBHCursor<N,E> cursor, Set<E> seenEdges, E edge);
    
    
}
