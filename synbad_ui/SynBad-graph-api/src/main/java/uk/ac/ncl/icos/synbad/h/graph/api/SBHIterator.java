/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.h.graph.api;

import java.util.ListIterator;

/**
 *
 * @author owengilfellon
 */
public interface SBHIterator<T> extends ListIterator<T> {
    
    public int getDepth();
    
}
