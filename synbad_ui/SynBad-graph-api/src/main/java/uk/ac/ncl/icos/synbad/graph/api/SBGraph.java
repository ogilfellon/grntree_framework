/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph.api;

import java.util.Collection;
import java.util.Set;
import uk.ac.ncl.icos.synbad.h.graph.api.GraphListener;

/**
 * Basic graph interface
 * @author owengilfellon
 */
public interface SBGraph<N extends SBObject, E extends SBEdge> extends SBEdgeProvider<N, E> {

    // ==========================
    //          Nodes
    // ==========================
    
    public Set<N> nodeSet();
    
    public boolean containsNode(N node);
    
    public boolean addNode(N node);
    
    public boolean removeNode(N node);

    public boolean removeAllNodes(Collection<? extends N> nodes);

    int degreeOf(N node);

    int inDegreeOf(N node);

    int outDegreeOf(N node);

    // ==========================
    //          Edges
    // ==========================
    
    public Set<E> edgeSet();
    
    public boolean containsEdge(E edge);
    
    public boolean containsEdge(N from, N to);

    public boolean addEdge(N from, N to, E edge);
    
    public E getEdge(N from, N to);
    
    public boolean removeEdge(E edge);

    public boolean removeAllEdges(Collection<? extends E> edges);

    public E removeEdge(N sourceNode, N targetNode);
    
    public Set<E> removeAllEdges(N sourceNode, N targetNode);

    public Set<E> getAllEdges(N node);

    public Set<E> getAllEdges(N sourceNode, N targetNode);

    
    // ==========================
    //          Listeners
    // ==========================
   
    public void addListener(GraphListener<N, E> listener);
    
    public void removeListener(GraphListener<N, E> listener);

}