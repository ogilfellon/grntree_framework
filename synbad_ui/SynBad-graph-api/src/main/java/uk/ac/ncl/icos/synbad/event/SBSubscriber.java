/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.event;

import java.util.logging.Logger;

/**
 *
 * @author owengilfellon
 */
public interface SBSubscriber {
    
    public void onEvent(SBEvent e);

    public class MessageLogger implements SBSubscriber {
        
        Logger logger = Logger.getLogger(getClass().getName());

        @Override
        public void onEvent(SBEvent e) {
            System.out.println(e);
        }
    }
}
