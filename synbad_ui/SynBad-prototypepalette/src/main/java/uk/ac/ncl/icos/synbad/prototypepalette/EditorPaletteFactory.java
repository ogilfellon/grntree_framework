/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.prototypepalette;

import java.util.List;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Children;
import org.openide.nodes.Node;

/**
 *
 * @author owengilfellon
 */
public class EditorPaletteFactory extends ChildFactory<String> {

    @Override
    protected boolean createKeys(List<String> toPopulate) {
        toPopulate.add("root");
        return true;
    }

    @Override
    protected Node[] createNodesForKey(String key) {
        return new Node[] {
            new PaletteCategoryNode("Prototype SVPs", Children.create(new PrototypeNodeFactory("Prototype SVPs"), true))
        };
    }
    
    
    
}
