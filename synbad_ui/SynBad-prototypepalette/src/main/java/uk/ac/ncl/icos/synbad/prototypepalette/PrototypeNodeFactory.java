/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.prototypepalette;

import java.util.List;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Node;
import uk.ac.ncl.icos.grntree.api.GRNTreeNode;
import uk.ac.ncl.icos.grntree.api.GRNTreeNodeFactory;
import uk.ac.ncl.icos.grntree.api.SVPHelper;
import uk.ac.ncl.icos.grntree.impl.InterfaceType;
import uk.ac.ncl.icos.synbad.svpfragment.nodes.SVPLeafNode;

/**
 *
 * @author owengilfellon
 */
public class PrototypeNodeFactory extends ChildFactory<String> {
    
    private SVPHelper helper = SVPHelper.getSVPHelper();
    private String part;

    public PrototypeNodeFactory(String part) {
        this.part = part;
    }

    @Override
    protected boolean createKeys(List<String> toPopulate) {
        toPopulate.add(part);
        return true;
    }

    @Override
    protected Node[] createNodesForKey(String key) {
        return new Node[] {
            new SVPLeafNode(helper.getPromoter("Promoter", 0.5)),
            new SVPLeafNode(helper.getOperator("Operator")),
            new SVPLeafNode(helper.getRBS("RBS", 0.5)),
            new SVPLeafNode(helper.getCDS("CDS", 0.5)),
            new SVPLeafNode(helper.getTerminator("Terminator")),
            new SVPLeafNode(helper.getShim("Shim"))
        };
    }
}
