/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.prototypepalette;

import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;

/**
 *
 * @author owengilfellon
 */
public class PaletteCategoryNode extends AbstractNode {

    public PaletteCategoryNode(String category, Children children) {
        super(children);
        this.setDisplayName(category);
    }
    
}
