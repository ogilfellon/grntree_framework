/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.algorithm;



import akka.NotUsed;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.stream.ActorMaterializer;
import akka.stream.Materializer;
import akka.stream.javadsl.Sink;
import akka.stream.javadsl.Source;
import java.util.concurrent.CompletionStage;
import org.junit.Test;
import uk.ac.ncl.icos.datamodel.workspace.api.IWorkspace;
import uk.ac.ncl.icos.model.VModel;
import uk.ac.ncl.icos.workspace.factories.ExampleFactory;
import uk.ac.ncl.icos.workspace.factories.SVPFactory;
import uk.ac.ncl.synbad.svp.Svp;


/**
 *
 * @author owengilfellon
 */
public class AkkaStreamTest {

    final ActorSystem system = ActorSystem.create("MySystem");
    final Materializer mat = ActorMaterializer.create(system);
    ActorRef myActor;

    public AkkaStreamTest() {
       
    }

    /**
     * Test of onReceive method, of class EntityActor.
     */
    @Test
    public void testOnReceive() throws Exception {

       //final Source<Integer, NotUsed> source = Source.range(1, 100);
       // source.runForeach(i -> System.out.println(i), mat);
        
       
        VModel model = ExampleFactory.setupTemplateModel();
        IWorkspace workspace = model.getWorkspace();
        
        final Source<Svp, NotUsed> entities = Source
                .from(workspace.getEntities())
                .fold(
                        SVPFactory.getTerminator(workspace, null, null, null),
                        (a, b) -> a).drop(1);
        

        
        final Sink<Integer, CompletionStage<Integer>> sumSink =
             Sink.<Integer, Integer>fold(0, (a, b) -> a + b);
        
        // Select chromosome from a given workspace and a predicate
        //Source<IEntity, NotUsed> getPromoter = Source
        
        /*
        ViewCrawler crawler = new ViewCrawler();
        crawler.addListener(new CrawlerDebugger());
        crawler.addListener(new IViewCrawlerListener() {

            @Override
            public void onEdge(VEdge edge) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void onEnded() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void onInstance(VNode instance, ViewCursor cursor) {
            
                // if is source (i.e. supplying data)
                    // get output ports - and therefore, types
                    // create source, and store with id

   
               // Source<?, NotUsed> source = 
                        
                        
                        
                // if is function 
                    // get input and output ports - and therefore types
                
                // if is sink
            }

            @Override
            public boolean terminate() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public boolean vetoEdge(VEdge edge) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        });
        
        
        // An Operator
        
        // initialise with mutation operator implemented as a strategy
        
        Flow<IEntity, IEntity, NotUsed> operator = Flow
                .fromFunction((IEntity arg0) -> {
                    
                    // return operatorstrategy.execute
                    
                    return SVPFactory.getTerminator(workspace, "", "", "");
                    
        });
        
        final Source<Svp, NotUsed> svps = entities
                .filter(e -> e instanceof Svp)
                .map(e -> (Svp)e);
        
        final Source<IPort, NotUsed> ports = svps
                .filter(s -> !s.getPorts().isEmpty())
                .map(s -> s.getPorts())
                .mapConcat(p -> p);

        ports.runForeach(i -> System.out.println(i), mat);*/
    }
}
