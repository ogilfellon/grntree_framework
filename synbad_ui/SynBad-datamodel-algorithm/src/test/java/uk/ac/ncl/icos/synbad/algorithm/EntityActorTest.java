/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.algorithm;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;

import org.junit.Test;
import uk.ac.ncl.icos.datamodel.workspace.api.IEntity;
import uk.ac.ncl.icos.model.VModel;
import uk.ac.ncl.icos.workspace.factories.ExampleFactory;

/**
 *
 * @author owengilfellon
 */
public class EntityActorTest {

    private final VModel model;
    ActorSystem system = ActorSystem.create("MySystem");
    ActorRef myActor;
    
    public EntityActorTest() {
        model = ExampleFactory.setupTemplateModel();
        myActor = system.actorOf(Props.create(ModuleActor.class, (IEntity)model.getModelRoot().getData().getValue()));
    }

    /**
     * Test of onReceive method, of class EntityActor.
     */
    @Test
    public void testOnReceive() throws Exception {
        myActor.tell(model, ActorRef.noSender());
        myActor.tell("ask", ActorRef.noSender());
        Thread.sleep(1000);
    }
    
}
