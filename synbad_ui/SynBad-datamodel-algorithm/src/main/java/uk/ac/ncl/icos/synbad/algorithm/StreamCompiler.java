/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.algorithm;

import akka.stream.ClosedShape;
import akka.stream.FlowShape;
import akka.stream.Outlet;
import akka.stream.SinkShape;
import akka.stream.SourceShape;
import akka.stream.javadsl.Flow;
import akka.stream.javadsl.GraphDSL;
import akka.stream.javadsl.RunnableGraph;
import akka.stream.javadsl.Sink;
import akka.stream.javadsl.Source;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import uk.ac.ncl.icos.datamodel.workspace.api.IEntity;
import uk.ac.ncl.icos.datamodel.workspace.api.SynBadObject;
import uk.ac.ncl.icos.datamodel.workspace.impl.EntityInstance;
import uk.ac.ncl.icos.datamodel.workspace.impl.PortDirection;
import uk.ac.ncl.icos.datamodel.workspace.impl.PortRecord;
import uk.ac.ncl.icos.view.api.IViewCrawlerListener;
import uk.ac.ncl.icos.model.entities.VEdge;
import uk.ac.ncl.icos.model.VModel;
import uk.ac.ncl.icos.view.api.ViewCrawler;
import uk.ac.ncl.icos.view.impl.IteratorRule;
import uk.ac.ncl.icos.view.impl.ViewCrawlerAdapter;
import uk.ac.ncl.icos.workspace.factories.ExampleFactory;

/**
 *
 * @author owengilfellon
 */
public class StreamCompiler {
    
    private enum Type { SOURCE, FLOW, SINK};
    
    private final IEntity root;
    private final VModel model;
    private final Map<IEntity, Type> type;
    private final Map<IEntity, Source> sources;
    private final Map<IEntity, Flow> flows;
    private final Map<IEntity, Sink> sinks;
    
    
    public static void main(String[] args) {
        StreamCompiler c = new StreamCompiler();
        c.compile();
        System.out.println();
    }

    public StreamCompiler() {

        model = ExampleFactory.setupTemplateModel();
        this.root = (IEntity)model.getModelRoot().getData().getValue();
        sources = new HashMap<>();
        flows = new HashMap<>();
        sinks = new HashMap<>();
        type = new HashMap<>();
    }
    
    public void compile() {
        add(root);
        
        ViewCrawler crawler = new ViewCrawler();
        crawler.addListener(new IteratorRule());
        
        
        RunnableGraph.fromGraph(
            GraphDSL.create(b -> { 
                
                final Map<Source, SourceShape> sourceShapes = new HashMap<>();
                final Map<Flow, FlowShape> flowShapes = new HashMap<>();
                final Map<Sink, SinkShape> sinkShapes = new HashMap<>();
                
                for(IEntity e : sources.keySet()) {
                    sourceShapes.put(sources.get(e), (SourceShape) b.add(sources.get(e)));
                }
                
                for(IEntity e : flows.keySet()) {
                    flowShapes.put(flows.get(e), (FlowShape) b.add(flows.get(e)));
                }
                
                 for(IEntity e : sinks.keySet()) {
                    sinkShapes.put(sinks.get(e), (SinkShape)b.add(sinks.get(e)));
                }
                
                IViewCrawlerListener listener = new ViewCrawlerAdapter() {

                    @Override
                    public void onEdge(VEdge edge) {
                        super.onEdge(edge);
                        SynBadObject from = edge.getFrom().getData().getValue();
                        SynBadObject to = edge.getTo().getData().getValue();

                        IEntity efrom = null;
                        IEntity eto = null;

                        if(from instanceof PortRecord && to instanceof PortRecord) {
                            efrom = ((PortRecord)from).getOwner().getValue();
                            eto = ((PortRecord)to).getOwner().getValue();
                        }

                        if(type.get(efrom) == Type.SOURCE) {
                            Source source = sources.get(efrom);
                            SourceShape sourceShape = sourceShapes.get(efrom);
                            if(type.get(eto) == Type.FLOW) {
                               b.from(sourceShape).via(flowShapes.get(flows.get(eto)));                              
                            } else if (type.get(eto) == Type.SINK) {
                               b.from(sourceShape).to(sinkShapes.get(sinks.get(eto)));
                            }
                        } 

                        if(type.get(efrom) == Type.FLOW) {
                            Flow flow = flows.get(efrom);
                            FlowShape flowShape = flowShapes.get(flows.get(efrom));
                            if(type.get(eto) == Type.FLOW) {
                               b.from(flowShape).via(flowShapes.get(flows.get(eto)));
                            } else if (type.get(eto) == Type.SINK) {
                               b.from(flowShape).to(sinkShapes.get(sinks.get(eto)));
                            }
                        }                 
                    }
                };
                
                crawler.addListener(listener);  
                crawler.run(model.getDefaultView().getView().createCursor());
            
                return ClosedShape.getInstance();
            
            }));
    }
    
    private void add(IEntity entity) {
        
        if(entity.getPort(p -> p.getPortDirection() == PortDirection.IN) == null) {
            
            // is source
            
            if(entity.getPort(p -> p.getPortDirection() == PortDirection.OUT) != null) {
                sources.put(entity, Source.from(Arrays.asList(entity.getName())));
                type.put(entity, Type.SOURCE);
            }
        } else if (entity.getPort(p -> p.getPortDirection() == PortDirection.OUT) != null) {
            
            // is flow

            flows.put(entity, Flow.of(String.class));
            type.put(entity, Type.FLOW);

        } else {
            
            // is sink
            
            sinks.put(entity, Sink.foreach(s -> System.out.println(s + "..." + entity.getName())));
            type.put(entity, Type.SINK);
        }
        
        
        for(EntityInstance i : entity.getChildren()) {
            add(i.getValue());
        }
    }
}
