/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.algorithm;

import akka.stream.Attributes;
import akka.stream.FlowShape;
import akka.stream.Inlet;
import akka.stream.Outlet;
import akka.stream.stage.AbstractInHandler;
import akka.stream.stage.AbstractOutHandler;
import akka.stream.stage.GraphStage;
import akka.stream.stage.GraphStageLogic;

/**
 *
 * @author owengilfellon
 */
public class ApplyOperator<T> extends GraphStage<FlowShape<T, T>> {

    public Inlet<T> in = Inlet.<T>create("HoldWithInitial.in");
    public Outlet<T> out = Outlet.<T>create("HoldWithInitial.out");
    private FlowShape<T, T> shape = FlowShape.of(in, out);
    private final T initialValue;
    
    
    public ApplyOperator(T initial) {
        this.initialValue = initial;
    }
    
    @Override
    public GraphStageLogic createLogic(Attributes atrbts) {
        return new GraphStageLogic(shape) {
            
            private T currentValue = null;
          
      
            
            {
                setHandler(in, new AbstractInHandler() {

                    @Override
                    public void onPush() throws Exception {
                        currentValue = grab(in);
                        pull(in);
                    }
                });
                
                setHandler(out, new AbstractOutHandler() {

                    @Override
                    public void onPull() throws Exception {
                        push(out, currentValue);
                    }
                });
            }
            
        };
    }

    @Override
    public FlowShape<T, T> shape() {
        return shape;
    }
    
}
