/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.algorithm;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;
import java.util.logging.Level;
import java.util.logging.Logger;

import uk.ac.ncl.icos.datamodel.workspace.api.IEntity;
import uk.ac.ncl.icos.datamodel.workspace.impl.EntityInstance;

/**
 *
 * @author owengilfellon
 */
public class ModuleActor extends UntypedActor {
    
    private static final Logger logger = Logger.getLogger(ModuleActor.class.getName());
    private final IEntity entity;
    private int messageCount = 0;

    public ModuleActor(IEntity entity) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
        this.entity = entity;
        for(EntityInstance child : entity.getChildren()) {
            getContext().actorOf(Props.create(ModuleActor.class, child.getValue()), child.getValue().getDisplayID().replace("/", "_"));
        }
        
        //Class<?> clazz = ClassLoader.getSystemClassLoader().loadClass("Intity");
        //IEntity i = (IEntity)clazz.newInstance();
    }

    @Override
    public void onReceive(Object o) throws Exception {
        
        messageCount++;
         
        if(!(o instanceof String)) {
            unhandled(o);
            return;
        }

        String s = (String) o;
        if (s.equals("ask")) {
            logger.log(Level.INFO, "{0}: Received {1} in {2}", new Object[]{messageCount, o, entity});
            
            for(ActorRef ref : getContext().getChildren()) {
                ref.tell("ask", getSelf());
            }
        } else {
            unhandled(o);
        }  
    }
}
