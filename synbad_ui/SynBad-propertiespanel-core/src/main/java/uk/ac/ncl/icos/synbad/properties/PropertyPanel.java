/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.properties;

import java.awt.Color;
import java.awt.GridBagLayout;
import java.util.Collection;
import javax.swing.JPanel;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.Utilities;

/**
 *
 * To implement a property panel, extend PropertyPanel.Adapter<T>, typed for the
 * Objects whose properties which will be displayed.
 * 
 * Annotate the class with @ServiceProvider(service=PropertyPanel.class), and pass
 * a class object to the super constructor.
 * 
 * @author owengilfellon
 */
public interface PropertyPanel {

    public void requestOpen();
    public void setContainer(PropertiesTopComponent container);
    public String getDisplayName();

    public abstract class Adapter<T> extends JPanel implements PropertyPanel, LookupListener {

        private Lookup.Result<T> result = null;
        private Collection<? extends T> instances = null;
        private PropertiesTopComponent container = null;
        protected GridBagLayout layout = null;

        public Adapter(Class<T> clazz) {
            result = Utilities.actionsGlobalContext().lookupResult(clazz);
            result.addLookupListener(this);
            layout = new GridBagLayout();
            this.setLayout(new GridBagLayout());
        }

        @Override
        public void setContainer(PropertiesTopComponent container) {
            this.container = container;
        }
        
        @Override
        public void requestOpen() {
            container.requestOpen(this);
        }

        @Override
        public void resultChanged(LookupEvent ev) {
            instances = result.allInstances();
            if(!instances.isEmpty()) {
                hasResult(instances);
                requestOpen();
            }
        }

        /**
         * Do something with the search result (i.e. display properties)
         * @param instances 
         */
        public abstract void hasResult(Collection<? extends T> instances);
        public abstract String getDisplayName();
    }
}
