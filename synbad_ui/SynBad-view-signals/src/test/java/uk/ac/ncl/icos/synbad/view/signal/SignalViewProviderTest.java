/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.view.signal;

import java.io.File;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.netbeans.junit.MockServices;
import org.openide.util.Exceptions;
import org.sbolstandard.core2.SBOLDocument;
import org.sbolstandard.core2.SBOLReader;
import uk.ac.ncl.icos.svpmanager.SVPManager;
import uk.ac.ncl.icos.synbad.datadefinition.IDataDefinitionProvider;
import uk.ac.ncl.icos.synbad.datadefinition.URIHelper;
import uk.ac.ncl.icos.synbad.datamodel.EntityManager;
import uk.ac.ncl.icos.synbad.datamodel.observers.EntityManagerDebugger;
import uk.ac.ncl.icos.synbad.datamodel.parser.SBOLParser;

/**
 *
 * @author owengilfellon
 */
public class SignalViewProviderTest {

    private final EntityManager manager;
    private final SVPManager m = SVPManager.getSVPManager();

    public SignalViewProviderTest() {
        MockServices.setServices(IDataDefinitionProvider.DefaultDefinitionProvider.class);
        manager = new EntityManager.DefaultDefinitionManager(URIHelper.synbad);
        manager.addListener(new EntityManagerDebugger());
        SBOLDocument document;
        try {
            document = SBOLReader.read(new File(System.getProperty("user.home") + "/PspaSConstruct.xml"));
            SBOLParser parser = new SBOLParser(manager);
            parser.convert(document);
            
            /*SVPParser parser = new SVPParser(manager);
            parser.convert(m.getInducPromoter());
            parser.convert(m.getRBS());
            parser.convert(m.getNegOperator());/**/
        } catch (Throwable ex) {
            Exceptions.printStackTrace(ex);
        }
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getView method, of class SignalViewProvider.
     */
    @Test
    public void testGetView() {
        
        SignalViewProvider instance = new SignalViewProvider();
        SignalView result = instance.getView(manager);
        assertNotNull(result);
    }
    
}
