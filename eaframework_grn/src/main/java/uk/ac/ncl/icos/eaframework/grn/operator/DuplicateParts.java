package uk.ac.ncl.icos.eaframework.grn.operator;

import uk.ac.ncl.icos.eaframework.operator.AbstractOperator;
import uk.ac.ncl.icos.svpmanager.SVPManager;
import uk.ac.ncl.icos.grntree.api.GRNTree;
import uk.ac.ncl.icos.grntree.api.GRNTreeNode;
import uk.ac.ncl.icos.grntree.impl.InterfaceType;
import uk.ac.ncl.icos.grntree.impl.LeafNode;
import uk.ac.ncl.icos.grntree.impl.SVPType;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.ncl.icos.eaframework.grn.GRNTreeChromosome;
import uk.ac.ncl.icos.grntree.api.GRNTreeNodeFactory;



/**
 * Randomly duplicates an RBS-CDS pair from a randomly chosen Transcriptional
 * Unit, and places them within a second randomly chosen Transcriptional Unit.
 * 
 * @author owengilfellon
 */
public class DuplicateParts extends AbstractOperator<GRNTreeChromosome> {
    
    final private SVPManager m = SVPManager.getSVPManager();
    final private int MAX_ATTEMPTS = 10;

    @Override
    public GRNTreeChromosome apply(GRNTreeChromosome c) {
        
        GRNTreeChromosome t = (GRNTreeChromosome) c.duplicate();
        List<GRNTreeNode> allTranscriptionalUnits = new ArrayList<GRNTreeNode>();
        Iterator<GRNTreeNode> it = t.getBreadthFirstIterator();

        // Retrieve all TUs from model by iterating through nodes and testing
        
        while(it.hasNext()) {
            GRNTreeNode n = it.next();
            if(n.isTranscriptionUnit()) {
                allTranscriptionalUnits.add(n);
            }
        }
        
        // If no TUs, then mutation cannot be performed. Return t.
        
        if(!allTranscriptionalUnits.isEmpty()){
            
            Random r = new Random();
            boolean mutationOccured = false;
            int attempt = 0;
            
            /*
             * Loop retrieves two random TUs and attempts mutation, while no
             * mutation has occured. In the (exceptional) case in which the
             * model has no TUs containing RBS nodes, a MAX_ATTEMPTS is specified
             * to terminate the loop.
             */
            
            while(!mutationOccured && attempt < MAX_ATTEMPTS) {
                
                GRNTreeNode source = allTranscriptionalUnits.get(r.nextInt(allTranscriptionalUnits.size()));
                
                /*
                 * Locates all RBS-CDS pairs in the first Transcriptional Unit.
                 * This can be done by identifying RBS nodes, as all RBS nodes
                 * will be followed by a CDS node, and all CDS nodes are preceded
                 * by RBS nodes.
                 */
                
                List<GRNTreeNode> startPoints = new ArrayList<GRNTreeNode>();

                for(GRNTreeNode n:source.getChildren()){
                    if(n instanceof LeafNode) {
                        LeafNode ln = (LeafNode) n;
                        if(ln.getType() == SVPType.RBS){
                            startPoints.add(n);
                        }
                    }
                }
                
                /*
                 * Although a Transcriptional Unit without RBSs should not occur,
                 * if such an example is found, the while loop will be repeated.
                 */

                if(!startPoints.isEmpty()){
                    
                    try {
                        
                        // Choose a random RBS-CDS pair from those identified
                        
                        LeafNode rbs = (LeafNode) startPoints.get(r.nextInt(startPoints.size()));
                        LeafNode cds = (LeafNode) rbs.getParent().getChildren().get(rbs.getParent().getChildren().indexOf(rbs) + 1);
                        
                        /*
                         * Identify valid destinations for the chosen CDS, to
                         * preserve modularity. If there are no valid destinations
                         * for the chosen CDS, the loop is repeated.
                         */
                        
                        List<GRNTreeNode> validDestinations = t.getValidLocations(cds);
                        
                        if(!validDestinations.isEmpty())
                        {
                            GRNTreeNode destination = validDestinations.get(r.nextInt(validDestinations.size()));
                            
                            /*
                             * Add the RBS-CDS pair before the Terminator of
                             * the destination TU.
                             */

                            destination.addNode(    destination.getChildren().size()-1,
                                                    GRNTreeNodeFactory.getLeafNode(rbs.getSVP(), InterfaceType.NONE));
                            destination.addNode(    destination.getChildren().size()-1,
                                                    GRNTreeNodeFactory.getLeafNode(cds.getSVP(), InterfaceType.OUTPUT));
                            
                            mutationOccured = true;
                        }
                    } catch (Exception ex) {
                        Logger.getLogger(DuplicateParts.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
              
                attempt++;
            }
        }

        return t;
    }
}
