/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.grn.fitness;

import uk.ac.ncl.icos.annotation.EAModule;
import uk.ac.ncl.icos.datatypes.Fitness;
import uk.ac.ncl.icos.eaframework.grn.simulator.CopasiSimulator;
import uk.ac.ncl.icos.eaframework.grn.Exporter;
import uk.ac.ncl.icos.eaframework.grn.exporter.SVPWriteExporter;
import uk.ac.ncl.icos.eaframework.grn.simulator.DependentValueCopasiSimulator;
import uk.ac.ncl.icos.svpcompiler.parsing.SVPParser;
import uk.ac.ncl.icos.grntree.api.GRNTree;

import java.util.ArrayList;
import uk.ac.ncl.icos.eaframework.grn.GRNTreeChromosome;

/**
 *
 * @author owengilfellon
 */

@EAModule(visualName = "Amplitude")
public class GRNAmplitudeSimulation extends GRNSimulationEvaluator<GRNTreeChromosome> {
    
    double target;
    
    public GRNAmplitudeSimulation(CopasiSimulator cs, double target)
    {
        super(cs);
        this.target = target;
    }
    
    @Override
    public Fitness evaluate(GRNTreeChromosome c)
    {    
        DependentValueCopasiSimulator ds = (DependentValueCopasiSimulator) cs;        
        GRNTreeChromosome g = (GRNTreeChromosome) c;
        Double t = Double.valueOf(target);
        Exporter<String> exporter = new SVPWriteExporter();
        ds.setModel(SVPParser.getSBML(exporter.export(g)));
        ds.run();
        ArrayList<Double> dependentsFinalRun = ds.getDependentMetaboliteValues().get(ds.getDependentMetaboliteValues().size()-1);
        Double o = dependentsFinalRun.get(dependentsFinalRun.size()-1);
        return new Fitness(Math.abs(t - o)); 
    }

}
