package uk.ac.ncl.icos.eaframework.grn.booleannetwork.ActivationFunctions;

import uk.ac.ncl.icos.eaframework.grn.booleannetwork.ActivationFunction;
import uk.ac.ncl.icos.eaframework.grn.booleannetwork.BooleanNode;

/**
 * Created by owengilfellon on 03/07/2014.
 */
public class Repress implements ActivationFunction {

    private BooleanNode repressor;

    public Repress(BooleanNode repressor) {
        this.repressor = repressor;
    }

    @Override
    public boolean assess() {
        return !repressor.getState();
    }
}
