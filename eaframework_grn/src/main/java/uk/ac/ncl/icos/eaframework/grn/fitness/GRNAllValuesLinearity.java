/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.grn.fitness;

import org.apache.commons.math3.stat.correlation.PearsonsCorrelation;
import uk.ac.ncl.icos.datatypes.Fitness;
import uk.ac.ncl.icos.eaframework.grn.Exporter;
import uk.ac.ncl.icos.eaframework.grn.GRNTreeChromosome;
import uk.ac.ncl.icos.eaframework.grn.exporter.SVPWriteExporter;
import uk.ac.ncl.icos.eaframework.grn.simulator.CopasiSimulator;
import uk.ac.ncl.icos.eaframework.grn.simulator.DynamicTimeCopasiSimulator;
import uk.ac.ncl.icos.svpcompiler.parsing.SVPParser;


/**
 *
 * @author owengilfellon
 */
public class GRNAllValuesLinearity extends GRNSimulationEvaluator<GRNTreeChromosome> {
     
    public GRNAllValuesLinearity(CopasiSimulator cs)
    {
        super(cs);
    }
    
    @Override
    public Fitness evaluate(GRNTreeChromosome c)
    {    
       
        DynamicTimeCopasiSimulator ds = (DynamicTimeCopasiSimulator) cs;        
        GRNTreeChromosome g = (GRNTreeChromosome) c;
       // Double t = Double.valueOf(target);
        Exporter<String> exporter = new SVPWriteExporter();
        ds.setModel(SVPParser.getSBML(exporter.export(g)));
        if(ds.run())
        {
            double[] responseCurve = new double[ds.getDependentMetaboliteValues().size()];
            double[] independentsRun = new double[ds.getIndependentMetaboliteValues().size()];

            for(int i=0; i<ds.getDependentMetaboliteValues().size(); i++)
            {
                responseCurve[i] = ds.getDependentMetaboliteValues().get(i).get(ds.getDependentMetaboliteValues().get(i).size()-1).doubleValue();
                independentsRun[i] = ds.getIndependentMetaboliteValues().get(i).doubleValue();
            }

            PearsonsCorrelation pc = new PearsonsCorrelation();
            double correlation = pc.correlation(independentsRun, responseCurve);
            double fitness = correlation > 0.0 ? correlation * 100 : 0.0;

            return new Fitness(fitness);
        }
        else
        {
            return new Fitness(0.0);
        }           
    } 
}
