package uk.ac.ncl.icos.eaframework.experiments;

import org.apache.log4j.PropertyConfigurator;
import uk.ac.ncl.icos.eaframework.chromosome.EvaluatedChromosome;
import uk.ac.ncl.icos.eaframework.evoengines.EvoEngine;
import uk.ac.ncl.icos.eaframework.factories.ChromosomeFactory;
import uk.ac.ncl.icos.eaframework.fitness.Evaluator;
import uk.ac.ncl.icos.eaframework.grn.GRNChromosomeFactory;
import uk.ac.ncl.icos.eaframework.grn.GRNTreeEngine;
import uk.ac.ncl.icos.eaframework.grn.fitness.GRNLinearityWithPartPenalties;
import uk.ac.ncl.icos.eaframework.grn.observer.SVPSimpleTreeObserver;
import uk.ac.ncl.icos.eaframework.grn.observer.SVPTreeResultsFileWriter;
import uk.ac.ncl.icos.eaframework.grn.operator.*;
import uk.ac.ncl.icos.eaframework.grn.simulator.DynamicTimeCopasiSimulator;
import uk.ac.ncl.icos.eaframework.observer.EvolutionObserver;
import uk.ac.ncl.icos.eaframework.operator.Operator;
import uk.ac.ncl.icos.eaframework.scheduler.ParameterScheduler;
import uk.ac.ncl.icos.eaframework.selection.MutantChamp;
import uk.ac.ncl.icos.eaframework.selection.Selection;
import uk.ac.ncl.icos.eaframework.terminationconditions.FitnessTermination;
import uk.ac.ncl.icos.eaframework.terminationconditions.GenerationTermination;
import uk.ac.ncl.icos.eaframework.terminationconditions.TerminationCondition;
import uk.ac.ncl.icos.eaframework.terminationconditions.TerminationGroup;
import uk.ac.ncl.icos.grntree.api.GRNTree;
import uk.ac.ncl.icos.grntree.api.GRNTreeFactory;

import java.util.ArrayList;
import java.util.List;
import uk.ac.ncl.icos.eaframework.grn.GRNTreeChromosome;

/**
 * Sain class for the CIBCB experiments. Uses a Mutant Champ strategy to evolve the SpaR-SpaK system to produce a
 * linear response to Subtilin, as determined by the concentration of GFP expressed by a PspaS promoter.
 *
 * @author owengilfellon
 */
public class TreeEvoTest {
    
    static final int POPULATION_SIZE = 1;
    static final int MAX_PARTS = 20;
    static final int MAX_TUS = 4;
    static final int MUTATIONS_PER_GENERATION = 1;


    public static void experiment(String[] args)
    {  

        // Add logger - primarily used to ignore SBML warnings

        PropertyConfigurator.configure("log4j.properties");


        /*
            The algorithm is run using a the GRNTreeEngine extension to the EvoEngine class.
            It takes:
                a ChromosomeFactory, to generate the population from a seed population
                an Operator, to perform mutations on the population
                a Selection class, to select members of the population for reproduction
                an Evaluator (i.e. Fitness Function), that determines the fitness of individuals in the population
                a TerminationCondition, to determine when the algorithm should terminate

         */

        /*
            Create a ChromosomeFactory by passing seed models, and a population size
         */

        List<GRNTreeChromosome> seed = new ArrayList<GRNTreeChromosome>();
        seed.add(new GRNTreeChromosome(GRNTreeFactory.getGRNTree("PspaRK:Prom; RBS_SpaR:RBS; SpaR:CDS; RBS_SpaK:RBS; SpaK:CDS; BO_4296:Ter;" +
                "PspaS:Prom; RBS_SpaS:RBS; GFP_rrnb:CDS; BO_4296:Ter")));
        ChromosomeFactory<GRNTreeChromosome> cf = new GRNChromosomeFactory(seed, POPULATION_SIZE);
        
        /*
         * Create an operator, in this case, an OperatorGroup which combines other mutation Operators.
         */

        List<Operator<GRNTreeChromosome>> operators = new ArrayList<Operator<GRNTreeChromosome>>();
        operators.add(new RandomiseRBS());


        operators.add(new AddRegulation());
        operators.add(new DuplicateParts());
        operators.add(new DuplicateTU());
        operators.add(new RandomiseConstPromoter());


        operators.add(new RemoveRegulation());
        operators.add(new SplitTU());
        operators.add(new SwapParts());

        operators.add(new RemoveParts());  /**/
        Operator<GRNTreeChromosome> o = new OperatorGroup(operators, MUTATIONS_PER_GENERATION);

        /*
         * Create a termination condition, in this case, a termination group which evaluates multiple termination
         * conditions
         */
  
        List<TerminationCondition> tc = new ArrayList<>();
        tc.add(new GenerationTermination(1000));
        tc.add(new FitnessTermination(100.0));
        TerminationCondition t = new TerminationGroup(tc);
        
        /*
         * Create a Simulator, specifying run length (in seconds) and duration (in steps), the number of different
         * metabolite values to simulate, and the range of metabolite concentrations. An independent and dependant
         * metabolite are specified (i.e. the metabolite to be varied, and the metabolite to be observed).
         */

        // 6 Hour Simulation
        DynamicTimeCopasiSimulator cs = new DynamicTimeCopasiSimulator(21600, 2160);

        // Parameters of 20 Minute Simulation - 1200, 1200
        // Parameters of Original Simulation - 15000, 1500
        
        cs.setNumberOfMetaboliteIncrements(100);
        cs.setMaximumMetaboliteConcentration(10000);
        cs.setMetabolitePair("Subtilin", "GFP_rrnb");
        
        /*
         * Create a Selection strategy - in this case, a Mutant Champ strategy.
         */
        
        Selection<GRNTreeChromosome> s = new MutantChamp<>();

        /*
         * An evaluator is created, passing the simulator as an argument for the purposes of assessing the SBML models.
         * This fitness function takes additional parameters, for the purposes of specifying acceptable metabolite
         * concentrations and model sizes.
         */

        ParameterScheduler scheduler = new ParameterScheduler(1, 800, 40.0, 20.0);


        Evaluator<GRNTreeChromosome> f = new GRNLinearityWithPartPenalties(cs, 0, 21428.5714, scheduler, 20);
        
        /*
         * Create the EA, passing the Chromosome, Operators, Fitness Evaluators and Termination Conditions.
         */
        
        EvoEngine<GRNTreeChromosome> e = new GRNTreeEngine(cf, o, s, f, t);
        
        /*
         * Create an SVPTreeObserver implementing EvolutionObserver. SVPTreeObserver takes a list of metabolite names,
         * the timecourses of which are retrieved from the COPASI simulations and recorded to disk.
         * The EvolutionObservers are attached to the EvoEngine.
         */
        
        ArrayList<String> toRecord = new ArrayList<>();
        toRecord.add("GFP_rrnb");
        EvolutionObserver observer = new SVPSimpleTreeObserver();
        EvolutionObserver writer = new SVPTreeResultsFileWriter("LinearityExperiments", toRecord);
        //EvolutionObserver debugger = new NodeManagerDebugger();

        e.attach(observer);
        e.attach(writer);
        //e.attach(debugger);
        e.attach((EvolutionObserver)scheduler);
        
        /*
         * The algorithm is run. The algorithm will continue until one of the termination conditions is met, and output
         * it's progress to console and to disk.
         */
        
        e.run(); 
    }

     
}
