package uk.ac.ncl.icos.eaframework.grn.operator;

import uk.ac.ncl.icos.annotation.EAModule;
import uk.ac.ncl.icos.eaframework.operator.AbstractOperator;
import uk.ac.ncl.icos.svpmanager.SVPManager;
import uk.ac.ncl.icos.grntree.api.GRNTree;
import uk.ac.ncl.icos.grntree.api.GRNTreeNode;
import uk.ac.ncl.icos.grntree.impl.LeafNode;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.ncl.icos.eaframework.grn.GRNTreeChromosome;

/**
 * A random part is chosen from a randomly selected transcriptional unit, and
 * is swapped with a randomly chosen part of the same type in a second random
 * TU.
 * 
 * @author owengilfellon
 */
public class SwapParts extends AbstractOperator<GRNTreeChromosome> {

    private final SVPManager m = SVPManager.getSVPManager();
    private final int        MAX_ATTEMPTS = 10;

    @Override
    public GRNTreeChromosome apply(GRNTreeChromosome c) {

        GRNTreeChromosome t = null;
        Random r = new Random();
        boolean mutationOccured = false;
        int attempt = 0;

        while ( !mutationOccured && attempt < MAX_ATTEMPTS ) {

            t = (GRNTreeChromosome) c.duplicate();
            List<GRNTreeNode> allTranscriptionalUnits = new ArrayList<GRNTreeNode>();
            Iterator<GRNTreeNode> it = t.getBreadthFirstIterator();

            while ( it.hasNext() ) {
                GRNTreeNode n = it.next();
                if ( n.isTranscriptionUnit() ) {
                    allTranscriptionalUnits.add( n );
                }
            }

            if ( allTranscriptionalUnits.isEmpty() ) {
                return c;
            }

            GRNTreeNode tu1 = allTranscriptionalUnits.get( r.nextInt( allTranscriptionalUnits.size() ) );
            GRNTreeNode tu2 = allTranscriptionalUnits.get( r.nextInt( allTranscriptionalUnits.size() ) );

            LeafNode node1 = (LeafNode) tu1.getChildren().get( r.nextInt( tu1.getChildren().size() ) );
            List<GRNTreeNode> potentialNode2s = new ArrayList<GRNTreeNode>();

            for ( GRNTreeNode node : tu2.getChildren() ) {
                LeafNode potentialNode2 = (LeafNode) node;
                if ( node1.getType().equals( potentialNode2.getType() ) ) {
                    potentialNode2s.add( node );
                }
            }

            if (!potentialNode2s.isEmpty()) {

                LeafNode node2 = (LeafNode) potentialNode2s.get( r.nextInt( potentialNode2s.size() ) );

                if (node1 != node2) {
                    try {
                        GRNTreeNode node1Copy = node1.duplicate();
                        GRNTreeNode node2Copy = node2.duplicate();
                        node1.replaceNode(node2Copy);
                        node2.replaceNode(node1Copy);
                        mutationOccured = true;
                    } catch (Exception ex) {
                        Logger.getLogger(SwapParts.class.getName()).log(Level.SEVERE, null, ex);
                        return c;
                    }
                }
            }

            attempt++;

            if(!mutationOccured && attempt == MAX_ATTEMPTS) {
                return c;
            }
        }

        return t;
    }
}
