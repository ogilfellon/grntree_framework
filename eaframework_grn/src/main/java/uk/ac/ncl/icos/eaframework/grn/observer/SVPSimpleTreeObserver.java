package uk.ac.ncl.icos.eaframework.grn.observer;

import uk.ac.ncl.icos.eaframework.chromosome.Chromosome;
import uk.ac.ncl.icos.eaframework.chromosome.EvaluatedChromosome;
import uk.ac.ncl.icos.eaframework.evoengines.EvoEngine;
import uk.ac.ncl.icos.eaframework.observer.EvolutionObserver;
import uk.ac.ncl.icos.eaframework.observer.EvolutionSubject;
import uk.ac.ncl.icos.eaframework.selection.AbstractMutantChamp;
import uk.ac.ncl.icos.eaframework.selection.Selection;
import uk.ac.ncl.icos.eaframework.stats.PopulationStats;

import java.io.Serializable;
import java.util.List;
import uk.ac.ncl.icos.eaframework.grn.GRNTreeChromosome;

/**
 *
 * @author owengilfellon
 */
public class SVPSimpleTreeObserver implements EvolutionObserver<Chromosome>, Serializable {

    public SVPSimpleTreeObserver() {
    }

    @Override
    public void update(EvolutionSubject<Chromosome> s) {
       
            EvoEngine e = (EvoEngine) s;
            List<EvaluatedChromosome> pop = e.getEvaluatedPopulation();
            Selection selection = e.getSelectionStrategy();
            if(selection instanceof AbstractMutantChamp) {

                double bestFitness = ((AbstractMutantChamp) selection).getBestChromosome() != null ?
                                     ((AbstractMutantChamp) selection).getBestChromosome().getFitness().getFitness() :
                                     0.0;

                /*if(((AbstractMutantChamp) selection).getBestChromosome() != null )
                {
                    System.out.println(((AbstractMutantChamp) selection).getBestChromosome().getChromosome());
                }*/

                PopulationStats ps = e.getPopulationStats();
                StringBuilder sb = new StringBuilder();

                sb.append("Gen: ").append(ps.getCurrentGeneration()).append(" | ");
                if(ps.getBestChromosome() instanceof GRNTreeChromosome){
                    GRNTreeChromosome t = (GRNTreeChromosome) ps.getBestChromosome();
                    sb.append("Parts: ").append(t.getPartsSize()).append(" | ");
                }

                sb.append("Curr Fit: ").append(ps.getBestFitness()).append(" | ");
                sb.append("Best Fit: ").append(bestFitness).append("\n");
                System.out.print(sb.toString());
            }

    }
    
}
