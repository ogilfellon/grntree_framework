package uk.ac.ncl.icos.eaframework.grn.observer;

import uk.ac.ncl.icos.datatypes.Fitness;
import uk.ac.ncl.icos.eaframework.chromosome.Chromosome;
import uk.ac.ncl.icos.eaframework.chromosome.EvaluatedChromosome;
import uk.ac.ncl.icos.eaframework.evoengines.EvoEngine;
import uk.ac.ncl.icos.eaframework.grn.Exporter;
import uk.ac.ncl.icos.eaframework.grn.GRNTreeEngine;
import uk.ac.ncl.icos.eaframework.grn.exporter.GRNTreeEngineSave;
import uk.ac.ncl.icos.eaframework.grn.exporter.SVPWriteExporter;
import uk.ac.ncl.icos.eaframework.grn.fitness.GRNLinearityWithPartPenalties;
import uk.ac.ncl.icos.eaframework.grn.fitness.GRNSimulationEvaluator;
import uk.ac.ncl.icos.eaframework.grn.simulator.DynamicTimeCopasiSimulator;
import uk.ac.ncl.icos.eaframework.observer.EvolutionObserver;
import uk.ac.ncl.icos.eaframework.observer.EvolutionSubject;
import uk.ac.ncl.icos.eaframework.scheduler.ParameterScheduler;
import uk.ac.ncl.icos.eaframework.selection.AbstractMutantChamp;
import uk.ac.ncl.icos.eaframework.selection.Selection;
import uk.ac.ncl.icos.filemanagement.Export;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import uk.ac.ncl.icos.eaframework.grn.GRNTreeChromosome;

/**
 *
 * @author owengilfellon
 */
public class SVPTreeResultsFileWriter implements EvolutionObserver<Chromosome>, Serializable {

    private final List<String> metabolites;
    private final String experimentName;
    private String timestamp;
    
    public SVPTreeResultsFileWriter(String experimentName,
                                    List<String> metabolitesToRecord) {
         this.metabolites = metabolitesToRecord;
         DateFormat dateFormat = new SimpleDateFormat("yyMMdd_HHmmss");
         this.timestamp = dateFormat.format(new Date());
         this.experimentName = experimentName;
    }

    public String getExperimentDirectory()
    {
        return experimentName + "_" + timestamp;
    }

    public String getTimestamp()
    {
        return timestamp;
    }

    public void setTimestamp(String timestamp)
    {
        this.timestamp = timestamp;
    }

    @Override
    public void update(EvolutionSubject<Chromosome> s) {

        EvoEngine e = (EvoEngine) s;

        //System.out.print("Serializing....");

        final File homeDir = new File(System.getProperty("user.home"));
        File directory = new File(homeDir + "/" +  "Results" + "/" + experimentName + "/" +  experimentName + "_" + timestamp);
        if (!directory.exists()) {
            if (!directory.mkdirs()) {
                System.out.println("Couldn't create directory: " + experimentName + "_" + timestamp);
            }
        }

        File file = new File(directory + "/" + "Save.sez");

        try {
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(new GRNTreeEngineSave((GRNTreeEngine)e));
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        }

        //System.out.println("...done");


        int parameter = 0;
            List<EvolutionObserver> observers = e.getObservers();

            for(EvolutionObserver observer:observers) {
                if(observer instanceof ParameterScheduler) {
                    ParameterScheduler scheduler = (ParameterScheduler) observer;
                    parameter = (int)scheduler.getParameter();
                }
            }

            EvaluatedChromosome evaluatedChromosome = (EvaluatedChromosome) e.getEvaluatedPopulation().get(0);
            GRNTreeChromosome t = (GRNTreeChromosome) evaluatedChromosome.getChromosome();
            Fitness f = evaluatedChromosome.getFitness();

            /*
             * Write Operation used
             */

            String operator = evaluatedChromosome.getOperator() == null ? "NONE" : evaluatedChromosome.getOperator();
            ArrayList<Double> fitness = new ArrayList<Double>();
            fitness.add(f.getFitness());
            ArrayList<Double> responseCurve = new ArrayList<Double>();
            StringBuilder penalties = new StringBuilder();

            if(e.getFitnessEvaluator() instanceof GRNSimulationEvaluator) {

                GRNSimulationEvaluator se = (GRNSimulationEvaluator) e.getFitnessEvaluator();

                if(se instanceof GRNLinearityWithPartPenalties) {
                    GRNLinearityWithPartPenalties lwp = (GRNLinearityWithPartPenalties) se;
                    penalties.append("Corr: ").append(lwp.getCorrelation()).append(" | ");
                    penalties.append("LowPen: ").append(lwp.getLowerBoundPenalty()).append(" | ");
                    penalties.append("HighPen: ").append(lwp.getUpperBoundPenalty()).append(" | ");
                    penalties.append("Ratio: ").append(lwp.getObservedRatio()).append(" | ");
                    penalties.append("RatioPen: ").append(lwp.getRatioPenalty()).append(" | ");
                    penalties.append("MaxParts: ").append(parameter).append(" | ");
                    penalties.append("PartsPen: ").append(lwp.getPartsSizePenalty()).append(" | ");
                    penalties.append("Fitness: ").append(lwp.getFinalFitness()).append("\n");
                }

                if(se.getSimulator() instanceof DynamicTimeCopasiSimulator) {
                    DynamicTimeCopasiSimulator dtcs = (DynamicTimeCopasiSimulator) se.getSimulator();
                    for(ArrayList<Double> al:dtcs.getDependentMetaboliteValues()){
                        responseCurve.add(al.get(al.size()-1));
                    }
                }
            }

            Exporter<String> exporter = new SVPWriteExporter();
            Selection selection = e.getSelectionStrategy();

            if(selection instanceof AbstractMutantChamp)
            {
                EvaluatedChromosome<GRNTreeChromosome> bestModel = ((AbstractMutantChamp) selection).getBestChromosome();
                String model = "";

                if(bestModel!=null) {
                    model =  bestModel.getFitness().getFitness() > evaluatedChromosome.getFitness().getFitness() ?
                            exporter.export((GRNTreeChromosome)bestModel.getChromosome()) :
                            exporter.export((GRNTreeChromosome)evaluatedChromosome.getChromosome());
                }
                else {
                    model = exporter.export((GRNTreeChromosome)evaluatedChromosome.getChromosome());
                }

                if (!Export.exportString(null,
                        "Results",
                        experimentName,
                        experimentName + "_" + timestamp,
                        "BestModels_" + timestamp + ".txt",
                        null,
                        "BestModel_" + e.getPopulationStats().getCurrentGeneration() + " " + model + "\n",
                        true)) {  System.out.println("Cannot export best chromosome");}
            }


            if(penalties!=null && !penalties.toString().equals("")) {

                if (!Export.exportString(     null,
                                               "Results",
                                               experimentName,
                                               experimentName + "_" + timestamp,
                                               "FitnessFunctionData_" + timestamp + ".txt",
                                               null,
                                               penalties.toString(),
                                               true)) {  System.out.println("Cannot export all fitness function data");}
            }


            if (!Export.exportString(  null,
                                       "Results",
                                       experimentName,
                                       experimentName + "_" + timestamp,
                                       "Operators_" + timestamp + ".txt",
                                       null,
                                       "Operator_" + e.getPopulationStats().getCurrentGeneration() +" " + operator + "\n",
                                           true)) {  System.out.println("Cannot Export Operator");}

            /*
             * Write SVPWrite
             */




            if (!Export.exportString(  null,
                                       "Results",
                                       experimentName,
                                       experimentName + "_" + timestamp,
                                       "Models_" + timestamp + ".txt",
                                       null,
                                       "Model_" + e.getPopulationStats().getCurrentGeneration() +" " + exporter.export(t) + "\n",
                                       true)) {  System.out.println("Cannot Export Model Description");}



            /*
             * Write Fitnesses
             */


            if(fitness != null && !fitness.isEmpty()) {
                if (!Export.exportDoubles(     null,
                                               "Results",
                                               experimentName,
                                               experimentName + "_" + timestamp,
                                               "AllFitnesses_" + timestamp + ".txt",
                                               fitness,
                                               true)) {  System.out.println("Cannot export all fitness results");}
            }
            else {
                if (!Export.exportString(  null,
                                           "Results",
                                           experimentName,
                                           experimentName + "_" + timestamp,
                                           "Operators_" + timestamp + ".txt",
                                           null,
                                               "!ERROR - Fitness is empty\n",
                                       true)) {  System.out.println("Cannot Export Model Description");}
            }
            /*
             * Write Response Curve
             */


            if(responseCurve!= null && !responseCurve.isEmpty()){
                if (!Export.exportDoubles( null,
                                               "Results",
                                               experimentName,
                                               experimentName + "_" + timestamp,
                                               "ResponseCurves_" + timestamp + ".txt",
                                               responseCurve,
                                               true)) {  System.out.println("Cannot export all acentotic values");}
            }
            else {

                ArrayList<Double> empty = new ArrayList<Double>();
                for(int i=0; i<50; i++){
                    empty.add(0.0);
                }

                if (!Export.exportDoubles( null,
                                               "Results",
                                               experimentName,
                                               experimentName + "_" + timestamp,
                                               "ResponseCurves_" + timestamp + ".txt",
                                               empty,
                                               true)) {  System.out.println("Cannot export empty acentotic values");}
            }




            if(e.getTerminationCondition().shouldTerminate(e.getPopulationStats())){
                if (!Export.exportString(  null,
                                           "Results",
                                           experimentName,
                                           experimentName + "_" + timestamp,
                                           "Termination_" + timestamp + ".txt",
                                           null,
                                           "Successfully completed at generation " + e.getPopulationStats().getCurrentGeneration(),
                                           true)) {  System.out.println("Cannot Export Termination");}
            }

        
    }
}
