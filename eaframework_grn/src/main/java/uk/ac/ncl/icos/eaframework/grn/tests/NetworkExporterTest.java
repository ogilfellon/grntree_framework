package uk.ac.ncl.icos.eaframework.grn.tests;

import org.jgrapht.DirectedGraph;
import org.jgrapht.ext.GmlExporter;
import uk.ac.ncl.icos.grntree.api.GRNTree;
import uk.ac.ncl.icos.grntree.api.GRNTreeFactory;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import uk.ac.ncl.icos.eaframework.grn.Exporter;
import uk.ac.ncl.icos.eaframework.grn.GRNTreeChromosome;
import uk.ac.ncl.icos.eaframework.grn.exporter.GeneNetworkExporter;

/**
 * Created by owengilfellon on 02/06/2014.
 */
public class NetworkExporterTest {
    

    public static void main(String[] args) {

        File homeDir = new File(System.getProperty("user.home"));
        File file = new File(homeDir + "/Desktop/LessGreedyTest_140606_165210_Gen 33.gml");
        if(!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        //String model = "PspaS:Prom; RBS_SpaS:RBS; SpaR:CDS; RBS_SpaR:RBS; SpaK:CDS; BO_27957:RBS; BO_31762:CDS; BO_28152:RBS; BO_32227:CDS; BO_4296:Ter; PspaS:Prom; BO_27957:RBS; BO_31762:CDS; BO_5297:Ter; BO_3477:Prom; BO_28485:RBS; BO_28892:CDS; BO_5972:Ter; BO_27657:Prom; BO_28485:RBS; BO_28892:CDS; BO_28207:RBS; BO_31152:CDS; BO_28399:RBS; BO_28831:CDS; BO_5169:Ter; BO_3477:Prom; BO_28485:RBS; BO_28892:CDS; BO_27901:RBS; BO_30695:CDS; BO_27901:RBS; BO_30695:CDS; BO_5972:Ter; BO_2914:Prom; BO_3963:Op; BO_28411:RBS; BO_31152:CDS; BO_28399:RBS; SpaK:CDS; BO_28181:RBS; BO_32227:CDS; BO_4426:Ter; BO_2872:Prom; BO_27901:RBS; BO_30695:CDS; BO_5559:Ter; BO_2709:Prom; BO_3781:Op; BO_3692:Op; BO_28372:RBS; SpaR:CDS; BO_27981:RBS; BO_30682:CDS; BO_27901:RBS; BO_30695:CDS; BO_27901:RBS; BO_30695:CDS; BO_27901:RBS; BO_30695:CDS; BO_4875:Ter; BO_3388:Prom; RBS_SpaK:RBS; GFP_rrnb:CDS; BO_4296:Ter; BO_27738:Prom; BO_28207:RBS; BO_31152:CDS; BO_5972:Ter; BO_27657:Prom; BO_28485:RBS; BO_28892:CDS; BO_27930:RBS; BO_31152:CDS; BO_28399:RBS; BO_28831:CDS; BO_5169:Ter\n";
        //String model = "BO_2917:Prom; BO_28510:RBS; SpaK:CDS; BO_27993:RBS; BO_32147:CDS; BO_27866:RBS; BO_28892:CDS; BO_27893:RBS; BO_32743:CDS; BO_4973:Ter; PspaS:Prom; BO_4227:Op; BO_3906:Op; BO_28326:RBS; BO_32307:CDS; BO_5427:Ter; BO_2939:Prom; BO_3583:Op; BO_28333:RBS; SpaR:CDS; BO_4775:Ter; PspaS:Prom; BO_3789:Op; BO_28475:RBS; SpaK:CDS; BO_28017:RBS; BO_32147:CDS; BO_5498:Ter; BO_3143:Prom; BO_28493:RBS; BO_28950:CDS; BO_28224:RBS; BO_32997:CDS; BO_6476:Ter; BO_27718:Prom; BO_27910:RBS; BO_31762:CDS; BO_28275:RBS; BO_31001:CDS; BO_4813:Ter; BO_3403:Prom; BO_28182:RBS; BO_32601:CDS; BO_28300:RBS; BO_32743:CDS; BO_28486:RBS; BO_32601:CDS; BO_28234:RBS; BO_32227:CDS; BO_28270:RBS; BO_30746:CDS; BO_5738:Ter; PspaS:Prom; BO_3600:Op; BO_3906:Op; BO_28092:RBS; BO_32307:CDS; BO_28142:RBS; BO_32307:CDS; BO_28096:RBS; BO_28950:CDS; BO_5427:Ter; BO_27782:Prom; BO_28152:RBS; BO_32633:CDS; BO_28298:RBS; SpaK:CDS; BO_28287:RBS; SpaR:CDS; BO_28249:RBS; BO_32653:CDS; BO_6472:Ter; BO_27780:Prom; BO_3596:Op; BO_27809:RBS; BO_28831:CDS; BO_28322:RBS; SpaR:CDS; BO_5139:Ter; PspaS:Prom; BO_3906:Op; BO_28326:RBS; BO_32307:CDS; BO_28319:RBS; BO_31337:CDS; BO_27809:RBS; BO_28831:CDS; BO_27910:RBS; BO_31762:CDS; BO_5427:Ter; PspaS:Prom; BO_28152:RBS; BO_28950:CDS; BO_6420:Ter; PspaS:Prom; BO_3906:Op; BO_27783:RBS; BO_32633:CDS; BO_28096:RBS; BO_32997:CDS; BO_4583:Ter; PspaS:Prom; BO_3559:Op; BO_3906:Op; BO_28326:RBS; BO_32307:CDS; BO_28144:RBS; BO_32307:CDS; BO_28326:RBS; BO_32307:CDS; BO_28157:RBS; BO_28950:CDS; BO_28182:RBS; BO_32601:CDS; BO_5427:Ter; BO_3388:Prom; RBS_SpaK:RBS; GFP_rrnb:CDS; BO_4296:Ter; PspaS:Prom; BO_3906:Op; BO_28326:RBS; BO_32307:CDS; BO_27910:RBS; BO_31762:CDS; BO_5427:Ter; PspaS:Prom; BO_28326:RBS; BO_32307:CDS; BO_28082:RBS; BO_32307:CDS; BO_28096:RBS; BO_28950:CDS; BO_5427:Ter; PspaS:Prom; BO_3906:Op; BO_28326:RBS; BO_32307:CDS; BO_28319:RBS; BO_31337:CDS; BO_27900:RBS; BO_32307:CDS; BO_5427:Ter; PspaS:Prom; BO_28152:RBS; BO_28950:CDS; BO_28185:RBS; BO_28950:CDS; BO_6420:Ter; PspaS:Prom; BO_4190:Op; BO_3600:Op; BO_3906:Op; BO_28326:RBS; BO_32307:CDS; BO_28326:RBS; BO_32307:CDS; BO_27903:RBS; BO_32307:CDS; BO_28096:RBS; BO_28950:CDS; BO_28051:RBS; BO_32731:CDS; BO_5427:Ter; PspaS:Prom; BO_3906:Op; BO_28134:RBS; BO_32307:CDS; BO_28326:RBS; BO_28892:CDS; BO_5427:Ter; BO_27782:Prom; BO_3711:Op; BO_28326:RBS; BO_32307:CDS; BO_5427:Ter; PspaS:Prom; RBS_SpaK:RBS; GFP_rrnb:CDS; BO_4296:Ter; PspaS:Prom; RBS_SpaK:RBS; GFP_rrnb:CDS; BO_4296:Ter; PspaS:Prom; RBS_SpaK:RBS; GFP_rrnb:CDS; BO_4296:Ter; PspaS:Prom; RBS_SpaK:RBS; GFP_rrnb:CDS; BO_4296:Ter; PspaS:Prom; BO_3906:Op; BO_28326:RBS; BO_32307:CDS; BO_28319:RBS; BO_31337:CDS; BO_27900:RBS; BO_32307:CDS; BO_5427:Ter";
        //String model = "PspaS:Prom; RBS_SpaR:RBS; SpaR:CDS; BO_28140:RBS; SpaK:CDS; BO_27925:RBS; BO_31152:CDS; BO_4296:Ter; BO_27654:Prom; BO_4062:Op; RBS_SpaS:RBS; GFP_rrnb:CDS; BO_4296:Ter; BO_3017:Prom; BO_27814:RBS; SpaR:CDS; BO_28246:RBS; BO_32147:CDS; BO_27875:RBS; BO_32147:CDS; BO_28522:RBS; BO_28831:CDS; BO_28458:RBS; BO_28831:CDS; BO_28458:RBS; BO_28831:CDS; BO_5248:Ter; BO_3475:Prom; BO_28458:RBS; BO_28831:CDS; BO_6486:Ter; BO_3403:Prom; BO_27793:RBS; SpaK:CDS; BO_5388:Ter; BO_3403:Prom; BO_28140:RBS; BO_32147:CDS; BO_5418:Ter";

        String model = "BO_2914:Prom; BO_3963:Op; BO_28411:RBS; BO_31152:CDS; BO_28399:RBS; SpaK:CDS; BO_4426:Ter; BO_2709:Prom; BO_3781:Op; BO_3692:Op; BO_28372:RBS; SpaR:CDS; BO_27901:RBS; BO_30695:CDS; BO_27901:RBS; BO_30695:CDS; BO_27901:RBS; BO_30695:CDS; BO_4875:Ter; BO_3388:Prom; RBS_SpaK:RBS; GFP_rrnb:CDS; BO_4296:Ter; PspaS:Prom; RBS_SpaS:RBS; SpaR:CDS; RBS_SpaR:RBS; SpaK:CDS; BO_27957:RBS; BO_31762:CDS; BO_4296:Ter";
        GRNTreeChromosome tree = new GRNTreeChromosome(GRNTreeFactory.getGRNTree(model));
        Exporter<DirectedGraph> exporter = new GeneNetworkExporter();
        DirectedGraph network = exporter.export(tree);

        try {
            Writer fileWriter = new FileWriter(file);
            GmlExporter graphMLExporter = new GmlExporter();
            graphMLExporter.setPrintLabels(GmlExporter.PRINT_EDGE_VERTEX_LABELS);
            graphMLExporter.export(fileWriter, network);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("!");
    }


}
