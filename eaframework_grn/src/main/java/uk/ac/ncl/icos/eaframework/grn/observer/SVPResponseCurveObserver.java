package uk.ac.ncl.icos.eaframework.grn.observer;

import uk.ac.ncl.icos.datatypes.ResponseCurve;
import uk.ac.ncl.icos.eaframework.chromosome.Chromosome;
import uk.ac.ncl.icos.eaframework.evoengines.EvoEngine;
import uk.ac.ncl.icos.eaframework.grn.Exporter;
import uk.ac.ncl.icos.eaframework.grn.datatype.SVPStats;
import uk.ac.ncl.icos.eaframework.grn.exporter.SVPWriteExporter;
import uk.ac.ncl.icos.eaframework.observer.EvolutionObserver;
import uk.ac.ncl.icos.eaframework.observer.EvolutionSubject;
import uk.ac.ncl.icos.eaframework.stats.PopulationStats;

import java.io.Serializable;
import java.util.ArrayList;
import uk.ac.ncl.icos.eaframework.grn.GRNTreeChromosome;

/**
 *
 * @author owengilfellon
 */
public class SVPResponseCurveObserver implements EvolutionObserver<Chromosome>, Serializable {

    ArrayList<String> metabolites = new ArrayList<String>();
    
    public SVPResponseCurveObserver(ArrayList<String> metabolitesToRecord) {
        this.metabolites = metabolitesToRecord;
    }

    @Override
    public void update(EvolutionSubject<Chromosome> s) {
       
            EvoEngine e = (EvoEngine) s;
            PopulationStats ps = e.getPopulationStats();            
            StringBuilder sb = new StringBuilder();
            
            sb.append("===============================================================\n");
            sb.append("Gen: ").append(ps.getCurrentGeneration()).append(" | ");
            sb.append("Pop: ").append(ps.getPopulationSize()).append(" | ");
            sb.append("Best Fit: ").append(ps.getBestFitness()).append(" | ");
            sb.append("Mean Fit: ").append(ps.getMeanFitness()).append("\n");
            Exporter<String> exporter = new SVPWriteExporter();
            Chromosome c = ps.getBestChromosome();
            if(c instanceof GRNTreeChromosome)
            {
                GRNTreeChromosome tree = (GRNTreeChromosome) c;
                sb.append(exporter.export(tree)).append("\n");
            }


            
            if(ps instanceof SVPStats)
            {
                SVPStats ss = (SVPStats) ps;
                ResponseCurve rc = ss.getResponseCurve();
                
                sb.append("RC: ");
                
                for(ArrayList<Double> r:rc.getResults()){
                    sb.append(r.get(r.size()-1)).append(" ");
                }
            }
            
            sb.append("\n");
            
            System.out.println(sb.toString());            
    }
    
}
