package uk.ac.ncl.icos.eaframework.grn.operator;

import uk.ac.ncl.icos.annotation.EAModule;
import uk.ac.ncl.icos.eaframework.operator.AbstractOperator;
import uk.ac.ncl.icos.svpmanager.SVPManager;
import uk.ac.ncl.icos.grntree.api.GRNTree;
import uk.ac.ncl.icos.grntree.api.GRNTreeNode;
import uk.ac.ncl.icos.grntree.impl.*;
import uk.ac.ncl.intbio.virtualparts.entity.Part;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import uk.ac.ncl.icos.eaframework.grn.GRNTreeChromosome;
import uk.ac.ncl.icos.grntree.api.GRNTreeNodeFactory;

/**
 * Randomly selects a regulated Transcriptional Unit, and removes a promoter,
 * if there is more than one in the transcription unit
 * 
 * @author owengilfellon
 */
public class RemovePromoter extends AbstractOperator<GRNTreeChromosome> {

    private final SVPManager m = SVPManager.getSVPManager();
    private final int        MAX_ATTEMPTS = 10;

    @Override
    public GRNTreeChromosome apply(GRNTreeChromosome c) {

        GRNTreeChromosome t = null;

        boolean mutationOccured = false;
        int attempt = 0;

        while ( !mutationOccured && attempt < MAX_ATTEMPTS )
        {
            t = (GRNTreeChromosome) c.duplicate();
            List<GRNTreeNode> allTranscriptionUnits = new ArrayList<GRNTreeNode>();
            Iterator<GRNTreeNode> i = t.getBreadthFirstIterator();

            while (i.hasNext()) {
                GRNTreeNode node = i.next();
                if ( node.isTranscriptionUnit() ) {
                    allTranscriptionUnits.add(node);
                }
            }

            if ( allTranscriptionUnits.isEmpty() ) {

                // =================================================================
                // There are no regulated TUs in the model, so return original model
                // =================================================================

                return c;
            }

            Random r = new Random();
            GRNTreeNode chosenTU = allTranscriptionUnits.get( r.nextInt ( allTranscriptionUnits.size() ) );
            List<GRNTreeNode> promoters = new ArrayList<GRNTreeNode>();

            for (GRNTreeNode n : chosenTU.getChildren()) {
                if (n instanceof LeafNode) {
                    LeafNode leafNode = (LeafNode) n;

                    // =================================================================
                    // Regulated part can be either Promoter, or Operator. Due to
                    // mathematical formulation of SVPs, positive regulation should be
                    // added using promoter, negative by operator.
                    // =================================================================

                    if (leafNode.getType() == SVPType.Prom) {
                        promoters.add(n);
                    }
                }
            }

            // If only one promoter, no action is performed this generation...

            if(promoters.size() > 1) {
                LeafNode promoter = (LeafNode) promoters.get(r.nextInt(promoters.size()));
                List<LeafNode> operators = new ArrayList<LeafNode>();
                int increment = 1;
                int promoterIndex = promoter.getParent().getChildren().indexOf(promoter);

                // Add all operators to a list

                while(((LeafNode)promoter.getParent()
                        .getChildren()
                        .get(promoterIndex + increment))
                        .getType().equals(SVPType.Op)) {

                    operators.add((LeafNode) promoter.getParent()
                            .getChildren()
                            .get(promoterIndex + increment++));
                }

                // ===========================================================
                // Remove promoter that regulate the regulatory part
                // ===========================================================

                promoter.getParent().removeNode(promoter);

                // ===========================================================
                // Remove parts that regulate the promoter
                // ===========================================================

                ArrayList<Part> modifierParts =  m.getModifierParts(promoter.getSVP());

                if( modifierParts != null && !modifierParts.isEmpty() ) {
                    List<LeafNode> modifierPartsInModel = new ArrayList<LeafNode>();

                    for( Part part : modifierParts ) {

                        GRNTreeNode modifierPart = GRNTreeNodeFactory.getLeafNode(part, InterfaceType.OUTPUT);

                        if(t.containsLeafNode(modifierPart)) {
                            modifierPartsInModel.add((LeafNode)modifierPart);
                        }
                    }

                    if(!modifierPartsInModel.isEmpty()) {
                        LeafNode toRemove = modifierPartsInModel.get(r.nextInt(modifierPartsInModel.size()));
                        List<GRNTreeNode> parents = t.getParents(toRemove);
                        GRNTreeNode parent = parents.get(r.nextInt(parents.size()));
                        List<GRNTreeNode> children = parent.getChildren();

                        for(GRNTreeNode child : children) {
                            if(((LeafNode)child).getSVP().getName().equals(toRemove.getName())) {
                                parent.removeNode(child);
                            }
                        }
                    }
                }

                for(GRNTreeNode operator:operators) {
                    operator.getParent().removeNode(operator);

                    // ===========================================================
                    // Remove parts that regulate the operators
                    // ===========================================================

                    modifierParts =  m.getModifierParts(((LeafNode)operator).getSVP());

                    if( modifierParts != null && !modifierParts.isEmpty() ) {
                        List<LeafNode> modifierPartsInModel = new ArrayList<LeafNode>();

                        for( Part part : modifierParts ) {

                            GRNTreeNode modifierPart = GRNTreeNodeFactory.getLeafNode(part, InterfaceType.OUTPUT);

                            if(t.containsLeafNode(modifierPart)) {
                                modifierPartsInModel.add((LeafNode)modifierPart);
                            }
                        }

                        if(!modifierPartsInModel.isEmpty()) {
                            LeafNode toRemove = modifierPartsInModel.get(r.nextInt(modifierPartsInModel.size()));
                            List<GRNTreeNode> parents = t.getParents(toRemove);
                            GRNTreeNode parent = parents.get(r.nextInt(parents.size()));
                            List<GRNTreeNode> children = parent.getChildren();

                            for(GRNTreeNode child : children) {
                                if(((LeafNode)child).getSVP().getName().equals(toRemove.getName())) {
                                    parent.removeNode(child);
                                }
                            }
                        }
                    }

                }

                mutationOccured = true;
            }

            attempt++;

            if(!mutationOccured && attempt == MAX_ATTEMPTS) {
                return c;
            }
        }

        return t;
    }
}
