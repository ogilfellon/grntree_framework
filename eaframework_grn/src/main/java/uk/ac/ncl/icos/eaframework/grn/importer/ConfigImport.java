package uk.ac.ncl.icos.eaframework.grn.importer;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import uk.ac.ncl.icos.eaframework.chromosome.EvaluatedChromosome;
import uk.ac.ncl.icos.eaframework.fitness.Evaluator;
import uk.ac.ncl.icos.eaframework.grn.simulator.CopasiSimulator;
import uk.ac.ncl.icos.eaframework.grn.operator.OperatorGroup;
import uk.ac.ncl.icos.eaframework.grn.simulator.DynamicTimeCopasiSimulator;
import uk.ac.ncl.icos.eaframework.observer.EvolutionObserver;
import uk.ac.ncl.icos.eaframework.operator.Operator;
import uk.ac.ncl.icos.eaframework.scheduler.ParameterScheduler;
import uk.ac.ncl.icos.eaframework.selection.Selection;
import uk.ac.ncl.icos.eaframework.terminationconditions.TerminationCondition;
import uk.ac.ncl.icos.eaframework.terminationconditions.TerminationGroup;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import uk.ac.ncl.icos.eaframework.grn.GRNTreeChromosome;
import uk.ac.ncl.icos.grntree.api.GRNTreeFactory;

/**
 * Created by owengilfellon on 03/04/2014.
 */
public class ConfigImport {

    // TODO - Is there a better, less brittle way to implement this? e.g. Use serialization? Or include class names in XML?

    private final String CONFIGURATION_FILE;

    private final String prefix = "evo:";
    private final String EXPERIMENT = prefix + "ExperimentName";
    private final String POPULATION = prefix + "PopulationSize";
    private final String MUTATIONS = prefix + "MutationsPerGeneration";
    private final String SELECTION = prefix + "SelectionStrategy";
    private final String RESTART = prefix + "AutomaticRestart";
    private final String MODEL = prefix + "Model";
    private final String OPERATOR = prefix + "Operator";
    private final String FITFUNC = prefix + "FitnessFunction";
    private final String PARAMETER = prefix + "Parameter";
    private final String TERMINATION = prefix + "TerminationCondition";
    private final String OBSERVER = prefix + "Observer";
    private final String SIMULATOR = prefix + "Simulator";
        private final String SIMULATION_DURATION = prefix + "Duration";
        private final String SIMULATION_STEPS = prefix + "Steps";
        private final String SIMULATION_DEPENDENTVARIABLE = prefix + "DependentVariable";
        private final String SIMULATION_INDEPENDENTVARIABLE = prefix + "IndependentVariable";
        private final String SIMULATION_INCREMENTS = prefix + "Increments";
        private final String SIMULATION_RANGE = prefix + "Range";
    private final String TORECORD = prefix + "ToRecord";

    private List<EvolutionObserver> observers;

    public ConfigImport(String CONFIGURATION_FILE) {
        this.CONFIGURATION_FILE = CONFIGURATION_FILE;
    }

    public String getCONFIGURATION_FILE()
    {
        return CONFIGURATION_FILE;
    }

    public Configuration getConfiguration() {


        Configuration configuration = null;

        try {
            File config = new File(CONFIGURATION_FILE);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document document = dBuilder.parse(config);
            document.getDocumentElement().normalize();
            Element rootNode = document.getDocumentElement();
            NodeList children = rootNode.getChildNodes();



            String experimentName;
            int populationSize;
            int mutationsPerGeneration;
            boolean automaticRestart;
            List<GRNTreeChromosome> seeds = new ArrayList<GRNTreeChromosome>();
            Operator<GRNTreeChromosome> o;
            TerminationCondition t;
            CopasiSimulator cs;
            Selection<GRNTreeChromosome> s = null;
            Evaluator<GRNTreeChromosome> f = null;
            observers = new ArrayList<EvolutionObserver>();


            // Get experiment name

            NodeList nodes = rootNode.getElementsByTagName(EXPERIMENT);
            experimentName = nodes.item(0).getChildNodes().item(0).getNodeValue();

            // Get population size

            nodes = rootNode.getElementsByTagName(POPULATION);
            populationSize = Integer.parseInt(nodes.item(0).getChildNodes().item(0).getNodeValue());

            // Get number of mutations

            nodes = rootNode.getElementsByTagName(MUTATIONS);
            mutationsPerGeneration = Integer.parseInt(nodes.item(0).getChildNodes().item(0).getNodeValue());

            // Get selection strategy

            nodes = rootNode.getElementsByTagName(SELECTION);
            Class selectionclass = Class.forName("uk.ac.ncl.intbio.eaframework.selection." + nodes.item(0).getAttributes().getNamedItem("type").getNodeValue());

            NodeList parameters = nodes.item(0).getChildNodes();
            List<Object> args1 = getParameters(parameters);
            Constructor[] constructors1 = selectionclass.getConstructors();
            for(int i=0; i<constructors1.length; i++) {
                if(constructors1[i].getGenericParameterTypes().length == args1.size())
                {
                    s = (Selection<GRNTreeChromosome>) constructors1[i].newInstance(args1.toArray());
                }
            }

            // Get automatic restart

            nodes = rootNode.getElementsByTagName(RESTART);
            automaticRestart = nodes.item(0).getChildNodes().item(0).getNodeValue().equals("true") ? true : false;

            // Get starting models


            nodes = rootNode.getElementsByTagName(MODEL);
            for(int i=0; i<nodes.getLength(); i++)
            {
                String svpWrite = nodes.item(i).getChildNodes().item(0).getNodeValue();
                seeds.add(new GRNTreeChromosome(GRNTreeFactory.getGRNTree(svpWrite)));
            }

            // Get operators

            nodes = rootNode.getElementsByTagName(OPERATOR);
            List<Operator<GRNTreeChromosome>> operators = new ArrayList<Operator<GRNTreeChromosome>>();

            for(int i=0; i<nodes.getLength(); i++) {
                selectionclass = Class.forName("uk.ac.ncl.intbio.eaframework.grn.operator." + nodes.item(i).getChildNodes().item(0).getNodeValue());
                Constructor constructor = selectionclass.getConstructor();
                operators.add((Operator<GRNTreeChromosome>) constructor.newInstance());
            }

            o = new OperatorGroup(operators, mutationsPerGeneration);


            // Get termination conditions

            nodes = rootNode.getElementsByTagName(TERMINATION);
            List<TerminationCondition> terminations = new ArrayList<TerminationCondition>();

            for(int i=0; i<nodes.getLength(); i++) {
                selectionclass = Class.forName("uk.ac.ncl.intbio.eaframework.terminationconditions." + nodes.item(i).getAttributes().getNamedItem("type").getNodeValue());
                Constructor[] constructors = selectionclass.getConstructors();

                // TerminationCondition contructor has parameters

                if(nodes.item(i).hasChildNodes()) {
                    String value = nodes.item(i).getChildNodes().item(0).getNodeValue();
                    Class<?>[] types = constructors[0].getParameterTypes();
                    Object parameter = this.stringToType(types[0], value);
                    terminations.add((TerminationCondition) constructors[0].newInstance(parameter));
                }

                // TerminationCondition constructor has no parameters

                else {
                    terminations.add((TerminationCondition) constructors[0].newInstance());
                }

            }

            t = new TerminationGroup(terminations);

            // Get observers

            nodes = rootNode.getElementsByTagName(OBSERVER);

            for(int i=0; i<nodes.getLength(); i++) {
                selectionclass = Class.forName("uk.ac.ncl.intbio.eaframework.grn.observer." + nodes.item(i).getAttributes().getNamedItem("type").getNodeValue());
                Constructor[] constructors = selectionclass.getConstructors();
                Class<?>[] types = constructors[0].getParameterTypes();

                // Observer constructor has parameters

                if(types.length > 0) {
                    ArrayList<String> toRecord = new ArrayList<String>();
                    NodeList nodes2 = rootNode.getElementsByTagName(TORECORD);
                    for(int j = 0; j<nodes2.getLength(); j++) {
                        toRecord.add(nodes2.item(j).getChildNodes().item(0).getNodeValue());
                    }

                    if(selectionclass.getCanonicalName().equals("SVPTreeResultsFileWriter")) {
                        observers.add((EvolutionObserver<GRNTreeChromosome>) constructors[0].newInstance(experimentName, toRecord));
                    } else {
                        observers.add((EvolutionObserver<GRNTreeChromosome>) constructors[0].newInstance(toRecord));
                    }
                }

                // Observer constructor has no parameters

                else {
                    observers.add((EvolutionObserver<GRNTreeChromosome>) constructors[0].newInstance());
                }
            }

            // Get Simulator

            nodes = rootNode.getElementsByTagName(SIMULATOR);
            selectionclass = Class.forName("uk.ac.ncl.intbio.eaframework.grn.simulator." + nodes.item(0).getAttributes().getNamedItem("type").getNodeValue());

            int duration = Integer.parseInt(rootNode.getElementsByTagName(SIMULATION_DURATION).item(0).getChildNodes().item(0).getNodeValue());
            int steps = Integer.parseInt(rootNode.getElementsByTagName(SIMULATION_STEPS).item(0).getChildNodes().item(0).getNodeValue());

            NodeList dv = rootNode.getElementsByTagName(SIMULATION_DEPENDENTVARIABLE);
            NodeList iv = rootNode.getElementsByTagName(SIMULATION_INDEPENDENTVARIABLE);
            NodeList inc = rootNode.getElementsByTagName(SIMULATION_INCREMENTS);
            NodeList ran = rootNode.getElementsByTagName(SIMULATION_RANGE);
            if(     dv.getLength() > 0 &&
                    iv.getLength() > 0 &&
                    inc.getLength() > 0 &&
                    ran.getLength() > 0) {

                String dependentVariable = dv.item(0).getChildNodes().item(0).getNodeValue();
                String independentVariable = iv.item(0).getChildNodes().item(0).getNodeValue();
                int increments = Integer.parseInt(inc.item(0).getChildNodes().item(0).getNodeValue());
                int range = Integer.parseInt(ran.item(0).getChildNodes().item(0).getNodeValue());

                Constructor[] constructors = selectionclass.getConstructors();
                Object[] args = new Object[2];
                args[0] = duration;
                args[1] = steps;

                cs = (CopasiSimulator) constructors[0].newInstance(args);


                if (cs instanceof CopasiSimulator) {


                    ((DynamicTimeCopasiSimulator) cs).setMetabolitePair(independentVariable, dependentVariable);
                    ((DynamicTimeCopasiSimulator)  cs).setMaximumMetaboliteConcentration(range);
                    ((DynamicTimeCopasiSimulator)  cs).setNumberOfMetaboliteIncrements(increments);
                }

            }
            else
            {
                Constructor[] constructors = selectionclass.getConstructors();
                Object[] args = new Object[2];
                args[0] = duration;
                args[1] = steps;
                cs = (CopasiSimulator) constructors[0].newInstance(args);
            }

            // Get Fitness Function

            nodes = rootNode.getElementsByTagName(FITFUNC);
            selectionclass = Class.forName("uk.ac.ncl.intbio.eaframework.grn.fitness." + nodes.item(0).getAttributes().getNamedItem("type").getNodeValue());
            parameters = nodes.item(0).getChildNodes();
            List<Object> args = getParameters(parameters);
            args.add(0, cs);
            Constructor[] constructors = selectionclass.getConstructors();
            f = (Evaluator<GRNTreeChromosome>) constructors[0].newInstance(args.toArray());

            // Return configuration

            return new Configuration(   seeds,
                                        o,
                                        t,
                                        cs,
                                        s,
                                        f,
                                        observers,
                                        populationSize,
                                        mutationsPerGeneration,
                                        automaticRestart,
                                        experimentName,
                                        document,
                                        CONFIGURATION_FILE);


        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        return configuration;
    }

    private List<Object> getParameters(NodeList parameters)
    {
        List<Object> args = new ArrayList<Object>();

        for(int i=0; i<parameters.getLength(); i++) {

            NodeList child = parameters.item(i).getChildNodes();
            for(int j=0; j<child.getLength(); j++)
            {
                if(child.item(j).getNodeName().equals(prefix + "Value")) {
                    double parameter = Double.parseDouble(child.item(j).getChildNodes().item(0).getNodeValue());
                    args.add(parameter);
                }
                else if (child.item(j).getNodeName().equals(prefix + "Scheduler")) {

                    NodeList schedulerParameters = child.item(j).getChildNodes();
                    int startGeneration = 0;
                    int endGeneration = 0;
                    double startValue = 0 ;
                    double endValue = 0;

                    for(int k=0; k<schedulerParameters.getLength(); k++)
                    {

                        if(schedulerParameters.item(k).getNodeName().equals(prefix + "StartGeneration")) {
                            startGeneration = Integer.parseInt(schedulerParameters.item(k).getChildNodes().item(0).getNodeValue());
                        }
                        else if (schedulerParameters.item(k).getNodeName().equals(prefix + "StartValue")) {
                            startValue = Double.parseDouble(schedulerParameters.item(k).getChildNodes().item(0).getNodeValue());
                        }
                        else if (schedulerParameters.item(k).getNodeName().equals(prefix + "EndGeneration")) {
                            endGeneration = Integer.parseInt(schedulerParameters.item(k).getChildNodes().item(0).getNodeValue());
                        }
                        else if (schedulerParameters.item(k).getNodeName().equals(prefix + "EndValue")) {
                            endValue = Double.parseDouble(schedulerParameters.item(k).getChildNodes().item(0).getNodeValue());
                        }

                    }

                    args.add(new ParameterScheduler(startGeneration, endGeneration, startValue, endValue));
                    observers.add((EvolutionObserver<GRNTreeChromosome>) args.get(args.size()-1));
                }
            }
        }

        return args;
    }

    private Object stringToType(Class<?> c, String value)
    {

        Object type = null;

        String expectedType = c.getCanonicalName();
        if (expectedType.equals("int")) {
            type = Integer.parseInt(value);
        }
        else if (expectedType.equals("double")) {
            type = Double.parseDouble(value);
        }

        return type;
    }
}
