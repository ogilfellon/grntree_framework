package uk.ac.ncl.icos.eaframework.grn.exporter;

import org.jgrapht.DirectedGraph;
import org.jgrapht.graph.DefaultDirectedGraph;
import uk.ac.ncl.icos.eaframework.grn.Exporter;
import uk.ac.ncl.icos.svpmanager.SVPManager;
import uk.ac.ncl.icos.grntree.api.GRNTree;
import uk.ac.ncl.icos.grntree.api.GRNTreeNode;
import uk.ac.ncl.icos.grntree.impl.BranchNode;
import uk.ac.ncl.icos.grntree.impl.InteractionType;
import uk.ac.ncl.icos.grntree.impl.LeafNode;
import uk.ac.ncl.icos.grntree.impl.SVPType;
import uk.ac.ncl.intbio.virtualparts.entity.Interaction;

import java.util.*;
import uk.ac.ncl.icos.eaframework.grn.GRNTreeChromosome;

/**
 * Created by owengilfellon on 13/05/2014.
 */
public class GeneNetworkExporter implements Exporter<DirectedGraph> {

    private final static SVPManager m = SVPManager.getSVPManager();
    private Set<String> ids = new HashSet<String>();


    @Override
    public DirectedGraph<ExportNodeWrapper, ExportEdgeWrapper> export(GRNTreeChromosome tree) {

        List<GRNTreeNode> genes = new ArrayList<GRNTreeNode>();
        DirectedGraph<ExportNodeWrapper, ExportEdgeWrapper> network =
                new DefaultDirectedGraph<ExportNodeWrapper, ExportEdgeWrapper>(ExportEdgeWrapper.class);

        // ===================================================
        // Add nodes to graph
        // ===================================================

        Iterator<GRNTreeNode> i = tree.getPreOrderIterator();
        while(i.hasNext()) {
            GRNTreeNode node = i.next();
            if(node.isTranscriptionUnit()) {
                network.addVertex(new ExportNodeWrapper(node));
                genes.add(node);
            }
        }

        // ===================================================
        // Add edges to graph
        // ===================================================

        System.out.println(tree.debugInfo());

        for( GRNTreeNode firstTU : genes ) {
            for(GRNTreeNode child: firstTU.getChildren()) {
                if(child instanceof LeafNode) {
                    LeafNode regulator = (LeafNode) child;

                    // For each CDS in each Transcription Unit....

                    if(regulator.getType().equals(SVPType.CDS)) {
                        List<GRNTreeNode> regulatedByCds = tree.getInteractingParts(regulator);
                        for(GRNTreeNode regulated:regulatedByCds) {
                            for(GRNTreeNode secondTU: tree.getParents((LeafNode)regulated)) {
                                for(Interaction interaction:m.getInteractions(regulator.getSVP(), ((LeafNode)regulated).getSVP())) {
                                    InteractionType type = null;
                                    if(interaction.getInteractionType().startsWith("Transcriptional activation")) {
                                        type = InteractionType.ACTIVATION;
                                    } else if(interaction.getInteractionType().startsWith("Transcriptional repression")) {
                                        type = InteractionType.REPRESSION;
                                    }

                                    if(type!=null) {

                                        network.addEdge(new ExportNodeWrapper(firstTU), new ExportNodeWrapper(secondTU), new ExportEdgeWrapper(type));
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return network;
    }

    public class ExportEdgeWrapper{

        private InteractionType type;

        public ExportEdgeWrapper(InteractionType type) {
            this.type = type;
        }

        public String toString()
        {
            if(this.type == InteractionType.ACTIVATION) {
                return "+";
            } else {
                return "-";
            }
        }

    }

    public class ExportNodeWrapper {

        private GRNTreeNode node;

        public ExportNodeWrapper(GRNTreeNode node) {
            this.node = node;
        }

        public GRNTreeNode getNode()
        {
            return node;
        }

        public String toString()
        {
            StringBuilder sb = new StringBuilder();

            if(node.isTranscriptionUnit()) {
                for(GRNTreeNode child:node.getChildren()) {
                    LeafNode leafNode = (LeafNode) child;
                    if(leafNode.getType().equals(SVPType.CDS)) {

                        String id = null;

                        if(leafNode.getSVP().getDisplayName() != null) {
                            id = leafNode.getSVP().getDisplayName();
                            if(id.contains("||")) {
                                id = id.substring(0, id.indexOf("||"));
                            }
                        } else {
                            id = leafNode.getSVP().getName();
                        }

                        String prefix = sb.length()==0 ? "" : "_";
                        sb.append(prefix).append(id);
                    }
                }
            }

            int index = 1;
            String nodeId = sb.toString();

            if(!ids.add(nodeId)) {
                while (!ids.add(nodeId + index)) {
                    index++;
                }
                nodeId += index;
            }

            return nodeId;
        }

        public boolean equals(Object obj)
        {
            if(obj == null)
                return false;
            if(obj == this)
                return true;
            if(!(obj instanceof ExportNodeWrapper))
                return false;

            ExportNodeWrapper node = (ExportNodeWrapper) obj;
            if (node.getNode().equals(this.node))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        @Override
        public int hashCode() {
            return node.hashCode();
        }

    }
}



