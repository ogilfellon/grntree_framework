/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.grn.tests;

import uk.ac.ncl.icos.eaframework.grn.operator.SplitTU;
import uk.ac.ncl.icos.eaframework.operator.Operator;
import uk.ac.ncl.icos.svpmanager.SVPManager;
import uk.ac.ncl.icos.grntree.api.GRNTree;
import uk.ac.ncl.icos.grntree.api.GRNTreeFactory;
import uk.ac.ncl.icos.grntree.api.GRNTreeNode;
import uk.ac.ncl.icos.grntree.impl.LeafNode;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.ncl.icos.eaframework.grn.GRNTreeChromosome;

/**
 *
 * @author owengilfellon
 */
public class GRNTreeTest {
    
    public static void main1(String[] args)
    {
        
        SVPManager m = SVPManager.getSVPManager();
        
        /*
         * Construct a GRNTree from an SVPWrite String by using GRNTreeFactory.
         */
        
        GRNTreeChromosome tree = new GRNTreeChromosome(GRNTreeFactory.getGRNTree("PspaRK:Prom; RBS_SpaR:RBS; SpaR:CDS; RBS_SpaK:RBS; SpaK:CDS; BO_4296:Ter; PspaS:Prom; RBS_SpaS:RBS; GFP_rrnb:CDS; BO_4296:Ter"));
        
        List<Operator<GRNTreeChromosome>> operators = new ArrayList<Operator<GRNTreeChromosome>>();
   
        /*
        List<Operator<GRNTree>> topology = new ArrayList<Operator<GRNTree>>();
        List<Operator<GRNTree>> parameter = new ArrayList<Operator<GRNTree>>();
        
        parameter.add(new RandomiseConstPromoter());
        parameter.add(new RandomiseRBS());
        topology.add(new DuplicateTU());
        topology.add(new RemoveRegulation());
           
        operators.add(new SwapParts());
        operators.add(new OperatorGroup(topology));
        operators.add(new OperatorGroup(parameter));
        
        Operator<GRNTree> o = new OperatorGroup(operators, 10);
        */
        
        Operator<GRNTreeChromosome> o = new SplitTU();
        GRNTreeChromosome evolvedTree = o.apply(tree);

        System.out.println(evolvedTree.debugInfo());
        
        printDetails(evolvedTree.getPreOrderIterator());
        
        
        /*
         * Setting up testing model - upon calling increaseDepth() on the second TU,
         * the parts in the second TU should be isolated from those of the first.
         * By setting the interfaceType of the node that TU2 has been placed in  to INPUT,
         * OUTPUT nodes may have potential interactions with (Input) parts within that node.
         */

       // tree.getRootNode().setName("ROOT");       
        //tree.getRootNode().getNodes().get(0).setName("TU1");
        //tree.getRootNode().getNodes().get(0).setInterfaceType(InterfaceType.OUTPUT);       
       
        
        /**/
        
        try {
           // tree.getRootNode().increaseDepth();
           // tree.getRootNode().getNodes().get(0).getNodes().get(1).increaseDepth();
           //tree.getRootNode().getNodes().get(1).increaseDepth();
           //tree.getRootNode().removeNode(tree.getRootNode().getNodes().get(0));
            
          
            
          //  tree.getRootNode().getNodes().get(0).replaceNode(tree.getRootNode().getNodes().get(1).duplicate());
           /* tree.getRootNode().addNode(tree.getRootNode().getChildren().get(1).duplicate());
            tree.getRootNode().getChildren().get(1).getChildren().get(0).replaceNode(new LeafNode(m.getConstPromoter(), InterfaceType.INPUT));
            tree.getRootNode().getChildren().get(1).increaseDepth();
            tree.getRootNode().getChildren().get(1).getChildren().get(0).setInterfaceType(InterfaceType.INPUT);
            */
            
//            tree.getRootNode().addNode(new LeafNode(m.getConstPromoter(), InterfaceType.INPUT));
            
            /*
            Random r = new Random();
            
            Part prom = m.getNegOperator();
            List<Part> mods = m.getModifierParts(prom);
            Part cds = mods.get(r.nextInt(mods.size()));
            
            List<GRNTreeNode> tu3nodes = new ArrayList<GRNTreeNode>();
            tu3nodes.add(new LeafNode(m.getConstPromoter(), InterfaceType.INPUT));
            tu3nodes.add(new LeafNode(m.getRBS(), InterfaceType.NONE));
            tu3nodes.add(new LeafNode(cds, InterfaceType.OUTPUT));
            tu3nodes.add(new LeafNode(m.getTerminator(), InterfaceType.NONE));
            
            tree.getRootNode().addNode(new BranchNode(InterfaceType.NONE, tu3nodes));         
            tree.getRootNode().getChildren().get(0).addNode(1, new LeafNode(prom, InterfaceType.INPUT));
            */
            
            
        } catch (Exception ex) {
            Logger.getLogger(GRNTreeTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        /*
        System.out.println("=====Depth First (Preorder)======");
        
        Iterator<GRNTreeNode> it = tree.getPreOrderIterator();
        while(it.hasNext()) {
            
            StringBuilder sb = new StringBuilder();
            GRNTreeNode n = it.next();
            
            sb.append("Depth: (").append(n.getDepth()).append(") ");
            
            if(!n.getChildren().isEmpty()) {
                sb.append("Nodes: [").append(n.getChildren().size()).append("] ");
            }
            
            if(n.getParent() == null) {
                sb.append("ROOT ");
            }
            
            else if(n.isTranscriptionUnit()) {
                sb.append("TU ");
            }
            
            else if (!(n instanceof LeafNode)) {
                sb.append("MODULE ");
            }

            if(n instanceof LeafNode) {
                LeafNode ln = (LeafNode) n;
                sb.append(" - ").append(ln.getSVP().getName());
                try {
                    sb.append(" Bounds: [").append(n.getParent().getBounds(n).size()).append("] ");
                } catch (Exception ex) {
                    Logger.getLogger(GRNTreeTest.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            
            System.out.println(sb.toString());
        }
        
        System.out.println("=====Breadth First======");
        
        Iterator<GRNTreeNode> it2 = tree.getBreadthFirstIterator();
        
        
        */
        
        /* */ 
        // tree.getRootNode().getNodes().get(1).getNodes().get(0).setInterfaceType(InterfaceType.INPUT);
        //tree.getRootNode().getNodes().get(1).setName("TU2Wrapper");      
        //tree.getRootNode().getNodes().get(1).setInterfaceType(InterfaceType.INPUT);       
        //tree.getRootNode().getNodes().get(1).getNodes().get(0).setInterfaceType(InterfaceType.INPUT);
        //tree.getRootNode().getNodes().get(1).getNodes().get(0).setName("TU2");
 /**/
        
        /*
        System.out.println(tree.debugInfo());
        List<GRNTreeNode> tus = tree.getRootNode().getAllDescendantTUs();
        for(GRNTreeNode t:tus)
        {
            for(GRNTreeNode part:t.getInputNodes())
            {
                LeafNode ln = (LeafNode) part;
                System.out.println("INPUT BOUNDS: " + ln.getName());
                List<GRNTreeNode> bounds = tree.getBounds(t, part);
                for(GRNTreeNode n:bounds)
                {
                    System.out.println(n.getNodes().size());
                    for(Part p:n.getAllParts())
                    {
                        System.out.println(p.getName());
                    }
                }
            }
            for(GRNTreeNode part:t.getOutputNodes())
            {
                LeafNode ln = (LeafNode) part;
                System.out.println("OUTPUT BOUNDS: " + ln.getName());
                List<GRNTreeNode> bounds = tree.getBounds(t, part);
                for(GRNTreeNode n:bounds)
                {
                    System.out.println(n.getNodes().size());
                    for(Part p:n.getAllParts())
                    {
                        System.out.println(p.getName());
                    }
                }
            }
        }*/
        
        
            
        
              /* */
        /*
         * Testing Bounds Code - should return a list of nodes that have potential interactions
         * with the node upon which getBounds is called.
         * All nodes in the model not in getBounds must be orthogonal.
         */
        
        /*
        
        List<GRNTreeNode> bounds = tree.getRootNode().getNodes().get(0).getNodes().get(2).getBounds();
        ListIterator<GRNTreeNode> b_it = bounds.listIterator();
        StringBuilder sb = new StringBuilder();
        while(b_it.hasNext()){
            sb.append(b_it.next().getName()).append("\n");}
        System.out.println(sb.toString());
*/
        /*
        System.out.println("Root=======");
        for(GRNTreeNode n:tree.getRootNode().getInteractingNodes())
        {
            System.out.println(n);
        }
        System.out.println("TU1=======");
        for(GRNTreeNode n:tree.getRootNode().getNodes().get(0).getInteractingNodes())
        {
            System.out.println(n);
        }
        System.out.println("TU2Wrapper=======");
        for(GRNTreeNode n:tree.getRootNode().getNodes().get(1).getInteractingNodes())
        {
            System.out.println(n);
        }
        System.out.println("TU2=======");
        for(GRNTreeNode n:tree.getRootNode().getNodes().get(1).getNodes().get(0).getInteractingNodes())
        {
            System.out.println(n);
        }

* */
        /* ===== the below causes an index out of bounds (index:1 ) exception
         * 
            tree.getRootNode().increaseDepth();
            GRNTreeNode n = tree.getRootNode().getNodes().get(0).getNodes().get(0);
            n.increaseDepth();
        */
        
        /*
         * // -------- Testing nextInterfaceNodes in Navigator -----------
        GRNBreadthTreeNavigator nav = new GRNBreadthTreeNavigator(tree.getRootNode());
        StringBuilder sb = new StringBuilder();
        
        while(nav.hasNextInterfaceNode(InterfaceType.INPUT))
        {

            List<GRNTreeNode> inodes = nav.nextInterfaceNodes(InterfaceType.INPUT);
            ListIterator<GRNTreeNode> inodes_it = inodes.listIterator();
            sb.append("nextInterfaceNode(): [")
                        .append(inodes.size())
                        .append("] ");
            while(inodes_it.hasNext())
            {
                sb.append(inodes_it.next().debugInfo())
                        .append("\n");
            }          
        }
        System.out.println(sb.toString());
        * */
    }
    
    static void printDetails(Iterator<GRNTreeNode> it)
    {
        while(it.hasNext()) {

            StringBuilder sb = new StringBuilder();
            GRNTreeNode n = it.next();

            sb.append("Depth: (").append(n.getDepth()).append(") ");

            if(!n.getChildren().isEmpty()) {
                sb.append("Nodes: [").append(n.getChildren().size()).append("] ");
            }

            if(n.getParent() == null) {
                sb.append("ROOT ");
            }

            else if(n.isTranscriptionUnit()) {
                sb.append("TU ");
            }

            else if (!(n instanceof LeafNode)) {
                sb.append("MODULE ");
            }

            if(n instanceof LeafNode) {
                LeafNode ln = (LeafNode) n;
                sb.append(" - ").append(ln.getSVP().getName()).append(":").append(ln.getSVP().getType());
                try {
                    sb.append(" | Bounds: [").append(n.getParent().getBounds(n).size()).append("] ");
                } catch (Exception ex) {
                    Logger.getLogger(GRNTreeTest.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            System.out.println(sb.toString());
        }
    }
}
