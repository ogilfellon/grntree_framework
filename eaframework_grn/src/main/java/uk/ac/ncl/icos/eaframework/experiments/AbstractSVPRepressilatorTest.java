package uk.ac.ncl.icos.eaframework.experiments;


import org.sbml.jsbml.SBMLDocument;
import uk.ac.ncl.icos.svpmanager.AbstractSVPManager;
import uk.ac.ncl.icos.svpmanager.MolecularForm;
import uk.ac.ncl.icos.svpmanager.RegulationRole;
import uk.ac.ncl.intbio.virtualparts.ModelBuilder;
import uk.ac.ncl.intbio.virtualparts.PartsHandler;
import uk.ac.ncl.intbio.virtualparts.SBML.SBMLHandler;
import uk.ac.ncl.intbio.virtualparts.entity.Interaction;
import uk.ac.ncl.intbio.virtualparts.entity.Part;

import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author b1050029
 */

public class AbstractSVPRepressilatorTest {
    
    private PartsHandler p;
    private SBMLDocument        container;
    private SBMLHandler sbmlhandler;
    private String              server = "http://localhost:8080";
    private AbstractSVPManager m = AbstractSVPManager.getSVPManager();
    private ModelBuilder mb;
    
    public void run() throws Exception
    {
        p = new PartsHandler(server);
        container = new SBMLHandler().GetSBMLTemplateModel("SVP_Model");
        mb = new ModelBuilder(container);
        
        // ===============================================
        // Create Parts, providing a Part ID
        // ===============================================
        
        Part a = m.getConstPromoterPart("A");
        Part b = m.getNegativeOperator("B");
        Part c = m.getRBS("C");
        Part d = m.getCDS("D");

        Part e = m.getConstPromoterPart("E");
        Part f = m.getNegativeOperator("F");
        Part g = m.getRBS("G");
        Part h = m.getCDS("H");

        Part i = m.getConstPromoterPart("I");
        Part j = m.getNegativeOperator("J");
        Part k = m.getRBS("K");
        Part l = m.getCDS("L");
        
        // ===============================================
        // Create Internal Interactions, passing parts
        // and parameters, if needed.
        // ===============================================

        List<Interaction> a_i = m.getPoPSProduction(a, 0.0269);
        List<Interaction> e_i = m.getPoPSProduction(e, 0.03242);
        List<Interaction> i_i = m.getPoPSProduction(i, 0.02513);
        
        List<Interaction> b_i = m.getOperatorPoPSModulation(b);
        List<Interaction> f_i = m.getOperatorPoPSModulation(f);
        List<Interaction> j_i = m.getOperatorPoPSModulation(j);
        
        List<Interaction> c_i = m.getRiPSProduction(c, 0.34255);
        List<Interaction> g_i = m.getRiPSProduction(g, 0.51213);
        List<Interaction> k_i = m.getRiPSProduction(k, 0.4685);
        
        List<Interaction> d_i = m.getProteinProductionAndDegradation(d, 0.01412);
        List<Interaction> h_i = m.getProteinProductionAndDegradation(h, 0.02134);
        List<Interaction> l_i = m.getProteinProductionAndDegradation(l, 0.03214);
        
        // ===============================================
        // Create Part-Part Interactions
        // ===============================================

        Interaction dRepressesf = m.getRegulationInteraction(   f, d,
                                                                MolecularForm.DEFAULT,
                                                                RegulationRole.REPRESSOR,
                                                                0.3456);
        
        Interaction hRepressesj = m.getRegulationInteraction(   j, h,
                                                                MolecularForm.DEFAULT,
                                                                RegulationRole.REPRESSOR,
                                                                0.4567);
        
        Interaction lRepressesb = m.getRegulationInteraction(   b, l,
                                                                MolecularForm.DEFAULT,
                                                                RegulationRole.REPRESSOR,
                                                                0.3754);
      
        // =======================================================
        // Create SBML models from parts and internal interactions
        // =======================================================

        SBMLDocument a_doc = p.CreatePartModel(a, a_i);
        SBMLDocument b_doc = p.CreatePartModel(b, b_i);
        SBMLDocument c_doc = p.CreatePartModel(c, c_i);
        SBMLDocument d_doc = p.CreatePartModel(d, d_i);
        SBMLDocument e_doc = p.CreatePartModel(e, e_i);
        SBMLDocument f_doc = p.CreatePartModel(f, f_i);
        SBMLDocument g_doc = p.CreatePartModel(g, g_i);
        SBMLDocument h_doc = p.CreatePartModel(h, h_i);
        SBMLDocument i_doc = p.CreatePartModel(i, i_i);
        SBMLDocument j_doc = p.CreatePartModel(j, j_i);
        SBMLDocument k_doc = p.CreatePartModel(k, k_i);
        SBMLDocument l_doc = p.CreatePartModel(l, l_i);

        // =======================================================
        // Create Lists of Parts for each Interaction
        // =======================================================

        List<Part> dTofParts = new ArrayList<Part>();
        dTofParts.add(d);
        dTofParts.add(f);
        
        List<Part> hTojParts = new ArrayList<Part>();
        hTojParts.add(h);
        hTojParts.add(j);
        
        List<Part> lTobParts = new ArrayList<Part>();
        lTobParts.add(l);
        lTobParts.add(b);

        // =======================================================
        // Create SBML Models for Interactions
        // (JParts attempts to retrieve non-existent parts)
        // TODO Update JParts to use above lists of parts
        // =======================================================

        SBMLDocument dTof = p.CreateInteractionModel(dTofParts, dRepressesf);
        SBMLDocument hToj = p.CreateInteractionModel(hTojParts, hRepressesj);
        SBMLDocument lTob = p.CreateInteractionModel(lTobParts, lRepressesb);

/*
        SBMLDocument dTof = p.CreateInteractionModel(dRepressesf);
        SBMLDocument hToj = p.CreateInteractionModel(hRepressesj);
        SBMLDocument lTob = p.CreateInteractionModel(lRepressesb);
*/
        
        SBMLDocument mrna1 = p.GetModel(p.GetPart("mRNA"));
        SBMLDocument mrna2 = p.GetModel(p.GetPart("mRNA"));
        SBMLDocument mrna3 = p.GetModel(p.GetPart("mRNA"));
   /**/
        // =======================================================
        // Compose Part and Interaction Models into a
        // simulateable model
        // TODO update compiler to automate this process
        // TODO can abstract parts and concrete parts be mixed?
        // =======================================================

        mb.Add(a_doc);
        mb.Link(a_doc, b_doc);
        mb.Add(b_doc);
        mb.Link(b_doc, lTob);
        mb.Add(lTob);
        mb.Add(mrna1);
        mb.Link(lTob, mrna1);
        mb.Link(mrna1, c_doc);
        mb.Add(c_doc);
        mb.Link(c_doc, d_doc);
        mb.Add(d_doc);
        
        mb.Add(e_doc);
        mb.Link(e_doc, f_doc);
        mb.Add(f_doc);
        mb.Link(f_doc, dTof);
        mb.Add(dTof);
        mb.Add(mrna2);
        mb.Link(dTof, mrna2);
        mb.Link(mrna2, g_doc);
        mb.Add(g_doc);
        mb.Link(g_doc, h_doc);
        mb.Add(h_doc);
        
        mb.Add(i_doc);
        mb.Link(i_doc, j_doc);
        mb.Add(j_doc);
        mb.Link(j_doc, hToj);
        mb.Add(hToj);
        mb.Add(mrna3);
        mb.Link(hToj, mrna3);
        mb.Link(mrna3, k_doc);
        mb.Add(k_doc);
        mb.Link(k_doc, l_doc);
        mb.Add(l_doc);/**/

        // =======================================================
        // Write compiled model to file
        // =======================================================

        try
        {
           FileWriter writer = new FileWriter("output" + ".xml");
           writer.write(mb.GetModelString());
           writer.flush();
           // System.out.println("\nModel written to " + filename + ".xml");
        }
        catch(Exception ex)
        {
           System.out.println(ex.getMessage());
        }

    }
    
    public static void main(String[] args)
    {        
        try {
            new AbstractSVPRepressilatorTest().run();
        } catch (Exception ex) {
            Logger.getLogger(AbstractSVPRepressilatorTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
