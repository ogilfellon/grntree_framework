package uk.ac.ncl.icos.eaframework.grn.tests;

import org.sbolstandard.core.SBOLDocument;
import org.sbolstandard.core.SBOLFactory;
import org.sbolstandard.core.SBOLWriter;
import uk.ac.ncl.icos.eaframework.grn.Exporter;
import uk.ac.ncl.icos.eaframework.grn.exporter.SBOLExporter;
import uk.ac.ncl.icos.grntree.api.GRNTree;
import uk.ac.ncl.icos.grntree.api.GRNTreeFactory;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import uk.ac.ncl.icos.eaframework.grn.GRNTreeChromosome;

/**
 * Created by owengilfellon on 21/02/2014.
 */
public class SBOLExportTest {

    public static void main(String[] args)
    {

        String model = "PspaS:Prom; RBS_SpaR:RBS; SpaR:CDS; BO_28140:RBS; SpaK:CDS; BO_27925:RBS; BO_31152:CDS; BO_4296:Ter; " +
                "BO_27654:Prom; BO_4062:Op; RBS_SpaS:RBS; GFP_rrnb:CDS; BO_4296:Ter;" +
                "BO_3017:Prom; BO_27814:RBS; SpaR:CDS; BO_28246:RBS; BO_32147:CDS; BO_27875:RBS;" +
                "BO_32147:CDS; BO_28522:RBS; BO_28831:CDS; BO_28458:RBS; BO_28831:CDS; BO_28458:RBS;" +
                "BO_28831:CDS; BO_5248:Ter; BO_3475:Prom; BO_28458:RBS; BO_28831:CDS; BO_6486:Ter; BO_3403:Prom;" +
                "BO_27793:RBS; SpaK:CDS; BO_5388:Ter; BO_3403:Prom; BO_28140:RBS; BO_32147:CDS; BO_5418:Ter";

        GRNTreeChromosome tree = new GRNTreeChromosome(GRNTreeFactory.getGRNTree(model));

        // Set model name

        tree.getRootNode().setName("SubtilinReceiver");

        // Create Exporter, and set project

        Exporter<SBOLDocument> exporter = new SBOLExporter("owengilfellon/linearity");
        SBOLDocument document = exporter.export(tree);

        // Write to file

        try
        {
            SBOLWriter writer = SBOLFactory.createWriter();
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            FileOutputStream fos = new FileOutputStream("SBOLtest.xml");
            writer.write(document, os);
            fos.write(os.toByteArray());
            fos.flush();
            fos.close();
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }
}
