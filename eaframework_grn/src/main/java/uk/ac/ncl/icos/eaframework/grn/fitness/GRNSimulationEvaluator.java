/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.grn.fitness;

import uk.ac.ncl.icos.datatypes.Fitness;
import uk.ac.ncl.icos.eaframework.chromosome.Chromosome;
import uk.ac.ncl.icos.eaframework.fitness.Evaluator;
import uk.ac.ncl.icos.eaframework.grn.simulator.CopasiSimulator;

import java.io.Serializable;

/**
 * 
 * @author owengilfellon
 */

public abstract class GRNSimulationEvaluator<T extends Chromosome> implements Evaluator<T>, Serializable {

    protected final CopasiSimulator cs;

    GRNSimulationEvaluator(CopasiSimulator cs) {
        this.cs = cs;
    }
    
    public CopasiSimulator getSimulator() {
        return cs;
    }
    
    @Override
    abstract public Fitness evaluate(T c);
    
}
