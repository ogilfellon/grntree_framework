package uk.ac.ncl.icos.eaframework.grn;

import uk.ac.ncl.icos.eaframework.grn.simulator.CopasiSimulator;
import uk.ac.ncl.icos.datatypes.Fitness;
import uk.ac.ncl.icos.datatypes.ResponseCurve;
import uk.ac.ncl.icos.eaframework.chromosome.Chromosome;
import uk.ac.ncl.icos.eaframework.chromosome.EvaluatedChromosome;
import uk.ac.ncl.icos.eaframework.evoengines.AbstractEvoEngine;
import uk.ac.ncl.icos.eaframework.factories.ChromosomeFactory;
import uk.ac.ncl.icos.eaframework.fitness.Evaluator;
import uk.ac.ncl.icos.eaframework.grn.datatype.SVPStats;
import uk.ac.ncl.icos.eaframework.grn.fitness.GRNSimulationEvaluator;
import uk.ac.ncl.icos.eaframework.grn.simulator.DependentValueCopasiSimulator;
import uk.ac.ncl.icos.eaframework.operator.Operator;
import uk.ac.ncl.icos.eaframework.selection.Selection;
import uk.ac.ncl.icos.eaframework.stats.PopulationStats;
import uk.ac.ncl.icos.eaframework.terminationconditions.TerminationCondition;
import uk.ac.ncl.icos.grntree.api.GRNTree;

import java.util.List;

/**
 * Implementation of EvoEngine with functionality specific to evolving GRNs. Uses GRNTreeChromosome as a GRN representation.
 *
 * @see GRNTreeChromosome
 * @author owengilfellon
 */
public class GRNTreeEngine extends AbstractEvoEngine<GRNTreeChromosome> {

    /**
     * The Evolutionary Algorithm is instantiated with its necessary components
     *
     * @param cf a ChromosomeFactory for generating populations
     * @param o an Operator (or group of Operators) for applying mutations to the population
     * @param s a Selection operator for selecting parts based on fitness for reproduction
     * @param e a Fitness function for evaluating a solution's behaviour in comparison to some target behaviour
     * @param t a Termination Condition, or group of Termination Conditions, that determine when to exit the algorithm
     */
    public GRNTreeEngine(ChromosomeFactory<GRNTreeChromosome> cf, Operator<GRNTreeChromosome> o, Selection<GRNTreeChromosome> s, Evaluator<GRNTreeChromosome> e, TerminationCondition t)
    {
        super(cf, o, s, e, t);
        this.population = cf.generatePopulation();
    }

    public GRNTreeEngine(ChromosomeFactory<GRNTreeChromosome> cf,
                         Operator<GRNTreeChromosome> o,
                         Selection<GRNTreeChromosome> s,
                         Evaluator<GRNTreeChromosome> e,
                         TerminationCondition t,
                         int startGeneration,
                         List<EvaluatedChromosome<GRNTreeChromosome>> evaluatedPopulation)
    {
        super(cf, o, s, e, t);
        this.currentGeneration = startGeneration;
        this.population = cf.generatePopulation();
        this.evaluatedPopulation.addAll(evaluatedPopulation);
    }

    /**
     * Steps the algorithm forward a single generation.
     */
    @Override
    public void stepForward() {
                   
        currentGeneration++;
        
        if(evaluatedPopulation != null && evaluatedPopulation.size() > 0 )
        {
            population.clear();
            
            while(population.size() < cf.getPopulationSize())
            {
                List<Chromosome> selected = s.select(evaluatedPopulation);
                for(Chromosome c:selected)
                {
                    GRNTreeChromosome t = (GRNTreeChromosome) o.apply(c);
                    population.add(t);
                }
            }
        }
        
        evaluatedPopulation.clear();
        int index = 0;
        
        for(GRNTreeChromosome t:population)
        {
            //System.out.println("Evaluating: " + index);
            Fitness fitness = e.evaluate(t);   
            evaluatedPopulation.add(new EvaluatedChromosome(t, fitness, o.getOperator()));
            index++;
        }

        alert();
    }

    /**
     * Runs the algorithm by repeatedly calling stepForward() until a termination condition is met.
     * @return The population, current as of when the termination conditions are met.
     */
    @Override
    public List<GRNTreeChromosome> run()
    {
        do
        {
            stepForward();
        }
        while(!t.shouldTerminate(getPopulationStats()));
        
        return population;
    }

    /**
     * Returns statistics for the most current generation - includes the best chromosome, the best fitness, the current
     * generation, the population size, and the mean fitness of the population.
     * @return An object containing the statistics
     */
    @Override
    public PopulationStats getPopulationStats() {

        GRNSimulationEvaluator se = (GRNSimulationEvaluator) e;
        CopasiSimulator cs =  se.getSimulator();
        if(cs instanceof DependentValueCopasiSimulator)
        {
            DependentValueCopasiSimulator dvcs = (DependentValueCopasiSimulator) cs;
            ResponseCurve rc = new ResponseCurve(dvcs.getDependentMetaboliteValues());
            return new SVPStats(rc, super.getPopulationStats());
        }
        else
        {
            return super.getPopulationStats();
        }
    }
}