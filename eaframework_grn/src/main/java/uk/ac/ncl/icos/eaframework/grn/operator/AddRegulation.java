package uk.ac.ncl.icos.eaframework.grn.operator;

import uk.ac.ncl.icos.annotation.EAModule;
import uk.ac.ncl.icos.eaframework.operator.AbstractOperator;
import uk.ac.ncl.icos.svpmanager.SVPManager;
import uk.ac.ncl.intbio.virtualparts.entity.Interaction;
import uk.ac.ncl.intbio.virtualparts.entity.InteractionPartDetail;
import uk.ac.ncl.intbio.virtualparts.entity.Part;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.ncl.icos.eaframework.grn.GRNTreeChromosome;
import uk.ac.ncl.icos.grntree.api.GRNTreeNode;
import uk.ac.ncl.icos.grntree.api.GRNTreeNodeFactory;
import uk.ac.ncl.icos.grntree.impl.BranchNode;
import uk.ac.ncl.icos.grntree.impl.InterfaceType;
import uk.ac.ncl.icos.grntree.impl.LeafNode;
import uk.ac.ncl.icos.grntree.impl.SVPType;


/**
 * Adds regulation to a TU, and places the regulating TF in an existing TU, or in
 * a newly created TU under control of a constitutive promoter. Positive
 * or negative regulation is chosen, by choosing the relevant part. TFs for that part
 * identified, and valid locations for those two part determined.
 * The regulated part is added to a valid location, and an valid existing or new TU
 * chosen at random for the TF.
 */
public class AddRegulation extends AbstractOperator<GRNTreeChromosome> {
    
    private final SVPManager m = SVPManager.getSVPManager();
    private final int        MAX_ATTEMPTS = 10;
    private Random           r = new Random();

    @Override
    public GRNTreeChromosome apply(GRNTreeChromosome c) {

        GRNTreeChromosome t = null;
        
        try {
            
            boolean mutationOccured = false;
            int attempt = 0;

            // =================================================================
            // Perform mutation in a while loop. If any exceptional cases occur,
            // while loop is repeated until mutation is successful.
            // =================================================================
            
            while(!mutationOccured && attempt < MAX_ATTEMPTS) {

                boolean regulatedAdded = false;
                boolean regulatorAdded = false;
                boolean phosphorylationRequired = false;
                boolean phosphorylationAdded = true;

                // =================================================================
                // Each mutation attempt uses a clean copy of the solution
                // =================================================================

                t = (GRNTreeChromosome) c.duplicate();

                Part regulatedPart = r.nextInt(2) == 0 ?
                                     m.getInducPromoter() :
                                     m.getNegOperator();

                List<Part> potentialRegulatingParts = m.getModifierParts(regulatedPart);

                if( !potentialRegulatingParts.isEmpty() ) {

                    Part regulatingPart = potentialRegulatingParts.get( r.nextInt( potentialRegulatingParts.size() ) );

                    // =================================================================
                    // All locations should currently be valid, as we are not using
                    // modularity features of GRNTree
                    // TODO replace these checks with something more efficient
                    // =================================================================

                    List<GRNTreeNode> potentialRegulatableTUs = t.getValidLocations(regulatedPart);
                    List<GRNTreeNode> potentialRegulatingTUs = t.getValidLocations(regulatingPart);
                    List<GRNTreeNode> potentialPhosphorylatingTU = null;
                    
                    if(!potentialRegulatableTUs.isEmpty() && !potentialRegulatingTUs.isEmpty()) {

                        int regulatedIndex = r.nextInt(potentialRegulatableTUs.size());

                        // =================================================================
                        // Currently, the mathematical definition of SVPs requires positive
                        // regulation to be implemented using Inducible Promoters, and negative
                        // regulation to be implemented using Operators.
                        // =================================================================

                        if( regulatedPart.getType().equals("Promoter") ) {

                            if (r.nextInt(2) == 0) {

                                // ========================================================
                                // Choose a random promoter in the TU, and replace....
                                // ========================================================

                                List<GRNTreeNode> promoters = new ArrayList<GRNTreeNode>();

                                for( GRNTreeNode node : potentialRegulatableTUs.get( regulatedIndex ).getChildren() ) {
                                    if( ( (LeafNode)node).getType().equals(SVPType.Prom) ) {
                                        promoters.add(node);
                                    }
                                }

                                int index = potentialRegulatableTUs.get( regulatedIndex )
                                        .getChildren()
                                        .indexOf(promoters.get(r.nextInt(promoters.size())));

                                potentialRegulatableTUs.get( regulatedIndex )
                                        .getChildren()
                                        .get(index)
                                        .replaceNode( GRNTreeNodeFactory.getLeafNode( regulatedPart, InterfaceType.INPUT ) );
                                regulatedAdded = true;

                            } else {

                                // ========================================================
                                // ... or insert promoter at beginning of TU
                                // ========================================================

                                potentialRegulatableTUs.get( regulatedIndex )
                                        .addNode(0, GRNTreeNodeFactory.getLeafNode( regulatedPart, InterfaceType.INPUT ) );

                                regulatedAdded = true;
                            }

                        } else if ( regulatedPart.getType().equals("Operator") ) {

                            // ========================================================
                            // Choose a random promoter in the TU, and append Operator
                            // TODO operator should only be appended to const promoter
                            // ========================================================

                            List<GRNTreeNode> promoters = new ArrayList<GRNTreeNode>();

                            for( GRNTreeNode node : potentialRegulatableTUs.get( regulatedIndex ).getChildren() ) {
                                if( ( (LeafNode)node).getType().equals(SVPType.Prom) ) {
                                    promoters.add(node);
                                }
                            }

                            int index = potentialRegulatableTUs.get( regulatedIndex )
                                    .getChildren()
                                    .indexOf(promoters.get(r.nextInt(promoters.size())));

                            potentialRegulatableTUs.get( regulatedIndex )
                                    .addNode(index + 1, GRNTreeNodeFactory.getLeafNode( regulatedPart, InterfaceType.INPUT ) );
                            regulatedAdded = true;
                        }
                        
                        List<Interaction> interactions = m.getInteractions( regulatingPart, regulatedPart );

                        // =================================================================
                        // This check no longer seems to be applicable, as there are part
                        // with more than one interaction (e.g. a CDS that both induces and
                        // represses a Promoter)
                        // TODO Test this and update as needed
                        // =================================================================

                        if(interactions.size() != 1) {
                            throw new Exception("No 1 interaction between " + regulatedPart.getName() + " and " + regulatingPart.getName());
                        }

                        // =================================================================
                        // The probability of adding the TF to an existing TU, or to a new
                        // TU is equally weighted (i.e. 1 / ( # of potentialTUs + 1 ) )
                        // =================================================================

                        // =================================================================
                        // TODO If already present, add with 50% probability
                        // =================================================================

                        regulatorAdded = addPartToModel(regulatingPart, t);

                        // =================================================================
                        // Determine whether phosphorylation is required for regulation
                        // =================================================================

                        List<InteractionPartDetail> interactionPartDetails = interactions.get( 0 ).getPartDetails();

                        for(InteractionPartDetail interactionPartDetail : interactionPartDetails) {
                            if( interactionPartDetail.getPartForm().equals( "Phosphorylated" ) ) {

                                phosphorylationRequired = true;
                                List<Part> phosphorylatingParts = m.getSinglePhosphorylationPath(regulatingPart);

                                // =================================================================
                                // TODO If already present, add with 50% probability
                                // =================================================================

                                if( phosphorylatingParts != null && !phosphorylatingParts.isEmpty() ) {
                                    for(Part phosphorylatingPart :phosphorylatingParts) {
                                        if(!addPartToModel(phosphorylatingPart, t)) {
                                            phosphorylationAdded = false;
                                        }
                                    }
                                } else {
                                    phosphorylationAdded = false;
                                    System.out.println("No phosphorylating parts for " + regulatingPart.getName());
                                }
                            }
                        }
                    }
                }

                if ( ( regulatedAdded && regulatorAdded && phosphorylationRequired && phosphorylationAdded ) ||
                    ( regulatedAdded && regulatorAdded && !phosphorylationRequired ) ) {
                    mutationOccured = true;
                }

                attempt++;

                if(!mutationOccured && attempt == MAX_ATTEMPTS) {
                    return c;
                }
            }
            
            
        } catch (Exception ex) {

            // ======================================================
            // In the case of an exception being thrown, the original
            // chromosome is returned, without mutation
            // ======================================================

            Logger.getLogger(AddRegulation.class.getName()).log(Level.SEVERE, null, ex);
            return c;
        }  
        
        return t;
    }

    private boolean addPartToModel(Part part, GRNTreeChromosome t) {


        // =================================================================
        // The probability of adding the part to an existing TU, or to a new
        // TU is equally weighted (i.e. 1 / ( # of potentialTUs + 1 ) )
        // =================================================================

        try {

            List<GRNTreeNode> potentialPhosphorylatingTU = t.getValidLocations( part );
            int phosphorylatingTuIndex = r.nextInt( potentialPhosphorylatingTU.size() + 1 );
            if(phosphorylatingTuIndex == potentialPhosphorylatingTU.size()) {

                List<GRNTreeNode> partsList = new ArrayList<GRNTreeNode>();
                partsList.add(GRNTreeNodeFactory.getLeafNode(m.getConstPromoter(), InterfaceType.INPUT));
                partsList.add(GRNTreeNodeFactory.getLeafNode(m.getRBS(), InterfaceType.NONE));
                partsList.add(GRNTreeNodeFactory.getLeafNode(part, InterfaceType.OUTPUT));
                partsList.add(GRNTreeNodeFactory.getLeafNode(m.getTerminator(), InterfaceType.NONE));
                potentialPhosphorylatingTU.get(phosphorylatingTuIndex-1)
                        .getParent()
                        .addNode(GRNTreeNodeFactory.getBranchNode(InterfaceType.NONE, partsList));

                return true;

            }  else {

                GRNTreeNode phosphorylatingTU = potentialPhosphorylatingTU.get(phosphorylatingTuIndex);
                int additionIndex = phosphorylatingTU.getChildren().size() - 1;
                phosphorylatingTU.addNode(additionIndex, GRNTreeNodeFactory.getLeafNode(m.getRBS(), InterfaceType.NONE));
                additionIndex++;
                phosphorylatingTU.addNode(additionIndex, GRNTreeNodeFactory.getLeafNode(part, InterfaceType.OUTPUT));
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public String toString() {
        return "Add Regulation";
    }
}
