/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.grn;

import uk.ac.ncl.icos.eaframework.factories.AbstractChromosomeFactory;
import uk.ac.ncl.icos.eaframework.operator.Operator;
import uk.ac.ncl.icos.grntree.api.GRNTreeFactory;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * A Factory class for generating populations of GRNTree, with or without diversity achieved by application of
 * mutation Operators.
 * @author owengilfellon
 */
public class GRNChromosomeFactory extends AbstractChromosomeFactory<GRNTreeChromosome> implements Serializable {

    /**
     * @param seeds The seed models from which the population is generated
     * @param populationSize The number of instances to add to the population
     */
    public GRNChromosomeFactory(List<GRNTreeChromosome> seeds, int populationSize)
    {
         super(seeds, populationSize);
    }

    /**
     * Generates a population of GRNTree, created by duplicating random GRNTrees from the seed list
     * @return
     */
    @Override
    public List<GRNTreeChromosome> generatePopulation() {
        List<GRNTreeChromosome> pop = new ArrayList<GRNTreeChromosome>();
        Random r = new Random();
        for(int i=0; i<POPULATON_SIZE; i++)
        {
            pop.add(new GRNTreeChromosome(GRNTreeFactory.getGRNTree(seeds.get(r.nextInt(seeds.size())).getGRNTree())));
        }
        return pop;
    }

    /**
     * Generates a population of GRNTree, created by duplicating random GRNTrees from the seed list, and performing
     * a single mutation on each, chosen at random from the provided operators.
     * @param operators the mutation operators to be applied to the seeds to generate diversity.
     * @return
     */
    @Override
    public List<GRNTreeChromosome> generatePopulation(List<Operator<GRNTreeChromosome>> operators) {
        List<GRNTreeChromosome> pop = new ArrayList<GRNTreeChromosome>();
        Random r = new Random();
        for(int i=0; i<POPULATON_SIZE; i++){
            GRNTreeChromosome mutatedTree = operators.get(r.nextInt(operators.size())).apply(seeds.get(r.nextInt(seeds.size())));
            pop.add(mutatedTree);
        }
        return pop;
    }




}
