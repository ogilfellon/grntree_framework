package uk.ac.ncl.icos.eaframework.experiments;


import org.sbml.jsbml.SBMLDocument;
import uk.ac.ncl.icos.svpmanager.AbstractSVPManager;
import uk.ac.ncl.intbio.virtualparts.ModelBuilder;
import uk.ac.ncl.intbio.virtualparts.PartsHandler;
import uk.ac.ncl.intbio.virtualparts.SBML.SBMLHandler;
import uk.ac.ncl.intbio.virtualparts.entity.Interaction;
import uk.ac.ncl.intbio.virtualparts.entity.Part;

import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author b1050029
 */

public class AbstractSVPCascadeTest {
    
    PartsHandler p;
    SBMLDocument container;
    private SBMLHandler sbmlhandler;
    ModelBuilder mb;
    String server = "http://sbol.ncl.ac.uk:8081";
    
    AbstractSVPManager m = AbstractSVPManager.getSVPManager();
    
    public void run() throws Exception
    {
        p = new PartsHandler(server);
        container = new SBMLHandler().GetSBMLTemplateModel("SVP_Model");
        mb = new ModelBuilder(container);
        
        /* ====================================
         * 
         * Create Parts
         * 
         * ==================================== */
          
        Part a = m.getConstPromoterPart("A");
        Part b = m.getRBS("B");
        Part c = m.getCDS("C");

        Part d = m.getConstPromoterPart("D");
        Part e = m.getRBS("E");
        Part f = m.getCDS("F");

        Part g = m.getConstPromoterPart("G");
        Part h = m.getRBS("H");
        Part i = m.getCDS("I");

        Part j = m.getConstPromoterPart("J");
        Part k = m.getRBS("K");
        Part l = m.getCDS("L");
  
         /* ====================================
         * 
         * Create Internal Interactions
         * 
         * ==================================== */
        
        List<Interaction> a_i = m.getPoPSProduction(a, 0.0269);
        List<Interaction> d_i = m.getPoPSProduction(d, 0.0269);
        List<Interaction> g_i = m.getPoPSProduction(g, 0.0269);
        List<Interaction> j_i = m.getPoPSProduction(j, 0.0269);

        List<Interaction> b_i = m.getRiPSProduction(b, 0.51213);
        List<Interaction> e_i = m.getRiPSProduction(e, 0.51213);
        List<Interaction> h_i = m.getRiPSProduction(h, 0.51213);
        List<Interaction> k_i = m.getRiPSProduction(k, 0.51213);

        List<Interaction> c_i = m.getProteinProductionAndDegradation(c, 0.012154);
        List<Interaction> f_i = m.getProteinProductionAndDegradation(f, 0.012154);
        List<Interaction> i_i = m.getProteinProductionAndDegradation(i, 0.012154);
        List<Interaction> l_i = m.getProteinProductionAndDegradation(l, 0.012154);
        
        c_i.addAll(m.getDephosphorylationandDegradation(c, 0.0544545, 0.117454));
        f_i.addAll(m.getDephosphorylationandDegradation(f, 0.0544545, 0.117454));
        i_i.addAll(m.getDephosphorylationandDegradation(i, 0.0544545, 0.117454));
        l_i.addAll(m.getDephosphorylationandDegradation(l, 0.0544545, 0.117454));
        
        /* ====================================
         * 
         * Create External Interactions
         * 
         * ==================================== */

        Interaction cPhosF = m.getPhosphorylationInteraction(c, f, 0.4151);
        Interaction fPhosI = m.getPhosphorylationInteraction(f, i, 0.4151);
        Interaction iPhosL = m.getPhosphorylationInteraction(i, l, 0.4151);
        
        /* ====================================
         * 
         * Create SBML Documents
         * 
         * ==================================== */

        SBMLDocument a_doc = p.CreatePartModel(a, a_i);
        SBMLDocument b_doc = p.CreatePartModel(b, b_i);
        SBMLDocument c_doc = p.CreatePartModel(c, c_i);
        SBMLDocument d_doc = p.CreatePartModel(d, d_i);
        SBMLDocument e_doc = p.CreatePartModel(e, e_i);
        SBMLDocument f_doc = p.CreatePartModel(f, f_i);
        SBMLDocument g_doc = p.CreatePartModel(g, g_i);
        SBMLDocument h_doc = p.CreatePartModel(h, h_i);
        SBMLDocument i_doc = p.CreatePartModel(i, i_i);
        SBMLDocument j_doc = p.CreatePartModel(j, j_i);
        SBMLDocument k_doc = p.CreatePartModel(k, k_i);
        SBMLDocument l_doc = p.CreatePartModel(l, l_i);
        
        List<Part> cTofParts = new ArrayList<Part>();
        cTofParts.add(c);
        cTofParts.add(f);
        
        List<Part> fToIParts = new ArrayList<Part>();
        fToIParts.add(f);
        fToIParts.add(i);
        
        List<Part> iToLParts = new ArrayList<Part>();
        iToLParts.add(i);
        iToLParts.add(l);
        /*  */
        
        SBMLDocument cTof = p.CreateInteractionModel(cTofParts, cPhosF);
        SBMLDocument fToi = p.CreateInteractionModel(fToIParts, fPhosI);
        SBMLDocument iTol = p.CreateInteractionModel(iToLParts, iPhosL);
        
        SBMLDocument mrna1 = p.GetModel(p.GetPart("mRNA"));
        SBMLDocument mrna2 = p.GetModel(p.GetPart("mRNA"));
        SBMLDocument mrna3 = p.GetModel(p.GetPart("mRNA"));
        SBMLDocument mrna4 = p.GetModel(p.GetPart("mRNA"));
        
        /* ====================================
         * 
         * Assemble SBML Model
         * 
         * ==================================== */



        mb.Add(a_doc);
        mb.Link(a_doc, mrna1);
        mb.Add(mrna1);
        mb.Link(mrna1, b_doc);
        mb.Add(b_doc);
        mb.Link(b_doc, c_doc);
        mb.Add(c_doc);
        
        mb.Add(d_doc);
        mb.Link(d_doc, mrna2);
        mb.Add(mrna2);
        mb.Link(mrna2, e_doc);
        mb.Add(e_doc);
        mb.Link(e_doc, f_doc);
        mb.Add(f_doc);
        mb.Add(cTof);
        
        mb.Add(g_doc);
        mb.Link(g_doc, mrna3);
        mb.Add(mrna3);
        mb.Link(mrna3, h_doc);
        mb.Add(h_doc);
        mb.Link(h_doc, i_doc);
        mb.Add(i_doc);
        mb.Add(fToi);
        
        mb.Add(j_doc);
        mb.Link(j_doc, mrna4);
        mb.Add(mrna4);
        mb.Link(mrna4, k_doc);
        mb.Add(k_doc);
        mb.Link(k_doc, l_doc);
        mb.Add(l_doc);
        mb.Add(iTol);

        


        try
       {
           FileWriter writer = new FileWriter("cascade" + ".xml");
           writer.write(mb.GetModelString());
           writer.flush();
           // System.out.println("\nModel written to " + filename + ".xml");
       }
       catch(Exception ex)
       {
           System.out.println(ex.getMessage());
       }
    }
    
    public static void main(String[] args)
    {        
        try {
            new AbstractSVPCascadeTest().run();
        } catch (Exception ex) {
            Logger.getLogger(AbstractSVPRepressilatorTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
