package uk.ac.ncl.icos.eaframework.grn.importer;

import uk.ac.ncl.icos.datatypes.Fitness;
import uk.ac.ncl.icos.eaframework.chromosome.Chromosome;
import uk.ac.ncl.icos.eaframework.chromosome.EvaluatedChromosome;
import uk.ac.ncl.icos.eaframework.evoengines.EvoEngine;
import uk.ac.ncl.icos.eaframework.factories.ChromosomeFactory;
import uk.ac.ncl.icos.eaframework.grn.GRNChromosomeFactory;
import uk.ac.ncl.icos.eaframework.grn.GRNTreeEngine;
import uk.ac.ncl.icos.eaframework.grn.exporter.GRNTreeEngineSave;
import uk.ac.ncl.icos.eaframework.grn.observer.SVPTreeResultsFileWriter;
import uk.ac.ncl.icos.eaframework.observer.EvolutionObserver;
import uk.ac.ncl.icos.eaframework.operator.Operator;
import uk.ac.ncl.icos.eaframework.selection.AbstractMutantChamp;
import uk.ac.ncl.icos.eaframework.selection.Selection;
import uk.ac.ncl.icos.grntree.api.GRNTreeFactory;

import java.io.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.util.ArrayList;
import java.util.List;
import uk.ac.ncl.icos.eaframework.grn.GRNTreeChromosome;

/**
 * Created by owengilfellon on 13/05/2014.
 */

public class ExperimentReader {

    private final Configuration c;
    private final File homeDir;
    private final String rootDirectory;
    private final File experimentDirectory;
    private FileLock lock = null;

    public ExperimentReader(Configuration c) {
        this.c  = c;
        this.homeDir = new File(System.getProperty("user.home"));
        this.rootDirectory = homeDir + "/Results/" + c.getEXPERIMENT_NAME();
        this.experimentDirectory = new File(rootDirectory);
    }

    public String[] getExperimentNames() {
        FilenameFilter filter = new HiddenFileFilter();
        String[] experiments = experimentDirectory.list(filter);
        return experiments;
    }

    public EvoEngine<GRNTreeChromosome> open(String experiment) {

        try {

            File subDirectory = new File(rootDirectory + "/" + experiment);

            if(subDirectory.exists()) {

                File configurationFile = new File(subDirectory + "/" + c.getConfigurationFileName());
                RandomAccessFile randomAccessFile = new RandomAccessFile(configurationFile, "rw");

                if(configurationFile.exists()) {
                    FileChannel fc = randomAccessFile.getChannel();
                    lock =  fc.tryLock();
                }

                // If experiment directory is not opened by an other instance

                if (lock != null) {

                    String timeStamp = experiment.replaceFirst(c.getEXPERIMENT_NAME() + "_", "");
                    File file = new File(subDirectory + "/" + "Save.sez");

                    FileInputStream fileInputStream = new FileInputStream(file);
                    ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
                    GRNTreeEngineSave tes = (GRNTreeEngineSave)objectInputStream.readObject();
                    objectInputStream.close();

                    boolean finished = false;

                    for(String s:this.getExperimentNames()) {
                        if(s.startsWith("Termination")) {
                            finished = true;
                        }
                    }

                    if(!finished ) {

                        // Initialise algorithm with information derived from files




                        EvaluatedChromosome<GRNTreeChromosome> ec = null;
                        if(tes.getBestChromosome() != null) {
                            Chromosome bestChromosome = new GRNTreeChromosome(GRNTreeFactory.getGRNTree(tes.getBestChromosome().getChromosome()));
                            ec = new EvaluatedChromosome(   bestChromosome,
                                                            new Fitness(tes.getBestChromosome().getFitness()),
                                                            getOperator(tes.getBestChromosome().getOperator()));
                        }

                        List<GRNTreeChromosome> seeds =new ArrayList<GRNTreeChromosome>();
                        List<EvaluatedChromosome<GRNTreeChromosome>> ecs = new ArrayList<EvaluatedChromosome<GRNTreeChromosome>>();
                        // TODO Pull selection class from config file instead of hardcoding
                        Selection selection = c.getSelection();

                        if( selection instanceof AbstractMutantChamp &&
                            ec != null) {
                                ((AbstractMutantChamp)selection).setBestChromosome(ec);
                        }

                        for(GRNTreeEngineSave.EvaluatedChromosomeSave save: tes.getPopulation()) {
                            GRNTreeChromosome t = new GRNTreeChromosome(GRNTreeFactory.getGRNTree(save.getChromosome()));
                            ecs.add(new EvaluatedChromosome(t, new Fitness(save.getFitness()), getOperator(save.getOperator())));
                        }

                        for(EvaluatedChromosome<GRNTreeChromosome> eee:ecs) {
                            seeds.add((GRNTreeChromosome)eee.getChromosome());
                        }

                        ChromosomeFactory<GRNTreeChromosome> cf = new GRNChromosomeFactory(seeds, c.getPOPULATION_SIZE());

                        // Initialise Evolutionary Algorithm

                        EvoEngine<GRNTreeChromosome> e = new GRNTreeEngine(
                            cf,
                            c.getOperator(),
                            selection,
                            c.getFitnessFunction(),
                            c.getTerminationCondition(),
                            tes.getGeneration(),
                            ecs);

                        for(EvolutionObserver ob :c.getObservers()) {
                            e.attach(ob);
                            if(ob instanceof SVPTreeResultsFileWriter) {
                                ((SVPTreeResultsFileWriter) ob).setTimestamp(timeStamp);
                            }
                        }
                        return e;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    private Operator<GRNTreeChromosome> getOperator(String operator) {
        if(!operator.equals("NONE"))
        {
            try {
                Class selectionclass = Class.forName("uk.ac.ncl.intbio.eaframework.grn.operator." + operator);
                Constructor constructor = selectionclass.getConstructor();
                return (Operator<GRNTreeChromosome>) constructor.newInstance();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    public void close()
    {
        try {
            lock.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static class HiddenFileFilter implements FilenameFilter {

        @Override
        public boolean accept(File dir, String name) {
            if(name.startsWith(".")) {
                return false;
            }

            return true;
        }
    }

}
