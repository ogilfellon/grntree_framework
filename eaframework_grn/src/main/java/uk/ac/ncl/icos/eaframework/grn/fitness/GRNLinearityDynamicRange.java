/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.grn.fitness;

import org.apache.commons.math3.stat.correlation.PearsonsCorrelation;
import uk.ac.ncl.icos.annotation.EAModule;
import uk.ac.ncl.icos.datatypes.Fitness;
import uk.ac.ncl.icos.eaframework.grn.simulator.CopasiSimulator;
import uk.ac.ncl.icos.eaframework.grn.Exporter;
import uk.ac.ncl.icos.eaframework.grn.GRNTreeChromosome;
import uk.ac.ncl.icos.eaframework.grn.exporter.SVPWriteExporter;
import uk.ac.ncl.icos.eaframework.grn.simulator.DynamicTimeCopasiSimulator;
import uk.ac.ncl.icos.svpcompiler.parsing.SVPParser;
import uk.ac.ncl.icos.grntree.api.GRNTree;

/**
 *
 * @author owengilfellon
 */

@EAModule(visualName = "Linearity - Dynamic Range")
public class GRNLinearityDynamicRange extends GRNSimulationEvaluator<GRNTreeChromosome> {
    
    private static double targetRange = 0.0;
    private final double WEIGHT = 0.002;

    public GRNLinearityDynamicRange(CopasiSimulator cs)
    {
        super(cs);
    }
    
    @Override
    public Fitness evaluate(GRNTreeChromosome c)
    {    
       
        DynamicTimeCopasiSimulator ds = (DynamicTimeCopasiSimulator) cs;        
        GRNTreeChromosome g = (GRNTreeChromosome) c;
       // Double t = Double.valueOf(target);
        Exporter<String> exporter = new SVPWriteExporter();
        ds.setModel(SVPParser.getSBML(exporter.export(g), "SubtilinReceiver"));
        if(ds.run()) {
            
            /*
             * Common to both fitness evaluations - move to another class?
             */
            
            double[] responseCurve = new double[ds.getDependentMetaboliteValues().size()];
            double[] independentsRun = new double[ds.getIndependentMetaboliteValues().size()];
            
            for(int i=0; i<ds.getDependentMetaboliteValues().size(); i++) {
                responseCurve[i] = ds.getDependentMetaboliteValues().get(i).get(ds.getDependentMetaboliteValues().get(i).size()-1).doubleValue();
                independentsRun[i] = ds.getIndependentMetaboliteValues().get(i).doubleValue();
            }
            
            // Linearity Fitness
            
            PearsonsCorrelation pc = new PearsonsCorrelation();
            double correlation = pc.correlation(independentsRun, responseCurve);
            
            // linearity fitness between 0 - 1
            
            double linearityFitness = correlation > 0.0 ? correlation : 0.0;
            double observedRange = Math.abs(responseCurve[responseCurve.length -1] - responseCurve[0]);
            
            if(targetRange == 0.0) {
                targetRange = observedRange;
            }
            
            // Amplitude Fitness between 0 - 1
                
            System.out.println("Target: " + targetRange);
            System.out.println("Observ: " + observedRange);
            
            double rangeFitness;
            
            if(observedRange > targetRange)
            {
                rangeFitness = 1.0;
            }
            else
            {
                rangeFitness = 1.0 / Math.sqrt(1.0 + (Math.abs(observedRange - targetRange) * WEIGHT));
            }
        
            // Calculate combined fitness
            
            System.out.println("Range: " + rangeFitness);
            System.out.println("Linearity: " + linearityFitness);
            
            double finalFit = linearityFitness * rangeFitness;
            
            System.out.println("Final: " + finalFit);
            
            // return new Fitness(finalFit * 100.0);
            
            return new Fitness(Math.min(rangeFitness, linearityFitness) * 100.0);
        }
        else
        {
            return new Fitness(0.0);
        }           
    } 
}
