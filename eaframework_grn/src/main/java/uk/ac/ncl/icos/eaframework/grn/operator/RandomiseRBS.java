package uk.ac.ncl.icos.eaframework.grn.operator;

import uk.ac.ncl.icos.annotation.EAModule;
import uk.ac.ncl.icos.eaframework.operator.AbstractOperator;
import uk.ac.ncl.icos.svpmanager.SVPManager;
import uk.ac.ncl.icos.grntree.api.GRNTree;
import uk.ac.ncl.icos.grntree.api.GRNTreeNode;
import uk.ac.ncl.icos.grntree.impl.InterfaceType;
import uk.ac.ncl.icos.grntree.impl.LeafNode;
import uk.ac.ncl.icos.grntree.impl.SVPType;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.ncl.icos.eaframework.grn.GRNTreeChromosome;
import uk.ac.ncl.icos.grntree.api.GRNTreeNodeFactory;


/**
 * Identifies all RBSs within the GRNTree, and replaces one
 * at random with a randomly chosen RBS part from the repository.
 * 
 * @author owengilfellon
 */
@EAModule(visualName = "Randomise RBS")
public class RandomiseRBS extends AbstractOperator<GRNTreeChromosome> {
    
    private final SVPManager m = SVPManager.getSVPManager();

    @Override
    public GRNTreeChromosome apply(GRNTreeChromosome c) {
        
        GRNTreeChromosome t = (GRNTreeChromosome) c.duplicate();
        List<GRNTreeNode> allRBSs = new ArrayList<GRNTreeNode>();
        Iterator<GRNTreeNode> it = t.getBreadthFirstIterator();
        
        /*
         * Identify all RBSs within the GRN Tree
         */
        
        while(it.hasNext()) {
            GRNTreeNode n = it.next();
            if(n instanceof LeafNode) {
                LeafNode ln = (LeafNode) n;
                if(ln.getType() == SVPType.RBS) {
                    allRBSs.add(n);
                }
            }
        }
        
        /*
         * If no constitutive promoters are found in the model, then the
         * mutation can not be applied. Return c.
         */
        
        if(allRBSs.isEmpty()) {
            return c;
        }
            
        try {

            /*
             * Replace a Promoter randomly selected from allPromoters
             * with a Promoter randomly retrieved from the repository.
             */

            allRBSs
                    .get(new Random().nextInt(allRBSs.size()))
                    .replaceNode(GRNTreeNodeFactory.getLeafNode(m.getRBS(), InterfaceType.NONE));

        } catch (Exception ex) {
            Logger.getLogger(RandomiseRBS.class.getName()).log(Level.SEVERE, null, ex);
            return c;
        }
        
        return t;
    }
}
