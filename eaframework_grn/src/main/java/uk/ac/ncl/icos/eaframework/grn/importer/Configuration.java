package uk.ac.ncl.icos.eaframework.grn.importer;

import org.w3c.dom.Document;
import uk.ac.ncl.icos.eaframework.chromosome.EvaluatedChromosome;
import uk.ac.ncl.icos.eaframework.fitness.Evaluator;
import uk.ac.ncl.icos.eaframework.grn.simulator.CopasiSimulator;
import uk.ac.ncl.icos.eaframework.observer.EvolutionObserver;
import uk.ac.ncl.icos.eaframework.operator.Operator;
import uk.ac.ncl.icos.eaframework.selection.Selection;
import uk.ac.ncl.icos.eaframework.terminationconditions.TerminationCondition;

import java.util.List;
import uk.ac.ncl.icos.eaframework.grn.GRNTreeChromosome;

/**
 * Created by owengilfellon on 03/04/2014.
 */
public class Configuration {

    private int POPULATION_SIZE;
    private int MUTATIONS_PER_GENERATION;
    private final String EXPERIMENT_NAME;
    private final boolean restart;
    private final String configurationFileName;
    private final Document CONFIGURATION_FILE;

    private final List<GRNTreeChromosome> seeds;
    private final Operator<GRNTreeChromosome> o;
    private final TerminationCondition t;
    private final CopasiSimulator cs;
    private final Selection<GRNTreeChromosome> s;
    private final Evaluator<GRNTreeChromosome> f;
    private final List<EvolutionObserver> observers;


    public Configuration(List<GRNTreeChromosome> seeds,
                         Operator<GRNTreeChromosome> o,
                         TerminationCondition t,
                         CopasiSimulator cs,
                         Selection<GRNTreeChromosome> s,
                         Evaluator<GRNTreeChromosome> f,
                         List<EvolutionObserver> observers,
                         int populationSize,
                         int mutationsPerGeneration,
                         boolean restart,
                         String experimentName,
                         Document configurationFile,
                         String configurationFileName)
    {
        this.seeds = seeds;
        this.o = o;
        this.t = t;
        this.cs = cs;
        this.s = s;
        this.f  = f;
        this.observers = observers;
        this.POPULATION_SIZE = populationSize;
        this.MUTATIONS_PER_GENERATION = mutationsPerGeneration;
        this.restart = restart;
        this.EXPERIMENT_NAME = experimentName;
        this.CONFIGURATION_FILE = configurationFile;
        this.configurationFileName = configurationFileName;

    }

    public String getConfigurationFileName() {
        return configurationFileName;
    }

    public Document getCONFIGURATION_FILE() { return CONFIGURATION_FILE; }

    public String getEXPERIMENT_NAME() { return EXPERIMENT_NAME; }

    public int getPOPULATION_SIZE() {
        return POPULATION_SIZE;
    }

    public int getMUTATIONS_PER_GENERATION() {
        return MUTATIONS_PER_GENERATION;
    }

    public boolean isRestart() {
        return restart;
    }

    public List<GRNTreeChromosome> getSeeds() {
        return seeds;
    }

    public Operator<GRNTreeChromosome> getOperator() {
        return o;
    }

    public TerminationCondition getTerminationCondition() {
        return t;
    }

    public CopasiSimulator getSimulator() {
        return cs;
    }

    public Selection<GRNTreeChromosome> getSelection() {
        return s;
    }

    public Evaluator<GRNTreeChromosome> getFitnessFunction() {
        return f;
    }

    public List<EvolutionObserver> getObservers() {
        return observers;
    }


}
