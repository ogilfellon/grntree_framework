package uk.ac.ncl.icos.eaframework.grn.booleannetwork;

/**
 * Created by owengilfellon on 03/07/2014.
 */
public class BooleanNode {

    private boolean state = false;
    private ActivationFunction activationFunction = null;
    private String id = "";

    public BooleanNode(boolean state, ActivationFunction activationFunction, String id)
    {
        this.state = state;
        this.activationFunction = activationFunction;
        this.id = id;
    }

    public boolean nextState()
    {
        return activationFunction.assess();
    }

    public boolean getState()
    {
        return state;
    }

    public ActivationFunction getActivationFunction()
    {
        return activationFunction;
    }

    public String getId()
    {
        return id;
    }


}
