/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.grn.datatype;

import uk.ac.ncl.icos.datatypes.ResponseCurve;
import uk.ac.ncl.icos.eaframework.chromosome.Chromosome;
import uk.ac.ncl.icos.eaframework.grn.GRNTreeChromosome;
import uk.ac.ncl.icos.eaframework.stats.PopulationStats;
import uk.ac.ncl.icos.grntree.api.GRNTree;

/**
 *
 * @author owengilfellon
 */
public class SVPStats<T extends GRNTreeChromosome> extends PopulationStats {
    
    ResponseCurve responseCurve;
    
    public SVPStats(ResponseCurve responseCurve, PopulationStats<T> stats){
        super(stats.getBestChromosome(), stats.getCurrentGeneration(), stats.getPopulationSize(), stats.getMeanFitness(), stats.getBestFitness());
        this.responseCurve = responseCurve;
    }
    
    public SVPStats(ResponseCurve responseCurve, Chromosome bestChromosome, int currentGeneration, int populationSize, double meanFitness, double bestFitness) {
        super(bestChromosome, currentGeneration, populationSize, meanFitness, bestFitness);
        this.responseCurve = responseCurve;
    }

    public ResponseCurve getResponseCurve() {
        return responseCurve;
    }

    
}
