package uk.ac.ncl.icos.eaframework.grn.operator;

import uk.ac.ncl.icos.annotation.EAModule;
import uk.ac.ncl.icos.eaframework.operator.AbstractOperator;
import uk.ac.ncl.icos.svpmanager.SVPManager;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.ncl.icos.eaframework.grn.GRNTreeChromosome;
import uk.ac.ncl.icos.grntree.api.GRNTreeNode;
import uk.ac.ncl.icos.grntree.api.GRNTreeNodeFactory;
import uk.ac.ncl.icos.grntree.impl.InterfaceType;
import uk.ac.ncl.icos.grntree.impl.LeafNode;
import uk.ac.ncl.icos.grntree.impl.SVPType;


/**
 * Identifies all Constitutive Promoters within the GRNTree, and replaces one
 * at random with a randomly chosen constitutive promoter from the repository.
 * 
 * @author owengilfellon
 */
@EAModule(visualName = "Randomise Constitutive Promoter")
public class RandomiseConstPromoter extends AbstractOperator<GRNTreeChromosome> {
    
    private final SVPManager m = SVPManager.getSVPManager();

    @Override
    public GRNTreeChromosome apply(GRNTreeChromosome c) {
        
        GRNTreeChromosome t = (GRNTreeChromosome) c.duplicate();
        List<GRNTreeNode> allPromoters = new ArrayList<>();
        Iterator<GRNTreeNode> it = t.getBreadthFirstIterator();
        
        /*
         * Identify all constitutive promoters within the GRN Tree
         */
        
        while(it.hasNext())
        {
            GRNTreeNode n = it.next();
            if(n instanceof LeafNode) {
                LeafNode leafNode = (LeafNode) n;
                if(leafNode.getType() == SVPType.Prom) {
                    if(m.isConstitutivePromoter(leafNode.getSVP())){
                            allPromoters.add(n);
                    }
                }
            }
        }

        
        /*
         * If no constitutive promoters are found in the model, then the
         * mutation can not be applied. Return c.
         */
        
        if(allPromoters.isEmpty()) {
            return c;
        }

        try {

            /*
             * Replace a Promoter randomly selected from allPromoters
             * with a Promoter randomly retrieved from the repository.
             */

            allPromoters
                    .get(new Random().nextInt(allPromoters.size()))
                    .replaceNode(GRNTreeNodeFactory.getLeafNode(m.getConstPromoter(), InterfaceType.NONE));
        } catch (Exception ex) {
            Logger.getLogger(RandomiseConstPromoter.class.getName()).log(Level.SEVERE, null, ex);
            return c;
        }
        
        return t;
    }
}
