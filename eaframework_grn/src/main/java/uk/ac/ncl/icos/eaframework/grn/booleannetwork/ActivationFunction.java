package uk.ac.ncl.icos.eaframework.grn.booleannetwork;

/**
 * Created by owengilfellon on 16/06/2014.
 */
public interface ActivationFunction {

    public boolean assess();

}
