package uk.ac.ncl.icos.eaframework.grn.operator;

import uk.ac.ncl.icos.annotation.EAModule;
import uk.ac.ncl.icos.eaframework.operator.AbstractOperator;
import uk.ac.ncl.icos.svpmanager.SVPManager;
import uk.ac.ncl.icos.grntree.api.GRNTree;
import uk.ac.ncl.icos.grntree.api.GRNTreeNode;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.ncl.icos.eaframework.grn.GRNTreeChromosome;

/**
 * Randomly selects a TU from within a GRNTree and duplicates it. The duplicate
 * is placed within the same parent as the original.
 * 
 * @author owengilfellon
 */
public class DuplicateTU extends AbstractOperator<GRNTreeChromosome> {
    
    private final SVPManager m = SVPManager.getSVPManager();

    @Override
    public GRNTreeChromosome apply(GRNTreeChromosome c) {
        
        GRNTreeChromosome t = (GRNTreeChromosome) c.duplicate();     
        List<GRNTreeNode> allTranscriptionalUnits = new ArrayList<>();
        Iterator<GRNTreeNode> it = t.getBreadthFirstIterator();
        
        // Retrieve all TUs from model by iterating through nodes and testing
        
        while(it.hasNext()) {
            GRNTreeNode n = it.next();
            if(n.isTranscriptionUnit()) {
                allTranscriptionalUnits.add(n);
            }
        }

        // If no TUs, then mutation cannot be performed. return t.
        
        if(!allTranscriptionalUnits.isEmpty()) {
            
            // Select a random TU from those identified
        
            int index = new Random().nextInt(allTranscriptionalUnits.size());
            GRNTreeNode duplicatedTU = allTranscriptionalUnits.get(index);
            
            // Currently places in existing module
            
            try {
                duplicatedTU.getParent().addNode(duplicatedTU.duplicate());
            } catch (Exception ex) {
                Logger.getLogger(DuplicateTU.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        return t;
    }
}
