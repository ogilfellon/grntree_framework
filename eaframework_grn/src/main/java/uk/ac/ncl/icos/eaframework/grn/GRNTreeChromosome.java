package uk.ac.ncl.icos.eaframework.grn;


import uk.ac.ncl.icos.grntree.impl.LeafNode;
import uk.ac.ncl.icos.grntree.impl.BranchNode;
import uk.ac.ncl.icos.grntree.impl.NodeManager;
import com.thoughtworks.xstream.XStream;
import uk.ac.ncl.icos.grntree.traversal.FlowNavigator;
import uk.ac.ncl.intbio.virtualparts.entity.Interaction;
import uk.ac.ncl.intbio.virtualparts.entity.Part;

import java.util.*;
import uk.ac.ncl.icos.eaframework.chromosome.Chromosome;
import uk.ac.ncl.icos.grntree.api.GRNEdge;
import uk.ac.ncl.icos.grntree.api.GRNTree;
import uk.ac.ncl.icos.grntree.api.GRNTreeFactory;
import uk.ac.ncl.icos.grntree.api.GRNTreeNode;
import uk.ac.ncl.icos.grntree.api.ModelObserver;
import uk.ac.ncl.icos.grntree.api.ModelSubject;


/**
 * A tree-based representation of a Genetic Regulatory Network (GRN), with identification of transcription units.
 * "Interface Types" can be specified in order to limit the interactionDocuments permitted between nodes (e.g. INPUT, OUTPUT).
 * A node (e.g. Promoter) with interface type INPUT can interact with sibling OUTPUT nodes. If a parent has an Interface
 * Type, the bounds of a node's potential interactionDocuments are extended to the parent's siblings. The set of nodes specified
 * by a given node's interaction bounds and it's compliment must be orthogonal (i.e. interactionDocuments are not allowed outside
 * of a given node's interaction bounds).
 *
 * @author owengilfellon
 */

//TODO stick this behind an api / interface

public class GRNTreeChromosome implements Chromosome, ModelSubject {

    private GRNTree tree;
 
    public GRNTreeChromosome() { tree=GRNTreeFactory.getGRNTree(); }
    public GRNTreeChromosome(GRNTree tree) { tree=this.tree;  }

    public GRNTree getGRNTree()
    {
        return tree;
    }
    

    /**
     * Returns the Root Node of the Tree.
     * @return the Root Node of the Tree.
     */
    public GRNTreeNode getRootNode()
    {
        return tree.getRootNode();
    }

    /**
     * Returns the number of nodes in the tree, including all Branch and Leaf nodes.
     * @return
     */
    public int getNodesSize()
    {
        return tree.getNodesSize();
    }

    /**
     * Returns the number of parts (i.e. SVPs, Genetic Features) in the tree.
     * @return
     */
    public int getPartsSize()
    {
        return tree.getPartsSize();
    }

    /**
     * Returns the number of Transcription Units in the tree (i.e. those nodes that are non-empty and contain only
     * LeafNodes)
     * @return
     */
    public int getTUSize()
    {
        return tree.getTUSize();
    }

    /**
     * Returns the number of Interactions between parts in the treeInteractions are determined computationally between
     * OUTPUT-> INPUT nodes in the case of regulation, or OUTPUT->OUTPUT nodes in the case of phosphorylation.
     * @return
     */
    public int getInteractionsSize()
    {
        return tree.getInteractionsSize();
    }

    /**
     * Returns all parts (i.e. SVPs) that are in the tree
     * @return
     */
    public List<Part> getAllParts()
    {
        return tree.getAllParts();
    }

    /**
     * Returns all Input Nodes (i.e. those that have been selected to receive inputs from outside their parent node)
     * @return
     */
    public List<GRNTreeNode> getInputNodes()
    {
        return tree.getInputNodes();
    }

    /**
     * Returns all Output Nodes (i.e. those that have been selected to send output outside their parent node)
     * @return
     */
    public List<GRNTreeNode> getOutputNodes()
    {
        return tree.getOutputNodes();
    }

    public List<GRNTreeNode> getParents(GRNTreeNode node) {
        return tree.getParents(node);
    }

    public Set<Interaction> getInteractions(GRNTreeNode node){
        return tree.getInteractions(node);
    }

    public List<GRNTreeNode> getInteractingParts(GRNTreeNode node) {
        return tree.getInteractingParts(node);
    }
    
    public boolean containsLeafNode(GRNTreeNode leafNode) {
        return tree.containsLeafNode(leafNode);
    }

    public List<GRNTreeNode> getParts(Interaction interaction) {
        return tree.getParts(interaction);
    }

    /**
     * Returns a verbose string with details of the tree's organisation, including location of parts, and interactionDocuments
     * @return
     */
    public String debugInfo()
    {
        return tree.debugInfo();
    }

    /**
     * Returns a description of the model in SVPWrite format.
     * @return
     */
    public String toString()
    {
        return tree.toString();
    }

    public GRNTreeChromosome duplicate() {
        
        return new GRNTreeChromosome(GRNTreeFactory.getGRNTree(tree));
    }

    public boolean addInteraction(Interaction interaction) {
        return tree.addInteraction(interaction);
    }

    // TODO This needs to be validated and optimised - is currently slow?

    // TODO Separate out validation from data structure? Create new classes for validation

    /**
     * Given a part p, returns allowed locations for p in the model, determined by ensuring required orthogonality as
     * specified by the INPUT and OUTPUTs of each node (i.e. p must be orthogonal to all other parts outside a given
     * node's interaction bounds).
     * @param p
     * @return
     * @throws Exception
     */
    public List<GRNTreeNode> getValidLocations(Part p) throws Exception
    {
        return tree.getValidLocations(p);
    }

    // TODO This needs to be validated and optimised - is currently slow?

    /**
     * Given a node n, returns allowed locations for n in the model, determined by ensuring required orthogonality as
     * specified by the INPUT and OUTPUTs of each node (i.e. n must be orthogonal to all other nodes outside a given
     * node's interaction bounds).
     * @param n
     * @return
     * @throws Exception
     */
    public List<GRNTreeNode> getValidLocations(GRNTreeNode n) throws Exception
    {
        return tree.getValidLocations(n);
    }

    /**
     * Returns a PreOrder Iterator for traversing the tree. Descendants are prioritised over siblings (i.e. Depth First)
     * @return
     */
    public Iterator<GRNTreeNode> getPreOrderIterator()
    {
        return tree.getPreOrderIterator();
    }

    /**
     * Returns a BreadthFirst Iterator for traversing the tree. Siblings are prioritised over descendants (i.e. Breadth
     * First)
     * @return
     */
    public Iterator<GRNTreeNode> getBreadthFirstIterator()
    {
        return tree.getBreadthFirstIterator();
    }

    public FlowNavigator getFlowNavigator(LeafNode startNode)
    {
        return tree.getFlowNavigator(startNode);
    }

    public FlowNavigator getFlowNavigator(String startNode)
    {
        return tree.getFlowNavigator(startNode);
    }

    public void alias(XStream xstream)
    {
    }

    @Override
    public void attach(ModelObserver o) {
        tree.attach(o);
    }

    @Override
    public void dettach(ModelObserver d) {
        tree.attach(d);
    }

    @Override
    public void alert() {
    }

    @Override
    public int hashCode() {
        return this.toString().hashCode();
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null)
            return false;
        if(obj == this)
            return true;
        if(!(obj instanceof GRNTreeChromosome))
            return false;

        GRNTreeChromosome other = (GRNTreeChromosome) obj;
        return other.toString().equals(this.toString());
    }
}
