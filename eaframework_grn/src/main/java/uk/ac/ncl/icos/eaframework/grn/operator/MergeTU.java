package uk.ac.ncl.icos.eaframework.grn.operator;

import uk.ac.ncl.icos.annotation.EAModule;
import uk.ac.ncl.icos.eaframework.operator.AbstractOperator;
import uk.ac.ncl.icos.grntree.api.GRNTree;
import uk.ac.ncl.icos.grntree.api.GRNTreeNode;
import uk.ac.ncl.icos.grntree.impl.BranchNode;
import uk.ac.ncl.icos.grntree.impl.InterfaceType;
import uk.ac.ncl.icos.grntree.impl.LeafNode;
import uk.ac.ncl.icos.grntree.impl.SVPType;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import uk.ac.ncl.icos.eaframework.grn.GRNTreeChromosome;
import uk.ac.ncl.icos.grntree.api.GRNTreeNodeFactory;

/**
 * Created by owengilfellon on 06/06/2014.
 */
@EAModule(visualName = "Merge TU")
public class MergeTU extends AbstractOperator<GRNTreeChromosome> {

    private final int        MAX_ATTEMPTS = 10;


    @Override
    public GRNTreeChromosome apply(GRNTreeChromosome c) {


        Random r = new Random();
        GRNTreeChromosome t = null;
        boolean mutationOccured = false;
        int attempt = 0;

        while(!mutationOccured && attempt < MAX_ATTEMPTS)
        {
            t = (GRNTreeChromosome) c.duplicate();

            Iterator<GRNTreeNode> i = t.getPreOrderIterator();
            List<GRNTreeNode> tus = new ArrayList<GRNTreeNode>();
            while(i.hasNext()) {
                GRNTreeNode n = i.next();
                if(n.isTranscriptionUnit()) {
                    tus.add(n);
                }
            }

            if(tus.size()<2) {
                return c;
            }

            int firstIndex = r.nextInt(tus.size());
            int secondIndex;

            do {secondIndex = r.nextInt(tus.size());}
            while(secondIndex == firstIndex);


            GRNTreeNode firstTU = tus.get(firstIndex);
            GRNTreeNode secondTU = tus.get(secondIndex);

            List<GRNTreeNode> parts = new ArrayList<GRNTreeNode>();

            Iterator<GRNTreeNode> j = firstTU.getChildren().iterator();

            while(j.hasNext()) {
                LeafNode part = (LeafNode)j.next();
                if(part.getType().equals(SVPType.Prom) || part.getType().equals(SVPType.Op)) {
                    parts.add(GRNTreeNodeFactory.getLeafNode(part.getSVP(), InterfaceType.NONE));
                }

            }

            j = secondTU.getChildren().iterator();

            while(j.hasNext()) {
                LeafNode part = (LeafNode)j.next();
                if(!part.getType().equals(SVPType.Prom) && !part.getType().equals(SVPType.Op)) {
                    parts.add(GRNTreeNodeFactory.getLeafNode(part.getSVP(), InterfaceType.NONE));
                }
            }


            GRNTreeNode mergedTU = GRNTreeNodeFactory.getBranchNode(InterfaceType.NONE, parts);
            t.getRootNode().removeNode(firstTU);
            t.getRootNode().removeNode(secondTU);
            try {
                t.getRootNode().addNode(mergedTU);
                mutationOccured = true;
            } catch (Exception e) {
                e.printStackTrace();
                return c;
            }

            attempt++;
        }


        return t;
    }

}
