package uk.ac.ncl.icos.eaframework.grn.operator;


import uk.ac.ncl.icos.eaframework.operator.AbstractOperator;
import uk.ac.ncl.icos.eaframework.operator.Operator;
import uk.ac.ncl.icos.svpmanager.SVPManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import uk.ac.ncl.icos.eaframework.grn.GRNTreeChromosome;

/**
 *
 * @author owengilfellon
 */
public class OperatorGroup extends AbstractOperator<GRNTreeChromosome> {
    
    private SVPManager m = SVPManager.getSVPManager();
    private List<Operator<GRNTreeChromosome>> operators = new ArrayList<Operator<GRNTreeChromosome>>();
    private int mutationsPerGeneration = 1;
    private Operator<GRNTreeChromosome> o;
    private final int        MAX_ATTEMPTS = 10;

    public OperatorGroup(List<Operator<GRNTreeChromosome>> operators) {
        this.operators = operators;
    }
    
    public OperatorGroup(List<Operator<GRNTreeChromosome>> operators, int mutationsPerGeneration) {
        this(operators);
        this.mutationsPerGeneration = mutationsPerGeneration;      
    }
    
    @Override
    public GRNTreeChromosome apply(GRNTreeChromosome c) {
        
        GRNTreeChromosome t = c;
        GRNTreeChromosome prevT = null;
        
        for(int i=0; i<mutationsPerGeneration; i++)
        {
            boolean mutationOccured = false;
            int attempt = 0;
            prevT = t;

            while(!mutationOccured && attempt < MAX_ATTEMPTS)
            {
                o = operators.get(new Random().nextInt(operators.size()));
                // System.out.println(o.getClass().getName());
                t = o.apply(t);
                if(!t.equals(prevT)) {
                    mutationOccured = true;
                }
                /*
                else {
                    System.out.println("========NO MUTATION OCCURRED==========");
                }*/
                attempt++;
            }
        }
        
        return t;
    }

    @Override
    public Operator<GRNTreeChromosome> getOperator() {
        return o;
    }
    
    
}
