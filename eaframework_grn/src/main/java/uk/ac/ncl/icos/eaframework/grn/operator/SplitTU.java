package uk.ac.ncl.icos.eaframework.grn.operator;

import uk.ac.ncl.icos.annotation.EAModule;
import uk.ac.ncl.icos.eaframework.operator.AbstractOperator;
import uk.ac.ncl.icos.svpmanager.SVPManager;
import uk.ac.ncl.icos.grntree.api.GRNTree;
import uk.ac.ncl.icos.grntree.api.GRNTreeNode;
import uk.ac.ncl.icos.grntree.impl.BranchNode;
import uk.ac.ncl.icos.grntree.impl.InterfaceType;
import uk.ac.ncl.icos.grntree.impl.LeafNode;
import uk.ac.ncl.icos.grntree.impl.SVPType;
import uk.ac.ncl.intbio.virtualparts.entity.Interaction;
import uk.ac.ncl.intbio.virtualparts.entity.InteractionPartDetail;
import uk.ac.ncl.intbio.virtualparts.entity.Part;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.ncl.icos.eaframework.grn.GRNTreeChromosome;
import uk.ac.ncl.icos.grntree.api.GRNTreeNodeFactory;


/**
 * Selects a random Transcriptional Unit (A) from the GRNTree. A random RBS node within
 * the TU is identified as a split point. The parts following the split point are
 * replaced with an RBS, CDS and terminator, with the CDS coding for a TF for the 
 * inducible promoter of a newly created TU (B). The parts that are removed from A
 * are placed into B.
 * 
 * @author owengilfellon
 */
public class SplitTU extends AbstractOperator<GRNTreeChromosome> {
    
    private final SVPManager m = SVPManager.getSVPManager();
    private final int MAX_ATTEMPTS = 10;
    private final Random r = new Random();

    @Override
    public GRNTreeChromosome apply(GRNTreeChromosome c) {

        GRNTreeChromosome t = null;

        try{

            boolean mutationOccured = false;
            int attempt = 0;

            while(!mutationOccured && attempt < MAX_ATTEMPTS) {

                boolean regulatedAdded = false;
                boolean regulatorAdded = false;
                boolean phosphorylationRequired = false;
                boolean phosphorylationAdded = false;

                t = (GRNTreeChromosome) c.duplicate();
                List<GRNTreeNode> allTranscriptionalUnits = new ArrayList<GRNTreeNode>();
                Iterator<GRNTreeNode> it = t.getBreadthFirstIterator();

                // Retrieve all Transcriptional Units from the GRNTree

                while(it.hasNext()) {
                    GRNTreeNode n = it.next();
                    if(n.isTranscriptionUnit()){
                        allTranscriptionalUnits.add(n);
                    }
                }

                /*
                 * If there are no Transcriptional Units, then the mutation cannot
                 * be applied. Return c.
                 */

                if(!allTranscriptionalUnits.isEmpty()){

                    /*
                     * Randomly select a TU to be split
                     */

                    GRNTreeNode A = allTranscriptionalUnits.get(r.nextInt(allTranscriptionalUnits.size())); 
                    List<GRNTreeNode> allRBSsInA = new ArrayList<GRNTreeNode>();
                    ListIterator<GRNTreeNode> lit = A.getChildren().listIterator();

                    // Identify potential start points of split

                    while(lit.hasNext()){

                        // Cast is safe - only LeafNodes should be present in TUs, checking using isTranscriptionUnit() above.

                        LeafNode leafNode = (LeafNode) lit.next();
                        if(leafNode.getType().equals(SVPType.RBS)){
                            allRBSsInA.add(leafNode);
                        }
                    }

                    /*
                     * If there are no RBS nodes in the chosen TU, the mutation
                     * cannot be applied. Re-run the loop.
                     */

                    if(!allRBSsInA.isEmpty()) {


                        /*
                         * Retrieve part that will regulate B - only positive regulation is
                         * chosen, so that the expression of A will induce the expression of B
                         */

                        Part regulatedPart = m.getInducPromoter();

                        // Retrieve all parts that can regulate the chosen promoter

                        List<Part> allRegulatingParts = m.getModifierParts(regulatedPart);

                        if(!allRegulatingParts.isEmpty()){

                            /*
                             * Storage for parts to be modified
                             */

                            List<GRNTreeNode> addToB = new ArrayList<GRNTreeNode>();
                            List<GRNTreeNode> removeFromA = new ArrayList<GRNTreeNode>();

                            /*
                             * Identify a random RBS node as the "split point"
                             */

                            GRNTreeNode splitPoint = allRBSsInA.get(r.nextInt(allRBSsInA.size()));
                            Part regulatingPart = allRegulatingParts.get(r.nextInt(allRegulatingParts.size()));
                            List<Part> phosphorylatingParts = null;

                            List<Interaction> interactions = m.getInteractions(regulatingPart, regulatedPart);

                            if(interactions.size() != 1){
                                throw new Exception("No 1 interaction between " + regulatedPart.getName() + " and " + regulatingPart.getName());
                            }

                            List<InteractionPartDetail> interactionPartDetails = interactions.get(0).getPartDetails();
                            for(InteractionPartDetail ipd:interactionPartDetails) {
                                if(ipd.getPartForm().equals("Phosphorylated")) {
                                    phosphorylationRequired = true;
                                    phosphorylatingParts = m.getSinglePhosphorylationPath(regulatingPart);
                                }
                            }

                            addToB.add(GRNTreeNodeFactory.getLeafNode(regulatedPart, InterfaceType.INPUT));

                            lit = A.getChildren().listIterator(A.getChildren().indexOf(splitPoint));

                            while(lit.hasNext()) {
                                GRNTreeNode node = lit.next();
                                addToB.add(node.duplicate());
                                removeFromA.add(node);
                            }

                            // Perform removals outside of iteration to avoid "Comodification"

                            for(GRNTreeNode node:removeFromA) {
                                A.removeNode(node);
                            }

                            // Add all identified parts to B

                            A.getParent().addNode(GRNTreeNodeFactory.getBranchNode(InterfaceType.NONE, addToB));
                            regulatedAdded = true;

                            /*
                             * If TF requires phosphorylation, add to same TU
                             */

                            if(phosphorylatingParts!=null && !phosphorylatingParts.isEmpty()) {

                                phosphorylationAdded = true;

                                for(Part part:phosphorylatingParts) {
                                    A.addNode(GRNTreeNodeFactory.getLeafNode(m.getRBS(), InterfaceType.NONE));
                                    A.addNode(GRNTreeNodeFactory.getLeafNode(part, InterfaceType.OUTPUT));
                                }
                            }

                            // Add regulating part to A, and replace parts moved to B

                            A.addNode(GRNTreeNodeFactory.getLeafNode(m.getRBS(), InterfaceType.NONE));
                            A.addNode(GRNTreeNodeFactory.getLeafNode(regulatingPart, InterfaceType.OUTPUT));
                            A.addNode(GRNTreeNodeFactory.getLeafNode(m.getTerminator(), InterfaceType.NONE));

                            regulatorAdded = true;

                            if ( ( regulatedAdded && regulatorAdded && phosphorylationRequired && phosphorylationAdded ) ||
                                    ( regulatedAdded && regulatorAdded && !phosphorylationRequired ) ) {
                                mutationOccured = true;
                            }

                        }
                    }

                    attempt++;

                    if(!mutationOccured && attempt == MAX_ATTEMPTS) {
                        return c;
                    }
                }
            }
        } catch(Exception e) {
            Logger.getLogger(SwapParts.class.getName()).log(Level.SEVERE, null, e);
            return c;
        }
        
        return t;
    }


}
