package uk.ac.ncl.icos.eaframework.grn.operator;

import uk.ac.ncl.icos.annotation.EAModule;
import uk.ac.ncl.icos.eaframework.operator.AbstractOperator;
import uk.ac.ncl.icos.svpmanager.SVPManager;
import uk.ac.ncl.icos.grntree.api.GRNTree;
import uk.ac.ncl.icos.grntree.api.GRNTreeNode;
import uk.ac.ncl.icos.grntree.impl.InterfaceType;
import uk.ac.ncl.icos.grntree.impl.LeafNode;
import uk.ac.ncl.icos.grntree.impl.SVPType;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.ncl.icos.eaframework.grn.GRNTreeChromosome;
import uk.ac.ncl.icos.grntree.api.GRNTreeNodeFactory;


/**
 * Randomly selects a regulated Transcriptional Unit, and removes its regulatory
 * element. If there is more than one regulatory element, then one is randomly
 * chosen.
 * 
 * @author owengilfellon
 */

public class RemoveRegulation extends AbstractOperator<GRNTreeChromosome> {

    private final SVPManager m = SVPManager.getSVPManager();
    private final int        MAX_ATTEMPTS = 10;

    @Override
    public GRNTreeChromosome apply(GRNTreeChromosome c) {

        GRNTreeChromosome t = null;

        boolean mutationOccured = false;
        int attempt = 0;

        while ( !mutationOccured && attempt < MAX_ATTEMPTS )
        {
            t = (GRNTreeChromosome) c.duplicate();
            List<GRNTreeNode> allRegulatedTUs = new ArrayList<GRNTreeNode>();
            Iterator<GRNTreeNode> i = t.getBreadthFirstIterator();

            while (i.hasNext()) {
                GRNTreeNode node = i.next();
                if ( node.isRegulatedTranscriptionUnit() ) {
                    allRegulatedTUs.add( node );
                }
            }

            if ( allRegulatedTUs.isEmpty() ) {

                // =================================================================
                // There are no regulated TUs in the model, so return original model
                // =================================================================

                return c;
            }

            Random r = new Random();
            GRNTreeNode chosenTU = allRegulatedTUs.get( r.nextInt ( allRegulatedTUs.size() ) );
            List<GRNTreeNode> regulatedPromoters = new ArrayList<GRNTreeNode>();
            List<GRNTreeNode> operators = new ArrayList<GRNTreeNode>();

            for (GRNTreeNode n : chosenTU.getChildren()) {
                if (n instanceof LeafNode) {
                    LeafNode leafNode = (LeafNode) n;

                    // =================================================================
                    // Regulated part can be either Promoter, or Operator. Due to
                    // mathematical formulation of SVPs, positive regulation should be
                    // added using promoter, negative by operator.
                    // =================================================================

                    if (leafNode.getType() == SVPType.Prom) {
                        if(m.isRegulatedPromoter(leafNode.getSVP()))  {
                            regulatedPromoters.add(n);
                        }
                    } else if (leafNode.getType() == SVPType.Op) {
                        operators.add(n);
                    }
                }
            }

            // =================================================================
            // The probability of choosing any individual regulated part is
            // equally weighted (i.e. 1 / Operators + Promoters )
            // =================================================================

            int index = r.nextInt(regulatedPromoters.size() + operators.size());

            if ( index < regulatedPromoters.size() ) {

                if ( r.nextInt(2) == 0 || regulatedPromoters.size() < 2) {

                    // =================================================================
                    // Replace a regulated promoter with a constitutive promoter
                    // =================================================================

                    try {
                        regulatedPromoters.get(index).replaceNode( GRNTreeNodeFactory.getLeafNode ( m.getConstPromoter(), InterfaceType.NONE ) );
                        mutationOccured = true;
                    } catch (Exception ex) {
                        Logger.getLogger(RemoveRegulation.class.getName()).log(Level.SEVERE, null, ex);
                        return c;
                    }
                } else {

                    // =================================================================
                    // If there is more than one promoter, we can remove a regulated
                    // promoter without replacing it
                    // =================================================================

                    regulatedPromoters.get(index).getParent().removeNode(regulatedPromoters.get(index));
                    mutationOccured = true;
                }

            }  else {

                // =================================================================
                // Or, remove an operator
                // =================================================================

                index = index - regulatedPromoters.size();
                operators.get(index).getParent().removeNode(operators.get(index));
                mutationOccured = true;
            }

            attempt++;

            if(!mutationOccured && attempt == MAX_ATTEMPTS) {
                return c;
            }
        }

        return t;
    }
}
