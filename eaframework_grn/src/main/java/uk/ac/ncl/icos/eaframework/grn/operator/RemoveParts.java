package uk.ac.ncl.icos.eaframework.grn.operator;

import uk.ac.ncl.icos.annotation.EAModule;
import uk.ac.ncl.icos.eaframework.operator.AbstractOperator;
import uk.ac.ncl.icos.svpmanager.SVPManager;
import uk.ac.ncl.icos.grntree.api.GRNTree;
import uk.ac.ncl.icos.grntree.api.GRNTreeNode;
import uk.ac.ncl.icos.grntree.impl.LeafNode;
import uk.ac.ncl.icos.grntree.impl.SVPType;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.ncl.icos.eaframework.grn.GRNTreeChromosome;


/**
 * Randomly selects an RBS-CDS pair from a randomly chosen Transcriptional
 * Unit, and removes from the GRNTree.
 * @author owengilfellon
 */
@EAModule(visualName = "Remove Parts")
public class RemoveParts extends AbstractOperator<GRNTreeChromosome> {
    
    final private SVPManager m = SVPManager.getSVPManager();
    final private int MAX_ATTEMPTS = 10;

    @Override
    public GRNTreeChromosome apply(GRNTreeChromosome c) {
        
        GRNTreeChromosome t = (GRNTreeChromosome) c.duplicate();       
        List<GRNTreeNode> allTranscriptionalUnits = new ArrayList<GRNTreeNode>();
        Iterator<GRNTreeNode> it = t.getBreadthFirstIterator();
        Random r = new Random();
        
        // Retrieve all TUs from model by iterating through nodes and testing
        
        while(it.hasNext()) {
            GRNTreeNode n = it.next();
            if(n.isTranscriptionUnit()) {
                allTranscriptionalUnits.add(n);
            }
        }
        
        // If no TUs, then mutation cannot be performed. Return t.
        
        if(!allTranscriptionalUnits.isEmpty()) {
            
            GRNTreeNode chosenTU = allTranscriptionalUnits.get(r.nextInt(allTranscriptionalUnits.size()));
            List<GRNTreeNode> allRBSsInChosenTU = new ArrayList<GRNTreeNode>();
            
            /*
             * Retrieves all RBSs from the randomly selected TU
             */
            
            for(GRNTreeNode n:chosenTU.getChildren()){
                if(n instanceof LeafNode) {
                    LeafNode ln = (LeafNode) n;
                    if(ln.getType() == SVPType.RBS){
                        allRBSsInChosenTU.add(n);
                    }
                }
            }
            
            boolean mutationOccured = false;
            int attempt = 0;
            
            /*
             * Loop retrieves a random RBS-CDS pair, and removes it from the
             * parent node. If this would result in a TU with no CDSs, the
             * parent node itself is removed.
             * 
             * In the (exceptional) case in which the model has no TUs
             * containing RBS nodes, a MAX_ATTEMPTS is specified to terminate
             * the loop.
             */
            
            while(!mutationOccured && attempt<MAX_ATTEMPTS)
            {
                
                if(!allRBSsInChosenTU.isEmpty())
                {
                    /*
                     * If the parts to be removed contain the only coding sequence in
                     * the TU, the whole TU is removed.
                     */

                    if(allRBSsInChosenTU.size() == 1)
                    {
                        chosenTU.getParent().removeNode(chosenTU);
                        mutationOccured = true;
                    }
                    else
                    {

                        try {
                            LeafNode rbs = (LeafNode)allRBSsInChosenTU.get(r.nextInt(allRBSsInChosenTU.size()));
                            LeafNode cds = (LeafNode) rbs.getParent().getChildren().get(rbs.getParent().getChildren().indexOf(rbs) + 1);

                            /*
                             * Perform the removal
                             */

                            chosenTU.removeNode(rbs);
                            chosenTU.removeNode(cds);

                            mutationOccured = true;

                        } catch (Exception ex) {
                            Logger.getLogger(RemoveParts.class.getName()).log(Level.SEVERE, null, ex);
                            return c;
                        }
                    }
                }
                
                attempt++;

                if(!mutationOccured && attempt == MAX_ATTEMPTS) {
                    return c;
                }
            }            
        }

        return t;
    }
}
