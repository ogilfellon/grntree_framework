/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.grn.fitness;

import org.apache.commons.math3.stat.correlation.PearsonsCorrelation;
import uk.ac.ncl.icos.annotation.EAModule;
import uk.ac.ncl.icos.datatypes.Fitness;
import uk.ac.ncl.icos.eaframework.grn.simulator.CopasiSimulator;
import uk.ac.ncl.icos.eaframework.grn.Exporter;
import uk.ac.ncl.icos.eaframework.grn.GRNTreeChromosome;
import uk.ac.ncl.icos.eaframework.grn.exporter.SVPWriteExporter;
import uk.ac.ncl.icos.eaframework.grn.simulator.DynamicTimeCopasiSimulator;
import uk.ac.ncl.icos.svpcompiler.parsing.SVPParser;
import uk.ac.ncl.icos.grntree.api.GRNTree;

/**
 * Fitness function that penalises biologically implausible concentrations,
 * and decreases in input-output ratios.
 * @author owengilfellon
 */


@EAModule(visualName = "Linearity - Minimal Parts")
public class GRNLinearityMinimalParts extends GRNSimulationEvaluator<GRNTreeChromosome> {

    /**
     * To DO:
     *
     * Add weights to the various penalties to allow customisation
     * Also, look into methods used for multi-objective fitness functions.
     */

    private final double OUTPUT_LOWER_BOUND;
    private final double OUTPUT_UPPER_BOUND;
    private final double PENALTY_WEIGHT;
    private final double SIZE_PENALTY_WEIGHT = 10;

    private double targetRatio = 0.0;
    private double upperBoundPenalty = 0.0;
    private double lowerBoundPenalty = 0.0;
    private double partsSizePenalty = 0.0;
    private double tusSizePenalty = 0.0;
    private double observedRatio = 0.0;
    private double ratioPenalty = 0.0;
    private double correlation = 0.0;
    private double finalFitness = 0.0;

    public double getRatioPenalty() {
        return ratioPenalty;
    }

    public double getCorrelation() {
        return correlation;
    }

    public double getLowerBoundPenalty() {
        return lowerBoundPenalty;
    }

    public double getUpperBoundPenalty() {
        return upperBoundPenalty;
    }

    public double getPartsSizePenalty() { return partsSizePenalty; }

    public double getObservedRatio() {
        return observedRatio;
    }

    public double getFinalFitness() {
        return finalFitness;
    }

    public GRNLinearityMinimalParts(CopasiSimulator cs,
                                    double outputLowerBound,
                                    double outputUpperBound,
                                    double penaltyWeight)
    {
        super(cs);
        this.OUTPUT_LOWER_BOUND = outputLowerBound;
        this.OUTPUT_UPPER_BOUND = outputUpperBound;
        this.PENALTY_WEIGHT = penaltyWeight;
    }
    
    @Override
    public Fitness evaluate(GRNTreeChromosome c)
    {
        GRNTreeChromosome g = (GRNTreeChromosome) c;
        DynamicTimeCopasiSimulator ds = (DynamicTimeCopasiSimulator) cs;
        Exporter<String> exporter = new SVPWriteExporter();
        ds.setModel(SVPParser.getSBML(exporter.export(g)));


        if(ds.run())
        {
            double[] responseCurve = new double[ds.getDependentMetaboliteValues().size()];
            double[] independentsRun = new double[ds.getIndependentMetaboliteValues().size()];

            for(int i=0; i<ds.getDependentMetaboliteValues().size(); i++){
                responseCurve[i] = ds.getDependentMetaboliteValues().get(i).get(ds.getDependentMetaboliteValues().get(i).size()-1).doubleValue();
                independentsRun[i] = ds.getIndependentMetaboliteValues().get(i).doubleValue();
            }
            
            // ==================================================================================
            // Linearity calculated using Pearson's Correlation, disregarding negative correlation
            // ==================================================================================

            PearsonsCorrelation pc = new PearsonsCorrelation();
            double correlation = pc.correlation(independentsRun, responseCurve);

            double linearityFitness = correlation > 0.0 ? correlation * 100 : 0.0;
            this.correlation = linearityFitness;

            // ==================================================================================
            // Penalties for infeasible and undesirable output concentrations
            // ==================================================================================
            
            // The penalty for the upper bound penalises values close to OUTPUT_UPPER_BOUND, so the aim is to minimise this value
            // The penalty for the lower bound penalises values deviating from OUTPUT_LOWER_BOUND, so the aim is to maximise this value
            
            double cappedDifference = Math.max(OUTPUT_UPPER_BOUND - responseCurve[responseCurve.length-1], 0.0);

            this.upperBoundPenalty = 1.0 - (1.0 / (( cappedDifference / PENALTY_WEIGHT) + 1.0));
            this.lowerBoundPenalty = (1.0 / ((( responseCurve[0] - (OUTPUT_LOWER_BOUND) ) / PENALTY_WEIGHT ) + 1.0 ));
            
            // The fitness is penalised when upperBoundPenalty > 0.0
            // The fitness is penalised when lowerBoundPenalty < 1.0
            
            double upperPenalisedFitness = linearityFitness * upperBoundPenalty;
            double lowerPenalisedFitness = upperPenalisedFitness * lowerBoundPenalty;

            // ================================
            // Input-output ratio penalties
            // ================================

            // The input-output observedRatio is calculated using the highest values of each

            this.observedRatio = responseCurve[responseCurve.length-1] / independentsRun[independentsRun.length-1] ;

            // If starting model input-output observedRatio is not recorded, do so now

            if(this.targetRatio == 0.0){
                this.targetRatio = this.observedRatio;
            }
            
            // The observed input-output observedRatio, relative to that of the initial model is used as a penalty

            double ratio = this.observedRatio / this.targetRatio;

            this.ratioPenalty = ratio >= 1.0 ? 1.0 : ratio;
            
            // Higher input-output ratios are allowed, lower ratios decrease fitness.
            
            double ratioFitness = lowerPenalisedFitness * this.ratioPenalty;

            // =============================================================
            // ----- Penalties for model sizes -----
            // Trying to minimise the parts without a lower limit means that
            // it will be impossible to achieve maximum fitness.
            // =============================================================

            int parts = g.getPartsSize();
            this.partsSizePenalty = 1.0 / ((parts / SIZE_PENALTY_WEIGHT) + 1.0);

            // =============================================================
            // Calculate final fitness
            // =============================================================

            /*
            System.out.println("Linear: " + linearityFitness);
            System.out.println("Upper: " + upperBoundPenalty);
            System.out.println("Lower: " + lowerBoundPenalty);
            System.out.println("Ratio: " + ratioPenalty);
            System.out.println("PaPen: " + this.partsSizePenalty);
            System.out.println("Fit Penalty: " + (upperBoundPenalty * lowerBoundPenalty * ratioPenalty * partsSizePenalty));
            */

            this.finalFitness = linearityFitness *
                    (upperBoundPenalty *
                     lowerBoundPenalty *
                     ratioPenalty *
                     partsSizePenalty);
           
            // Return the final fitness
            
            return new Fitness(this.finalFitness);
        }
        else
        {
            // If there was a problem with the simulation, return Fitness = 0.0
            
            return new Fitness(0.0);
        }           
    } 
}
