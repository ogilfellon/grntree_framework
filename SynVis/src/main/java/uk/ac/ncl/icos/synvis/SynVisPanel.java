/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synvis;

import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import prefuse.data.CascadedTable;
import prefuse.data.Graph;
import prefuse.data.Node;
import prefuse.data.Table;
import uk.ac.ncl.icos.hierarchicalgraph.api.IDataModelEdge;
import uk.ac.ncl.icos.synbad.datadefinition.types.EntityType;
import uk.ac.ncl.icos.workspace.api.IEntity;
import uk.ac.ncl.icos.workspace.api.IInstance;
import uk.ac.ncl.icos.workspace.api.IPropertied;
import uk.ac.ncl.icos.workspace.impl.EntityInstance;
import uk.ac.ncl.icos.workspace.impl.ProxyPort;
import uk.ac.ncl.icos.workspace.model.api.DataModelCrawler;
import uk.ac.ncl.icos.workspace.model.api.IDataModel;
import uk.ac.ncl.icos.workspace.model.api.IDataModelCursor;
import uk.ac.ncl.icos.workspace.model.api.IDataModelView;
import uk.ac.ncl.icos.workspace.model.impl.DataModelImpl;
import uk.ac.ncl.icos.workspace.model.impl.IDataModelCrawlerAdapter;
import uk.ac.ncl.icos.workspace.model.impl.IteratorRule;

/**
 *
 * @author owengilfellon
 */
public class SynVisPanel extends JPanel {

    private final JLayeredPane background = new JLayeredPane();
    private final JLayeredPane border = new JLayeredPane();
    private final JLayeredPane entities = new JLayeredPane();
    private final JLayeredPane wires = new JLayeredPane();

    public SynVisPanel(IEntity entity) {
        
        final prefuse.data.Graph graph = new Graph();
        
        IDataModel model = new DataModelImpl(entity.getWorkspace(), entity);
        IDataModelView view = model.getCurrentView();
        DataModelCrawler crawler = new DataModelCrawler();
        crawler.addListener(new IteratorRule());
        
        crawler.addListener(new IDataModelCrawlerAdapter() {

            @Override
            public <T extends IPropertied, V extends IPropertied> void onEdge(IDataModelEdge<T, V> edge) {
                super.onEdge(edge); //To change body of generated methods, choose Tools | Templates.
                String from = addNode(edge.getFrom());
                String to = addNode(edge.getTo());
                if( //(edge instanceof IDataModelEdge.SignalEdge || edge.getTo().getValue().hasProperty(EntityType.class, EntityType.Port))&&
                    
                        graph.
                        graph.gete(from + "_" + to) == null) {
                    Edge e = graph.addEdge(from + "_" + to, from, to);
                    if(edge instanceof IDataModelEdge.StructureEdge) {
                        e.addAttribute("ui.style", "fill-color: rgb(25, 75, 75); stroke-mode:dots;");
                    } else if (edge instanceof IDataModelEdge.SignalEdge) {
                        if(edge.getTo().getValue() instanceof ProxyPort || edge.getFrom().getValue() instanceof ProxyPort) {
                            e.addAttribute("ui.style", "fill-color: rgb(200, 200, 50); size:2;");
                        } else {
                            e.addAttribute("ui.style", "fill-color: rgb(200, 200, 50); size:5;");
                        }
                        
                    }
                    
                }
                
            }
            
            private <T extends IPropertied> String addNode(IInstance<T> instance) {
                String id = instance.getIdentity().toString();
                if(graph.getNode(id) == null) {
                    Node n = graph.addNode(id);
                    
                    n.addAttribute("ui.style", "text-color: rgb(200, 200, 50);");
                    n.addAttribute("ui.label", instance.getValue().getName());
                    if(instance.getValue().hasProperty(EntityType.class, EntityType.Module)) {
                        n.addAttribute("ui.style", "fill-color: rgb(200, 200, 50);");
                        n.addAttribute("ui.style", "size: 30px, 30px;");
                    }
                    if(instance.getValue().hasProperty(EntityType.class, EntityType.Interaction)) {
                        n.addAttribute("ui.style", "fill-color: rgb(50, 150, 50);");
                        n.addAttribute("ui.style", "size: 20px, 20px;");
                    }
                       
                    if(instance.getValue().hasProperty(EntityType.class, EntityType.Component)) {
                        n.addAttribute("ui.style", "fill-color: rgb(100, 100, 255);");
                        n.addAttribute("ui.style", "size: 15px, 15px;");
                    }
                        
                    if(instance.getValue().hasProperty(EntityType.class, EntityType.Port)) {
                        n.addAttribute("ui.style", "fill-color: rgb(255,255,255);");
                        n.addAttribute("ui.style", "size: 5px, 5px;");
                        n.addAttribute("ui.style", "shape:box;");
                    }
                    
                }
                    
                return id;
            }
        });
        
        crawler.run(view.getGraph().getCursor());
        
        
        
        
        
    }

    
    
    
    
    
}
